//
//  HBBaseListModel.h
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/4/3.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HBBaseListModel : NSObject
//总页数
@property (nonatomic) NSInteger totalPages;

//是否最后一页
@property (nonatomic) BOOL isLast;

//当前第几页
@property (nonatomic) NSInteger number;
@end

NS_ASSUME_NONNULL_END
