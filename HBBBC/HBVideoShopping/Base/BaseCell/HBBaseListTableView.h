//
//  HBBaseListTableView.h
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/4/3.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBBaseTableView.h"
#import "HBBaseListTableCell.h"
#import "HBBaseListModel.h"
//#import "MJBaseGifHeader.h"
//#import "MJBaseGifFooter.h"
NS_ASSUME_NONNULL_BEGIN

@interface HBBaseListTableView : HBBaseTableView

@property (nonatomic, strong)NSMutableArray *allData;
//页码
@property (nonatomic) NSInteger pageIndex;

//每次请求的个数
@property (nonatomic) NSInteger pageSize;

//类型ID（自定义）
@property (nonatomic) NSInteger typeId;

//是否刷新过
@property (nonatomic) BOOL isRefreshed;

@property (nonatomic, strong) HBBaseListModel *baseNumModel;

//点击cell的block
@property (nonatomic, copy)void (^cellClickBlock)(NSIndexPath*indexPath);

//停止刷新
- (void)endMJRefreshing;
@end

NS_ASSUME_NONNULL_END
