//
//  HBBaseTableView.m
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/4/3.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBBaseTableView.h"

@interface HBBaseTableView ()<UITableViewDelegate>
{
    CGFloat tableViewOffset;
}
@end

@implementation HBBaseTableView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.estimatedRowHeight = 200;
        self.estimatedSectionHeaderHeight = 0;
        self.estimatedSectionFooterHeight = 0;
        self.delegate = self;
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    
    self = [super initWithFrame:frame style:style];
    if (self) {
        self.estimatedRowHeight = 200;
        self.estimatedSectionHeaderHeight = 0;
        self.estimatedSectionFooterHeight = 0;
        
        self.delegate = self;
    }
    return self;
}
@end
