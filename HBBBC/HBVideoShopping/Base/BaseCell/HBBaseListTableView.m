//
//  HBBaseListTableView.m
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/4/3.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBBaseListTableView.h"

@interface HBBaseListTableView ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation HBBaseListTableView


- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.isRefreshed = NO;
        self.allData = [NSMutableArray array];
        self.pageIndex = 1;
        self.pageSize = 10;
        
    }
    return self;
}

- (void)setBaseNumModel:(HBBaseListModel *)baseNumModel{
    _baseNumModel = baseNumModel;
    
    [self endMJRefreshing];
    if (baseNumModel.isLast) {
        [self.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)endMJRefreshing{
    [self.mj_header endRefreshing];
    [self.mj_footer endRefreshing];
}

#pragma mark TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 15.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _allData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HBBaseListTableCell *cell = [HBBaseListTableCell cellWithTableView:tableView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.cellClickBlock) {
        self.cellClickBlock(indexPath);
    }
}


@end
