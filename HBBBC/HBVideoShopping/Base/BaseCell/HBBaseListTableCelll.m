//
//  HBBaseTableCell.m
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/4/3.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBBaseListTableCell.h"

#define CELLID NSStringFromClass([self class])

@implementation HBBaseListTableCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+ (void)registerInTableView:(UITableView *)tableView{
    
    [tableView registerNib:[UINib nibWithNibName:CELLID bundle:nil] forCellReuseIdentifier:CELLID];
}

+ (instancetype)XIBcellWithTableView:(UITableView *)tableView{
    HBBaseListTableCell * cell = [tableView dequeueReusableCellWithIdentifier:CELLID];
    if (!cell) {
        UINib *nib = [UINib nibWithNibName:CELLID bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CELLID];
        cell = [tableView dequeueReusableCellWithIdentifier:CELLID];
    }
    
    return cell;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    
    // NSLog(@"cellForRowAtIndexPath");
    // 1.缓存中取
    HBBaseListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CELLID];
    // 2.创建
    if (cell == nil) {
        cell = [[[self class] alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELLID];
    }
    return cell;
}

- (CGFloat)defaultHeight{
    return 50;
}
@end
