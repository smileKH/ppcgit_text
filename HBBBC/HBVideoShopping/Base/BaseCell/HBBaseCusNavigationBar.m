//
//  HBBaseCusNavigationBar.m
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/5/17.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBBaseCusNavigationBar.h"

@implementation HBBaseCusNavigationBar

#pragma mark - 导航栏左右按钮事件
-(void)clickLeftButton:(UIButton *)button{
    if (self.onClickLeftButton) {
        self.onClickLeftButton();
    }
}
-(void)clickRightButton:(UIButton *)button{
    if (self.onClickRightButton) {
        self.onClickRightButton();
    }
}
#pragma mark - getter
-(UIButton *)leftButton
{
    if (!_leftButton) {
        _leftButton = [[UIButton alloc] init];
        [_leftButton addTarget:self action:@selector(clickLeftButton:) forControlEvents:UIControlEventTouchUpInside];
        _leftButton.imageView.contentMode = UIViewContentModeCenter;
        _leftButton.hidden = NO;
    }
    return _leftButton;
}
-(UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [[UIButton alloc] init];
        [_rightButton addTarget:self action:@selector(clickRightButton:) forControlEvents:UIControlEventTouchUpInside];
        _rightButton.imageView.contentMode = UIViewContentModeCenter;
        _rightButton.hidden = NO;
    }
    return _rightButton;
}
- (UIView *)centerView{
    if (!_centerView) {
        _centerView = [[UIView alloc]init];
        _centerView.backgroundColor = [UIColor clearColor];
    }
    return _centerView;
}
-(UIView *)bgView{
    if (!_bgView) {
        _bgView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = Color_BG;
            
            view ;
        }) ;
    }
    return _bgView ;
}
- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc] init];
        _bottomLine.backgroundColor = black_Color;
    }
    return _bottomLine;
}
@end
