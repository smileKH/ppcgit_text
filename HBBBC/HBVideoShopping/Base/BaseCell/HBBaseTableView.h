//
//  HBBaseTableView.h
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/4/3.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HBBaseTableView : UITableView

@end

NS_ASSUME_NONNULL_END
