//
//  HBBaseCusNavigationBar.h
//  HBGoldRoom
//
//  Created by 胡明波 on 2019/5/17.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface HBBaseCusNavigationBar : UIView
@property (nonatomic, copy) void(^onClickLeftButton)(void);
@property (nonatomic, copy) void(^onClickRightButton)(void);
@property (nonatomic, strong) UIButton  * leftButton;
@property (nonatomic, strong) UIButton  * rightButton;
@property (nonatomic ,strong) UIView    * bgView;
@property (nonatomic , strong)UIView      *centerView;
@property (nonatomic, strong) UIView      *bottomLine;//一根线
@end

NS_ASSUME_NONNULL_END
