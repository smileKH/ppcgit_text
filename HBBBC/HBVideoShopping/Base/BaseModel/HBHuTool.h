//
//  HBHuTool.h
//  HBRealTreasure
//
//  Created by 胡明波 on 16/12/18.
//  Copyright © 2016年 mingboJob. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBHuTool : NSObject
//返回一个正常的按钮
+(UIButton *)addTestClickButton:(NSString *)buttonName andFrame:(CGRect)frame andTarget:(id)target andAction:(SEL)action;
//返回导航栏按钮
+(UIBarButtonItem *)addBarButtonItemImage:(NSString *)image andTarget:(id)target andAction:(SEL)action;
+(UILabel *)addNavigationItemTitleName:(NSString *)name;
//返回一个正常label
+(UILabel *)addLabelNormalNameTiele:(NSString *)name;

//验证手机号码
+ (BOOL) validateMobile:(NSString *)mobile;

//验证身份证格式
+(BOOL)judgeIdentityStringValid:(NSString *)identityString;

//验证电子邮箱
+ (BOOL) validateEmail:(NSString *)email;

//验证密码
+ (BOOL) validatePassword:(NSString *) password;
//退出账号
+(BOOL)exitAccount;
+(NSMutableDictionary *)VEComponentsStringToDic:(NSString*)AllString withSeparateString:(NSString *)FirstSeparateString AndSeparateString:(NSString *)SecondSeparateString;
//计算结束时间与现在相差多少秒
+(NSInteger)currentDateOrEndDate:(NSNumber *)endDate;
//自己算秒杀的世界
+(NSInteger)getCurentDateHourMinuteSecond;
//获取计算是几点的场
+(NSString *)currentDataOrDatePlace:(NSNumber *)startData;
//获取最近的几个时间段
+(NSArray *)gainLatelyDataSet;
//获取当前时间段
+(NSString *)gainCurrentTiemePeriod;
//判断订单状态
+(NSString *)returnSteOrderStatus:(NSString *)orderStatus;
//判断一个数组是否为空
+(BOOL)judgeArrayIsEmpty:(NSArray *)arr;
//判断NSNumber是否为null 如果为空则返回YES
+(BOOL)isJudgeNumber:(NSNumber *)number;
//判断字符串是否为空 如果空返回yes  不空返回No
+(BOOL)isJudgeString:(NSString *)string;
//判断字典的某个值是否为NSNULL
+(BOOL)isJudgeDictionary:(NSString *)string;
//返回label的高度
+ (CGFloat)heighForText:(NSString *)string andScreenWidth:(CGFloat)width;
/**
 *  调整图片尺寸和大小
 *
 *  @param sourceImage  原始图片
 *  @param maxImageSize 新图片最大尺寸
 *  @param maxSize      新图片最大存储大小
 *
 *  @return 新图片imageData
 */
+ (NSData *)reSizeImageData:(UIImage *)sourceImage maxImageSize:(CGFloat)maxImageSize maxSizeWithKB:(CGFloat) maxSize;
//字体改变大小颜色
+(NSMutableAttributedString*)attributedPriceString:(NSString *)priceStr customColor:(UIColor *)customColor customFont:(UIFont *)font;
//设置金额颜色
+(NSMutableAttributedString*)attributedOldString:(NSString *)OldString priceString:(NSString *)priceStr;
//数组转json格式
+(NSString *)jsonArrayToJsonString:(NSArray *)array;
@end
