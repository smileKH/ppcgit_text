//
//  HBMessageButton.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/11/11.
//  Copyright © 2017年 fenzukeji. All rights reserved.
//

#import "HBMessageButton.h"

static const CGFloat imageHeightHig = 25;
@interface HBMessageButton()
{
    UILabel      *_badge;
}
@end
@implementation HBMessageButton
- (void)setButtonBadgeValue:(NSInteger)badgeValue{
    _badge = [[UILabel alloc] initWithFrame:CGRectMake(imageHeightHig/2 + 8, -10, 18, 18)];
    _badge.backgroundColor = Color_BG;
    [_badge setFont:Font12];
    _badge.textAlignment = NSTextAlignmentCenter;
    _badge.textColor = [UIColor redColor];
    _badge.userInteractionEnabled = NO;
    _badge.layer.cornerRadius = 9;
    _badge.layer.masksToBounds = YES;
    _badge.hidden = YES;
    
    //    _badge.tag = i;
    [self addSubview:_badge];
    
    _badge.text = [NSString stringWithFormat:@"%ld",(long)badgeValue];
    //    _badge.text  = @"1";
    _badge.hidden = NO;
    
    NSInteger count = badgeValue;
    if (count> 99) {
        _badge.frame = CGRectMake(_badge.frame.origin.x, _badge.frame.origin.y, 30, 18);
        _badge.text = @"99+";
    }
    else if (count > 9) {
        _badge.frame = CGRectMake(_badge.frame.origin.x, _badge.frame.origin.y, 24, 18);
    }
    else if (count > 0) {
        _badge.frame = CGRectMake(_badge.frame.origin.x, _badge.frame.origin.y, 18, 18);
    }
    else if (count == -1)
    {
        //显示小红点
        _badge.frame = CGRectMake(imageHeightHig/2 + 13, 6, 8, 8);
        _badge.layer.cornerRadius = 4;
        _badge.layer.masksToBounds = YES;
        _badge.text = @"";
    }
    else if (count == -2)
    {
        //显示小红点
        _badge.frame = CGRectMake(imageHeightHig/2 + 6, 3, 32, 18);
        _badge.text = @"new";
    }
    else {
        _badge.hidden = YES;
    }

}

@end
