//
//  HBMessageButton.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/11/11.
//  Copyright © 2017年 fenzukeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBMessageButton : UIButton

- (void)setButtonBadgeValue:(NSInteger)badgeValue;
@end
