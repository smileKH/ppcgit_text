//
//  HBMainTabBarController.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "HBMainTabBarController.h"
#import "UITabBar+CustomBadge.h"
#import "XYTabBar.h"

//根视图
#import "HomeVC.h"
#import "CategryViewController.h"
#import "ShoppingCarViewController.h"
#import "HBMineViewController.h"
#import "HBRecentlyMerchantsVC.h"
#import "HBbcManageHomeVC.h"
@interface HBMainTabBarController ()<UITabBarControllerDelegate>
@property (nonatomic,strong) NSMutableArray * VCS;//tabbar root VC
@end

@implementation HBMainTabBarController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    //初始化tabbar
    [self setUpTabBar];
    //添加子控制器
    [self setUpAllChildViewController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


#pragma mark ————— 初始化TabBar —————
-(void)setUpTabBar{
    //设置背景色 去掉分割线
    [self setValue:[XYTabBar new] forKey:@"tabBar"];
    [self.tabBar setBackgroundColor:[UIColor whiteColor]];
    [self.tabBar setBackgroundImage:[UIImage new]];
    //通过这两个参数来调整badge位置
    //    [self.tabBar setTabIconWidth:29];
    //    [self.tabBar setBadgeTop:9];
}

#pragma mark - ——————— 初始化VC ————————
-(void)setUpAllChildViewController{
    _VCS = @[].mutableCopy;
    //首页
     //HomeVC *home = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    HBbcManageHomeVC *home = [HBbcManageHomeVC new];
    [self setupChildViewController:home title:@"首页" imageName:@"icon_index0.imageset" seleceImageName:@"icon_index0_selected.imageset"];
    //分类
    CategryViewController *makeFriendVC = [[CategryViewController alloc] initWithNibName:@"CategryViewController" bundle:nil];
    [self setupChildViewController:makeFriendVC title:@"分类" imageName:@"icon_index1.imageset" seleceImageName:@"icon_index1_selected.imageset"];
    //
    HBRecentlyMerchantsVC *merchantsVC = [[HBRecentlyMerchantsVC alloc] initWithNibName:@"HBRecentlyMerchantsVC" bundle:nil];
    [self setupChildViewController:merchantsVC title:@"最近商家" imageName:@"icon_index2.imageset" seleceImageName:@"icon_index2_selected.imageset"];
    
    ShoppingCarViewController *cartVC = [ShoppingCarViewController new];
    [self setupChildViewController:cartVC title:@"购物车" imageName:@"icon_index3.imageset" seleceImageName:@"icon_index3_selected.imageset"];
    
    
    HBMineViewController *mineVC = [[HBMineViewController alloc]init];
    [self setupChildViewController:mineVC title:@"我的" imageName:@"icon_index4.imageset" seleceImageName:@"icon_index4_selected.imageset"];
    
    self.viewControllers = _VCS;
}

-(void)setupChildViewController:(UIViewController*)controller title:(NSString *)title imageName:(NSString *)imageName seleceImageName:(NSString *)selectImageName{
    controller.title = title;
    controller.tabBarItem.title = title;//跟上面一样效果
    controller.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    controller.tabBarItem.selectedImage = [[UIImage imageNamed:selectImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //未选中字体颜色
    [controller.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:black_Color,NSFontAttributeName:Font(10.0f)} forState:UIControlStateNormal];
    
    //选中字体颜色
    [controller.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MAINTextCOLOR,NSFontAttributeName:Font(10.0f)} forState:UIControlStateSelected];
    //包装导航控制器
     HBRootNavigationController*nav = [[HBRootNavigationController alloc]initWithRootViewController:controller];
    nav.tag = 1;
    
    //    [self addChildViewController:nav];
    [_VCS addObject:nav];
    
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    NSInteger selectIndex= tabBarController.selectedIndex;
    tabBarController.selectedIndex =selectIndex;
//    if (selectIndex == 2||selectIndex==3) {
//        if ([userManager judgeLoginState]) {
//            //登录成功
//            tabBarController.selectedIndex =selectIndex;
//        }
//        else{
//
//        }
//    }
//    else {
//        self.lsatInteger = selectIndex;
//        tabBarController.selectedIndex = selectIndex;
//
//    }
}

-(void)setRedDotWithIndex:(NSInteger)index isShow:(BOOL)isShow{
    if (isShow) {
        [self.tabBar setBadgeStyle:kCustomBadgeStyleRedDot value:0 atIndex:index];
    }else{
        [self.tabBar setBadgeStyle:kCustomBadgeStyleNone value:0 atIndex:index];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
