//
//  HBRootNavigationController.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 导航控制器基类
 */
@interface HBRootNavigationController : UINavigationController
/*!
 *  返回到指定的类视图
 *
 *  @param ClassName 类名
 *  @param animated  是否动画
 */
-(BOOL)popToAppointViewController:(NSString *)ClassName animated:(BOOL)animated;

@property (nonatomic, assign) NSInteger tag;
@end
