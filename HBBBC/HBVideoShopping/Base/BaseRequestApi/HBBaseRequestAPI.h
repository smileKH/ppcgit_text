//
//  HBBaseRequestAPI.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/21.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol HBBaseRequestAPIDelegate <NSObject>
@optional
/**
 数据加载完成
 */
-(void)requestDataCompleted;

@end
@interface HBBaseRequestAPI : NSObject
@property (nonatomic, strong)  NSMutableDictionary * parameters;
@property (nonatomic,assign) NSInteger  page;//页码
@property (nonatomic,strong) NSMutableArray * allData;//数据源

@property(nonatomic,weak)id<HBBaseRequestAPIDelegate> delegagte;
/**
 拉取数据
 */
-(void)loadData;
@end
