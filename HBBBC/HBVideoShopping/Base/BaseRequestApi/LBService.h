//
//  LBService.h
//  LJBiddingPlatform
//
//  Created by Fanxx on 16/1/13.
//  Copyright © 2016年 Do1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPNetworkHelper.h"
@class LBResponse;

/**
 *	@brief	请求参数编码
 */
typedef enum : NSUInteger{
    LBRequestParameterEncodingURL = 0,
    LBRequestParameterEncodingURLEncodedInURL,
    LBRequestParameterEncodingJSON,
} LBRequestParameterEncoding;
/**
 *	@brief	接口请求
 */
@interface LBRequest : NSObject

@property (strong,nonatomic) NSString *path;
@property (strong,nonatomic) NSString *method;
@property (assign,nonatomic) BOOL isCache;
@property (assign,nonatomic) LBRequestParameterEncoding encoding;
@property (strong,nonatomic) NSDictionary<NSString*,id> *params;
@property (strong,nonatomic) void(^completion)(LBResponse*);

@end

/**
 *	@brief	请求响应
 */
@interface LBResponse : NSObject

@property (strong,nonatomic) LBRequest *request;
@property (assign,nonatomic) NSInteger resultCode;
@property (strong,nonatomic) NSString *message;
@property (assign,nonatomic) BOOL succeed;
/**
 *  Logined表示登录有效性，值为0表示用户登录状态失效，值为1表示登录状态有效。
 */
@property (strong,nonatomic) id data;
@property (strong,nonatomic) NSDictionary<NSString*,id> *result;

@end

@interface LBService : NSObject

/**
 开启日志打印 (Debug级别)
 */
+ (void)openLog;

/**
 关闭日志打印,默认关闭
 */
+ (void)closeLog;
/**
 开启加密
 */
+(void)openAES;

/**
 关闭加密  关闭后要记得开启 不然影响其他接口
 */
+(void)closeAES;

///检查返回的数据
+(void)setResponseCheck:(BOOL(^)(LBResponse*))check;

+(void)pause:(LBRequest*)request;

///请求接口
+(void)request:(LBRequest*)request;

///GET请求
+(void)get:(NSString*)path params:(NSDictionary<NSString*,id>*)params completion:(void(^)(LBResponse* response))completion;
///POST请求
+(void)post:(NSString*)path params:(NSDictionary<NSString*,id>*)params completion:(void(^)(LBResponse* response))completion;

///GET请求 有缓存
+(void)get:(NSString*)path params:(NSDictionary<NSString*,id>*)params responseCache:(BOOL)isCache completion:(void(^)(LBResponse* response))completion;



@end
