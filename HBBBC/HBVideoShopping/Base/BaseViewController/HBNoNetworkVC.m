//
//  HBNoNetworkVC.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/11/13.
//  Copyright © 2017年 fenzukeji. All rights reserved.
//

#import "HBNoNetworkVC.h"

@interface HBNoNetworkVC ()

@end

@implementation HBNoNetworkVC

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //准备退出的时候
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"无网络连接";
    self.view.backgroundColor = white_Color;
    //添加子视图
    [self addNoNetworkSubView];
}

#pragma mark - 添加子视图
-(void)addNoNetworkSubView{
    //添加子视图
    UILabel *titleLab = [[UILabel alloc]init];
    titleLab.text = @"请设置你的网络";
    titleLab.font = textFont_Content14;
    
    UILabel *textView = [[UILabel alloc]init];
    textView.text = @"1、打开设备的“系统设置”>“无线网络”>“移动网络”。\n2、打开设备的“系统设置”>“Wi-fi”，“启动Wi-fi”后从中选择一个可用的热点连接。";
    textView.font = textFont_ContentVice12;
    textView.numberOfLines = 0;
    
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = Color_BG;
    
    UILabel *twoTitle = [[UILabel alloc]init];
    twoTitle.text = @"如果你已经连接Wi-Fi网络";
    twoTitle.font = textFont_Content14;
    
    UILabel *contentLab = [[UILabel alloc]init];
    contentLab.text = @"要确认你所接入的Wi-Fi网络已经连入互联网，或者确认你的设备是否被允许访问该热点";
    contentLab.numberOfLines = 0;
    contentLab.font = textFont_ContentVice12;
    
    [self.view sd_addSubviews:@[titleLab,textView,lineView,twoTitle,contentLab]];
    CGFloat top = 20;
    CGFloat left = 20;
    CGFloat labWid = SCREEN_WIDTH-2*left;
    
    
    titleLab.sd_layout
    .topSpaceToView(self.view,64)
    .leftSpaceToView(self.view,left)
    .widthIs(labWid)
    .heightIs(20);
    
    textView.sd_layout
    .topSpaceToView(titleLab,10)
    .leftEqualToView(titleLab)
    .widthIs(labWid)
    .heightIs(50);
    
    lineView.sd_layout
    .topSpaceToView(textView,20)
    .leftSpaceToView(self.view,0)
    .widthIs(SCREEN_WIDTH)
    .heightIs(15);
    
    twoTitle.sd_layout
    .topSpaceToView(lineView,top)
    .leftEqualToView(titleLab)
    .widthIs(labWid)
    .heightIs(20);
    
    contentLab.sd_layout
    .topSpaceToView(twoTitle,10)
    .leftEqualToView(titleLab)
    .widthIs(labWid)
    .heightIs(40);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
