//
//  HBRootViewController.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJRefresh.h>
#import "UIViewController+AlertViewAndActionSheet.h"
#import "UIScrollView+EmptyDataSet.h"
/**
 VC 基类
 */
@interface HBRootViewController : UIViewController<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>{
    UIView              *_contentNullPromptView;//无数据view
    UIImageView         *_contentNullPromptImgView;//默认图片
    UILabel             *_contentNullPromptLabel;//无数据label
    UIButton            *_contentNullPromptButton;//无数据按钮
    NSString            *_contentNullPrompt;//无数据content
    UIView            *_brokenNetworkView; //无网络view
}

/**
 *  修改状态栏颜色
 */
@property (nonatomic, assign) UIStatusBarStyle StatusBarStyle;
//数据数组
@property (nonatomic , strong)NSMutableArray * allData;
//当前页
@property (nonatomic , assign)int pageNo;
//每页大小
@property (nonatomic , assign)int pageSize;

//collectionview
//@property (nonatomic, strong) UICollectionView * collectionView;
//scrollView
@property (nonatomic, strong) UIScrollView * scrollView;

/**
 *  显示没有数据页面
 */
-(void)showNoDataImage;

/**
 *  移除无数据页面
 */
-(void)removeNoDataImage;

/**
 *  加载视图
 */
- (void)showLoadingAnimation;

/**
 *  停止加载
 */
- (void)stopLoadingAnimation;

/**
 *  是否显示返回按钮,默认情况是YES
 */
@property (nonatomic, assign) BOOL isShowLiftBack;

/**
 是否隐藏导航栏
 */
@property (nonatomic, assign) BOOL isHidenNaviBar;



/**
 导航栏添加文本按钮
 
 @param titles 文本数组
 @param isLeft 是否是左边 非左即右
 @param target 目标
 @param action 点击方法
 @param tags tags数组 回调区分用
 */
- (void)addNavigationItemWithTitles:(NSArray *)titles isLeft:(BOOL)isLeft target:(id)target action:(SEL)action tags:(NSArray *)tags;

/**
 导航栏添加图标按钮
 
 @param imageNames 图标数组
 @param isLeft 是否是左边 非左即右
 @param target 目标
 @param action 点击方法
 @param tags tags数组 回调区分用
 */
- (void)addNavigationItemWithImageNames:(NSArray *)imageNames isLeft:(BOOL)isLeft target:(id)target action:(SEL)action tags:(NSArray *)tags;

/**
 *  默认返回按钮的点击事件，默认是返回，子类可重写
 */
- (void)backBtnClicked;

//取消网络请求
- (void)cancelRequest;

//退出登录通知
- (void)logoutSuccess:(NSNotification *)noti;
//登录成功通知
- (void)loginSuccess:(NSNotification *)noti;
//隐藏
- (void)hideBrokenNetworkView;
//有网络的时候
-(void)haveNetwork:(NSNotification *)noti;
//没有网络的时候
-(void)brokenNetwork:(NSNotification *)noti;

//这一部分都是需要改的
//默认没有数据提示
-(void)showContentNullPromptNormal;
//改变图片和提示
-(void)showContentNullPromptImg:(NSString *)img withLabelName:(NSString *)name;
//改变图片和提示和按钮文字
-(void)showContentNullPromptImg:(NSString *)img withLabelName:(NSString *)name withButtonName:(NSString *)buttonName;
//隐藏提示
-(void)hideContentNullPromptView;
//默认pop 如果需要dismess 则重写back方法
- (void)back;

-(void)popoverActionSender:(UIButton *)button andIsShow:(BOOL)isShow andShareUrl:(NSString *)url;
//NSArray *temArray = self.navigationController.viewControllers;
//for(UIViewController *temVC in temArray){
//    if ([temVC isKindOfClass:[HBMineViewController class]]){
//        [self.navigationController popToViewController:temVC animated:YES];
//    }
//}
@end
