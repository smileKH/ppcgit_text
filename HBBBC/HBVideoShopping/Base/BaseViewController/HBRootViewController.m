//
//  HBRootViewController.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import <UShareUI/UShareUI.h>
#import "HBNoNetworkVC.h"
@interface HBRootViewController ()<UIGestureRecognizerDelegate>
@property (nonatomic,strong) UIImageView* noDataView;

@end

@implementation HBRootViewController
- (NSMutableArray *)allData{
    
    if (!_allData) {
        _allData = [NSMutableArray array];
    }
    return _allData;
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return _StatusBarStyle;
}
//动态更新状态栏颜色
-(void)setStatusBarStyle:(UIStatusBarStyle)StatusBarStyle{
    _StatusBarStyle=StatusBarStyle;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = white_Color;
    //初始化数据大小
    self.pageNo =1;
    //是否显示返回按钮
    self.isShowLiftBack = YES;
    //默认导航栏样式：黑字
    self.StatusBarStyle = UIStatusBarStyleDefault;
    //监听事件
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutSuccess:) name:NotificationLogoutSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:NotificationLoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(haveNetwork:) name:NotificationHaveNet object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(brokenNetwork:) name:NotificationBrokenNetwork object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginBackNotification:) name:NotificationHBLoginVC object:nil];
    //设置导航栏从原点开始
    self.automaticallyAdjustsScrollViewInsets = NO;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}
#pragma mark ==========点击登录返回通知==========
-(void)loginBackNotification:(NSNotification *)noti{
    
}
#pragma mark - 退出登录发通知
- (void)logoutSuccess:(NSNotification *)noti
{
    
}
#pragma mark - 登录成功发通知
- (void)loginSuccess:(NSNotification *)noti
{
    
}
#pragma mark - 有网络的时候
-(void)haveNetwork:(NSNotification *)noti{
    //有网络时候，隐藏界面提示，
    [self hideBrokenNetworkView];
    //先判断有没有登录，如果没有，那么重新自动登录
    //    [self setBrokenNetworkLogin];
}
#pragma mark - 没有网络的时候
-(void)brokenNetwork:(NSNotification *)noti{
    //没有网络的时候，显示界面提示
    [self showBrokenNetworkView];
}
//默认没有数据提示
-(void)showBrokenNetworkView{
    //直接展示原始值
    //添加无内容View
    [self addBrokenNetworkView];
}
- (void)hideBrokenNetworkView
{
    _brokenNetworkView.hidden = YES;
}
- (void)showLoadingAnimation
{
    
}

- (void)stopLoadingAnimation
{
    
}


-(void)showNoDataImage
{
    _noDataView=[[UIImageView alloc] init];
    [_noDataView setImage:[UIImage imageNamed:@"zanwushangp"]];
    [self.view.subviews enumerateObjectsUsingBlock:^(UITableView* obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UITableView class]]) {
            [_noDataView setFrame:CGRectMake(0, 0,obj.frame.size.width, obj.frame.size.height)];
            [obj addSubview:_noDataView];
        }
    }];
}

-(void)removeNoDataImage{
    if (_noDataView) {
        [_noDataView removeFromSuperview];
        _noDataView = nil;
    }
}


/**
 *  懒加载scrollView
 *
 *  @return scrollView
 */
-(UIScrollView *)scrollView{
    if (_scrollView==nil) {
        CGFloat height = SCREEN_HEIGHT - bNavAllHeight;
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , height)];
        _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, height);
        _scrollView.backgroundColor = white_Color;
        _scrollView.alwaysBounceVertical = YES;
    }
    return _scrollView;
}
/**
 *  是否显示返回按钮
 */
- (void) setIsShowLiftBack:(BOOL)isShowLiftBack
{
    _isShowLiftBack = isShowLiftBack;
    NSInteger VCCount = self.navigationController.viewControllers.count;
    //下面判断的意义是 当VC所在的导航控制器中的VC个数大于1 或者 是present出来的VC时，才展示返回按钮，其他情况不展示
    if (isShowLiftBack && ( VCCount > 1 || self.navigationController.presentingViewController != nil)) {
        [self addNavigationItemWithImageNames:@[@"nav_back"] isLeft:YES target:self action:@selector(backBtnClicked) tags:nil];
        
    } else {
        self.navigationItem.hidesBackButton = YES;
        UIBarButtonItem * NULLBar=[[UIBarButtonItem alloc]initWithCustomView:[UIView new]];
        self.navigationItem.leftBarButtonItem = NULLBar;
    }
}
- (void)backBtnClicked
{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
//默认pop 如果需要dismess 则重写back方法
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark ————— 导航栏 添加图片按钮 —————
/**
 导航栏添加图标按钮
 
 @param imageNames 图标数组
 @param isLeft 是否是左边 非左即右
 @param target 目标
 @param action 点击方法
 @param tags tags数组 回调区分用
 */
- (void)addNavigationItemWithImageNames:(NSArray *)imageNames isLeft:(BOOL)isLeft target:(id)target action:(SEL)action tags:(NSArray *)tags
{
    NSMutableArray * items = [[NSMutableArray alloc] init];
    //调整按钮位置
    //    UIBarButtonItem* spaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    //    //将宽度设为负值
    //    spaceItem.width= -5;
    //    [items addObject:spaceItem];
    NSInteger i = 0;
    for (NSString * imageName in imageNames) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
//        [btn setTitle:@"" forState:UIControlStateNormal];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        btn.frame = CGRectMake(0, 0, 35, 35);
//        btn.backgroundColor = red_Color;
        [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        // 注意:一定要在按钮内容有尺寸的时候,设置才有效果
       // btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        if (isLeft) {
            if (@available(iOS 11.0, *)){
                [btn setContentEdgeInsets:UIEdgeInsetsMake(10, -10, 10, 10)];
            }else{
                [btn setContentEdgeInsets:UIEdgeInsetsMake(6, -10, 6, 10)];
            }
            
        }else{
            if (@available(iOS 11.0, *)){
                [btn setContentEdgeInsets:UIEdgeInsetsMake(10, 10, 10, -10)];
            }else{
                [btn setContentEdgeInsets:UIEdgeInsetsMake(6, 10, 6, -10)];
            }
            
        }
//        btn.backgroundColor = red_Color;
        btn.tag = [tags[i++] integerValue];
        UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [items addObject:item];
        
    }
    if (isLeft) {
        self.navigationItem.leftBarButtonItems = items;
    } else {
        self.navigationItem.rightBarButtonItems = items;
    }
}

#pragma mark ————— 导航栏 添加文字按钮 —————
- (NSMutableArray<UIButton *> *)addNavigationItemWithTitles:(NSArray *)titles isLeft:(BOOL)isLeft target:(id)target action:(SEL)action tags:(NSArray *)tags
{
    
    NSMutableArray * items = [[NSMutableArray alloc] init];
    
    //调整按钮位置
    //    UIBarButtonItem* spaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    //    //将宽度设为负值
    //    spaceItem.width= -5;
    //    [items addObject:spaceItem];
    
    NSMutableArray * buttonArray = [NSMutableArray array];
    NSInteger i = 0;
    for (NSString * title in titles) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 30, 30);
        [btn setTitle:title forState:UIControlStateNormal];
        [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = Font(16);
        [btn setTitleColor:white_Color forState:UIControlStateNormal];
        btn.tag = [tags[i++] integerValue];
        [btn sizeToFit];
        
        //设置偏移
        if (isLeft) {
            [btn setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 10)];
        }else{
            [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
        }
        
        UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [items addObject:item];
        [buttonArray addObject:btn];
    }
    if (isLeft) {
        self.navigationItem.leftBarButtonItems = items;
    } else {
        self.navigationItem.rightBarButtonItems = items;
    }
    return buttonArray;
}

//取消请求
- (void)cancelRequest
{
    
}

#pragma mark ==========设置无网络代理==========
-(void)addBrokenNetworkView{
    
    if (_brokenNetworkView==nil) {
        _brokenNetworkView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 40)];
        _brokenNetworkView.backgroundColor = gray_Color;
        _brokenNetworkView.alpha = 0.9f;
        UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
        [currentWindow addSubview:_brokenNetworkView];
        
        //添加手势
        UITapGestureRecognizer * PrivateLetterTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAvatarView:)];
        PrivateLetterTap.numberOfTouchesRequired = 1; //手指数
        PrivateLetterTap.numberOfTapsRequired = 1; //tap次数
        PrivateLetterTap.delegate= self;
        [_brokenNetworkView addGestureRecognizer:PrivateLetterTap];
        
        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.image = [UIImage imageNamed:@"wifi.png"];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        
        UILabel *label = [[UILabel alloc]init];
        label.text = @"网络请求失败,请检查您的网络设置";
        label.textColor = [UIColor whiteColor];
        label.font = textFont_Content14;
        
        UIImageView *line = [[UIImageView alloc]init];
        line.image = [UIImage imageNamed:@"whiteWifi.png"];
        line.contentMode = UIViewContentModeScaleAspectFit;
        
        [_brokenNetworkView sd_addSubviews:@[imgView,label,line]];
        
        CGFloat imgWid = 30;
        CGFloat leftSpacing = 10;
        CGFloat spacing = 10;
        CGFloat lineWid = 20;
        CGFloat rightSpacing = 10;
        CGFloat labWid = SCREEN_WIDTH-imgWid-leftSpacing-spacing-lineWid-rightSpacing;
        CGFloat labHeight = 20;
        imgView.sd_layout
        .centerYEqualToView(_brokenNetworkView)
        .leftSpaceToView(_brokenNetworkView,leftSpacing)
        .widthIs(imgWid)
        .heightIs(imgWid);
        
        label.sd_layout
        .centerYEqualToView(_brokenNetworkView)
        .leftSpaceToView(imgView,spacing)
        .widthIs(labWid)
        .heightIs(labHeight);
        
        line.sd_layout
        .centerYEqualToView(_brokenNetworkView)
        .rightSpaceToView(_brokenNetworkView,rightSpacing)
        .widthIs(lineWid)
        .heightIs(lineWid);
    }
    else{
        _brokenNetworkView.hidden = NO;
    }
    
}
#pragma mark - 点击没有网络的view
-(void)tapAvatarView:(UITapGestureRecognizer *)gesture{
    //获取当前的vc
    UIViewController *current = [app getNowViewController];
    if (![current isKindOfClass:[HBNoNetworkVC class]]) {
        HBNoNetworkVC *vc = [[HBNoNetworkVC alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [current.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - 添加无内容view
- (void)addContentNullView:(BOOL)isShow
{
    if (_contentNullPromptView==nil) {
        
        //添加没内容view
        _contentNullPromptView = [[UIView alloc]init];
        _contentNullPromptView.backgroundColor = [UIColor clearColor];
        [self.view sd_addSubviews:@[_contentNullPromptView]];
        _contentNullPromptView.sd_layout
        .centerXEqualToView(self.view)
        .centerYEqualToView(self.view)
        .widthIs(SCREEN_WIDTH)
        .heightIs(180);
        
        
        _contentNullPromptImgView = [[UIImageView alloc]init];
        _contentNullPromptImgView.image = [UIImage imageNamed:@"zanwushangp"];
        _contentNullPromptImgView.contentMode = UIViewContentModeScaleAspectFit;
        
        
        _contentNullPromptLabel = [[UILabel alloc]init];
        _contentNullPromptLabel.backgroundColor = [UIColor clearColor];
        _contentNullPromptLabel.textColor = [UIColor grayColor];
        _contentNullPromptLabel.textAlignment = NSTextAlignmentCenter;
        _contentNullPromptLabel.font = [UIFont systemFontOfSize:14];
        _contentNullPromptLabel.tag = 0;
        _contentNullPromptLabel.text = @"没有找到对应的数据！";
        _contentNullPromptLabel.numberOfLines = 0;
        
        
        [_contentNullPromptView sd_addSubviews:@[_contentNullPromptImgView,_contentNullPromptLabel]];
        CGFloat spacing = 10;
        CGFloat labHeight = 20;
        CGFloat imgWidth = 80;
        CGFloat buttonWidth = 80;
        CGFloat buttonHeight = 40;
        
        
        _contentNullPromptLabel.sd_layout
        .centerXEqualToView(_contentNullPromptView)
        .centerYEqualToView(_contentNullPromptView)
        .heightIs(labHeight)
        .widthIs(SCREEN_WIDTH-2*spacing);
        
        _contentNullPromptImgView.sd_layout
        .bottomSpaceToView(_contentNullPromptLabel,spacing)
        .centerXEqualToView(_contentNullPromptView)
        .widthIs(imgWidth)
        .heightIs(imgWidth);
        
        if (isShow) {
            _contentNullPromptButton = [UIButton buttonWithType:UIButtonTypeCustom];
            //    [_contentNullPromptButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            //    [_contentNullPromptButton setImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
            [_contentNullPromptButton setTitle:@"刷新试试" forState:UIControlStateNormal];
            [_contentNullPromptButton setTitle:@"刷新试试" forState:UIControlStateHighlighted];
            [_contentNullPromptButton setTitleColor:white_Color forState:UIControlStateNormal];
            _contentNullPromptButton.backgroundColor = MAINTextCOLOR;
            _contentNullPromptButton.layer.borderWidth = 1;
            _contentNullPromptButton.layer.borderColor = MAINTextCOLOR.CGColor;
            _contentNullPromptButton.layer.cornerRadius = 5;
            [_contentNullPromptButton.layer setMasksToBounds:YES];
            [_contentNullPromptButton addTarget:self action:@selector(clickSpaceButton) forControlEvents:UIControlEventTouchUpInside];
            _contentNullPromptButton.titleLabel.font = Font(12);
            _contentNullPromptButton.tag = 52000;
            
            [_contentNullPromptView sd_addSubviews:@[_contentNullPromptButton]];
            
            _contentNullPromptButton.sd_layout
            .centerXEqualToView(_contentNullPromptView)
            .topSpaceToView(_contentNullPromptLabel,spacing)
            .widthIs(buttonWidth)
            .heightIs(buttonHeight);
        }
        
        
        
        
    }
    else{
        _contentNullPromptView.hidden = NO;
    }
    
}
//默认没有数据提示
-(void)showContentNullPromptNormal{
    //直接展示原始值
    //添加无内容View
    [self addContentNullView:NO];
}

//改变图片和提示
-(void)showContentNullPromptImg:(NSString *)img withLabelName:(NSString *)name{
    [self addContentNullView:NO];
    _contentNullPromptImgView.image = [UIImage imageNamed:img];
    _contentNullPromptLabel.text = name;
}
//改变图片和提示和按钮文字
-(void)showContentNullPromptImg:(NSString *)img withLabelName:(NSString *)name withButtonName:(NSString *)buttonName{
    [self addContentNullView:YES];
    if ([HBHuTool isJudgeString:img]) {
        _contentNullPromptImgView.image = [UIImage imageNamed:@"bbs_cartImg"];
    }
    else{
        _contentNullPromptImgView.image = [UIImage imageNamed:img];
    }
    if ([HBHuTool isJudgeString:name]) {
        _contentNullPromptLabel.text = @"没有找到对应的数据！";
    }
    else{
        _contentNullPromptLabel.text = name;
    }
    if ([HBHuTool isJudgeString:buttonName]) {
        [_contentNullPromptButton setTitle:@"刷新试试" forState:UIControlStateNormal];
        [_contentNullPromptButton setTitle:@"刷新试试" forState:UIControlStateHighlighted];
    }
    else{
        [_contentNullPromptButton setTitle:buttonName forState:UIControlStateNormal];
        [_contentNullPromptButton setTitle:buttonName forState:UIControlStateHighlighted];
    }
    
}



- (void)hideContentNullPromptView
{
    _contentNullPromptView.hidden = YES;
 
}
#pragma mark - 点击按钮
-(void)clickSpaceButton{
    
    //    NSLog(@"点击了重新加载按钮");
}

- (void)dealloc
{
    [self cancelRequest];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)popoverActionSender:(UIButton *)button andIsShow:(BOOL)isShow andShareUrl:(NSString *)url{
    PopoverAction *action1 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l1") title:@"    首页  " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 0;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    PopoverAction *action2 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l2") title:@"    分类    " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 1;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    PopoverAction *action3 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l3") title:@"    购物车    " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    PopoverAction *action4 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l4") title:@"    会员  " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 3;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    PopoverAction *action5 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l5") title:@"    分享  " handler:^(PopoverAction *action) {
        
    }];
    PopoverView *popoverView = [PopoverView popoverView];
    popoverView.style = PopoverViewStyleDark;
    popoverView.arrowStyle = PopoverViewArrowStyleTriangle;
    [popoverView showToView:button withActions:@[action1,action2,action3,action4,action5]];
}
#pragma mark EmptyDataSet DataSource
//无数据图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    return nil;
}
//无数据标题
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    NSString *text = @"没有找到对应的数据";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: MAINCOLOR};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
//字符明细
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView{
    NSString *text = @"";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
//背景色
- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView{
    return white_Color;
}
#pragma mark EmptyDataSet Delegate
//是否允许点击，默认YES
- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView{
    return YES;
}
//是否允许滚动，默认NO
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}
//点击整个view的代理方法，可在对应的子类中重写这个方法
- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{
    // Do something
    NSLog(@"刷新数据 didTapView");
}
//点击按钮代理方法，可在对应的子类中重写这个方法
- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button{
    // Do something
    NSLog(@"刷新数据 didTapButton");
}

@end
