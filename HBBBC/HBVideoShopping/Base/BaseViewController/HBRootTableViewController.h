//
//  HBRootTableViewController.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/25.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBRootTableViewController : HBRootViewController
//tableview
@property (nonatomic, strong) UITableView * tableView;
/**
 是否支持下拉刷新，默认是不支持
 */
@property (nonatomic, assign) BOOL isRefreshHeader;

/**
 是否支持上拉加载，默认是不支持
 */
@property (nonatomic, assign) BOOL isRefreshFooter;
/**
 *  刚开始关闭无数据图片
 */
@property (nonatomic, assign) BOOL isFirstLoad;
@property (nonatomic, strong) NSString *emptyTitle;          // 空数据显示内容
@property (nonatomic, strong) NSString *emptyDetail;          // 空数据详情
@property (nonatomic, strong) NSString *emptyImage;          // 空数据的图片
@end
