//
//  HBRootWebViewController.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLWebViewController.h"
@interface HBRootWebViewController : XLWebViewController
//在多级跳转后，是否在返回按钮右侧展示关闭按钮
@property(nonatomic,assign) BOOL isShowCloseBtn;
@end
