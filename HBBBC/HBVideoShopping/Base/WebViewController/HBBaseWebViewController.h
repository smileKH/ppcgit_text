//
//  HBBaseWebViewController.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/10/13.
//  Copyright © 2017年 fenzukeji. All rights reserved.
//

#import "HBRootViewController.h"
@protocol HBBaseWebViewDelegate<JSExport>
//返回token
-(NSString *)userInfo;
//点击返回按钮
- (void)backButton;
//转跳到登录
-(void)userLogin;
@end
@interface HBBaseWebViewController : HBRootViewController<UIWebViewDelegate, NSURLConnectionDelegate,HBBaseWebViewDelegate>
@property (nonatomic ,strong)UIWebView * webView;
@property (nonatomic , strong)JSContext * jsContext;
@property (nonatomic, strong) NSURLRequest *request;
//返回按钮
@property (nonatomic, strong) UIButton *backItem;
//关闭按钮
@property (nonatomic, strong) UIButton *closeItem;
//保存成功block
@property (nonatomic , copy)void(^webViewClassSuccessBack)();
//声明一个方法，外接调用时，只需要传递一个URL即可
- (void)loadHTML:(NSString *)htmlString;
//刷新
-(void)updateLoadHtml;
@end
