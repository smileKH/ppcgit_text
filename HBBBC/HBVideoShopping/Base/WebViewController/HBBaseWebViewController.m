//
//  HBBaseWebViewController.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/10/13.
//  Copyright © 2017年 fenzukeji. All rights reserved.
//

#import "HBBaseWebViewController.h"
@interface NSURLRequest (InvalidSSLCertificate)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;

@end
@interface HBBaseWebViewController ()

//判断是否是HTTPS的
@property (nonatomic, assign) BOOL isAuthed;
@property (nonatomic , strong)NSTimer * timer;
@property (nonatomic , strong)NSString * htmlString;
@property (nonatomic , strong)NSString * url;
@property (nonatomic , strong)NSURL * oldUrl;
@property (nonatomic , assign) BOOL shouldIntercept;
@end

@implementation HBBaseWebViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //释放资源,防止闪退
    self.webView.delegate = nil;
    self.request = nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self.webView removeAllSubview];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //创建webView
//    [self addBaseWebView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.shouldIntercept = NO;
    self.view.backgroundColor = Color_BG;
    //添加返回按钮
    [self addGoodsDetailSubView];
}
//刷新
-(void)updateLoadHtml{
    [self addGoodsDetailSubView];
}
#pragma mark - 添加子视图
-(void)addGoodsDetailSubView{
    
    if (self.webView) {
        [self.webView stopLoading];
        self.webView.delegate = nil;
        [self.webView removeFromSuperview];
        self.webView = nil;
    }
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.webView.backgroundColor = [UIColor whiteColor];
    for (UIView * views in [self.webView subviews]) {
        if ([views isKindOfClass:[UIScrollView class]]) {
            //去掉水平方向的滑动条
            [(UIScrollView *)views setShowsHorizontalScrollIndicator:NO];
            //去掉垂直方向的滑动条
            [(UIScrollView *)views setShowsVerticalScrollIndicator:NO];
            for (UIView * inScrollView in views.subviews) {
                if ([inScrollView isKindOfClass:[UIImageView class]]) {
                    //隐藏上下滚动出边界时的黑色的图片
                    inScrollView.hidden = YES;
                }
            }
        }
    }
    self.webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
    //加载上下文
//    [self loadGoodsJSContextAndURL];
}
#pragma mark - 加载URL
- (void)loadHTML:(NSString *)htmlString
{
    self.htmlString = htmlString;
    self.url = htmlString;
    [self loadGoodsJSContextAndURL];
}
//设置加载10如果不成功，那么就别加载了
-(void)action{
    [MBProgressHUD hideHUD];
}

#pragma mark - 加载上下文和链接
-(void)loadGoodsJSContextAndURL{
    [self loadJSContext];
    NSURL *url = [NSURL URLWithString:self.url];
    self.request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
    [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
    [self.webView loadRequest:self.request];
    [MBProgressHUD showHUDisWindiw:NO];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(action) userInfo:nil repeats:NO];
}
#pragma mark - 加载上下文
-(void)loadJSContext{
    self.jsContext = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    //将tianbai对象指向自身
    self.jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        NSLog(@"异常信息：%@", exceptionValue);
    };
    self.jsContext[@"nativeToWeb"] = self;
    
}
//点击返回按钮
- (void)backButton{
    //如果当前链接与加载链接一样，那么返回APP界面
    if ([[self.oldUrl absoluteString] isEqualToString:self.url]) {
        if ([NSThread isMainThread]) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }
    else{
        //如果加载链接与当前链接不一样，那么加载加载链接
        if (self.jsContext != nil){
            //重新加载
            [self addGoodsDetailSubView];
            self.shouldIntercept =NO;
        }
    }
}
//关闭H5页面，直接回到原生页面
- (void)closeNative
{
    NSLog(@"点击了原生的返回按钮");
    [self backButton];
}
#pragma mark - 返回token给他们
- (NSString *)userInfo{
    NSString *token = USERDEFAULT_value(TOKEN_XY_APP);
    return token;
}
//转跳到登录
-(void)userLogin{
    if ([NSThread isMainThread]) {
        //还没有登录
        //UIViewController *currentVC = [app getNowViewController];
//        HBLoginVC *loginVC = [[HBLoginVC alloc] init];
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginVC];
//        [currentVC presentViewController:navController animated:YES completion:nil];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            //还没有登录
//            UIViewController *currentVC = [app getNowViewController];
//            HBLoginVC *loginVC = [[HBLoginVC alloc] init];
//            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginVC];
//            [currentVC presentViewController:navController animated:YES completion:nil];
        });
    }
}



#pragma mark - UIWebViewDelegate
//开始加载
- (BOOL)webView:(UIWebView *)awebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    if (self.jsContext != nil &&self.shouldIntercept){
        //当转跳界面的时候，先把之前的webview去掉，然后重新添加
        [self.webView stopLoading];
        self.webView.delegate = nil;
        [self.webView removeFromSuperview];
        self.webView = nil;
        //重新创建webview
        self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        self.webView.delegate = self;
        [self.view addSubview:self.webView];
        //加载界面
        self.request = [NSURLRequest requestWithURL:[request URL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
        [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[[request URL] host]];
        [self.webView loadRequest:self.request];
        
        //加载上下文
        [self loadJSContext];
        self.shouldIntercept =NO;
        return YES;
    }
    else{
        
        //加载
        [self loadJSContext];
        self.shouldIntercept =YES;
        
    }
    //那就判断URL啊
    //    NSLog(@"打印URL:%@",[request URL]);
    //记录当前的URL，用来判断返回事件
    self.oldUrl = [request URL];
    
    return YES;
}

//设置webview的title为导航栏的title
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUD];
    [self.timer invalidate];
    //隐藏返回按钮
    // 防止内存飙升
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitDiskImageCacheEnabled"];//自己添加的，原文没有提到。
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitOfflineWebApplicationCacheEnabled"];//自己添加的，原文没有提到。
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self addLeftButton];
    [_backItem setImage:[UIImage imageNamed:@"whiteBack.png"] forState:0];
    _backItem.hidden = YES;
    [self hideContentNullPromptView];
}
#pragma mark- 加载失败的情况
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    //显示加载失败 或者界面上没有数据怎么办
    [MBProgressHUD hideHUD];
    [self.timer invalidate];
    [self addLeftButton];
    //添加一个背景图片
    [self addNoDataWebView];
    
}
-(void)addNoDataWebView{
    [app showToastView:@"加载失败"];
    [_backItem setImage:[UIImage imageNamed:@"Back.png"] forState:0];
//    _contentNullPromptView = nil;
    _backItem.hidden = NO;
    [self showContentNullPromptImg:nil withLabelName:nil withButtonName:nil];
    
}
- (void)clickSpaceButton{
    //刷新数据
    [self addGoodsDetailSubView];
}
#pragma mark ================= NSURLConnectionDataDelegate <NSURLConnectionDelegate>
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] == 0) {
        self.isAuthed = YES;
        //NSURLCredential 这个类是表示身份验证凭据不可变对象。凭证的实际类型声明的类的构造函数来确定。
        NSURLCredential *cre = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        [challenge.sender useCredential:cre forAuthenticationChallenge:challenge];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [app showToastView:@"网络不给力"];
    NSLog(@"网络不给力");
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.isAuthed = YES;
    //webview 重新加载请求。
    [self.webView loadRequest:self.request];
    [connection cancel];
}
#pragma mark - 添加关闭按钮
- (void)addLeftButton
{
    [self.view addSubview:self.backItem];
    
}

//点击返回的方法
- (void)backNative
{
    //判断是否有上一层H5页面
    if ([self.webView canGoBack]) {
        //如果有则返回
        [self.webView goBack];
        //同时设置返回按钮和关闭按钮为导航栏左边的按钮
        //        self.navigationItem.leftBarButtonItems = @[self.backItem, self.closeItem];
        [self.view addSubview:self.backItem];
        [self.view addSubview:self.closeItem];
    } else {
        [self closeNative];
    }
}

#pragma mark - init
- (UIButton *)backItem
{
    if (!_backItem) {
        _backItem = [UIButton buttonWithType:UIButtonTypeSystem];
        _backItem.frame = CGRectMake(10, 20, 45, 35);
        //        [_backItem setBackgroundColor:[UIColor redColor]];
        //        _backItem.layer.zPosition = 999;
        [_backItem setImage:[UIImage imageNamed:@"Back.png"] forState:0];
        [_backItem setImageEdgeInsets:UIEdgeInsetsMake(4, -10, 4, 10)];
        [_backItem.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [_backItem addTarget:self action:@selector(backNative) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backItem;
}

- (UIButton *)closeItem
{
    if (!_closeItem) {
        _closeItem = [UIButton buttonWithType:UIButtonTypeSystem];
        _closeItem.frame = CGRectMake(55, 20, 40, 20);
        //        _closeItem.layer.zPosition = 998;
        _closeItem.backgroundColor = [UIColor redColor];
        //        [_closeItem setTitle:@"" forState:UIControlStateNormal];
        //        [_closeItem setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_closeItem addTarget:self action:@selector(closeBack) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _closeItem;
}
-(void)closeBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.webView loadHTMLString:@"" baseURL:nil];
    [self.webView stopLoading];
    [self.webView removeFromSuperview];
    self.webView = nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    NSLog(@"释放了webview");
}



@end
