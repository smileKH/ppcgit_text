//
//  HBEvaluationModel.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/31.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBEvaluationListModel;
@interface HBEvaluationModel : NSObject
@property (nonatomic ,strong)NSString *status;
@property (nonatomic ,strong)NSString *buyer_rate;
@property (nonatomic ,strong)NSArray *orders;
@property (nonatomic ,strong)NSString *refund_fee;
@property (nonatomic ,strong)NSString *shoplogo;
@property (nonatomic ,strong)NSString *shop_type;
@property (nonatomic ,strong)NSString *shop_id;
@property (nonatomic ,strong)NSString *tid;
@property (nonatomic ,strong)NSString *shopname;

@end

@interface HBEvaluationListModel : NSObject
@property (nonatomic ,strong)NSString *buyer_rate;
@property (nonatomic ,strong)NSString *status;
@property (nonatomic ,strong)NSString *end_time;
@property (nonatomic ,strong)NSString *aftersales_status;
@property (nonatomic ,strong)NSString *title;
@property (nonatomic ,strong)NSString *pic_path;
@property (nonatomic ,strong)NSString *oid;
@property (nonatomic ,strong)NSString *item_id;
@property (nonatomic, strong)NSString *textViewStr;
/**
 0 好评 1 中评 2 差评
 */
@property (nonatomic, assign) int evaluationType;
@end
