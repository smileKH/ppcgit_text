//
//  HBOrderEvaluationCell.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/31.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderEvaluationCell.h"

@implementation HBOrderEvaluationCell
- (void)setModel:(HBEvaluationListModel *)model{
    _model = model;
    self.textLab.text = [NSString stringWithFormat:@"%@",model.title];
    self.chiMaLab.text = [NSString stringWithFormat:@""];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    
}
- (IBAction)btnActions:(UIButton *)sender {
    [self resetUI];
    sender.selected = YES;
    sender.backgroundColor = UIColorFromHex(0x2BA6FF);
    self.model.evaluationType = (int)sender.tag-100;
    if (_evaluationCallback) {
        _evaluationCallback(_model,(int)sender.tag-100);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
//    [_textView addPlaceHolder:@"评论内容最多300字"];
//    _textView.maxLength = @(300);
    UIColor *bc = UIColorFromHex(0xF2F2F2);
    _textView.layer.borderColor = bc.CGColor;
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.layer.borderWidth = 1;
     _textView.layer.masksToBounds = YES;
    _textView.layer.cornerRadius = 8;
    _textView.delegate = self;
    _zhongBtn.layer.cornerRadius = 8;
    _zhongBtn.clipsToBounds = YES;
    _chaBtn.layer.cornerRadius = 8;
    _chaBtn.clipsToBounds = YES;
    _goodBtn.layer.cornerRadius = 8;
    _goodBtn.clipsToBounds = YES;
    [self resetUI];
    _goodBtn.selected = YES;
    _goodBtn.backgroundColor = UIColorFromHex(0x2BA6FF);
    
    self.model.evaluationType = 0;
}

-(void)resetUI{
    _goodBtn.selected = NO;
    
    _zhongBtn.selected = NO;
    
    _chaBtn.selected = NO;
    
    _goodBtn.backgroundColor = _zhongBtn.backgroundColor = _chaBtn.backgroundColor = UIColorFromHex(0xF2F2F2);

}

- (void)textViewDidEndEditing:(UITextView *)textView{
    self.model.textViewStr = textView.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
