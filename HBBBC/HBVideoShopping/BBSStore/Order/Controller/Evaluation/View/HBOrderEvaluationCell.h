//
//  HBOrderEvaluationCell.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/31.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBEvaluationModel.h"
#import "UITextView+PlaceHolder.h"
typedef void (^EvaluationCallback)(HBEvaluationListModel *model , int evaluationType );

@interface HBOrderEvaluationCell : UITableViewCell<UITextViewDelegate>
@property (nonatomic, copy  ) EvaluationCallback evaluationCallback;
/**
 0 好评 1 中评 2 差评
 */
@property (nonatomic, assign) int evaluationType;
@property (nonatomic ,strong)HBEvaluationListModel *model;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;
@property (weak, nonatomic) IBOutlet UILabel *textLab;
@property (weak, nonatomic) IBOutlet UILabel *chiMaLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *zhongBtn;
@property (weak, nonatomic) IBOutlet UIButton *chaBtn;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *goodBtn;


@property (nonatomic, copy  ) NSString *text;


@end
