//
//  HBOrderEvaluationVC.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderEvaluationVC.h"
#import "HBOrderEvaluationCell.h"
#import "HBEvaluationModel.h"
#import "UITextView+PlaceHolder.h"
@interface HBOrderEvaluationVC ()<UITableViewDelegate>
@property (nonatomic ,strong)HBEvaluationModel *evModel;

@property (nonatomic, assign) BOOL unName;
@end

@implementation HBOrderEvaluationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _unName = NO;
    self.title = @"评价";
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self setupTableview];
    [self setNavBar];
    //请求数据
    [self requestOrderEvaluation];
}

-(void)requestOrderEvaluation{
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"tid"] = self.tid;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MEMBER_RATE_GET params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.evModel = [HBEvaluationModel mj_objectWithKeyValues:response.result[@"data"]];
            [weakSelf.tableView reloadData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}

- (void)setupTableview
{
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderEvaluationCell" bundle:nil] forCellReuseIdentifier:@"HBOrderEvaluationCell"];
    self.tableView.tableFooterView = [[UIView alloc]init];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.evModel.orders.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
       HBOrderEvaluationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBOrderEvaluationCell"];
    cell.model = self.evModel.orders[indexPath.section];
    cell.shopNameLab.text = [NSString stringWithFormat:@"%@",self.evModel.shopname];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    WEAKSELF;
    cell.evaluationCallback = ^(HBEvaluationListModel *model, int evaluationType) {
//        weakSelf.evaluationType = evaluationType;
    };
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5)];
    return v;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 370;
}

#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
- (IBAction)clickNiBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
    _unName = !sender.selected;
}
- (IBAction)clickSaveBtn:(UIButton *)sender {
    
    NSMutableArray *cart_params = [NSMutableArray array];
    for (HBEvaluationListModel *evModel in self.evModel.orders) {//商品
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        parameters[@"oid"] = evModel.oid;
        NSString * vericaStr= @"good";
        if (evModel.evaluationType == 1) {
            vericaStr = @"neutral";
        }else if (evModel.evaluationType == 2) {
            vericaStr = @"bad";
        }
        parameters[@"result"] = vericaStr;
        parameters[@"content"] = evModel.textViewStr;
        
        [cart_params addObject:parameters];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cart_params
                                                       options:kNilOptions
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    // 是否匿名，true匿名 false不匿名
    NSString * anony = @"false";
    if (_unName) {
        anony = @"true";
    }
    // _unName YES 匿名 NO 不匿名
    // evaluationType 0 好评 1 中评 2差评
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"tid"] = self.evModel.tid;
    parameters[@"rate_data"] = jsonString;
    parameters[@"anony"] = anony;
    parameters[@"tally_score"] = @(5);
    parameters[@"attitude_score"] = @(5);
    parameters[@"delivery_speed_score"] = @(5);
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MEMBER_RATE_ADD params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [app showToastView:response.message];
            //发通知
            [[NSNotificationCenter defaultCenter]postNotificationName:@"HBOrderEvaluationVC" object:nil];
            
        }else{
            [app showToastView:response.message];
        }
    }];
    
    
}
@end
