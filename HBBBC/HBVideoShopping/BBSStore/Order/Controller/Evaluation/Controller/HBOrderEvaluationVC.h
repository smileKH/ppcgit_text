//
//  HBOrderEvaluationVC.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBOrderEvaluationVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)clickNiBtn:(UIButton *)sender;
- (IBAction)clickSaveBtn:(UIButton *)sender;
//@property (nonatomic ,strong)HBOrderList *model;
@property (nonatomic ,strong)NSString *tid;
@end
