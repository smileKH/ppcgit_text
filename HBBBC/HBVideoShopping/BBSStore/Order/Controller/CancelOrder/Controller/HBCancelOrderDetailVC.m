//
//  HBCancelOrderDetailVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCancelOrderDetailVC.h"
#import "HBCancelOrderHeaterCell.h"
#import "HBCancelOrderContentCell.h"
#import "HBCancelDetailModel.h"
@interface HBCancelOrderDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) HBCancelDetailModel * detailModel;
@end

@implementation HBCancelOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"取消订单详情";
    [self setupUI];
    //请求数据
    [self requestCancelOrderData];
}
#pragma mark ==========请求数据==========
-(void)requestCancelOrderData{
    
    WEAKSELF;
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"cancel_id"] = self.cancel_id;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MY_ORDER_CANCEL_GET params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.detailModel = [HBCancelDetailModel mj_objectWithKeyValues:response.result[@"data"]];

        }else{
            [app showToastView:response.message];
        }
        [weakSelf.tableView reloadData];
    }];
}
#pragma mark ==========添加子视图==========
-(void)setupUI{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"HBCancelOrderHeaterCell" bundle:nil] forCellReuseIdentifier:@"HBCancelOrderHeaterCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"HBCancelOrderContentCell" bundle:nil] forCellReuseIdentifier:@"HBCancelOrderContentCell"];
    [self.view addSubview:self.tableView];
}

#pragma mark ==========tableView Delegate==========
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        HBCancelOrderHeaterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBCancelOrderHeaterCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.detailModel;
        return cell;
    }else{
        HBCancelOrderContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBCancelOrderContentCell"];
        cell.model = self.detailModel;
        return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 250;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
