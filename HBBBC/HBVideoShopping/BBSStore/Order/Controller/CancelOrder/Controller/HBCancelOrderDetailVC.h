//
//  HBCancelOrderDetailVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootTableViewController.h"

@interface HBCancelOrderDetailVC : HBRootTableViewController
@property (nonatomic, strong) NSString * cancel_id;
@end
