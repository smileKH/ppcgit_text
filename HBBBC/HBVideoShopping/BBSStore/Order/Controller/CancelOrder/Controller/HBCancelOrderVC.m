//
//  HBCancelOrderVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCancelOrderVC.h"
#import "HBCancelOrderCell.h"
#import "HBMyOrderDetailVC.h"
#import "HBCancelOrderDetailVC.h"
#import "HBCancelModel.h"
@interface HBCancelOrderVC ()<UITableViewDelegate,UITableViewDataSource>
//@property (nonatomic, strong) HBMyOrderModel * orderModel;
@end

@implementation HBCancelOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"取消订单列表";
    //
    [self setupUI];
    //请求数据
    [self requestCancelOrderData:YES];
    WEAKSELF;
    //默认block方法：设置下拉刷新
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestCancelOrderData:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestCancelOrderData:NO];
//    }];
}
#pragma mark ==========请求数据==========
-(void)requestCancelOrderData:(BOOL)isRefresh{
    
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    WEAKSELF;
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MY_ORDER_CANCEL_LIST params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"total"]intValue];
                NSArray *arr = [dic objectForKey:@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [HBCancelModel mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            }

        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptImg:@"bbs_orderImg" withLabelName:@"您还没有相关的订单记录"];
        }
        [weakSelf.tableView reloadData];
//        [weakSelf.tableView.mj_footer endRefreshing];
//        [weakSelf.tableView.mj_header endRefreshing];
    }];
}
#pragma mark ==========添加子视图==========
-(void)setupUI{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"HBCancelOrderCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
}

#pragma mark ==========tableView Delegate==========
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.allData.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HBCancelOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.allData[indexPath.row];
    WEAKSELF;
    cell.clickPayStateBtn = ^(HBCancelModel *model, NSString *btnStyle) {
        [weakSelf clickStateBtnModel:model];
    };
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 250;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HBCancelModel *model =self.allData[indexPath.row];
    HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
    vc.controllerClass = @"HBCancelOrderVC";
    vc.tidStr = model.tid;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========点击查看详情==========
-(void)clickStateBtnModel:(HBCancelModel *)model{
    //查看详情
    HBCancelOrderDetailVC *vc = [[HBCancelOrderDetailVC alloc]init];
    vc.cancel_id = model.cancel_id;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
