//
//  HBCancelReasonSelectVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/7/5.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBCancelReasonSelectVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)clickConfBtn:(id)sender;
@property (nonatomic, strong) NSString * tid;
@end
