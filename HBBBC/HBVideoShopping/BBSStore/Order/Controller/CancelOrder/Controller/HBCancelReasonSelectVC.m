//
//  HBCancelReasonSelectVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/7/5.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCancelReasonSelectVC.h"
#import "HBOrderPayStyleCell.h"
@interface HBCancelReasonSelectVC ()
@property (nonatomic, strong) NSMutableArray * payStyleArr;
@property (nonatomic, assign) NSInteger  selectIndex;
@end

@implementation HBCancelReasonSelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"取消订单";
    //请求投诉类型
    [self requesTouSuData];
    self.payStyleArr = [NSMutableArray array];
    self.selectIndex = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderPayStyleCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.tableFooterView = [[UIView alloc]init];
    
    //添加headerview
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 40)];
    lineView.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:lineView];
    
    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 50, 20)];
    lab.text = @"订单号:";
    lab.font = Font(14);
    [lineView addSubview:lab];
    
    UILabel *detailLab = [[UILabel alloc]initWithFrame:CGRectMake(80, 10, 200, 20)];
    detailLab.text = self.tid;
    detailLab.textColor = [UIColor colorWithHexString:@"FF3363"];
    detailLab.font = Font(14);
    [lineView addSubview:detailLab];
    
    self.tableView.tableHeaderView = headerView;
}
#pragma mark ==========取消原因===========
-(void)requesTouSuData{
    WEAKSELF;
    [LBService post:MY_ORDER_REASON_GET params:nil completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSArray *arr = dic[@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    for (NSInteger i=0; i<arr.count; i++) {
                        HBOrderPayStyleModel *model = [HBOrderPayStyleModel new];
                        model.name = arr[i];
                        if (i==0) {
                            model.isSelect = YES;
                        }else{
                            model.isSelect = NO;
                        }
                        [self.payStyleArr addObject:model];
                    }
                }
            }
            
            [weakSelf.tableView reloadData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HBOrderPayStyleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.payStyleArr[indexPath.row];
    WEAKSELF;
    cell.clickPayStyleBtn = ^(HBOrderPayStyleModel *model) {
        [weakSelf selectIndexPath:indexPath];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.payStyleArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 200, 20)];
    lab.text = @"请选择取消订单原因";
    lab.font = Font(14);
    [headerView addSubview:lab];
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(20, 40-1, SCREEN_WIDTH-20, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    [headerView addSubview:line];
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectIndexPath:indexPath];
}
#pragma mark ==========选择优惠券==========
-(void)selectIndexPath:(NSIndexPath *)indexPath{
    HBOrderPayStyleModel *model = self.payStyleArr[indexPath.row] ;
    //选中
    model.isSelect = YES ;
    //其它置为不选中
    for (int i=0; i<self.payStyleArr.count; i++) {
        if (i==indexPath.row) {
            continue ;
        }else{
            HBOrderPayStyleModel *tempModel = self.payStyleArr[i] ;
            tempModel.isSelect = NO ;
        }
    }
    self.selectIndex = indexPath.row;
    [self.tableView reloadData] ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickConfBtn:(id)sender {
    
    HBOrderPayStyleModel *model = self.payStyleArr[self.selectIndex];
    WEAKSELF;
    [self AlertWithTitle:@"确定要取消订单？" message:@"取消订单后订单不可恢复" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
            [LBService post:MY_ORDER_CANCEL_CREATE params:@{@"tid":self.tid,@"cancel_reason":model.name} completion:^(LBResponse *response) {
                [MBProgressHUD hideHUD];
                if (response.succeed) {
                    //成功 刷新界面
                    [app showToastView:@"取消成功"];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"HBCancelReasonSelectVC" object:nil];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                    
                }else{
                    [app showToastView:response.message];
                }
            }];
        }else{
            //取消
        }
    }];
}
@end
