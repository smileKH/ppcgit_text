//
//  HBCancelOrderContentCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBCancelDetailModel.h"
@interface HBCancelOrderContentCell : UITableViewCell
@property (nonatomic, strong) HBCancelDetailModel * model;
@property (weak, nonatomic) IBOutlet UILabel *orderLab;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UILabel *resonLab;

@end
