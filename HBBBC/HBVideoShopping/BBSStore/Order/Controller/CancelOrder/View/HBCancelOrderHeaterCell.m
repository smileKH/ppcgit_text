//
//  HBCancelOrderHeaterCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCancelOrderHeaterCell.h"

@implementation HBCancelOrderHeaterCell
- (void)setModel:(HBCancelDetailModel *)model{
    _model = model;
    NSString *createStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.created_time];
    NSString *modified_time = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.modified_time];
    self.createLab.text = [NSString stringWithFormat:@"申请时间:%@",createStr];
    self.statusLab.text = [NSString stringWithFormat:@"%@",model.status_desc];
    if ([model.refunds_status isEqualToString:@"SUCCESS"]) {
        self.statusDetailLab.text = [NSString stringWithFormat:@"完成时间:%@",modified_time];
    }else{
        self.statusDetailLab.text = [NSString stringWithFormat:@"完成时间:"];
    }
    
    self.detailLab.text = [NSString stringWithFormat:@"%@",model.status_detail];
    //计算进度
    if ([model.process isEqualToString:@"0"]) {
        //提交申请
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_nnn");
        self.threeImg.image = ImageNamed(@"bbs_nnn");
        self.fourImg.image = ImageNamed(@"bbs_nnn");
    }else if ([model.process isEqualToString:@"1"]){
        //等待寄回
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_nnn");
        self.fourImg.image = ImageNamed(@"bbs_nnn");
    }else if ([model.process isEqualToString:@"2"]){
        //已寄回
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_sss");
        self.fourImg.image = ImageNamed(@"bbs_nnn");
    }else if ([model.process isEqualToString:@"3"]){
        //商家驳回
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.hidden = YES;
        self.threeImg.hidden = YES;
        self.fourImg.image = ImageNamed(@"bbs_sss");
        self.twoLab.hidden = YES;
        self.threeLab.hidden = YES;

    }else if ([model.process isEqualToString:@"4"]){
        //退款
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_sss");
        self.fourImg.image = ImageNamed(@"bbs_nnn");

    }else if ([model.process isEqualToString:@"5"]){
        //完成
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_sss");
        self.fourImg.image = ImageNamed(@"bbs_sss");
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
