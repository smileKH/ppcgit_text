//
//  HBCancelOrderCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBCancelModel.h"
@interface HBCancelOrderCell : UITableViewCell
@property (nonatomic, strong) HBCancelModel * model;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;
@property (weak, nonatomic) IBOutlet UILabel *orderStateLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UILabel *totalLab;
@property (weak, nonatomic) IBOutlet UIButton *payStateBtn;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UIView *imgScroView;

@property (nonatomic, copy) void(^clickPayStateBtn)(HBCancelModel *model,NSString *btnStyle);
@end
