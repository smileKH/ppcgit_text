//
//  HBCancelOrderContentCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCancelOrderContentCell.h"

@implementation HBCancelOrderContentCell
- (void)setModel:(HBCancelDetailModel *)model{
    _model = model;
    self.orderLab.text = [NSString stringWithFormat:@"%@",model.tid];
    self.moneyLab.text = [NSString stringWithFormat:@"%@",model.payed_fee];
    self.resonLab.text = [NSString stringWithFormat:@"%@",model.reason];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
