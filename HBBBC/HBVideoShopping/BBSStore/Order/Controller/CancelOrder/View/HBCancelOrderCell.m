//
//  HBCancelOrderCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCancelOrderCell.h"

@implementation HBCancelOrderCell
- (void)setModel:(HBCancelModel *)model{
    _model = model;
    self.shopNameLab.text = model.shopname;
    self.orderStateLab.text = model.status_desc;
    self.numLab.text = [NSString stringWithFormat:@"共%@件",model.totalItem];
    self.totalLab.text = [NSString stringWithFormat:@"实付金额:%@",model.payed_fee];
    
    //判断，如果只有一件衣服，那么就展示一件
    if ([HBHuTool judgeArrayIsEmpty:model.order]) {
        //有
        if (model.order.count>1) {
            NSLog(@"我是等于二的");
        }
        UIScrollView *scroView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.imgScroView.width_sd, self.imgScroView.height_sd)];
        scroView.backgroundColor = [UIColor whiteColor];
        CGFloat imgWidth = 70;
        for (NSInteger i=0; i<model.order.count; i++) {
            UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(i*imgWidth+(i+1)*10, 0, imgWidth, imgWidth)];
            imageV.centerY_sd = scroView.centerY_sd;
            HBOrderModel *items = model.order[i];
            [imageV sd_setImageWithURL:[NSURL URLWithString:items.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
            
            [scroView addSubview:imageV];
        }
        scroView.contentSize = CGSizeMake(model.order.count*imgWidth+10, self.imgScroView.height_sd);
        [self.imgScroView addSubview:scroView];
    }
    
}

#pragma mark ==========点击按钮==========

- (IBAction)clickStateBtn:(UIButton *)sender {
    self.clickPayStateBtn(self.model, @"1");
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgScroView.userInteractionEnabled = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
