//
//  HBCancelModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/15.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBCancelOrderModel;
@interface HBCancelModel : NSObject
@property (nonatomic, strong) NSString * shopname;
@property (nonatomic, strong) NSString * payed_fee;
@property (nonatomic, strong) NSArray * order;
@property (nonatomic, strong) NSString * refunds_status;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) NSString * totalItem;
@property (nonatomic, strong) NSString * cancel_id;
@property (nonatomic, strong) NSString * shop_id;
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * process;
@end

@interface HBCancelOrderModel : NSObject
@property (nonatomic, strong) NSString * sku_id;
@property (nonatomic, strong) NSString * spec_nature_info;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * pic_path;
@property (nonatomic, strong) NSString * gift_data;
@property (nonatomic, strong) NSString * oid;
@property (nonatomic, strong) NSString * shop_id;
@property (nonatomic, strong) NSString * num;
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * item_id;
@end
