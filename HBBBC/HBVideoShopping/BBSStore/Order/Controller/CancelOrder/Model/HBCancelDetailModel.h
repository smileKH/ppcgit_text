//
//  HBCancelDetailModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Cur_symbol;
@interface HBCancelDetailModel : NSObject
@property (nonatomic, strong) NSString * shop_reject_reason;
@property (nonatomic, strong) NSString * cancel_id;
@property (nonatomic, strong) NSString * refunds_status;
@property (nonatomic, strong) NSNumber * created_time;
@property (nonatomic, strong) NSString * status_detail;
@property (nonatomic, strong) NSArray * log;
@property (nonatomic, strong) NSString * shop_id;

@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSNumber * modified_time;
@property (nonatomic, strong) NSString * pay_type;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) NSString * reason;
@property (nonatomic, strong) NSString * payed_fee;

@property (nonatomic, strong) NSString * cancel_from;
@property (nonatomic, strong) NSString * process;
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@end

@interface HBCancelDetailLogModel :NSObject
@property (nonatomic, strong) NSString * log_id;
@property (nonatomic, strong) NSString * rel_id;
@property (nonatomic, strong) NSString * op_id;

@property (nonatomic, strong) NSString * behavior;
@property (nonatomic, strong) NSString * log_time;
@property (nonatomic, strong) NSString * op_name;
@property (nonatomic, strong) NSString * log_text;
@property (nonatomic, strong) NSString * op_role;
@end
