//
//  MyOrderViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyOrderViewController.h"
#import "MyOrderCell.h"
#import "HBMyOrderDetailVC.h"
#import "HBMyOrderModel.h"
#import "PayViewController.h"
#import "HBOrderLogisticsVC.h"
#import "HBOrderEvaluationVC.h"
#import "HBCancelOrderDetailVC.h"
#import "HBCancelReasonSelectVC.h"
@interface MyOrderViewController ()<UITableViewDelegate,UITableViewDataSource>
//@property (nonatomic, strong) HBMyOrderModel * orderModel;
@property (nonatomic, strong) NSString * statusStr;
@end

@implementation MyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的订单";
    
    [self setupScroll];
    [self setupTableview];
    [self setNavBar];
    
    //去滚动条
    self.scroll.showsHorizontalScrollIndicator = NO;
    
    //设置一下初值
    [self setInitValueButton:self.selectTopIndex];
    
    //请求数据
    [self requestMyOrder:YES];
    WEAKSELF;
    //默认block方法：设置下拉刷新
    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestMyOrder:YES];
    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestMyOrder:NO];
//    }];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"MyOrderViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"HBOrderEvaluationVC" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"HBCancelReasonSelectVC" object:nil];
}
//更新数据
-(void)uplaodData{
    [self requestMyOrder:YES];
}
#pragma mark ==========请求我的订单==========
-(void)requestMyOrder:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    self.pageSize = 10;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    parameter[@"status"] = self.statusStr;
    WEAKSELF;
    [LBService post:MY_TRADE_LIST params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            HBMyOrderModel *model = [HBMyOrderModel mj_objectWithKeyValues:response.result[@"data"]];
                NSInteger number = model.pagers.page_count;
                NSArray *arr = model.list;
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [HBOrderList mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            

        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptImg:@"bbs_orderImg" withLabelName:@"您还没有相关的订单记录"];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    self.navigationItem.rightBarButtonItems = @[item1];
}


-(void)setupScroll
{
    NSArray *arrays = @[@"全部",@"待付款",@"待发货",@"待收货",@"待评价"];
    CGFloat spacing = 10;
    CGFloat buttonWid = (SCREEN_WIDTH-6*spacing)/5;
    for (NSInteger i = 0; i < arrays.count; i ++) {
        UIButton *buttons = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttons setTitle:arrays[i] forState:UIControlStateNormal];
        [buttons setTitleColor:MAINBLACKCOLOR forState:UIControlStateNormal];
        [buttons setTitleColor:MAINTextCOLOR forState:UIControlStateSelected];
        buttons.titleLabel.font = [UIFont systemFontOfSize:12];
        [buttons addTarget:self action:@selector(clcikMoreBtn:) forControlEvents:UIControlEventTouchUpInside];
        if (i == self.selectTopIndex) {
            buttons.selected = YES;
        }else{
            buttons.selected = NO;
        }
        buttons.tag = i;
        buttons.frame = CGRectMake( i * buttonWid+i*spacing+spacing, 0, buttonWid, 40);
        [self.scroll addSubview:buttons];
        
    }
    self.scroll.contentSize = CGSizeMake(SCREEN_WIDTH, 40);
}
#pragma mark ==========点击事件==========
-(void)clcikMoreBtn:(UIButton *)btn{
    for (UIView * aSubView in self.scroll.subviews) {
        if ([aSubView isKindOfClass:[UIButton class]]) {
            UIButton *aSubButton = (UIButton *)aSubView;
            aSubButton.selected = NO;
        }
    }
    btn.selected = YES;
    
    //设置一下
    [self setInitValueButton:btn.tag];
    //请求数据
    [self requestMyOrder:YES];
}
#pragma mark - 设置一下初值
-(void)setInitValueButton:(NSInteger )index{
    //点击按钮的时候设置
    switch (index) {
        case 0:
        {   //全部
            self.statusStr = @"";
        }
            break;
        case 1:
        {   //待付款
            self.statusStr = WAIT_BUYER_PAY;
        }
            break;
        case 2:
        {   //待发货
            self.statusStr = WAIT_SELLER_SEND_GOODS;
        }
            break;
        case 3:
        {   //待收货
            self.statusStr = WAIT_BUYER_CONFIRM_GOODS;
        }
            break;
        case 4:
        {   //待评价
            self.statusStr = WAIT_RATE;
        }
            break;
        case 5:
        {   //已取消
            self.statusStr = TRADE_FINISHED;
        }
            break;
            
        default:
            break;
    }
    
}
- (void)setupTableview
{
    [self.tableview registerNib:[UINib nibWithNibName:@"MyOrderCell" bundle:nil] forCellReuseIdentifier:@"cell"];
//    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1)];
//    headerView.backgroundColor = Color_BG;
//    self.tableview.tableHeaderView = headerView;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.allData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.allData[indexPath.section];
    WEAKSELF;
    cell.clickPayStateBtn = ^(HBOrderList *model, NSString *btnStyle) {
        [weakSelf clickStateBtnModel:model andType:btnStyle];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 250;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HBMyOrderDetailVC *vc= [[HBMyOrderDetailVC alloc] init];
    HBOrderList *model = self.allData[indexPath.section];
    vc.tidStr = model.tid;
    vc.controllerClass = @"MyOrderVC";
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ==========点击订单类型==========
-(void)clickStateBtnModel:(HBOrderList *)model andType:(NSString *)style{
    //设置状态按钮
    if ([style isEqualToString:STATUTS_PAY]) {
        //待付款 取消   去付款
        [self pushPayViewController:model];
        
    }else if ([style isEqualToString:STATUTS_CANCEL]){
        //待发货 取消
         [self cancelOrder:model];
        
    }
    else if ([style isEqualToString:STATUTS_LOGISTICS]){
        //待收货  查看物流
        //查看物流
        HBOrderLogisticsVC *vc = [[HBOrderLogisticsVC alloc]initWithNibName:@"HBOrderLogisticsVC" bundle:nil];
        vc.corp_code = model.delivery.corp_code;
        vc.logi_no = model.delivery.logi_no;
        vc.logi_name = model.delivery.logi_name;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else if ([style isEqualToString:STATUTS_CONFIRM]){
        //确认收货
        [self confirmGoods:model];
        
    }else if([style isEqualToString:STATUTS_EXALUATION]){
        //去评价
        HBOrderEvaluationVC *vc = [[HBOrderEvaluationVC alloc]initWithNibName:@"HBOrderEvaluationVC" bundle:nil];
        vc.tid = model.tid;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if([style isEqualToString:STATUTS_CHA_KAN_XIANGQ]){
        //查看取消详情
        HBCancelOrderDetailVC *vc = [[HBCancelOrderDetailVC alloc]init];
        vc.cancel_id = model.cancel_id;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        
    }
}
#pragma mark ==========跳到付款界面==========
-(void)pushPayViewController:(HBOrderList *)model{
    HBCashierDeskVC *vc = [[HBCashierDeskVC alloc]initWithNibName:@"HBCashierDeskVC" bundle:nil];
    vc.payment_id = @"";
    vc.payment_type = @"";
    vc.tid = model.tid;
    vc.payMoney = model.payment;
    vc.controllerClass = @"MyOrderViewController";
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========取消订单==========
-(void)cancelOrder:(HBOrderList *)model{
    //cancel_reason
    //
    HBCancelReasonSelectVC *vc = [[HBCancelReasonSelectVC alloc]init];
    vc.tid  = model.tid;
    [self.navigationController pushViewController:vc animated:YES];

}
#pragma mark ==========确认收货==========
-(void)confirmGoods:(HBOrderList *)model{
    WEAKSELF;
    [self AlertWithTitle:@"确认收货?" message:@"确认收货此订单将完成" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            [weakSelf confirmGoodsModel:model];
        }else{
            //取消
        }
    }];
    
}
#pragma mark ==========确认收货==========
-(void)confirmGoodsModel:(HBOrderList *)model{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MY_ORDER_CONFIRM params:@{@"tid":model.tid} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功 刷新界面
            [app showToastView:@"确认收货成功"];
            [weakSelf requestMyOrder:YES];
            
        }else{
            [app showToastView:response.message];
        }
    }];
}

#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
