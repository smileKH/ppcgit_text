//
//  HBOrderLogisticsModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/16.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBOrderLogisticsListModel;
@interface HBOrderLogisticsModel : NSObject
@property (nonatomic, strong) NSArray * tracker;
@end


@interface HBOrderLogisticsListModel : NSObject
@property (nonatomic, strong) NSString * AcceptTime;
@property (nonatomic, strong) NSString * AcceptStation;
@end
