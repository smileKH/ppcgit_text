//
//  HBOrderLogisticsModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/16.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderLogisticsModel.h"

@implementation HBOrderLogisticsModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"tracker":@"HBOrderLogisticsListModel"};
}
@end

@implementation HBOrderLogisticsListModel

@end
