//
//  HBMyOrderDetailModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/16.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBMyOrderDetailGoodsModel;
@class HBOrderLogiModel;
@class HBOrderCancelInfoModel;
@interface HBMyOrderDetailModel : NSObject
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * shipping_type;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * payment;
@property (nonatomic, strong) NSString * points_fee;
@property (nonatomic, strong) NSString * cancel_status;
@property (nonatomic, strong) NSString * hongbao_fee;
@property (nonatomic, strong) NSString * post_fee;
@property (nonatomic, strong) NSString * pay_type;
@property (nonatomic, strong) NSString * payed_fee;
@property (nonatomic, strong) NSNumber * pay_time;
@property (nonatomic, strong) NSString * receiver_state;
@property (nonatomic, strong) NSString * receiver_city;
@property (nonatomic, strong) NSString * receiver_district;
@property (nonatomic, strong) NSString * receiver_address;
@property (nonatomic, strong) NSString * trade_memo;
@property (nonatomic, strong) NSString * receiver_name;
@property (nonatomic, strong) NSString * receiver_mobile;
@property (nonatomic, strong) NSString * ziti_addr;
@property (nonatomic, strong) NSString * ziti_memo;
@property (nonatomic, strong) NSString * total_fee;
@property (nonatomic, strong) NSString * discount_fee;
@property (nonatomic, assign) NSInteger  buyer_rate;
@property (nonatomic, strong) NSString * adjust_fee;
@property (nonatomic, strong) NSNumber * created_time;
@property (nonatomic, strong) NSString * shop_id;
@property (nonatomic, strong) NSString * need_invoice;
@property (nonatomic, strong) NSString * invoice_name;
@property (nonatomic, strong) NSString * invoice_type;
@property (nonatomic, strong) NSString * invoice_main;
@property (nonatomic, strong) NSString * invoice_vat_main;
@property (nonatomic, strong) NSString * cancel_reason;
@property (nonatomic, strong) HBOrderCancelInfoModel * cancelInfo;
@property (nonatomic, strong) NSString * refund_fee;
@property (nonatomic, strong) NSArray * orders;
@property (nonatomic, strong) NSString * shipping_type_name;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) NSString * is_buyer_rate;
@property (nonatomic, strong) NSString * shopname;
@property (nonatomic, strong) HBOrderLogiModel * logi;
@property (nonatomic, strong) NSString * url;//视频
@property (nonatomic, strong) NSString * cancel_id;
@end


@interface HBMyOrderDetailGoodsModel : NSObject
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * aftersales_status;
@property (nonatomic, strong) NSString * aftersales_bn;
@property (nonatomic, assign) NSInteger  num;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * item_id;
@property (nonatomic, strong) NSString * cat_id;
@property (nonatomic, strong) NSString * end_time;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * pic_path;
@property (nonatomic, strong) NSString * total_fee;
@property (nonatomic, strong) NSString * adjust_fee;
@property (nonatomic, strong) NSString * spec_nature_info;
@property (nonatomic, strong) NSString * gift_data;
@property (nonatomic, strong) NSString * complaints_status;
@property (nonatomic, strong) NSString * buyer_rate;
@property (nonatomic, strong) NSString * oid;
@end

@interface HBOrderLogiModel :NSObject
@property (nonatomic, strong) NSString * logi_name;
@property (nonatomic, strong) NSString * logi_no;
@property (nonatomic, strong) NSString * corp_code;
@property (nonatomic, strong) NSString * delivery_id;
@property (nonatomic, strong) NSString * receiver_name;
@property (nonatomic, strong) NSString * oid;
@property (nonatomic, strong) NSString * t_begin;
@end

@interface HBOrderCancelInfoModel :NSObject
@property (nonatomic, strong) NSString * cancel_id;

@end

