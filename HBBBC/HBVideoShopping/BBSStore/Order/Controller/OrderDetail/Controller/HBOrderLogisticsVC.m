//
//  HBOrderLogisticsVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/27.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderLogisticsVC.h"
#import "HBLoisticsLineCell.h"
#import "LogIsticsCell.h"
#import "HBOrderLogisticsModel.h"
@interface HBOrderLogisticsVC ()
@property (nonatomic, strong) HBOrderLogisticsModel * logModel;
@end

@implementation HBOrderLogisticsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"物流信息";
    [self setupTableview];
    [self setNavBar];
    [self setUpadtValue];
   
    [self requestLogisticData:YES];
}
//设置初值
-(void)setUpadtValue{
    self.nameLab.text = [NSString stringWithFormat:@"承运公司:%@",self.logi_name];
    self.numberLab.text = [NSString stringWithFormat:@"运单编号:%@",self.logi_no];
}
#pragma mark ==========请求物流信息==========
-(void)requestLogisticData:(BOOL)isRefresh{
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"logi_no"] = self.logi_no;
    parameters[@"corp_code"] = self.corp_code;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:LOGISTICS_GET params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.logModel = [HBOrderLogisticsModel mj_objectWithKeyValues:response.result[@"data"]];
            [weakSelf.tableview reloadData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}

- (void)setupTableview
{
    [self.tableview registerNib:[UINib nibWithNibName:@"HBLoisticsLineCell" bundle:nil] forCellReuseIdentifier:@"HBLoisticsLineCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"LogIsticsCell" bundle:nil] forCellReuseIdentifier:@"LogIsticsCell"];
    self.tableview.tableFooterView = [[UIView alloc]init];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==self.logModel.tracker.count-1) {
        return 1;
    }else{
        return 2;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.logModel.tracker.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        LogIsticsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LogIsticsCell"];
        cell.model = self.logModel.tracker[indexPath.section];
        return cell;
    }else{
        HBLoisticsLineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBLoisticsLineCell"];
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return 83;
    }else{
        return 33;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
