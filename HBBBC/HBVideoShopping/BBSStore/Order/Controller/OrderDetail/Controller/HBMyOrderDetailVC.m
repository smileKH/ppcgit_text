//
//  HBMyOrderDetailVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/16.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBMyOrderDetailVC.h"
#import "OrderCell.h"
#import "OrderAddressCell.h"
#import "OrderLogisticsCell.h"
//#import "OrderGoodsCell.h"
#import "OrderOtherCell.h"
#import "OrderBillCell.h"
#import "HBMyOrderDetailModel.h"

#import "HBOrderGoodsMoneyDetailCell.h"
#import "HBOrderGoodsHeaderCell.h"
#import "HBOrderGoodsCell.h"
#import "HBOrderLogisticsVC.h"

#import "MyReutrnGoodsViewController.h"
#import "ReturnGoodsDetailViewController.h"
#import "ApplyBackViewController.h"
#import "HBMyOrderDetailSeeVidoVC.h"
#import "HBOrderEvaluationVC.h"
#import "HBCancelOrderVC.h"
#import "AddLogViewController.h"
#import "MyComplainDetailViewController.h"
#import "AddComplainViewController.h"
#import "HBCancelOrderDetailVC.h"
#import "HBCancelReasonSelectVC.h"
static const CGFloat bottomAllHeight = 45;
@interface HBMyOrderDetailVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) HBMyOrderDetailModel * detailModel;
@property (nonatomic , strong)UIView * bottomView;
@property (nonatomic , strong)NSArray * stateArray;
@end

@implementation HBMyOrderDetailVC
- (NSArray *)stateArray{
    if (!_stateArray) {
        _stateArray = [NSArray array];
    }
    return _stateArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //订单详情
    self.title = @"订单详情";
    //子视图
    [self setupUI];
    //请求订单详情
    [self requestOrderDetail];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"ApplyBackViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"LogDetailViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"AddComplainViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"MyOrderViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"HBOrderEvaluationVC" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"HBCancelReasonSelectVC" object:nil];
}
//更新
-(void)uplaodData{
    //请求订单详情
    [self requestOrderDetail];
}
#pragma mark ==========添加子视图==========
-(void)setupUI{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
//    [self.view addSubview:self.tableView];
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = Color_BG;
    
    [self.view sd_addSubviews:@[self.tableView,self.bottomView]];
    
    CGFloat tableHeight = SCREEN_HEIGHT-bNavAllHeight-bottomAllHeight;
    
    self.tableView.sd_layout
    .topSpaceToView(self.view,0)
    .leftSpaceToView(self.view,0)
    .widthIs(SCREEN_WIDTH)
    .heightIs(tableHeight);
    
    self.bottomView.sd_layout
    .topSpaceToView(self.tableView,0)
    .leftSpaceToView(self.view,0)
    .widthIs(SCREEN_WIDTH)
    .heightIs(bottomAllHeight);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderCell" bundle:nil] forCellReuseIdentifier:@"order"];
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderAddressCell" bundle:nil] forCellReuseIdentifier:@"address"];
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderLogisticsCell" bundle:nil] forCellReuseIdentifier:@"log"];
    //[self.tableView registerNib:[UINib nibWithNibName:@"OrderGoodsCell" bundle:nil] forCellReuseIdentifier:@"goods"];
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderOtherCell" bundle:nil] forCellReuseIdentifier:@"other"];
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderBillCell" bundle:nil] forCellReuseIdentifier:@"bill"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderGoodsMoneyDetailCell" bundle:nil] forCellReuseIdentifier:@"money"];
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderGoodsHeaderCell" bundle:nil] forCellReuseIdentifier:@"header"];
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderGoodsCell" bundle:nil] forCellReuseIdentifier:@"goods"];
    self.tableView.estimatedRowHeight = 100;
    
    
}
#pragma mark ==========请求订单详情==========
-(void)requestOrderDetail{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MY_ORDER_TRADE params:@{@"tid":self.tidStr} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.detailModel = [HBMyOrderDetailModel mj_objectWithKeyValues:response.result[@"data"]];
//          weakSelf.detailModel.url =   @"http://120.79.62.192/images/73/13/bc/88a966bbeb5e06df70fef2775603e540a2796416.mp4";
            [weakSelf.tableView reloadData];
            //设置bottomView
            [weakSelf setBottomViewData];
        }else{
            
        }
    }];
}
//设置底部视图
-(void)setBottomViewData{
    [self.bottomView removeAllSubviews];
    NSArray *array = [self returnSetModelState:self.detailModel];
    if ([HBHuTool judgeArrayIsEmpty:array]) {
        [self setStateViewSubView:array];
    }else{
        self.tableView.sd_layout
        .heightIs(SCREEN_HEIGHT-bNavAllHeight);
        self.bottomView.hidden = YES;
    }
}
#pragma mark = 设置状态
-(void)setStateViewSubView:(NSArray *)array{
    //添加分类
    CGFloat buttonWidth = 65;
    CGFloat buttonHight = 25;
    CGFloat buttonSping = 10;
    CGFloat topSpacing = (40-25)/2;
    for (int i =0; i<array.count; i++) {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.frame = CGRectMake(SCREEN_WIDTH-(buttonWidth+buttonSping)*(i+1), topSpacing, buttonWidth, buttonHight);
        [aButton setTitle:array[i] forState:UIControlStateNormal];
        [aButton setTitleColor:white_Color forState:UIControlStateNormal];
        aButton.layer.borderWidth = 0.5;
        aButton.layer.borderColor = MAINTextCOLOR.CGColor;
        aButton.layer.cornerRadius = 3;
        aButton.titleLabel.font = textFont_Content14;
        aButton.tag = 100+i;
        aButton.backgroundColor = MAINTextCOLOR;
        [aButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:aButton];
    }
}
-(NSArray *)returnSetModelState:(HBMyOrderDetailModel *)model{
    NSArray *array = [NSArray array];
    //设置状态按钮
    if ([model.status isEqualToString:WAIT_BUYER_PAY]) {
        //待付款 取消   去付款
        array = @[STATUTS_PAY,STATUTS_CANCEL];
        
    }else if ([model.status isEqualToString:WAIT_SELLER_SEND_GOODS]){
        //待发货 取消
        //待发货 取消  判断取消状态
        if ([model.cancel_status isEqualToString:CANCEL_NO_APPLY_CANCEL]) {
            //未申请
            array = @[STATUTS_CANCEL];
        }else if ([model.cancel_status isEqualToString:CANCEL_WAIT_PROCESS]){
            //等待申请
            array = @[STATUTS_CHA_KAN_XIANGQ];
        }else if ([model.cancel_status isEqualToString:CANCEL_REFUND_PROCESS]){
            //取消申请
            array = @[STATUTS_CHA_KAN_XIANGQ];
        }else if ([model.cancel_status isEqualToString:CANCEL_SUCCESS]){
            //取消成功
            array = @[];
        }else if ([model.cancel_status isEqualToString:CANCEL_FAILS]){
            //取消失败
            array = @[];
        }else{
            array = @[];
        }
        
    }
    else if ([model.status isEqualToString:WAIT_BUYER_CONFIRM_GOODS]){
        //待收货  查看物流
        if ([model.pay_type isEqualToString:@"offline"]) {//线下
            array = @[STATUTS_LOGISTICS];
        }else{//线上
            array = @[STATUTS_LOGISTICS,STATUTS_CONFIRM];
        }
        
    }
    else if ([model.status isEqualToString:TRADE_FINISHED]){
        //已完成 评价
        if (model.buyer_rate) {
            array = @[];
        }else{
            array = @[STATUTS_EXALUATION];
        }
  
    }else if([model.status isEqualToString:TRADE_CLOSED_BY_SYSTEM]){
        //已关闭
        //[self.payStateBtn setTitle:@"去购买" forState:UIControlStateNormal];
        return @[];
    }else{
        return @[];
    }
    return array;
}
-(void)clickButton:(UIButton *)button{
    [self clickStateBtnModel:self.detailModel andType:button.titleLabel.text];
}
#pragma mark ==========点击订单类型==========
-(void)clickStateBtnModel:(HBMyOrderDetailModel *)model andType:(NSString *)style{
    //设置状态按钮
    if ([style isEqualToString:STATUTS_PAY]) {
        //待付款 取消   去付款
        [self pushPayViewController:model];
        
    }else if ([style isEqualToString:STATUTS_CANCEL]){
        //待发货 取消
        [self cancelOrder:model];
        
    }
    else if ([style isEqualToString:STATUTS_LOGISTICS]){
        //待收货  查看物流
        //查看物流
        HBOrderLogisticsVC *vc = [[HBOrderLogisticsVC alloc]initWithNibName:@"HBOrderLogisticsVC" bundle:nil];
        vc.corp_code = model.logi.corp_code;
        vc.logi_no = model.logi.logi_no;
        vc.logi_name = model.logi.logi_name;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else if ([style isEqualToString:STATUTS_CONFIRM]){
        //确认收货
        [self confirmGoods:model];
        
    }else if([style isEqualToString:STATUTS_EXALUATION]){
        //去评价
        HBOrderEvaluationVC *vc = [[HBOrderEvaluationVC alloc]initWithNibName:@"HBOrderEvaluationVC" bundle:nil];
        vc.tid = model.tid;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if([style isEqualToString:STATUTS_CHA_KAN_XIANGQ]){
        //查看取消详情
        HBCancelOrderDetailVC *vc = [[HBCancelOrderDetailVC alloc]init];
        vc.cancel_id = model.cancelInfo.cancel_id;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        
    }
}
#pragma mark ==========跳到付款界面==========
-(void)pushPayViewController:(HBMyOrderDetailModel *)model{
    HBCashierDeskVC *vc = [[HBCashierDeskVC alloc]initWithNibName:@"HBCashierDeskVC" bundle:nil];
    vc.payment_id = @"";
    vc.payment_type = @"";
    vc.tid = model.tid;
    vc.payMoney = model.payment;
    vc.controllerClass = @"MyOrderViewController";
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========取消订单==========
-(void)cancelOrder:(HBMyOrderDetailModel *)model{
    //
    HBCancelReasonSelectVC *vc = [[HBCancelReasonSelectVC alloc]init];
    vc.tid  = model.tid;
    [self.navigationController pushViewController:vc animated:YES];
//    WEAKSELF;
//    [self AlertWithTitle:@"确定要取消订单？" message:@"取消订单后订单不可恢复" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
//        if (index==0) {
//            //确定
//            [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
//            [LBService post:MY_ORDER_CANCEL_CREATE params:@{@"tid":model.tid,@"cancel_reason":@"下错单了"} completion:^(LBResponse *response) {
//                [MBProgressHUD hideHUD];
//                if (response.succeed) {
//                    //成功 刷新界面
//                    [app showToastView:@"取消成功"];
//                    [weakSelf requestOrderDetail];
//
//                }else{
//                    [app showToastView:response.message];
//                }
//            }];
//        }else{
//            //取消
//        }
//    }];
}
#pragma mark ==========确认收货==========
-(void)confirmGoods:(HBMyOrderDetailModel *)model{
    WEAKSELF;
    [self AlertWithTitle:@"确认收货?" message:@"确认收货此订单将完成" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            [weakSelf confirmGoodsModel:model];
        }else{
            //取消
        }
    }];
    
}
#pragma mark ==========确认收货==========
-(void)confirmGoodsModel:(HBMyOrderDetailModel *)model{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MY_ORDER_CONFIRM params:@{@"tid":model.tid} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功 刷新界面
            [app showToastView:@"确认收货成功"];
            [weakSelf requestOrderDetail];
            
        }else{
            [app showToastView:response.message];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section==3) {
        return self.detailModel.orders.count+2;
    }else if (section==2){
        if ([self.detailModel.status isEqualToString:WAIT_BUYER_CONFIRM_GOODS]) {
            return 1;
        }else{
            return 0;
        }
    }else{
      return 1;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WEAKSELF;
    if (indexPath.section == 0) {
        OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"order"];
        cell.model = self.detailModel;
        return cell;
    }else if (indexPath.section == 1) {
        OrderAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"address"];
        cell.model = self.detailModel;
        return cell;
    }else if (indexPath.section == 2) {
        OrderLogisticsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"log"];
        cell.model = self.detailModel;
        return cell;
    }else if (indexPath.section == 3) {
        NSInteger count = self.detailModel.orders.count+2;
        if (indexPath.row==0) {
            HBOrderGoodsHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"header"];
            cell.model = self.detailModel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else if (indexPath.row==count-1){
            HBOrderGoodsMoneyDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"money"];
            cell.model = self.detailModel;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else{
            HBOrderGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goods"];
            cell.model = self.detailModel.orders[indexPath.row-1];
            cell.clickOrderGoodsStateBtn = ^(HBMyOrderDetailGoodsModel *model, NSString *btnStyle) {
                [weakSelf selectModel:model andButtonText:btnStyle];
            };
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
        
    }else{
        OrderOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"other"];
        cell.model = self.detailModel;
        cell.clcikShiPinBtn = ^(id obj) {
            //查看视频 判断
            if ([HBHuTool isJudgeString:self.detailModel.url]) {
                //
                [app showToastView:@"暂无视频"];
            }else{
                HBMyOrderDetailSeeVidoVC *video = [[HBMyOrderDetailSeeVidoVC alloc] init];
                video.vodeourl = weakSelf.detailModel.url;
                [weakSelf.navigationController pushViewController:video animated:YES];
            }
        };
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger count = self.detailModel.orders.count+2;
    if (indexPath.section==3) {
        if (indexPath.row==0||indexPath.row==count-1) {
            return UITableViewAutomaticDimension;
        }else{
           return 90;
        }
    }else{
      return UITableViewAutomaticDimension;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5)];
    footer.backgroundColor = MAINBGCOLOR;
    
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 5) {
        return 0.01;
    }
    return 5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==2){
        //查看物流
        HBOrderLogisticsVC *vc = [[HBOrderLogisticsVC alloc]initWithNibName:@"HBOrderLogisticsVC" bundle:nil];
        vc.corp_code = self.detailModel.logi.corp_code;
        vc.logi_no = self.detailModel.logi.logi_no;
        vc.logi_name = self.detailModel.logi.logi_name;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 3) {
        NSInteger count = self.detailModel.orders.count+2;
        if (indexPath.row==0) {
            //点击头部视图 跳到店铺
            ShopViewController *vc = [[ShopViewController alloc]initWithNibName:@"ShopViewController" bundle:nil];
            vc.shop_id = self.detailModel.shop_id;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else if (indexPath.row==count-1){
            //点击订单money展示 不跳
            
        }else{
            //点击每件商品 跳到商品详情
            HBMyOrderDetailGoodsModel *model = self.detailModel.orders[indexPath.row-1];
            ProductFirstViewController *vc= [[ProductFirstViewController alloc] initWithNibName:@"ProductFirstViewController" bundle:nil];
            vc.item_id = model.item_id;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        
    }
}

#pragma mark ==========处理申请退货按钮==========
-(void)selectModel:(HBMyOrderDetailGoodsModel *)model andButtonText:(NSString *)buttonText{
 
    if ([buttonText isEqualToString:AF_TUI_HUAN_HUO]) {
        //申请退换货 申请
        //不在售后范围内，那么可以申请售后
        ApplyBackViewController *vc = [[ApplyBackViewController alloc]initWithNibName:@"ApplyBackViewController" bundle:nil];
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
    }else if([buttonText isEqualToString:AF_SHOU_CHU_LI]){
        //售后处理中 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_QING_TUI_HUO]){
        //已同意,请退货 待寄回
        AddLogViewController *vc = [[AddLogViewController alloc]initWithNibName:@"AddLogViewController" bundle:nil];
        vc.aftersales_bn = model.aftersales_bn;
        vc.controllerClass = @"HBMyOrderDetailVC";
        [self.navigationController pushViewController:vc animated:YES];
    }else if([buttonText isEqualToString:AF_QUE_REN_SHOU_HUO]){
        //待卖家确认收货 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_TUI_KUAN_ZHON]){
        //退款中 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_TUI_KUAN_WAN_CHENG]){
        //退款完成 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_SHOU_HOU_BO_HUI]){
        //售后驳回 退换货详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_TOU_SU_SHANG_JIA]){
        //投诉商家 去投诉
        AddComplainViewController *vc = [[AddComplainViewController alloc]initWithNibName:@"AddComplainViewController" bundle:nil];
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if([buttonText isEqualToString:AF_TOU_SU_CHU_LI_ZHONG]){
        //投诉处理中 投诉详情
        [self pushComplainDetailDetail:model];
    }else if([buttonText isEqualToString:AF_ZAI_CI_SHEN_QING]){
        //再次申请退换货
        ApplyBackViewController *vc = [[ApplyBackViewController alloc]initWithNibName:@"ApplyBackViewController" bundle:nil];
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
    }else if([buttonText isEqualToString:AF_TOU_SU_CEH_XIAO]){
        //投诉已撤销
        [self pushComplainDetailDetail:model];
    }else if([buttonText isEqualToString:AF_TOU_SU_GUAN_BI]){
        //投诉已关闭
        [self pushComplainDetailDetail:model];
    }
}
#pragma mark ==========跳到投诉详情==========
-(void)pushComplainDetailDetail:(HBMyOrderDetailGoodsModel *)model{
    MyComplainDetailViewController *vc = [[MyComplainDetailViewController alloc]initWithNibName:@"MyComplainDetailViewController" bundle:nil];
    vc.oid = model.oid;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========跳到退货详情==========
-(void)pushReturnGoodsDetail:(HBMyOrderDetailGoodsModel *)model{
    ReturnGoodsDetailViewController *vc= [[ReturnGoodsDetailViewController alloc] initWithNibName:@"ReturnGoodsDetailViewController" bundle:nil];
    vc.aftersales_bn = model.aftersales_bn;
    vc.oid = model.oid;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 返回按钮 很重要
- (void)backBtnClicked{
    //返回类型 1、返回购物车 2、返回商品详情  3、返回我的订单
    id vc = nil;
    if ([self.controllerClass isEqualToString:@"ShoppingCarViewController"]) {
        vc = [ShoppingCarViewController class];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"ShoppingCarViewController" object:nil];
    }
    else if([self.controllerClass isEqualToString:@"ProductFirstViewController"]){
        vc = [ProductFirstViewController class];
        //给商品详情发一个通知
        [[NSNotificationCenter defaultCenter]postNotificationName:@"ProductFirstViewController" object:nil];
    }else if([self.controllerClass isEqualToString:@"MyOrderViewController"]){
        vc = [MyOrderViewController class];
        //给我的订单发一个通知
        [[NSNotificationCenter defaultCenter]postNotificationName:@"MyOrderViewController" object:nil];
    }else if([self.controllerClass isEqualToString:@"HBCancelOrderVC"]){
        vc = [HBCancelOrderVC class];
    }
    else if([self.controllerClass isEqualToString:@"MyOrderVC"]){
        vc = [MyOrderViewController class];//这是直接从我的订单过来的
    }
    else{
        return;
    }
    NSArray *temArray = self.navigationController.viewControllers;
    for(UIViewController *temVC in temArray){
        if ([temVC isKindOfClass:vc]){
            [self.navigationController popToViewController:temVC animated:YES];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
