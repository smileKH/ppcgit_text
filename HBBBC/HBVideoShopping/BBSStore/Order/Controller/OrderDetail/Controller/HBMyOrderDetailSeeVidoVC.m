//
//  HBMyOrderDetailSeeVidoVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/5/10.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBMyOrderDetailSeeVidoVC.h"
#import "VideoViewController.h"
#import "ZFPlayer.h"
@interface HBMyOrderDetailSeeVidoVC ()
@property (nonatomic, strong) VideoViewController *videoVC;
@end

@implementation HBMyOrderDetailSeeVidoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"拣货视频";
    NSLog(@"打印链接:%@",self.vodeourl);
    self.view.backgroundColor = [UIColor whiteColor];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_videoVC dissMissVideo];
    _videoVC = nil;
}

-(void)showVideoVC{
    
    _videoVC =[[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
    _videoVC.view.frame = CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT);
    _videoVC.placeholderImage = [UIImage imageNamed:ZAN_WU_TUPIAN];
    _videoVC.vodeourl = [self.vodeourl URLEncodedString];
    WEAKSELF;
    _videoVC.videoDissMissBlock = ^(BOOL isDissmiss) {
        STRONGSELF;
        if (isDissmiss) {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    };
    [self.view.window addSubview:_videoVC.view];
    [self addChildViewController:_videoVC];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showVideoVC];
}


@end
