//
//  HBOrderLogisticsVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/27.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import "HBMyOrderDetailModel.h"
@interface HBOrderLogisticsVC : HBRootViewController
@property (nonatomic, strong) NSString * logi_no;
@property (nonatomic, strong) NSString * corp_code;
@property (nonatomic, strong) NSString * logi_name;
@property (weak, nonatomic) IBOutlet UILabel *stateLab;

@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
