//
//  OrderOtherCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderOtherCell.h"

@implementation OrderOtherCell
-(void)setModel:(HBMyOrderDetailModel *)model{
    _model = model;
    NSString *str = nil;
    if ([model.pay_type isEqualToString:@"online"]) {
        str = @"线上支付";
    }else{
        str = @"货到付款";
    }
    self.payStyleLab.text = [NSString stringWithFormat:@"%@",str];
    self.logisticsLab.text = [NSString stringWithFormat:@"%@",model.shipping_type_name];
    self.noteLab.text = [NSString stringWithFormat:@"%@",@"无"];
    if ([HBHuTool isJudgeString:model.url]) {
        self.payVideoLab.text = @"暂无视频";
    }else{
        self.payVideoLab.text = [NSString stringWithFormat:@"%@",@"查看视频"];
    }
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickShiPinBtn:(id)sender {
    self.clcikShiPinBtn(nil);
}
@end
