//
//  HBOrderGoodsHeaderCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/19.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBMyOrderDetailModel.h"
@interface HBOrderGoodsHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;//店铺名称
@property (nonatomic, strong) HBMyOrderDetailModel * model;
@end
