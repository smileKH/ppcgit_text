//
//  LogIsticsCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "LogIsticsCell.h"

@implementation LogIsticsCell
-(void)setModel:(HBOrderLogisticsListModel *)model{
    _model = model;
    _addressLab.text = [NSString stringWithFormat:@"%@",model.AcceptStation];
    NSString *dateStr = [XH_Date_String endStringFormat:@"HH:mm" andStateStringFormat:@"yyyy-MM-dd HH:mm:ss" andValueStr:model.AcceptTime];
    
    NSString *detailStr = [XH_Date_String endStringFormat:@"yyyy-MM-dd" andStateStringFormat:@"yyyy-MM-dd HH:mm:ss" andValueStr:model.AcceptTime];
    _dateLab.text = [NSString stringWithFormat:@"%@",dateStr];
    _dateDetailLab.text = [NSString stringWithFormat:@"%@",detailStr];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
