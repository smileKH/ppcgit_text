//
//  HBOrderGoodsMoneyDetailCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/19.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderGoodsMoneyDetailCell.h"

@implementation HBOrderGoodsMoneyDetailCell
-(void)setModel:(HBMyOrderDetailModel *)model{
    _model = model;
    self.goodsTotalMoneyLab.text = [NSString stringWithFormat:@"%.02f",[model.total_fee   doubleValue]];//总金额
    self.jianMianLab.text = [NSString stringWithFormat:@"-%.02f",[model.discount_fee doubleValue]];//减免
    self.integralLab.text = [NSString stringWithFormat:@"%.02f",[model.points_fee doubleValue]];//积分抵扣
    self.yunFeiLab.text = [NSString stringWithFormat:@"%.02f",[model.post_fee doubleValue]];//运费
    self.hongBaoLab.text = [NSString stringWithFormat:@"%.02f",[model.hongbao_fee doubleValue]];//红包
    self.shiFuLab.text = [NSString stringWithFormat:@"%.02f",[model.payment doubleValue]];//实付款
    self.shiFuLab.textColor = MAINREDCOLOR;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
