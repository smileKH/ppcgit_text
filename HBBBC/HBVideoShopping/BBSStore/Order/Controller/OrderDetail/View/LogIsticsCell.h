//
//  LogIsticsCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBOrderLogisticsModel.h"
@interface LogIsticsCell : UITableViewCell
@property (nonatomic, strong) HBOrderLogisticsListModel * model;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *dateDetailLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;

@end
