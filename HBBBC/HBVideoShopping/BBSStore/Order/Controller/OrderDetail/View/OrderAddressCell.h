//
//  OrderAddressCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (nonatomic, strong) HBMyOrderDetailModel * model;
@end
