//
//  OrderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderCell.h"

@implementation OrderCell
- (void)setModel:(HBMyOrderDetailModel *)model{
    _model = model;
    self.stateLab.text  = [NSString stringWithFormat:@"%@",model.status_desc];
    self.orderNubLab.text = [NSString stringWithFormat:@"订单号：%@",model.tid];
    NSString *createTime = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.created_time];
    self.createDateLab.text = [NSString stringWithFormat:@"创建时间:%@",createTime];
    NSString *payTime = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.pay_time];
    self.payDateLab.text = [NSString stringWithFormat:@"支付时间:%@",payTime];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
