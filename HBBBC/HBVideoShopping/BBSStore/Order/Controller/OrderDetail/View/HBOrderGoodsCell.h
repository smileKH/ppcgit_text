//
//  HBOrderGoodsCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/19.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBMyOrderDetailModel.h"
@interface HBOrderGoodsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgView;//商品图片
@property (weak, nonatomic) IBOutlet UILabel *goodsContentLab;//商品介绍
@property (weak, nonatomic) IBOutlet UILabel *goodsNubLab;//商品数量
@property (weak, nonatomic) IBOutlet UILabel *goodsMoneyLab;//商品金额
@property (weak, nonatomic) IBOutlet UIButton *reqBtn;
@property (weak, nonatomic) IBOutlet UIView *moreBtnView;

@property (nonatomic, strong) HBMyOrderDetailGoodsModel * model;
- (IBAction)clickBtn:(UIButton *)sender;

@property (nonatomic, copy) void(^clickBtnReturnGoods)(HBMyOrderDetailGoodsModel *goodsModel);
@property (nonatomic, copy) void(^clickOrderGoodsStateBtn)(HBMyOrderDetailGoodsModel *model,NSString *btnStyle);
@end
