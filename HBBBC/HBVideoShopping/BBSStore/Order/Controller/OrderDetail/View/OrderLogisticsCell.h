//
//  OrderLogisticsCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderLogisticsCell : UITableViewCell
@property (nonatomic, strong) HBMyOrderDetailModel * model;
@end
