//
//  HBOrderGoodsHeaderCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/19.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderGoodsHeaderCell.h"

@implementation HBOrderGoodsHeaderCell
-(void)setModel:(HBMyOrderDetailModel *)model{
    _model = model;
    self.shopNameLab.text = [NSString stringWithFormat:@"%@",model.shopname];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
