//
//  HBOrderGoodsMoneyDetailCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/19.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBMyOrderDetailModel.h"
@interface HBOrderGoodsMoneyDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *goodsTotalMoneyLab;//商品总额
@property (weak, nonatomic) IBOutlet UILabel *jianMianLab;//减免
@property (weak, nonatomic) IBOutlet UILabel *integralLab;//积分
@property (weak, nonatomic) IBOutlet UILabel *yunFeiLab;//运费
@property (weak, nonatomic) IBOutlet UILabel *hongBaoLab;//红包抵扣
@property (weak, nonatomic) IBOutlet UILabel *shiFuLab;//实付金额
@property (nonatomic, strong) HBMyOrderDetailModel * model;

@end
