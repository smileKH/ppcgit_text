//
//  OrderAddressCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderAddressCell.h"

@implementation OrderAddressCell

- (void)setModel:(HBMyOrderDetailModel *)model{
    _model = model;
    self.nameLab.text = [NSString stringWithFormat:@"收货人:%@",model.receiver_name];
    self.phoneLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.addressLab.text = [NSString stringWithFormat:@"%@%@%@",model.receiver_state,model.receiver_city,model.receiver_address];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
