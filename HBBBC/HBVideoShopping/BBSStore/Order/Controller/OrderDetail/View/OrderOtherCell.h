//
//  OrderOtherCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderOtherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *payStyleLab;
@property (weak, nonatomic) IBOutlet UILabel *logisticsLab;
@property (weak, nonatomic) IBOutlet UILabel *noteLab;
@property (weak, nonatomic) IBOutlet UILabel *payVideoLab;
@property (nonatomic, strong) HBMyOrderDetailModel * model;
- (IBAction)clickShiPinBtn:(id)sender;

@property (nonatomic, copy) void(^clcikShiPinBtn)(id obj);
@end
