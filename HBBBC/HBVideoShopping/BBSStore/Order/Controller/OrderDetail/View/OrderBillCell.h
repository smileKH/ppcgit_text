//
//  OrderBillCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderBillCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLab;
@property (weak, nonatomic) IBOutlet UILabel *companyNumLab;
@property (weak, nonatomic) IBOutlet UILabel *companyAddressLab;
@property (weak, nonatomic) IBOutlet UILabel *companyPhoneLab;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLab;
@property (weak, nonatomic) IBOutlet UILabel *bankNumLab;
@property (nonatomic, strong) HBMyOrderDetailModel * model;
@end
