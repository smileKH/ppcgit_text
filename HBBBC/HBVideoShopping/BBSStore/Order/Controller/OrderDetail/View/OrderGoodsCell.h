//
//  OrderGoodsCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderGoodsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *applyBackBtn;

- (IBAction)applyBackAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;//店铺名称
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgView;//商品图片
@property (weak, nonatomic) IBOutlet UILabel *goodsContentLab;//商品介绍
@property (weak, nonatomic) IBOutlet UILabel *goodsNubLab;//商品数量
@property (weak, nonatomic) IBOutlet UILabel *goodsMoneyLab;//商品金额
@property (weak, nonatomic) IBOutlet UILabel *goodsTotalMoneyLab;//商品总额
@property (weak, nonatomic) IBOutlet UILabel *jianMianLab;//减免
@property (weak, nonatomic) IBOutlet UILabel *integralLab;//积分
@property (weak, nonatomic) IBOutlet UILabel *yunFeiLab;//运费
@property (weak, nonatomic) IBOutlet UILabel *hongBaoLab;//红包抵扣
@property (weak, nonatomic) IBOutlet UILabel *shiFuLab;//实付金额
@property (nonatomic, strong) HBMyOrderDetailModel * model;
@end
