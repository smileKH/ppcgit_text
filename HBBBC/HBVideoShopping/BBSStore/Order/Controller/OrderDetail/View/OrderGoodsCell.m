//
//  OrderGoodsCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderGoodsCell.h"

@implementation OrderGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.applyBackBtn.layer.cornerRadius = 10;
    self.applyBackBtn.layer.masksToBounds = YES;
    self.applyBackBtn.layer.borderColor = MAINTextCOLOR.CGColor;
    self.applyBackBtn.layer.borderWidth = 1;
}

-(void)setModel:(HBMyOrderDetailModel *)model{
    _model = model;
    self.shopNameLab.text = [NSString stringWithFormat:@"%@",model.shopname];
   // self.goodsImgView.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.goodsContentLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.goodsNubLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.goodsMoneyLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.goodsTotalMoneyLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.jianMianLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.integralLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.yunFeiLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.hongBaoLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
    self.shiFuLab.text = [NSString stringWithFormat:@"%@",model.receiver_mobile];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)applyBackAction:(UIButton *)sender {
}
@end
