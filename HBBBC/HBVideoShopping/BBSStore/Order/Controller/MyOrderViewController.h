//
//  MyOrderViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MyOrderViewController : HBRootViewController
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (nonatomic, assign) NSInteger selectTopIndex;//默认 0
@end
