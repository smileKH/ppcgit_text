//
//  MyOrderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyOrderCell.h"
@interface MyOrderCell ()

@property (nonatomic , strong)NSArray * stateArray;
@end
@implementation MyOrderCell
- (NSArray *)stateArray{
    if (!_stateArray) {
        _stateArray = @[@"再次购买",@"去评价",@"查看物流",@"确认收货"];
    }
    return _stateArray;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgScroView.userInteractionEnabled = NO;
}
- (void)setModel:(HBOrderList *)model{
    _model = model;
    self.shopNameLab.text = model.shopname;
    self.orderStateLab.text = model.status_desc;
    self.numLab.text = [NSString stringWithFormat:@"共%@件",model.totalItem];
    self.totalLab.attributedText = [HBHuTool attributedOldString:@"实付金额:" priceString:model.payment];
    
    
    //判断，如果只有一件衣服，那么就展示一件
    if ([HBHuTool judgeArrayIsEmpty:model.order]) {
        //有
        UIScrollView *scroView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.imgScroView.width_sd, self.imgScroView.height_sd)];
        scroView.backgroundColor = [UIColor whiteColor];
        CGFloat imgWidth = 70;
        for (NSInteger i=0; i<model.order.count; i++) {
            UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(i*imgWidth+(i+1)*10, 0, imgWidth, imgWidth)];
            imageV.centerY_sd = scroView.centerY_sd;
            HBOrderModel *items = model.order[i];
            [imageV sd_setImageWithURL:[NSURL URLWithString:items.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
            
            [scroView addSubview:imageV];
        }
        scroView.contentSize = CGSizeMake(model.order.count*imgWidth+10, self.imgScroView.height_sd);
        [self.imgScroView addSubview:scroView];
    }
    //显示状态按钮
    NSArray *array = [self returnSetModelState:model];
    [self setStateViewSubView:array];
    
}
#pragma mark ==========点击状态按钮==========
- (IBAction)clickPayBtn:(UIButton *)sender {
    self.clickPayStateBtn(self.model, @"1");
}

#pragma mark ==========点击其他按钮==========

- (IBAction)clickOtherBtn:(UIButton *)sender {
    self.clickPayStateBtn(self.model, @"2");
}

#pragma mark = 设置状态
-(void)setStateViewSubView:(NSArray *)array{
    
    //添加分类
    CGFloat buttonWidth = 65;
    CGFloat buttonHight = 25;
    CGFloat buttonSping = 10;
    CGFloat topSpacing = (40-25)/2;
    CGFloat viewWid = 170;
    UIView *scroView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.moreBtnView.width_sd, self.moreBtnView.height_sd)];
    scroView.backgroundColor = [UIColor whiteColor];
    for (int i =0; i<array.count; i++) {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.frame = CGRectMake(viewWid-(buttonWidth+buttonSping)*(i+1), topSpacing, buttonWidth, buttonHight);
        [aButton setTitle:array[i] forState:UIControlStateNormal];
        [aButton setTitleColor:white_Color forState:UIControlStateNormal];
        aButton.layer.borderWidth = 0.5;
        aButton.layer.borderColor = MAINTextCOLOR.CGColor;
        aButton.layer.cornerRadius = 3;
        aButton.titleLabel.font = textFont_Content14;
        aButton.tag = 100+i;
        aButton.backgroundColor = MAINTextCOLOR;
        [aButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [scroView addSubview:aButton];
    }
    
    [self.moreBtnView addSubview:scroView];
    
}
-(NSArray *)returnSetModelState:(HBOrderList *)model{
    NSArray *array = [NSArray array];
    //设置状态按钮
    if ([model.status isEqualToString:WAIT_BUYER_PAY]) {
        //待付款 取消   去付款
        array = @[STATUTS_PAY,STATUTS_CANCEL];

    }else if ([model.status isEqualToString:WAIT_SELLER_SEND_GOODS]){
        //待发货 取消  判断取消状态
        if ([model.cancel_status isEqualToString:CANCEL_NO_APPLY_CANCEL]) {
            //未申请
            array = @[STATUTS_CANCEL];
        }else if ([model.cancel_status isEqualToString:CANCEL_WAIT_PROCESS]){
            //等待申请
//            array = @[STATUTS_CHA_KAN_XIANGQ];
            array = @[];
        }else if ([model.cancel_status isEqualToString:CANCEL_REFUND_PROCESS]){
            //取消申请
//            array = @[STATUTS_CHA_KAN_XIANGQ];
            array = @[];
        }else if ([model.cancel_status isEqualToString:CANCEL_SUCCESS]){
            //取消成功
            array = @[];
        }else if ([model.cancel_status isEqualToString:CANCEL_FAILS]){
            //取消失败
            array = @[];
        }else{
            array = @[];
        }
    }
    else if ([model.status isEqualToString:WAIT_BUYER_CONFIRM_GOODS]){
        
        //待收货  查看物流 确认收货
        if ([model.pay_type isEqualToString:@"offline"]) {//线下
            array = @[STATUTS_LOGISTICS];
        }else{//线上
            array = @[STATUTS_LOGISTICS,STATUTS_CONFIRM];
        }
        
        
    }
    else if ([model.status isEqualToString:TRADE_FINISHED]){
        //已完成 评价
        if (model.buyer_rate) {
            array = @[];
        }else{
            array = @[STATUTS_EXALUATION];
        }
    }else if([model.status isEqualToString:TRADE_CLOSED_BY_SYSTEM]){
        //已关闭
        return @[];
    }else{
        return @[];
    }
    return array;
}

-(void)clickButton:(UIButton *)button{
    self.clickPayStateBtn(self.model, button.titleLabel.text);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
