//
//  MyOrderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBMyOrderModel.h"
@interface MyOrderCell : UITableViewCell
@property (nonatomic, strong) HBOrderList * model;

@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;
@property (weak, nonatomic) IBOutlet UILabel *orderStateLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UILabel *totalLab;
@property (weak, nonatomic) IBOutlet UIButton *payStateBtn;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UIView *imgScroView;
@property (weak, nonatomic) IBOutlet UIButton *otherBtn;

@property (nonatomic, copy) void(^clickPayStateBtn)(HBOrderList *model,NSString *btnStyle);
@property (weak, nonatomic) IBOutlet UIView *moreBtnView;

@end
