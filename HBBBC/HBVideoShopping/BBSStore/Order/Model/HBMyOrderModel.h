//
//  HBMyOrderModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/16.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBOrderPayModel.h"
@class Pagers;
@class HBOrderList;
@class HBDeliveryModel;
@interface HBMyOrderModel : NSObject
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@property (nonatomic, strong) Pagers * pagers;
@property (nonatomic, strong) NSArray * list;
@end


@interface Pagers : NSObject
@property (nonatomic, assign) NSInteger  total;
@property (nonatomic, assign) NSInteger  page_count;
@end

@interface HBOrderList : NSObject
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * shop_id;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * cancel_status;
@property (nonatomic, strong) NSString * payment;
@property (nonatomic, strong) NSString * pay_type;
@property (nonatomic, strong) NSString * created_time;
@property (nonatomic, assign) NSInteger  buyer_rate;
@property (nonatomic, strong) HBDeliveryModel * delivery;
@property (nonatomic, strong) NSArray * order;
@property (nonatomic, assign) BOOL  is_buyer_rate;
@property (nonatomic, strong) NSString * totalItem;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) NSString * shopname;
@property (nonatomic, strong) NSString * payed_fee;
@property (nonatomic, strong) NSString * cancel_id;
@end

@interface HBOrderModel : NSObject
@property (nonatomic, strong) NSString * title;
@property (nonatomic, assign) NSInteger  num;
@property (nonatomic, strong) NSString * pic_path;
@property (nonatomic, strong) NSString * oid;
@property (nonatomic, strong) NSString * aftersales_status;
@property (nonatomic, assign) NSInteger  complaints_status;
@property (nonatomic, strong) NSString * item_id;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, assign) NSInteger  gift_data;
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * gift_count;
@end

@interface HBDeliveryModel : NSObject
@property (nonatomic, strong) NSString * logi_name;
@property (nonatomic, strong) NSString * logi_no;
@property (nonatomic, strong) NSString * corp_code;
@end
