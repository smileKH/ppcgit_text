//
//  OldLoginViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OldLoginViewController.h"
#import "OtherLoginViewController.h"
#import "NSString+Base64Image.h"
@interface OldLoginViewController ()
{
    NSString *sess_id;
}

@end

@implementation OldLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //微信授权
    self.title = @"授权登录";
    [self getpicCode];
    //点击刷新
    self.imageCode.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [self.imageCode addGestureRecognizer:tap];
}

- (void)tapClick:(UITapGestureRecognizer*)tap
{
    //    [self.codeImgVeiw freshVerCode];
    [self getpicCode];
}
//获取验证码
- (void)getpicCode
{
    WEAKSELF;
    [LBService post:USER_VC_CODE params:@{@"vcode_type":@"topapi_bind"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            sess_id = response.result[@"data"][@"sess_id"];
            NSString *imageString = response.result[@"data"][@"base64Image"];
            weakSelf.imageCode.image = [imageString toBase64Image];
        }else{
            
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark ==========点击密码==========
- (IBAction)checkPassord:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.psw1.secureTextEntry = !sender.selected;
    self.psw2.secureTextEntry = !sender.selected;
}

- (IBAction)clickFinshBtn:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.phone.text.length <= 0) {
        [app showToastView:@"请输入账号"];
        return;
    }
    
    if (self.psw1.text.length <= 0) {
        [app showToastView:@"请输入密码"];
        return;
    }
    if (self.psw2.text.length <= 0) {
        [app showToastView:@"请输入确认密码"];
        return;
    }
    if (![self.psw2.text isEqualToString:self.psw1.text]) {
        [app showToastView:@"两次输入的密码不一样！"];
        return;
    }
    
    if (self.code.text.length <= 0) {
        [app showToastView:@"请输入验证码"];
        return;
    }
    
    [self login];
}

- (IBAction)clickOldAccBtn:(UIButton *)sender {
    OtherLoginViewController *vc= [[OtherLoginViewController alloc] initWithNibName:@"OtherLoginViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)login{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"openid"] = USERDEFAULT_value(WX_OPENID);
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"id"] = @"weixin";
    dict[@"authResult"] = dic;
    dict[@"userInfo"] = USERDEFAULT_value(WX_UNIONID);
    
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"uname"] = self.phone.text;
    parameters[@"password"] = self.psw1.text;
    parameters[@"password_confirm"] = self.psw2.text;
    parameters[@"option"] = @"new";
    parameters[@"verifycode"] = self.code.text;
    parameters[@"deviceid"] = @"iPhone";
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    parameters[@"trust_params"] = jsonString;
    parameters[@"sess_id"] = sess_id;
    [LBService post:USER_TRUST_BIND params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            //先保存token
            NSDictionary *dic = response.result[@"data"];
            NSString *accessToken = dic[@"accessToken"];
            NSNumber *binded = dic[@"binded"];//绑定
            //保存账号信息
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:accessToken forKey:TOKEN_XY_APP];
            [userDefaults setObject:binded forKey:AUTH_RESULT_BINDED];
            [userDefaults synchronize];
            userManager.loginState = 1;
            //登录成功，请求用户信息
            [weakSelf requesUserInfo];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========请求用户资料==========
-(void)requesUserInfo{
    //请求个人资料
    WEAKSELF;
    [userManager requestUserInfo:nil completion:^(BOOL success, NSString *des) {
        if (success) {
            //
            //发一个通知
            [[NSNotificationCenter defaultCenter]postNotificationName:NotificationLoginSuccess object:nil];
            [weakSelf back];
        }else{
            
        }
    }];
}
- (void)back{
    
    NSArray *temArray = self.navigationController.viewControllers;
    for(UIViewController *temVC in temArray){
        if ([temVC isKindOfClass:[HBMineViewController class]]){
            [self.navigationController popToViewController:temVC animated:YES];
        }
    }
    
}
@end
