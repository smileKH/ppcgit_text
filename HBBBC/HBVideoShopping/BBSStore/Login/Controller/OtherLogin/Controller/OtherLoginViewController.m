//
//  OtherLoginViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OtherLoginViewController.h"
#import "OldLoginViewController.h"
@interface OtherLoginViewController ()

@end

@implementation OtherLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"绑定老账户";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)checkPassord:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.psw1.secureTextEntry = !sender.selected;
}

- (IBAction)clickNewBtn:(id)sender {
    OldLoginViewController *vc= [[OldLoginViewController alloc] initWithNibName:@"OldLoginViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)clickFinshBtn:(id)sender {
    [self.view endEditing:YES];
    if (self.phone.text.length <= 0) {
        [app showToastView:@"请输入账号"];
        return;
    }
    
    if (self.psw1.text.length <= 0) {
        [app showToastView:@"请输入密码"];
        return;
    }
    
    
    [self login];
}
- (void)login{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"openid"] = USERDEFAULT_value(WX_OPENID);
    //    dic[@"userInfo"] = USERDEFAULT_value(WX_UNIONID);
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"id"] = @"weixin";
    dict[@"authResult"] = dic;
    dict[@"userInfo"] = USERDEFAULT_value(WX_UNIONID);
    
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"uname"] = self.phone.text;
    parameters[@"password"] = self.psw1.text;
    parameters[@"password_confirm"] = self.psw1.text;
    parameters[@"option"] = @"old";
    parameters[@"deviceid"] = @"iPhone";
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    parameters[@"trust_params"] = jsonString;
    [LBService post:USER_TRUST_BIND params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            //先保存token
            NSDictionary *dic = response.result[@"data"];
            NSString *accessToken = dic[@"accessToken"];
            NSNumber *binded = dic[@"binded"];//绑定
            //保存账号信息
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:accessToken forKey:TOKEN_XY_APP];
            [userDefaults setObject:binded forKey:AUTH_RESULT_BINDED];
            [userDefaults synchronize];
            userManager.loginState = 1;
            //登录成功，请求用户信息
            [weakSelf requesUserInfo];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========请求用户资料==========
-(void)requesUserInfo{
    //请求个人资料
    WEAKSELF;
    [userManager requestUserInfo:nil completion:^(BOOL success, NSString *des) {
        if (success) {
            //
            //发一个通知
            [[NSNotificationCenter defaultCenter]postNotificationName:NotificationLoginSuccess object:nil];
            [weakSelf back];
        }else{
            
        }
    }];
}
- (void)back{
    
    NSArray *temArray = self.navigationController.viewControllers;
    for(UIViewController *temVC in temArray){
        if ([temVC isKindOfClass:[HBMineViewController class]]){
            [self.navigationController popToViewController:temVC animated:YES];
        }
    }
    
}
@end

