//
//  OldLoginViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OldLoginViewController : HBRootViewController
@property (strong, nonatomic) IBOutlet UITextField *phone;
@property (strong, nonatomic) IBOutlet UIImageView *imageCode;
@property (strong, nonatomic) IBOutlet UITextField *code;
@property (strong, nonatomic) IBOutlet UITextField *psw1;
@property (strong, nonatomic) IBOutlet UITextField *psw2;
- (IBAction)checkPassord:(UIButton *)sender;

- (IBAction)clickFinshBtn:(UIButton *)sender;
- (IBAction)clickOldAccBtn:(UIButton *)sender;

@end
