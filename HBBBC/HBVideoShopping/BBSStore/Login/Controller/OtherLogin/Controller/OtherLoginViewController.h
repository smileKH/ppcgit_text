//
//  OtherLoginViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherLoginViewController : HBRootViewController
@property (strong, nonatomic) IBOutlet UITextField *phone;
@property (strong, nonatomic) IBOutlet UITextField *psw1;
- (IBAction)clickFinshBtn:(id)sender;
- (IBAction)checkPassord:(id)sender;
- (IBAction)clickNewBtn:(id)sender;

@end
