//
//  ForgetPswViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/9.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPswViewController : HBRootViewController
- (IBAction)next:(id)sender;



@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *verifyAccount_token;

@property (strong, nonatomic) IBOutlet UITextField *vcodeTf;

@property (strong, nonatomic) IBOutlet UILabel *phone;
@property (strong, nonatomic) IBOutlet UIButton *timeBtn;
- (IBAction)clickTiemBtn:(UIButton *)sender;

@end
