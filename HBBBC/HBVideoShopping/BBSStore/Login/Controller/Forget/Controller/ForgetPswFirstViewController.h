//
//  ForgetPswFirstViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MQVerCodeImageView.h"
@interface ForgetPswFirstViewController : HBRootViewController
- (IBAction)next:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *imageCode;
@property (strong, nonatomic) IBOutlet UITextField *code;

@property (strong, nonatomic) IBOutlet UITextField *phone;
@end
