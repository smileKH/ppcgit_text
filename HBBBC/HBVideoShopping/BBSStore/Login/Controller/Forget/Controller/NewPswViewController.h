//
//  NewPswViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/9.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPswViewController : HBRootViewController
@property (nonatomic, strong) NSString *tel;
@property (strong, nonatomic) IBOutlet UITextField *psw1;
@property (nonatomic, strong) NSString *forgot_token;
@property (strong, nonatomic) IBOutlet UITextField *psw2;
- (IBAction)forgetPswAction:(UIButton *)sender;
@end
