//
//  ForgetPswViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/9.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ForgetPswViewController.h"
#import "NewPswViewController.h"
@interface ForgetPswViewController ()
{
    NSTimer *timer;
    NSInteger time;
}
@end

@implementation ForgetPswViewController
-(void)backBtnClicked{
    WEAKSELF;
    [self AlertWithTitle:@"提示" message:@"确定放弃找回密码?" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            NSArray *temArray = self.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[LoginViewController class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }        }else{
                //取消
            }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.title = @"忘记密码";//获取手机验证码
    NSString *tit = [NSString stringWithFormat:@"验证码已发送至您的手机：%@",self.tel];
    self.phone.attributedText = [StringAttribute stringToAttributeString:tit withRange:NSMakeRange(11, tit.length-11) withFont:14 withColor:MAINTextCOLOR];
    [self setTimer];
    [self sendSMS];
}
- (void)setTimer{
    time = 60;
    if (@available(iOS 10.0, *)) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            time--;
            self.timeBtn.userInteractionEnabled = NO;
            [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
            if (time == 0) {
                time = 60;
                [timer invalidate];
                self.timeBtn.userInteractionEnabled = YES;
                [self.timeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
            }
        }];
    } else {
        // Fallback on earlier versions
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(times) userInfo:nil repeats:YES];
    }
}
-(void)times{
    time--;
    self.timeBtn.userInteractionEnabled = NO;
    [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
    if (time == 0) {
        [timer invalidate];
        time = 60;
        self.timeBtn.userInteractionEnabled = YES;
        [self.timeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
    }
}
- (void)sendSMS
{
    [LBService post:USER_SEND_SMS params:@{@"mobile":self.tel,@"send_sms_token":self.verifyAccount_token,@"type":@"forgot"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //发送信息成功
             [timer fire];
        }else{
            //发送信息失败
            [timer invalidate];
            time = 60;
            self.timeBtn.userInteractionEnabled = YES;
            [self.timeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========下一步==========
- (void)checkSMSCode
{
    [LBService post:USER_VERIFY_SMS params:@{@"mobile":self.tel,@"vcode":self.vcodeTf.text,@"type":@"forgot"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            NSString *verifySms_token = response.result[@"data"][@"verifySms_token"];
            NewPswViewController *vc= [[NewPswViewController alloc] initWithNibName:@"NewPswViewController" bundle:nil];
            vc.forgot_token = verifySms_token;
            vc.tel = self.tel;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)next:(id)sender {
    [self checkSMSCode];
   
}
#pragma mark ==========重新获取==========
- (IBAction)clickTiemBtn:(UIButton *)sender {
    [self setTimer];
    [self sendSMS];
}
@end
