//
//  ForgetPswFirstViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ForgetPswFirstViewController.h"
#import "ForgetPswViewController.h"
#import "NSString+Base64Image.h"
@interface ForgetPswFirstViewController ()
{
    NSString *sess_id;
}
@end

@implementation ForgetPswFirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //    UISQURE
    self.title = @"忘记密码";//输入验证码
    
    //点击刷新
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    self.imageCode.userInteractionEnabled = YES;
    [self.imageCode addGestureRecognizer:tap];
    [self getpicCode];
}

- (void)getpicCode
{
    WEAKSELF;
    [LBService post:USER_VC_CODE params:@{@"vcode_type":@"topapi_forgot"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //判断一下比较好
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                sess_id = dic[@"sess_id"];
                NSString *imageString = dic[@"base64Image"];
                weakSelf.imageCode.image = [imageString toBase64Image];
            }else{
                [app showToastView:@"获取验证码失败，请稍后再试"];
            }
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)tapClick:(UITapGestureRecognizer*)tap
{
    [self getpicCode];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)next:(id)sender {
    if (self.phone.text.length <= 0) {
        [app showToastView:@"请填写账号"];
        return;
    }
    if (self.code.text.length <= 0) {
        [app showToastView:@"请填写验证码"];
        return;
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:USER_VERIFY_ACCOUNT params:@{@"account":self.phone.text,@"vcode_type":@"topapi_forgot",@"verifycode":self.code.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSString *verifyAccount_token = dic[@"verifyAccount_token"];
                ForgetPswViewController *vc= [[ForgetPswViewController alloc] initWithNibName:@"ForgetPswViewController" bundle:nil];
                vc.tel = weakSelf.phone.text;
                vc.verifyAccount_token = verifyAccount_token;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else{
                [app showToastView:@"请稍后再试"];
            }
        }else{
            [app showToastView:response.message];
            
        }
    }];
    
    
}
@end

