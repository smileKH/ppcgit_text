//
//  LoginViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MQVerCodeImageView.h"
@interface LoginViewController : HBRootViewController
@property (weak, nonatomic) IBOutlet UIButton *registBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkbtn;
@property (weak, nonatomic) IBOutlet UITextField *pswTextField;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
- (IBAction)loginAction:(UIButton *)sender;
- (IBAction)forgetAction:(UIButton *)sender;
- (IBAction)registAction:(UIButton *)sender;
- (IBAction)checkPsw:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *codeView;

@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIImageView *codeImgVeiw;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordConstraint;


@end
