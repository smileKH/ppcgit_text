//
//  LoginViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgetPswViewController.h"
#import "NewPswViewController.h"
#import "ForgetPswFirstViewController.h"

#import "RegistFirstViewController.h"



#import "OtherLoginViewController.h"
#import "OldLoginViewController.h"

#import "NSString+Base64Image.h"
#import "HBUserInfo.h"
//#import "BQLAuthEngine.h"
@interface LoginViewController ()
{
    NSString *sess_id;
}
@property (nonatomic, strong) NSString * isState;
@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isState = @"0";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    UISQURE
//    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.navigationItem.backBarButtonItem = item;
//    self.navigationController.navigationBar.barTintColor = white_Color;
//    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.title = @"用户登录";
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.layer.cornerRadius = 4;
    self.registBtn.layer.masksToBounds = YES;
    self.registBtn.layer.cornerRadius = 4;
    self.registBtn.layer.borderColor = MAINBGCOLOR.CGColor;
    self.registBtn.layer.borderWidth = 1.0f;
    //注册完成
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(registerResult) name:@"RegisterResultViewController" object:nil];
    
    //点击刷新
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    self.codeImgVeiw.userInteractionEnabled = YES;
    [self.codeImgVeiw addGestureRecognizer:tap];
//    [self getpicCode];
    self.codeView.hidden = YES;
    self.loginConstraint.constant-=50;
    self.passwordConstraint.constant-=50;
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(authsuccess) name:WX_AUTH_TOKEN_ALLDATA object:nil];
    
}
- (void)getpicCode
{
    WEAKSELF;
    [LBService post:USER_VC_CODE params:@{@"vcode_type":@"topapi_login"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            sess_id = response.result[@"data"][@"sess_id"];
            NSString *imageString = response.result[@"data"][@"base64Image"];
            weakSelf.codeImgVeiw.image = [imageString toBase64Image];
        }else{
            
        }
    }];
}
- (void)tapClick:(UITapGestureRecognizer*)tap
{
//    [self.codeImgVeiw freshVerCode];
    [self getpicCode];
}
-(void)registerResult{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)qq:(id)sender {
    [app showToastView:@"此功能暂未开放"];
}
- (IBAction)wx:(id)sender {
    
    [app showToastView:@"此功能暂未开放"];
    
//    SendAuthReq *req =[[SendAuthReq alloc ] init];
//    req.scope = @"snsapi_message,snsapi_userinfo,snsapi_friend,snsapi_contact" ;
//    // 这个字符串你最好使用加密算法得到,这里我是乱写的,功能无影响
//    req.state = @"qwertyuioplkjhgfdsazxcvbnm";
//    [WXApi sendReq:req];
 
}
- (IBAction)wb:(id)sender {
    [app showToastView:@"此功能暂未开放"];
}

//微信登录
-(void)authsuccess{
    OldLoginViewController *vc= [[OldLoginViewController alloc] initWithNibName:@"OldLoginViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginAction:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.userTextField.text.length <= 0) {
        [app showToastView:@"请输入账号"];
        return;
    }
    
    if (self.pswTextField.text.length <= 0) {
        [app showToastView:@"请输入密码"];
        return;
    }
    
    [self login];
}

- (IBAction)forgetAction:(UIButton *)sender {
    ForgetPswFirstViewController *vc= [[ForgetPswFirstViewController alloc] initWithNibName:@"ForgetPswFirstViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)registAction:(UIButton *)sender {
    RegistFirstViewController *vc= [[RegistFirstViewController alloc] initWithNibName:@"RegistFirstViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)checkPsw:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.pswTextField.secureTextEntry = !sender.selected;
}

- (void)login{
    WEAKSELF;
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = self.userTextField.text;
    parameter[@"password"] = self.pswTextField.text;
    parameter[@"deviceid"] = @"app";
    if ([self.isState isEqualToString:@"1"]) {
        //要验证码
        if ([HBHuTool isJudgeString:self.codeTF.text]) {
            [app showToastView:@"请填写验证码"];
        }
        parameter[@"vcode"] = self.codeTF.text;
        parameter[@"sess_id"] = sess_id;
    }
    
    
    [MBProgressHUD showActivityMessageInWindow:@"登录中..." timer:10];
    [userManager login:bUserLoginTypePwd params:parameter completion:^(BOOL success, NSString *des) {
        [MBProgressHUD hideHUD];
        if (success) {
            //保存账号信息
//            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//            NSDictionary *dicAccount = [[NSDictionary alloc] initWithObjectsAndKeys:weakSelf.accountStr,ACCOUNT,weakSelf.passwordStr,PASSWORD, nil];
//            [userDefaults setObject:dicAccount forKey:AUTOLOGIN];
//            [userDefaults synchronize];
            //登录成功，请求用户信息
            [weakSelf requesUserInfo];
        }else{
            [app showToastView:des];
            if ([des isEqualToString:@"验证码填写错误"]&&[weakSelf.isState isEqualToString:@"0"]) {
                weakSelf.isState = @"1";
                //刷新 界面
                [weakSelf getpicCode];
                weakSelf.codeView.hidden = NO;
                if (!weakSelf.codeView.hidden) {
                    weakSelf.loginConstraint.constant+=50;
                    weakSelf.passwordConstraint.constant+=50;
                }
                
            }
        }
    }];
    
   

}
#pragma mark ==========请求用户资料==========
-(void)requesUserInfo{
    //请求个人资料
    WEAKSELF;
    [userManager requestUserInfo:nil completion:^(BOOL success, NSString *des) {
        if (success) {
            //
            //发一个通知
            [[NSNotificationCenter defaultCenter]postNotificationName:NotificationLoginSuccess object:nil];
            [weakSelf back];
        }else{
            
        }
    }];
}
- (void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
