//
//  RegistFirstViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "RegistFirstViewController.h"
#import "RegisterViewController.h"
#import "NSString+Base64Image.h"
#import "HBRegistSeeArgreementVC.h"
@interface RegistFirstViewController ()
{
    NSString *sess_id;
}
@end

@implementation RegistFirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"注册";
    
    //点击刷新
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    self.imageCode.userInteractionEnabled = YES;
    [self.imageCode addGestureRecognizer:tap];
    [self getpicCode];
}
-(void)backBtnClicked{
    WEAKSELF;
    [self AlertWithTitle:@"提示" message:@"确定放弃注册?" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            NSArray *temArray = self.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[LoginViewController class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }        }else{
            //取消
        }
    }];
}

- (void)getpicCode
{
    WEAKSELF;
    [LBService post:USER_VC_CODE params:@{@"vcode_type":@"topapi_register"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //判断一下比较好
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                sess_id = dic[@"sess_id"];
                NSString *imageString = dic[@"base64Image"];
                weakSelf.imageCode.image = [imageString toBase64Image];
            }else{
                [app showToastView:@"获取验证码失败，请稍后再试"];
            }
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)tapClick:(UITapGestureRecognizer*)tap
{
    [self getpicCode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)next:(id)sender
{
    [self.view endEditing:YES];
    
    if (!self.protoBtn.selected) {
        [app showToastView:@"请同意注册协议"];
        return;
    }
    if (self.phone.text.length <= 0) {
        [app showToastView:@"请填写账号"];
        return;
    }
    if (self.code.text.length <= 0) {
        [app showToastView:@"请填写验证码"];
        return;
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:USER_VERIFY_ACCOUNT params:@{@"account":self.phone.text,@"vcode_type":@"topapi_register",@"verifycode":self.code.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSString *verifyAccount_token = dic[@"verifyAccount_token"];
                RegisterViewController *vc= [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
                vc.tel = self.phone.text;
                vc.verifyAccount_token = verifyAccount_token;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else{
                [app showToastView:@"请稍后再试"];
            }
            
        }else{
            [app showToastView:response.message];
        }
    }];

}
- (IBAction)clickProtoBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
}
//点击查看协议
- (IBAction)clickSeeArgeementBtn:(UIButton *)sender {
    HBRegistSeeArgreementVC *vc = [[HBRegistSeeArgreementVC alloc]initWithNibName:@"HBRegistSeeArgreementVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
