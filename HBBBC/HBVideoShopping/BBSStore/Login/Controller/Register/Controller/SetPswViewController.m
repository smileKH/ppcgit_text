//
//  SetPswViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "SetPswViewController.h"
#import "RegisterResultViewController.h"
@interface SetPswViewController ()

@end

@implementation SetPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"设置密码";
}
-(void)backBtnClicked{
    WEAKSELF;
    [self AlertWithTitle:@"提示" message:@"确定放弃注册?" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            NSArray *temArray = self.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[LoginViewController class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }        }else{
            //取消
        }
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)next:(id)sender {
    [self.view endEditing:YES];
    if (self.psw1.text.length<6 || self.psw1.text.length > 20) {
        [app showToastView:@"密码长度为6~20位"];
        return;
    }
    if (![self.psw1.text isEqualToString:self.psw2.text]) {
        [app showToastView:@"两次密码不一致"];
        return;
    }
    [self registMember];
   
}

- (void)registMember
{
    WEAKSELF;
    [LBService post:USER_SIGN_UP params:@{@"account":self.tel,@"password":self.psw1.text,@"signup_token":self.signup_token,@"password_confirmation":self.psw2.text,@"deviceid":@"mobile"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //注册成功就已经拿到token了
            //NSString *userID = response.result[@"data"][@"user_id"];//用户id
            NSString *accessToken = response.result[@"data"][@"accessToken"];//用户token
            //NSString *sendPointNum = response.result[@"data"][@"sendPointNum"];//赠送的积分数量
            
            //保存账号信息
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:accessToken forKey:TOKEN_XY_APP];
            [userDefaults synchronize];
            userManager.loginState = 1;
            //发一个通知
            [[NSNotificationCenter defaultCenter]postNotificationName:NotificationLoginSuccess object:nil];

            RegisterResultViewController *vc= [[RegisterResultViewController alloc] initWithNibName:@"RegisterResultViewController" bundle:nil];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];

}
@end
