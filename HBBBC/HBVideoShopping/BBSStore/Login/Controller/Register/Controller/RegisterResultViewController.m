//
//  RegisterResultViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "RegisterResultViewController.h"

@interface RegisterResultViewController ()

@end

@implementation RegisterResultViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //注册完成
}
-(void)backBtnClicked{
    
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"RegisterResultViewController" object:nil];
//    NSArray *temArray = self.navigationController.viewControllers;
//    for(UIViewController *temVC in temArray){
//        if ([temVC isKindOfClass:[LoginViewController class]]){
//            [self.navigationController popToViewController:temVC animated:YES];
//        }
//    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"RegisterResultViewController" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (IBAction)goAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"RegisterResultViewController" object:nil];
}
@end
