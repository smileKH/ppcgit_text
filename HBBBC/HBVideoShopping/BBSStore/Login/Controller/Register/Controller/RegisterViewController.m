//
//  RegisterViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/19.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "RegisterViewController.h"
#import "SetPswViewController.h"
@interface RegisterViewController ()
{
    NSTimer *timer;
    NSInteger time;
}
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    NSString *tit = [NSString stringWithFormat:@"验证码已发送至您的手机：%@",self.tel];
    self.phone.attributedText = [StringAttribute stringToAttributeString:tit withRange:NSMakeRange(11, tit.length-11) withFont:14 withColor:MAINTextCOLOR];
    self.title = @"注册";
    [self setTimer];
    [self sendSMS];
    
}
-(void)backBtnClicked{
    WEAKSELF;
    [self AlertWithTitle:@"提示" message:@"确定放弃注册?" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            NSArray *temArray = self.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[LoginViewController class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }        }else{
            //取消
        }
    }];
}
- (void)setTimer{
    time = 60;
    if (@available(iOS 10.0, *)) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            time--;
            self.timeBtn.userInteractionEnabled = NO;
            [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
            if (time == 0) {
                time = 60;
                [timer invalidate];
                self.timeBtn.userInteractionEnabled = YES;
                [self.timeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
            }
        }];
    } else {
        // Fallback on earlier versions
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(times) userInfo:nil repeats:YES];
    }
}
-(void)times{
    time--;
    self.timeBtn.userInteractionEnabled = NO;
    [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
    if (time == 0) {
        [timer invalidate];
        time = 60;
        self.timeBtn.userInteractionEnabled = YES;
        [self.timeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
    }
}
- (void)sendSMS
{
    [LBService post:USER_SEND_SMS params:@{@"mobile":self.tel,@"send_sms_token":self.verifyAccount_token,@"type":@"signup"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            [timer fire];
        }else{
            [app showToastView:response.message];
        }
    }];
}

- (void)checkSMSCode
{
    WEAKSELF;
    [LBService post:USER_VERIFY_SMS params:@{@"mobile":self.tel,@"vcode":self.vcodeTf.text,@"type":@"signup"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            NSString *verifySms_token = response.result[@"data"][@"verifySms_token"];
            SetPswViewController *vc= [[SetPswViewController alloc] initWithNibName:@"SetPswViewController" bundle:nil];
            vc.signup_token = verifySms_token;
            vc.tel = weakSelf.tel;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)next:(id)sender {
    if (self.vcodeTf.text.length <= 0) {
        [app showToastView:@"请填写短信验证码"];
        return;
    }
    [self checkSMSCode];
}
#pragma mark ==========重新获取数据==========
- (IBAction)clickTimeBtn:(UIButton *)sender {
    [self setTimer];
    [self sendSMS];
}
@end
