//
//  RegisterResultViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterResultViewController : HBRootViewController
@property (strong, nonatomic) IBOutlet UIButton *go;
- (IBAction)goAction:(id)sender;


@end
