//
//  RegistFirstViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MQVerCodeImageView.h"

#import "MF_Base64Additions.h"
@interface RegistFirstViewController : HBRootViewController
- (IBAction)next:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imageCode;
@property (strong, nonatomic) IBOutlet UITextField *code;

@property (strong, nonatomic) IBOutlet UITextField *phone;
- (IBAction)clickProtoBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *protoBtn;
- (IBAction)clickSeeArgeementBtn:(UIButton *)sender;


@end
