//
//  HBCartOffterLineModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/6.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Price;
@interface HBCartOffterLineModel : NSObject
@property (nonatomic, assign) BOOL nocheckedall;
@property (nonatomic, strong) NSMutableArray * shopArray;
@end

@interface HBCartOfShopItmModel : NSObject
@property (nonatomic, strong) NSString * shopName;
@property (nonatomic, strong) NSString * shopId;
@property (nonatomic, assign) BOOL is_checked;//自己添加上去的选中按钮
@property (nonatomic, assign) CGFloat shopMoney;//店铺合计金额
@property (nonatomic, strong) NSMutableArray *goods;
@property (nonatomic, strong) NSString * hasCoupon;
@end

@interface HBCartOfGoodsItemModel : NSObject

@property (nonatomic, strong) NSString * goodsName;//名字
@property (nonatomic, strong) NSString * image_default_id;
@property (nonatomic, strong) NSString * goodsId;
@property (nonatomic, strong) NSString * money;
@property (nonatomic, assign)  NSInteger quantity;//    int        3    商品数量
@property (nonatomic, strong)  NSString *sku_id;//    int    required_if:obj_type,item    3    货品id
@property (nonatomic, strong)  NSString *package_sku_ids;//    string    required_if:obj_type,package    11,21,45    组合促销sku_id
@property (nonatomic, strong)  NSString *package_id;//    integer    sometimes    required    integer | 3 | 组合促销id
@property (nonatomic, strong)  NSString *obj_type;//    string    in:item,package    item    对象类型 item普通商品，package组合促销
@property (nonatomic, strong)  NSString *mode;//cart,fastbuy

@property (nonatomic, assign) BOOL is_checked;
@property (nonatomic, strong) Price *price;
@end

