//
//  ShoppingCarModel.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShoppingCarModel.h"

@implementation ShoppingCarModel
- (void)setCartlist:(NSArray *)cartlist
{
    _cartlist = [Cartlist mj_objectArrayWithKeyValuesArray:cartlist];
}
@end

@implementation Cartlist

- (void)setPromotion_cartitems:(NSArray *)promotion_cartitems
{
    _promotion_cartitems = [Cartitems  mj_objectArrayWithKeyValuesArray:promotion_cartitems];
    [self setGoods:promotion_cartitems];
}

- (void)setGoods:(NSArray *)goods
{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSInteger i = 0; i < self.promotion_cartitems.count; i ++) {
        Cartitems *pri = self.promotion_cartitems[i];
        
        for (NSInteger j = 0; j < pri.cartitemlist.count; j ++) {
            Cartitemlist *lis = pri.cartitemlist[j];
            // 添加选中活动
            for (Promotiontags* promotiontags in lis.promotiontags) {
                if ([promotiontags.promotion_id isEqualToString:lis.selected_promotion]) {
                    promotiontags.selected = YES;
                }
            }
            GoodsItem * item = [GoodsItem new];
            item.promotion = pri.promotion;
            item.discount_price = pri.discount_price;
            item.goodsItem = lis;
            [arr addObject:item];
        }
        
    }
    _goods = (NSArray *)arr;
    
}

- (void)setIs_checked:(BOOL)is_checked{
    BOOL isSelectAll = YES;
    for (NSInteger i = 0; i < self.goods.count; i++) {
        GoodsItem *item = self.goods[i];
        isSelectAll =  item.goodsItem.is_checked && isSelectAll;
    }
    
    _is_checked = isSelectAll;
}
@end

@implementation TotalCart

@end

@implementation Price

@end

@implementation Cartitemlist
- (void)setPromotiontags:(NSArray *)promotiontags
{
    _promotiontags = [Promotiontags mj_objectArrayWithKeyValuesArray:promotiontags];
}


@end


@implementation Promotion_item

@end

@implementation CartCount

@end

@implementation Promotiontags
@end
@implementation Cartitems
- (void)setCartitemlist:(NSArray *)cartitemlist
{
    _cartitemlist = [Cartitemlist mj_objectArrayWithKeyValuesArray:cartitemlist];
}

@end
@implementation GoodsItem
@end
