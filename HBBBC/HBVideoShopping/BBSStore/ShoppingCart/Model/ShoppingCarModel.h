//
//  ShoppingCarModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GoodsListModel.h"
@class Cartlist;
@class TotalCart;
@class Price;
@class Cartitems;
@class Cartitemlist;
@class Price;
@class CartCount;
@class Promotion_item;
@class Promotiontags;
@class GoodsItem;
@interface ShoppingCarModel : NSObject
@property (nonatomic, assign) BOOL nocheckedall;
@property (nonatomic, strong) NSArray *cartlist;
@property (nonatomic, strong) TotalCart *totalCart;
@end


@interface Cartlist : NSObject
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *shop_name;
@property (nonatomic, strong) NSString *shop_type;
@property (nonatomic, strong) CartCount *cartCount;
@property (nonatomic, strong) NSString *hasCoupon;
@property (nonatomic, strong) NSString *nocheckedall;
@property (nonatomic, strong) NSArray *promotion_cartitems;
@property (nonatomic, strong) NSArray *goods;
@property (nonatomic, assign) BOOL is_checked;//自己添加上去的选中按钮
@property (nonatomic, assign) CGFloat shopMoney;//店铺合计金额
@end

@interface Cartitems : NSObject
@property (nonatomic, strong) Promotion_item *promotion;
@property (nonatomic, strong) NSString *discount_price;
@property (nonatomic, strong) NSArray *cartitemlist;
@end

@interface Promotion_item : NSObject
@property (nonatomic, strong) NSString *promotion_id;
@property (nonatomic, strong) NSString *rel_promotion_id;
@property (nonatomic, strong) NSString *promotion_type;
@property (nonatomic, strong) NSString *promotion_name;
@property (nonatomic, strong) NSString *promotion_tag;
@end

@interface Cartitemlist : NSObject
@property (nonatomic, strong) NSString *cart_id;
@property (nonatomic, strong) NSString *obj_type;
@property (nonatomic, strong) NSString *item_id;
@property (nonatomic, strong) NSString *sku_id;
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *selected_promotion;
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *sub_stock;
@property (nonatomic, strong) NSString *spec_info;
@property (nonatomic, strong) NSString *bn;
@property (nonatomic, strong) NSString *dlytmpl_id;
@property (nonatomic, strong) NSString *store;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) Price *price;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image_default_id;
@property (nonatomic, strong) NSString *weight;
@property (nonatomic, strong) NSString *valid;
@property (nonatomic, assign) BOOL is_checked;
@property (nonatomic, strong) NSString *promotions;
@property (nonatomic, strong) NSString *activityinfo;
@property (nonatomic, strong) NSArray *promotiontags;
@property (nonatomic, strong) Gift *gifts;
@end

@interface GoodsItem : NSObject
@property (nonatomic, strong) Promotion_item *promotion;
@property (nonatomic, strong) NSString *discount_price;
@property (nonatomic, strong) Cartitemlist *goodsItem;
@end


@interface Promotiontags : NSObject
@property (nonatomic, strong) NSString *promotion_id;
@property (nonatomic, strong) NSString *promotion_name;
@property (nonatomic, strong) NSString *promotion_tag;
@property (nonatomic, assign) BOOL selected;
@end


@interface Price : NSObject
@property (nonatomic, strong) NSString *discount_price;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *total_price;
@end


@interface CartCount : NSObject
@property (nonatomic, strong) NSString *total_weight;
@property (nonatomic, strong) NSString *itemnum;
@property (nonatomic, strong) NSString *total_fee;
@property (nonatomic, strong) NSString *total_discount;
@property (nonatomic, strong) NSString *total_coupon_discount;
@property (nonatomic, strong) NSString * payment;
@property (nonatomic, strong) NSString * post_fee;
@property (nonatomic, strong) NSString * obtain_point_fee;
@end

@interface TotalCart : NSObject
@property (nonatomic, strong) NSString *voucherDicountPrice;
@property (nonatomic, strong) NSString *totalWeight;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *totalPrice;
@property (nonatomic, strong) NSString *totalAfterDiscount;
@property (nonatomic, strong) NSString *totalDiscount;
@end
