//
//  HBCartTotalModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/14.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCartTotalModel.h"
@class HBTotalModel;
@class HBUserPointModel;
@class HBTotalShopModel;
@class HBcatItemPriceModel;
@class HBcatItemPriceShopModel;
@implementation HBCartTotalModel

@end

@implementation HBTotalModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"shop":@"HBTotalShopModel",@"catItemPrice":@"HBcatItemPriceModel"};
}
@end

@implementation HBUserPointModel

@end

@implementation HBTotalShopModel

@end

@implementation HBcatItemPriceModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"shop":@"HBcatItemPriceShopModel"};
}
@end

@implementation HBcatItemPriceShopModel

@end

