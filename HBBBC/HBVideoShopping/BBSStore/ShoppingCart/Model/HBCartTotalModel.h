//
//  HBCartTotalModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/14.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBTotalModel;
@class HBUserPointModel;
@class HBTotalShopModel;
@class HBcatItemPriceModel;
@class HBcatItemPriceShopModel;
@interface HBCartTotalModel : NSObject
@property (nonatomic, strong) HBTotalModel * total;
@property (nonatomic, strong) HBUserPointModel * userPoint;
@end

@interface HBTotalModel : NSObject
@property (nonatomic, strong) NSString * allPayment;
@property (nonatomic, strong) NSString * allPostfee;
@property (nonatomic, strong) NSString * disCountfee;
@property (nonatomic, strong) NSArray * shop;
@property (nonatomic, strong) NSArray * catItemPrice;
@end

@interface HBTotalShopModel : NSObject
@property (nonatomic, strong) NSString * payment;
@property (nonatomic, strong) NSString * total_fee;
@property (nonatomic, strong) NSString * discount_fee;
@property (nonatomic, strong) NSString * obtain_point_fee;
@property (nonatomic, strong) NSString * post_fee;
@property (nonatomic, strong) NSString * totalWeight;
@property (nonatomic, strong) NSString * shop_id;
@end

@interface HBcatItemPriceModel : NSObject
@property (nonatomic, strong) NSArray * shop;
@property (nonatomic, strong) NSString * car_lv1_id;
@end

@interface HBcatItemPriceShopModel : NSObject
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * shop_id;
@end

@interface HBUserPointModel : NSObject
@property (nonatomic, strong) NSString * open_point_deduction;
@property (nonatomic, strong) NSString * point_deduction_rate;
@property (nonatomic, strong) NSString * point_deduction_max;
@property (nonatomic, strong) NSString * points;
@end




