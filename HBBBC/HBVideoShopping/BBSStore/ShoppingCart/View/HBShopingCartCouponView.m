//
//  HBShopingCartCouponView.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/12.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBShopingCartCouponView.h"
#import "HBCartCouponCell.h"
#define SCREEN_WIDTH            ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT           ([[UIScreen mainScreen] bounds].size.height)
const static CGFloat  bottomHeight  =  270;
@interface HBShopingCartCouponView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic , strong)UITableView * tableView;
//主要的Windows
@property (nonatomic, strong) UIWindow *backWindow;
//黑色的view
@property (nonatomic , strong)UIView * drakView;
//白色的view
@property (nonatomic, strong) UIView *bottomView;
//取消button
@property (nonatomic , strong)UIButton * cancelButton;
//@property (nonatomic, strong) UIImageView * imgView;
@property (nonatomic, strong) UILabel * titleLab;
@property (nonatomic, strong) UIButton * confBtn;//确定按钮
@property (nonatomic,assign) NSInteger selectIndex ;    //选中行
@property (nonatomic, strong) UIImageView * imgView;
@property (nonatomic, strong)Promotiontags *selectPromModel;
@end
@implementation HBShopingCartCouponView

- (instancetype)init
{
    self = [super init];
    if (self) {
        // 暗黑色的view
        UIView *darkView = [[UIView alloc] init];
        [darkView setAlpha:0];
        [darkView setUserInteractionEnabled:NO];
        [darkView setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [darkView setBackgroundColor:[UIColor grayColor]];
        [self addSubview:darkView];
        self.drakView = darkView;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
        [darkView addGestureRecognizer:tap];
        
        self.selectIndex = 1000;
        // 所有按钮的底部view
        UIView *bottomView = [[UIView alloc] init];
        [bottomView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:bottomView];
        _bottomView = bottomView;
        
        [bottomView setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, bottomHeight)];
        //添加子视图
        [self addBootomView];
        
        [self setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [self.backWindow addSubview:self];
        
    }
    return self;
}

#pragma mark ==========添加子视图==========
-(void)addBootomView{
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]init];
    //标题
    [_bottomView sd_addSubviews:@[self.titleLab,self.tableView,self.confBtn]];
    
    self.titleLab.sd_layout
    .centerXEqualToView(_bottomView)
    .topSpaceToView(_bottomView, 10)
    .widthIs(SCREEN_WIDTH-40)
    .heightIs(15);
    // 10 + 15
    self.tableView.sd_layout
    .topSpaceToView(self.titleLab, 10)
    .leftSpaceToView(_bottomView, 0)
    .widthIs(SCREEN_WIDTH)
    .heightIs(185);
    // 10 + 185
    self.confBtn.sd_layout
    .bottomSpaceToView(_bottomView, 0)
    .leftSpaceToView(_bottomView, 0)
    .widthIs(SCREEN_WIDTH)
    .heightIs(40);
    
}
- (void)setModel:(GoodsItem *)model{
    _model = model;
    
    //显示优惠券
    for (Promotiontags *promModel in model.goodsItem.promotiontags) {
        if (promModel.selected) {
            _selectPromModel = promModel;
            break;
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark ==========点击确定==========
-(void)clickConfBtn:(UIButton *)button{
    if (self.selectIndex!=1000) {
        //返回优惠
        Promotiontags *model = self.model.goodsItem.promotiontags[self.selectIndex];
        //其它置为不选中
        for (int i=0; i<self.self.model.goodsItem.promotiontags.count; i++) {
            Promotiontags *tempModel =  self.model.goodsItem.promotiontags[i];
            
            tempModel.selected = i==self.selectIndex;
        }
        self.clickShopingCartCoponView(model);
        
        [self dismiss];
    }else{
        self.clickShopingCartCoponView(nil);
        [self dismiss];
    }
    
}

#pragma mark ==========getter==========
- (UIButton *)confBtn{
    if (!_confBtn) {
        _confBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_confBtn setTitleColor:white_Color forState:UIControlStateNormal];
        [_confBtn setBackgroundColor:MAINTextCOLOR];
        [_confBtn addTarget:self action:@selector(clickConfBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confBtn;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc]init];
        _titleLab.text = @"促销活动";
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font = Font(14);
    }
    return _titleLab;
}

#pragma mark - tableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.self.model.goodsItem.promotiontags.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *rid=@"cell";
    HBCartCouponCell *cell=[tableView dequeueReusableCellWithIdentifier:rid];
    if(cell==nil){
        
        cell=[[HBCartCouponCell alloc] initWithStyle:UITableViewCellStyleDefault      reuseIdentifier:rid];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    Promotiontags *model = self.self.model.goodsItem.promotiontags[indexPath.row];
    cell.model = model;
    if (_selectPromModel && [_selectPromModel.promotion_id isEqualToString:model.promotion_id]) {
        cell.isHasSelect = YES;
    }else{
        cell.isHasSelect = NO;
    }
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Promotiontags *model = self.model.goodsItem.promotiontags[indexPath.row] ;
    self.imgView.hidden = YES;
    self.selectPromModel = model;
    self.selectIndex = indexPath.row;
    [self.tableView reloadData] ;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 120, 20)];
    label.text = @"不使用活动促销";
    label.textColor  = black_Color;
    label.font = Font(14);
    
    if (!self.imgView) {
        self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-15-20, 10, 20, 20)];
        self.imgView.image = [UIImage imageNamed:@"bbs_as"];
        self.imgView.hidden = YES;
    }
    self.imgView.hidden = self.selectPromModel;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    [btn addTarget:self action:@selector(clickFooterBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [footer addSubview:label];
    [footer addSubview:self.imgView];
    [footer addSubview:btn];
    
    return footer;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40;
}
#pragma mark ==========点击footer按钮==========
-(void)clickFooterBtn:(UIButton *)button{
    //点击按钮，
    self.imgView.hidden = NO;
    
    //其它置为不选中
    for (int i=0; i<self.self.model.goodsItem.promotiontags.count; i++) {
        
        Promotiontags *tempModel = self.self.model.goodsItem.promotiontags[i] ;
        tempModel.selected = NO ;
        
    }
    self.selectPromModel = nil;
    self.selectIndex = 1000;
    [self.tableView reloadData] ;
}
#pragma mark - 设置tableview的分割线
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // cell的分割线从0坐标开始
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)clickButton:(UIButton *)button{
    
    [self dismiss];
}
- (UIWindow *)backWindow {
    
    if (_backWindow == nil) {
        
        _backWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backWindow.windowLevel       = UIWindowLevelStatusBar;
        _backWindow.backgroundColor   = [UIColor clearColor];
        _backWindow.hidden = NO;
    }
    
    return _backWindow;
}
- (void)dismiss:(UITapGestureRecognizer *)tap {
    
    [self dismiss];
}

-(void)dismiss{
    [UIView animateWithDuration:0.3f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         [self.drakView setAlpha:0];
                         [self.drakView setUserInteractionEnabled:NO];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = SCREEN_HEIGHT;
                         [_bottomView setFrame:frame];
                         
                         CGRect cancelFrame = _cancelButton.frame;
                         cancelFrame.origin.y = -40;
                         [_cancelButton setFrame:cancelFrame];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         _backWindow.hidden = YES;
                         
                         [self removeFromSuperview];
                     }];
    
    
}


- (void)show {
    
    _backWindow.hidden = NO;
    
    [UIView animateWithDuration:.75f
                          delay:0.2
         usingSpringWithDamping:0.65f
          initialSpringVelocity:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.drakView setAlpha:0.4f];
                         [self.drakView setUserInteractionEnabled:YES];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = SCREEN_HEIGHT-bottomHeight;
                         [_bottomView setFrame:frame];
                         
                         CGRect cancelFrame = _cancelButton.frame;
                         cancelFrame.origin.y = frame.origin.y/2;
                         [_cancelButton setFrame:cancelFrame];
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
}


@end

