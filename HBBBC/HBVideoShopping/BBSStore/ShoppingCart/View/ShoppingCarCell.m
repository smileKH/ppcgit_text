//
//  ShoppingCarCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/5.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShoppingCarCell.h"

@implementation ShoppingCarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.apview.layer.masksToBounds = YES;
    self.apview.layer.cornerRadius = 4;
    self.apview.layer.borderWidth = 1;
    self.apview.layer.borderColor = MAINBGCOLOR.CGColor;
    
    self.couponView.layer.borderColor = MAINBGCOLOR.CGColor;
    self.couponView.layer.borderWidth = 1;
    self.couponView.layer.cornerRadius = 4;
    //添加手势
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction4)];
    [self.couponView addGestureRecognizer:tap4];
}
#pragma mark ==========点击手势==========
-(void)tapAction4{
    self.clickCouponView(self.model);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.backgroundColor = white_Color;
    // Configure the view for the selected state
}

- (void)setModel:(GoodsItem *)model
{
    _model = model;
    
    [self.pic sd_setImageWithURL:[NSURL URLWithString:model.goodsItem.image_default_id] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    self.title.text = model.goodsItem.title;
    self.detail.text = model.goodsItem.spec_info;
    self.number.text = integerString(model.goodsItem.quantity);
    self.price.text = [NSString stringWithFormat:@"¥ %0.2f",[model.goodsItem.price.price doubleValue]];
    self.selectBtn.selected = model.goodsItem.is_checked;
    
    //判断有没有促销活动 如果有，那么隐藏规格，显示
    if ([HBHuTool judgeArrayIsEmpty:model.goodsItem.promotiontags]) {
        //有
        self.detail.hidden = YES;
        self.couponView.hidden = NO;
        //显示优惠券
        self.couponLab.text = @"不使用活动促销";
        for (Promotiontags *promModel in model.goodsItem.promotiontags) {
            if (promModel.selected){
                self.couponLab.text = [NSString stringWithFormat:@"%@",promModel.promotion_name];
            }
//            if ([promModel.promotion_id integerValue]==[model.goodsItem.selected_promotion integerValue]) {
//                self.couponLab.text = [NSString stringWithFormat:@"%@",promModel.promotion_name];
//            }else
        }
        
    }else{
        self.detail.hidden  = NO;
        self.couponView.hidden = YES;
    }
    
    
}

- (void)setGoodsItemModel:(HBCartOfGoodsItemModel *)goodsItemModel{
    _goodsItemModel = goodsItemModel;
    if ([HBHuTool isJudgeString:goodsItemModel.image_default_id]) {
        [self.pic sd_setImageWithURL:[NSURL URLWithString:goodsItemModel.image_default_id]];
    }
    self.title.text = goodsItemModel.goodsName;
    self.detail.text = goodsItemModel.goodsName;
    self.number.text = integerString(goodsItemModel.quantity);
    
    self.price.text = [NSString stringWithFormat:@"¥ %0.2f",[goodsItemModel.money doubleValue]];
    self.selectBtn.selected = goodsItemModel.is_checked;
    self.detail.hidden  = NO;
    self.couponView.hidden = YES;
}


- (IBAction)selectAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        self.model.goodsItem.is_checked = sender.selected;
        self.selectModel(sender.selected, self.model);
    }else{
        //还没登录
        self.goodsItemModel.is_checked = sender.selected;
        self.selectOfLineModel(sender.selected, self.goodsItemModel);
    }
    
}
- (IBAction)disAction:(UIButton *)sender {
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        if (self.model.goodsItem.quantity <= 1) {
            //不能小于1
            return;
        }
        self.model.goodsItem.quantity --;
        [self updataUI];
        self.selectModel(self.selectBtn.selected, self.model);//此处仅仅是为了 出发计算金钱
    }else{
        //还没登录
        if (self.goodsItemModel.quantity <= 1) {
            //不能小于1
            return;
        }
        self.goodsItemModel.quantity --;
        [self updataOfLineUI];
        self.selectOfLineModel(self.selectBtn.selected, self.goodsItemModel);//此处仅仅是为了 出发计算金钱
    }
   
}

- (IBAction)addAction:(UIButton *)sender {
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        self.model.goodsItem.quantity ++;
        [self updataUI];
        self.selectModel(self.selectBtn.selected, self.model);//此处仅仅是为了 出发计算金钱
    }else{
        //还没登录
        self.goodsItemModel.quantity ++;
        [self updataOfLineUI];
        self.selectOfLineModel(self.selectBtn.selected, self.goodsItemModel);//此处仅仅是为了 出发计算金钱
    }
    
}
- (void)updataUI
{
    Price *price = self.model.goodsItem.price;
    price.total_price = floatString(self.model.goodsItem.quantity *([price.price floatValue] - [price.discount_price floatValue]));//
    
    self.number.text = integerString(self.model.goodsItem.quantity);
}

- (void)updataOfLineUI
{
    //这里是为了方便计算钱
    Price *price = self.goodsItemModel.price;
    price.total_price = floatString(self.goodsItemModel.quantity *([price.price floatValue] - [price.discount_price floatValue]));//
    self.number.text = integerString(self.goodsItemModel.quantity);
}
@end
