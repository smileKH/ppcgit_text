//
//  ShoppingCarFooter.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/19.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShoppingCarFooter.h"

@implementation ShoppingCarFooter

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//- (void)setTotalModel:(HBTotalShopModel *)totalModel{
//    _totalModel = totalModel;
//    self.costs.text = [NSString stringWithFormat:@"小计：¥ %.02f",[totalModel.payment doubleValue]- [totalModel.post_fee doubleValue]];
//}
- (void)setModel:(Cartlist *)model
{
    _model = model;
    
    self.costs.text = [NSString stringWithFormat:@"小计：¥ %.02f",[model.cartCount.total_fee doubleValue]];
    
}

-(void)setShopModel:(HBCartOfShopItmModel *)shopModel{
    _shopModel = shopModel;
    self.costs.text = [NSString stringWithFormat:@"小计：¥ %@",floatString(shopModel.shopMoney)];
}
@end
