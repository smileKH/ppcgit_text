//
//  ShoppingCarFooter.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/19.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCarModel.h"

@interface ShoppingCarFooter : UITableViewHeaderFooterView
@property (strong, nonatomic) IBOutlet UILabel *costs;
@property (nonatomic, strong) Cartlist *model;
@property (nonatomic, strong) HBCartOfShopItmModel * shopModel;
@end
