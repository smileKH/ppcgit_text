//
//  ShoppingCarHeader.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShoppingCarHeader.h"
@interface ShoppingCarHeader ()
{
    UIButton *_selectBtn;
    UIImageView *_shopImageView;
    UILabel *_shopTitleLabel;
}
@end
@implementation ShoppingCarHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews {
    [super layoutSubviews];
    
    
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        _selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectBtn setBackgroundImage:[UIImage imageNamed:@"list_unchoose"] forState:UIControlStateNormal];
        [_selectBtn addTarget:self action:@selector(selectedAll:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_selectBtn];
        
        _shopImageView = [[UIImageView alloc] init];
        _shopImageView.image = [UIImage imageNamed:@"shopImage"];
        [self.contentView addSubview:_shopImageView];
        
        _shopTitleLabel = [[UILabel alloc] init];
        _shopTitleLabel.font = [UIFont systemFontOfSize:14.0];
        [self.contentView addSubview:_shopTitleLabel];
    }
    return self;
}

- (void)selectedAll:(UIButton *)btn {
    btn.selected = !btn.selected;
    [self setSelectedBtnImg:btn.selected];
 
}
- (void)setSelectedBtnImg:(BOOL)slected {
    
}

- (void)setModel:(Cartlist *)model
{
    _model = model;
    
    self.label.text = model.shop_name;
    self.selectBtn.selected = model.is_checked;
    if ([model.hasCoupon isEqualToString:@"1"]) {//有券
        self.couponBtn.hidden = NO;
    }else{
        self.couponBtn.hidden = YES;
    }
}

- (void)setShopModel:(HBCartOfShopItmModel *)shopModel{
    _shopModel = shopModel;
    
    self.label.text = shopModel.shopName;
    self.selectBtn.selected = shopModel.is_checked;
    if ([shopModel.hasCoupon isEqualToString:@"1"]) {//有券
        self.couponBtn.hidden = NO;
    }else{
        self.couponBtn.hidden = YES;
    }
}


- (IBAction)selectAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        self.model.is_checked = sender.selected;
        self.selectModel(sender.selected, self.model);
    }else{
        //还没有登录
        self.shopModel.is_checked = sender.selected;
        self.selectOfLineModel(sender.selected, self.shopModel);
    }
    
}
- (IBAction)clickCouponAction:(UIButton *)sender {
    self.clickCouponBtn(self.model);
}

@end
