//
//  ShoppingCarHeader.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCarModel.h"
@interface ShoppingCarHeader : UITableViewHeaderFooterView
@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) Cartlist *model;
@property (strong, nonatomic) IBOutlet UIButton *selectBtn;
- (IBAction)selectAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *couponBtn;

@property (nonatomic, strong) void(^selectModel)(BOOL ischeck, Cartlist *selectmodel);

@property (nonatomic, strong) void(^clickCouponBtn)(Cartlist *selectmodel);

@property (nonatomic, strong) HBCartOfShopItmModel * shopModel;
@property (nonatomic, strong) void(^selectOfLineModel)(BOOL ischeck, HBCartOfShopItmModel *selectmodel);
@end
