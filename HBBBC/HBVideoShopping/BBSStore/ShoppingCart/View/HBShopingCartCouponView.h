//
//  HBShopingCartCouponView.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/12.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBShopingCartCouponView : UIView
@property (nonatomic, copy)void(^clickShopingCartCoponView)(Promotiontags *model);
- (void)show;
@property (nonatomic, strong) GoodsItem * model;
@end
