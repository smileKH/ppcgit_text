//
//  HBCartCouponCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/12.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCartCouponCell.h"
@interface HBCartCouponCell()

@end
@implementation HBCartCouponCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //
        [self setUpUI];
    }
    return self;
}
- (void)setUpUI{
    
    [self sd_addSubviews:@[self.fullLab,self.detailLab,self.imgView]];
    
    CGFloat width = SCREEN_WIDTH - 30-40-40-25;
    self.fullLab.sd_layout
    .centerYEqualToView(self)
    .leftSpaceToView(self, 30)
    .widthIs(40)
    .heightIs(25);
    
    self.detailLab.sd_layout
    .centerYEqualToView(self)
    .leftSpaceToView(self.fullLab, 5)
    .widthIs(width)
    .heightIs(20);
    
    
    self.imgView.sd_layout
    .centerYEqualToView(self)
    .rightSpaceToView(self, 15)
    .widthIs(17)
    .heightIs(22);
    
}

- (UILabel *)fullLab{
    if (!_fullLab) {
        _fullLab = [[UILabel alloc]init];
        _fullLab.text = @"满减";
        _fullLab.layer.borderWidth = 1;
        _fullLab.layer.borderColor = MAINTextCOLOR.CGColor;
        _fullLab.layer.cornerRadius = 2;
        _fullLab.font = Font(14);
        _fullLab.textAlignment = NSTextAlignmentCenter;
        _fullLab.textColor = MAINTextCOLOR;
    }
    return _fullLab;
}
- (UILabel *)detailLab{
    if (!_detailLab) {
        _detailLab = [[UILabel alloc]init];
        _detailLab.text = @"满2减1";
        
        _detailLab.font = Font(14);
    }
    return _detailLab;
}

- (UIImageView *)imgView{
    if (!_imgView) {
        _imgView = [[UIImageView alloc]init];
        _imgView.image = [UIImage imageNamed:@"bbs_as"];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgView;
}

- (void)setModel:(Promotiontags *)model{
    _model = model;
    _fullLab.text = model.promotion_tag;
    _detailLab.text = model.promotion_name;
//    if (model.selected) {
//        self.imgView.hidden =NO;
//    }else{
//        self.imgView.hidden = YES;
//    }
}
-(void)setIsHasSelect:(BOOL)isHasSelect{
    _isHasSelect = isHasSelect;
    self.imgView.hidden = !isHasSelect;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
