//
//  HBCartCouponCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/12.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBCartCouponCell : UITableViewCell
@property (nonatomic, strong) UILabel * fullLab;
@property (nonatomic, strong) UILabel * detailLab;
@property (nonatomic, strong) UIImageView * imgView;
@property (nonatomic, strong) Promotiontags * model;
@property (nonatomic, assign) BOOL isHasSelect;

@end
