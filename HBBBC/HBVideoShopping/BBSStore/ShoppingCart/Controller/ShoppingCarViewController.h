//
//  ShoppingCarViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/4.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingCarViewController : HBRootViewController
@property (weak, nonatomic) IBOutlet UIView *bottomView;
- (IBAction)payAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *payView;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIView *tableHeader;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)loginAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UILabel *totalMaoeyLabel;
- (IBAction)seletAll:(UIButton *)sender;
@property (nonatomic, strong) IBOutlet UIButton *allBtn;
- (IBAction)delectAction:(UIButton *)sender;



@end
