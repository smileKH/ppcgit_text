//
//  HBPayListModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/6/12.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBPayListpaymentModel;
@class HBPayListContentModel;
@interface HBPayListModel : NSObject
@property (nonatomic, strong) NSArray *list;
@property (nonatomic, strong) HBPayListpaymentModel * payment;
@property (nonatomic, assign) BOOL  hasDepositPassword;
@end

@interface HBPayListContentModel : NSObject
@property (nonatomic, strong) NSString * app_name;
@property (nonatomic, strong) NSString * app_staus;
@property (nonatomic, strong) NSString * app_id;
@property (nonatomic, strong) NSString * app_rpc_id;
@property (nonatomic, strong) NSString * app_class;
@property (nonatomic, strong) NSString * app_des;

@property (nonatomic, strong) NSString * app_pay_type;
@property (nonatomic, strong) NSString * app_display_name;
@property (nonatomic, strong) NSString * app_pay_brief;
@property (nonatomic, strong) NSString * app_order_by;
@property (nonatomic, strong) NSString * app_info;
@property (nonatomic, strong) NSString * support_cur;
@property (nonatomic, strong) NSString * pay_fee;

@property (nonatomic, strong) NSString * platform;
@property (nonatomic, strong) NSString * def_payment;
@property (nonatomic, strong) NSString * img;
@end

@interface HBPayListpaymentModel : NSObject
@property (nonatomic, strong) NSString * payment_id;
@property (nonatomic, strong) NSString * money;
@property (nonatomic, strong) NSString * cur_money;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * user_name;

@property (nonatomic, strong) NSString * pay_type;
@property (nonatomic, strong) NSString * pay_app_id;
@property (nonatomic, strong) NSString * pay_name;
@property (nonatomic, strong) NSString * payed_time;
@property (nonatomic, strong) NSString * op_id;
@property (nonatomic, strong) NSString * op_name;

@end
