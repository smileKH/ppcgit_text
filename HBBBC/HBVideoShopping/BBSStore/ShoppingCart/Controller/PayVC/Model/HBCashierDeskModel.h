//
//  HBCashierDeskModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/6/12.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBCashierDeskModel : NSObject
//支付宝
@property (nonatomic,copy) NSString *josonStr ;
//以下是微信
@property (nonatomic,copy) NSString *appid ;
@property (nonatomic,copy) NSString *noncestr ;
@property (nonatomic,copy) NSString *package ;
@property (nonatomic,copy) NSString *partnerid ;
@property (nonatomic,copy) NSString *prepayid ;
@property (nonatomic,copy) NSString *sign ;
@property (nonatomic,copy) NSString *timestamp ;
@end
