//
//  PayViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/10.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "PayViewController.h"

@interface PayViewController ()
@property (nonatomic, strong) NSArray *tablearray;
@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
@property (weak, nonatomic) IBOutlet UIButton *threeBtn;

@end

@implementation PayViewController
- (void)backBtnClicked{
    //判断从哪里来的
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"确认要离开收银台?" message:@"您的订单在一天内将被取消，请尽快完成支付" delegate:self cancelButtonTitle:@"继续支付" otherButtonTitles:@"确认离开", nil];
    alert.tag = 1002;
    alert.delegate = self;
    [alert show];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"订单支付";
    UISQURE
    
}
- (void)setPayMoney:(NSString *)payMoney{
    _payMoney = payMoney;
    self.moneyLab.text = [NSString stringWithFormat:@"￥%@",payMoney];
}


- (IBAction)selectOneBtn:(UIButton *)sender {
    self.oneBtn.selected = YES;
    self.twoBtn.selected = NO;
    self.threeBtn.selected = NO;
}
- (IBAction)selectTwoBtn:(UIButton *)sender {
    self.oneBtn.selected = NO;
    self.twoBtn.selected = YES;
    self.threeBtn.selected = NO;
}
- (IBAction)selectThreeBtn:(UIButton *)sender {
    self.oneBtn.selected = NO;
    self.twoBtn.selected = NO;
    self.threeBtn.selected = YES;
}
- (IBAction)clickPayBtn:(UIButton *)sender {
    //点击支付
    [app showToastView:@"点击支付按钮"];
//    [LBService post:<#(NSString *)#> params:<#(NSDictionary<NSString *,id> *)#> completion:<#^(LBResponse *response)completion#>]
}


-(void)addAlertMessage:(NSString *)message{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alert.delegate = self;
    alert.tag = 1001;
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1001) {
        //跳回去
        [self backControllerView];
    }
    else if (alertView.tag==1002){
        
        if (buttonIndex==0) {
            
        }
        else{
            
            //跳回去
            [self backControllerView];
        }
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"HBCashierDeskVC" object:nil];
    
}
#pragma mark - 支付成功跳回去
-(void)backControllerView{
    //从哪里来回哪里去
    if([self.controllerClass isEqualToString:@"MyOrderViewController"]){
        //我的订单--收银台--我的订单
        //发一个通知
        [[NSNotificationCenter defaultCenter]postNotificationName:@"MyOrderViewController" object:nil];
        [self.navigationController popViewControllerAnimated:YES];//返回我的订单
    }
    else if([self.controllerClass isEqualToString:@"HBShopingCartVC"]){
        //购物车结算--确认订单--收银台--订单详情--收银台--订单详情--购物车
        HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
        vc.tidStr = self.tid;//订单号
        vc.controllerClass = @"HBShopingCartVC";//返回购物车
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if([self.controllerClass isEqualToString:@"HBGoodsDetailViewController"]){
        //立即购买--确认订单--收银台--订单详情--收银台--订单详情--商品详情
        HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
        vc.tidStr = self.tid;//订单号
        vc.controllerClass = @"HBGoodsDetailViewController";//返回商品详情
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if([self.controllerClass isEqualToString:@"HBWebManageVC"]){
        //立即购买--确认订单--收银台--订单详情--收银台--订单详情--商品详情
        HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
        vc.tidStr = self.tid;//订单号
        vc.controllerClass = @"HBWebManageVC";//返回商品详情
        [self.navigationController pushViewController:vc animated:YES];
    }else if([self.controllerClass isEqualToString:@"HBMeVC"]){
        //我的订单--订单详情--收银台--订单详情--我的订单
        HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
        vc.tidStr = self.tid;//订单号
        vc.controllerClass = @"3";//返回我的订单
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
    }
}


@end
