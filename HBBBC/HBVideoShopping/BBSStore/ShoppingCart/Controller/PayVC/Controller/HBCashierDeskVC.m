//
//  HBCashierDeskVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/7.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCashierDeskVC.h"
#import "HBCashierDeskModel.h"
@interface HBCashierDeskVC ()

@end

@implementation HBCashierDeskVC
- (void)backBtnClicked{
    //判断从哪里来的
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"确认要离开收银台?" message:@"您的订单在一天内将被取消，请尽快完成支付" delegate:self cancelButtonTitle:@"继续支付" otherButtonTitles:@"确认离开", nil];
    alert.tag = 1002;
    alert.delegate = self;
    [alert show];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"订单支付";
    //设置UI
    [self setupUI];
    //获取支付列表
//    [self requestCreateOrder];
    
    self.moneyLab.text = [NSString stringWithFormat:@"￥%@",self.payMoney];
    //注册微信支付通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxpayresult:) name:WEAL_WEIXIN_PAYNotice object:nil];
    //注册支付宝支付通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alipayresult:) name:WEAL_PAY_PAYNOTICE object:nil];
}
//实现接收微信支付通知的方法
- (void)wxpayresult:(NSNotification *)notification
{
    NSString *result = notification.userInfo[@"payresult"];
    if ([result isEqualToString:@"success"]) {
        //跳转到完善店铺资料
        [MBProgressHUD showSuccessMessage:@"支付成功"];
        //跳回去
        WEAKSELF;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf backControllerView];
        });
    }else{
        [MBProgressHUD showErrorMessage:@"支付失败"];
    }
}

//实现接收支付宝支付通知的方法
- (void)alipayresult:(NSNotification *)notification
{
    NSString *result = notification.userInfo[@"payresult"];
    if ([result isEqualToString:@"success"]) {
        [MBProgressHUD showSuccessMessage:@"支付成功"];
        //跳转到完善店铺资料
        WEAKSELF;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf backControllerView];
        });
    }else{
        [MBProgressHUD showErrorMessage:@"支付失败"];
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:WEAL_WEIXIN_PAYNotice object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:WEAL_PAY_PAYNOTICE object:nil];
}
#pragma mark ==========创建订单==========
-(void)requestCreateOrder{
    //创建订单
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"tid"] = self.payment_id;
    [LBService post:PAYMENT_PAY_CREATE params:parameters completion:^(LBResponse *response) {
        if (response.succeed) {
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSString *payment_id = dic[@"payment_id"];
                // 调模拟接口
                //                [weakSelf requestPayOrder:payment_id];
                weakSelf.payment_id = payment_id;
                //获取支付方式列表
                [weakSelf requestPayList:payment_id];
            }
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========获取支付列表==========
-(void)requestPayList:(NSString *)payment_id{
//    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payment_id"] = payment_id;
    [LBService post:PAYMENT_PAY_PAYCENTER params:parameters completion:^(LBResponse *response) {
        if (response.succeed) {
            // 调模拟接口
//            [weakSelf AlertWithTitle:@"提示" message:@"支付成功" andOthers:@[@"确定"] animated:YES action:^(NSInteger index) {
//                //确定
//                [weakSelf backControllerView];
//            }];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========初始化界面==========
-(void)setupUI{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction1)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction2)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction3)];
    [self.view1 addGestureRecognizer:tap1];
    [self.view2 addGestureRecognizer:tap2];
    [self.view3 addGestureRecognizer:tap3];
    self.oneBtn.userInteractionEnabled = NO;
    self.twoBtn.userInteractionEnabled = NO;
//    self.threeBtn.userInteractionEnabled = NO;
    //设置默认值
    self.oneBtn.selected = YES;
    self.payment_type = @"wxpayApp";
}

-(void)tapAction1{//微信支付
    self.oneBtn.selected = YES;
    self.twoBtn.selected = NO;
    self.payment_type = @"wxpayApp";
//    self.threeBtn.selected = NO;
}
-(void)tapAction2{//支付宝支付支付
    self.oneBtn.selected = NO;
    self.twoBtn.selected = YES;
    self.payment_type = @"alipayApp";
//    self.threeBtn.selected = NO;
}
-(void)tapAction3{
    self.oneBtn.selected = NO;
    self.twoBtn.selected = NO;
//    self.threeBtn.selected = YES;
}


-(void)addAlertMessage:(NSString *)message{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alert.delegate = self;
    alert.tag = 1001;
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1001) {
        //跳回去
        [self backControllerView];
    }
    else if (alertView.tag==1002){
        
        if (buttonIndex==0) {
            
        }
        else{
            
            //跳回去
            [self backControllerView];
        }
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"HBCashierDeskVC" object:nil];
    
}
#pragma mark - 支付成功跳回去
-(void)backControllerView{
    //从哪里来回哪里去
    if([self.controllerClass isEqualToString:@"MyOrderViewController"]){
        //我的订单--收银台--我的订单
        //发一个通知
        [[NSNotificationCenter defaultCenter]postNotificationName:@"MyOrderViewController" object:nil];
        [self.navigationController popViewControllerAnimated:YES];//返回我的订单
    }
    else if([self.controllerClass isEqualToString:@"ShoppingCarViewController"]){
        //购物车结算--确认订单--收银台--订单详情--收银台--订单详情--购物车
        HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
        vc.tidStr = self.tid;//订单号
        vc.controllerClass = @"ShoppingCarViewController";//返回购物车
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if([self.controllerClass isEqualToString:@"ProductFirstViewController"]){
        //立即购买--确认订单--收银台--订单详情--收银台--订单详情--商品详情
        HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
        vc.tidStr = self.tid;//订单号
        vc.controllerClass = @"ProductFirstViewController";//返回商品详情
        [self.navigationController pushViewController:vc animated:YES];
    }else if([self.controllerClass isEqualToString:@"HBMeVC"]){
        //订单详情--收银台--订单详情--我的订单
        HBMyOrderDetailVC *vc = [[HBMyOrderDetailVC alloc]init];
        vc.tidStr = self.tid;//订单号
        vc.controllerClass = @"3";//返回我的订单
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark ==========点击支付按钮==========
- (IBAction)clickPayButton:(UIButton *)sender {
    //    [app showToastView:@"暂不支持支付"];
    [self requestPayOrder:nil];
}
-(void)requestPayOrder:(NSString *)payment_id{
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payment_id"] = self.payment_id;
    parameters[@"pay_app_id"] = self.payment_type;//wxpayApp alipayApp
    [LBService post:PAYMENT_PAY_DODO params:parameters completion:^(LBResponse *response) {
        if (response.succeed) {
            // 调模拟接口

            if ([weakSelf.payment_type isEqualToString:@"wxpayApp"]) {
                HBCashierDeskModel *model = [HBCashierDeskModel mj_objectWithKeyValues:response.result] ;
                //微信支付
                [weakSelf wxPayRequest:model] ;
            }else{
                //支付宝支付
                NSDictionary *dic = response.result[@"data"];
                if (ValidDict(dic)) {
                    [weakSelf aliPayRequest:dic[@"orderInfo"]] ;
                }
                
            }
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========微信支付==========
-(void)wxPayRequest:(HBCashierDeskModel *) model{
    PayReq *request = [[PayReq alloc] init] ;
    request.partnerId = model.partnerid ;
    request.prepayId= model.prepayid ;
    request.package = model.package ;
    request.nonceStr= model.noncestr;
    request.timeStamp = [model.timestamp intValue];
    request.sign= model.sign;
    [WXApi sendReq:request];
}
#pragma mark ==========支付宝支付==========
-(void)aliPayRequest:(NSString *)orderInfo{
    WEAKSELF;
    // NOTE: 调用支付结果开始支付
    [[AlipaySDK defaultService] payOrder:orderInfo fromScheme:kAppScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
        NSString *resultStatus = resultDic[@"resultStatus"];
        if ([resultStatus isEqualToString:@"9000"]) {
            [MBProgressHUD showSuccessMessage:@"支付成功"];
            //转跳
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf backControllerView];
            });
        }else{
            [MBProgressHUD showErrorMessage:@"支付失败"];
        }
    }];
}
@end
