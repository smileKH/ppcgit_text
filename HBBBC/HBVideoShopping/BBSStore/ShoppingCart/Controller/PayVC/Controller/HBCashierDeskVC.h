//
//  HBCashierDeskVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/7.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBCashierDeskVC : HBRootViewController
@property (nonatomic, strong) NSString *payMoney;//支付金额
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
//@property (weak, nonatomic) IBOutlet UIButton *threeBtn;
@property (nonatomic, strong) NSString * payment_type;//支付类型
@property (nonatomic, strong) NSString * payment_id;//支付id
@property (nonatomic, strong) NSString * tid;//订单id
@property (nonatomic ,strong)NSString *controllerClass;//从哪里来的

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
- (IBAction)clickPayButton:(UIButton *)sender;

@end
