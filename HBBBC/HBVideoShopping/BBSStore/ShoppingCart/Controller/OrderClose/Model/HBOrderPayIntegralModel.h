//
//  HBOrderPayIntegralModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/14.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBOrderPayIntegralModel : NSObject
@property (nonatomic, strong) NSString * points;
@property (nonatomic, strong) NSString * point_deduction_max;
@property (nonatomic, strong) NSString * point_deduction_rate;
@property (nonatomic, strong) NSString * open_point_deduction;
@property (nonatomic, strong) NSString * use_points;//可用积分
@property (nonatomic, strong) NSString * discount_price;//可用金额
@end
