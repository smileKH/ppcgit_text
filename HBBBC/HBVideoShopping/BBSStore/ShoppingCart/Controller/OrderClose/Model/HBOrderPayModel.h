//
//  HBOrderPayModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/13.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductDetailModel.h"
#import "ShoppingCarModel.h"
@class PayType;
@class UserPoint;
@class CartInfo;
@class Invoice;
@class Default_address;
@class Total;
@class Invoice_vat;
@class ResultCartData;
@class Shipping;
@class CartByDlytmpl;
@class HBCartCount;
@class Items;
@class CatItemPrice;
@class CartCount;
@class Price;
@class Promotions;
@class HBCouponlist;
@interface HBOrderPayModel : NSObject
@property (nonatomic, strong) NSString * isSelfShop;
@property (nonatomic, strong) NSString * ifOpenOffline;
@property (nonatomic, strong) NSString * md5_cart_info;
@property (nonatomic, strong) PayType * payType;
@property (nonatomic, strong) Default_address * default_address;
@property (nonatomic, strong) Total * total;
@property (nonatomic, strong) UserPoint * userPoint;
@property (nonatomic, strong) CartInfo * cartInfo;
@property (nonatomic, strong) Invoice * invoice;
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@end

@interface PayType : NSObject
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * pay_type;
@end


@interface Default_address : NSObject
@property (nonatomic, strong) NSString * addr_id;
@property (nonatomic, strong) NSString * area;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * area_id;
@property (nonatomic, strong) NSString * addr;
@property (nonatomic, strong) NSString * zip;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * tel;
@property (nonatomic, strong) NSString * def_addr;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * addrdetail;
@end

@interface Total : NSObject
@property (nonatomic, strong) NSString * allCostFee;
@property (nonatomic, strong) NSString * allPostfee;
@property (nonatomic, strong) NSString * allPayment;
@property (nonatomic, strong) NSString * disCountfee;
@end


@interface UserPoint : NSObject
@property (nonatomic, strong) NSString * points;
@property (nonatomic, strong) NSString * point_deduction_max;
@property (nonatomic, strong) NSString * point_deduction_rate;
@property (nonatomic, strong) NSString * open_point_deduction;
@end

@interface CartInfo : NSObject
@property (nonatomic, strong) NSArray * resultCartData;
@property (nonatomic, strong) CatItemPrice * catItemPrice;
@end


@interface Invoice : NSObject
@property (nonatomic, strong) Invoice_vat * invoice_vat;
@property (nonatomic, strong) NSString * invoice_title;
@property (nonatomic, strong) NSString * invoice_content;
@property (nonatomic, strong) NSString * invoice_type;
@end





@interface Invoice_vat : NSObject
@property (nonatomic, strong) NSString * company_name;
@property (nonatomic, strong) NSString * company_phone;
@property (nonatomic, strong) NSString * bankname;
@property (nonatomic, strong) NSString * bankaccount;
@property (nonatomic, strong) NSString * company_address;
@property (nonatomic, strong) NSString * registration_number;
@end


@interface ResultCartData : NSObject
@property (nonatomic, strong) NSString * shop_id;
@property (nonatomic, strong) NSString * shop_name;
@property (nonatomic, strong) NSString * shop_type;
@property (nonatomic, strong) CartCount * cartCount;
@property (nonatomic, strong) NSArray * cartByDlytmpl;
@property (nonatomic, strong) NSString * totalItem;
@property (nonatomic, strong) NSArray * items;
@property (nonatomic, strong) Shipping * shipping;
@property (nonatomic, strong) NSString * ziti;
@property (nonatomic, strong) NSArray * couponlist;
@property (nonatomic, strong) NSString * memo;//买家留言
@end

@interface Shipping : NSObject
@property (nonatomic, strong) NSString * shipping_type;
@property (nonatomic, strong) NSString * shipping_name;
@end

@interface CartByDlytmpl : NSObject
@property (nonatomic, strong) NSString * total_quantity;
@property (nonatomic, strong) NSString * total_weight;
@property (nonatomic, strong) NSString * total_price;
@property (nonatomic, strong) NSString * dlytmpl_id;
@end

@interface HBCartCount : NSObject
@property (nonatomic, strong) NSString * total_weight;
@property (nonatomic, strong) NSString * itemnum;
@property (nonatomic, strong) NSString * total_fee;
@property (nonatomic, strong) NSString * total_discount;
@property (nonatomic, strong) NSString * payment;
@property (nonatomic, strong) NSString * obtain_point_fee;
@property (nonatomic, strong) NSString * post_fee;
@end

@interface Items : NSObject
@property (nonatomic, strong) NSString * cart_id;
@property (nonatomic, strong) NSString * obj_type;
@property (nonatomic, strong) NSString * item_id;
@property (nonatomic, strong) NSString * sku_id;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * selected_promotion;
@property (nonatomic, strong) NSString * cat_id;

@property (nonatomic, strong) NSString * sub_stock;
@property (nonatomic, strong) NSString * spec_info;
@property (nonatomic, strong) NSString * bn;
@property (nonatomic, strong) NSString * dlytmpl_id;

@property (nonatomic, strong) NSString * store;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) Price * price;///模型
@property (nonatomic, strong) NSString * quantity;

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * image_default_id;
@property (nonatomic, strong) NSString * weight;
@property (nonatomic, strong) NSString * valid;

@property (nonatomic, strong) NSString * is_checked;
@property (nonatomic, strong) NSArray * promotions;
@property (nonatomic, strong) NSString * promotion_type;
@property (nonatomic, strong) NSString * promotion_id;
@property (nonatomic, strong) NSString * cat_lv1_id;
@property (nonatomic, strong) NSString * gift;
@end

@interface CatItemPrice : NSObject

@end

//@interface CartCount : NSObject
//@property (nonatomic, strong) NSString * total_weight;
//@property (nonatomic, strong) NSString * itemnum;
//@property (nonatomic, strong) NSString * total_fee;
//@property (nonatomic, strong) NSString * total_discount;
//@property (nonatomic, strong) NSString * payment;
//@property (nonatomic, strong) NSString * obtain_point_fee;
//@property (nonatomic, strong) NSString * post_fee;
//@end

//@interface Price : NSObject
//@property (nonatomic, strong) NSString * discount_price;
//@property (nonatomic, strong) NSString * price;
//@property (nonatomic, strong) NSString * total_price;
//@end

@interface Promotions : NSObject
@property (nonatomic, strong) NSString * promotion_id;
@property (nonatomic, strong) NSString * rel_promotion_id;
@property (nonatomic, strong) NSString * promotion_type;

@property (nonatomic, strong) NSString * shop_id;
@property (nonatomic, strong) NSString * promotion_name;
@property (nonatomic, strong) NSString * promotion_tag;

@property (nonatomic, strong) NSString * promotion_desc;
@property (nonatomic, strong) NSString * used_platform;
@property (nonatomic, strong) NSString * start_time;

@property (nonatomic, strong) NSString * end_time;
@property (nonatomic, strong) NSString * created_time;
@property (nonatomic, strong) NSString * check_status;

@property (nonatomic, strong) NSString * tag;
@property (nonatomic, strong) NSString * valid;
@end


@interface HBCouponlist : NSObject
@property (nonatomic, strong) NSString * coupon_code;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * shop_id;

@property (nonatomic, strong) NSString * coupon_id;
@property (nonatomic, strong) NSString * obtain_desc;
@property (nonatomic, strong) NSString * obtain_time;

@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * is_valid;
@property (nonatomic, strong) NSString * used_platform;

@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSNumber * start_time;
@property (nonatomic, strong) NSNumber * end_time;

@property (nonatomic, strong) NSString * canuse_start_time;
@property (nonatomic, strong) NSString * canuse_end_time;

@property (nonatomic, strong) NSString * limit_money;
@property (nonatomic, strong) NSString * deduct_money;
@property (nonatomic, strong) NSString * coupon_name;
@property (nonatomic, strong) NSString * coupon_desc;
@property (nonatomic,assign) BOOL selected ;//是否选择
@end
