//
//  HBOrderPayModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/13.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderPayModel.h"

@implementation HBOrderPayModel

@end



@implementation PayType

@end

@implementation UserPoint

@end

@implementation CartInfo
+ (NSDictionary *)objectClassInArray{
    return @{
             @"resultCartData" : @"ResultCartData"
             };
}
@end

@implementation Invoice

@end

@implementation Default_address

@end

@implementation Total

@end

@implementation Invoice_vat

@end

@implementation ResultCartData
+ (NSDictionary *)objectClassInArray{
    return @{
             @"items" : @"Items",
             @"cartByDlytmpl" : @"CartByDlytmpl",
             @"couponlist" : @"HBCouponlist"
             };
}
@end

@implementation Shipping

@end

@implementation CartByDlytmpl

@end

@implementation HBCartCount

@end

@implementation Items
+ (NSDictionary *)objectClassInArray{
    return @{
             @"promotions" : @"Promotions"
             };
}
@end

@implementation CatItemPrice

@end

//@implementation CartCount
//
//@end

//@implementation Price
//
//@end

@implementation Promotions

@end

@implementation HBCouponlist

@end




