//
//  HBOrderPayStyleModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBOrderPayStyleModel : NSObject
@property (nonatomic, strong) NSString * name;
@property (nonatomic, assign) BOOL isSelect;
@end
