//
//  OrderPayDetailCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPayDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *totalLab;
@property (weak, nonatomic) IBOutlet UILabel *manJianLab;
@property (weak, nonatomic) IBOutlet UILabel *yunFeiLab;
@property (nonatomic, strong) ResultCartData * model;
@end
