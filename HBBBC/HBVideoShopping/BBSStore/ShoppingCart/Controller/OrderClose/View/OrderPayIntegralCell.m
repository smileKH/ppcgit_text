//
//  OrderPayIntegralCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayIntegralCell.h"

@implementation OrderPayIntegralCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickSelectBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.integralSelectBtn(sender.selected);
}
-(void)setModel:(HBOrderPayIntegralModel *)model{
    _model = model;
    if (![HBHuTool isJudgeString:model.point_deduction_max]&&![HBHuTool isJudgeString:model.point_deduction_rate]) {
        
        self.contentLab.text = [NSString stringWithFormat:@"积分抵扣:可用积分%@,抵扣￥%@",model.use_points,model.discount_price];
        
    }else{
        self.contentLab.text = [NSString stringWithFormat:@"积分抵扣:可用积分0,抵扣￥0"];
   
    }
    
    if ([model.use_points doubleValue]<=0.0) {
        self.selectBtn.enabled = NO;
    }else{
        self.selectBtn.enabled = YES;
    }
    
}
@end
