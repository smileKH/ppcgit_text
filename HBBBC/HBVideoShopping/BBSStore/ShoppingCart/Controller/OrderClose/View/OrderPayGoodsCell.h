//
//  OrderPayGoodsCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBOrderPayModel.h"
@interface OrderPayGoodsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UIView *imgScroView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) ResultCartData * model;
@property (nonatomic, copy) void(^clickShopNameBtn)(ResultCartData *model);
@property (nonatomic, copy) void(^clickGoodsScrolviewBtn)(ResultCartData *model);
- (IBAction)clickShopNameBtn:(UIButton *)sender;
- (IBAction)clickGoodsScroView:(UIButton *)sender;

@end
