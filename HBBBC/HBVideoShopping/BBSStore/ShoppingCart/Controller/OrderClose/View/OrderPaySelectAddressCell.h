//
//  OrderPaySelectAddressCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBAddressModel.h"
@interface OrderPaySelectAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UIButton *moRenBtn;
@property (nonatomic, strong) Default_address * model;
@property (nonatomic, copy) void(^clickMoRenBtn)(Default_address *model);
@end
