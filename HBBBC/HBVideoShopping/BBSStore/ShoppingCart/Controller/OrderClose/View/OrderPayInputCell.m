//
//  OrderPayInputCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayInputCell.h"
@interface OrderPayInputCell()<UITextFieldDelegate>


@end
@implementation OrderPayInputCell

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.updatePayInputTextResonTF) {
        self.updatePayInputTextResonTF(textField.text, self.model);
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        
        return FALSE;
    }
    return TRUE;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.resonTF.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
