//
//  OrderPayDetailCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayDetailCell.h"

@implementation OrderPayDetailCell
- (void)setModel:(ResultCartData *)model{
    _model = model;
    
    self.totalLab.text = [NSString stringWithFormat:@"￥%0.2f",[model.cartCount.total_fee doubleValue]];
    self.manJianLab.text = [NSString stringWithFormat:@"-￥%0.2f",[model.cartCount.total_discount doubleValue]];
    self.yunFeiLab.text = [NSString stringWithFormat:@"+￥%0.2f",[model.cartCount.post_fee doubleValue]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
