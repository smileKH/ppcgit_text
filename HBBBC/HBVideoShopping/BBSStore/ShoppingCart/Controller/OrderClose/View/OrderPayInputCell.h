//
//  OrderPayInputCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBOrderPayModel.h"

@interface OrderPayInputCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *resonTF;
@property (nonatomic, strong) ResultCartData * model;
@property (nonatomic, copy) void(^updatePayInputTextResonTF)(NSString *text,ResultCartData *model);
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSIndexPath *indexPath;
@end
