//
//  OrderPaySelectCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBOrderPayModel.h"
@interface OrderPaySelectCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLab;
@property (nonatomic, strong) PayType * model;
@end
