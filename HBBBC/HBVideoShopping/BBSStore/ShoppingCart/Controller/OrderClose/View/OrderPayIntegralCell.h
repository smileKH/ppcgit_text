//
//  OrderPayIntegralCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBOrderPayIntegralModel.h"
@interface OrderPayIntegralCell : UITableViewCell
- (IBAction)clickSelectBtn:(UIButton *)sender;
@property (nonatomic, copy) void(^integralSelectBtn)(BOOL isSelect);
@property (nonatomic, strong) HBOrderPayIntegralModel * model;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@end
