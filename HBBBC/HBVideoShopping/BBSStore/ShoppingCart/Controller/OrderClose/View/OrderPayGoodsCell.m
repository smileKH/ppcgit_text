//
//  OrderPayGoodsCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayGoodsCell.h"

@implementation OrderPayGoodsCell
- (void)setModel:(ResultCartData *)model{
    _model = model;
    self.shopNameLab.text = [NSString stringWithFormat:@"%@",model.shop_name];
    self.numLab.text = [NSString stringWithFormat:@"%@件",model.totalItem];
    //判断，如果只有一件衣服，那么就展示一件
    if ([HBHuTool judgeArrayIsEmpty:model.items]) {
        //有
        UIScrollView *scroView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.imgScroView.width_sd, self.imgScroView.height_sd)];
        scroView.backgroundColor = [UIColor whiteColor];
        CGFloat imgWidth = 70;
        for (NSInteger i=0; i<model.items.count; i++) {
            UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(i*imgWidth+(i+1)*10, 0, imgWidth, imgWidth)];
            imageV.centerY_sd = scroView.centerY_sd;
            Items *items = model.items[i];
            [imageV sd_setImageWithURL:[NSURL URLWithString:items.image_default_id] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
            
            [scroView addSubview:imageV];
        }
        scroView.contentSize = CGSizeMake(model.items.count*imgWidth+10, self.imgScroView.height_sd);
        [self.imgScroView addSubview:scroView];
    }
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgScroView.userInteractionEnabled = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark ==========点击店铺名字==========
- (IBAction)clickShopNameBtn:(UIButton *)sender {
    self.clickShopNameBtn(self.model);
}
#pragma mark ==========点击商品图片==========
- (IBAction)clickGoodsScroView:(UIButton *)sender {
    self.clickGoodsScrolviewBtn(self.model);
}
@end
