//
//  OrderPayCouponCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPayCouponCell : UITableViewCell
@property (nonatomic, strong) HBCouponlist * model;
@property (weak, nonatomic) IBOutlet UILabel *couponQuanLab;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
- (IBAction)clickSelectBtn:(UIButton *)sender;
@property (nonatomic, strong) NSString * shop_name;
@property (nonatomic, copy) void(^clickCouponlistBtn)(HBCouponlist *model);
@end
