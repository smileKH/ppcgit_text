//
//  OrderPayCouponCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayCouponCell.h"

@implementation OrderPayCouponCell
- (void)setModel:(HBCouponlist *)model{
    _model = model;
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.coupon_name];
    NSString *startStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd" tenNumber:model.start_time];
    NSString *endStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd" tenNumber:model.end_time];
    self.dateLab.text = [NSString stringWithFormat:@"优惠券%@-%@",startStr,endStr];
    self.selectBtn.selected = model.selected;
}
- (void)setShop_name:(NSString *)shop_name{
    _shop_name = shop_name;
    self.shopNameLab.text = [NSString stringWithFormat:@"%@",shop_name];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickSelectBtn:(UIButton *)sender {
    //点击选择按钮
    self.clickCouponlistBtn(self.model);
}
@end
