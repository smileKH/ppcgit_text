//
//  OrderPayCouponViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBOrderPayModel.h"
@interface OrderPayCouponViewController : HBRootViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, copy) void(^clickConfButton)(HBCouponlist *listModel);
@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)clickBtn:(UIButton *)sender;
@property (nonatomic, strong) ResultCartData * model;
@property (nonatomic, strong) NSString * isModel;

@end
