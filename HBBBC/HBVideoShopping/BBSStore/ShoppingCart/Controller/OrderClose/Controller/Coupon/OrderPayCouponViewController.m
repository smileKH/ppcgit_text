//
//  OrderPayCouponViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayCouponViewController.h"
#import "OrderPayCouponCell.h"
@interface OrderPayCouponViewController ()
@property (nonatomic,assign) NSInteger selectIndex ;    //选中行
@property (nonatomic, strong) UIButton * headerBtn;
@property (nonatomic, strong) UIView * tableheader;
@end

@implementation OrderPayCouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"优惠券";
    
    [self.tableview registerNib:[UINib nibWithNibName:@"OrderPayCouponCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    [self tableHeader];
    self.headerBtn.selected = YES;
    self.selectIndex=-1;
    self.tableview.tableFooterView = [[UIView alloc]init];
    
    for (int i=0; i<self.model.couponlist.count; i++) {
        HBCouponlist *tempModel = self.model.couponlist[i] ;
        tempModel.selected = NO ;
    }
     [self.tableview reloadData] ;
    //设置初值
    if ([HBHuTool judgeArrayIsEmpty:self.model.couponlist]) {
        //有数据
        [self hideContentNullPromptView];
        self.tableheader.hidden = NO;
    }else{
        [self showContentNullPromptNormal];
        self.tableheader.hidden = YES;
    }
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    OrderPayCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.shop_name = self.model.shop_name;
    cell.model = self.model.couponlist[indexPath.row];
    WEAKSELF;
    cell.clickCouponlistBtn = ^(HBCouponlist *model) {
        [weakSelf selectIndexPath:indexPath];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.model.couponlist.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectIndexPath:indexPath];
}
#pragma mark ==========选择优惠券==========
-(void)selectIndexPath:(NSIndexPath *)indexPath{
    HBCouponlist *model = self.model.couponlist[indexPath.row] ;
    //选中
    model.selected = YES ;
    //其它置为不选中
    for (int i=0; i<self.model.couponlist.count; i++) {
        if (i==indexPath.row) {
            continue ;
        }else{
            HBCouponlist *tempModel = self.model.couponlist[i] ;
            tempModel.selected = NO ;
        }
    }
    self.selectIndex = indexPath.row;
    self.headerBtn.selected = NO;
    [self.tableview reloadData] ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableHeader
{
    self.tableheader =[[ UIView alloc] initWithFrame:CGRectMake( 0 , 0, SCREEN_WIDTH, 45)];
    self.tableheader .backgroundColor = white_Color;
    
    
    UILabel *labe = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH-100, 45)];
    labe.font = [UIFont systemFontOfSize:14];
    labe.text = @"不使用优惠券";
    [self.tableheader addSubview:labe];
    
    
    self.headerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.headerBtn setImage:ImageNamed(@"bbs_yuann") forState:UIControlStateNormal];
    [self.headerBtn setImage:ImageNamed(@"bbs_yuan") forState:UIControlStateSelected];
    self.headerBtn.frame = CGRectMake(SCREEN_WIDTH-40, 13, 21, 22);
    [self.tableheader addSubview:self.headerBtn];
    [self.headerBtn addTarget:self action:@selector(clickHeaderBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableview.tableHeaderView = self.tableheader;
 
}
#pragma mark ==========点击头视图button==========
-(void)clickHeaderBtn:(UIButton *)button{
    button.selected = !button.selected;
    if (button.selected) {
        //其它置为不选中
        for (int i=0; i<self.model.couponlist.count; i++) {
            HBCouponlist *tempModel = self.model.couponlist[i] ;
            tempModel.selected = NO ;
        }
        self.selectIndex = -1;
        //刷新
        [self.tableview reloadData] ;
    }else{
        
    }
}
#pragma mark ==========点击确定按钮==========
- (IBAction)clickBtn:(UIButton *)sender {
    if (self.selectIndex==-1) {
        [app showToastView:@"你还未选择优惠券"];
        return;
    }
    //回调
    HBCouponlist *tempModel = self.model.couponlist[self.selectIndex];
    [self userCoponModel:tempModel];
   
    
}
#pragma mark ==========用户使用优惠券==========
-(void)userCoponModel:(HBCouponlist *)model{
    //用户使用优惠券
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:PRO_SHOP_COUPON_USE params:@{@"coupon_code":model.coupon_code,@"mode":self.isModel,@"platform":@"app"} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            // 如果成功，那么计算价格
            weakSelf.clickConfButton(model);
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}
@end
