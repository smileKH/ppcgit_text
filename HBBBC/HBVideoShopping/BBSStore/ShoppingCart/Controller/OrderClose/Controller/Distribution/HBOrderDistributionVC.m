//
//  HBOrderDistributionVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderDistributionVC.h"
#import "HBOrderDistributionCell.h"
@interface HBOrderDistributionVC ()
@property (nonatomic, strong) NSMutableArray * payStyleArr;
@property (nonatomic, assign) NSInteger  selectIndex;
@end

@implementation HBOrderDistributionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配送方式";
    self.payStyleArr = [NSMutableArray array];
    NSArray *arr = @[@"快递配送"];
    self.selectIndex = 0;
    for (NSInteger i=0; i<arr.count; i++) {
        HBOrderPayStyleModel *model = [HBOrderPayStyleModel new];
        model.name = arr[i];
        if (i==0) {
            model.isSelect = YES;
        }else{
            model.isSelect = NO;
        }
        [self.payStyleArr addObject:model];
    }
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderDistributionCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.tableFooterView = [[UIView alloc]init];
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HBOrderDistributionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.payStyleArr[indexPath.row];
    WEAKSELF;
    cell.clickDistributionBtn = ^(HBOrderPayStyleModel *model) {
        [weakSelf selectIndexPath:indexPath];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.payStyleArr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectIndexPath:indexPath];
}
#pragma mark ==========选择优惠券==========
-(void)selectIndexPath:(NSIndexPath *)indexPath{
    HBOrderPayStyleModel *model = self.payStyleArr[indexPath.row] ;
    //选中
    model.isSelect = YES ;
    //其它置为不选中
    for (int i=0; i<self.payStyleArr.count; i++) {
        if (i==indexPath.row) {
            continue ;
        }else{
            HBOrderPayStyleModel *tempModel = self.payStyleArr[i] ;
            tempModel.isSelect = NO ;
        }
    }
    self.selectIndex = indexPath.row;
    [self.tableView reloadData] ;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)clickBtn:(UIButton *)sender {
    HBOrderPayStyleModel *model = self.payStyleArr[self.selectIndex];
    self.clickDistributionVC(model);
    [self.navigationController popViewControllerAnimated:YES];
}
@end
