//
//  OrderPayAddressViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayAddressViewController.h"
#import "OrderPaySelectAddressCell.h"
#import "AddAddressViewController.h"
#import "HBAddressModel.h"
@interface OrderPayAddressViewController ()

@property (nonatomic, strong) Default_address * selectModel;
@end

@implementation OrderPayAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"选择地址";//结算页选择地址
    
    [self.tableview registerNib:[UINib nibWithNibName:@"OrderPaySelectAddressCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.tableFooterView = [[UIView alloc]init];
    [self setNavBar];
    
    //请求数据
    [self requestAddressModel:YES];
    WEAKSELF;
    //默认block方法：设置下拉刷新
//    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestAddressModel:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestAddressModel:NO];
//    }];
    //添加通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(upadeloadAddress) name:@"AddAddressViewController" object:nil];
}
#pragma mark ==========刷新数据==========
-(void)upadeloadAddress{
    [self requestAddressModel:YES];
}
#pragma mark ==========请求地址==========
-(void)requestAddressModel:(BOOL)isRefresh{
    WEAKSELF;
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:ADDRESS_LIST_MEMBER params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"total"]intValue];
                NSArray *arr = [dic objectForKey:@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [Default_address mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            }
            
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptImg:@"bbs_addressImg" withLabelName:@"您还没有收货地址" withButtonName:@"新增地址"];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}
- (void)setNavBar
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"新增" forState:UIControlStateNormal];
    [button setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(addAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}
#pragma mark ==========点击添加收货地址==========
-(void)clickSpaceButton{
    AddAddressViewController *vc = [[AddAddressViewController alloc] initWithNibName:@"AddAddressViewController" bundle:nil];
    vc.title = @"新增收货地址";
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)addAction{
    AddAddressViewController *vc = [[AddAddressViewController alloc] initWithNibName:@"AddAddressViewController" bundle:nil];
    vc.title = @"新增收货地址";
    [self.navigationController pushViewController:vc animated:YES];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    OrderPaySelectAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.model = self.allData[indexPath.row];
    cell.clickMoRenBtn = ^(Default_address *model) {
        //点击默认
        [self moRenAddress:model];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark ==========设置默认地址==========
-(void)moRenAddress:(Default_address *)model{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:ADDRESS_SET_DEFAULTT params:@{@"addr_id":model.addr_id} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功
            [app showToastView:@"设置成功"];
            //刷新界面
            [weakSelf requestAddressModel:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return self.allData.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Default_address *model = self.allData[indexPath.row];
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:ADDRESS_SET_DEFAULTT params:@{@"addr_id":model.addr_id} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功
            weakSelf.selectModel = model;
            weakSelf.clickAddressBlack(model);//刷新列表看看
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
    
}
#pragma mark ==========点击确定按钮==========
- (IBAction)clickBtn:(UIButton *)sender {
    
}
@end
