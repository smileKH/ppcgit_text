//
//  OrderPayAddressViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPayAddressViewController : HBRootViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) void(^clickAddressBlack)(id obj);
@property (nonatomic, strong) HBOrderPayModel * model;
- (IBAction)clickBtn:(UIButton *)sender;


@end
