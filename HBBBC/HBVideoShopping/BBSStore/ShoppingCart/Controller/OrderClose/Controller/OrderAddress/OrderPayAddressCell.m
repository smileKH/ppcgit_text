//
//  OrderPayAddressCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayAddressCell.h"

@implementation OrderPayAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(Default_address *)model{
    _model = model;
    if ([HBHuTool isJudgeString:model.name]) {
        //还没有收货地址
        self.tiShiLab.hidden = NO;
        self.nameLab.hidden = YES;
        self.phoneLab.hidden = YES;
        self.addressLab.hidden = YES;
        
    }else{
        self.tiShiLab.hidden = YES;
        self.nameLab.hidden = NO;
        self.phoneLab.hidden = NO;
        self.addressLab.hidden = NO;
        self.nameLab.text = [NSString stringWithFormat:@"收货人:%@",model.name];
        self.phoneLab.text = [NSString stringWithFormat:@"%@",model.mobile];
        self.addressLab.text = [NSString stringWithFormat:@"%@%@",model.area,model.addr];
    }
    
}

@end
