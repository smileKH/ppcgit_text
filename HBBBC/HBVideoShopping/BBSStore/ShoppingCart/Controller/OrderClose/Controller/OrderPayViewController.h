//
//  OrderPayViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPayViewController : HBRootViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *bottomview;
- (IBAction)payAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLab;

@property (nonatomic, strong) NSString * isModel;
@property (nonatomic ,strong)NSString *controllerClass;//从哪里来的
@end
