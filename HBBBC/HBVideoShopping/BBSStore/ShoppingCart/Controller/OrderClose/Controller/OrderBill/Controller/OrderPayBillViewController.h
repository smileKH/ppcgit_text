//
//  OrderPayBillViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPayBillViewController : HBRootViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
@property (weak, nonatomic) IBOutlet UIButton *threeBtn;

//发票按钮
- (IBAction)clickOneBtn:(UIButton *)sender;
- (IBAction)clickTwoBtn:(UIButton *)sender;
- (IBAction)clickThreeBtn:(UIButton *)sender;



@end
