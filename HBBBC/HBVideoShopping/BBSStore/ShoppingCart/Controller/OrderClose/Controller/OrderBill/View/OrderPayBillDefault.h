//
//  OrderPayBillDefault.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/7.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPayBillDefault : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
@property (weak, nonatomic) IBOutlet UITextField *contentTF;
- (IBAction)clickOneBtn:(UIButton *)sender;
- (IBAction)clickTwoBtn:(UIButton *)sender;
@property (nonatomic, assign) NSInteger type;//1 个人 2企业


@end
