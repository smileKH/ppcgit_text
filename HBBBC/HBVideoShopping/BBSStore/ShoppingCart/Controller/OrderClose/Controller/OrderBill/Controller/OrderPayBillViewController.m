//
//  OrderPayBillViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "OrderPayBillViewController.h"
#import "OrderPayBillCell.h"
#import "OrderPayBillDefault.h"
@interface OrderPayBillViewController ()
@property (nonatomic, assign) NSInteger type;
@end

@implementation OrderPayBillViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //发票信息
    self.title = @"发票信息";
    self.type = 0;//0 普通发票 1 增值发票 2 不需要发票
    self.oneBtn.selected = YES;
    self.tableview.tableHeaderView = self.header;
    [self.tableview registerNib:[UINib nibWithNibName:@"OrderPayBillCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"OrderPayBillDefault" bundle:nil] forCellReuseIdentifier:@"OrderPayBillDefault"];
    
    //设置初值
    [self setBtnValue];
}
#pragma mark ==========设置初值==========
-(void)setBtnValue{
    [self.oneBtn setBackgroundImage:[UIImage createImageWithColor:gray_Color] forState:UIControlStateNormal];
    [self.oneBtn setBackgroundImage:[UIImage createImageWithColor:red_Color] forState:UIControlStateSelected];
    
    [self.twoBtn setBackgroundImage:[UIImage createImageWithColor:gray_Color] forState:UIControlStateNormal];
    [self.twoBtn setBackgroundImage:[UIImage createImageWithColor:red_Color] forState:UIControlStateSelected];
    
    [self.threeBtn setBackgroundImage:[UIImage createImageWithColor:gray_Color] forState:UIControlStateNormal];
    [self.threeBtn setBackgroundImage:[UIImage createImageWithColor:red_Color] forState:UIControlStateSelected];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (self.type ==0) {
        OrderPayBillDefault *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderPayBillDefault"];
        return cell;
    }else if (self.type ==1){
        OrderPayBillCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        return cell;
    }else{
        return nil;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.type==2) {
        return 0;
    }else{
        return  1;
    }
    
}

- (IBAction)clickOneBtn:(UIButton *)sender {
    self.oneBtn.selected = YES;
    self.twoBtn.selected = NO;
    self.threeBtn.selected = NO;
    self.type = 0;
    
    [self.tableview reloadData];
}

- (IBAction)clickTwoBtn:(UIButton *)sender {
    self.oneBtn.selected = NO;
    self.twoBtn.selected = YES;
    self.threeBtn.selected = NO;
    self.type = 1;
    [self.tableview reloadData];
}

- (IBAction)clickThreeBtn:(UIButton *)sender {
    self.oneBtn.selected = NO;
    self.twoBtn.selected = NO;
    self.threeBtn.selected = YES;
    self.type = 2;
    [self.tableview reloadData];
}
@end
