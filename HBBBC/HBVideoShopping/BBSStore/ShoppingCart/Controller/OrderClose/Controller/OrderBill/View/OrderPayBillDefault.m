//
//  OrderPayBillDefault.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/7.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "OrderPayBillDefault.h"

@implementation OrderPayBillDefault

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //设置初值
    [self setBtnValue];
    
    //设置初值
    self.oneBtn.selected = YES;
    self.type = 1;
}
#pragma mark ==========设置初值==========
-(void)setBtnValue{
    [self.oneBtn setBackgroundImage:[UIImage createImageWithColor:gray_Color] forState:UIControlStateNormal];
    [self.oneBtn setBackgroundImage:[UIImage createImageWithColor:red_Color] forState:UIControlStateSelected];
    
    [self.twoBtn setBackgroundImage:[UIImage createImageWithColor:gray_Color] forState:UIControlStateNormal];
    [self.twoBtn setBackgroundImage:[UIImage createImageWithColor:red_Color] forState:UIControlStateSelected];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickOneBtn:(UIButton *)sender {
    self.oneBtn.selected = YES;
    self.twoBtn.selected = NO;
    self.type = 1;
}

- (IBAction)clickTwoBtn:(UIButton *)sender {
    self.oneBtn.selected = NO;
    self.twoBtn.selected = YES;
    self.type = 2;
}

@end
