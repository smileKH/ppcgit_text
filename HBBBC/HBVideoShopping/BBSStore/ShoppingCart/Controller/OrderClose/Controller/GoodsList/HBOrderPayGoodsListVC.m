//
//  HBOrderPayGoodsListVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderPayGoodsListVC.h"
#import "HBOrderPayGoodsListCell.h"
@interface HBOrderPayGoodsListVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation HBOrderPayGoodsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"商品清单";
    //添加子视图
    [self setupUI];
}
#pragma mark ==========添加子视图==========
-(void)setupUI{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderPayGoodsListCell" bundle:nil] forCellReuseIdentifier:@"goodsList"];
    [self.view addSubview:self.tableView];
}

#pragma mark ==========tableview delegate==========
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.model.items.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //支付地址
    HBOrderPayGoodsListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goodsList"];
    cell.model = self.model.items[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
