//
//  HBOrderPayGoodsListCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderPayGoodsListCell.h"

@implementation HBOrderPayGoodsListCell
- (void)setModel:(Items *)model{
    _model = model;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:[model.image_default_id URLEncodedString]] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.title];
    self.numLab.text = [NSString stringWithFormat:@"x%@",model.quantity];
    self.moneyLab.text = [NSString stringWithFormat:@"￥%@",model.price.price];
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
