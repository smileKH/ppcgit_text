//
//  HBOrderPayGoodsListVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBOrderPayGoodsListVC : HBRootTableViewController
@property (nonatomic, strong) ResultCartData * model;
@end
