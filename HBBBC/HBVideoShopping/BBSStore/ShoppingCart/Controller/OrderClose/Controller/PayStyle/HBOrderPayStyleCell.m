//
//  HBOrderPayStyleCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBOrderPayStyleCell.h"

@implementation HBOrderPayStyleCell

- (void)setModel:(HBOrderPayStyleModel *)model{
    _model = model;
    self.titleLab.text = model.name;
    self.btn.selected = model.isSelect;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickBtn:(UIButton *)sender {
    self.clickPayStyleBtn(self.model);
}
@end
