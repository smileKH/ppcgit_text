//
//  HBOrderPayStyleVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import "HBOrderPayStyleModel.h"
@interface HBOrderPayStyleVC : HBRootViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)clickBtn:(id)sender;
@property (nonatomic, copy) void(^clickPayStyleVC)(HBOrderPayStyleModel *model);
@end
