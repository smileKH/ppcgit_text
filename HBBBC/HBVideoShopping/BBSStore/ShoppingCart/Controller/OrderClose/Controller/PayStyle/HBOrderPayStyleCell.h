//
//  HBOrderPayStyleCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBOrderPayStyleModel.h"
@interface HBOrderPayStyleCell : UITableViewCell
@property (nonatomic, strong) HBOrderPayStyleModel * model;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
- (IBAction)clickBtn:(UIButton *)sender;
@property (nonatomic, copy) void(^clickPayStyleBtn)(HBOrderPayStyleModel *model);
@end
