//
//  HBCartCouponTabCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCartCouponTabCell.h"

@implementation HBCartCouponTabCell
- (void)setModel:(HBCartCouponListModel *)model{
    _model = model;
    self.nameLab.text = [NSString stringWithFormat:@"￥%@",model.deduct_money];
    NSString *startStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd" tenNumber:model.canuse_start_time];
    NSString *endStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd" tenNumber:model.canuse_end_time];
    self.dateLab.text = [NSString stringWithFormat:@"优惠券%@-%@",startStr,endStr];
    self.shopNameLab.text = [NSString stringWithFormat:@"%@",model.coupon_desc];
    self.selectBtn.selected = model.selected;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickSelectBtn:(UIButton *)sender {
    //点击选择按钮
    self.clickCouponlistBtn(self.model);
}

@end
