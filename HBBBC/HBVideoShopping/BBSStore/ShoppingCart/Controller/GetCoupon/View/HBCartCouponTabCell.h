//
//  HBCartCouponTabCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBCartCouponModel.h"
@interface HBCartCouponTabCell : UITableViewCell
@property (nonatomic, strong) HBCartCouponListModel * model;
@property (weak, nonatomic) IBOutlet UILabel *couponQuanLab;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
- (IBAction)clickSelectBtn:(UIButton *)sender;
@property (nonatomic, strong) NSString * shop_name;
@property (nonatomic, copy) void(^clickCouponlistBtn)(HBCartCouponListModel *model);
@end
