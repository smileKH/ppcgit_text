//
//  HBCartCouponModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCartCouponModel.h"

@implementation HBCartCouponModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"list":@"HBCartCouponListModel"};
}
@end

@implementation HBCartCouponListModel

@end
