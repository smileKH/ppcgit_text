//
//  HBCartCouponModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Pagers;
@class Cur_symbol;
@class HBCartCouponListModel;
@interface HBCartCouponModel : NSObject

@property (nonatomic, strong) NSString * shopname;
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@property (nonatomic, strong) NSArray * list;
@property (nonatomic, strong) Pagers * pagers;
@end

@interface HBCartCouponListModel : NSObject
@property (nonatomic,assign) BOOL selected ;//是否选择
@property (nonatomic, strong) NSString * coupon_id;
@property (nonatomic, strong) NSString * coupon_desc;
@property (nonatomic, strong) NSString * deduct_money;
@property (nonatomic, strong) NSNumber * canuse_end_time;
@property (nonatomic, strong) NSString * coupon_name;
@property (nonatomic, strong) NSNumber * canuse_start_time;
@end
