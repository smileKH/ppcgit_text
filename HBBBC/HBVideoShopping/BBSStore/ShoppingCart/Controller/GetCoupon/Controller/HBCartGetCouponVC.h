//
//  HBCartGetCouponVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBCartGetCouponVC : HBRootViewController
@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)clickBtn:(UIButton *)sender;
@property (nonatomic, strong) NSString * shop_id;
@end
