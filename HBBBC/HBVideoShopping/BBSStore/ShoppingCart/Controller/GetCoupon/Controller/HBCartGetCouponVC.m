//
//  HBCartGetCouponVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCartGetCouponVC.h"
#import "HBCartCouponTabCell.h"
#import "HBCartCouponModel.h"
@interface HBCartGetCouponVC ()
@property (nonatomic,assign) NSInteger selectIndex ;    //选中行
@property (nonatomic, strong) UIButton * headerBtn;
@property (nonatomic, strong) UIView * tableheader;
@end

@implementation HBCartGetCouponVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"优惠券";
    
    [self.tableview registerNib:[UINib nibWithNibName:@"HBCartCouponTabCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
//    [self tableHeader];
    self.headerBtn.selected = YES;
    self.selectIndex=-1;
    self.tableview.tableFooterView = [[UIView alloc]init];
    
    //请求优惠券
    [self requestShopConponList:YES];
    WEAKSELF;
    //默认block方法：设置下拉刷新
//    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestShopConponList:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestShopConponList:NO];
//    }];
}
#pragma mark ==========请求我的订单==========
-(void)requestShopConponList:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    parameter[@"shop_id"] = self.shop_id;
    WEAKSELF;
    [LBService post:PRO_SHOP_LIST_GET params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"total"]intValue];
                NSArray *arr = [dic objectForKey:@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [HBCartCouponListModel mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            }
           
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptNormal];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HBCartCouponTabCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.allData[indexPath.row];
    WEAKSELF;
    cell.clickCouponlistBtn = ^(HBCartCouponListModel *model) {
        [weakSelf selectIndexPath:indexPath];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.allData.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectIndexPath:indexPath];
}
#pragma mark ==========选择优惠券==========
-(void)selectIndexPath:(NSIndexPath *)indexPath{
    HBCartCouponListModel *model = self.allData[indexPath.row] ;
    //选中
    model.selected = YES ;
    //其它置为不选中
    for (int i=0; i<self.allData.count; i++) {
        if (i==indexPath.row) {
            continue ;
        }else{
            HBCouponlist *tempModel = self.allData[i] ;
            tempModel.selected = NO ;
        }
    }
    self.selectIndex = indexPath.row;
    self.headerBtn.selected = NO;
    [self.tableview reloadData] ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ==========点击确定按钮==========
- (IBAction)clickBtn:(UIButton *)sender {
    if (self.selectIndex==-1) {
        [app showToastView:@"你还未选择优惠券"];
        return;
    }
    //用户领取优惠券
    HBCartCouponListModel *model = self.allData[self.selectIndex];
//    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:PRO_SHOP_CODE_GET params:@{@"coupon_id":model.coupon_id} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            // 如果成功，那么计算价格
            [app showToastView:@"领取成功"];
        }else{
            [app showToastView:response.message];
        }
    }];
}


@end
