//
//  ShoppingCarViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/4.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShoppingCarViewController.h"
#import "ShoppingCarCell.h"
#import "ShoppingCarHeader.h"
#import "ShoppingCarFooter.h"
#import "PayViewController.h"
#import "HBMyOrderDetailVC.h"
#import "OrderPayViewController.h"
#import "LoginViewController.h"
#import "ShoppingCarModel.h"

#import "HBShopingCartCouponView.h"
#import "HBCartGetCouponVC.h"
#import "HBB_DataManage.h"
#import "HBCartOffterLineModel.h"
#import "HBCartTotalModel.h"
@interface ShoppingCarViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) ShoppingCarModel *carmodel;
@property (nonatomic, strong) HBShopingCartCouponView * pushView;
@property (nonatomic, strong) HBB_DataManage * fmdbManage;
@property (nonatomic, strong) HBCartOffterLineModel * lineModel;
@property (nonatomic, strong) UIButton * rightBtn;
@property (nonatomic, assign) BOOL  isPayViewSelet;
@end

@implementation ShoppingCarViewController

#pragma mark ==========获取购物车数据==========
- (void)getMyCar
{
    WEAKSELF;
    NSDictionary*dic = @{@"accessToken":LocalValue(TOKEN_XY_APP),@"mode":@"cart",@"platform":@"wap"};
    [LBService post:CART_GET_LIST params:dic completion:^(LBResponse *response) {
        if (response.succeed) {
            weakSelf.carmodel = [ShoppingCarModel mj_objectWithKeyValues:response.result[@"data"]];

            [weakSelf.tableview reloadData];
            //计算价格
//            [weakSelf countTotalMoney];
            //显示总金额
            [weakSelf updateTotalMoney];
            
            if (weakSelf.carmodel.cartlist.count == 0){
                //暂无数据
                weakSelf.bottomView.hidden = YES;
                weakSelf.rightBtn.hidden = YES;
                [weakSelf showContentNullPromptImg:@"" withLabelName:@"购物车空空如也" withButtonName:@"去逛逛"];
            }else{
                weakSelf.bottomView.hidden = NO;
                if (weakSelf.isPayViewSelet) {
                    weakSelf.payView.hidden = YES;
                }else{
                    weakSelf.payView.hidden = NO;
                }
                
                weakSelf.rightBtn.hidden = NO;
                [weakSelf hideContentNullPromptView];
            }
        }else{
            [app showToastView:response.message];
        }
    }];
}

#pragma mark ==========显示总金额==========
-(void)updateTotalMoney{
    
    self.totalMaoeyLabel.attributedText = [HBHuTool attributedOldString:@"合计 ：" priceString:self.carmodel.totalCart.totalAfterDiscount];
}

#pragma mark ==========点击了逛一逛按钮==========
-(void)clickSpaceButton{
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
}
//每次进来界面的时候
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isPayViewSelet = NO;
    [self setNavBar];
    //判断是否登录
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        self.tableview.tableHeaderView = nil;
        [self getMyCar];
    } else{
        //还没有等
        self.tableview.tableHeaderView = self.tableHeader;
        //没有登录 获取数据库
        [self requestFMDBdata];
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"购物车";
    //设置基本UI
    [self setuptableview];
    //设置初值
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.layer.cornerRadius = 6;
    self.loginBtn.layer.borderWidth =1;
    self.loginBtn.layer.borderColor = MAINTextCOLOR.CGColor;
    
    //接收通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateLoadData) name:@"ShoppingCarViewController" object:nil];
}
-(void)updateLoadData{
    [self setNavBar];
    //判断是否登录
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        self.tableview.tableHeaderView = nil;
        [self getMyCar];
    } else{
        //还没有等
        self.tableview.tableHeaderView = self.tableHeader;
        //没有登录 获取数据库
        [self requestFMDBdata];
    }
}

#pragma mark ==========获取数据库数据==========
-(void)requestFMDBdata{
    
    self.fmdbManage = [HBB_DataManage sharedHBB_DataManage];
    //创建表
    [self.fmdbManage createTableWithName:@""];
    //查询
    self.lineModel = [[HBCartOffterLineModel alloc]init];
    self.lineModel.shopArray = [NSMutableArray array];
    NSArray *arr = [self.fmdbManage fmdbSelectData];
//    if (![HBHuTool judgeArrayIsEmpty:arr]) {
//        //没有数据
//        self.bottomView.hidden = YES;
//        [self showContentNullPromptImg:@"" withLabelName:@"购物车空空如也" withButtonName:@"去逛逛"];
//        return;
//    }
    //遍历得到一个新的数组
    NSMutableArray *array = [NSMutableArray arrayWithArray:arr];
    
    NSMutableArray *dateMutablearray = [@[] mutableCopy];
    for (int i = 0; i < array.count; i ++) {
        
        Cart_add *model = array[i];
        
        NSMutableArray *tempArray = [@[] mutableCopy];
        
        [tempArray addObject:model];
        
        for (int j = i+1; j < array.count; j ++) {
            
            Cart_add *jsModel = array[j];
            
            if([model.shopId isEqualToString:jsModel.shopId]){
                
                [tempArray addObject:jsModel];
                
                [array removeObjectAtIndex:j];
                j -= 1;
                
            }
        }
        [dateMutablearray addObject:tempArray];
    }
    for (NSInteger i=0; i<dateMutablearray.count; i++) {
        NSArray *arr = dateMutablearray[i];
        HBCartOfShopItmModel *shoModel = [[HBCartOfShopItmModel alloc]init];
        shoModel.goods = [NSMutableArray array];
        if ([HBHuTool judgeArrayIsEmpty:arr]) {
            for (Cart_add *model in arr) {
                //判断id是否相同
                shoModel.shopId = model.shopId;
                shoModel.shopName = model.shopName;
                shoModel.is_checked = NO;
                
                HBCartOfGoodsItemModel *goodsModel = [[HBCartOfGoodsItemModel alloc]init];
                goodsModel.goodsId = model.goodsId;
                goodsModel.goodsName = model.goodsName;
                goodsModel.money = model.money;
                goodsModel.quantity = [model.quantity integerValue];
                goodsModel.sku_id = model.sku_id;
                goodsModel.package_sku_ids = model.package_sku_ids;
                goodsModel.package_id = model.package_id;
                goodsModel.obj_type = model.obj_type;
                goodsModel.mode = model.mode;
                goodsModel.price.price = model.money;
                goodsModel.image_default_id = model.image_default_id;
                goodsModel.is_checked = NO;
                //商品
                [shoModel.goods addObject:goodsModel];
                
            }
        }
        //店铺
        [self.lineModel.shopArray addObject:shoModel];
    }
    
    //计算价格
    [self countOfLineTotalMoney];
    
    if (self.lineModel.shopArray.count == 0){
        //暂无数据
        self.bottomView.hidden = YES;
        self.rightBtn.hidden = YES;
        [self showContentNullPromptImg:@"" withLabelName:@"购物车空空如也" withButtonName:@"去逛逛"];
    }else{
        self.bottomView.hidden = NO;
        self.rightBtn.hidden = NO;
        [self hideContentNullPromptView];
    }
    //刷新界面
    [self.tableview reloadData];
}
#pragma mark ==========登录成功刷新界面==========
-(void)loginSuccess:(NSNotification *)noti{
    //登录成功，如果数据库有数据，那么自动更新数据库数据 然后刷新界面
    self.fmdbManage = [HBB_DataManage sharedHBB_DataManage];
    //创建表
    [self.fmdbManage createTableWithName:@""];
    NSArray *arr = [self.fmdbManage fmdbSelectData];
    if ([HBHuTool judgeArrayIsEmpty:arr]) {
        //更新到服务器
        //已经是登录状态
        for (Cart_add *model in arr) {
            //遍历数组 一个个添加
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"quantity"] = model.quantity;
            parameters[@"sku_id"] = model.sku_id;
            parameters[@"obj_type"] = @"item";
            parameters[@"mode"] = @"cart";
            [LBService post:GOODS_ADD_CART params:parameters completion:^(LBResponse *response) {
                if (response.succeed) {
                    //
                    NSLog(@"更新服务器成功");
                }else{
                    
                }
            }];
        }
        //既然已经更新了购物车，那么就清除数据
        [self.fmdbManage deleteAllTable];
    }
}
#pragma mark ==========退出登录==========
- (void)logoutSuccess:(NSNotification *)noti{

}

#pragma mark ==========设置导航栏按钮==========
- (void)setNavBar
{
   self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBtn.bounds = CGRectMake(0, 0, 50, 40);
    [self.rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [self.rightBtn setTitle:@"完成" forState:UIControlStateSelected];
    [self.rightBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.rightBtn setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}
#pragma mark ==========注册基本UI界面==========
- (void)setuptableview
{
    _tableview.estimatedRowHeight = 0;
    _tableview.estimatedSectionHeaderHeight = 0;
    _tableview.estimatedSectionFooterHeight = 0;
    [self.tableview registerNib:[UINib nibWithNibName:@"ShoppingCarCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"ShoppingCarHeader" bundle:nil] forHeaderFooterViewReuseIdentifier:@"header"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"ShoppingCarFooter" bundle:nil] forHeaderFooterViewReuseIdentifier:@"footer"];
    
}
#pragma mark ==========设置tableview类型==========
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete |UITableViewCellEditingStyleInsert;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark ==========tableViewDelegate==========
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        return self.carmodel.cartlist.count;
    }else{
        //没有登录 获取数据库
        return self.lineModel.shopArray.count;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        Cartlist *list = self.carmodel.cartlist[section];
        return  list.goods.count;
    }else{
        //没有登录 获取数据库
        HBCartOfShopItmModel *shop = self.lineModel.shopArray[section];
        return  shop.goods.count;
    }
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShoppingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    WEAKSELF;
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        Cartlist *list = self.carmodel.cartlist[indexPath.section];
        GoodsItem *model = list.goods[indexPath.row];
        cell.model = model;
        list.is_checked = NO;
        //计算价格与总金额
        
        cell.selectModel = ^(BOOL ischeck, GoodsItem *selectModel) {
            [weakSelf refreashWithIndexPath:indexPath select:ischeck];
        };
        cell.clickCouponView = ^(GoodsItem *selectModel) {
            //点击促销
            [weakSelf pushViewAndModel:model andIndexPatah:indexPath];
            
        };
    }else{
        //没有登录 获取数据库
        HBCartOfShopItmModel *shop = self.lineModel.shopArray[indexPath.section];
        HBCartOfGoodsItemModel *goodItem = shop.goods[indexPath.row];
        cell.goodsItemModel = goodItem;
//        cell.model = list.goods[indexPath.row];
        shop.is_checked = NO;
        //计算价格与总金额
//        WEAKSELF;
        cell.selectOfLineModel = ^(BOOL ischeck, HBCartOfGoodsItemModel *selectModel) {
            [weakSelf refreashOfLineWithIndexPath:indexPath select:ischeck];
        };
        cell.clickCouponView = ^(GoodsItem *selectModel) {
            //点击促销
        };
    }
    
    return cell;

}
-(void)pushViewAndModel:(GoodsItem *)selectModel andIndexPatah:(NSIndexPath *)indexPath{
    WEAKSELF;
    self.pushView = [[HBShopingCartCouponView alloc]init];
    self.pushView.model =  selectModel;
    [self.pushView show];
    self.pushView.clickShopingCartCoponView = ^(Promotiontags *model) {
        //促销活动 提交促销信息
        [weakSelf refreashWithIndexPath:indexPath select:selectModel.goodsItem.is_checked];
//        [weakSelf updateShopingCartCouponModel:selectModel andPromotiontModel:model];
    };
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ShoppingCarHeader* header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        Cartlist *model = self.carmodel.cartlist[section];
        header.model = model;
        header.selectModel = ^(BOOL ischeck, Cartlist *selectmodel) {
            [self refreashHeaderWithSection:section select:ischeck];//根据头刷新
        };
        WEAKSELF;
        header.clickCouponBtn = ^(Cartlist *selectmodel) {
            //点击领劵按钮 跳到领券界面
            HBCartGetCouponVC *vc = [[HBCartGetCouponVC alloc]initWithNibName:@"HBCartGetCouponVC" bundle:nil];
            vc.shop_id = model.shop_id;
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        };
    }else{
        //没有登录 获取数据库
        HBCartOfShopItmModel *shop = self.lineModel.shopArray[section];
        header.shopModel = shop;
        header.selectOfLineModel = ^(BOOL ischeck, HBCartOfShopItmModel *selectmodel) {
            //
            [self refreashOfLineHeaderWithSection:section select:ischeck];//根据头刷新
        };
    }

    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ShoppingCarFooter* footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"footer"];
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        footer.model = self.carmodel.cartlist[section];
        
    }else{
        //没有登录 获取数据库
        HBCartOfShopItmModel *shop = self.lineModel.shopArray[section];
        footer.shopModel = shop;
    }
    return footer;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        Cartlist *list = self.carmodel.cartlist[indexPath.section];
        GoodsItem *model = list.goods[indexPath.row];
        ProductFirstViewController *vc= [[ProductFirstViewController alloc] initWithNibName:@"ProductFirstViewController" bundle:nil];
        vc.item_id = model.goodsItem.item_id;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        //没有登录 获取数据库
        HBCartOfShopItmModel *shop = self.lineModel.shopArray[indexPath.section];
        HBCartOfGoodsItemModel *goodItem = shop.goods[indexPath.row];
        ProductFirstViewController *vc= [[ProductFirstViewController alloc] initWithNibName:@"ProductFirstViewController" bundle:nil];
        vc.item_id = goodItem.goodsId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
#pragma mark ==========数据库 改变数量和选择==========
-(void)refreashOfLineWithIndexPath:(NSIndexPath *)indexPath select:(BOOL)isSelect;
{
    //改变数据源
    HBCartOfShopItmModel *shop = self.lineModel.shopArray[indexPath.section];
    
    //更新数据库数据
    HBCartOfGoodsItemModel *goodsItem = shop.goods[indexPath.row];
    [self.fmdbManage fmdbUpdateData:goodsItem];
    
    if (!isSelect) {
        shop.is_checked = NO;
        self.lineModel.nocheckedall = NO;
        self.allBtn.selected = NO;
    }
    BOOL isSelectAll = YES;
    for (NSInteger i = 0; i < shop.goods.count; i++) {
        HBCartOfGoodsItemModel *item = shop.goods[i];
        isSelectAll =  item.is_checked && isSelectAll;
    }
    
    shop.is_checked = isSelectAll;
    //计算价格
    [self countOfLineTotalMoney];
    
    [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    
}
#pragma mark ==========数据库 计算价格==========
- (void)countOfLineTotalMoney{
    CGFloat totalMoney = 0.00;
    BOOL s1 = YES;
    for (NSInteger i = 0; i < self.lineModel.shopArray.count; i ++) {
#pragma mark    ------   //使用参与活动与否
        BOOL s2 = YES;
        HBCartOfShopItmModel *shop = self.lineModel.shopArray[i];
        CGFloat m1 = 0.00;
        for (NSInteger j = 0; j < shop.goods.count; j++) {
            HBCartOfGoodsItemModel *item = shop.goods[j];
            BOOL isselect = item.is_checked;
            if (isselect) { //如果选中
                m1 = m1 + item.quantity*[item.money integerValue];
                
            }
            shop.shopMoney = m1;
            s2 = s2 && isselect;
        }
        totalMoney = totalMoney +shop.shopMoney;
        s1 = s1 && s2;
    }
    self.allBtn.selected = s1;
    self.totalMaoeyLabel.attributedText = [HBHuTool attributedOldString:@"合计 ：" priceString:floatString(totalMoney)];
}

#pragma mark ==========数据库 选择店铺头按钮==========
- (void)refreashOfLineHeaderWithSection:(NSInteger )section select:(BOOL)isSelect
{
    if (!isSelect) {
        self.lineModel.nocheckedall = NO;
        self.allBtn.selected = NO;
    }
    //改变数据源
    HBCartOfShopItmModel *shop = self.lineModel.shopArray[section];
    for (NSInteger i = 0; i < shop.goods.count; i++) {
        HBCartOfGoodsItemModel *item = shop.goods[i];
        item.is_checked = isSelect;
    }
    shop.is_checked = isSelect;
    //更新数据再计算价格
    [self countOfLineTotalMoney];
    //修改总的选中情况
    [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
 
}
#pragma mark ==========点击结算按钮==========
- (IBAction)payAction:(UIButton *)sender {
    //更新购物车
    //次数拼接要删除的item
    //删除后  重新获取数据
//    NSMutableString *strs = [NSMutableString string];//可变字符串
    BOOL  isPush = NO;
    for (NSInteger i = 0; i < self.carmodel.cartlist.count; i ++) {
        Cartlist *list = self.carmodel.cartlist[i];
        for (NSInteger j = 0; j < list.goods.count; j++) {
            GoodsItem *item = list.goods[j];
            BOOL isselect = item.goodsItem.is_checked;
            if (isselect) { //如果选中
                isPush = YES;
                break;
                //[strs appendString:[NSString stringWithFormat:@",%@",item.goodsItem.cart_id]];
            }
        }
    }
    
    if (isPush) {
        OrderPayViewController *vc= [[OrderPayViewController alloc] initWithNibName:@"OrderPayViewController" bundle:nil];
        vc.isModel = @"cart";
        vc.controllerClass = @"ShoppingCarViewController";
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [app showToastView:@"您还未选中任何商品"];
    }
    
}
#pragma mark ==========设置滚动视图==========
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableview)
    {
        UITableView *tableview = (UITableView *)scrollView;
        CGFloat sectionHeaderHeight = 50;
        CGFloat sectionFooterHeight = 50;
        CGFloat offsetY = tableview.contentOffset.y;
        if (offsetY >= 0 && offsetY <= sectionHeaderHeight)
        {
            tableview.contentInset = UIEdgeInsetsMake(-offsetY, 0, -sectionFooterHeight, 0);
        }else if (offsetY >= sectionHeaderHeight && offsetY <= tableview.contentSize.height - tableview.frame.size.height - sectionFooterHeight)
        {
            tableview.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, -sectionFooterHeight, 0);
        }else if (offsetY >= tableview.contentSize.height - tableview.frame.size.height - sectionFooterHeight && offsetY <= tableview.contentSize.height - tableview.frame.size.height)         {
            tableview.contentInset = UIEdgeInsetsMake(-offsetY, 0, -(tableview.contentSize.height - tableview.frame.size.height +sectionFooterHeight), 0);
        }
    }
}

#pragma mark ---- 刷新计算金钱操作
-(void)refreashWithIndexPath:(NSIndexPath *)indexPath select:(BOOL)isSelect
{
//    //先更新购物车 计算优惠券
//    NSMutableArray *cart_params = [NSMutableArray array];
//    Cartlist *list = self.carmodel.cartlist[indexPath.section];
//    GoodsItem *goods = list.goods[indexPath.row];
//    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    parameters[@"cart_id"] = goods.goodsItem.cart_id;
//    parameters[@"cart_id"] = goods.goodsItem.cart_id;
//    if (goods.goodsItem.is_checked) {
//        parameters[@"is_checked"] = @(1);
//    }else{
//        parameters[@"is_checked"] = @(0);
//    }
//
//    //遍历促销券
//    for (Promotiontags *promModel in goods.goodsItem.promotiontags) {
//        if (promModel.selected) {//选择了促销
//            parameters[@"selected_promotion"] = promModel.promotion_id;
//        }
//    }
//
//    parameters[@"totalQuantity"] = @(goods.goodsItem.quantity);
//
//    [cart_params addObject:parameters];
    //先更新购物车 计算优惠券
    NSMutableArray *cart_params = [NSMutableArray array];
    Cartlist *list = self.carmodel.cartlist[indexPath.section];
    for (GoodsItem *goods in list.goods) {//商品
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        parameters[@"cart_id"] = goods.goodsItem.cart_id;
        if (goods.goodsItem.is_checked) {
            parameters[@"is_checked"] = @(1);
        }else{
            parameters[@"is_checked"] = @(0);
        }
        //遍历促销券
        for (Promotiontags *promModel in goods.goodsItem.promotiontags) {
            if (promModel.selected) {//选择了促销
                parameters[@"selected_promotion"] = promModel.promotion_id;
            }
        }
        parameters[@"totalQuantity"] = @(goods.goodsItem.quantity);
        
        [cart_params addObject:parameters];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cart_params
                                                       options:kNilOptions
                                                         error:nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    WEAKSELF;
    [LBService post:CART_GOODS_UPDATE params:@{@"cart_params":jsonString,@"mode":@"cart",@"obj_type":@"item"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            [weakSelf updateCartDataWithIndexPath:indexPath select:isSelect];
        }else{
            [app showToastView:response.message];
        }
    }];
 
    
}
#pragma mark ==========更新了服务器数据再更新购物车==========
-(void)updateCartDataWithIndexPath:(NSIndexPath *)indexPath select:(BOOL)isSelect{
    //改变数据源
    Cartlist *list = self.carmodel.cartlist[indexPath.section];
    if (!isSelect) {
        list.is_checked = NO;
        self.carmodel.nocheckedall = NO;
        self.allBtn.selected = NO;
    }
    BOOL isSelectAll = YES;
    for (NSInteger i = 0; i < list.goods.count; i++) {
        GoodsItem *item = list.goods[i];
        isSelectAll =  item.goodsItem.is_checked && isSelectAll;
    }
    
    list.is_checked = isSelectAll;
    //计算价格
    [self countTotalMoney];
    
    [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
   
}
#pragma mark ==========选择店铺头按钮==========
- (void)refreashHeaderWithSection:(NSInteger )section select:(BOOL)isSelect
{
    //先更新购物车
    NSMutableArray *cart_params = [NSMutableArray array];
    Cartlist *list = self.carmodel.cartlist[section];
    for (GoodsItem *goods in list.goods) {//商品
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        parameters[@"cart_id"] = goods.goodsItem.cart_id;
        if (isSelect) {
            parameters[@"is_checked"] = @(1);
        }else{
            parameters[@"is_checked"] = @(0);
        }
        //遍历促销券
        for (Promotiontags *promModel in goods.goodsItem.promotiontags) {
            if (promModel.selected) {//选择了促销
                parameters[@"selected_promotion"] = promModel.promotion_id;
            }
        }
        parameters[@"totalQuantity"] = @(goods.goodsItem.quantity);
        
        [cart_params addObject:parameters];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cart_params
                                                       options:kNilOptions
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    WEAKSELF;
    [LBService post:CART_GOODS_UPDATE params:@{@"cart_params":jsonString,@"mode":@"cart",@"obj_type":@"item"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            [weakSelf updateHeaderDataWithSection:section select:isSelect];
        }else{
            [app showToastView:response.message];
        }
    }];
    
    
    
}
#pragma mark ==========更新本地购物车数据==========
-(void)updateHeaderDataWithSection:(NSInteger )section select:(BOOL)isSelect{
    if (!isSelect) {
        self.carmodel.nocheckedall = NO;
        self.allBtn.selected = NO;
    }
    //改变数据源
    Cartlist *list = self.carmodel.cartlist[section];
    for (NSInteger i = 0; i < list.goods.count; i++) {
        GoodsItem *item = list.goods[i];
        item.goodsItem.is_checked = isSelect;
    }
    //更新数据再计算价格
    [self countTotalMoney];
    //修改总的选中情况
    [self.tableview reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark ==========计算总金额==========
- (void)countTotalMoney{
    CGFloat totalMoney = 0.00;
    BOOL s1 = YES;
    for (NSInteger i = 0; i < self.carmodel.cartlist.count; i ++) {
#pragma mark    ------   //使用参与活动与否
        BOOL s2 = YES;
        Cartlist *list = self.carmodel.cartlist[i];
        CGFloat m1 = 0.00;
        for (NSInteger j = 0; j < list.goods.count; j++) {
            GoodsItem *item = list.goods[j];
            BOOL isselect = item.goodsItem.is_checked;
            if (isselect) { //如果选中
               m1 = m1 + [item.goodsItem.price.total_price floatValue];
                
            }
            list.shopMoney = m1;
            s2 = s2 && isselect;
        }
        totalMoney = totalMoney +list.shopMoney;
        s1 = s1 && s2;
    }
    
    self.allBtn.selected = s1;
    
    //判断是否登录
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        //请求总金额
        [self requestCartTotalMoney];
    }else{
        //没有登录 获取数据库
        self.totalMaoeyLabel.attributedText = [HBHuTool attributedOldString:@"合计 ：" priceString:floatString(totalMoney)];
    }
}

#pragma mark ==========请求购物车总金额==========
-(void)requestCartTotalMoney{
    //重新请求
    [self getMyCar];
}

- (void)editAction:(UIButton *)sender
{
    sender.selected = !self.isPayViewSelet;
//    _payView.hidden = sender.selected;
    
    self.isPayViewSelet = sender.selected;
    _payView.hidden = self.isPayViewSelet;
}
- (IBAction)loginAction:(UIButton *)sender {
    //判断是否登录
    if ([userManager judgeLoginState]) {

    }
}
//点击了全选
- (IBAction)seletAll:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        self.carmodel.nocheckedall = sender.selected;
        for (NSInteger i = 0; i < self.carmodel.cartlist.count; i++) {
            Cartlist *list = self.carmodel.cartlist[i];
            list.is_checked = sender.selected;
            [self refreashHeaderWithSection:i select:sender.selected];
        }
    }else{
        //没有登录
        self.lineModel.nocheckedall = sender.selected;
        for (NSInteger i = 0; i < self.lineModel.shopArray.count; i++) {
            HBCartOfShopItmModel *shop = self.lineModel.shopArray[i];
            shop.is_checked = sender.selected;
            [self refreashOfLineHeaderWithSection:i select:sender.selected];
        }
    }
    
}
- (IBAction)delectAction:(UIButton *)sender {
    //次数拼接要删除的item
    //删除后  重新获取数据
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
        NSMutableString *strs = [NSMutableString string];//可变字符串
        for (NSInteger i = 0; i < self.carmodel.cartlist.count; i ++) {
            Cartlist *list = self.carmodel.cartlist[i];
            for (NSInteger j = 0; j < list.goods.count; j++) {
                GoodsItem *item = list.goods[j];
                BOOL isselect = item.goodsItem.is_checked;
                if (isselect) { //如果选中
                    [strs appendString:[NSString stringWithFormat:@",%@",item.goodsItem.cart_id]];
                }
            }
        }
        NSDictionary *dicts = @{@"cart_id":strs,@"mode":@"0"};
        [MBProgressHUD showActivityMessageInView:@"删除中..."];
        [LBService post:CART_GOODS_DEL params:dicts completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //更新界面
                [self getMyCar];
            }else{
                [app showToastView:response.message];
            }
        }];
    }else{
        //还没登录
        for (NSInteger i = 0; i < self.lineModel.shopArray.count; i ++) {
            HBCartOfShopItmModel *shop = self.lineModel.shopArray[i];
            for (NSInteger j = 0; j < shop.goods.count; j++) {
                HBCartOfGoodsItemModel *item = shop.goods[j];
                BOOL isselect = item.is_checked;
                if (isselect) { //如果选中
                    //操作数据库删除
                   BOOL isSu =  [self.fmdbManage fmdbDeleteData:item];
                    if (isSu) {
                        [app showToastView:@"删除成功"];
                    }else{
                        [app showToastView:@"删除失败"];
                    }
                }
            }
        }
        //更新数据库 刷新界面
        [self requestFMDBdata];
  
    }
    
}

@end
