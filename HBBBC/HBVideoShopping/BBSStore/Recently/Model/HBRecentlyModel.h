//
//  HBRecentlyModel.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBRecentlyModel : NSObject
@property (nonatomic, strong) NSString * wangwang;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * open_time;
@property (nonatomic, strong) NSString * close_reason;
@property (nonatomic, strong) NSString * shopuser_name;
@property (nonatomic, strong) NSString * tel;
@property (nonatomic, strong) NSString * shopuser_identity;
@property (nonatomic, strong) NSString * shop_descript;
@property (nonatomic, strong) NSString * shop_addr;
@property (nonatomic, strong) NSString * latitude;
@property (nonatomic, strong) NSString * shop_logo;
@property (nonatomic, strong) NSString * bulletin;
@property (nonatomic, strong) NSString * shopuser_identity_img_f;
@property (nonatomic, strong) NSString * shopuser_identity_img;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * shop_type;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * close_time;
@property (nonatomic, strong) NSString * longitude;
@property (nonatomic, strong) NSString * distance;
@property (nonatomic, strong) NSString * area;
@property (nonatomic, strong) NSString * qq;
@property (nonatomic, strong) NSString * seller_id;
@property (nonatomic, strong) NSString * shop_area;
@property (nonatomic, strong) NSString * shop_name;
@property (nonatomic, strong) NSString * shop_id;

@end

