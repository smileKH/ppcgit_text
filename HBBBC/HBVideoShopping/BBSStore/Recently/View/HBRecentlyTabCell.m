//
//  HBRecentlyTabCell.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRecentlyTabCell.h"

@implementation HBRecentlyTabCell
-(void)setModel:(HBRecentlyModel *)model{
    _model = model;
    
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.shop_name];
    self.locLab.text = [NSString stringWithFormat:@"%@km",model.distance];
    self.phoneLab.text = [NSString stringWithFormat:@"%@",model.mobile];
    self.addreLab.text = [NSString stringWithFormat:@"%@%@",model.shop_area,model.shop_addr];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.shop_logo] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickPhoneBtn:(UIButton *)sender {
    self.clickRecentPhoneBlock(nil);
}

- (IBAction)clickAddressBtn:(UIButton *)sender {
    self.clickRecentAddressBlock(nil);
}
@end
