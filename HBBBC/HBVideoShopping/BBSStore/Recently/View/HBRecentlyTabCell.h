//
//  HBRecentlyTabCell.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBRecentlyModel.h"
@interface HBRecentlyTabCell : UITableViewCell
@property (nonatomic ,strong)HBRecentlyModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *locLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addreLab;
@property (nonatomic ,copy)void(^clickRecentPhoneBlock)(id obj);
@property (nonatomic ,copy)void(^clickRecentAddressBlock)(id obj);
- (IBAction)clickPhoneBtn:(UIButton *)sender;
- (IBAction)clickAddressBtn:(UIButton *)sender;

@end
