//
//  HBRecentlyMerchantsVC.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBRecentlyMerchantsVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
