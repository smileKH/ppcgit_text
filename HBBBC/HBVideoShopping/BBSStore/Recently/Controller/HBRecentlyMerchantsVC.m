//
//  HBRecentlyMerchantsVC.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/20.
//  Copyright © 2018年 FirstVision. All rights reserved.
//附近商家

#import "HBRecentlyMerchantsVC.h"
#import "HBRecentlyTabCell.h"
#import "HBRecentlyModel.h"
#import "HBShowShopMapVC.h"
#import "MoLocationManager.h"
@interface HBRecentlyMerchantsVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,assign)double longitude;
@property (nonatomic ,assign)double latitude;
@property (nonatomic ,strong)HBRecentlyModel *selectModel;
@end

@implementation HBRecentlyMerchantsVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"附近商家";
    //注册
    [self.tableView registerNib:[UINib nibWithNibName:@"HBRecentlyTabCell" bundle:nil] forCellReuseIdentifier:@"goods"];
    self.tableView.tableFooterView = [[UIView alloc]init];
    
    //只获取一次
    __block  BOOL isOnece = YES;
    WEAKSELF;
    [MoLocationManager getMoLocationWithSuccess:^(double lat, double lng){
        isOnece = NO;
        //只打印一次经纬度
        NSLog(@"lat lng (%f, %f)", lat, lng);
        weakSelf.longitude = lng;
        weakSelf.latitude = lat;
        //获取数据
        [weakSelf requestMyCollectData];
        
        if (!isOnece) {
            [MoLocationManager stop];
        }
    } Failure:^(NSError *error){
        isOnece = NO;
        NSLog(@"error = %@", error);
        if (!isOnece) {
            [MoLocationManager stop];
        }
    }];
    //    //一直持续获取定位则
    //    [MoLocationManager getMoLocationWithSuccess:^(double lat, double lng){
    //        //不断的打印经纬度
    //        NSLog(@"lat lng (%f, %f)", lat, lng);
    //    } Failure:^(NSError *error){
    //        NSLog(@"error = %@", error);
    //    }];
}

#pragma mark ==========请求我的商品收藏==========
-(void)requestMyCollectData{
    WEAKSELF;
    [LBService post:RECENTLY_SHOP_POSITION params:@{@"longitude":@(self.longitude),@"latitude":@(self.latitude)} completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            NSArray *arr = response.result[@"data"];
            if ([HBHuTool judgeArrayIsEmpty:arr]) {
                NSArray *array = [HBRecentlyModel mj_objectArrayWithKeyValuesArray:arr];
                if ([HBHuTool judgeArrayIsEmpty:array]) {
                    [weakSelf.allData addObjectsFromArray:array];
                }
            }
            [self.tableView reloadData];
            if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
                //有数据
                [weakSelf hideContentNullPromptView];
            }else{
                [weakSelf showContentNullPromptNormal];
            }
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========tableviewdelegate==========
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allData.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF;
    HBRecentlyTabCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goods" forIndexPath:indexPath];
    cell.model = self.allData[indexPath.row];
    HBRecentlyModel *model = self.allData[indexPath.row];
    
    cell.clickRecentPhoneBlock = ^(id obj) {
        //点击电话
        [weakSelf telPhone:model];
        
    };
    cell.clickRecentAddressBlock = ^(id obj) {
        //点击地址
        [weakSelf clickAddress:model];
    };
    
    return cell;
    
}
#pragma mark ==========拨打电话==========
-(void)telPhone:(HBRecentlyModel *)model{
    //拨打电话
    self.selectModel = model;
    NSString *phoneStr = nil;
    if (model.tel.length>0) {
        phoneStr = model.tel;
    }else{
        phoneStr = model.mobile;
    }
    NSString *message = [NSString stringWithFormat:@"确认拨打店铺电话%@",phoneStr];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    alert.tag = 1001;
    alert.delegate = self;
    [alert show];
}
#pragma mark ==========alertViewDelegate==========
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1001) {
        if (buttonIndex==1) {
            NSString *phoneStr = nil;
            if (self.selectModel.tel.length>0) {
                phoneStr = self.selectModel.tel;
            }else{
                phoneStr = self.selectModel.mobile;
            }
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",phoneStr];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }
}
#pragma mark ==========点击地址==========
-(void)clickAddress:(HBRecentlyModel *)model{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"shop_id"] = model.shop_id;
    [LBService post:GOODS_SHOP_BASIC params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功
            HBShopInfoModel *model = [HBShopInfoModel mj_objectWithKeyValues:response.result[@"data"]];
            HBShowShopMapVC *vc= [[HBShowShopMapVC alloc] initWithNibName:@"HBShowShopMapVC" bundle:nil];
            vc.model = model;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HBRecentlyModel *model = self.allData[indexPath.row];
    ShopViewController *vc = [[ShopViewController alloc] initWithNibName:@"ShopViewController" bundle:nil];
    vc.shop_id = model.shop_id;
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
