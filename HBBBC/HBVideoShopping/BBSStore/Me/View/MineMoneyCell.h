//
//  MineMoneyCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBCengerIndexModel.h"
@interface MineMoneyCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (nonatomic, strong) void(^select)(NSInteger selectIndex);
@property (nonatomic, strong) HBCengerIndexModel * model;
@property (weak, nonatomic) IBOutlet UILabel *youhuiLab;
@property (weak, nonatomic) IBOutlet UILabel *jifenLab;
@property (weak, nonatomic) IBOutlet UIButton *clickMoneyBtn;

- (IBAction)clickBtn:(UIButton *)sender;

@end
