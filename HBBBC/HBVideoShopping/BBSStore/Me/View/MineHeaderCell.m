//
//  MineHeaderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MineHeaderCell.h"

@implementation MineHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.headerImage.layer.cornerRadius = 25;
    self.headerImage.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchName)];
    [self.serName addGestureRecognizer:tap];
}
- (IBAction)clickCodeButton:(id)sender {
    self.clickCodeAction(nil);
}

- (void)setModel:(HBCengerIndexModel *)model{
    _model = model;
    if ([HBHuTool isJudgeString:model.gradeInfo.grade_name]) {
        if([HBHuTool isJudgeString:model.username]){
           self.custCalss.text = @"天宫";
        }else{
            self.custCalss.text = [NSString stringWithFormat:@"%@",model.username];
        }
        
    }else{
        if([HBHuTool isJudgeString:model.username]){
            self.custCalss.text = @"天宫";
        }else{
            self.custCalss.text = [NSString stringWithFormat:@"%@",model.gradeInfo.grade_name];
        }
        
    }
    if ([HBHuTool isJudgeString:curUser.portrait_url]) {
        self.headerImage.image = [UIImage imageNamed:@"bbs_header"];
    }else{
        [self.headerImage sd_setImageWithURL:[NSURL URLWithString:curUser.portrait_url] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    }
    

        if([HBHuTool isJudgeString:model.username]){
            self.serName.text = @"天宫";
        }else{
            self.serName.text = [NSString stringWithFormat:@"%@",model.username];
        }
        

        if([HBHuTool isJudgeString:curUser.username]){
            self.serName.text = @"天宫";
        }else{
            self.serName.text = [NSString stringWithFormat:@"%@",curUser.username];
        }
    
    if ([self.serName.text isEqualToString:@"null"]) {
        self.serName.text = @"天宫";
    }
        
    
    if ([userManager oneceJudgeLoginState]) {
        //登录
        self.custCalss.hidden = NO;
        self.serName.hidden = NO;
        self.dengLuLab.hidden = YES;
    }else{
        self.custCalss.hidden = YES;
        self.serName.hidden = YES;
        self.dengLuLab.hidden = NO;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)touchName
{
    self.nameAction(nil);
}

@end
