//
//  MineHeaderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBCengerIndexModel.h"
@interface MineHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *custCalss;//会员等级
@property (weak, nonatomic) IBOutlet UIImageView *code;//二维码

@property (weak, nonatomic) IBOutlet UILabel *serName;//
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;


@property (nonatomic, strong) void(^nameAction)(id objc);

@property (nonatomic, strong) void(^clickCodeAction)(id objc);
@property (weak, nonatomic) IBOutlet UILabel *dengLuLab;
@property (nonatomic, strong) HBCengerIndexModel * model;
@end
