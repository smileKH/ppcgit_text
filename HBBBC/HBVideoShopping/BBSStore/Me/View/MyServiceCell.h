//
//  MyServiceCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyServiceCell : UITableViewCell
@property (nonatomic, strong) void(^select)(NSInteger selectIndex);

-(IBAction)selectTip:(UIButton *)sender;
@end
