//
//  MineMoneyCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MineMoneyCell.h"

@implementation MineMoneyCell
- (void)setModel:(HBCengerIndexModel *)model{
    _model = model;
    if ([HBHuTool isJudgeString:model.coupon_num]) {
        self.youhuiLab.text = @"0张";
    }else{
        self.youhuiLab.text = [NSString stringWithFormat:@"%@张",model.coupon_num];
    }
    
    if ([HBHuTool isJudgeString:model.point]) {
        self.jifenLab.text = @"0";
    }else{
        self.jifenLab.text = [NSString stringWithFormat:@"%@",model.point];
    }
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction1)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction2)];
    [self.view1 addGestureRecognizer:tap1];
    [self.view2 addGestureRecognizer:tap2];
}
- (void)tapAction1
{
    self.select(1);
}

- (void)tapAction2
{
    self.select(2);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickBtn:(UIButton *)sender {
}
@end
