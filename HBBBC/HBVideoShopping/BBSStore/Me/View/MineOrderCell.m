//
//  MineOrderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MineOrderCell.h"

@implementation MineOrderCell
- (void)setModel:(HBCengerIndexModel *)model{
    _model = model;
    if (model.wait_pay_num==0) {
        self.fukuanLab.hidden = YES;//待付款
    }else{
        self.fukuanLab.hidden = NO;
        self.fukuanLab.text = [NSString stringWithFormat:@"%ld",model.wait_pay_num];
    }
    
    if (model.wait_send_goods_num==0) {
        self.fahuoLab.hidden = YES;//发货
    }else{
        self.fahuoLab.hidden = NO;
        self.fahuoLab.text = [NSString stringWithFormat:@"%ld",model.wait_send_goods_num];
    }
    
    if (model.wait_confirm_goods_num==0) {
        self.shouhuoLab.hidden = YES;//收货
    }
    else{
        self.shouhuoLab.hidden = NO;
        self.shouhuoLab.text = [NSString stringWithFormat:@"%ld",model.wait_confirm_goods_num];
    }
    
    if (model.notrate_num==0) {
        self.pingjiaLab.hidden = YES;//评价
    }else{
        self.pingjiaLab.hidden = NO;
        self.pingjiaLab.text = [NSString stringWithFormat:@"%ld",model.notrate_num];
    }
    
    if (model.canceled_num==0) {
        self.quxiaoLab.hidden = YES;//取消
    }else{
        self.quxiaoLab.hidden = NO;
        self.quxiaoLab.text = [NSString stringWithFormat:@"%ld",model.canceled_num];
    }
   
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectTip:(UIButton *)sender {
    self.select(sender.tag);
}
- (IBAction)clickMyOrderBtn:(UIButton *)sender {
    self.select(sender.tag);
}
@end
