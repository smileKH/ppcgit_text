//
//  MyServiceCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyServiceCell.h"

@implementation MyServiceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(IBAction)selectTip:(UIButton *)sender
{
    self.select(sender.tag);
}
@end
