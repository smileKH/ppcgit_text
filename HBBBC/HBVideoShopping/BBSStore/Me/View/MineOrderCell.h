//
//  MineOrderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBCengerIndexModel.h"
@interface MineOrderCell : UITableViewCell
- (IBAction)selectTip:(UIButton *)sender;

@property (nonatomic, strong) void(^select)(NSInteger selectIndex);
- (IBAction)clickMyOrderBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *fukuanLab;
@property (weak, nonatomic) IBOutlet UILabel *fahuoLab;
@property (weak, nonatomic) IBOutlet UILabel *shouhuoLab;
@property (weak, nonatomic) IBOutlet UILabel *pingjiaLab;
@property (weak, nonatomic) IBOutlet UILabel *quxiaoLab;

@property (nonatomic, strong) HBCengerIndexModel * model;



@end
