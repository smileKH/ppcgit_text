//
//  HBCengerIndexModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/22.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBGradeInfoModel;
@class Cur_symbol;
@interface HBCengerIndexModel : NSObject
@property (nonatomic, strong) HBGradeInfoModel * gradeInfo;
@property (nonatomic, assign) NSInteger wait_pay_num;//待付款
@property (nonatomic, assign) NSInteger wait_send_goods_num;//待发货
@property (nonatomic, assign) NSInteger wait_confirm_goods_num;//待收货
@property (nonatomic, assign) NSInteger canceled_num;//已取消
@property (nonatomic, assign) NSInteger voucher_num;
@property (nonatomic, assign) NSInteger  notrate_num;//待评价
@property (nonatomic, strong) NSString * deposit;//我的资产
@property (nonatomic, strong) NSString * coupon_num;//优惠券
@property (nonatomic, strong) NSString * point;//积分
@property (nonatomic, strong) Cur_symbol * cur_symbol;//待付款
@property (nonatomic, strong) NSString * username;//用户名

@end


@interface HBGradeInfoModel : NSObject
@property (nonatomic, strong) NSString * grade_id;//等级
@property (nonatomic, strong) NSString * experience;//经验
@property (nonatomic, strong) NSString * grade_name;//会员等级名
@property (nonatomic, strong) NSString * grade_logo;//等级logo

@end
