//
//  MineViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/4.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineViewController : HBRootViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
