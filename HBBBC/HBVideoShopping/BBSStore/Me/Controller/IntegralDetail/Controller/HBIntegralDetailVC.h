//
//  HBIntegralDetailVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/27.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBIntegralDetailVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *totalIntegralLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
