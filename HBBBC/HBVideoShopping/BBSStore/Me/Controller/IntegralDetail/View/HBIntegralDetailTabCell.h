//
//  HBIntegralDetailTabCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/28.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBIntegralDetailModel.h"
@interface HBIntegralDetailTabCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *integralLab;
@property (nonatomic, strong) HBIntegralDetailListModel * model;
@end
