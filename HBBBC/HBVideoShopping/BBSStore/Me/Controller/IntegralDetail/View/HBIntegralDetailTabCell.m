//
//  HBIntegralDetailTabCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/28.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBIntegralDetailTabCell.h"

@implementation HBIntegralDetailTabCell
- (void)setModel:(HBIntegralDetailListModel *)model{
    _model = model;
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.behavior];
    NSString *dateStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.modified_time];
    self.dateLab.text = dateStr;
    if ([model.behavior_type isEqualToString:@"consume"]) {//-
        self.integralLab.text = [NSString stringWithFormat:@"-%@",model.point];
    }else{
        self.integralLab.text = [NSString stringWithFormat:@"+%@",model.point];
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
