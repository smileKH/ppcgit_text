//
//  HBIntegralDetailModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/28.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBIntegralDetailListModel;
@class Pagers;
@class HBPoint_total;
@interface HBIntegralDetailModel : NSObject
@property (nonatomic, strong)NSArray  * list;
@property (nonatomic, strong) Pagers * pagers;
@property (nonatomic, strong) HBPoint_total * point_total;
@end


@interface HBIntegralDetailListModel : NSObject
@property (nonatomic, strong) NSString * behavior_type;
@property (nonatomic, strong) NSNumber * modified_time;
@property (nonatomic, strong) NSString * behavior;
@property (nonatomic, strong) NSString * point;
@property (nonatomic, strong) NSString * remark;
@property (nonatomic, strong) NSString * pointlog_id;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * expiration_time;
@end

@interface HBPoint_total : NSObject
@property (nonatomic, strong) NSNumber * expired_time;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * modified_time;
@property (nonatomic, strong) NSString * point_count;
@property (nonatomic, strong) NSString * expired_point;
@end

