//
//  HBMineViewController.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBMineViewController.h"
#import "MineHeaderCell.h"

#import "MineOrderCell.h"
#import "MineMoneyCell.h"
#import "MyServiceCell.h"

#import "LoginViewController.h"
#import "SetViewController.h"
#import "MyOrderViewController.h"

#import "UserCenterViewController.h"
#import "MyDiscussController.h"

#import "MyCollectViewController.h"
#import "MyCouponsViewController.h"
#import "MySafeViewController.h"
#import "MyComplainViewController.h"
#import "MyReutrnGoodsViewController.h"
#import "HBMyCodePushView.h"
#import "HBCancelOrderVC.h"
#import "HBCengerIndexModel.h"
#import "HBIntegralDetailVC.h"
#import "HBReandCollectVC.h"
#import "HBReandManageSeeVC.h"
@interface HBMineViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong)UITableView *tableView;
@property (nonatomic, strong) HBMyCodePushView * myCodeView;
@property (nonatomic, strong) HBCengerIndexModel * indelModel;
@end

@implementation HBMineViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
    self.StatusBarStyle = UIStatusBarStyleLightContent;
    //判断
    if ([userManager oneceJudgeLoginState]) {
        //请求未读信息数量
        [self requestCenterData];
        
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
    self.StatusBarStyle = UIStatusBarStyleDefault;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //个人中心
    [self setuptableview];
     
   
}
#pragma mark ==========请求个人中心资料==========
-(void)requestCenterData{
    //个人中心资料
    WEAKSELF;
    [LBService post:MEMBER_INDEX_NUMBER params:@{@"":@""} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            weakSelf.indelModel = [HBCengerIndexModel mj_objectWithKeyValues:response.result[@"data"]];
            //更新个人中心资料
            [weakSelf updateCenterData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========更新个人中心资料==========
-(void)updateCenterData{
    [self.tableView reloadData];
}
//退出登录，刷新界面
- (void)logoutSuccess:(NSNotification *)noti{
    self.indelModel = nil;
    [self.tableView reloadData];
}
- (void)setuptableview
{
    
    //    self.tableview.contentInset = UIEdgeInsetsMake(-bNavAllHeight, 0, 0, 0);
    //隐藏导航栏
    self.isHidenNaviBar = YES;
    self.StatusBarStyle = UIStatusBarStyleLightContent;
    self.isShowLiftBack = NO;//每个根视图需要设置该属性为NO，否则会出现导航栏异常
    
    [self.view addSubview:self.tableView];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MineHeaderCell" bundle:nil] forCellReuseIdentifier:@"header"];
    [self.tableView registerNib:[UINib nibWithNibName:@"MineOrderCell" bundle:nil] forCellReuseIdentifier:@"order"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MineMoneyCell" bundle:nil] forCellReuseIdentifier:@"money"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MyServiceCell" bundle:nil] forCellReuseIdentifier:@"service"];
    
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-bTabBarHeight)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    }
    return _tableView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    WEAKSELF;
    if (indexPath.section ==0) {
        MineHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"header"];
        cell.model = self.indelModel;
        cell.nameAction = ^(id objc) {
            if ([weakSelf getJudgeLoginState]) {
                UserCenterViewController *vc= [[UserCenterViewController alloc] initWithNibName:@"UserCenterViewController" bundle:nil];
                vc.model = self.indelModel;
                [self.navigationController pushViewController:vc animated:YES];
            }
        };
        
        cell.clickCodeAction = ^(id objc) {
            if ([weakSelf getJudgeLoginState]) {
                //弹出二维码
                weakSelf.myCodeView = [[HBMyCodePushView alloc]init];
                [weakSelf.myCodeView show];
            }
            
        };
        
        return cell;
    }else if (indexPath.section ==1) {
        MineOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"order"];
        cell.model = self.indelModel;
        cell.select = ^(NSInteger selectIndex) {
            [self selectMyOrder:selectIndex];
        };
        return cell;
    }else if (indexPath.section ==2) {
        MineMoneyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"money"];
        cell.model = self.indelModel;
        cell.select = ^(NSInteger selectIndex) {
            [self selectMyMoney:selectIndex];
        };
        return cell;
    }else{
        MyServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"service"];
        cell.select = ^(NSInteger selectIndex) {
            [self selectMyService:selectIndex];
        };
        return cell;
    }
    
}
//判断登录状态
-(BOOL)getJudgeLoginState{
    //登录成功
    //    return YES;
    if ([userManager judgeLoginState]) {
        //登录成功
        return YES;
    }
    else{
        //还没有登录
        return NO;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0) {
        return 145;
    }else if (indexPath.section ==1) {
        return 145;
    }else if (indexPath.section ==2) {
        return 131;
    }else{
        return SCREEN_WIDTH/4 *2 + 45 +10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1 || section == 2) {
        return 4;
    }
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 4)];
    footer.backgroundColor = UIColorFromHex(0xF2F2F2);
    return footer;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if ([self getJudgeLoginState]) {
        UserCenterViewController *vc= [[UserCenterViewController alloc] initWithNibName:@"UserCenterViewController" bundle:nil];
            vc.model = self.indelModel;
        [self.navigationController pushViewController:vc animated:YES];
        }
    }
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}


#pragma mark   我的服务
-(void)selectMyService:(NSInteger)selectTag
{
    //1=收藏 2=退换货 3=安全中心 4=评价 5=投诉 6设置
    if ([self getJudgeLoginState]) {
        
        
        
        switch (selectTag) {
            case 1:
            {   //收藏
                MyCollectViewController *vc= [[MyCollectViewController alloc] initWithNibName:@"MyCollectViewController" bundle:nil];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 2:
            {
                //退换货
                MyReutrnGoodsViewController *vc= [[MyReutrnGoodsViewController alloc] initWithNibName:@"MyReutrnGoodsViewController" bundle:nil];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 3:
            {
                //安全中心
                MySafeViewController *vc= [[MySafeViewController alloc] initWithNibName:@"MySafeViewController" bundle:nil];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 4:
            {   //评价
                MyDiscussController *vc= [[MyDiscussController alloc] initWithNibName:@"MyDiscussController" bundle:nil];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 5:
            {
                //投诉
                MyComplainViewController *vc= [[MyComplainViewController alloc] initWithNibName:@"MyComplainViewController" bundle:nil];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            case 6:
            {   //设置
                SetViewController *vc= [[SetViewController alloc] initWithNibName:@"SetViewController" bundle:nil];
                vc.model = self.indelModel;
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 7:
            {
                //需求管理
                HBReandManageSeeVC *vc= [[HBReandManageSeeVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            case 8:
            {   //需求收藏
                HBReandCollectVC *vc= [[HBReandCollectVC alloc] init];
//                vc.model = self.indelModel;
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
                
            default:
                break;
        }
        
    }
}
#pragma mark   我的资产
-(void)selectMyMoney:(NSInteger)selectTag
{
    // 1=优惠券   2=积分
    if ([self getJudgeLoginState]) {
        
        if (selectTag == 1) {
            
            MyCouponsViewController *vc= [[MyCouponsViewController alloc] initWithNibName:@"MyCouponsViewController" bundle:nil];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
           
            HBIntegralDetailVC *vc= [[HBIntegralDetailVC alloc] initWithNibName:@"HBIntegralDetailVC" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}
#pragma mark   我的订单
-(void)selectMyOrder:(NSInteger)selectTag
{
    if ([self getJudgeLoginState]) {
        if (selectTag==6) {
            //点击取消订单
            HBCancelOrderVC *vc = [[HBCancelOrderVC alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
        MyOrderViewController *vc = [[MyOrderViewController alloc] initWithNibName:@"MyOrderViewController" bundle:nil];
        vc.selectTopIndex = selectTag-1;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
