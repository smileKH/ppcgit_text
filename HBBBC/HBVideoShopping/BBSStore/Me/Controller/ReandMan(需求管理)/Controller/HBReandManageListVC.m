//
//  HBReandManageListVC.m
//  HBVideoShopping
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBReandManageListVC.h"
#import "HBReandManageListTabCell.h"
#import "HBDemandListModel.h"
#import "HBReandDetailVC.h"
@interface HBReandManageListVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView * tableView;
@property (nonatomic ,strong)NSString *selectTitleStr;//选择按钮
@property (nonatomic ,strong)NSIndexPath *selectIndexPath;//选择
@end

@implementation HBReandManageListVC
#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

- (void)listDidAppear {}

- (void)listDidDisappear {}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = white_Color;
    [self setTableView];
    //请求数据
    [self requestReandManageList:YES];
    
    //通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateSuccessChange) name:@"HBDemandPushSuccessChange" object:nil];
}
#pragma mark ==========通知改变==========
-(void)updateSuccessChange{
    //请求数据
    [self requestReandManageList:YES];
}
- (void)setTableView{
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT - 42 - bNavAllHeight ) style:UITableViewStyleGrouped];
    tableView.backgroundColor = white_Color;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.estimatedRowHeight = 100;
    tableView.estimatedSectionHeaderHeight = 0;
    tableView.estimatedSectionFooterHeight = 0;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
//    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:deCellID];
//    [tableView registerClass:[HBGoodsEvaluationTabCell class] forCellReuseIdentifier:@"HBGoodsEvaluationTabCell"];
//    [tableView registerClass:[HBGoodsNameTabCell class] forCellReuseIdentifier:@"HBGoodsNameTabCell"];
//    [tableView registerClass:[HBSecurityTabCell class] forCellReuseIdentifier:@"HBSecurityTabCell"];
//    [tableView registerClass:[HBGoodsShopMessageTabCell class] forCellReuseIdentifier:@"HBGoodsShopMessageTabCell"];
//    tableView.tableHeaderView = self.headerView;
    //添加子视图
    [self.view addSubview:tableView];
    self.tableView = tableView;
    if (@available(iOS 11.0, *)) {
        tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    WEAKSELF;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //请求数据
        [weakSelf requestReandManageList:YES];
    }];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        //请求数据
        [weakSelf requestReandManageList:NO];
    }];
    
    
}
- (void)requestReandManageList:(BOOL)isRefresh
{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    parameters[@"page_size"] = @(10);
    parameters[@"page"] = @(self.pageNo);
    if (self.index==0) {
        parameters[@"demand_status"] = @"unsolved";
    }else{
        parameters[@"demand_status"] = @"solved";
    }
    parameters[@"search_user_id"] = userManager.curUserInfo.user_id;
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:DEMAND_INDEX_LIST_FAUTH params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            if (weakSelf.pageNo==1) {
                [self.allData removeAllObjects];
            }
            [weakSelf.tableView.mj_footer endRefreshing];
            [weakSelf.tableView.mj_header endRefreshing];
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
//                NSDictionary *pagDic = dic[@"pagers"];
//                int number = [pagDic[@"current_page"]intValue];
                NSArray *arr = [dic objectForKey:@"demand_list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [HBDemandListModel mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        //设置视图
                        //[weakSelf setGoodsViewController];
                    }
                }else{
                    if (weakSelf.pageNo>0) {
                        [app showToastView:Text_NoMoreData];
                        [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                    }
                }
                
            }
        }else{
            [app showToastView:response.message];
        }
        //        weakSelf.collectionView.dataArray = weakSelf.allData;
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptNormal];
        }
        [weakSelf.tableView reloadData];
       
    }];
}
#pragma mark ==========tableViewDelegate==========
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.allData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HBReandManageListTabCell *cell = [HBReandManageListTabCell cellWithTableView:tableView];
    cell.selectionStyle = 0;
    cell.model = self.allData[indexPath.section];
    WEAKSELF;
    cell.clickMoreChangeBtn = ^(NSString * _Nonnull titleStr) {
        //点击了按钮
        weakSelf.selectTitleStr = titleStr;
        weakSelf.selectIndexPath = indexPath;
        [weakSelf reandChangeTitleMonth];
    };
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
    footerView.backgroundColor = Color_BG;
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
    headerView.backgroundColor = Color_BG;
    return headerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HBDemandListModel *model =   self.allData[indexPath.section];
    HBReandDetailVC *vc = [[HBReandDetailVC alloc]init];
    vc.listModel = model;
    [self.upController.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========点击操作事件==========
-(void)reandChangeTitleMonth{
    //提示
    NSString *message = nil;
    if ([self.selectTitleStr isEqualToString:@"删除"]) {
        message = @"是否删除该需求？";
    }else{
        message = @"确定需求已经解决？";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    alert.tag = 1001;
    alert.delegate = self;
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        //取消
    }else{
        //确定
        if ([self.selectTitleStr isEqualToString:@"删除"]) {
            //删除
            [self deleteReandMonth];
        }else{
            //已解决
            [self changeReandMonth];
        }
    }
}
#pragma mark ==========删除需求==========
-(void)deleteReandMonth{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    HBDemandListModel *model = self.allData[self.selectIndexPath.section];
    parameters[@"demand_id"] = model.demand_id;
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:DEMAND_DEMAND_DELETE params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
//            [MBProgressHUD showInfoMessage:@"删除成功"];
            //刷新列表
            [weakSelf requestReandManageList:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========已解决需求==========
-(void)changeReandMonth{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    HBDemandListModel *model = self.allData[self.selectIndexPath.section];
    parameters[@"demand_id"] = model.demand_id;
    parameters[@"demand_status"] = @"solved";
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:DEMAND_DEMAND_UPDATE params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //            [MBProgressHUD showInfoMessage:@"成功"];
            //刷新列表
            [weakSelf requestReandManageList:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
