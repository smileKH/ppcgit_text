//
//  HBReandManageSeeVC.m
//  HBVideoShopping
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBReandManageSeeVC.h"
#import "HBReandManageListVC.h"
#import "HBDemandPushVC.h"
@interface HBReandManageSeeVC ()<UITextFieldDelegate>
{
    CGFloat categoryViewHeight;
}
@property (nonatomic, copy) NSArray *titles;
@property (nonatomic, strong) JXCategoryTitleView *myCategoryView;
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;
@property (nonatomic ,strong)UIButton *searchBtn;//搜索按钮
@property (nonatomic ,strong)NSString *textFieldStr;
@property (nonatomic ,strong)NSString *keyboreType;
@end
@implementation HBReandManageSeeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = white_Color;
    self.title = @"需求管理";
    //隐藏导航栏
    self.isHidenNaviBar = NO;
    //设置导航栏颜色
    //    self.StatusBarStyle = UIStatusBarStyleLightContent;
    self.isShowLiftBack = YES;//每个根视图需要设置该属性为NO，否则会出现导航栏异常
    //设置界面
    [self initDate];
    //设置滚动视图
    [self setupCategoryView];
    
    [self setupNavBar];
}

-(void)setupNavBar{
    UIButton *messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    messageBtn.bounds  = CGRectMake(0, 0, 25, 25);
//    //    [messageBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, -6)];
//    [messageBtn setBackgroundImage:ImageNamed(@"bbs_search") forState:UIControlStateNormal];
//    //[messageBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
//    messageBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
    [messageBtn setTitle:@"需求发布" forState:UIControlStateNormal];
    [messageBtn setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
    messageBtn.titleLabel.font = Font(14);
    [messageBtn addTarget:self action:@selector(clickMessage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:messageBtn];
    self.navigationItem.rightBarButtonItem = item;
    
}
//点击消息
-(void)clickMessage:(UIButton *)buton{
    HBDemandPushVC *vc = [[HBDemandPushVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ==========更新数据==========
-(void)updateMaterialData{
    [self.listContainerView reloadData];
    [self.myCategoryView reloadData];
}

#pragma mark =======初始化滚动视图==========
- (void)initDate{
    categoryViewHeight = 42.0;
    self.titles = @[@"待解决",@"已解决"];
}


- (void)setupCategoryView{
    
    JXCategoryTitleView *categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, categoryViewHeight)];
    categoryView.delegate = self;
    [self.view addSubview:categoryView];
    //添加按钮
    categoryView.titles = self.titles;
    categoryView.titleFont =  Font(14);
    categoryView.titleSelectedFont = Font(16);
    categoryView.titleSelectedColor = MAINTextCOLOR;
    categoryView.titleColor = black_Color;
    categoryView.averageCellSpacingEnabled = YES;
    categoryView.backgroundColor = RGBA(247, 247, 247, 1);
    categoryView.titleColorGradientEnabled = YES;
    categoryView.titleLabelZoomEnabled = YES;
    
    
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.componentPosition = JXCategoryViewAutomaticDimension;
    lineView.indicatorHeight = 2.0;
    lineView.indicatorColor = MAINTextCOLOR;
    lineView.lineStyle = JXCategoryIndicatorLineStyle_Normal;
    categoryView.indicators = @[lineView];
    
    self.myCategoryView = categoryView;
    
    //self.listContainerView = [[JXCategoryListContainerView alloc] initWithDelegate:self];
    self.listContainerView = [[JXCategoryListContainerView alloc]initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    // self.listContainerView.didAppearPercent = 0.99; //滚动一点就触发加载
    self.listContainerView.defaultSelectedIndex = 0;
    self.listContainerView.frame = CGRectMake(0, categoryViewHeight+1, SCREEN_WIDTH, SCREEN_HEIGHT - categoryViewHeight - bNavAllHeight);
    [self.view addSubview:self.listContainerView];
    
    self.myCategoryView.contentScrollView = self.listContainerView.scrollView;
    
}

#pragma mark - JXCategoryViewDatasource
- (id<JXCategoryListContentViewDelegate>)preferredListAtIndex:(NSInteger)index {
    HBReandManageListVC *vc = [[HBReandManageListVC alloc]init];
    vc.index = index;
    vc.upController = self;
    return vc;
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didClickSelectedItemAtIndex:(NSInteger)index {
    
    [self.listContainerView didClickSelectedItemAtIndex:index];
}

- (void)categoryView:(JXCategoryBaseView *)categoryView scrollingFromLeftIndex:(NSInteger)leftIndex toRightIndex:(NSInteger)rightIndex ratio:(CGFloat)ratio {
    [self.listContainerView scrollingFromLeftIndex:leftIndex toRightIndex:rightIndex ratio:ratio selectedIndex:categoryView.selectedIndex];
}

#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    id<JXCategoryListContentViewDelegate> list = [self preferredListAtIndex:index];
    return list;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

@end
