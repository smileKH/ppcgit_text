//
//  HBReandManageListVC.h
//  HBVideoShopping
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HBReandManageListVC : HBRootViewController<JXCategoryViewDelegate,JXCategoryListContentViewDelegate>
@property (nonatomic ,assign)NSInteger index;//0、待解决 1、已解决
@property (nonatomic ,weak)UIViewController *upController;
@end

NS_ASSUME_NONNULL_END
