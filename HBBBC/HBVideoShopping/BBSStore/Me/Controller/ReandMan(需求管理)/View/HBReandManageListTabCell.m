//
//  HBReandManageListTabCell.m
//  HBVideoShopping
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBReandManageListTabCell.h"
#import "UIButton+ImageTitleSpacing.h"
@interface HBReandManageListTabCell ()
@property (nonatomic ,strong)UILabel *timeLabel;//时间
@property (nonatomic ,strong)UILabel *stateLabel;//状态
@property (nonatomic ,strong)UIView *topLineView;//一根线
@property (nonatomic ,strong)UILabel *titleLabel;//标题
@property (nonatomic ,strong)UIImageView *imgView;//图片
@property (nonatomic ,strong)UIView *moreView;//更多按钮
@property (nonatomic ,strong)UIView *botLineView;//下面线
@property (nonatomic ,strong)UIView *btnOtherView;//下面更多按钮
@property (nonatomic ,strong)MASConstraint *bgImgViewWidth;
@property (nonatomic, strong) NSMutableArray *btnArr;
@end
@implementation HBReandManageListTabCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUp];
    }
    return self;
}
-(void)setModel:(HBDemandListModel *)model{
    _model = model;
    CGFloat layoutWidth = 112;
    if ([HBHuTool isJudgeString:model.video_path]) {
        //为空，是图片
        CGFloat retrHeight = [model.image_default_id.height floatValue];
        CGFloat retrWidth = [model.image_default_id.width floatValue];
        CGFloat scaleWidth =  retrWidth*layoutWidth/retrHeight;
        if (scaleWidth>layoutWidth) {
            scaleWidth = layoutWidth;
            [self.bgImgViewWidth uninstall];
            [self.imgView mas_updateConstraints:^(MASConstraintMaker *make) {
                self.bgImgViewWidth = make.width.mas_equalTo(scaleWidth);
            }];
            self.imgView.contentMode = UIViewContentModeScaleAspectFill;
            self.imgView.clipsToBounds = YES;
        }else{
            if (scaleWidth>0) {
                [self.bgImgViewWidth uninstall];
                [self.imgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    self.bgImgViewWidth = make.width.mas_equalTo(scaleWidth);
                }];
                self.imgView.contentMode = UIViewContentModeScaleAspectFit;
            }
            
        }
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.image_default_id.url] placeholderImage:ImageNamed(ZAN_WU_TUPIAN)];
        //
        
    }else{
        CGFloat retrHeight = [model.video_pic_url.height floatValue];
        CGFloat retrWidth = [model.video_pic_url.width floatValue];
        CGFloat scaleWidth =  retrWidth*layoutWidth/retrHeight;
        if (scaleWidth>layoutWidth) {
            scaleWidth = layoutWidth;
            [self.bgImgViewWidth uninstall];
            [self.imgView mas_updateConstraints:^(MASConstraintMaker *make) {
               self.bgImgViewWidth = make.width.mas_equalTo(scaleWidth);
            }];
            self.imgView.contentMode = UIViewContentModeScaleAspectFill;
            self.imgView.clipsToBounds = YES;
        }else{
            if (scaleWidth>0) {
                [self.bgImgViewWidth uninstall];
                [self.imgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    self.bgImgViewWidth = make.width.mas_equalTo(scaleWidth);
                }];
                self.imgView.contentMode = UIViewContentModeScaleAspectFit;
            }
            
        }
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.video_pic_url.url] placeholderImage:ImageNamed(ZAN_WU_TUPIAN)];
        
    }
    
    self.titleLabel.text = model.demand_desc;
    self.timeLabel.text = [XH_Date_String dateStringWithFormat: @"yyyy-MM-dd" tenNumber:model.created_time];
    if ([model.demand_status isEqualToString:@"solved"]) {
        self.stateLabel.text = @"已解决";
        [self setBotttomBtnMenu:@[@"删除"]];
    }else{
        self.stateLabel.text = @"待解决";
        [self setBotttomBtnMenu:@[@"已解决",@"删除"]];
    }
    
    
    NSString *oneStr = [NSString stringWithFormat:@"投诉 %@",model.complaint]; //投诉
    NSString *twoStr = [NSString stringWithFormat:@"收藏 %@",model.favorite];//收藏
    NSString *threeStr = [NSString stringWithFormat:@"评论 %@",model.comment];//评论
    NSString *fourStr = [NSString stringWithFormat:@"点赞 %@",model.like];//点赞
    NSArray *titleArr = @[oneStr,twoStr,threeStr,fourStr];

    BOOL iscollect = [model.is_favorite boolValue];
    BOOL isLike = [model.is_like boolValue];
    NSArray *selectArr = @[@(NO),@(iscollect),@(NO),@(isLike)];

    for (UIButton *btn in _btnArr) {
        NSInteger index = [_btnArr indexOfObject:btn];
        BOOL isSelect = [[selectArr objectAtIndex:index] boolValue];
        NSString *title = [titleArr objectAtIndex:index];
        [btn setTitle:title forState:UIControlStateNormal];
        btn.selected = isSelect;
        [btn layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleLeft imageTitleSpace:5];
    }
}
-(void)setUp{
    [self addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(15);
        make.height.mas_equalTo(12);
    }];
    
    [self addSubview:self.stateLabel];
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.timeLabel);
        make.right.equalTo(self).offset(-15);
        make.height.mas_equalTo(12);
    }];
    
    [self addSubview:self.topLineView];
    [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeLabel.mas_bottom).offset(10);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topLineView.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-15);
        make.left.equalTo(self).offset(15);
    }];
    
    [self addSubview:self.imgView];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.equalTo(self).offset(15);
        make.height.mas_equalTo(112);
        self.bgImgViewWidth = make.width.mas_equalTo(112);
//        make.size.mas_equalTo(CGSizeMake(112, 112));
    }];
    
    [self addSubview:self.moreView];
    [self.moreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgView.mas_bottom).offset(5);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(30);
    }];
    
    
    [self addSubview:self.botLineView];
    [self.botLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.moreView.mas_bottom).offset(5);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
    [self addSubview:self.btnOtherView];
    [self.btnOtherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.botLineView.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(50);
        make.bottom.equalTo(self);
    }];
    
    [self addMoreViewButton];
}
#pragma mark ==========添加子视图==========
-(void)addMoreViewButton{
    NSArray *bottomTitleArr = @[@"投诉 0",@"收藏 0",@"评论 0",@"点赞 0"];
    NSArray *bottomImageArr = @[@"bs_toushu_img",@"bs_shouchang_coll",@"bs_pinglun_img",@"bs_dianzan_no"];
    NSArray *bottomSelectImageArr = @[@"bs_toushu_img",@"bs_shouchang_collSelect",@"bs_pinglun_img",@"bs_dianzan_select"];
    NSMutableArray *btnArr = [NSMutableArray array];
    for (int i = 0; i<bottomTitleArr.count; i++) {
        NSString *title = [bottomTitleArr objectAtIndex:i];
        NSString *imageName = [bottomImageArr objectAtIndex:i];
        NSString *selectImageName = [bottomSelectImageArr objectAtIndex:i];
        UIButton *btn = [[UIButton alloc] init];
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:selectImageName] forState:UIControlStateSelected];
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:black_Color forState:UIControlStateNormal];
        btn.titleLabel.font = Font(10);
        [self.moreView addSubview:btn];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleLeft imageTitleSpace:5];
        btn.tag = i;
        [btnArr addObject:btn];
        
    }
    _btnArr = btnArr;
    WEAKSELF;
    [btnArr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:10 tailSpacing:10];
    [btnArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.moreView).offset(0);
        make.height.mas_equalTo(30);
    }];
}
- (void)btnClick:(UIButton *)sender{
    //    sender.selected = !sender.selected;
    //    if (self.toolClickBlock) {
    //        self.toolClickBlock(sender.tag, sender.selected);
    //    }
    //
    //    if (sender.tag == 1) {
    //        //收藏
    //        [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_PUB_COLLECT object:sender];
    //    }else if (sender.tag == 2){
    //        //点赞
    //        [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_COMMENT_LIKE object:sender];
    //    }
}
#pragma mark =======设置底部按钮==========
-(void)setBotttomBtnMenu:(NSArray *)arrTitle{
    
    [self.btnOtherView removeAllSubview];
    //添加分类
    CGFloat buttonWidth = 71;
    CGFloat buttonHight = 30;
    CGFloat buttonSping = 10;
    CGFloat topSpacing = 10;
    for (int i =0; i<arrTitle.count; i++) {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.frame = CGRectMake(SCREEN_WIDTH-(buttonWidth+buttonSping)*(i+1), topSpacing, buttonWidth, buttonHight);
        NSString *titleStr = arrTitle[i];
        [aButton setTitle:arrTitle[i] forState:UIControlStateNormal];
        if ([titleStr isEqualToString:@"删除"]) {
            [aButton setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
            aButton.layer.borderColor = MAINTextCOLOR.CGColor;
            aButton.layer.borderWidth = 1;
        }else{
            [aButton setTitleColor:white_Color forState:UIControlStateNormal];
            [aButton setBackgroundColor:MAINTextCOLOR];
            aButton.layer.borderColor = MAINTextCOLOR.CGColor;
            aButton.layer.borderWidth = 1;
        }
        aButton.layer.cornerRadius = 4;
        aButton.clipsToBounds = YES;
        aButton.titleLabel.font = Font(15);
        aButton.tag = i;
        [aButton addTarget:self action:@selector(clickMenuBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnOtherView addSubview:aButton];
    }
    
}
#pragma mark ==========点击按钮事件==========
-(void)clickMenuBtn:(UIButton *)button{
    if (self.clickMoreChangeBtn) {
        self.clickMoreChangeBtn(button.titleLabel.text);
    }
    
}
#pragma mark ==========getter==========
-(UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(13);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _timeLabel ;
}
-(UILabel *)stateLabel{
    if (!_stateLabel) {
        _stateLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = MAINTextCOLOR;//设置字体颜色
            label.font = Font(12);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _stateLabel ;
}
-(UIView *)topLineView{
    if (!_topLineView) {
        _topLineView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = RGBA(241, 241, 241, 1);
            
            view ;
        }) ;
    }
    return _topLineView ;
}
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = black_Color;//设置字体颜色
            label.font = Font(14);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _titleLabel ;
}
-(UIImageView *)imgView{
    if (!_imgView) {
        _imgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:ZAN_WU_TUPIAN];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _imgView ;
}
-(UIView *)moreView{
    if (!_moreView) {
        _moreView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _moreView ;
}
-(UIView *)botLineView{
    if (!_botLineView) {
        _botLineView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = RGBA(241, 241, 241, 1);;
            
            view ;
        }) ;
    }
    return _botLineView ;
}
-(UIView *)btnOtherView{
    if (!_btnOtherView) {
        _btnOtherView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _btnOtherView ;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
