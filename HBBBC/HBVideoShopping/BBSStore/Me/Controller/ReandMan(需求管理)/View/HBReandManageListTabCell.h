//
//  HBReandManageListTabCell.h
//  HBVideoShopping
//
//  Created by Mac on 2020/1/5.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBBaseListTableCell.h"
#import "HBDemandListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HBReandManageListTabCell : HBBaseListTableCell
@property (nonatomic ,strong)HBDemandListModel *model;
@property (nonatomic ,copy)void(^clickMoreChangeBtn)(NSString *titleStr);
@end

NS_ASSUME_NONNULL_END
