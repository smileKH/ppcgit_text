//
//  HBSafeModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBSafeModel : NSObject
@property (nonatomic, strong) NSString * userMobile;
@property (nonatomic, assign) NSInteger  hasDepositPassword;
@end
