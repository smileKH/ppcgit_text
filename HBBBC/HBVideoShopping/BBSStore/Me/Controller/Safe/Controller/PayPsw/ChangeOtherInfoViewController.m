//
//  ChangeOtherInfoViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ChangeOtherInfoViewController.h"
#import "HBPayOriginalVC.h"
@interface ChangeOtherInfoViewController ()

@end

@implementation ChangeOtherInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (self.model.hasDepositPassword==1) {
        //验证支付密码
        self.title = @"支付密码";
    }else{
        //验证登录密码
        self.title = @"登录密码密码";
    }
    
    
}

- (IBAction)clickBtn:(UIButton *)sender {
   
    if (self.model.hasDepositPassword==1) {
        //验证支付密码
        if ([HBHuTool isJudgeString:self.inputTf.text]) {
            [app showToastView:@"请输入支付密码"];
            return;
        }
        //验证支付密码
        [self verifyPayPsw];
    }else{
        //验证登录密码
        if ([HBHuTool isJudgeString:self.inputTf.text]) {
            [app showToastView:@"请输入登录密码"];
            return;
        }
        //验证登录密码
        [self verifyLoginPsw];
    }
    
}
#pragma mark ==========验证支付密码==========
-(void)verifyPayPsw{
    [MBProgressHUD showActivityMessageInView:nil];
    WEAKSELF;
    [LBService post:SECURITY_CHECK_PAY_PS params:@{@"pay_password":self.inputTf.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            HBPayOriginalVC *vc= [[HBPayOriginalVC alloc] initWithNibName:@"HBPayOriginalVC" bundle:nil];
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========验证登录密码==========
-(void)verifyLoginPsw{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:nil];
    [LBService post:SECURITY_CHECK_LOGINPS params:@{@"password":self.inputTf.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            HBPayOriginalVC *vc= [[HBPayOriginalVC alloc] initWithNibName:@"HBPayOriginalVC" bundle:nil];
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
            
        }else{
            [app showToastView:response.message];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
