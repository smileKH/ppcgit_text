//
//  HBPayPswChangeVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBPayPswChangeVC.h"
#import "ChangeOtherInfoViewController.h"
#import "HBPayForgetPswVC.h"
@interface HBPayPswChangeVC ()

@end

@implementation HBPayPswChangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"支付密码";
    self.tableView.tableFooterView = [[UIView alloc]init];
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *rid=@"cell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:rid];
    
    if(cell==nil){
        
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault      reuseIdentifier:rid];
        
    }
    if (self.model.hasDepositPassword==1) {
        //已经设置
        if (indexPath.row==0) {
            cell.textLabel.text = @"修改密码";
        }else{
            cell.textLabel.text = @"忘记密码";
        }
    }else{
        //未设置
        cell.textLabel.text = @"设置密码";
    }
    cell.textLabel.font = Font(12);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.model.hasDepositPassword==1) {
        //已经设置
        return 2;
    }else{
        //未设置
        return  1;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        //修改密码
        ChangeOtherInfoViewController *vc= [[ChangeOtherInfoViewController alloc] initWithNibName:@"ChangeOtherInfoViewController" bundle:nil];
        vc.model = self.model;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //忘记密码
        HBPayForgetPswVC *vc = [[HBPayForgetPswVC alloc]initWithNibName:@"HBPayForgetPswVC" bundle:nil];
        vc.model = self.model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
