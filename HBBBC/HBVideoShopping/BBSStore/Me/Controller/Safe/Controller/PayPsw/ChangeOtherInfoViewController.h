//
//  ChangeOtherInfoViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBSafeModel.h"
@interface ChangeOtherInfoViewController : HBRootViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *inputTf;
@property (nonatomic, strong) HBSafeModel * model;
@end
