//
//  HBPayForgetPswVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import "HBSafeModel.h"
@interface HBPayForgetPswVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UILabel *phonLab;

@property (nonatomic, strong) HBSafeModel * model;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIImageView *codeImgView;
@property (weak, nonatomic) IBOutlet UITextField *phoneCodeTF;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
- (IBAction)clickBtn:(UIButton *)sender;
- (IBAction)clickCodeBtn:(UIButton *)sender;

@end
