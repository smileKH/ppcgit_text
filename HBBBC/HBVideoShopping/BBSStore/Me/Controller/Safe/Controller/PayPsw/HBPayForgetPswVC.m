//
//  HBPayForgetPswVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBPayForgetPswVC.h"
#import "HBPayOriginalVC.h"
#import "NSString+Base64Image.h"
#import "HBPayPswChangeVC.h"
@interface HBPayForgetPswVC ()
{
    NSTimer *timer;
    NSInteger time;
    NSString *sess_id;
}
@end

@implementation HBPayForgetPswVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"找回支付密码";
    
    //点击刷新
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    self.codeImgView.userInteractionEnabled = YES;
    [self.codeImgView addGestureRecognizer:tap];
    [self getpicCode];
    
    NSString *tit = [NSString stringWithFormat:@"验证码已发送至您的手机：%@",self.model.userMobile];
    self.phonLab.attributedText = [StringAttribute stringToAttributeString:tit withRange:NSMakeRange(11, tit.length-11) withFont:14 withColor:MAINTextCOLOR];
    [self setTimer];
    [self sendSMS];
    //还有请求code
}
-(void)backBtnClicked{
    WEAKSELF;
    [self AlertWithTitle:@"提示" message:@"确定放弃找回密码?" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            NSArray *temArray = self.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[HBPayPswChangeVC class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }        }else{
                //取消
            }
    }];
}
//图片验证码
- (void)getpicCode
{
    WEAKSELF;
    [LBService post:USER_VC_CODE params:@{@"vcode_type":@"topapi_forgot"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //判断一下比较好
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                sess_id = dic[@"sess_id"];
                NSString *imageString = dic[@"base64Image"];
                weakSelf.codeImgView.image = [imageString toBase64Image];
            }else{
                [app showToastView:@"获取验证码失败，请稍后再试"];
            }
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)tapClick:(UITapGestureRecognizer*)tap
{
    [self getpicCode];
}

- (void)setTimer{
    time = 60;
    if (@available(iOS 10.0, *)) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            time--;
            self.codeBtn.userInteractionEnabled = NO;
            [self.codeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
            if (time == 0) {
                time = 60;
                [timer invalidate];
                self.codeBtn.userInteractionEnabled = YES;
                [self.codeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
            }
        }];
    } else {
        // Fallback on earlier versions
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(times) userInfo:nil repeats:YES];
    }
}
-(void)times{
    time--;
    self.codeBtn.userInteractionEnabled = NO;
    [self.codeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
    if (time == 0) {
        [timer invalidate];
        time = 60;
        self.codeBtn.userInteractionEnabled = YES;
        [self.codeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
    }
}
- (void)sendSMS
{
    [LBService post:SECURITY_SETPAY_MOBLIE params:@{@"mobile":self.model.userMobile} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            [timer fire];
        }else{
            [timer invalidate];
            time = 60;
            self.codeBtn.userInteractionEnabled = YES;
            [self.codeBtn setTitle:@"重新获取" forState:UIControlStateNormal];
            [app showToastView:response.message];
        }
    }];
}

- (void)checkSMSCode
{
    WEAKSELF;
    [LBService post:USER_VERIFY_SMS params:@{@"mobile":self.model.userMobile,@"vcode":self.phoneCodeTF.text,@"type":@"signup"} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            HBPayOriginalVC *vc = [[HBPayOriginalVC alloc]initWithNibName:@"HBPayOriginalVC" bundle:nil];
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        }else{
            [app showToastView:response.message];
        }
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickBtn:(UIButton *)sender {
    if (self.phoneCodeTF.text.length <= 0) {
        [app showToastView:@"请填写短信验证码"];
        return;
    }
    [self checkSMSCode];
}

- (IBAction)clickCodeBtn:(UIButton *)sender {
    [self setTimer];
    [self sendSMS];
}
@end
