//
//  HBPayOriginalVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBPayOriginalVC.h"
#import "MySafeViewController.h"
@interface HBPayOriginalVC ()

@end

@implementation HBPayOriginalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"设置支付密码";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickBtn:(UIButton *)sender {
    if (self.oneTF.text.length<6 || self.twoTF.text.length > 20) {
        [app showToastView:@"密码长度为6~20位"];
        return;
    }
    if (![self.oneTF.text isEqualToString:self.twoTF.text]) {
        [app showToastView:@"两次密码不一致"];
        return;
    }
    //支付密码
    [MBProgressHUD showActivityMessageInView:nil];
    WEAKSELF;
    [LBService post:SECURITY_SETPAY_PASSWORD params:@{@"password":self.oneTF.text,@"password_confirmation":self.twoTF.text,@"deviceid":@"mobile"} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            [app showToastView:@"设置成功"];
            //返回 个人中心
            NSArray *temArray = self.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[MySafeViewController class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
@end
