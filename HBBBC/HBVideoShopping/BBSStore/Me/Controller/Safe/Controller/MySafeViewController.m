//
//  MySafeViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MySafeViewController.h"
#import "InputLoginPswViewController.h"
#import "HBPayPswChangeVC.h"
#import "HBSafeModel.h"
#import "HBBindingPhoneVC.h"
@interface MySafeViewController ()
@property (nonatomic, strong) HBSafeModel * model;
@end

@implementation MySafeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
    self.title = @"安全中心";
//    ////[self.navigationController setBackButton];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction1)];
//    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction2)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction3)];
    [self.view1 addGestureRecognizer:tap1];
//    [self.view2 addGestureRecognizer:tap2];
    [self.view3 addGestureRecognizer:tap3];
    
    //获取配置
    [self securityRequestData];
}
- (void)tapAction1
{   //修改登录密码
    InputLoginPswViewController *vc= [[InputLoginPswViewController alloc] initWithNibName:@"InputLoginPswViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tapAction2
{   //设置支付密码
    HBPayPswChangeVC *vc= [[HBPayPswChangeVC alloc] initWithNibName:@"HBPayPswChangeVC" bundle:nil];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
}

//绑定手机号码
- (void)tapAction3
{   HBBindingPhoneVC *vc = [[HBBindingPhoneVC alloc]initWithNibName:@"HBBindingPhoneVC" bundle:nil];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ==========获取配置==========
-(void)securityRequestData{
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:SECURITY_USER_CONF params:nil completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //看看
            self.model = [HBSafeModel mj_objectWithKeyValues:response.result[@"data"]];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
