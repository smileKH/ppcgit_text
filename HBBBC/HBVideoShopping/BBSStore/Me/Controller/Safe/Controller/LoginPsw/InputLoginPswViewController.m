//
//  InputLoginPswViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "InputLoginPswViewController.h"
#import "ChangePswViewController.h"
#import "ChangeOtherInfoViewController.h"
@interface InputLoginPswViewController ()
@property (weak, nonatomic) IBOutlet UITextField *inputTF;

@end

@implementation InputLoginPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE 修改登录密码
    self.title = @"输入登录密码";
//    ////[self.navigationController setBackButton];
}
- (IBAction)clickBtn:(UIButton *)sender {
    if ([HBHuTool isJudgeString:self.inputTF.text]) {
        [app showToastView:@"请输入登录密码"];
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:nil];
    [LBService post:SECURITY_CHECK_LOGINPS params:@{@"password":self.inputTF.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
  
                ChangePswViewController *vc= [[ChangePswViewController alloc] initWithNibName:@"ChangePswViewController" bundle:nil];
                [weakSelf.navigationController pushViewController:vc animated:YES];
           
            
        }else{
            [app showToastView:response.message];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)next:(UIButton *)sender {
    
    
}
@end
