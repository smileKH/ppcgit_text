//
//  ChangePswViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePswViewController : HBRootViewController
@property (nonatomic, assign) NSInteger interType;//1=修改登录密码 2=修改支付密码  3= 绑定手号
@end
