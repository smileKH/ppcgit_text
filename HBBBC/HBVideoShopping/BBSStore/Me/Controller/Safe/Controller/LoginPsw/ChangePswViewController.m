//
//  ChangePswViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ChangePswViewController.h"

@interface ChangePswViewController ()
@property (weak, nonatomic) IBOutlet UITextField *oneTF;
@property (weak, nonatomic) IBOutlet UITextField *twoTF;

@end

@implementation ChangePswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"修改密码";
}
- (IBAction)clickBtn:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.oneTF.text.length<6 || self.twoTF.text.length > 20) {
        [app showToastView:@"密码长度为6~20位"];
        return;
    }
    if (![self.oneTF.text isEqualToString:self.twoTF.text]) {
        [app showToastView:@"两次密码不一致"];
        return;
    }
    [self registMember];
}
- (void)registMember
{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:nil];
    [LBService post:SECURITY_UPDATE_LOGINPS params:@{@"password":self.oneTF.text,@"password_confirmation":self.twoTF.text,@"deviceid":@"mobile"} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            [app showToastView:@"修改成功,请重新登录！"];
            //返回 个人中心
            //退出登录
            //退出登录成功
            if ([HBHuTool exitAccount]) {
                //退出成功，进行操作 发一条通知
                userManager.loginState = 0;
                //发一个通知
                [[NSNotificationCenter defaultCenter]postNotificationName:NotificationLogoutSuccess object:nil];
            }else{
                //退出失败
            }
            NSArray *temArray = weakSelf.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[HBMineViewController class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }
            
            
        }else{
            [app showToastView:response.message];
        }
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
