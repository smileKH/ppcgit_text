//
//  HBBindingChangePhoneVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBBindingChangePhoneVC.h"
#import "MySafeViewController.h"
@interface HBBindingChangePhoneVC ()
{
    NSTimer *timer;
    NSInteger time;
}
@end

@implementation HBBindingChangePhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"手机验证";
    NSString *tit = [NSString stringWithFormat:@"验证码已发送至您的手机：%@",self.tel];
    self.phone.attributedText = [StringAttribute stringToAttributeString:tit withRange:NSMakeRange(11, tit.length-11) withFont:14 withColor:MAINTextCOLOR];
    [self setTimer];
    [self sendSMS];
}
-(void)backBtnClicked{
    WEAKSELF;
    [self AlertWithTitle:@"提示" message:@"确定放弃注册?" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //确定
            NSArray *temArray = self.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:[HBMineViewController class]]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }        }else{
                //取消
            }
    }];
}
- (void)setTimer{
    time = 60;
    if (@available(iOS 10.0, *)) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            time--;
            self.timeBtn.userInteractionEnabled = NO;
            [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
            if (time == 0) {
                time = 60;
                [timer invalidate];
                self.timeBtn.userInteractionEnabled = YES;
                [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
            }
        }];
    } else {
        // Fallback on earlier versions
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(times) userInfo:nil repeats:YES];
    }
}
-(void)times{
    time--;
    self.timeBtn.userInteractionEnabled = NO;
    [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
    if (time == 0) {
        [timer invalidate];
        time = 60;
        self.timeBtn.userInteractionEnabled = YES;
        [self.timeBtn setTitle:[NSString stringWithFormat:@"%ld秒",(long)time] forState:UIControlStateNormal];
    }
}
- (void)sendSMS
{
    [LBService post:SECURITY_UPDATE_MOBILE params:@{@"mobile":self.tel} completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            [timer fire];
        }else{
            [app showToastView:response.message];
        }
    }];
}

- (void)checkSMSCode
{
    WEAKSELF;
    [LBService post:SECURITY_SAVE_MOBILE params:@{@"mobile":self.tel,@"vcode":self.vcodeTf.text} completion:^(LBResponse *response) {
        if (response.succeed) {
            
            //成功
            [weakSelf setSucceed];
           
        }else{
            [app showToastView:response.message];
        }
    }];
    
}
-(void)setSucceed{
    [self AlertWithTitle:@"提示" message:@"修改手机号成功,请重新登录" andOthers:@[@"确定"] animated:YES action:^(NSInteger index) {
        //退到个人中心，然后跳到登录界面
        NSArray *temArray = self.navigationController.viewControllers;
        for(UIViewController *temVC in temArray){
            if ([temVC isKindOfClass:[MySafeViewController class]]){
                [self.navigationController popToViewController:temVC animated:YES];
            }
        }
        if ([userManager judgeLoginState]) {
            //
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickBtn:(UIButton *)sender {
    if (self.vcodeTf.text.length <= 0) {
        [app showToastView:@"请填写短信验证码"];
        return;
    }
}
@end
