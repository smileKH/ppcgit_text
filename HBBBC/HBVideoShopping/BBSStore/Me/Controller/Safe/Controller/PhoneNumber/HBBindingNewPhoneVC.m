//
//  HBBindingNewPhoneVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBBindingNewPhoneVC.h"
#import "HBBindingChangePhoneVC.h"
@interface HBBindingNewPhoneVC ()

@end

@implementation HBBindingNewPhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"填写手机号";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickBtn:(UIButton *)sender {
    if ([HBHuTool isJudgeString:self.phoneTF.text]) {
        [app showToastView:@"请输入手机号码"];
        return;
    }
    HBBindingChangePhoneVC *vc = [[HBBindingChangePhoneVC alloc]initWithNibName:@"HBBindingChangePhoneVC" bundle:nil];
    vc.tel = self.phoneTF.text;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
