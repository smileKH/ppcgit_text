//
//  HBBindingNewPhoneVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBBindingNewPhoneVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
- (IBAction)clickBtn:(UIButton *)sender;

@end
