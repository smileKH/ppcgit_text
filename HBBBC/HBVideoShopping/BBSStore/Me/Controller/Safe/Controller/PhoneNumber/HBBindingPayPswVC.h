//
//  HBBindingPayPswVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBBindingPayPswVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UITextField *pswTF;
- (IBAction)clickBtn:(UIButton *)sender;

@end
