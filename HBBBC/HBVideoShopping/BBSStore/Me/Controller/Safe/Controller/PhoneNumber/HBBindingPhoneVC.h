//
//  HBBindingPhoneVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import "HBSafeModel.h"
@interface HBBindingPhoneVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
- (IBAction)clickBtn:(UIButton *)sender;
@property (nonatomic, strong) HBSafeModel * model;
@end
