//
//  HBBindingPhoneVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBBindingPhoneVC.h"
#import "HBBindingPayPswVC.h"
@interface HBBindingPhoneVC ()

@end

@implementation HBBindingPhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"已绑定手机号";
    self.phoneLab.text = self.model.userMobile;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickBtn:(UIButton *)sender {
    HBBindingPayPswVC *vc= [[HBBindingPayPswVC alloc] initWithNibName:@"HBBindingPayPswVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
