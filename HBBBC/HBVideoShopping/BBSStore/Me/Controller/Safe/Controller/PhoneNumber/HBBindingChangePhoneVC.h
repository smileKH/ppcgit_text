//
//  HBBindingChangePhoneVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBBindingChangePhoneVC : HBRootViewController
@property (strong, nonatomic) IBOutlet UITextField *vcodeTf;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *verifyAccount_token;

@property (strong, nonatomic) IBOutlet UILabel *phone;
@property (strong, nonatomic) IBOutlet UIButton *timeBtn;
- (IBAction)clickBtn:(UIButton *)sender;

@end
