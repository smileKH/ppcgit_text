//
//  HBBindingPayPswVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBBindingPayPswVC.h"
#import "HBBindingNewPhoneVC.h"
@interface HBBindingPayPswVC ()

@end

@implementation HBBindingPayPswVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"输入登录密码";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickBtn:(UIButton *)sender {
    if ([HBHuTool isJudgeString:self.pswTF.text]) {
        [app showToastView:@"请输入登录密码"];
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:nil];
    [LBService post:SECURITY_CHECK_LOGINPS params:@{@"password":self.pswTF.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            HBBindingNewPhoneVC *vc= [[HBBindingNewPhoneVC alloc] initWithNibName:@"HBBindingNewPhoneVC" bundle:nil];
            [weakSelf.navigationController pushViewController:vc animated:YES]; 
        }else{
            [app showToastView:response.message];
        }
    }];
}
@end
