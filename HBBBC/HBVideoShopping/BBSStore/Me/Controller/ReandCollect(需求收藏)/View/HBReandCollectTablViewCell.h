//
//  HBReandCollectTablViewCell.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2020/2/7.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBBaseListTableCell.h"
#import "HBDemandListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HBReandCollectTablViewCell : HBBaseListTableCell
@property (nonatomic ,strong)HBDemandListModel *model;
@property (nonatomic ,copy)void(^clickMoreChangeBtn)(NSString *titleStr);
@end

NS_ASSUME_NONNULL_END
