//
//  HBUserAgreementVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBUserAgreementVC.h"

@interface HBUserAgreementVC ()
@property (nonatomic ,strong)NSString *htmlStr;
@end

@implementation HBUserAgreementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"用户协议";
    
    //请求数据
    [self requestAgreementData];
}
#pragma mark - 用户协议
-(void)requestAgreementData{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:USER_PROTOCOL params:nil completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                weakSelf.htmlStr = dic[@"useProtocol"];
                [weakSelf setUIData];
            }
        }else{
            
        }
    }];
}
-(void)setUIData{
    if ([HBHuTool isJudgeString:self.htmlStr]) {
        [app showToastView:@"请求不到协议连接"];
        return;
    }
    NSAttributedString *contents = [[NSAttributedString alloc] initWithData:[self.htmlStr dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.textView.attributedText = contents;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
