//
//  CommonViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "CommonViewController.h"

@interface CommonViewController ()

@end

@implementation CommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"通用";
    NSLog(@"网络缓存大小cache = %fKB",[PPNetworkCache getAllHttpCacheSize]/1024.f);
    NSString *string = [NSString stringWithFormat:@"%fKB",[PPNetworkCache getAllHttpCacheSize]/1024.f];
    [self.huanCunLab setTitle:string forState:UIControlStateNormal];
    //设置当前版本
    NSString *version = kAppVersion;
    self.vsionLab.text = [NSString stringWithFormat:@"当前版本：%@",version] ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickBtn:(UIButton *)sender {
    [self AlertWithTitle:@"提示" message:@"确定要清除缓存？" andOthers:@[@"确定",@"取消"] animated:YES action:^(NSInteger index) {
        if (index==0) {
            //
            [PPNetworkCache removeAllHttpCache];
        }else{
            
        }
    }];
}
@end
