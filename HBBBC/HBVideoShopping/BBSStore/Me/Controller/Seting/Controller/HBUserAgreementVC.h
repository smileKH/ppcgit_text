//
//  HBUserAgreementVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/24.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBUserAgreementVC : HBRootViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
