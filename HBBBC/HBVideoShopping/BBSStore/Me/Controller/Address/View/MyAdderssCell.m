//
//  MyAdderssCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyAdderssCell.h"

@implementation MyAdderssCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
#pragma mark ==========点击默认按钮==========
- (IBAction)clickDefaultAction:(UIButton *)sender {
    
    self.clickMoRenBtn(self.model);
}
#pragma mark ==========点击编辑按钮==========
- (IBAction)clickEditBtn:(UIButton *)sender {
    self.clickEditBtn(self.model);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(Default_address *)model{
    _model = model;
    self.nameLab.text = [NSString stringWithFormat:@"收货人:%@",model.name];
    self.phoneLab.text = [NSString stringWithFormat:@"%@",model.mobile];
    self.addressLab.text = [NSString stringWithFormat:@"收货地址:%@%@",model.area,model.addrdetail];
    if ([model.def_addr integerValue] ==1) {
        self.moRenBtn.selected = YES;
    }else{
        self.moRenBtn.selected = NO;
    }
}

@end
