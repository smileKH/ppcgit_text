//
//  HBMeAddressPushView.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/2.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBMeAddressPushView.h"
#define SCREEN_WIDTH            ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT           ([[UIScreen mainScreen] bounds].size.height)
@interface HBMeAddressPushView ()<UIPickerViewDataSource, UIPickerViewDelegate>
//主要的Windows
@property (nonatomic, strong) UIWindow *backWindow;
//黑色的view
@property (nonatomic , strong)UIView * drakView;
//白色的view
@property (nonatomic, strong) UIView *bottomView;
//取消button
@property (nonatomic , strong)UIButton * cancelButton;

/** 2.当前省数组 */
@property (nonatomic, strong, nullable)NSMutableArray *arrayProvince;
/** 3.当前城市数组 */
@property (nonatomic, strong, nullable)NSMutableArray *arrayCity;
/** 4.当前地区数组 */
@property (nonatomic, strong, nullable)NSMutableArray *arrayArea;
/** 5.当前选中数组 */
//@property (nonatomic, strong, nullable)NSMutableArray *arraySelected;

@property (nonatomic, strong) HBAreaProvinceModel * selectProvice;

/** 6.省份 */
@property (nonatomic, strong, nullable)HBAreaProvinceModel *province;
/** 7.城市 */
@property (nonatomic, strong, nullable)HBAreaChildrenModel *city;
/** 8.地区 */
@property (nonatomic, strong, nullable)HBAreaChildrenModel *area;
@property (nonatomic, strong) UIButton * confBtn;//确定按钮
@end
@implementation HBMeAddressPushView



-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 暗黑色的view
        UIView *darkView = [[UIView alloc] init];
        [darkView setAlpha:0];
        [darkView setUserInteractionEnabled:NO];
        [darkView setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [darkView setBackgroundColor:[UIColor grayColor]];
        [self addSubview:darkView];
        self.drakView = darkView;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
        [darkView addGestureRecognizer:tap];
        
        // 所有按钮的底部view
        UIView *bottomView = [[UIView alloc] init];
        [bottomView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:bottomView];
        _bottomView = bottomView;
        
        [bottomView setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT/2)];
        //添加子视图
        //        [self addAreaPickerView];
        //        //加一个取消的按钮
        //        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        //        button.backgroundColor = [UIColor clearColor];
        //        [button setImage:[UIImage imageNamed:@"push_quxiao.png"] forState:UIControlStateNormal];
        //        [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        //        [self addSubview:button];
        //        self.cancelButton = button;
        //
        //        [button setFrame:CGRectMake(SCREEN_WIDTH-40-40, -40, 40, 40)];
        
        
        [self setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [self.backWindow addSubview:self];
        
    }
    return self;
}

#pragma mark - 添加地区的视图
-(void)addAreaPickerView{
    
    for (HBAreaProvinceModel *province in self.model.region) {
        [self.arrayProvince addObject:province];//省
    }
    if ([HBHuTool judgeArrayIsEmpty:self.model.region]) {
        HBAreaProvinceModel *province = self.model.region[0];
        for (HBAreaCityModel *city in province.children) {
            [self.arrayCity addObject:city];//城市
        }
        
        if ([HBHuTool judgeArrayIsEmpty:self.arrayCity]) {
            HBAreaCityModel *city = self.arrayCity[0];
            for (HBAreaChildrenModel *children in city.children) {
                [self.arrayArea addObject:children];//县
            }
        }
    }

    //赋予初值
    self.province = self.arrayProvince[0];
    self.city = self.arrayCity[0];
    if (self.arrayArea.count != 0) {
        self.area = self.arrayArea[0];
    }else{
        self.area = nil;
    }
    
    if (self.area) {
        self.codeStr = [NSString stringWithFormat:@"%@,%@,%@", self.province.id, self.city.id, self.area.id];
        self.address = [NSString stringWithFormat:@"%@/%@/%@", self.province.value, self.city.value, self.area.value];
    }else{
        self.codeStr = [NSString stringWithFormat:@"%@,%@", self.province.id, self.city.id];
        self.address = [NSString stringWithFormat:@"%@/%@", self.province.value, self.city.value];
    }
    //创建packerView
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, SCREEN_HEIGHT/2-10-40)];
    
    self.confBtn.frame = CGRectMake(0, SCREEN_HEIGHT/2-40, SCREEN_WIDTH, 40);
    
    // 2.设置视图的默认属性
    _heightPickerComponent = 32;
    //[self setTitle:@"请选择城市地区"];
    [self.pickerView setDelegate:self];
    [self.pickerView setDataSource:self];
    [_bottomView addSubview:self.pickerView];
    [_bottomView addSubview:self.confBtn];
    
}
#pragma mark ==========getter==========
- (UIButton *)confBtn{
    if (!_confBtn) {
        _confBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_confBtn setTitleColor:white_Color forState:UIControlStateNormal];
        [_confBtn setBackgroundColor:MAINTextCOLOR];
        [_confBtn addTarget:self action:@selector(clickConfBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confBtn;
}

#pragma mark ==========点击按钮==========
-(void)clickConfBtn:(UIButton *)button{
    [self dismiss];
    
    self.clcikDimss(self.address,self.codeStr);
}
#pragma mark - --- delegate 视图委托 ---

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.arrayProvince.count;
    }else if (component == 1) {
        return self.arrayCity.count;
    }else{
        return self.arrayArea.count;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return self.heightPickerComponent;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        
        HBAreaProvinceModel *province = self.model.region[row];//拿到省
        self.selectProvice = province;
        
        [self.arrayCity removeAllObjects];//删除城市
        for (HBAreaCityModel *city in province.children) {
            [self.arrayCity addObject:city];//城市
        }
        [self.arrayArea removeAllObjects];//删除县
        
        HBAreaCityModel *city = self.arrayCity[0];
        for (HBAreaChildrenModel *children in city.children) {
            [self.arrayArea addObject:children];
        }
        
        [pickerView reloadComponent:1];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
    }else if (component == 1) {
        
        if (!self.selectProvice) {
            //拿第一个
            HBAreaProvinceModel *province = self.model.region[0];//拿到省
            self.selectProvice = province;
        }
        [self.arrayArea removeAllObjects];//删除县
        
        HBAreaCityModel *city = self.selectProvice.children[row];
        for (HBAreaChildrenModel *children in city.children) {
            [self.arrayArea addObject:children];
        }
  
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
    }else{
        
    }
    
    [self reloadData];
}
//这里是显示数据
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view
{
    
    NSString *text;
    if (component == 0) {
        HBAreaProvinceModel *province = self.arrayProvince[row];
        text =  province.value;
    }else if (component == 1){
        HBAreaCityModel *city = self.arrayCity[row];
        text =  city.value;
    }else{
        if (self.arrayArea.count > 0) {
            HBAreaChildrenModel *children = self.arrayArea[row];
            text = children.value;
        }else{
            text =  @"";
        }
    }
    
    UILabel *label = [[UILabel alloc]init];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont systemFontOfSize:17]];
    [label setText:text];
    return label;
    
    
}
#pragma mark - --- event response 事件相应 ---

- (void)selectedOk
{
    //    [self.delegate pickerArea:self province:self.province city:self.city area:self.area];
    //    [super selectedOk];
}

#pragma mark - --- private methods 私有方法 ---

- (void)reloadData
{
    NSInteger index0 = [self.pickerView selectedRowInComponent:0];
    NSInteger index1 = [self.pickerView selectedRowInComponent:1];
    NSInteger index2 = [self.pickerView selectedRowInComponent:2];

    self.city = self.arrayCity[index1];
    if (self.arrayArea.count != 0) {
        self.area = self.arrayArea[index2];
    }else{
        self.area = nil;
    }


        self.province = self.arrayProvince[index0];
        self.city = self.arrayCity[index1];
        if (self.arrayArea.count != 0) {
            self.area = self.arrayArea[index2];
        }else{
            self.area = nil;
        }
    
    if (self.area) {
        self.codeStr = [NSString stringWithFormat:@"%@,%@,%@", self.province.id, self.city.id, self.area.id];
        self.address = [NSString stringWithFormat:@"%@/%@/%@", self.province.value, self.city.value, self.area.value];
    }else{
        self.codeStr = [NSString stringWithFormat:@"%@,%@", self.province.id, self.city.id];
        self.address = [NSString stringWithFormat:@"%@/%@", self.province.value, self.city.value];
    }
    NSLog(@"title:%@",self.address);
//
//        NSString *title = [NSString stringWithFormat:@"%@/%@/%@", self.province, self.city, self.area];
//        NSLog(@"title:%@",title);
//        self.address = title;
    
}

#pragma mark - --- setters 属性 ---

- (void)setModel:(HBAreaModel *)model{
    _model = model;
    [self addAreaPickerView];
}


- (NSMutableArray *)arrayProvince
{
    if (!_arrayProvince) {
        _arrayProvince = [NSMutableArray array];
    }
    return _arrayProvince;
}

- (NSMutableArray *)arrayCity
{
    if (!_arrayCity) {
        _arrayCity = [NSMutableArray array];
    }
    return _arrayCity;
}

- (NSMutableArray *)arrayArea
{
    if (!_arrayArea) {
        _arrayArea = [NSMutableArray array];
    }
    return _arrayArea;
}

//- (NSMutableArray *)arraySelected
//{
//    if (!_arraySelected) {
//        _arraySelected = [NSMutableArray array];
//    }
//    return _arraySelected;
//}

-(void)clickButton:(UIButton *)button{
    
    [self dismiss];
}
- (UIWindow *)backWindow {
    
    if (_backWindow == nil) {
        
        _backWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backWindow.windowLevel       = UIWindowLevelStatusBar;
        _backWindow.backgroundColor   = [UIColor clearColor];
        _backWindow.hidden = NO;
    }
    
    return _backWindow;
}
- (void)dismiss:(UITapGestureRecognizer *)tap {
    
    [self dismiss];
    
    self.clcikDimss(self.address,self.codeStr);
}

-(void)dismiss{
    [UIView animateWithDuration:0.3f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         [self.drakView setAlpha:0];
                         [self.drakView setUserInteractionEnabled:NO];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = SCREEN_HEIGHT;
                         [_bottomView setFrame:frame];
                         
                         CGRect cancelFrame = _cancelButton.frame;
                         cancelFrame.origin.y = -40;
                         [_cancelButton setFrame:cancelFrame];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         _backWindow.hidden = YES;
                         
                         [self removeFromSuperview];
                     }];
    
    
}


- (void)show {
    
    _backWindow.hidden = NO;
    
    [UIView animateWithDuration:.75f
                          delay:0.2
         usingSpringWithDamping:0.65f
          initialSpringVelocity:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.drakView setAlpha:0.4f];
                         [self.drakView setUserInteractionEnabled:YES];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = SCREEN_HEIGHT/2;
                         [_bottomView setFrame:frame];
                         
                         CGRect cancelFrame = _cancelButton.frame;
                         cancelFrame.origin.y = frame.origin.y/2;
                         [_cancelButton setFrame:cancelFrame];
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
}

@end
