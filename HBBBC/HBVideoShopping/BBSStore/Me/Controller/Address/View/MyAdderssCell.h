//
//  MyAdderssCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBAddressModel.h"
@interface MyAdderssCell : UITableViewCell
@property (nonatomic, strong) Default_address * model;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UIButton *moRenBtn;

@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@property (nonatomic, copy) void(^clickMoRenBtn)(Default_address *model);
@property (nonatomic, copy) void(^clickEditBtn)(Default_address *model);
@end
