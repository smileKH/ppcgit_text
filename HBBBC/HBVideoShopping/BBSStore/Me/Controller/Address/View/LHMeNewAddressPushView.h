//
//  LHMeNewAddressPushView.h
//  雾霾口罩
//
//  Created by aplle on 2017/5/8.
//
//

#import <UIKit/UIKit.h>
#import "HBAreaModel.h"
@interface LHMeNewAddressPushView : UIView<UITextFieldDelegate,UIAlertViewDelegate>
- (void)show;
/** 3.选择器 */
@property (nonatomic, strong)UIPickerView *pickerView;
//选择的地址
@property (nonatomic , strong)NSString * address;
/** 1.数据源数组 */
@property (nonatomic, strong, nullable)NSArray *arrayRoot;
@property (nonatomic, strong) HBAreaModel * model;
/** 1.中间选择框的高度，default is 32*/
@property (nonatomic, assign)CGFloat heightPickerComponent;

@property (nonatomic, strong) void(^clcikDimss)(NSString *address);

@end
