//
//  HBMeAddressPushView.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/2.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBAreaModel.h"
@interface HBMeAddressPushView : UIView<UITextFieldDelegate,UIAlertViewDelegate>
- (void)show;
/** 3.选择器 */
@property (nonatomic, strong)UIPickerView *pickerView;
//选择的地址
@property (nonatomic , strong)NSString * address;
@property (nonatomic , strong)NSString * codeStr;

@property (nonatomic, strong) HBAreaModel * model;
/** 1.中间选择框的高度，default is 32*/
@property (nonatomic, assign)CGFloat heightPickerComponent;

@property (nonatomic, strong) void(^clcikDimss)(NSString *address,NSString *codeStr);

@end
