//
//  LHMeNewAddressPushView.m
//  雾霾口罩
//
//  Created by aplle on 2017/5/8.
//
//

#import "LHMeNewAddressPushView.h"
#define SCREEN_WIDTH            ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT           ([[UIScreen mainScreen] bounds].size.height)
@interface LHMeNewAddressPushView ()<UIPickerViewDataSource, UIPickerViewDelegate>
//主要的Windows
@property (nonatomic, strong) UIWindow *backWindow;
//黑色的view
@property (nonatomic , strong)UIView * drakView;
//白色的view
@property (nonatomic, strong) UIView *bottomView;
//取消button
@property (nonatomic , strong)UIButton * cancelButton;

/** 2.当前省数组 */
@property (nonatomic, strong, nullable)NSMutableArray *arrayProvince;
/** 3.当前城市数组 */
@property (nonatomic, strong, nullable)NSMutableArray *arrayCity;
/** 4.当前地区数组 */
@property (nonatomic, strong, nullable)NSMutableArray *arrayArea;
/** 5.当前选中数组 */
@property (nonatomic, strong, nullable)NSMutableArray *arraySelected;


/** 2.当前省数组 */
@property (nonatomic, strong, nullable)NSMutableArray *provinceIdArray;
/** 3.当前城市数组 */
@property (nonatomic, strong, nullable)NSMutableArray *cityIdArray;
/** 4.当前地区数组 */
@property (nonatomic, strong, nullable)NSMutableArray *areaIdArray;

/** 6.省份 */
@property (nonatomic, strong, nullable)NSString *province;
/** 7.城市 */
@property (nonatomic, strong, nullable)NSString *city;
/** 8.地区 */
@property (nonatomic, strong, nullable)NSString *area;
@end
@implementation LHMeNewAddressPushView


-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // 暗黑色的view
        UIView *darkView = [[UIView alloc] init];
        [darkView setAlpha:0];
        [darkView setUserInteractionEnabled:NO];
        [darkView setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [darkView setBackgroundColor:[UIColor grayColor]];
        [self addSubview:darkView];
        self.drakView = darkView;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
        [darkView addGestureRecognizer:tap];
        
        // 所有按钮的底部view
        UIView *bottomView = [[UIView alloc] init];
        [bottomView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:bottomView];
        _bottomView = bottomView;
        
        [bottomView setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT/2)];
        //添加子视图
//        [self addAreaPickerView];
//        //加一个取消的按钮
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        button.backgroundColor = [UIColor clearColor];
//        [button setImage:[UIImage imageNamed:@"push_quxiao.png"] forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:button];
//        self.cancelButton = button;
//        
//        [button setFrame:CGRectMake(SCREEN_WIDTH-40-40, -40, 40, 40)];
        
        
        [self setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [self.backWindow addSubview:self];
        
    }
    return self;
}

#pragma mark - 添加地区的视图
-(void)addAreaPickerView{
    
    
    // 1.获取数据
    [self.arrayRoot enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.arrayProvince addObject:obj[@"value"]];
    }];
    
    NSMutableArray *citys = [NSMutableArray arrayWithArray:[self.arrayRoot firstObject][@"children"]];
    [citys enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.arrayCity addObject:obj[@"value"]];
    }];
    
    NSMutableArray *Area = [NSMutableArray arrayWithArray:[citys firstObject][@"children"]];
    [Area enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.arrayArea addObject:obj[@"value"]];
    }];
    
    self.province = self.arrayProvince[0];
    self.city = self.arrayCity[0];
    if (self.arrayArea.count != 0) {
        self.area = self.arrayArea[0];
    }else{
        self.area = @"";
    }
    
    //创建packerView
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 300)];
    
    // 2.设置视图的默认属性
    _heightPickerComponent = 32;
    //[self setTitle:@"请选择城市地区"];
    [self.pickerView setDelegate:self];
    [self.pickerView setDataSource:self];
    [_bottomView addSubview:self.pickerView];
    
}
#pragma mark - --- delegate 视图委托 ---

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.arrayProvince.count;
    }else if (component == 1) {
        return self.arrayCity.count;
    }else{
        return self.arrayArea.count;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return self.heightPickerComponent;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        self.arraySelected = self.arrayRoot[row][@"children"];
        
        [self.arrayCity removeAllObjects];
        [self.arraySelected enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.arrayCity addObject:obj[@"value"]];
        }];
        [self.arrayArea removeAllObjects];
        NSMutableArray *Area = [NSMutableArray arrayWithArray:[self.arraySelected firstObject][@"children"]];
        [Area enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.arrayArea addObject:obj[@"value"]];
        }];
        
        [pickerView reloadComponent:1];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
    }else if (component == 1) {
        if (self.arraySelected.count == 0) {
            self.arraySelected = [self.arrayRoot firstObject][@"children"];
        }
        [self.arrayArea removeAllObjects];
        NSMutableArray *Area = [NSMutableArray arrayWithArray:[self.arraySelected objectAtIndex:row][@"children"]];
        [Area enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.arrayArea addObject:obj[@"value"]];
        }];
//        self.arrayArea = [NSMutableArray arrayWithArray:[self.arraySelected objectAtIndex:row][@"children"]];
        
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
    }else{
    }
    
    [self reloadData];
}
//这里是显示数据
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view
{
    
    NSString *text;
    if (component == 0) {
        text =  self.arrayProvince[row];
    }else if (component == 1){
        text =  self.arrayCity[row];
    }else{
        if (self.arrayArea.count > 0) {
            text = self.arrayArea[row];
        }else{
            text =  @"";
        }
    }
    
    
    UILabel *label = [[UILabel alloc]init];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont systemFontOfSize:17]];
    [label setText:text];
    return label;
    
    
}
#pragma mark - --- event response 事件相应 ---

- (void)selectedOk
{
    //    [self.delegate pickerArea:self province:self.province city:self.city area:self.area];
    //    [super selectedOk];
}

#pragma mark - --- private methods 私有方法 ---

- (void)reloadData
{
    NSInteger index0 = [self.pickerView selectedRowInComponent:0];
    NSInteger index1 = [self.pickerView selectedRowInComponent:1];
    NSInteger index2 = [self.pickerView selectedRowInComponent:2];
    NSDictionary *dic = self.arrayRoot[index0];
    NSString *proviceId = dic[@"id"];
    
    self.city = self.arrayCity[index1];
    if (self.arrayArea.count != 0) {
        self.area = self.arrayArea[index2];
    }else{
        self.area = @"";
    }
    
    NSString *title = [NSString stringWithFormat:@"%@/%@/%@", self.province, self.city, self.area];
    NSLog(@"title:%@",title);
    self.address = title;
    
//    self.province = self.arrayProvince[index0];
//    self.city = self.arrayCity[index1];
//    if (self.arrayArea.count != 0) {
//        self.area = self.arrayArea[index2];
//    }else{
//        self.area = @"";
//    }
//
//    NSString *title = [NSString stringWithFormat:@"%@/%@/%@", self.province, self.city, self.area];
//    NSLog(@"title:%@",title);
//    self.address = title;
    
}

#pragma mark - --- setters 属性 ---

- (void)setArrayRoot:(NSArray *)arrayRoot{
    _arrayRoot = arrayRoot;
    [self addAreaPickerView];
}

- (void)setModel:(HBAreaModel *)model{
    _model = model;
    [self addAreaPickerView];
}

- (NSMutableArray *)provinceIdArray
{
    if (!_provinceIdArray) {
        _provinceIdArray = [NSMutableArray array];
    }
    return _provinceIdArray;
}

- (NSMutableArray *)cityIdArray
{
    if (!_cityIdArray) {
        _cityIdArray = [NSMutableArray array];
    }
    return _cityIdArray;
}

- (NSMutableArray *)areaIdArray
{
    if (!_areaIdArray) {
        _areaIdArray = [NSMutableArray array];
    }
    return _areaIdArray;
}
- (NSMutableArray *)arrayProvince
{
    if (!_arrayProvince) {
        _arrayProvince = [NSMutableArray array];
    }
    return _arrayProvince;
}

- (NSMutableArray *)arrayCity
{
    if (!_arrayCity) {
        _arrayCity = [NSMutableArray array];
    }
    return _arrayCity;
}

- (NSMutableArray *)arrayArea
{
    if (!_arrayArea) {
        _arrayArea = [NSMutableArray array];
    }
    return _arrayArea;
}

- (NSMutableArray *)arraySelected
{
    if (!_arraySelected) {
        _arraySelected = [NSMutableArray array];
    }
    return _arraySelected;
}

-(void)clickButton:(UIButton *)button{
    
    [self dismiss];
}
- (UIWindow *)backWindow {
    
    if (_backWindow == nil) {
        
        _backWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backWindow.windowLevel       = UIWindowLevelStatusBar;
        _backWindow.backgroundColor   = [UIColor clearColor];
        _backWindow.hidden = NO;
    }
    
    return _backWindow;
}
- (void)dismiss:(UITapGestureRecognizer *)tap {
    
    [self dismiss];
    
    self.clcikDimss(self.address);
}

-(void)dismiss{
    [UIView animateWithDuration:0.3f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         [self.drakView setAlpha:0];
                         [self.drakView setUserInteractionEnabled:NO];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = SCREEN_HEIGHT;
                         [_bottomView setFrame:frame];
                         
                         CGRect cancelFrame = _cancelButton.frame;
                         cancelFrame.origin.y = -40;
                         [_cancelButton setFrame:cancelFrame];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         _backWindow.hidden = YES;
                         
                         [self removeFromSuperview];
                     }];
    
    
}


- (void)show {
    
    _backWindow.hidden = NO;
    
    [UIView animateWithDuration:.75f
                          delay:0.2
         usingSpringWithDamping:0.65f
          initialSpringVelocity:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.drakView setAlpha:0.4f];
                         [self.drakView setUserInteractionEnabled:YES];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = SCREEN_HEIGHT/2;
                         [_bottomView setFrame:frame];
                         
                         CGRect cancelFrame = _cancelButton.frame;
                         cancelFrame.origin.y = frame.origin.y/2;
                         [_cancelButton setFrame:cancelFrame];
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
}


@end
