//
//  AddAddressViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "AddAddressViewController.h"
#import "HBMeAddressPushView.h"
#import "HBAreaModel.h"
@interface AddAddressViewController ()
@property (weak, nonatomic) IBOutlet UITextField *personNameTF;//姓名

@property (weak, nonatomic) IBOutlet UITextField *phoneTF;//电话号码

@property (weak, nonatomic) IBOutlet UITextField *detailAddTF;//详细地址
@property (weak, nonatomic) IBOutlet UIButton *areaBtn;

@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UILabel *areaLab;//颜色
@property (weak, nonatomic) IBOutlet UIButton *setDufltBtn;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;//邮政编码

@property (nonatomic, strong) HBMeAddressPushView * addressPushView;

//@property (nonatomic, strong) NSArray * arrRoot;
@property (nonatomic, strong) HBAreaModel * rootModel;
@property (nonatomic, strong) NSString * addressStr;
@property (nonatomic, strong) NSString * codeStr;
@property (nonatomic, assign) NSInteger  isDefult;
@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"新增收货地址";
    //请求地区数据
    [self requestAddressData];
    self.isDefult = 0;
    //设置初始值
    [self setInitValue];
}
#pragma mark ==========设置初始值==========
-(void)setInitValue{
    
    //设置值
    if (![HBHuTool isJudgeString:self.model.name]) {
        self.personNameTF.text = [NSString stringWithFormat:@"%@",self.model.name];
        self.phoneTF.text = [NSString stringWithFormat:@"%@",self.model.mobile];
        self.detailAddTF.text = [NSString stringWithFormat:@"%@",self.model.addrdetail];
        self.areaLab.text = [NSString stringWithFormat:@"%@",self.model.area];
        self.addressStr = [NSString stringWithFormat:@"%@",self.model.area];
        self.codeTF.text = [NSString stringWithFormat:@"%@",self.model.zip];
        
        if ([self.model.def_addr integerValue] ==1) {
            self.setDufltBtn.selected = YES;
        }else{
            self.setDufltBtn.selected = NO;
        }
    }
    
}
#pragma mark ==========请求地区地址==========
-(void)requestAddressData{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInWindow:Text_JIA_ZAI_ZHONG];
    [LBService post:ADDRESS_REGION_JSON params:@{} completion:^(LBResponse *response) {
        if (response.succeed) {
            [MBProgressHUD hideHUD];
            //成功
            weakSelf.rootModel = [HBAreaModel mj_objectWithKeyValues:response.result[@"data"]];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========选择默认地址==========
- (IBAction)clickSetDefuBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        //选择了默认
        self.isDefult = 1;
    }else{
        self.isDefult  = 0;
    }
}

#pragma mark ==========点击省市区==========
- (IBAction)shengShiQuBtn:(UIButton *)sender {
    [self.view endEditing:YES];
    if (![HBHuTool judgeArrayIsEmpty:self.rootModel.region]) {
        [app showToastView:@"接口数据错误"];
        return;
    }
    self.addressPushView = [[HBMeAddressPushView alloc]init];
    self.addressPushView.model = self.rootModel;
    WEAKSELF;
    self.addressPushView.clcikDimss = ^(NSString *address,NSString *codeStr) {
        weakSelf.addressStr = address;
        weakSelf.codeStr = codeStr;
        weakSelf.areaLab.text = address;
        weakSelf.areaLab.textColor = [UIColor blackColor];
    };
    [self.addressPushView show];
}
#pragma mark ==========点击街道==========
- (IBAction)jieDaoAddressBtn:(UIButton *)sender {
    [app showToastView:@"暂不用选择"];
}


#pragma mark ==========点击默认按钮==========
- (IBAction)clickMoRenBtn:(UIButton *)sender {
}
#pragma mark ==========点击保存按钮==========
- (IBAction)clickConfBtn:(UIButton *)sender {
    if ([HBHuTool isJudgeString:self.personNameTF.text]) {
        [app showToastView:@"请填写收件人姓名"];
        return;
    }
    if ([HBHuTool isJudgeString:self.phoneTF.text]) {
        [app showToastView:@"请填写联系人电话号码"];
        return;
    }
    if ([HBHuTool isJudgeString:self.codeTF.text]) {
        [app showToastView:@"请填写邮政编码"];
        return;
    }
    if ([HBHuTool isJudgeString:self.addressStr]) {
        [app showToastView:@"请选择地区"];
        return;
    }
    if ([HBHuTool isJudgeString:self.detailAddTF.text]) {
        [app showToastView:@"请填写街道地址"];
        return;
    }
    [MBProgressHUD showActivityMessageInWindow:Text_JIA_ZAI_ZHONG];
    [LBService post:ADDRESS_CREATE_MEMBER params:@{@"area":self.codeStr,@"addr":self.detailAddTF.text,@"name":self.personNameTF.text,@"mobile":self.phoneTF.text,@"zip":self.codeTF.text,@"def_addr":@(self.isDefult)} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功
            [app showToastView:@"保存成功"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AddAddressViewController" object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
