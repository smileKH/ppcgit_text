//
//  MyAddressViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyAddressViewController.h"
#import "MyAdderssCell.h"
#import "AddAddressViewController.h"
#import "HBAddressModel.h"
@interface MyAddressViewController ()
//@property (nonatomic, strong) HBAddressModel * addressModel;
@end

@implementation MyAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"地址管理";
    
    [self setupTableview];
    
    [self setNavBar];
    
    //请求地址
    [self requestAddressModel:YES];
    WEAKSELF;
    //默认block方法：设置下拉刷新
//    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestAddressModel:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestAddressModel:NO];
//    }];
    //刷新
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadData) name:@"AddAddressViewController" object:nil];
}
-(void)reloadData{
    [self requestAddressModel:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark ==========请求地址==========
-(void)requestAddressModel:(BOOL)isRefresh{
    WEAKSELF;
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    [LBService post:ADDRESS_LIST_MEMBER params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"total"]intValue];
                NSArray *arr = [dic objectForKey:@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [Default_address mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            }

            
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptImg:@"bbs_addressImg" withLabelName:@"您还没有收货地址" withButtonName:@"新增地址"];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}
#pragma mark ==========点击添加收货地址==========
-(void)clickSpaceButton{
    AddAddressViewController *vc = [[AddAddressViewController alloc] initWithNibName:@"AddAddressViewController" bundle:nil];
    vc.title = @"新增收货地址";
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)setNavBar
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"新增" forState:UIControlStateNormal];
    [button setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(addAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}
-(void)addAction{
    AddAddressViewController *vc = [[AddAddressViewController alloc] initWithNibName:@"AddAddressViewController" bundle:nil];
    vc.title = @"新增收货地址";
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)setupTableview
{
    [self.tableview registerNib:[UINib nibWithNibName:@"MyAdderssCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}
#pragma mark ==========tableview delegate==========
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.allData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyAdderssCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.allData[indexPath.section];
    cell.clickMoRenBtn = ^(Default_address *model) {
        //点击默认
        [self moRenAddress:model];
    };
    cell.clickEditBtn = ^(Default_address *model) {
        //点击编辑
        [self clickEditBtn:model];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定要删除该商品?删除后无法恢复!" preferredStyle:1];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //这里要调接口删除商品
            [self callbackInterfaceDeleteItems:indexPath didTableView:tableView];
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}
#pragma mark 调接口删除地址
-(void)callbackInterfaceDeleteItems:(NSIndexPath *)indexPath didTableView:(UITableView *)tableView{
    Default_address *model = self.allData[indexPath.section];
    WEAKSELF;
    [LBService post:ADDRESS_MENBER_DELETE params:@{@"addr_id":model.addr_id} completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            [app showToastView:@"删除成功"];
            [weakSelf requestAddressModel:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}
-(void)clickEditBtn:(Default_address *)model{
    AddAddressViewController *vc = [[AddAddressViewController alloc]init];
    vc.model = model;
    vc.title = @"编辑收货地址";
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========设置默认地址==========
-(void)moRenAddress:(Default_address *)model{
    WEAKSELF;
    [LBService post:ADDRESS_SET_DEFAULTT params:@{@"addr_id":model.addr_id} completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            [app showToastView:@"设置成功"];
            //刷新界面
            [weakSelf requestAddressModel:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}

@end
