//
//  HBAreaModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/2.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBAreaProvinceModel;
@class HBAreaCityModel;
@class HBAreaChildrenModel;
@interface HBAreaModel : NSObject
@property (nonatomic, strong) NSArray * region;
@end

//省
@interface HBAreaProvinceModel : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, strong) NSString * parentId;
@property (nonatomic, strong) NSArray * children;
@end
//市
@interface HBAreaCityModel : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, strong) NSString * parentId;
@property (nonatomic, strong) NSArray * children;
@end

@interface HBAreaChildrenModel : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, strong) NSString * parentId;
@end
