//
//  HBAreaModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/2.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBAreaModel.h"

@implementation HBAreaModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"region":@"HBAreaProvinceModel"};
}
@end

@implementation HBAreaProvinceModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"children":@"HBAreaCityModel"};
}
@end

@implementation HBAreaCityModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"children":@"HBAreaChildrenModel"};
}
@end

@implementation HBAreaChildrenModel

@end
