//
//  HBAddressModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/13.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
//@class HBAddressListModel;
@class Default_address;
@class HBAddressCountModel;
@interface HBAddressModel : NSObject
@property (nonatomic, strong) HBAddressCountModel * count;
@property (nonatomic, strong) NSArray * list;
@end

@interface HBAddressCountModel : NSObject
@property (nonatomic, strong) NSString * nowcount;
@property (nonatomic, strong) NSString * maxcount;
@end


//@interface HBAddressListModel : NSObject
//@property (nonatomic, strong) NSString * addr_id;
//@property (nonatomic, strong) NSString * area;
//@property (nonatomic, strong) NSString * mobile;
//@property (nonatomic, strong) NSString * addrdetail;
//@property (nonatomic, strong) NSString * addr;
//@property (nonatomic, strong) NSString * zip;
//@property (nonatomic, strong) NSString * user_id;
//@property (nonatomic, strong) NSString * tel;
//@property (nonatomic, strong) NSString * def_addr;
//@property (nonatomic, strong) NSString * name;
//@property (nonatomic, strong) NSString * region_id;
//@end

