//
//  MyCouponsUnuseCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyCouponsUnuseCell.h"

@implementation MyCouponsUnuseCell
- (void)setModel:(HBCouponsModel *)model{
    _model = model;
    self.totalMoneyLab.text = [NSString stringWithFormat:@"%@",model.deduct_money];
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.shop_name];
    self.detailLab.text = [NSString stringWithFormat:@"%@",model.coupon_desc];
    NSString *startStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd" tenNumber:model.start_time];
    NSString *endStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd" tenNumber:model.end_time];
    self.dateLab.text = [NSString stringWithFormat:@"%@-%@",startStr,endStr];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
