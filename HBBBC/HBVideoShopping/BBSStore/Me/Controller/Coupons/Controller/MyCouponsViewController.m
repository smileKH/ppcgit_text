//
//  MyCouponsViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyCouponsViewController.h"
#import "MyCouponsUsedCell.h"
#import "MyCouponsUnuseCell.h"
#import "MyCouponsExpireCell.h"
#import "MyCouponsListModel.h"
@interface MyCouponsViewController ()
@property (nonatomic, strong) UIView *line;
@property (nonatomic, assign) NSInteger pageState;
@property (nonatomic, strong) MyCouponsListModel * coupModel;
@end

@implementation MyCouponsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"我的优惠券";
    
    self.line.center = CGPointMake(self.btn1.center.x, 39);
    [self.topview addSubview:self.line];
    
    [self setupTableview];
    [self setNavBar];
    [self setupData];
}
- (void)setupData{
    self.fields = @"";
    self.orderBy = @"";
    self.shop_id = @"";
    self.is_valid = 1;// 0已使用 1有效 2已过期
    self.platform = @"";//平台   不传为所有平台优惠券
    [self getmyCoupons:YES];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}

- (void)getmyCoupons:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    NSMutableDictionary *paramester = [NSMutableDictionary dictionary];
    paramester[@"page_size"] = @(10);
    paramester[@"page_no"] = integerString(page_no);
    paramester[@"platform"] = self.platform;
    paramester[@"is_valid"] = @(self.is_valid);
    paramester[@"shop_id"] = @"";
    paramester[@"orderBy"] = @"";
    paramester[@"fields"] = @"";
    paramester[@"accessToken"] = LocalValue(TOKEN_XY_APP);
    paramester[@"page_size"] = @(self.pageSize);
    paramester[@"page_no"] = @(self.pageNo);
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:CENTER_COUPON_LIST params:paramester completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //成功
            weakSelf.coupModel = [MyCouponsListModel mj_objectWithKeyValues:response.result[@"data"]];
            
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.coupModel.list]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptImg:@"bbs_cardImg" withLabelName:@"您目前没有优惠券，快去领取吧"];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];

}
- (void)setupTableview
{
    [self.tableview registerNib:[UINib nibWithNibName:@"MyCouponsUsedCell" bundle:nil] forCellReuseIdentifier:@"used"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"MyCouponsUnuseCell" bundle:nil] forCellReuseIdentifier:@"use"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"MyCouponsExpireCell" bundle:nil] forCellReuseIdentifier:@"expire"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)topSelect:(UIButton *)sender {
    self.btn1.selected = NO;
    self.btn2.selected = NO;
    self.btn3.selected = NO;
    self.pageState = sender.tag -1;
    sender.selected = !sender.selected;
    if (self.pageState==0) {//未使用 有效
        self.is_valid = 1;//1有效
    }else if (self.pageState==1){
        self.is_valid = 0;//0已使用
    }else if (self.pageState==2){
        self.is_valid = 2;//2已过期
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.line.center = CGPointMake(sender.center.x, 39);
    }];
    //请求数据
    [self getmyCoupons:YES];
}

- (UIView *)line{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = MAINTextCOLOR;
        _line.bounds = CGRectMake(0, 0, SCREEN_WIDTH/3, 3);
    }
    return _line;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.coupModel.list.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.pageState==0) {//未使用
        MyCouponsUnuseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"use" forIndexPath:indexPath];
        cell.model = self.coupModel.list[indexPath.row];
        
        return cell;
    }else if (self.pageState==1){//已使用
        
        MyCouponsUsedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"used" forIndexPath:indexPath];
        cell.model = self.coupModel.list[indexPath.row];
        
        return cell;
    }else{//已过期
        MyCouponsExpireCell *cell = [tableView dequeueReusableCellWithIdentifier:@"expire" forIndexPath:indexPath];
         cell.model = self.coupModel.list[indexPath.row];
        
        return cell;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 93;
}

#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
@end
