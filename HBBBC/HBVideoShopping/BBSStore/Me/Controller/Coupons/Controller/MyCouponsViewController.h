//
//  MyCouponsViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCouponsViewController : HBRootViewController
{
    NSInteger page_no;
}
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *topview;
- (IBAction)topSelect:(UIButton *)sender;
@property (nonatomic, strong) IBOutlet UIButton *btn1;
@property (nonatomic, strong) IBOutlet UIButton *btn2;
@property (nonatomic, strong) IBOutlet UIButton *btn3;

//@property (nonatomic, strong) NSString *page_no;
//@property (nonatomic, strong) NSString *page_size;
@property (nonatomic, strong) NSString *fields;
@property (nonatomic, strong) NSString *orderBy;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, assign) NSInteger is_valid;//获取是否有效的参数 0已使用 1有效 2已过期
@property (nonatomic, strong) NSString *platform;
@end
