//
//  MyCouponsListModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Cur_symbol;
@class Pagers;
@class HBCouponsModel;
@interface MyCouponsListModel : NSObject
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@property (nonatomic, strong) Pagers * pagers;
@property (nonatomic, strong) NSArray * list;
@end

@interface HBCouponsModel :NSObject
@property (nonatomic, strong) NSString * coupon_desc;
@property (nonatomic, strong) NSString * is_valid;
@property (nonatomic, strong) NSString * canuse_start_time;
@property (nonatomic, strong) NSString * coupon_code;
@property (nonatomic, strong) NSString * canuse_end_time;
@property (nonatomic, strong) NSString * deduct_money;
@property (nonatomic, strong) NSString * coupon_name;
@property (nonatomic, strong) NSString * shop_name;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * coupon_id;
@property (nonatomic, strong) NSString * used_platform;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSNumber * start_time;
@property (nonatomic, strong) NSNumber * end_time;
@property (nonatomic, strong) NSString * obtain_time;
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * limit_money;
@property (nonatomic, strong) NSString * shop_id;
@end
