//
//  HBReturnGoodsListTabCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/28.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HBReturnGoodsListTabCell : UITableViewCell
@property (nonatomic, strong) HBOrderList * model;

@property (weak, nonatomic) IBOutlet UILabel *shopNameLab;
@property (weak, nonatomic) IBOutlet UILabel *orderStateLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UILabel *totalLab;
@property (weak, nonatomic) IBOutlet UIButton *payStateBtn;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UIView *imgScroView;
@property (weak, nonatomic) IBOutlet UIButton *otherBtn;

@property (nonatomic, copy) void(^clickPayStateBtn)(HBOrderList *model,NSString *btnStyle);
- (IBAction)clickBtn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *moreBtnView;

@end
