//
//  HBReturnGoodsListTabCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/28.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBReturnGoodsListTabCell.h"

@implementation HBReturnGoodsListTabCell
-(void)setModel:(HBOrderList *)model{
    _model = model;
    self.shopNameLab.text = model.shopname;
    self.orderStateLab.text = model.status_desc;
    self.numLab.text = [NSString stringWithFormat:@"共%@件",model.totalItem];
    self.totalLab.attributedText = [HBHuTool attributedOldString:@"实付金额:" priceString:model.payment];
    
    
    //判断，如果只有一件衣服，那么就展示一件
    if ([HBHuTool judgeArrayIsEmpty:model.order]) {
        //有
        UIScrollView *scroView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.imgScroView.width_sd, self.imgScroView.height_sd)];
        scroView.backgroundColor = [UIColor whiteColor];
        CGFloat imgWidth = 70;
        for (NSInteger i=0; i<model.order.count; i++) {
            UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(i*imgWidth+(i+1)*10, 0, imgWidth, imgWidth)];
            imageV.centerY_sd = scroView.centerY_sd;
            HBOrderModel *items = model.order[i];
            [imageV sd_setImageWithURL:[NSURL URLWithString:items.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
            
            [scroView addSubview:imageV];
        }
        scroView.contentSize = CGSizeMake(model.order.count*imgWidth+10, self.imgScroView.height_sd);
        [self.imgScroView addSubview:scroView];
    }
    //显示状态按钮
    NSArray *array = [self returnSetModelState:model];
    [self setStateViewSubView:array];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgScroView.userInteractionEnabled = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickBtn:(UIButton *)sender {
    
}

#pragma mark = 设置状态
-(void)setStateViewSubView:(NSArray *)array{
    
    [self.moreBtnView removeAllSubview];
    //添加分类
    CGFloat buttonWidth = 65;
    CGFloat buttonHight = 25;
    CGFloat buttonSping = 10;
    CGFloat topSpacing = (40-25)/2;
    CGFloat viewWid = 170;
    for (int i =0; i<array.count; i++) {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.frame = CGRectMake(viewWid-(buttonWidth+buttonSping)*(i+1), topSpacing, buttonWidth, buttonHight);
        [aButton setTitle:array[i] forState:UIControlStateNormal];
        [aButton setTitleColor:white_Color forState:UIControlStateNormal];
        aButton.layer.borderWidth = 0.5;
        aButton.layer.borderColor = MAINTextCOLOR.CGColor;
        aButton.layer.cornerRadius = 3;
        aButton.titleLabel.font = textFont_Content14;
        aButton.tag = 100+i;
        aButton.backgroundColor = MAINTextCOLOR;
        [aButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.moreBtnView addSubview:aButton];
    }
    
}

-(void)clickButton:(UIButton *)button{
    self.clickPayStateBtn(self.model, button.titleLabel.text);
}
-(NSArray *)returnSetModelState:(HBOrderList *)model{
    NSArray *array = [NSArray array];
//    //设置状态按钮
//    if ([model.sku.aftersales_status isEqualToString:WAIT_BUYER_RETURN_GOODS]&&[model.sku.complaints_status isEqualToString:COMPLAINTS_NOT_COMPLAINTS]) {
//        //买家未进行投诉;
//        array = @[@"填写信息"];
//
//    }else if ([model.sku.aftersales_status isEqualToString:SELLER_REFUSE_BUYER]&&[model.sku.complaints_status isEqualToString:COMPLAINTS_WAIT_SYS_AGREE]){
//        //商家接受申请，等待消费者回寄
//        array = @[];
//
//    }else if ([model.sku.complaints_status isEqualToString:COMPLAINTS_NOT_COMPLAINTS]){
//        //商家接受申请，等待消费者回寄
//        array = @[@"投诉商家"];
//
//    }else{
//        return @[];
//    }
    
    array = @[];
    return array;
}
@end
