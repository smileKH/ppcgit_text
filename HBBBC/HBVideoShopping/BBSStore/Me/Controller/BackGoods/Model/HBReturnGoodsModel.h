//
//  HBReturnGoodsModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/28.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Cur_symbol;
@class Pagers;
@class HBSku;
@class HBReturnGoodsListModel;

@interface HBReturnGoodsModel : NSObject
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@property (nonatomic, strong) Pagers * pagers;
@property (nonatomic, strong) NSArray * list;
@end

@interface HBReturnGoodsListModel : NSObject
@property (nonatomic, strong) NSString * is_complaints;
@property (nonatomic, strong) NSString * created_time;
@property (nonatomic, strong) NSString * shopname;
@property (nonatomic, strong) NSString * aftersales_bn;
@property (nonatomic, strong) NSString * aftersales_type;
@property (nonatomic, strong) NSString * gift_count;
@property (nonatomic, strong) NSString * aftersales_type_desc;
@property (nonatomic, strong) NSString * totalItem;
@property (nonatomic, strong) NSString * gift_data;
@property (nonatomic, strong) NSString * progress;
@property (nonatomic, strong) NSString * oid;
@property (nonatomic, strong) NSString * num;
@property (nonatomic, strong) HBSku    * sku;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * shop_id;
@end

@interface HBSku : NSObject
@property (nonatomic, strong) NSString * aftersales_status;
@property (nonatomic, strong) NSString * bn;
@property (nonatomic, strong) NSString * complaints_status;
@property (nonatomic, strong) NSString * points_fee;
@property (nonatomic, strong) NSString * item_id;
@property (nonatomic, strong) NSString * consume_point_fee;
@property (nonatomic, strong) NSString * sku_id;
@property (nonatomic, strong) NSString * spec_nature_info;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * oid;
@property (nonatomic, strong) NSString * pic_path;
@property (nonatomic, strong) NSString * payment;
@end
