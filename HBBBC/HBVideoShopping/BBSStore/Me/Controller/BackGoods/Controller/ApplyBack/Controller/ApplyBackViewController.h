//
//  ApplyBackViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyBackViewController : HBRootViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UITextView *textView;

- (IBAction)clickReturnBtn:(UIButton *)sender;
@property (nonatomic, strong) NSString * oid;
@end
