//
//  HBApplyTypeCell.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBApplyTypeModel.h"
@interface HBApplyTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *textLab;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (nonatomic ,strong)HBApplyTypeModel *model;
@property (nonatomic ,copy)void(^clickTypeModel)(HBApplyTypeModel *model);
- (IBAction)clickBtn:(UIButton *)sender;

@end
