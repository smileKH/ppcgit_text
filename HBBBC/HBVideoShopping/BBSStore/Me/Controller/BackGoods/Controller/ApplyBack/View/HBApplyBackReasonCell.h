//
//  HBApplyBackReasonCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBApplyBackModel.h"
@interface HBApplyBackReasonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *textLab;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
- (IBAction)clickSelectBtn:(UIButton *)sender;
@property (nonatomic ,strong)HBApplyReasonModel *model;
@property (nonatomic ,copy)void(^clickReasonModel)(HBApplyReasonModel *model);
@end
