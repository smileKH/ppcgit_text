//
//  ApplyBackViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ApplyBackViewController.h"

#import "ApplyBackHeaderCell.h"
//#import "ApplyBackReasonCell.h"
//#import "ApplyBackTypeTableViewCell.h"
#import "HBApplyBackHeaderView.h"
#import "HBApplyBackReasonCell.h"
#import "HBApplyBackModel.h"
#import "HBApplyTypeModel.h"
#import "HBApplyTypeCell.h"
@interface ApplyBackViewController ()
@property (nonatomic ,strong)NSMutableArray *backArray;
@property (nonatomic ,strong)HBApplyBackModel *model;
@property (nonatomic ,strong)NSString *typeString;
@property (nonatomic ,strong)NSString *resonString;
@end

@implementation ApplyBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"申请售后";//从商品详情申请售后
    [self setupTableview];
    [self setNavBar];
    [self setBackArrayData];
    
    //请求申请售后的数据
    [self retuqestBackGoodsData];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}
//初始化数据
-(void)setBackArrayData{
    self.backArray = [NSMutableArray array];
    NSArray *array = @[@{@"name":@"仅退款",@"isSelect":@YES},@{@"name":@"退货退款",@"isSelect":@NO},@{@"name":@"换货",@"isSelect":@NO},];
    for (NSInteger i=0; i<array.count; i++) {
        HBApplyTypeModel *model = [HBApplyTypeModel new];
        NSDictionary *dic = array[i];
        model.name = dic[@"name"];
        model.select = NO;
        [self.backArray addObject:model];
    }
    [self.tableview reloadData];
}
#pragma mark ==========申请售后数据==========
-(void)retuqestBackGoodsData{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:AFTERSALES_MEMBER_APPLY_INFO params:@{@"oid":self.oid} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.model = [HBApplyBackModel mj_objectWithKeyValues:response.result[@"data"]];
            [weakSelf.tableview reloadData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)setupTableview
{
    [self.tableview registerNib:[UINib nibWithNibName:@"ApplyBackHeaderCell" bundle:nil] forCellReuseIdentifier:@"header"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"HBApplyBackReasonCell" bundle:nil] forCellReuseIdentifier:@"HBApplyBackReasonCell"];
     [self.tableview registerNib:[UINib nibWithNibName:@"HBApplyBackHeaderView" bundle:nil] forCellReuseIdentifier:@"HBApplyBackHeaderView"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"HBApplyTypeCell" bundle:nil] forCellReuseIdentifier:@"HBApplyTypeCell"];
    
    self.tableview.tableFooterView = self.header;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else if (section==1){
        return self.backArray.count+1;
    }else{
        return self.model.reasonArray.count+1;
    }
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    WEAKSELF;
    if (indexPath.section == 0) {
        
        ApplyBackHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"header"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.model;
        return cell;
        
    }else if(indexPath.section == 1){
        if (indexPath.row==0) {
            HBApplyBackHeaderView *cell = [tableView dequeueReusableCellWithIdentifier:@"HBApplyBackHeaderView"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.headerLab.text = @"请选择售后类型";
            return cell;
        }else{
            HBApplyTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBApplyTypeCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.model = self.backArray[indexPath.row-1];
            cell.clickTypeModel = ^(HBApplyTypeModel *model) {
                [weakSelf selectTypeIndexPath:indexPath];
            };
            return cell;
        }
    }else{
        if (indexPath.row==0) {
            HBApplyBackHeaderView *cell = [tableView dequeueReusableCellWithIdentifier:@"HBApplyBackHeaderView"];
            cell.headerLab.text = @"请选择售后原因";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else{
            HBApplyBackReasonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBApplyBackReasonCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.model = self.model.reasonArray[indexPath.row-1];
            cell.clickReasonModel = ^(HBApplyReasonModel *model) {
                [weakSelf selectReasonIndexPath:indexPath];
            };
            return cell;
        }
    }
}
#pragma mark ==========选择类型==========
-(void)selectTypeIndexPath:(NSIndexPath *)indexPath{
    HBApplyTypeModel *model = self.backArray[indexPath.row-1] ;
    //选中
    model.select = YES ;
    //其它置为不选中
    for (int i=0; i<self.backArray.count; i++) {
        if (i==indexPath.row-1) {
            continue ;
        }else{
            HBApplyTypeModel *tempModel = self.backArray[i] ;
            tempModel.select = NO ;
        }
    }
    self.typeString = model.name;
    [self.tableview reloadData];
}
-(void)selectReasonIndexPath:(NSIndexPath *)indexPath{
    HBApplyReasonModel *model = self.model.reasonArray[indexPath.row-1] ;
    //选中
    model.isSelect = YES ;
    //其它置为不选中
    for (int i=0; i<self.model.reasonArray.count; i++) {
        if (i==indexPath.row-1) {
            continue ;
        }else{
            HBApplyReasonModel *tempModel = self.model.reasonArray[i] ;
            tempModel.isSelect = NO ;
        }
    }
    self.resonString = model.name;
    [self.tableview reloadData] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        //
    }else if (indexPath.section==1){
        if (indexPath.row==0) {
            //
        }else{
           [self selectTypeIndexPath:indexPath];
        }
        
    }else{
        if (indexPath.row==0) {
            //
        }else{
            [self selectReasonIndexPath:indexPath];
        }
        
    }
}
#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
#pragma mark ==========点击申请退货按钮==========
- (IBAction)clickReturnBtn:(UIButton *)sender {
    //开始申请退货咯
    if ([HBHuTool isJudgeString:self.typeString]) {
        [app showToastView:@"请选择售后类型"];
        return;
    }
    if ([HBHuTool isJudgeString:self.resonString]) {
        [app showToastView:@"请选择售后原因"];
        return;
    }
    if ([self.resonString isEqualToString:@"其他原因"]) {
        if ([HBHuTool isJudgeString:self.textView.text]) {
            [app showToastView:@"请填写详细描述"];
            return;
        }
    }
    NSString *aftersalsStr = nil;
    if ([self.typeString isEqualToString:@"仅退款"]) {
        aftersalsStr = @"ONLY_REFUND";
    }else if ([self.typeString isEqualToString:@"退货退款"]){
        aftersalsStr = @"REFUND_GOODS";
    }else{
        aftersalsStr = @"EXCHANGING_GOODS";
    }
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"tid"] = self.model.orderInfo.tid;
    parameters[@"oid"] = self.model.orderInfo.oid;
    parameters[@"reason"] = self.resonString;
    parameters[@"description"] = self.textView.text;
    parameters[@"aftersales_type"] = aftersalsStr;
    parameters[@"evidence_pic"] = @"";
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:AFTERSALES_MEMBER_APPLY params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //申请成功 跳到列表
            [app showToastView:@"申请成功"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"ApplyBackViewController" object:nil];
           [weakSelf.navigationController popViewControllerAnimated:YES];
            
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
@end
