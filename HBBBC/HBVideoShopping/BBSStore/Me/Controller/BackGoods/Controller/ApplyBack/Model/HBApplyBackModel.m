//
//  HBApplyBackModel.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBApplyBackModel.h"

@implementation HBApplyBackModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"logistics":@"HBLogisticsModel"};
}
- (void)setReason:(NSArray *)reason{
    [self setReasonArray:reason];
}

-(void)setReasonArray:(NSArray *)reasonArray{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSInteger i = 0; i < reasonArray.count; i ++) {
        NSString *str = reasonArray[i];
        HBApplyReasonModel *model = [HBApplyReasonModel new];
        model.name = str;
//        model.isSelect = NO;

//        if (i==0) {
//            model.isSelect = YES;
//        }else{
//            model.isSelect = NO;
//        }
        
        [arr addObject:model];
    }
    _reasonArray = (NSArray *)arr;
}
@end

@implementation HBLogisticsModel

@end

@implementation HBApplyReasonModel

@end

@implementation HBApplyOrderInfoModel

@end

