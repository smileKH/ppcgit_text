//
//  HBApplyBackReasonCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBApplyBackReasonCell.h"

@implementation HBApplyBackReasonCell
- (void)setModel:(HBApplyReasonModel *)model{
    _model = model;
    self.textLab.text = model.name;
    self.selectBtn.selected = model.isSelect;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickSelectBtn:(UIButton *)sender {
    self.clickReasonModel(self.model);
}
@end
