//
//  ApplyBackHeaderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBApplyBackModel.h"
@interface ApplyBackHeaderCell : UITableViewCell
@property (nonatomic ,strong)HBApplyBackModel *model;
@property (weak, nonatomic) IBOutlet UILabel *ordLab;
@property (weak, nonatomic) IBOutlet UILabel *textLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;

@end
