//
//  HBApplyTypeModel.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBApplyTypeModel : NSObject
@property (nonatomic ,strong)NSString *name;
@property (nonatomic ,assign)BOOL select;
@end
