//
//  HBApplyBackHeaderView.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBApplyBackHeaderView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerLab;

@end
