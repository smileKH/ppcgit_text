//
//  HBApplyTypeCell.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBApplyTypeCell.h"

@implementation HBApplyTypeCell
- (void)setModel:(HBApplyTypeModel *)model{
    _model = model;
    self.textLab.text = model.name;
    self.selectBtn.selected = model.select;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickBtn:(UIButton *)sender {
    self.clickTypeModel(self.model);
}
@end
