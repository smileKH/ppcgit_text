//
//  HBApplyBackModel.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/29.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Cur_symbol;
@class HBLogisticsModel;
@class HBApplyReasonModel;
@class HBApplyOrderInfoModel;
@interface HBApplyBackModel : NSObject
@property (nonatomic ,strong)Cur_symbol *cur_symbol;
@property (nonatomic ,strong)NSArray *logistics;
@property (nonatomic ,strong)NSArray *reason;
@property (nonatomic ,strong)HBApplyOrderInfoModel *orderInfo;
@property (nonatomic ,strong)NSArray *reasonArray;
@end

@interface HBLogisticsModel : NSObject
@property (nonatomic ,strong)NSString *corp_code;
@property (nonatomic ,strong)NSString *corp_name;
@end

@interface HBApplyReasonModel : NSObject
@property (nonatomic ,strong)NSString *name;
@property (nonatomic ,assign)BOOL isSelect;
@end

@interface HBApplyOrderInfoModel : NSObject
@property (nonatomic ,strong)NSString *aftersales_status;
@property (nonatomic ,strong)NSString *complaints_status;
@property (nonatomic ,strong)NSString *cat_id;
@property (nonatomic ,strong)NSString *item_id;
@property (nonatomic ,strong)NSString *refund_enabled;
@property (nonatomic ,strong)NSString *sku_id;
@property (nonatomic ,strong)NSString *spec_nature_info;
@property (nonatomic ,strong)NSString *title;
@property (nonatomic ,strong)NSString *price;
@property (nonatomic ,strong)NSString *oid;
@property (nonatomic ,strong)NSString *num;
@property (nonatomic ,strong)NSString *end_time;
@property (nonatomic ,strong)NSString *changing_enabled;
@property (nonatomic ,strong)NSString *tid;
@property (nonatomic ,strong)NSString *pic_path;
@property (nonatomic ,strong)NSString *status;
@end
