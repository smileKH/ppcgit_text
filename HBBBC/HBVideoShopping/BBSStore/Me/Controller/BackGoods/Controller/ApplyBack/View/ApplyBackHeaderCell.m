//
//  ApplyBackHeaderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ApplyBackHeaderCell.h"

@implementation ApplyBackHeaderCell
- (void)setModel:(HBApplyBackModel *)model{
    _model = model;
    self.ordLab.text = [NSString stringWithFormat:@"订单号:%@",model.orderInfo.oid];
    self.textLab.text = [NSString stringWithFormat:@"%@",model.orderInfo.title];
    self.numberLab.text = [NSString stringWithFormat:@"%@件",model.orderInfo.num];
    self.moneyLab.text = [NSString stringWithFormat:@"￥%@",model.orderInfo.price];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.orderInfo.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
