//
//  MyReutrnGoodsViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyReutrnGoodsViewController.h"
#import "HBMyOrderDetailVC.h"

#import "ApplyBackViewController.h"
#import "ReturnGoodsDetailViewController.h"
#import "HBReturnGoodsListTabCell.h"
//#import "HBReturnGoodsModel.h"

#import "AddComplainViewController.h"
#import "AddLogViewController.h"
#import "HBApplyReturnDetailVC.h"
@interface MyReutrnGoodsViewController ()
//@property (nonatomic, strong) HBReturnGoodsModel * returnModel;
@end

@implementation MyReutrnGoodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"申请售后";//会员退换货记录列表
    [self setupTableview];
    [self setNavBar];
    //请求数据
    [self requestReturnList:YES];
//    WEAKSELF;
    //默认block方法：设置下拉刷新
//    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestReturnList:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestReturnList:NO];
//    }];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateData) name:@"AddComplainViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateData) name:@"LogDetailViewController" object:nil];
}
-(void)updateData{
    //请求数据
    [self requestReturnList:YES];
}
#pragma mark ==========退换货列表==========
-(void)requestReturnList:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    parameter[@"status"] = WAIT_RATE;
    WEAKSELF;
    [LBService post:MY_TRADE_LIST params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            HBMyOrderModel *model = [HBMyOrderModel mj_objectWithKeyValues:response.result[@"data"]];
            NSInteger number = model.pagers.page_count;
            NSArray *arr = model.list;
            if ([HBHuTool judgeArrayIsEmpty:arr]) {
                //数据转模型
                NSArray *array = [HBOrderList mj_objectArrayWithKeyValuesArray:arr];
                //正常有数据 隐藏无数据view
                if (array != nil && array.count > 0) {
                    [weakSelf.allData addObjectsFromArray:array];
                    
                }
            }
            //加载 没有更多数据
            if ((weakSelf.pageNo > number)&&(number>0)) {
                [app showToastView:Text_NoMoreData];
                
            }
            
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptNormal];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}

- (void)setupTableview
{
    [self.tableview registerNib:[UINib nibWithNibName:@"HBReturnGoodsListTabCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.tableFooterView = [[UIView alloc]init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allData.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HBReturnGoodsListTabCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.allData[indexPath.row];
    WEAKSELF;
    cell.clickPayStateBtn = ^(HBOrderList *model, NSString *btnStyle) {
        [weakSelf clickPayBtn:model andType:btnStyle];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 250;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HBOrderList *model = self.allData[indexPath.row];
    //查看详情
    [self seeReturnGoodsList:model];
    
}
-(void)seeReturnGoodsList:(HBOrderList *)model{
    HBApplyReturnDetailVC *vc = [[HBApplyReturnDetailVC alloc]initWithNibName:@"HBApplyReturnDetailVC" bundle:nil];
    vc.tidStr = model.tid;
    [self.navigationController pushViewController:vc animated:YES];
//    ReturnGoodsDetailViewController *vc= [[ReturnGoodsDetailViewController alloc] initWithNibName:@"ReturnGoodsDetailViewController" bundle:nil];
//    vc.aftersales_bn = model.aftersales_bn;
//    vc.oid = model.oid;
//    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========点击了按钮==========
-(void)clickPayBtn:(HBOrderList *)model andType:(NSString *)btnStyle{
    /*
    //设置状态按钮
    if ([model.sku.aftersales_status isEqualToString:WAIT_BUYER_RETURN_GOODS]&&[model.sku.complaints_status isEqualToString:COMPLAINTS_NOT_COMPLAINTS]) {
        //商家接受申请，等待消费者回寄 @"填写信息" 跳到物流列表
        AddLogViewController *vc = [[AddLogViewController alloc]initWithNibName:@"AddLogViewController" bundle:nil];
        vc.aftersales_bn = model.aftersales_bn;
        vc.controllerClass = @"MyReutrnGoodsViewController";
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if ([model.sku.aftersales_status isEqualToString:SELLER_REFUSE_BUYER]&&[model.sku.complaints_status isEqualToString:COMPLAINTS_WAIT_SYS_AGREE]){
        //查看详情
        ReturnGoodsDetailViewController *vc= [[ReturnGoodsDetailViewController alloc] initWithNibName:@"ReturnGoodsDetailViewController" bundle:nil];
        vc.aftersales_bn = model.aftersales_bn;
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if ([model.sku.complaints_status isEqualToString:COMPLAINTS_NOT_COMPLAINTS]){
        //商家接受申请，等待消费者回寄
        //买家未进行投诉; @"投诉商家" 跳到投诉
        AddComplainViewController *vc = [[AddComplainViewController alloc]initWithNibName:@"AddComplainViewController" bundle:nil];
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        
    }*/

}
#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
