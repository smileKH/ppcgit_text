//
//  HBCustomerServiceVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/21.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCustomerServiceVC.h"

@interface HBCustomerServiceVC ()
@property (nonatomic, strong) NSString * im_account_qq;
@end

@implementation HBCustomerServiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"联系平台";
    //请求平台
    [self requestQQData];
    
    
}
#pragma mark ==========请求平台工具==========
-(void)requestQQData{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:COMMON_PLATFORM_CST params:nil completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                weakSelf.im_account_qq = dic[@"im_account_qq"];
                
                //添加子视图
                [weakSelf addRegisteredProtocolSubView];
            }
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
-(void)addRegisteredProtocolSubView{
    NSString *str = [NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=web",self.im_account_qq];
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
