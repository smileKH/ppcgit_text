//
//  ReturnGoodsDetailHeaderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ReturnGoodsDetailHeaderCell.h"

@implementation ReturnGoodsDetailHeaderCell
- (void)setModel:(HBReturnGoodsDetailModel *)model{
    _model = model;
    NSString *applyStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.created_time];
    self.applyDateLab.text = [NSString stringWithFormat:@"申请时间:%@",applyStr];
    
   NSString *endStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.modified_time];
    
    self.endDateLab.text = [NSString stringWithFormat:@"完成时间:%@",endStr];
    self.stateLab.text = [NSString stringWithFormat:@"%@",model.status_desc];
    
    if ([model.progress isEqualToString:@"0"]) {
        //提交申请
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_nnn");
        self.threeImg.image = ImageNamed(@"bbs_nnn");
        self.fourImg.image = ImageNamed(@"bbs_nnn");
        self.fiveImg.image = ImageNamed(@"bbs_nnn");
        self.detailLab.text = @"亲爱的客户，请耐心等待";
    }else if ([model.progress isEqualToString:@"1"]){
        //等待寄回
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_nnn");
        self.fourImg.image = ImageNamed(@"bbs_nnn");
        self.fiveImg.image = ImageNamed(@"bbs_nnn");
        self.detailLab.text = @"亲爱的客户，商家处理成功请寄回商品";
    }else if ([model.progress isEqualToString:@"2"]){
        //已寄回
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_sss");
        self.fourImg.image = ImageNamed(@"bbs_nnn");
        self.fiveImg.image = ImageNamed(@"bbs_nnn");
        self.detailLab.text = @"亲爱的客户，请耐心等待";
    }else if ([model.progress isEqualToString:@"3"]){
        //商家驳回
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.hidden = YES;
        self.threeImg.hidden = YES;
        self.fourImg.hidden = YES;
        self.twoLab.hidden = YES;
        self.threeLab.hidden = YES;
        self.fourLab.hidden = YES;
        self.fiveImg.image = ImageNamed(@"bbs_sss");
        self.fiveLab.text = @"商家驳回";
        self.detailLab.text = @"亲爱的客户，好遗憾，商家已驳回";
    }else if ([model.progress isEqualToString:@"4"]){
        //退款
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_sss");
        self.fourImg.image = ImageNamed(@"bbs_sss");
        self.fiveImg.image = ImageNamed(@"bbs_nnn");
        self.detailLab.text = @"亲爱的客户，退款中，请耐心等待";
    }else if ([model.progress isEqualToString:@"5"]){
        //完成
        self.oneImg.image = ImageNamed(@"bbs_sss");
        self.twoImg.image = ImageNamed(@"bbs_sss");
        self.threeImg.image = ImageNamed(@"bbs_sss");
        self.fourImg.image = ImageNamed(@"bbs_sss");
        self.fiveImg.image = ImageNamed(@"bbs_sss");
        self.detailLab.text = @"亲爱的客户，退款已完成";
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
