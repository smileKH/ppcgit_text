//
//  HBReturnGoodsDetailModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/28.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Cur_symbol;
@interface HBReturnGoodsDetailModel : NSObject
@property (nonatomic, strong) NSString * descriptionStr;
@property (nonatomic, strong) NSNumber * created_time;
@property (nonatomic, strong) NSString * is_show_return_goods;
@property (nonatomic, strong) NSString * aftersales_bn;
@property (nonatomic, strong) NSString * aftersales_type;
@property (nonatomic, strong) NSString * aftersales_type_desc;
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@property (nonatomic, strong) NSString * show_shop_return_goods;
@property (nonatomic, strong) NSNumber * modified_time;
@property (nonatomic, strong) NSString * progress;
@property (nonatomic, strong) NSString * evidence_pic;
@property (nonatomic, strong) NSString * reason;
@property (nonatomic, strong) NSString * sendback_data;
@property (nonatomic, strong) NSString * sendconfirm_data;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) NSString * shop_explanation;

@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * is_return_goods;
@end
