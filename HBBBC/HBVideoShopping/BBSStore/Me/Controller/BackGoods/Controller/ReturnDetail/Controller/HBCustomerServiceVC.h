//
//  HBCustomerServiceVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/21.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBCustomerServiceVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
