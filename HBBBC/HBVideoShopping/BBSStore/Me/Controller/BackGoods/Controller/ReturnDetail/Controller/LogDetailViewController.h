//
//  LogDetailViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBAddLogModel.h"
@interface LogDetailViewController : HBRootViewController
@property (weak, nonatomic) IBOutlet UITextField *orderTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *addressTF;
@property (nonatomic, strong) NSString * aftersales_bn;
@property (nonatomic ,strong)HBAddLogListModel *addModel;
- (IBAction)clickBtn:(UIButton *)sender;
@property (nonatomic, strong) NSString * controllerClass;

@end
