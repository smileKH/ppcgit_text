//
//  ReturnGoodsDetailOrderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ReturnGoodsDetailOrderCell.h"

@implementation ReturnGoodsDetailOrderCell
- (void)setModel:(HBReturnGoodsDetailModel *)model{
    _model = model;
    self.numberLab.text = [NSString stringWithFormat:@"%@",model.aftersales_bn];
    self.returnType.text = [NSString stringWithFormat:@"%@",model.aftersales_type_desc];
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickPingTaiBtn:(UIButton *)sender {
    self.clickPingTaiBtn(nil);
}
@end
