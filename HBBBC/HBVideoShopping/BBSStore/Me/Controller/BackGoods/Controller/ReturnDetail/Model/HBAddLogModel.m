//
//  HBAddLogModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBAddLogModel.h"

@implementation HBAddLogModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"list":@"HBAddLogListModel"};
}
@end

@implementation HBAddLogListModel

@end
