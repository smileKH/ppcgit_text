//
//  ReturnGoodsDetailHeaderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBReturnGoodsDetailModel.h"
@interface ReturnGoodsDetailHeaderCell : UITableViewCell
@property (nonatomic, strong) HBReturnGoodsDetailModel * model;
@property (weak, nonatomic) IBOutlet UILabel *applyDateLab;
@property (weak, nonatomic) IBOutlet UILabel *stateLab;
@property (weak, nonatomic) IBOutlet UILabel *endDateLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;

@property (weak, nonatomic) IBOutlet UIImageView *oneImg;
@property (weak, nonatomic) IBOutlet UILabel *oneLab;
@property (weak, nonatomic) IBOutlet UIImageView *twoImg;
@property (weak, nonatomic) IBOutlet UILabel *twoLab;
@property (weak, nonatomic) IBOutlet UIImageView *threeImg;
@property (weak, nonatomic) IBOutlet UILabel *threeLab;
@property (weak, nonatomic) IBOutlet UIImageView *fourImg;
@property (weak, nonatomic) IBOutlet UILabel *fourLab;
@property (weak, nonatomic) IBOutlet UIImageView *fiveImg;
@property (weak, nonatomic) IBOutlet UILabel *fiveLab;

@end
