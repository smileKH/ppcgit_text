//
//  AddLogCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "AddLogCell.h"

@implementation AddLogCell
- (void)setModel:(HBAddLogListModel *)model{
    _model = model;
    self.textLab.text = [NSString stringWithFormat:@"%@",model.corp_name];
    if (model.isSelect) {
        self.selectBtn.hidden = NO;
    }else{
        self.selectBtn.hidden = YES;
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickBtn:(UIButton *)sender {
}
@end
