//
//  AddLogCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBAddLogModel.h"
@interface AddLogCell : UITableViewCell
@property (nonatomic ,strong)HBAddLogListModel *model;
@property (weak, nonatomic) IBOutlet UILabel *textLab;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
- (IBAction)clickBtn:(UIButton *)sender;

@end
