//
//  ReturnGoodsDetailReasonCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBReturnGoodsDetailModel.h"
@interface ReturnGoodsDetailReasonCell : UITableViewCell
@property (nonatomic, strong) HBReturnGoodsDetailModel * model;
@property (weak, nonatomic) IBOutlet UILabel *resonLab;
@property (weak, nonatomic) IBOutlet UILabel *describeLab;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLab;

@end
