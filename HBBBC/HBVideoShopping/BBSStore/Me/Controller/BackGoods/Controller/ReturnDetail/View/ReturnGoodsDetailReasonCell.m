//
//  ReturnGoodsDetailReasonCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ReturnGoodsDetailReasonCell.h"

@implementation ReturnGoodsDetailReasonCell
- (void)setModel:(HBReturnGoodsDetailModel *)model{
    _model = model;
    self.resonLab.text = [NSString stringWithFormat:@"%@",model.reason];
    if (![HBHuTool isJudgeString:model.descriptionStr]) {
        self.describeLab.text = [NSString stringWithFormat:@"%@",model.descriptionStr];
    }else{
        self.describeLab.text = [NSString stringWithFormat:@"%@",@"无详细描述"];
    }
    
    self.instructionsLab.text = [NSString stringWithFormat:@"%@",model.shop_explanation];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
