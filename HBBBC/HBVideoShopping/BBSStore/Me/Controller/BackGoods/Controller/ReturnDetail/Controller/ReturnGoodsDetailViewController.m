//
//  ReturnGoodsDetailViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ReturnGoodsDetailViewController.h"
#import "ReturnGoodsDetailOrderCell.h"
#import "ReturnGoodsDetailHeaderCell.h"
#import "ReturnGoodsDetailReasonCell.h"
#import "ReturnGoodsDetailLogisticsCell.h"

#import "AddLogViewController.h"
#import "HBReturnGoodsDetailModel.h"
#import "HBCustomerServiceVC.h"
@interface ReturnGoodsDetailViewController ()
@property (nonatomic, strong) HBReturnGoodsDetailModel * detailModel;
@end

@implementation ReturnGoodsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"申请售后";
    [self setupTableview];
    [self setNavBar];
    
    [self requestGoodsDetailData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(upadloadData) name:@"LogDetailViewController" object:nil];
}
//更新
-(void)upadloadData{
    [self requestGoodsDetailData];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}
#pragma mark ==========请求数据==========
-(void)requestGoodsDetailData{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"aftersales_bn"] = self.aftersales_bn;
    parameters[@"oid"] = self.oid;
    [LBService post:AFTERSALES_MEMBER_GET params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.detailModel = [HBReturnGoodsDetailModel mj_objectWithKeyValues:response.result[@"data"]];
            //初始化数据
            [weakSelf.tableview reloadData];
//            [weakSelf initSetReturnGoodsDetailData];
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)setupTableview
{
    [self.tableview registerNib:[UINib nibWithNibName:@"ReturnGoodsDetailOrderCell" bundle:nil] forCellReuseIdentifier:@"order"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"ReturnGoodsDetailReasonCell" bundle:nil] forCellReuseIdentifier:@"reason"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"ReturnGoodsDetailHeaderCell" bundle:nil] forCellReuseIdentifier:@"header"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"ReturnGoodsDetailLogisticsCell" bundle:nil] forCellReuseIdentifier:@"log"];
    
//    self.tableview.tableFooterView = self.header;
}
#pragma mark ==========初始化数据==========
-(void)initSetReturnGoodsDetailData{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        ReturnGoodsDetailHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"header"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.detailModel;
        return cell;
    }else if (indexPath.section == 1) {
        ReturnGoodsDetailOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"order"];
        cell.orderLab.text = [NSString stringWithFormat:@"%@",self.oid];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.detailModel;
        WEAKSELF;
        cell.clickPingTaiBtn = ^(id obj) {
            //跳到QQ
            HBCustomerServiceVC *vc = [[HBCustomerServiceVC alloc]initWithNibName:@"HBCustomerServiceVC" bundle:nil];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
        return cell;
    }else if (indexPath.section == 2) {
        ReturnGoodsDetailReasonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reason"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.detailModel;
        return cell;
    }else{
        ReturnGoodsDetailLogisticsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"log"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.detailModel;
        return cell;
    }
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.detailModel.is_return_goods isEqualToString:@"0"]) {
        //隐藏
        return 3;
    }else{
        //显示
       return 4;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3) {
        AddLogViewController *vc =[[AddLogViewController alloc] initWithNibName:@"AddLogViewController" bundle:nil];
        vc.aftersales_bn = self.detailModel.aftersales_bn;
        vc.controllerClass = @"ReturnGoodsDetailViewController";
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
@end
