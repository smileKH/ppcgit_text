//
//  AddLogViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "AddLogViewController.h"
#import "AddLogCell.h"
#import "LogDetailViewController.h"
#import "HBAddLogModel.h"
@interface AddLogViewController ()
@property (nonatomic, strong) HBAddLogModel * addModel;
@property (nonatomic ,strong)HBAddLogListModel *selectModel;
@end

@implementation AddLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"填写物流信息";
    [self.tableview registerNib:[UINib nibWithNibName:@"AddLogCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self setNavBar];
    //请求物流列表
    [self requestLogiDetailData:YES];
//    WEAKSELF;
//    //默认block方法：设置下拉刷新
//    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestLogiDetailData:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestLogiDetailData:NO];
//    }];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//    [messageBtn1 setImage:BGPIC(@"nav_more") forState:UIControlStateNormal];
    [messageBtn1 setTitle:@"确定" forState:UIControlStateNormal];
    [messageBtn1 setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
    messageBtn1.titleLabel.font = [UIFont systemFontOfSize:14];;
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    self.navigationItem.rightBarButtonItems = @[item1];
}
#pragma mark ==========请求物流公司==========
-(void)requestLogiDetailData:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    self.pageSize = 100;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//    parameter[@"page_size"] = @(self.pageSize);
//    parameter[@"page_no"] = @(self.pageNo);
    WEAKSELF;
    [LBService post:LOGISTICS_LIST params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.addModel = [HBAddLogModel mj_objectWithKeyValues:response.result[@"data"]];
            
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.addModel.list]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptNormal];
        }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)confirmAction
{
    LogDetailViewController *vc = [[LogDetailViewController alloc]initWithNibName:@"LogDetailViewController" bundle:nil];
    vc.addModel = self.selectModel;
    vc.aftersales_bn = self.aftersales_bn;
    vc.controllerClass = self.controllerClass;
    [self.navigationController pushViewController:vc animated:YES];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
        AddLogCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.model = self.addModel.list[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.addModel.list.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectTypeIndexPath:indexPath];
}
#pragma mark ==========选择类型==========
-(void)selectTypeIndexPath:(NSIndexPath *)indexPath{
    HBAddLogListModel *model = self.addModel.list[indexPath.row] ;
    //选中
    model.isSelect = YES ;
    //其它置为不选中
    for (int i=0; i<self.addModel.list.count; i++) {
        if (i==indexPath.row) {
            continue ;
        }else{
            HBAddLogListModel *tempModel = self.addModel.list[i] ;
            tempModel.isSelect = NO ;
        }
    }
    self.selectModel = model;
    [self.tableview reloadData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    if (self.selectModel) {
        LogDetailViewController *vc = [[LogDetailViewController alloc]initWithNibName:@"LogDetailViewController" bundle:nil];
        vc.addModel = self.selectModel;
        vc.aftersales_bn = self.aftersales_bn;
        vc.controllerClass = self.controllerClass;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [app showToastView:@"请选择物流公司"];
    }
    
}
@end
