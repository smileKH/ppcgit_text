//
//  ReturnGoodsDetailViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReturnGoodsDetailViewController : HBRootViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic, strong) NSString * aftersales_bn;
@property (nonatomic, strong) NSString * oid;
@end
