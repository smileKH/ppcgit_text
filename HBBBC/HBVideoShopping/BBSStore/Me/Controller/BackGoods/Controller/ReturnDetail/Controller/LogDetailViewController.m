//
//  LogDetailViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "LogDetailViewController.h"
#import "ReturnGoodsDetailViewController.h"
#import "MyReutrnGoodsViewController.h"
@interface LogDetailViewController ()

@end

@implementation LogDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"填写物流信息";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickBtn:(UIButton *)sender {

    if ([HBHuTool isJudgeString:self.orderTF.text]) {
        [app showToastView:@"请填写物流单号"];
        return;
    }
    if ([HBHuTool isJudgeString:self.phoneTF.text]) {
        [app showToastView:@"请填写手机号码"];
        return;
    }
    if ([HBHuTool isJudgeString:self.addressTF.text]) {
        [app showToastView:@"请填写收货地址"];
        return;
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"aftersales_bn"] = self.aftersales_bn;
    parameters[@"corp_code"] =  self.addModel.corp_code;
    parameters[@"logi_name"] = self.addModel.corp_name;
    parameters[@"logi_no"] = self.orderTF.text;
    parameters[@"receiver_address"] = self.addressTF.text;
    parameters[@"mobile"] = self.phoneTF.text;
    [LBService post:LOGISTICS_SEND params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            [app showToastView:@"提交成功"];
            //来一个通知刷新
            [[NSNotificationCenter defaultCenter]postNotificationName:@"LogDetailViewController" object:nil];
            id vc = nil;
            if ([weakSelf.controllerClass isEqualToString:@"ReturnGoodsDetailViewController"]) {
                vc = [ReturnGoodsDetailViewController class];
            }
            else if([weakSelf.controllerClass isEqualToString:@"MyReutrnGoodsViewController"]){
                vc = [MyReutrnGoodsViewController class];

            }
            NSArray *temArray = weakSelf.navigationController.viewControllers;
            for(UIViewController *temVC in temArray){
                if ([temVC isKindOfClass:vc]){
                    [weakSelf.navigationController popToViewController:temVC animated:YES];
                }
            }
        }else{
            [app showToastView:response.message];
        }
    }];
}
@end
