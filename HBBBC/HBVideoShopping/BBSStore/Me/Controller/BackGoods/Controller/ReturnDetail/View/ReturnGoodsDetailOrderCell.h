//
//  ReturnGoodsDetailOrderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBReturnGoodsDetailModel.h"
@interface ReturnGoodsDetailOrderCell : UITableViewCell
@property (nonatomic, strong) HBReturnGoodsDetailModel * model;
@property (weak, nonatomic) IBOutlet UILabel *orderLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;

@property (weak, nonatomic) IBOutlet UILabel *returnType;
@property (weak, nonatomic) IBOutlet UIButton *pinBtn;
- (IBAction)clickPingTaiBtn:(UIButton *)sender;
@property (nonatomic, copy) void(^clickPingTaiBtn)(id obj);
@end
