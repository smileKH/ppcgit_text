//
//  HBAddLogModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBAddLogListModel;
@interface HBAddLogModel : NSObject
@property (nonatomic, strong) NSArray * list;
@end

@interface HBAddLogListModel : NSObject
@property (nonatomic, strong) NSString * corp_code;
@property (nonatomic, strong) NSString * corp_name;
@property (nonatomic ,assign)BOOL isSelect;
@end
