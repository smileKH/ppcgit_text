//
//  HBApplyReturnDetailCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/26.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBApplyReturnDetailCell.h"

@implementation HBApplyReturnDetailCell

-(void)setModel:(HBMyOrderDetailGoodsModel *)model{
    _model = model;
    [self.goodsImgView sd_setImageWithURL:[NSURL URLWithString:model.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    self.goodsContentLab.text = [NSString stringWithFormat:@"%@",model.title];
    self.goodsNubLab.text = [NSString stringWithFormat:@"x%ld",model.num];
    
    self.goodsMoneyLab.text = [NSString stringWithFormat:@"￥%.02f",[model.total_fee doubleValue]];
    self.goodsMoneyLab.textColor = MAINREDCOLOR;
    
    //显示状态按钮
    NSArray *array = [self returnSetModelState:model];
    [self setStateViewSubView:array];
}
#pragma mark = 设置状态
-(void)setStateViewSubView:(NSArray *)array{
    //添加分类
    CGFloat buttonWidth = 65;
    CGFloat buttonHight = 25;
    CGFloat buttonSping = 10;
    CGFloat topSpacing = 0;
    CGFloat viewWid = 120;
    UIView *scroView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.moreBtnView.width_sd, self.moreBtnView.height_sd)];
    scroView.backgroundColor = [UIColor whiteColor];
    for (int i =0; i<array.count; i++) {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.frame = CGRectMake(viewWid-(buttonWidth+buttonSping)*(i+1), topSpacing, buttonWidth, buttonHight);
        [aButton setTitle:array[i] forState:UIControlStateNormal];
        [aButton setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
        aButton.titleLabel.font = textFont_ContentVice12;
        [aButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [scroView addSubview:aButton];
    }
    
    [self.moreBtnView addSubview:scroView];
    
}
-(NSArray *)returnSetModelState:(HBMyOrderDetailGoodsModel *)model{
    NSArray *array = [NSArray array];
    
    if (![model.status isEqualToString:TRADE_FINISHED]) {
        return @[];
    }
    
    if ([model.aftersales_status isEqualToString:WAIT_SELLER_AGREE]) {
        ////买家已经申请退款，等待卖家同意;
        array = @[AF_SHOU_CHU_LI];
        
    }else if ([model.aftersales_status isEqualToString:WAIT_BUYER_RETURN_GOODS]){
        //卖家已经同意退款，等待买家退货;
        array = @[AF_QING_TUI_HUO];
        
    }
    else if ([model.aftersales_status isEqualToString:WAIT_SELLER_CONFIRM_GOODS]){
        //买家已经退货，等待卖家确认收货;
        array = @[AF_QUE_REN_SHOU_HUO];
        
    }
    else if ([model.aftersales_status isEqualToString:AFTERSALES_SUCCESS]){
        
        array = @[AF_TUI_KUAN_WAN_CHENG];
        
    }else if([model.aftersales_status isEqualToString:AFTERSALES_CLOSED]){
        //退款关闭;
        array = @[];
    }else if([model.aftersales_status isEqualToString:AFTERSALES_REFUNDING]){
        //退款中;
        array = @[AF_TUI_KUAN_ZHON];
    }else if([model.aftersales_status isEqualToString:SELLER_REFUSE_BUYER]){
        //卖家拒绝退款; 判断投诉状态
        if ([model.complaints_status isEqualToString:COMPLAINTS_NOT_COMPLAINTS]) {
            array = @[AF_TOU_SU_SHANG_JIA,AF_SHOU_HOU_BO_HUI];
        }else if ([model.complaints_status isEqualToString:COMPLAINTS_WAIT_SYS_AGREE]){
            array = @[AF_TOU_SU_CHU_LI_ZHONG,AF_SHOU_HOU_BO_HUI];
        }else if ([model.complaints_status isEqualToString:COMPLAINTS_FINISHED]){
            //投诉处理完成 再次申请退换货
            array = @[AF_ZAI_CI_SHEN_QING,AF_SHOU_HOU_BO_HUI];
        }else if ([model.complaints_status isEqualToString:COMPLAINTS_BUYER_CLOSED]){
            //买家撤销投诉; 投诉已撤销
            array = @[AF_TOU_SU_CEH_XIAO,AF_SHOU_HOU_BO_HUI];
        }else if ([model.complaints_status isEqualToString:COMPLAINTS_CLOSED]){
            //平台关闭投诉，不需要处理直接关闭;) 投诉已关闭
            array = @[AF_TOU_SU_GUAN_BI,AF_SHOU_HOU_BO_HUI];
        }
        
    }else if([model.aftersales_status isEqualToString:SELLER_SEND_GOODS]){
        //卖家已发货;)
        array = @[];
    }else{//为空 申请退换货
        array = @[AF_TUI_HUAN_HUO];
    }
    return array;
}

-(void)clickButton:(UIButton *)button{
    self.clickOrderGoodsStateBtn(self.model, button.titleLabel.text);
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
