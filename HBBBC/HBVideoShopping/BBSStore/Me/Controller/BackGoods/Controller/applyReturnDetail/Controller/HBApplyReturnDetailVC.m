//
//  HBApplyReturnDetailVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/26.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBApplyReturnDetailVC.h"
#import "HBApplyReturnDetailCell.h"

#import "AddLogViewController.h"
#import "MyComplainDetailViewController.h"
#import "AddComplainViewController.h"
#import "ApplyBackViewController.h"
#import "ReturnGoodsDetailViewController.h"
@interface HBApplyReturnDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) HBMyOrderDetailModel * detailModel;
@end

@implementation HBApplyReturnDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"申请售后详情";
    //添加子视图
    [self setupUI];
    //请求订单详情
    [self requestOrderDetail];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"ApplyBackViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"LogDetailViewController" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(uplaodData) name:@"AddComplainViewController" object:nil];
}
//更新
-(void)uplaodData{
    //请求订单详情
    [self requestOrderDetail];
}
#pragma mark ==========添加子视图==========
-(void)setupUI{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"HBApplyReturnDetailCell" bundle:nil] forCellReuseIdentifier:@"HBApplyReturnDetailCell"];
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableView];
}
#pragma mark ==========请求订单详情==========
-(void)requestOrderDetail{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MY_ORDER_TRADE params:@{@"tid":self.tidStr} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.detailModel = [HBMyOrderDetailModel mj_objectWithKeyValues:response.result[@"data"]];
            
            [weakSelf.tableView reloadData];
            //设置bottomView
//            [weakSelf setBottomViewData];
        }else{
            
        }
    }];
}
#pragma mark ==========tableview delegate==========
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.detailModel.orders.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //支付地址
    HBApplyReturnDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBApplyReturnDetailCell"];
    cell.model = self.detailModel.orders[indexPath.row];
    WEAKSELF;
    cell.clickOrderGoodsStateBtn = ^(HBMyOrderDetailGoodsModel *model, NSString *btnStyle) {
        [weakSelf selectModel:model andButtonText:btnStyle];
    };
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
#pragma mark ==========处理申请退货按钮==========
-(void)selectModel:(HBMyOrderDetailGoodsModel *)model andButtonText:(NSString *)buttonText{
    
    if ([buttonText isEqualToString:AF_TUI_HUAN_HUO]) {
        //申请退换货 申请
        //不在售后范围内，那么可以申请售后
        ApplyBackViewController *vc = [[ApplyBackViewController alloc]initWithNibName:@"ApplyBackViewController" bundle:nil];
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
    }else if([buttonText isEqualToString:AF_SHOU_CHU_LI]){
        //售后处理中 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_QING_TUI_HUO]){
        //已同意,请退货 待寄回
        AddLogViewController *vc = [[AddLogViewController alloc]initWithNibName:@"AddLogViewController" bundle:nil];
        vc.aftersales_bn = model.aftersales_bn;
        vc.controllerClass = @"MyReutrnGoodsViewController";
        [self.navigationController pushViewController:vc animated:YES];
    }else if([buttonText isEqualToString:AF_QUE_REN_SHOU_HUO]){
        //待卖家确认收货 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_TUI_KUAN_ZHON]){
        //退款中 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_TUI_KUAN_WAN_CHENG]){
        //退款完成 查看详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_SHOU_HOU_BO_HUI]){
        //售后驳回 退换货详情
        [self pushReturnGoodsDetail:model];
    }else if([buttonText isEqualToString:AF_TOU_SU_SHANG_JIA]){
        //投诉商家 去投诉
        AddComplainViewController *vc = [[AddComplainViewController alloc]initWithNibName:@"AddComplainViewController" bundle:nil];
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if([buttonText isEqualToString:AF_TOU_SU_CHU_LI_ZHONG]){
        //投诉处理中 投诉详情
        [self pushComplainDetailDetail:model];
    }else if([buttonText isEqualToString:AF_ZAI_CI_SHEN_QING]){
        //再次申请退换货
        ApplyBackViewController *vc = [[ApplyBackViewController alloc]initWithNibName:@"ApplyBackViewController" bundle:nil];
        vc.oid = model.oid;
        [self.navigationController pushViewController:vc animated:YES];
    }else if([buttonText isEqualToString:AF_TOU_SU_CEH_XIAO]){
        //投诉已撤销
        [self pushComplainDetailDetail:model];
    }else if([buttonText isEqualToString:AF_TOU_SU_GUAN_BI]){
        //投诉已关闭
        [self pushComplainDetailDetail:model];
    }
}
#pragma mark ==========跳到投诉详情==========
-(void)pushComplainDetailDetail:(HBMyOrderDetailGoodsModel *)model{
    MyComplainDetailViewController *vc = [[MyComplainDetailViewController alloc]initWithNibName:@"MyComplainDetailViewController" bundle:nil];
    vc.oid = model.oid;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========跳到退货详情==========
-(void)pushReturnGoodsDetail:(HBMyOrderDetailGoodsModel *)model{
    ReturnGoodsDetailViewController *vc= [[ReturnGoodsDetailViewController alloc] initWithNibName:@"ReturnGoodsDetailViewController" bundle:nil];
    vc.aftersales_bn = model.aftersales_bn;
    vc.oid = model.oid;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
