//
//  HBApplyReturnDetailVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/26.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface HBApplyReturnDetailVC : HBRootViewController
@property (nonatomic, strong) NSString * tidStr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
