//
//  ChangeNameViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ChangeNameViewController.h"

@interface ChangeNameViewController ()



@end

@implementation ChangeNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    ////[self.navigationController setBackButton];
//    UISQURE
    self.title = @"修改名称";
    if (![HBHuTool isJudgeString:curUser.username]) {
        self.nameTF.text = [NSString stringWithFormat:@"%@",curUser.username];
    }
    
}
#pragma mark ==========点击保存按钮==========
- (IBAction)saveBtn:(UIButton *)sender {
    if ([HBHuTool isJudgeString:self.nameTF.text]) {
        [app showToastView:@"填填写名字"];
        return;
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MEMBER_BASICS_UPDATE params:@{@"username":self.nameTF.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            curUser.username = weakSelf.nameTF.text;
        
            [app showToastView:@"修改成功"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
