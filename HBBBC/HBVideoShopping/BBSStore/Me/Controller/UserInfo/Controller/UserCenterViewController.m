//
//  UserCenterViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "UserCenterViewController.h"
#import "ChangeSexViewController.h"
#import "ChangeNameViewController.h"
#import "ChangeNickNameViewController.h"
#import "TZImagePickerController.h"
@interface UserCenterViewController ()

@end

@implementation UserCenterViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setInitValue];
    
    
}
#pragma mark ==========设置初始值==========
-(void)setInitValue{
    //设置初值
    if (![HBHuTool isJudgeString:curUser.login_account]) {
        self.accoutLab.text = [NSString stringWithFormat:@"%@",curUser.login_account];
    }else{
        self.accoutLab.text = [NSString stringWithFormat:@"%@",@""];
    }
    
    if ([curUser.username isEqualToString:@"null"]) {
        self.userName.text = [NSString stringWithFormat:@"%@",@"设置姓名"];
    }else if (![HBHuTool isJudgeString:curUser.username]) {
        self.userName.text = [NSString stringWithFormat:@"%@",curUser.username];
    }else{
        self.userName.text = [NSString stringWithFormat:@"%@",@"设置姓名"];
    }
    
    if ([curUser.name isEqualToString:@"null"]) {
        self.nickName.text = [NSString stringWithFormat:@"%@",@"设置昵称"];
    }else if (![HBHuTool isJudgeString:curUser.name]) {
        self.nickName.text = [NSString stringWithFormat:@"%@",curUser.name];
    }else{
        self.nickName.text = [NSString stringWithFormat:@"%@",@"设置昵称"];
    }
    
    if (curUser.sex==0) {
        self.sexLab.text = @"女";
    }else if(curUser.sex==1){
        self.sexLab.text = @"男";
    }else{
        self.sexLab.text = @"保密";
    }
    
    //设置头像
    if ([HBHuTool isJudgeString:curUser.portrait_url]) {
        self.headerImgView.image = [UIImage imageNamed:@"bbs_header"];
    }else{
        [self.headerImgView sd_setImageWithURL:[NSURL URLWithString:curUser.portrait_url] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    }
    
    self.headerImgView.layer.cornerRadius = 22;
    self.headerImgView.layer.masksToBounds = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"会员信息";
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction1)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction2)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction3)];
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction4)];
    [self.view1 addGestureRecognizer:tap1];
    [self.view2 addGestureRecognizer:tap2];
    [self.view3 addGestureRecognizer:tap3];
    [self.view4 addGestureRecognizer:tap4];
    
    
}
//点击换头像 上传图片
- (void)tapAction1
{
    WEAKSELF;
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:nil];
    imagePickerVc.naviTitleColor = black_Color;
    imagePickerVc.barItemTextColor = black_Color;
//    imagePickerVc.oKButtonTitleColorNormal = black_Color;
//    imagePickerVc.oKButtonTitleColorDisabled = black_Color;
    //上传照片
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos,NSArray *assets,BOOL isSelectOriginalPhoto) {
        [weakSelf uploadLogoImg:photos andType:@"0"];
        //                    [weakself uploadLogoImg:photos];
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
    
    
}
- (void)tapAction2
{
    ChangeNameViewController *vc= [[ChangeNameViewController alloc] initWithNibName:@"ChangeNameViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)tapAction3
{
    ChangeNickNameViewController *vc= [[ChangeNickNameViewController alloc] initWithNibName:@"ChangeNickNameViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tapAction4
{
    ChangeSexViewController *vc= [[ChangeSexViewController alloc] initWithNibName:@"ChangeSexViewController" bundle:nil];
    
    [self.navigationController pushViewController:vc animated:YES];
}

//上传照片
-(void) uploadLogoImg:(NSArray *) imgs andType:(NSString *)type{

    if ([HBHuTool judgeArrayIsEmpty:imgs]) {
        [MBProgressHUD showActivityMessageInView:@"上传中..."];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSData *data = UIImageJPEGRepresentation(imgs[0], 0.5f);
        NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        NSString *str = [NSString stringWithFormat:@"%@%@",@"data:image/jpeg;base64,",encodedImageStr];
        parameters[@"image"] = str;
        parameters[@"upload_type"] = @"base64";
        parameters[@"image_input_title"] = @"headerImg.jpg";
        WEAKSELF;
        [LBService post:USER_UPDATE_PORTRAIT params:parameters completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //
                NSDictionary *dic = response.result[@"data"];
                if (ValidDict(dic)) {
                    //
                    NSString *url = dic[@"url"];
                    //
                    curUser.portrait_url = url;
                    
                    [app showToastView:@"修改成功"];
                    [weakSelf setInitValue];
                }
            }else{
                [app showToastView:response.message];
            }
        }];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
