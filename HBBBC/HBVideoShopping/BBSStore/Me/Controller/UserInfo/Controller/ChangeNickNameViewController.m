//
//  ChangeNickNameViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/20.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ChangeNickNameViewController.h"

@interface ChangeNickNameViewController ()

@end

@implementation ChangeNickNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    ////[self.navigationController setBackButton];
//    UISQURE
    self.title = @"修改昵称";
    if (![HBHuTool isJudgeString:curUser.name]) {
        self.nickNameLab.text = [NSString stringWithFormat:@"%@",curUser.name];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickBtn:(UIButton *)sender {
    if ([HBHuTool isJudgeString:self.nickNameLab.text]) {
        [app showToastView:@"填填写昵称"];
        return;
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MEMBER_BASICS_UPDATE params:@{@"name":self.nickNameLab.text} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            curUser.name = weakSelf.nickNameLab.text;
            [app showToastView:@"修改成功"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];

}
@end
