//
//  ChangeSexViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ChangeSexViewController.h"

@interface ChangeSexViewController ()
@property (nonatomic, assign) NSInteger isSex;
@end

@implementation ChangeSexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    ////[self.navigationController setBackButton];
//    UISQURE
    self.title = @"修改性别";
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction2)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction3)];
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction4)];
    [self.view1 addGestureRecognizer:tap2];
    [self.view2 addGestureRecognizer:tap3];
    [self.view3 addGestureRecognizer:tap4];
    
    //判断初值
    if (curUser.sex==0) {//女
        //
        self.btnOne.hidden = YES;//男
        self.btnTwo.hidden = NO;//女
        self.btnThree.hidden = YES;//保密
        
        self.isSex = 0;
        
    }else if(curUser.sex==1){//男
        self.btnOne.hidden = NO;
        self.btnTwo.hidden = YES;
        self.btnThree.hidden = YES;
        
        self.isSex =1;
        
    }else{
        //保密
        self.btnOne.hidden = YES;
        self.btnTwo.hidden = YES;
        self.btnThree.hidden = NO;
        
        self.isSex = 2;
    }
}

- (void)tapAction2
{
    self.btnOne.hidden = NO;//男
    self.btnTwo.hidden = YES;//女
    self.btnThree.hidden = YES;//保密
    
    self.isSex = 0;
}


- (void)tapAction3
{
    self.btnOne.hidden = YES;//男
    self.btnTwo.hidden = NO;//女
    self.btnThree.hidden = YES;//保密
    
    self.isSex = 1;
}

- (void)tapAction4
{
    self.btnOne.hidden = YES;//男
    self.btnTwo.hidden = YES;//女
    self.btnThree.hidden = NO;//保密
    
    self.isSex = 2;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickBtn:(UIButton *)sender {
    //点击按钮
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MEMBER_BASICS_UPDATE params:@{@"sex":@(self.isSex)} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            curUser.sex = weakSelf.isSex;
            [app showToastView:@"修改成功"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }
    }];
}
@end
