//
//  HBComplainDetailContentCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBComplainDetailModel.h"
@interface HBComplainDetailContentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (nonatomic, strong) HBComplainDetailModel * model;
@property (weak, nonatomic) IBOutlet UIButton *ceXiaoBtn;
- (IBAction)clickCeXiaoBtn:(UIButton *)sender;

@property (nonatomic, copy) void(^clickCeXiaoBtn)(id obj);

@end
