//
//  MyComplainDetailViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyComplainDetailViewController.h"
#import "HBComplainDetailHeaderCell.h"
#import "HBComplainDetailContentCell.h"
#import "HBComplainDetailFooterCell.h"
#import "HBComplainDetailModel.h"
#import "CancelComplainViewController.h"
@interface MyComplainDetailViewController ()
@property (nonatomic, strong) HBComplainDetailModel * detailModel;
@end

@implementation MyComplainDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"投诉详情";
    [self setupUI];
    //请求数据
    [self requestComplainList];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateData) name:@"CancelComplainViewController" object:nil];
}
#pragma mark ==========更新==========
-(void)updateData{
    //请求数据
    [self requestComplainList];
}
#pragma mark ==========添加子视图==========
-(void)setupUI{
    [self.tableview registerNib:[UINib nibWithNibName:@"HBComplainDetailHeaderCell" bundle:nil] forCellReuseIdentifier:@"HBComplainDetailHeaderCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"HBComplainDetailContentCell" bundle:nil] forCellReuseIdentifier:@"HBComplainDetailContentCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"HBComplainDetailFooterCell" bundle:nil] forCellReuseIdentifier:@"HBComplainDetailFooterCell"];
    self.tableview.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}
#pragma mark ==========退换货详情==========
-(void)requestComplainList{

    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"oid"] = self.oid;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    WEAKSELF;
    [LBService post:COMPLAINTS_GET params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            
        weakSelf.detailModel = [HBComplainDetailModel mj_objectWithKeyValues:response.result[@"data"]];
            
        }else{
            [app showToastView:response.message];
        }
        [weakSelf.tableview reloadData];
    }];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        HBComplainDetailHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBComplainDetailHeaderCell"];
        cell.model = self.detailModel;
        return cell;
    }else if(indexPath.section==1){
        HBComplainDetailContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBComplainDetailContentCell"];
        //这里可能有多项
        cell.titleLab.text = [NSString stringWithFormat:@"投诉内容"];
        cell.contentLab.text = [NSString stringWithFormat:@"%@",self.detailModel.content];
        //判断
        if ([self.detailModel.status isEqualToString:COMPLAINTS_WAIT_SYS_AGREE]) {
            //显示按钮
            cell.ceXiaoBtn.hidden = NO;
        }else{
            cell.ceXiaoBtn.hidden = YES;
        }
        WEAKSELF;
        cell.clickCeXiaoBtn = ^(id obj) {
            //撤销界面
            CancelComplainViewController *vc = [[CancelComplainViewController alloc]initWithNibName:@"CancelComplainViewController" bundle:nil];
            vc.model = weakSelf.detailModel;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
        return cell;
    }else if(indexPath.section==2){
        HBComplainDetailContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBComplainDetailContentCell"];
        //这里可能有多项
        cell.titleLab.text = [NSString stringWithFormat:@"撤销原因"];
        cell.contentLab.text = [NSString stringWithFormat:@"%@",self.detailModel.buyer_close_reasons];
        cell.ceXiaoBtn.hidden = YES;
        return cell;
    }else{
        HBComplainDetailFooterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HBComplainDetailFooterCell"];
        cell.model = self.detailModel;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==2) {
        if ([self.detailModel.status isEqualToString: COMPLAINTS_BUYER_CLOSED]) {
            return 1;
        }else{
            return 0;
        }
    }else{
       return  1;
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==2) {
        if ([self.detailModel.status isEqualToString: COMPLAINTS_BUYER_CLOSED]) {
            return 7;
        }else{
            return 0.01;
        }
    }
    return 7;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
