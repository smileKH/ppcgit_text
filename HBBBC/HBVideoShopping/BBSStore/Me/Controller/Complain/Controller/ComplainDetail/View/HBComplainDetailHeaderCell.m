//
//  HBComplainDetailHeaderCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBComplainDetailHeaderCell.h"

@implementation HBComplainDetailHeaderCell
- (void)setModel:(HBComplainDetailModel *)model{
    _model = model;
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.complaints_type];
    self.statusLab.text = [NSString stringWithFormat:@"%@",model.status_desc];
    NSString *dateStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.created_time];
    self.dateLab.text = [NSString stringWithFormat:@"%@",dateStr];
    self.contentLab.text = [NSString stringWithFormat:@"%@",model.order.title];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.order.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
