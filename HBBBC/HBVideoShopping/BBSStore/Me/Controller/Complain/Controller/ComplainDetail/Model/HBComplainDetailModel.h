//
//  HBComplainDetailModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/3.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBComplainDetailOrderModel;
@interface HBComplainDetailModel : NSObject
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSNumber * created_time;
@property (nonatomic, strong) NSString * shop_name;
@property (nonatomic, strong) NSString * buyer_close_reasons;
@property (nonatomic, strong) NSString * image_url;
@property (nonatomic, strong) NSString * complaints_id;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * memo;
@property (nonatomic, strong) NSString * oid;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) HBComplainDetailOrderModel * order;
@property (nonatomic, strong) NSString * tel;
@property (nonatomic, strong) NSString * complaints_type;
@property (nonatomic, strong) NSString * tid;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * shop_id;

@end

@interface HBComplainDetailOrderModel : NSObject
@property (nonatomic, strong) NSString * item_id;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * pic_path;

@end
