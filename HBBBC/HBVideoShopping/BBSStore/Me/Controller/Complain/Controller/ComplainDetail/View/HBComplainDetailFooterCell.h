//
//  HBComplainDetailFooterCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBComplainDetailModel.h"
@interface HBComplainDetailFooterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *compNameLab;
@property (weak, nonatomic) IBOutlet UILabel *orderLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (nonatomic, strong) HBComplainDetailModel * model;
@end
