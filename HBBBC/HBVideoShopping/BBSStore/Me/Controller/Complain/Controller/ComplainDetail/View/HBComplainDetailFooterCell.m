//
//  HBComplainDetailFooterCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/1.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBComplainDetailFooterCell.h"

@implementation HBComplainDetailFooterCell
- (void)setModel:(HBComplainDetailModel *)model{
    _model = model;
    self.compNameLab.text = [NSString stringWithFormat:@"%@",model.shop_name];
    self.orderLab.text = [NSString stringWithFormat:@"%@",model.oid];
    self.phoneLab.text = [NSString stringWithFormat:@"%@",model.tel];
   
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
