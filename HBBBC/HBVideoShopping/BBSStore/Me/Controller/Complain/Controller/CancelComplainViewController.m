//
//  CancelComplainViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/22.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "CancelComplainViewController.h"

@interface CancelComplainViewController ()

@end

@implementation CancelComplainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"取消投诉";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickBtn:(UIButton *)sender {
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"complaints_id"] = self.model.complaints_id;
    parameter[@"buyer_close_reasons"] = self.textView.text;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    WEAKSELF;
    [LBService post:COMPLAINTS_CLOSE params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            [app showToastView:@"取消成功"];
             [[NSNotificationCenter defaultCenter]postNotificationName:@"CancelComplainViewController" object:nil];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }
        
    }];
}
@end
