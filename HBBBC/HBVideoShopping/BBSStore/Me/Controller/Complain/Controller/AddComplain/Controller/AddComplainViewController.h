//
//  AddComplainViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddComplainViewController : HBRootViewController
@property (nonatomic, strong) NSString * oid;//传这个就可以了
@property (weak, nonatomic) IBOutlet UITextField *typeTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)clickBtn:(UIButton *)sender;
- (IBAction)clickTypeBtn:(UIButton *)sender;


@end
