//
//  HBAddComplainTypeVC.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/3.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import "HBOrderPayStyleModel.h"
@interface HBAddComplainTypeVC : HBRootViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)clickTypeBtn:(UIButton *)sender;
@property (nonatomic, copy) void(^clickComplainStyleVC)(HBOrderPayStyleModel *model);
@end
