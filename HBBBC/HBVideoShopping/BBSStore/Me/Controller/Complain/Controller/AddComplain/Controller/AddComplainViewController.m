//
//  AddComplainViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "AddComplainViewController.h"
#import "HBAddComplainTypeVC.h"
@interface AddComplainViewController ()
@property (nonatomic, strong) NSString * complaints_type;
@end

@implementation AddComplainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"发起投诉";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickBtn:(UIButton *)sender {
    if ([HBHuTool isJudgeString:self.complaints_type]) {
        [app showToastView:@"请选择投诉类型"];
        return;
    }
    if ([HBHuTool isJudgeString:self.phoneTF.text]) {
        [app showToastView:@"请填写手机号码"];
        return;
    }
    if ([HBHuTool isJudgeString:self.textView.text]) {
        [app showToastView:@"请填写投诉内容"];
        return;
    }
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"oid"] = self.oid;
    parameter[@"complaints_type"] = self.complaints_type;
    parameter[@"tel"] = self.phoneTF.text;
    parameter[@"content"] = self.textView.text;
    parameter[@"image_url"] = @"";
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    WEAKSELF;
    [LBService post:COMPLAINTS_CARATE params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            [app showToastView:@"提交成功"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AddComplainViewController" object:nil];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [app showToastView:response.message];
        }

    }];
}
#pragma mark ==========点击选择类型==========
- (IBAction)clickTypeBtn:(UIButton *)sender {
    HBAddComplainTypeVC *vc = [[HBAddComplainTypeVC alloc]init];
    WEAKSELF;
    vc.clickComplainStyleVC = ^(HBOrderPayStyleModel *model) {
        weakSelf.complaints_type = model.name;
        weakSelf.typeTF.text = model.name;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
@end
