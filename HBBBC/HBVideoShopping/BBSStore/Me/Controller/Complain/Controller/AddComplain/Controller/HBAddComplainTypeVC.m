//
//  HBAddComplainTypeVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/3.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBAddComplainTypeVC.h"
#import "HBOrderPayStyleCell.h"
@interface HBAddComplainTypeVC ()
@property (nonatomic, strong) NSMutableArray * payStyleArr;
@property (nonatomic, assign) NSInteger  selectIndex;
@end

@implementation HBAddComplainTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"投诉类型";
    //请求投诉类型
    [self requesTouSuData];
    self.payStyleArr = [NSMutableArray array];
    //NSArray *arr = @[@"商品问题",@"配送问题",@"支付问题",@"促销活动问题",@"账户问题",@"发票问题",@"系统问题",@"退货/换货问题",@"投诉工作人员",@"其他"];
    self.selectIndex = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"HBOrderPayStyleCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.tableFooterView = [[UIView alloc]init];
}
#pragma mark ==========投诉列表===========
-(void)requesTouSuData{
    WEAKSELF;
    [LBService post:COMMON_TYPE_LIST params:nil completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            NSArray *arr = response.result[@"data"];
            if ([HBHuTool judgeArrayIsEmpty:arr]) {
                for (NSInteger i=0; i<arr.count; i++) {
                    HBOrderPayStyleModel *model = [HBOrderPayStyleModel new];
                    model.name = arr[i];
                    if (i==0) {
                        model.isSelect = YES;
                    }else{
                        model.isSelect = NO;
                    }
                    [self.payStyleArr addObject:model];
                }
            }
            [weakSelf.tableView reloadData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HBOrderPayStyleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.payStyleArr[indexPath.row];
    WEAKSELF;
    cell.clickPayStyleBtn = ^(HBOrderPayStyleModel *model) {
        [weakSelf selectIndexPath:indexPath];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.payStyleArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectIndexPath:indexPath];
}
#pragma mark ==========选择优惠券==========
-(void)selectIndexPath:(NSIndexPath *)indexPath{
    HBOrderPayStyleModel *model = self.payStyleArr[indexPath.row] ;
    //选中
    model.isSelect = YES ;
    //其它置为不选中
    for (int i=0; i<self.payStyleArr.count; i++) {
        if (i==indexPath.row) {
            continue ;
        }else{
            HBOrderPayStyleModel *tempModel = self.payStyleArr[i] ;
            tempModel.isSelect = NO ;
        }
    }
    self.selectIndex = indexPath.row;
    [self.tableView reloadData] ;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickTypeBtn:(UIButton *)sender {
    HBOrderPayStyleModel *model = self.payStyleArr[self.selectIndex];
    self.clickComplainStyleVC(model);
    [self.navigationController popViewControllerAnimated:YES];
}
@end
