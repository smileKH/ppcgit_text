//
//  MyComplainViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyComplainViewController.h"
#import "MyComplainCell.h"
#import "MyComplainDetailViewController.h"
#import "HBMyComplainModel.h"
@interface MyComplainViewController ()
//@property (nonatomic, strong) HBMyComplainModel * comModel;
@end

@implementation MyComplainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    ////[self.navigationController setBackButton];
    self.title = @"我的投诉";
    [self.tableview registerNib:[UINib nibWithNibName:@"MyComplainCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    //请求数据
    [self requestComplainList:YES];
    WEAKSELF;
    //默认block方法：设置下拉刷新
//    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestComplainList:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestComplainList:NO];
//    }];
}
#pragma mark ==========退换货列表==========
-(void)requestComplainList:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    WEAKSELF;
    [LBService post:COMPLAINTS_LIST params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"total"]intValue];
                NSArray *arr = [dic objectForKey:@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [HBMyComplainListModel mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            }
        }else{
            [app showToastView:response.message];
        }
                if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
                    //有数据
                    [weakSelf hideContentNullPromptView];
                }else{
                    [weakSelf showContentNullPromptNormal];
                }
        [weakSelf.tableview reloadData];
        [weakSelf.tableview.mj_footer endRefreshing];
        [weakSelf.tableview.mj_header endRefreshing];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    MyComplainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.allData[indexPath.section];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.allData.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 7;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //投诉详情
    MyComplainDetailViewController *vc = [[MyComplainDetailViewController alloc]initWithNibName:@"MyComplainDetailViewController" bundle:nil];
    HBMyComplainListModel *model = self.allData[indexPath.section];
    vc.oid = model.oid;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
