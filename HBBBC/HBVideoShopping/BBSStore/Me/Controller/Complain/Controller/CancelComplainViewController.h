//
//  CancelComplainViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/22.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBComplainDetailModel.h"
@interface CancelComplainViewController : HBRootViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)clickBtn:(UIButton *)sender;
@property (nonatomic, strong) HBComplainDetailModel * model;
@end
