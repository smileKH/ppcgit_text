//
//  HBMyComplainModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/3.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Pagers;
@class HBMyComplainListModel;
@interface HBMyComplainModel : NSObject
@property (nonatomic, strong) Pagers * pagers;
@property (nonatomic, strong) NSArray * list;
@end

@interface HBMyComplainListModel : NSObject
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * complaints_type;
@property (nonatomic, strong) NSString * complaints_id;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * pic_path;
@property (nonatomic, strong) NSString * status_desc;
@property (nonatomic, strong) NSString * oid;
@property (nonatomic, strong) NSNumber * created_time;
@property (nonatomic, strong) NSString * item_id;
@end
