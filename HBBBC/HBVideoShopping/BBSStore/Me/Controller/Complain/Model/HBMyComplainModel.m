//
//  HBMyComplainModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/3.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBMyComplainModel.h"

@implementation HBMyComplainModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"list":@"HBMyComplainListModel"};
}
@end

@implementation HBMyComplainListModel

@end
