//
//  MyComplainCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyComplainCell.h"

@implementation MyComplainCell
- (void)setModel:(HBMyComplainListModel *)model{
    _model = model;
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.complaints_type];
    self.stateLab.text = [NSString stringWithFormat:@"%@",model.status_desc];
    self.contentLab.text = [NSString stringWithFormat:@"%@",model.title];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.pic_path] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    NSString *dateStr = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.created_time];
    self.dateLab.text = dateStr;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
