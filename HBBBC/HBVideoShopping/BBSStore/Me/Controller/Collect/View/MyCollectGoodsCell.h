//
//  MyCollectGoodsCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBCollectModel.h"
@interface MyCollectGoodsCell : UITableViewCell
@property (nonatomic, strong) HBCollectListModel * model;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;


@end
