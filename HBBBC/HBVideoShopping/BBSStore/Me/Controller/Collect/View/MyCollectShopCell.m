//
//  MyCollectShopCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyCollectShopCell.h"

@implementation MyCollectShopCell
- (void)setModel:(HBCollectShopListModel *)model{
    _model = model;
    self.shopNameLab.text = [NSString stringWithFormat:@"%@",model.shop_name];
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.shop_logo] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
