//
//  MyCollectGoodsCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyCollectGoodsCell.h"

@implementation MyCollectGoodsCell
- (void)setModel:(HBCollectListModel *)model{
    _model = model;
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.goods_name];
    self.moneyLab.text = [NSString stringWithFormat:@"￥%@",model.goods_price];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.image_default_id] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
