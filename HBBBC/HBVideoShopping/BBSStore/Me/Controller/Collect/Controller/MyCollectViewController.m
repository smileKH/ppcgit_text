//
//  MyCollectViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyCollectViewController.h"
#import "MyCollectShopCell.h"
#import "MyCollectGoodsCell.h"
#import "HBCollectModel.h"
#import "HBCollectShopModel.h"
@interface MyCollectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, assign) NSInteger pageState;//0=goods 1=shop
@property (nonatomic, strong) HBCollectModel * model;
@property (nonatomic, strong) HBCollectShopModel * shopModel;
@end

@implementation MyCollectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"我的收藏";
    [self setNavBar];
    [self setupTableview];
    
    [self requestMyCollectData];
}
#pragma mark ==========请求我的商品收藏==========
-(void)requestMyCollectData{
    self.pageState = 0;
    [LBService post:GOODS_FAVORITE_LIST_ITEM params:@{} completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            self.model = [HBCollectModel mj_objectWithKeyValues:response.result[@"data"]];
            
            [self.tableview reloadData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========请求我的店铺收藏==========
-(void)requestMyShopData{
    self.pageState = 1;
    [LBService post:GOODS_FAVORITE_LIST_SHOP params:@{} completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            self.shopModel = [HBCollectShopModel mj_objectWithKeyValues:response.result[@"data"]];
            
            [self.tableview reloadData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
    
    UISegmentedControl *seg = [[UISegmentedControl alloc] initWithItems:@[@"商品",@"店铺"]];
    seg.bounds = CGRectMake(0, 0, 141, 28);
    seg.tintColor = MAINBLACKCOLOR;
    seg.selectedSegmentIndex = 0;
    [seg addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = seg;
}

-(void)setupTableview{
    
    [self.tableview registerNib:[UINib nibWithNibName:@"MyCollectShopCell" bundle:nil] forCellReuseIdentifier:@"shop"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"MyCollectGoodsCell" bundle:nil] forCellReuseIdentifier:@"goods"];
    
    self.tableview.tableFooterView = [[UIView alloc]init];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)change:(UISegmentedControl *)seg
{
    self.pageState = seg.selectedSegmentIndex;
    if (self.pageState==0) {
        //商品
        [self requestMyCollectData];
    }else{
        [self requestMyShopData];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (self.pageState==0) {
        return self.model.list.count;
    }else{
        return self.shopModel.list.count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.pageState==0) {
        MyCollectGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goods" forIndexPath:indexPath];
        cell.model = self.model.list[indexPath.row];
        // Configure the cell...
        
        return cell;
    }else{
        MyCollectShopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shop" forIndexPath:indexPath];
        
        // Configure the cell...
        cell.model = self.shopModel.list[indexPath.row];
        return cell;
    }
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.pageState==0) {
         HBCollectListModel *goodsModel =  self.model.list[indexPath.row];
        ProductFirstViewController *vc = [[ProductFirstViewController alloc]initWithNibName:@"ProductFirstViewController" bundle:nil];
        vc.item_id = goodsModel.item_id;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        HBCollectShopListModel *shopModel =  self.shopModel.list[indexPath.row];
        ShopViewController *vc = [[ShopViewController alloc]initWithNibName:@"ShopViewController" bundle:nil];
        vc.shop_id = shopModel.shop_id;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSString *message = nil;
        if (self.pageState==0) {
            //商品
            message = @"确定要删除该收藏商品?删除后无法恢复!";
        }else{
            //店铺
            message = @"确定要删除该收藏店铺?删除后无法恢复!";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:1];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //这里要调接口删除商品
            [self callbackInterfaceDeleteItems:indexPath didTableView:tableView];
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

#pragma mark 调接口删除商品
-(void)callbackInterfaceDeleteItems:(NSIndexPath *)indexPath didTableView:(UITableView *)tableView{
    //先请求服务器
    WEAKSELF;
    if (self.pageState==0) {
        HBCollectListModel *goodsModel =  self.model.list[indexPath.row];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"item_id"] = goodsModel.item_id;
        [MBProgressHUD showHUDisWindiw:NO];
        [LBService post:GOODS_FAVORITE_ITEM_REMOVE params:parameter completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //改变数组状态 更新数据
                [app showToastView:@"删除成功"];
                [weakSelf requestMyCollectData];
            }
            else{
                //发生错误
                [app showToastView:response.message];
            }
            
        }];
    }else{
        HBCollectShopListModel *shopModel =  self.shopModel.list[indexPath.row];
        NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
        parameter[@"shop_id"] = shopModel.shop_id;
        [MBProgressHUD showHUDisWindiw:NO];
        [LBService post:GOODS_FAVORITE_SHOP_REMOVE params:parameter completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //改变数组状态 更新数据
                [app showToastView:@"删除成功"];
                [weakSelf requestMyShopData];
            }
            else{
                //发生错误
                [app showToastView:response.message];
            }
            
        }];
    }
    
    
    
}
#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
