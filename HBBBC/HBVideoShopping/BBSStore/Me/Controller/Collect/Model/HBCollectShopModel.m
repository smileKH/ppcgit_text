//
//  HBCollectShopModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCollectShopModel.h"

@implementation HBCollectShopModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"list":@"HBCollectShopListModel"};
}
@end

@implementation HBCollectShopListModel

@end
