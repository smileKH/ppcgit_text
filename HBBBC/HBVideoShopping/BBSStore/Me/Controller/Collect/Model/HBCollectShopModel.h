//
//  HBCollectShopModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBCollectModel.h"
@interface HBCollectShopModel : NSObject
@property (nonatomic, strong) HBPagers * pagers;
@property (nonatomic, strong) NSArray * list;
@end

@interface HBCollectShopListModel : NSObject
@property (nonatomic, strong) NSString * shop_logo;
@property (nonatomic, strong) NSString * snotify_id;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * shop_name;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * shop_id;
@end
