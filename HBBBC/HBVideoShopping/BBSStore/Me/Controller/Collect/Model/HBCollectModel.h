//
//  HBCollectModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBPagers;
@class HBCollectListModel;
@interface HBCollectModel : NSObject
@property (nonatomic, strong) Cur_symbol * cur_symbol;
@property (nonatomic, strong) HBPagers * pagers;
@property (nonatomic, strong) NSArray * list;
@end

@interface HBPagers : NSObject
@property (nonatomic, strong) NSString * total;
@end

@interface HBCollectListModel : NSObject
@property (nonatomic, strong) NSString * goods_name;
@property (nonatomic, strong) NSString * cellphone;
@property (nonatomic, strong) NSString * cat_id;
@property (nonatomic, strong) NSString * send_time;
@property (nonatomic, strong) NSString * item_id;
@property (nonatomic, strong) NSString * disabled;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * image_default_id;
@property (nonatomic, strong) NSString * object_type;
@property (nonatomic, strong) NSString * remark;
@property (nonatomic, strong) NSString * gnotify_id;
@property (nonatomic, strong) NSString * goods_price;
@property (nonatomic, strong) NSString * create_time;
@property (nonatomic, strong) NSString * email;
@end
