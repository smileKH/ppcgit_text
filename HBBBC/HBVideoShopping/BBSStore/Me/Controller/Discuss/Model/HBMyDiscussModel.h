//
//  HBMyDiscussModel.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/3/31.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Pagers;
@interface HBMyDiscussModel : NSObject
@property (nonatomic ,strong)Pagers *pagers;
@property (nonatomic ,strong)NSArray *list;
@end


@interface HBMyDiscussListModel : NSObject
@property (nonatomic ,strong)NSString *reply_content;
@property (nonatomic ,strong)NSString *appeal_again;
@property (nonatomic ,strong)NSString *tid;
@property (nonatomic ,strong)NSString *appeal_time;
@property (nonatomic ,strong)NSString *modified_time;
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *anony;
@property (nonatomic ,strong)NSString *oid;
@property (nonatomic ,strong)NSString *rate_pic;
@property (nonatomic ,strong)NSString *reply_time;
@property (nonatomic ,strong)NSString *rate_id;
@property (nonatomic ,strong)NSString *is_reply;

@property (nonatomic ,strong)NSString *appeal_status;
@property (nonatomic ,strong)NSString *is_check_append;
@property (nonatomic ,strong)NSString *spec_nature_info;
@property (nonatomic ,strong)NSString *is_appeal;
@property (nonatomic ,strong)NSString *append;
@property (nonatomic ,strong)NSString *trade_end_time;
@property (nonatomic ,strong)NSString *is_lock;
@property (nonatomic ,strong)NSString *item_title;

@property (nonatomic ,strong)NSString *item_id;
@property (nonatomic ,strong)NSString *item_price;
@property (nonatomic ,strong)NSString *item_pic;
@property (nonatomic ,strong)NSString *role;
@property (nonatomic ,strong)NSString *result;
@property (nonatomic ,strong)NSString *is_append;
@property (nonatomic ,strong)NSNumber *created_time;
@property (nonatomic ,strong)NSString *disabled;
@property (nonatomic ,strong)NSString *content;
@property (nonatomic ,strong)NSString *shop_id;
@end
