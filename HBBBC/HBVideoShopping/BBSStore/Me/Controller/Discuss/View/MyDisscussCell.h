//
//  MyDisscussCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBMyDiscussModel.h"
@interface MyDisscussCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic ,strong)HBMyDiscussListModel *model;
@property (weak, nonatomic) IBOutlet UILabel *textLab;
@property (weak, nonatomic) IBOutlet UIImageView *pinJiaImgView;
@property (weak, nonatomic) IBOutlet UILabel *pingJiaLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;

@end
