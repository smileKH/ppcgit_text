//
//  MyDisscussCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/21.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "MyDisscussCell.h"

@implementation MyDisscussCell
- (void)setModel:(HBMyDiscussListModel *)model{
    _model = model;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.item_pic] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    self.textLab.text = [NSString stringWithFormat:@"%@",model.item_title];
    self.detailLab.text = [NSString stringWithFormat:@"%@",model.content];
    self.dateLab.text = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:model.created_time];
    if ([model.result isEqualToString:@"good"]) {
        //好评
        self.pingJiaLab.text = @"好评";
        self.pinJiaImgView.image = [UIImage imageNamed:@"bbs_evaluationHao"];
    }else if ([model.result isEqualToString:@"neutral"]){
        //中评
        self.pingJiaLab.text = @"中评";
        self.pinJiaImgView.image = [UIImage imageNamed:@"bbs_evaluationZhong"];
    }else if ([model.result isEqualToString:@"bad"]){
        //差评
        self.pingJiaLab.text = @"差评";
        self.pinJiaImgView.image = [UIImage imageNamed:@"bbs_evaluationCha"];
    }
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
