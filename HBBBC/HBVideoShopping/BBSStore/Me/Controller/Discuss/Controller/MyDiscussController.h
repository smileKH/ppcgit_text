//
//  MyDiscussController.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/11.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

@interface MyDiscussController : HBRootViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
