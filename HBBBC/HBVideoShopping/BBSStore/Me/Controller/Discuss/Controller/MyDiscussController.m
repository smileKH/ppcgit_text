//
//  MyDiscussController.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/11.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "MyDiscussController.h"
#import "MyDisscussCell.h"
#import "HBMyDiscussModel.h"
@interface MyDiscussController ()
//@property (nonatomic ,strong)HBMyDiscussModel *model;
@end

@implementation MyDiscussController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的评价";
    self.tableView.estimatedRowHeight = 200;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MyDisscussCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [self setNavBar];
    //请求数据
    [self requestDiscussList:YES];
    
    WEAKSELF;
    //默认block方法：设置下拉刷新
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [weakSelf requestDiscussList:YES];
//    }];
//
//    //默认block方法：设置上拉加载更多
//    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        [weakSelf requestDiscussList:NO];
//    }];
}

#pragma mark ==========退换货列表==========
-(void)requestDiscussList:(BOOL)isRefresh{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"page_size"] = @(self.pageSize);
    parameter[@"page_no"] = @(self.pageNo);
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:MEMBER_RATE_LIST params:parameter completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            //第一页 下拉刷新
            if (weakSelf.pageNo == 1) {
                [weakSelf.allData removeAllObjects];
            }
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"total"]intValue];
                NSArray *arr = [dic objectForKey:@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [HBMyDiscussListModel mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            }
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptNormal];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView.mj_header endRefreshing];
    }];
}
- (void)setNavBar
{
    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = @[item1];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyDisscussCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.allData[indexPath.row];
    // Configure the cell...
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
