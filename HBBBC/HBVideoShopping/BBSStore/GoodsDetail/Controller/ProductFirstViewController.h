//
//  ProductFirstViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductFirstViewController : HBRootViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSArray *tablearray;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
- (IBAction)selectDelect:(UIButton *)sender;
@property (nonatomic, strong)  NSString *item_id;
@property (nonatomic, strong)  NSString *fields;
- (IBAction)addgoodsAction:(UIButton *)sender;
- (IBAction)buyAction:(UIButton *)sender;
- (IBAction)connectAction:(UIButton *)sender;
- (IBAction)collectAction:(UIButton *)sender;
- (IBAction)toshoppingCarAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *itemCollectBtn;

@property (weak, nonatomic) IBOutlet UIButton *daohuoBtn;

- (IBAction)clickDaoHuoBtn:(UIButton *)sender;

@end


