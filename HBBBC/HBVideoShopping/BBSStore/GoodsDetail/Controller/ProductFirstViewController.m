//
//  ProductFirstViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ProductFirstViewController.h"
#import "ProductHeaderCell.h"
#import "ProductImagesCell.h"
#import "ProductSecondCell.h"
#import "ProductNaturePropsCell.h"
#import "ProductAppraiseCell.h"
#import "HBPromotionTagCell.h"
#import "ProductAppraiseHeaderCell.h"
#import "GoodsSelectDetailViewController.h"
#import "ProductDetailModel.h"
#import "ShopViewController.h"
#import "GoodsAppraiseModel.h"
#import "PopoverView.h"
#import "PopoverAction.h"

#import "ProductDescModel.h"
#import "LoginViewController.h"
#import "Cart_add.h"

#import "ShoppingCarViewController.h"
#import "OrderPayViewController.h"
#import "HBB_DataManage.h"
#import "Masonry.h"
#import "HBGoodsServiceVC.h"
#import "HBCollectStateModel.h"
#import "HBProductSharePushView.h"
@interface ProductFirstViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>
{
    UIButton *navigationBtn;
    NSInteger page_no;
    NSString *rate_type;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollow;
@property(nonatomic, strong) UITableView *ftableview;
@property(nonatomic, strong) UITableView *stableview;
@property(nonatomic, strong) UITableView *ttableview;
@property (nonatomic, strong)UIView *line;
@property (nonatomic, strong) ProductDetailModel *productModel;//产品
@property (nonatomic, strong) ProductDescModel *descModel;//描述
@property (nonatomic, strong) NSMutableArray *appraiseArray;
@property (nonatomic, strong) NSString * rate_isJugle;
@property (nonatomic, strong) UIView *topview;
@property (nonatomic, strong) NSMutableArray *topviewBtns;
@property (nonatomic, strong) Cart_add *addgoodsModel;//选中的货品

//促销展开控制
@property (nonatomic, assign) BOOL  sectionOpen;
@property (nonatomic, strong) HBB_DataManage * fmdbManage;
//2个并行请求成功开关
@property (nonatomic, assign) BOOL loadDiscussListSuccess;
@property (nonatomic, assign) BOOL loadGoodDescSuccess;
@property (assign, nonatomic) float haseScrollOffsizeY;
@property (nonatomic, assign) int currtenPagIndex;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, assign) float webViewHeight;
/**
 *  是否上滑动
 */
@property (assign, nonatomic) BOOL isTopScrollow;
@property (nonatomic ,strong)HBCollectStateModel *collectModel;
/**
 *  总评论数
 */
@property (nonatomic, strong) NSString * pingJiaNumb;
@property (nonatomic, strong) NSString * haoPingLvStr;
/**
 *  加入购物车状态
 */
@property (nonatomic, assign) BOOL   isRealStore;
//分享
@property (nonatomic, strong) HBProductSharePushView * pushView;
@end

@implementation ProductFirstViewController
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ProductHeaderCell" object:nil];
}
#pragma mark --- 获取商品详情
- (void)getDetails
{

    WEAKSELF;
    NSDictionary *dict = @{@"item_id":self.item_id,@"fields":@""};
    
    [LBService post:GOODS_ITEM_DETAIL params:dict completion:^(LBResponse *response) {
        
        if (response.succeed) {
            //
            weakSelf.productModel = [ProductDetailModel mj_objectWithKeyValues:response.result[@"data"]];
            //获取商品描述
            [weakSelf loadDesc];
            //设置加入购物车状态
            [self setAddCartButtonState];
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========设置购物车按钮状态==========
-(void)setAddCartButtonState{
    if ([self.productModel.item.realStore doubleValue]>0) {
        //有货
        self.isRealStore = NO;
        self.daohuoBtn.hidden = YES;
    }else{
        //无货
        self.isRealStore = YES;
        self.daohuoBtn.hidden = NO;
    }
}
#pragma mark ------ 收藏店铺
-(void)collectionShop:(Shop *)shop canCollectShop:(BOOL)canCollectShop
{
    
    if (canCollectShop) {
        NSString * shop_id = shop.shop_id;
        NSString * accessToken = LocalValue(TOKEN_XY_APP);
        
        [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        [LBService post:GOODS_FAVORITE_SHOP_ADD params:@{@"shop_id":shop_id,@"accessToken":accessToken} completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //
                [app showToastView:@"收藏成功"];
            }else{
                [app showToastView:response.message];
            }
        }];
    }else{//取消
        NSString * shop_id = shop.shop_id;
        NSString * accessToken = LocalValue(TOKEN_XY_APP);
        
        [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        [LBService post:GOODS_FAVORITE_SHOP_REMOVE params:@{@"shop_id":shop_id,@"accessToken":accessToken} completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //
                [app showToastView:@"取消成功"];
            }else{
                [app showToastView:response.message];
            }
        }];
    }
   

}


#pragma mark -------  获取评论列表
- (void)discussList:(BOOL)isFirst
{
//    rate_type //晒图。类型值为0：全部，1：好评，2：中评，3：差评，4：晒图
    WEAKSELF;
    NSDictionary *dicts = @{@"item_id":self.item_id,@"":integerString(page_no),@"page_size":@"10",@"rate_type":rate_type};
    [LBService post:GOODS_RATE_LIST params:dicts completion:^(LBResponse *response) {
        if (response.succeed) {
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                weakSelf.pingJiaNumb = dic[@"pagers"][@"total"];
                weakSelf.haoPingLvStr = dic[@"pagers"][@"count"];
                NSArray *loadArray = [GoodsAppraiseModel mj_objectArrayWithKeyValuesArray:dic[@"list"]];
                weakSelf.appraiseArray = loadArray.mutableCopy;
                if (isFirst) {
                    weakSelf.loadDiscussListSuccess = YES;
                    [weakSelf loadAllDataSuccess];
                }else{
                    [weakSelf.ttableview reloadData];
                }
            }else{
                [app showToastView:response.message];
            }
            }
           
    }];
}
#pragma mark ==========获取商品描述==========
- (void)loadDesc{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:GOODS_DESC_ITEM params:@{@"item_id":self.item_id} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.descModel = [ProductDescModel mj_objectWithKeyValues:response.result[@"data"]];
            if ([userManager oneceJudgeLoginState]) {
                //已经登录 //获取收藏状态
                [weakSelf requestCollect];
            }
            weakSelf.loadGoodDescSuccess = YES;
            [weakSelf loadWebHeight];
            [weakSelf loadAllDataSuccess];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark --🍎--🙏-- 设置请求收藏状态
-(void)requestCollect{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"shop_id"] = self.productModel.shop.shop_id;
    parameters[@"item_id"] = self.item_id;
    [LBService post:MEMBER_SHOP_ISCOLLECT params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.collectModel = [HBCollectStateModel mj_objectWithKeyValues:response.result[@"data"]];
            //刷新界面
            [weakSelf updateUIData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark------------刷新界面-------
-(void)updateUIData{
    [self.ftableview reloadData];
    if ([self.collectModel.is_item isEqualToString:@"1"]) {
        self.itemCollectBtn.selected = YES;
    }else{
        self.itemCollectBtn.selected = NO;
    }
}
-(UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 1)];
        _webView.scalesPageToFit = YES;
        _webView.scrollView.bounces = NO;
        _webView.scrollView.showsVerticalScrollIndicator = NO;
        _webView.scrollView.showsHorizontalScrollIndicator = NO;
        _webView.delegate = self;
        _webView.hidden = YES;
        [self.view insertSubview:_webView atIndex:0];
    }
    return _webView;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    float height = webView.scrollView.contentSize.height;
    _webViewHeight = height + 10;
    self.descModel.catchCellHeight = _webViewHeight;
    [_webView removeFromSuperview];
    if (height>1) {
        _webView.height = height;
         [self.ftableview reloadData];
    }
}
-(void)loadWebHeight{

    [self.webView loadHTMLString:self.descModel.wap_desc baseURL:nil];
}
/**
 第一次加载完所有数据
 */
-(void)loadAllDataSuccess{
    if (self.loadGoodDescSuccess &&self.loadDiscussListSuccess) {
        [self.ftableview reloadData];
        [self.stableview reloadData];
        [self.ttableview reloadData];
        if (self.ftableview.contentSize.height<self.ftableview.height) {
            self.ftableview.contentSize = CGSizeMake(0, self.ftableview.height);
        }
        if (self.stableview.contentSize.height<self.stableview.height) {
            self.stableview.contentSize = CGSizeMake(0, self.stableview.height);
        }
        if (self.ttableview.contentSize.height<self.ttableview.height) {
            self.ttableview.contentSize = CGSizeMake(0, self.ttableview.height);
        }
    }
}

#pragma mark ---- 加入购物车 或者立即购买
- (void)addcargoods
{
    
}

-(UIView *)line
{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = MAINBLACKCOLOR;
        _line.bounds = CGRectMake(0, 0, 40, 3);
    }
    return _line;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //商品详情
//    UISQURE
    rate_type = @"0";
    self.rate_isJugle = @"0";
    [self setUIScrollow];
    [self loadUIDetail];
    [self setNavbar];
    [self getDetails];
    [self discussList:YES];
   
    if ([userManager oneceJudgeLoginState]) {
        //已经登录
    }else{
        self.fmdbManage = [HBB_DataManage sharedHBB_DataManage];
    }
    //初始化
    self.daohuoBtn.hidden = YES;
}
#pragma mark --🍎--🙏-- 设置scrollowView
-(void)setUIScrollow{
    
    self.scrollow.delegate = self;
    
    self.scrollow.contentSize = CGSizeMake(SCREEN_WIDTH*3, 0);
    [self addTableViews];
    
}
#pragma mark --🍎--🙏-- 添加tableViews
-(void)addTableViews{
    float navBarHeight  = [[UIApplication sharedApplication] statusBarFrame].size.height + 44;
    float bottomSafeHeight = ([[UIScreen mainScreen] bounds].size.height == 812)?34:0;
    
    
    float height = SCREEN_HEIGHT -navBarHeight - 44 -44 - bottomSafeHeight;
    for (int i =0; i<3; i++) {
        UITableView *tb = [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*i, 0, SCREEN_WIDTH, height) style:UITableViewStyleGrouped];
        tb.estimatedRowHeight = 200;
        tb.tag = 100+i;
        tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        tb.delegate = self;
        tb.dataSource = self;
        if (i==0) {
            self.ftableview = tb;
        }else if (i==1){
            self.stableview = tb;
        }else if (i==2){
            self.ttableview = tb;
        }
        [self.scrollow addSubview:tb];
        
    }
}

- (void)setNavbar
{
    _currtenPagIndex = 0;
    _topviewBtns = [NSMutableArray array];
    _topview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 205, 44)];
    NSArray *array =@[@"商品",@"详情",@"评价"];
    [_topview addSubview:self.line];
    for (NSInteger i = 0; i < array.count; i++) {
        UIButton *topbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [topbtn setTitle:array[i] forState:UIControlStateNormal];
        [topbtn setTitleColor:MAINBLACKCOLOR forState:UIControlStateNormal];
        topbtn.tag = i + 1;
        topbtn.titleLabel.font = [UIFont systemFontOfSize:14];
        topbtn.frame = CGRectMake(i *75, 10, 75, 25);
        [topbtn addTarget: self action:@selector(selectTopAction:) forControlEvents:UIControlEventTouchUpInside];
        [_topview addSubview:topbtn];
        if (!i) {
            self.line.center = CGPointMake(topbtn.center.x, 40);
        }
        [_topviewBtns addObject:topbtn];
    }
    self.navigationItem.titleView = _topview;
    navigationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [navigationBtn setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    navigationBtn.bounds  = CGRectMake(0, 0, 30, 30);
    [navigationBtn setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
    [navigationBtn addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
    navigationBtn.titleLabel.font= [UIFont systemFontOfSize:14];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:navigationBtn];
    
}

-(void)scrollToindex:(NSInteger)index{
    if (_currtenPagIndex == index) {
        return;
    }
    [self.scrollow  setContentOffset:CGPointMake(index * SCREEN_WIDTH, 0) animated:YES];
    _currtenPagIndex = (int)index;
    UIButton*sender = _topviewBtns[index];
    [UIView animateWithDuration:0.2 animations:^{
        self.line.center = CGPointMake(sender.center.x, 40);
        
    }];
}

- (void)selectTopAction:(UIButton *)sender

{
    
    [self scrollToindex:sender.tag - 1];
    
}
#pragma mark ==========点击更多菜单==========
- (void)rightAction:(UIButton *)sender
{
    PopoverAction *action1 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l1") title:@"    首页  " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 0;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    PopoverAction *action2 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l2") title:@"    分类    " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 1;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    PopoverAction *action3 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l3") title:@"    购物车    " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 2;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    PopoverAction *action4 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l4") title:@"    会员  " handler:^(PopoverAction *action) {
        
        self.tabBarController.selectedIndex = 3;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    WEAKSELF;
    PopoverAction *action5 = [PopoverAction actionWithImage:ImageNamed(@"bbs_l5") title:@"    分享  " handler:^(PopoverAction *action) {
        //分享
        [weakSelf shareProduct];
        
    }];
    PopoverView *popoverView = [PopoverView popoverView];
    popoverView.style = PopoverViewStyleDark;
    popoverView.arrowStyle = PopoverViewArrowStyleTriangle;
    [popoverView showToView:sender withActions:@[action1,action2,action3,action4,action5]];
    
}
-(void)shareProduct{
    WEAKSELF;
    self.pushView = [[HBProductSharePushView alloc]init];
    self.pushView.clickShareItemsView = ^(NSInteger index) {
        //点击了分享
        if (index==100) {
            //微信
            [weakSelf shareWebPageToPlatformType:JSHAREPlatformWechatSession];
        }else if (index==101){
            //朋友圈
            [weakSelf shareWebPageToPlatformType:JSHAREPlatformWechatTimeLine];
        }else if (index==102){
            //QQ
            [weakSelf shareWebPageToPlatformType:JSHAREPlatformQQ];
        }else if (index==103){
            //QQ空间
            [weakSelf shareWebPageToPlatformType:JSHAREPlatformQzone];
        }
    };
    [self.pushView show];
}
#pragma mark ==========开始分享==========
- (void)shareWebPageToPlatformType:(JSHAREPlatform)platform
{
    [app shareLinkWithPlatform:platform title:@"天宫" andShareText:@"欢迎使用天宫" andSingleImage:self.productModel.share.image andLineUrl:self.productModel.share.h5href];
}

- (void)loadUIDetail
{
    self.bottomView.layer.borderColor = UIColorFromHex(0xcccccc).CGColor;
    self.bottomView.layer.borderWidth = 0.5;
}


#pragma mark ==========tableviewDelegate==========
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //原来是6个 现在7个
    if (tableView == self.ftableview) {
        return 4;
    }else if (tableView == self.stableview) {
        return 1;
    }else if (tableView == self.ttableview) {
        return 2;
    }else{
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.ftableview) {
        if (section == 1){//测试促销活动
            if (self.sectionOpen) {
                return self.productModel.promotionTag.fulldiscount.count;
            }else{
                return 0;
            }
            
        }else if (section == 4) {
            return  [self.productModel.item.natureProps count];
        }
        return 1;
    }else if (tableView == self.stableview) {
        return self.productModel.item.natureProps.count;;
    }else if (tableView == self.ttableview) {
        if (section == 1) {
            return self.appraiseArray.count;
        }
        return 1;
    }else{
        return 0;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF;
    if (tableView == self.ftableview) {
        if (indexPath.section == 0) {//头视图
            ProductHeaderCell *cell = (ProductHeaderCell*)[self cellFromTableView:tableView rid:@"ProductHeaderCell"];
            
            cell.model = self.productModel.item;
            cell.prodModel = self.productModel;
            cell.clickProductHeaderShare = ^(id obj) {
                //分享
                [weakSelf shareProduct];
            };
            return cell;
        }
        else if (indexPath.section == 1){//促销视图
            HBPromotionTagCell *cell = (HBPromotionTagCell*)[self cellFromTableView:tableView rid:@"HBPromotionTagCell"];
            cell.model = self.productModel.promotionTag.fulldiscount[indexPath.row];
            return cell;
        }else if (indexPath.section == 2){//店铺头视图
            
            ProductSecondCell *cell = (ProductSecondCell*)[self cellFromTableView:tableView rid:@"ProductSecondCell"];
            cell.collModel = self.collectModel;
            cell.model = self.productModel.shop;
            cell.toShop = ^(Shop *shop) {
                ShopViewController *vc = [[ShopViewController alloc] initWithNibName:@"ShopViewController" bundle:nil];
                vc.shop_id = self.productModel.shop.shop_id;
                [self.navigationController pushViewController:vc animated:YES];
            };
            
            cell.collectShop = ^(Shop *shop,BOOL canCollectShop) {
                
                [self collectionShop:shop canCollectShop:canCollectShop];
            };
            return cell;
        }else if (indexPath.section == 3){//图片显示视图
            ProductImagesCell *cell = (ProductImagesCell*)[self cellFromTableView:tableView rid:@"ProductImagesCell"];
            cell.contentWebView = self.webView;
            return cell;
        }
    }else if (tableView == self.stableview){
        ProductNaturePropsCell *cell = (ProductNaturePropsCell*)[self cellFromTableView:tableView rid:@"ProductNaturePropsCell"];
        cell.props = self.productModel.item.natureProps[indexPath.row];
        return cell;
    }else if (tableView == self.ttableview){
        if (!indexPath.section ){//评论显示视图
            ProductAppraiseHeaderCell *cell = (ProductAppraiseHeaderCell*)[self cellFromTableView:tableView rid:@"ProductAppraiseHeaderCell"];
            cell.state = rate_type;
            cell.pingStr = self.pingJiaNumb;
            cell.haoPingStr = self.haoPingLvStr;
            
            WEAKSELF;
            cell.selectState = ^(NSString *sta) {
                if (sta.length <=0) {
                    sta = @"0";
                }
                weakSelf.rate_isJugle = @"1";
                rate_type = sta;
                [weakSelf discussList:NO];
            };
            return cell;
        }else{//评论显示列表
            ProductAppraiseCell *cell = (ProductAppraiseCell*)[self cellFromTableView:tableView rid:@"ProductAppraiseCell"];
            cell.model = self.appraiseArray[indexPath.row];
            return cell;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.ftableview) {
        if (!indexPath.section) {
            if (self.productModel.item.catchCellHeight>0) {
                return self.productModel.item.catchCellHeight;
            }
        }else if (indexPath.section==1) {//促销
            HBPromotionListModel *model = self.productModel.promotionTag.fulldiscount[indexPath.row];
            if (model.catchCellHeight>0) {
                return model.catchCellHeight;
            }
        }else if (indexPath.section==2) {
            
            if (self.productModel.shop.catchCellHeight>0) {
                return self.productModel.shop.catchCellHeight;
            }
        }else if (indexPath.section==3) {
            
            if (self.descModel.catchCellHeight>0) {
                return self.descModel.catchCellHeight;
            }else{
                if (self.webViewHeight>0) {
                    return self.webViewHeight + 15 +10;
                }
            }
        }
    }else if (tableView == self.stableview) {
        NatureProps *model = self.productModel.item.natureProps[indexPath.row];
        if (model.catchCellHeight>0) {
            return model.catchCellHeight;
        }
    }else if (tableView == self.ttableview) {
        if (!indexPath.section) {
            // 评论头固定高度
            return 99;
        }else if (indexPath.section) {
            GoodsAppraiseModel * model = self.appraiseArray[indexPath.row];
            if (model.catchCellHeight>0) {
                return model.catchCellHeight;
            }
        }
    }
    return UITableViewAutomaticDimension;
}
#pragma mark ==========点击促销头视图==========
-(void)clicHeadBtn:(UIButton *)button{
    //更新第一 section
    self.sectionOpen = !self.sectionOpen;
    [self.ftableview reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark --🍎--🙏-- sectionHeader
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.ftableview) {
        if (section==1) {
            if ([HBHuTool judgeArrayIsEmpty:self.productModel.promotionTag.fulldiscount]) {
                UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
                headerView.backgroundColor = UIColorFromHex(0xf2f2f2);
                
                //满减
                CGFloat offX = 20;
                CGFloat offY = 10;
                CGFloat width = 40;
                CGFloat height = 20;
                CGFloat spaing = 10;
                UILabel *cuLab = [[UILabel alloc]initWithFrame:CGRectMake(offX, offY, width, height)];
                cuLab.text = @"促销";
                cuLab.font = Font(12);
                cuLab.textColor = UIColorFromHex(0x999999);
                
                for (int i=0; i<self.productModel.promotionTag.fulldiscount.count; i++) {
                    UILabel *jianLab = [[UILabel alloc]initWithFrame:CGRectMake(offX+width+width*i+spaing+spaing*i, offY, width, height)];
                    HBPromotionListModel *model = self.productModel.promotionTag.fulldiscount[i];
                    jianLab.text = model.promotion_tag;
                    jianLab.layer.borderColor = UIColor.redColor.CGColor;
                    jianLab.layer.borderWidth = 1;
                    jianLab.textColor = red_Color;
                    jianLab.font = Font(12);
                    jianLab.layer.cornerRadius = 3;
                    jianLab.textAlignment = NSTextAlignmentCenter;
                    
                    [headerView addSubview:jianLab];
                }
                
                
                
//                UILabel *zheLab = [[UILabel alloc]initWithFrame:CGRectMake(offX+2*width+2*spaing, offY, width, height)];
//                zheLab.text = @"满折";
//                zheLab.layer.borderColor = UIColor.redColor.CGColor;
//                zheLab.layer.borderWidth = 1;
//                zheLab.textColor = red_Color;
//                zheLab.font = Font(12);
//                zheLab.layer.cornerRadius = 3;
//                zheLab.textAlignment = NSTextAlignmentCenter;
                
                UIButton *headBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                [headBtn addTarget:self action:@selector(clicHeadBtn:) forControlEvents:UIControlEventTouchUpInside];
                headBtn.frame = headerView.frame;
                
                [headerView addSubview:cuLab];
                
//                [headerView addSubview:zheLab];
                [headerView addSubview:headBtn];
                return headerView;
            }
        }
    }else if (tableView == self.stableview ){
        
           UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
            label.text = @"基本参数";
            label.font = [UIFont systemFontOfSize:14];
            label.backgroundColor = UIColorFromHex(0xf2f2f2);
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = UIColorFromHex(0x999999);
            return label;
    }
    return [self naneSpaceView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.ftableview ){
        if (section == 1) {//促销
            if ([HBHuTool judgeArrayIsEmpty:self.productModel.promotionTag.fulldiscount]) {
                //显示促销
                return 40;
            }
        }
    }else if (tableView == self.stableview ){
        return 40;
    }
    
    return 0.01;
}
#pragma mark --🍎--🙏-- sectionFooter
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (tableView == self.ftableview) {
        if (section == 3) {
             return [self sepSpaceView];
        }
    }
    return [self naneSpaceView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView == self.ftableview) {
        if (section == 3) {
            return 10;
        }
    }
    return 0.01;
}
-(UIView*)naneSpaceView{
    UIView * naneSpaceView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 0.01)];
    
    return naneSpaceView;
}
-(UIView*)sepSpaceView{
    UIView * sepSpaceView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5)];
    sepSpaceView.backgroundColor = MAINBGCOLOR;
    return sepSpaceView;
}
#pragma mark --🍎--🙏-- 滚动视图

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.scrollow) {
        
        float offsizeX=scrollView.contentOffset.x;
        
        int page = (int)(offsizeX / SCREEN_WIDTH);
        NSLog(@"end  %d  %.2f",page,offsizeX);
        [self scrollToindex:page];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
   
    float height = scrollView.bounds.size.height;
    float contentHeight = scrollView.contentSize.height;
    if (contentHeight<height) {
        contentHeight  = height;
    }
    
    float offsizey=scrollView.contentOffset.y;
    
    if (offsizey<-40) {
        if (scrollView == self.ttableview) {
            [self scrollToindex:1];
        }else if (scrollView == self.stableview) {
            [self scrollToindex:0];
        }
        
    }else if (offsizey >(contentHeight+40 - height)){
        if (scrollView == self.ftableview) {
            [self scrollToindex:1];
        }else if (scrollView == self.stableview) {
            [self scrollToindex:2];
        }
    }
}

#pragma mark ==========点击选择规格==========
- (IBAction)selectDelect:(UIButton *)sender {
   
    //弹出规格 1选择规格  2加购物车  3立即购买
    [self pushSelectGoodsView:@"1"];
    
}
#pragma mark ==========弹出规格界面==========
-(void)pushSelectGoodsView:(NSString *)type{
    
    if ([self.productModel.item.nospec isEqualToString:@"1"]) {
        //单一商品
        //创建实例
        self.addgoodsModel = [Cart_add new];
        self.addgoodsModel.quantity = @"1";
        self.addgoodsModel.obj_type = @"item";
        self.addgoodsModel.sku_id = self.productModel.item.sku.sku_id;
        self.addgoodsModel.shopId = self.productModel.shop.shop_id;
        self.addgoodsModel.goodsId = self.productModel.item.item_id;
        self.addgoodsModel.goodsName = self.productModel.item.sub_title;
        self.addgoodsModel.shopName = self.productModel.shop.shop_name;
        self.addgoodsModel.image_default_id = self.productModel.item.image_default_id;
        self.addgoodsModel.money = self.productModel.item.cost_price;
        //此处获取到确定购买的商品
        if ([type isEqualToString:@"1"]) {
            //规格 1选择规格 什么也不做
            [app showToastView:@"该商品为单一规格商品"];
            
        }else if ([type isEqualToString:@"2"]){
            //2加购物车 加入购物车
            [self addShoppingCart];
            
        }else if ([type isEqualToString:@"3"]){
            //3立即购买 立即购买
            [self buyGoodsAndSelectGoods];
        }
        
    }else{//多规格商品
        GoodsSelectDetailViewController *vc = [[GoodsSelectDetailViewController alloc] initWithNibName:@"GoodsSelectDetailViewController" bundle:nil];
        
        vc.model = self.productModel;
        vc.type = type;
        vc.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        if ([self.productModel.item.realStore integerValue]>0) {
            //有货
            vc.daoBtn.hidden = YES;
            
        }else{
            //无货
            vc.daoBtn.hidden = NO;
            
        }
        vc.confirmGoods = ^(Cart_add *caraddmodel,NSString *type) {
            self.addgoodsModel = caraddmodel;
            //此处获取到确定购买的商品
            if ([type isEqualToString:@"1"]) {
                //规格 1选择规格 什么也不做
                
            }else if ([type isEqualToString:@"2"]){
                //2加购物车 加入购物车
                [self addShoppingCart];
                
            }else if ([type isEqualToString:@"3"]){
                //3立即购买 立即购买
                [self buyGoodsAndSelectGoods];
            }
            
        };
        [self.view.window addSubview:vc.view];
        [self addChildViewController:vc];
    }
    
}

#pragma mark ==========点击加入购物车==========
- (IBAction)addgoodsAction:(UIButton *)sender {
    if (self.isRealStore) {
        //无货
        [app showToastView:@"该商品暂无库存"];
    }else{
        //有货
        //添加购物车
        [self addShoppingCart];
    }
    
}
#pragma mark ==========加入购物车逻辑==========
-(void)addShoppingCart{
    if ([self.addgoodsModel.quantity integerValue] <= 0) {
        //弹出
        //弹出规格 1选择规格  2加购物车  3立即购买
        [self pushSelectGoodsView:@"2"];
        return;
    }
    if ([userManager oneceJudgeLoginState]) {
        //已经是登录状态
        self.addgoodsModel.obj_type = @"item";
        self.addgoodsModel.mode = @"cart";//加入购物车
        WEAKSELF;
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        parameters[@"quantity"] = self.addgoodsModel.quantity;
        parameters[@"sku_id"] = self.addgoodsModel.sku_id;
        parameters[@"package_sku_ids"] = self.addgoodsModel.package_sku_ids;
        parameters[@"package_id"] = self.addgoodsModel.package_id;
        parameters[@"obj_type"] = @"item";
        parameters[@"mode"] = @"cart";
        [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        [LBService post:GOODS_ADD_CART params:parameters completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //
                [app showToastView:@"加入购物车成功"];
                weakSelf.addgoodsModel = nil;
            }else{
                [app showToastView:response.message];
            }
        }];
    }else{
        //还没有登录，那么就直接加入数据库
        //创建表
        [self.fmdbManage createTableWithName:@""];
        //加入数据库
        BOOL isSu = [self.fmdbManage writeDBWithData:self.addgoodsModel];
        if (isSu) {
            [app showToastView:@"加入购物车成功"];
        }else{
           [app showToastView:@"加入购物车失败"];
        }
    }
    
    
}
#pragma mark ==========点击立即购买==========
- (IBAction)buyAction:(UIButton *)sender {
    //立即购买
    [self buyGoodsAndSelectGoods];
    
}
#pragma mark ==========立即购买啦，立即购买啦==========
-(void)buyGoodsAndSelectGoods{
    if ([userManager judgeLoginState]) {
        
        if ([self.addgoodsModel.quantity integerValue] <= 0) {
            //弹出
            //弹出规格 1选择规格  2加购物车  3立即购买
            [self pushSelectGoodsView:@"3"];
            return;
        }
        //立即购买
        self.addgoodsModel.obj_type = @"item";
        self.addgoodsModel.mode = @"fastbuy";//立即购买
        WEAKSELF;
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.addgoodsModel.mj_keyValues];
        [dic setObject:LocalValue(TOKEN_XY_APP) forKey:@"accessToken"];
        [LBService post:GOODS_ADD_CART params:dic completion:^(LBResponse *response) {
            if (response.succeed) {
                //跳到结算页
                weakSelf.addgoodsModel = nil;
                OrderPayViewController *vc= [[OrderPayViewController alloc] initWithNibName:@"OrderPayViewController" bundle:nil];
                vc.isModel = @"fastbuy";
                vc.controllerClass = @"ProductFirstViewController";
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else{
                [app showToastView:response.message];
            }
        }];
        
    }
}
#pragma mark ==========联系客服==========
- (IBAction)connectAction:(UIButton *)sender {
    //联系我们
    HBGoodsServiceVC *vc = [[HBGoodsServiceVC alloc]initWithNibName:@"HBGoodsServiceVC" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========点击收藏==========
- (IBAction)collectAction:(UIButton *)sender {
    //添加收藏
    if ([userManager judgeLoginState]) {
        
        self.itemCollectBtn.selected = !self.itemCollectBtn.selected;
        if (self.itemCollectBtn.selected) {
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"item_id"] = self.productModel.item.item_id;
            parameters[@"accessToken"] = LocalValue(TOKEN_XY_APP);
            [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
            [LBService post:GOODS_FAVORITE_ADD params:parameters completion:^(LBResponse *response) {
                [MBProgressHUD hideHUD];
                if (response.succeed) {
                    //
                    [MBProgressHUD showSuccessMessage:@"收藏成功"];
                }else{
                    [app showToastView:response.message];
                }
            }];
        }else{//取消
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"item_id"] = self.productModel.item.item_id;
            parameters[@"accessToken"] = LocalValue(TOKEN_XY_APP);
            [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
            [LBService post:GOODS_FAVORITE_ITEM_REMOVE params:parameters completion:^(LBResponse *response) {
                [MBProgressHUD hideHUD];
                if (response.succeed) {
                    //
                    [MBProgressHUD showSuccessMessage:@"取消成功"];
                }else{
                    [app showToastView:response.message];
                }
            }];
        }
    }
    
}
#pragma mark ==========点击查看购物车==========
- (IBAction)toshoppingCarAction:(UIButton *)sender {
    //查看购物车
    ShoppingCarViewController *vc = [[ShoppingCarViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (UITableViewCell*)cellFromTableView:(UITableView *)tableView rid:(NSString*)rid
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:rid];
    
    if(!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:rid owner:nil options:nil] firstObject];
        
        cell.preservesSuperviewLayoutMargins = NO;
        cell.separatorInset = UIEdgeInsetsMake(0, 15.0f, 0, 15.0f);
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
    return cell;
}
- (IBAction)clickDaoHuoBtn:(UIButton *)sender {
    if (self.isRealStore) {
        //无货
        [app showToastView:@"该商品暂无库存"];
    }
    
}
@end



