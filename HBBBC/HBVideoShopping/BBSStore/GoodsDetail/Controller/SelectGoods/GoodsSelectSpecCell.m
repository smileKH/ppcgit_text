//
//  GoodsSelectSpecCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/3.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "GoodsSelectSpecCell.h"

@implementation GoodsSelectSpecCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.title.layer.masksToBounds = YES;
    self.title.layer.cornerRadius = 6;
}

- (void)setSelected:(BOOL)selected
{
    if (selected) {
        self.title.backgroundColor = MAINTextCOLOR;
    }else{
        self.title.backgroundColor = white_Color;
    }
}

- (void)setModel:(Spec_values *)model
{
    _model = model;
    self.title.text = model.spec_value;
}
@end
