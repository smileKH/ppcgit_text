//
//  GoodsSelectSpecHeader.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/3.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsSelectSpecHeader : UICollectionReusableView
@property (nonatomic, strong) IBOutlet UILabel *title;
@end
