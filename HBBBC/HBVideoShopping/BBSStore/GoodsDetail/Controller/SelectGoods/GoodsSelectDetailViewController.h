//
//  GoodsSelectDetailViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/19.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
#import "Cart_add.h"
//选择规格
@interface GoodsSelectDetailViewController : HBRootViewController
{
    NSInteger selectIndex;
    NSInteger selectIndexs;
    Spec_values *modelColor;
    Spec_values *modelSpec;
    NSInteger counts;
}
@property (nonatomic, strong) ProductDetailModel *model;
@property (nonatomic, strong) NSArray *specsArray;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionview;
@property (strong, nonatomic) IBOutlet UILabel *specLabel;//规格值
@property (strong, nonatomic) IBOutlet UILabel *price;//价格
@property (strong, nonatomic) IBOutlet UIImageView *picImg;
@property (strong, nonatomic) IBOutlet UIButton *confirmBtn;
- (IBAction)dissmiss:(UIButton *)sender;
- (IBAction)confirmAction:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UILabel *numberLabel;

- (IBAction)minusAction:(UIButton *)sender;
- (IBAction)addCount:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *topView;


@property (strong, nonatomic) IBOutlet UIView *countView;
@property (nonatomic, strong) Cart_add *car_addModel;
@property (nonatomic, strong) NSString * type;//1选择规格  2加购物车  3立即购买


@property (nonatomic, strong) void (^confirmGoods)(Cart_add *caraddmodel,NSString *type);
@property (weak, nonatomic) IBOutlet UIButton *daoBtn;


@property (weak, nonatomic) IBOutlet UIView *bottonView;

- (IBAction)clickDaoHuoBtn:(UIButton *)sender;

@end


