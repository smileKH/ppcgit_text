//
//  GoodsSelectDetailViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/19.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "GoodsSelectDetailViewController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "GoodsSelectSpecCell.h"
#import "GoodsSelectSpecHeader.h"

@interface GoodsSelectDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
{
    NSString *goodsSkuid;//货品ID
}
/**
 *  加入购物车状态
 */
@property (nonatomic, assign) BOOL   isRealStore;
@end

@implementation GoodsSelectDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //选择商品规格
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.4];
    self.picImg.layer.masksToBounds = YES;
    self.picImg.layer.cornerRadius = 6;
    self.picImg.layer.borderWidth = 2;
    self.picImg.layer.borderColor =white_Color.CGColor;
    
    self.countView.layer.masksToBounds = YES;
    self.countView.layer.cornerRadius = 10;
    self.countView.layer.borderWidth = 1;
    self.countView.layer.borderColor = UIColorFromHex(0xcccccc).CGColor;
    selectIndex = 0;
    selectIndexs = 0;
    counts = 1;
    [self setupCollection];
    
    //设置模型初值
    [self setProductDetailModelInit];
    
    //点击手势
    UITapGestureRecognizer *r5 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doTapChange:)];
    r5.numberOfTapsRequired = 1;
    [self.topView addGestureRecognizer:r5];
    
}
-(void)doTapChange:(UITapGestureRecognizer *)sender{
    [self.view removeFromSuperview];
}
- (void)setupCollection
{
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.columnCount = 6;
    layout.headerHeight = 30;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 8;
    layout.minimumInteritemSpacing = 15;
   
    self.collectionview.collectionViewLayout = layout;
    [self.collectionview registerNib:[UINib nibWithNibName:@"GoodsSelectSpecCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.collectionview registerNib:[UINib nibWithNibName:@"GoodsSelectSpecHeader" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:@"header"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dissmiss:(UIButton *)sender {
    [self.view removeFromSuperview];
}

#pragma mark ==========点击确定按钮==========
- (IBAction)confirmAction:(UIButton *)sender {
    if (self.isRealStore) {
        //无货
        [app showToastView:@"该商品暂无库存"];
        [self.view removeFromSuperview];
    }else{
        //有货
        
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        NSString *strings = nil;
        if ([self.model.item.spec_count isEqualToString:@"1"]) {
            //一种规格
            Specs *s0 = self.specsArray[0];
            strings =[NSString stringWithFormat:@"%@:%@",s0.spec_name, modelColor.spec_value];
        }else if([self.model.item.spec_count isEqualToString:@"2"]){
            //两种规格
            Specs *s0 = self.specsArray[0];
            Specs *s1 = self.specsArray[1];
            strings =[NSString stringWithFormat:@"%@:%@、%@:%@",s0.spec_name, modelColor.spec_value,s1.spec_name,modelSpec.spec_value];
        }
    
    NSDictionary *dicts = @{@"item_id":self.model.item.item_id,@"spec_info":strings};
    [LBService post:GOODS_SKU_DETAIL params:dicts completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            
            NSArray *array = response.result[@"data"];
            if ([HBHuTool judgeArrayIsEmpty:array]) {
                goodsSkuid = array[0][@"sku_id"];
            }
            
            //创建实例
            weakSelf.car_addModel = [Cart_add new];
            weakSelf.car_addModel.quantity = integerString(counts);
            weakSelf.car_addModel.obj_type = @"item";
            weakSelf.car_addModel.sku_id = goodsSkuid;
            weakSelf.car_addModel.shopId = weakSelf.model.shop.shop_id;
            weakSelf.car_addModel.goodsId = weakSelf.model.item.item_id;
            weakSelf.car_addModel.goodsName = weakSelf.model.item.sub_title;
            weakSelf.car_addModel.shopName = weakSelf.model.shop.shop_name;
            weakSelf.car_addModel.image_default_id = weakSelf.model.item.image_default_id;
            weakSelf.car_addModel.money = weakSelf.model.item.cost_price;
            if (counts > 0) {
                weakSelf.confirmGoods(self.car_addModel,self.type);
            }
            [weakSelf.view removeFromSuperview];
        }else{
            [app showToastView:response.message];
        }
    }];
        
    }
    
}

//设置初始值
-(void)setProductDetailModelInit{
    if ([self.model.item.realStore integerValue]>0) {
        //有货
        self.isRealStore = NO;
        
    }else{
        //无货
        self.isRealStore = YES;
        
    }
    
    self.specsArray = self.model.item.spec.specs;
    
    [self.collectionview reloadData];
    [self loadMianView];
}


- (void)loadMianView
{
    if ([self.model.item.spec_count isEqualToString:@"1"]) {
        //一种规格
        Specs *s0 = self.specsArray[0];
        if (selectIndex >= 0) {
            modelColor = s0.spec_values[selectIndex];
        }
        
        [self.picImg sd_setImageWithURL:[NSURL URLWithString:modelColor.spec_image]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
        
        if ([HBHuTool isJudgeString:s0.spec_name]) {
            self.specLabel.text = [NSString stringWithFormat:@"请选择规格"];
        }else{
            self.specLabel.text = [NSString stringWithFormat:@"%@:%@",s0.spec_name, modelColor.spec_value];
        }
        
        self.price.text = [NSString stringWithFormat:@"¥ %0.2f",[_model.item.price doubleValue]];
        
    }else if([self.model.item.spec_count isEqualToString:@"2"]){
        //两种规格
        Specs *s0 = self.specsArray[0];
        Specs *s1 = self.specsArray[1];
        if (selectIndex >= 0) {
            modelColor = s0.spec_values[selectIndex];
        }
        if (selectIndexs >= 0) {
            modelSpec = s1.spec_values[selectIndexs];
        }
        
        [self.picImg sd_setImageWithURL:[NSURL URLWithString:modelColor.spec_image]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
        
        if ([HBHuTool isJudgeString:s0.spec_name]&&[HBHuTool isJudgeString:s1.spec_name]) {
            self.specLabel.text = [NSString stringWithFormat:@"请选择规格"];
        }else if ([HBHuTool isJudgeString:s0.spec_name]){
            self.specLabel.text = [NSString stringWithFormat:@"%@:%@",s1.spec_name,modelSpec.spec_value];
        }else if ([HBHuTool isJudgeString:s1.spec_name]){
            self.specLabel.text = [NSString stringWithFormat:@"%@:%@",s0.spec_name, modelColor.spec_value];
        }else{
            self.specLabel.text = [NSString stringWithFormat:@"%@:%@,%@:%@",s0.spec_name, modelColor.spec_value,s1.spec_name,modelSpec.spec_value];
        }
        
        self.price.text = [NSString stringWithFormat:@"¥ %0.2f",[_model.item.price doubleValue]];
    }
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    Specs *specs = self.specsArray[section];
    
    return [specs.spec_values count];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.specsArray.count;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodsSelectSpecCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    Specs *specs = self.specsArray[indexPath.section];
    cell.model = specs.spec_values[indexPath.row];
    if (indexPath.section == 0) {
        if (indexPath.row == selectIndex) {
            cell.selected = YES;
        }else{
            cell.selected = NO;
        }
    }else{
        if (indexPath.row == selectIndexs) {
            cell.selected = YES;
        }else{
            cell.selected = NO;
        }
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        selectIndex = indexPath.row;
    }else{
        selectIndexs = indexPath.row;
    }
    [self.collectionview reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
    [self loadMianView];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH/6, 50);
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    if (kind == CHTCollectionElementKindSectionHeader) {
        GoodsSelectSpecHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        Specs *specs = self.specsArray[indexPath.section];
        header.title.text = specs.spec_name;
        return header;
    }
    return nil;
}

- (IBAction)minusAction:(UIButton *)sender {
    if (counts == 1) {
        self.numberLabel.text = @"1";
        counts = 1;
        return;
    }
    counts--;
    self.numberLabel.text = integerString(counts);
}

- (IBAction)addCount:(UIButton *)sender {
    counts ++;
    self.numberLabel.text = integerString(counts);
}

- (IBAction)clickDaoHuoBtn:(UIButton *)sender {
    
    [app showToastView:@"该商品暂无库存"];
}
@end


