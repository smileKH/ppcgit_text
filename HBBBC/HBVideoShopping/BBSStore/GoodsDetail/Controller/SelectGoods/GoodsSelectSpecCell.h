//
//  GoodsSelectSpecCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/3.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
@interface GoodsSelectSpecCell : UICollectionViewCell
@property (nonatomic, strong) Spec_values *model;
@property (strong, nonatomic) IBOutlet UILabel *title;

@end
