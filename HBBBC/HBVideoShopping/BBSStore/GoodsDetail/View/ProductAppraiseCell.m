//
//  ProductAppraiseCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ProductAppraiseCell.h"

@implementation ProductAppraiseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.model.catchCellHeight = self.bounds.size.height+1;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(GoodsAppraiseModel *)model

{
    _model  =  model;
    if (!model) {
        return;
    }
    self.content.text = model.content;
    self.time.text =  [NSString stringWithFormat:@"%@  %@",model.user_name,[Is stringToTimeString:model.created_time]];
    if ([model.result isEqualToString:@"neutral"]) {
        self.state.text = @"中评";
    }else if ([model.result isEqualToString:@"bad"]){
        self.state.text = @"差评";
    }else if ([model.result isEqualToString:@"good"]){
        self.state.text = @"好评";
    }
    
}
@end
