//
//  HBPromotionTagCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
@interface HBPromotionTagCell : UITableViewCell
@property (nonatomic, strong) HBPromotionListModel * model;

@property (weak, nonatomic) IBOutlet UILabel *manJianLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;


@end
