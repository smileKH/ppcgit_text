//
//  HBProductSharePushView.h
//  HBVideoShopping
//
//  Created by aplle on 2018/6/26.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBProductSharePushView : UIView

@property (nonatomic, copy)void(^clickShareItemsView)(NSInteger index);
-(void)show;
@end
