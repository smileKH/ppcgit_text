//
//  ProductAppraiseHeaderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/19.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ProductAppraiseHeaderCell.h"

@implementation ProductAppraiseHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _state = @"-100";
    // Initialization code
    
//    [self.allBtn setTitleColor:MAINREDCOLOR forState:UIControlStateSelected];
//    [self.allBtn setTitleColor:MAINBLACKCOLOR forState:UIControlStateNormal];
//    [self.goodBtn setTitleColor:MAINREDCOLOR forState:UIControlStateSelected];
//    [self.goodBtn setTitleColor:MAINBLACKCOLOR forState:UIControlStateNormal];
//    [self.nomalBtn setTitleColor:MAINREDCOLOR forState:UIControlStateSelected];
//    [self.nomalBtn setTitleColor:MAINBLACKCOLOR forState:UIControlStateNormal];
//    [self.badbtn setTitleColor:MAINREDCOLOR forState:UIControlStateSelected];
//    [self.badbtn setTitleColor:MAINBLACKCOLOR forState:UIControlStateNormal];
//    [self.picBtn setTitleColor:MAINREDCOLOR forState:UIControlStateSelected];
//    [self.picBtn setTitleColor:MAINBLACKCOLOR forState:UIControlStateNormal];
    
    self.allBtn.layer.masksToBounds = YES;
    self.allBtn.layer.cornerRadius = 4;
    self.allBtn.layer.borderColor = MAINREDCOLOR.CGColor;
    self.allBtn.layer.borderWidth = 1;
    
    self.picBtn.layer.masksToBounds = YES;
    self.picBtn.layer.cornerRadius = 4;
    self.picBtn.layer.borderColor = MAINREDCOLOR.CGColor;
    self.picBtn.layer.borderWidth = 1;
    
    self.badbtn.layer.masksToBounds = YES;
    self.badbtn.layer.cornerRadius = 4;
    self.badbtn.layer.borderColor = MAINREDCOLOR.CGColor;
    self.badbtn.layer.borderWidth = 1;
    
    self.nomalBtn.layer.masksToBounds = YES;
    self.nomalBtn.layer.cornerRadius = 4;
    self.nomalBtn.layer.borderColor = MAINREDCOLOR.CGColor;
    self.nomalBtn.layer.borderWidth = 1;
    
    self.goodBtn.layer.masksToBounds = YES;
    self.goodBtn.layer.cornerRadius = 4;
    self.goodBtn.layer.borderColor = MAINREDCOLOR.CGColor;
    self.goodBtn.layer.borderWidth = 1;
    
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setPingStr:(NSString *)pingStr{
    _pingStr = pingStr;
    if (![HBHuTool isJudgeString:pingStr]) {
        self.pingJiaLab.text = [NSString stringWithFormat:@"%@人评价",pingStr];
    }else{
        self.pingJiaLab.text = @"0人评价";
    }
    
}
- (void)setHaoPingStr:(NSString *)haoPingStr{
    _haoPingStr = haoPingStr;
    if (![HBHuTool isJudgeString:haoPingStr]) {
        self.haoPingLab.text = [NSString stringWithFormat:@"%@%@好评率",haoPingStr,@"%"];
    }else{
        self.haoPingLab.text = @"0%好评率";
    }
}
- (void)setState:(NSString *)state{
    if ([_state isEqualToString:state]) {
        return;
    }
    _state = state;
    [self restAllButton];
    UIButton *btns= [self.contentView viewWithTag:[state integerValue]+1];
    btns.selected = YES;
    btns.backgroundColor = MAINREDCOLOR;
}
-(void)restAllButton{
    for (NSInteger i = 0; i< 5; i++) {
        UIButton *btns= [self.contentView viewWithTag:i+1];
        btns.selected = NO;
        btns.backgroundColor = white_Color;
    }
}
- (IBAction)clickAction:(UIButton *)sender {
    NSString *selectState = integerString(sender.tag-1);
    if ([_state isEqualToString:selectState]) {
        return;
    }
    [self restAllButton];
    sender.selected = YES;
    sender.backgroundColor = MAINREDCOLOR;
    self.state = selectState;
    self.selectState(self.state);
}
@end
