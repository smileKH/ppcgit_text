//
//  ProductNaturePropsCell.m
//  HBVideoShopping
//
//  Created by hua on 2018/4/13.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "ProductNaturePropsCell.h"

@implementation ProductNaturePropsCell
-(void)layoutSubviews{
    [super layoutSubviews];
    self.props.catchCellHeight = self.bounds.size.height+1;
}
-(void)setProps:(NatureProps *)props{
    _props = props;
    if (!props) {
        return;
    }
    NSString *string = [NSString stringWithFormat:@"%@                     %@",props.prop_name,props.prop_value];

    self.textLabel.attributedText =  [StringAttribute stringToAttributeString:string withRange:NSMakeRange(string.length-2, 2) withFont:14 withColor:MAINBLACKCOLOR];//@"面料:   涤纶 ";
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.textLabel.textColor = UIColorFromHex(0xcccccc);
    self.textLabel.font = [UIFont systemFontOfSize:14];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
