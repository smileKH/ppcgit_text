//
//  ProductAppraiseHeaderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/19.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductAppraiseHeaderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *allBtn;
@property (strong, nonatomic) IBOutlet UIButton *goodBtn;
@property (strong, nonatomic) IBOutlet UIButton *nomalBtn;
@property (strong, nonatomic) IBOutlet UIButton *badbtn;
@property (strong, nonatomic) IBOutlet UIButton *picBtn;
- (IBAction)clickAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *haoPingLab;

@property (weak, nonatomic) IBOutlet UILabel *pingJiaLab;

@property (nonatomic, strong) NSString * pingStr;
@property (nonatomic, strong) NSString * haoPingStr;

@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) void(^selectState)(NSString *sta);


@end
