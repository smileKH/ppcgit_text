//
//  HBProductSharePushView.m
//  HBVideoShopping
//
//  Created by aplle on 2018/6/26.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBProductSharePushView.h"
#import "UIButton+ImageTitleSpacing.h"
#define bottomOffX       0//X
#define bottomOffY       SCREEN_HEIGHT//Y
#define bottomOffWidth   SCREEN_WIDTH//Y
#define bottomOffHeight  150//Y
@interface HBProductSharePushView ()
//主要的Windows
@property (nonatomic, strong) UIWindow * backWindow;
//黑色的view
@property (nonatomic, strong) UIView * drakView;
//白色的view
@property (nonatomic, strong) UIView * bottomView;
//取消button
@property (nonatomic, strong) UIButton * cancelButton;
//------------------------分割线-------------------------//

@end
@implementation HBProductSharePushView

- (instancetype)init{
    self = [super init];
    if (self) {
        //初始化黑色的view
        [self addSubview:self.drakView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss:)];
        [self.drakView addGestureRecognizer:tap];
        
        //底部的子视图
        [self addSubview:self.bottomView];
        
        //添加取消按钮
        //        [self addSubview:self.cancelButton];
        
        //添加子视图
        [self addBootomView];
        
        //设置视图在Windows上
        [self setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [self.backWindow addSubview:self];
    }
    return self;
}
#pragma mark ==========添加子视图==========
-(void)addBootomView{
    
    NSArray *imgArr = @[@"weixin_haoyou",@"pengyou_quan",@"QQ_haoyou",@"QQ_kongjian"];
    NSArray *titleArr = @[@"微信好友",@"朋友圈",@"QQ好友",@"QQ空间"];
    CGFloat btnWidth = SCREEN_WIDTH/4;
    for (NSInteger i=0; i<imgArr.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(btnWidth*i, 20, btnWidth, btnWidth);
        [btn setImage:[UIImage imageNamed:imgArr[i]] forState:UIControlStateNormal];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleTop imageTitleSpace:10];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        btn.tag = i+100;
        [btn addTarget:self action:@selector(clickShareBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.bottomView addSubview:btn];
    }
}

-(void)clickShareBtn:(UIButton *)btn{
    [self dismiss];
    if (self.clickShareItemsView) {
        self.clickShareItemsView(btn.tag);
    }
}
#pragma mark ==========初始化方法==========
- (UIView *)drakView{
    if (!_drakView) {
        _drakView = [[UIView alloc] init];
        [_drakView setAlpha:0];
        [_drakView setUserInteractionEnabled:NO];
        [_drakView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_drakView setBackgroundColor:[UIColor grayColor]];
    }
    return _drakView;
}

- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.frame = CGRectMake(bottomOffX, bottomOffY, bottomOffWidth, bottomOffHeight);
    }
    return _bottomView;
}

- (UIButton *)cancelButton{
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.backgroundColor = [UIColor clearColor];
        [_cancelButton setFrame:CGRectMake(SCREEN_WIDTH-40, -40, 40, 40)];
        [_cancelButton setImage:[UIImage imageNamed:@"push_quxiao.png"] forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(clickCancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}
- (UIWindow *)backWindow {
    
    if (_backWindow == nil) {
        
        _backWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backWindow.windowLevel       = UIWindowLevelStatusBar;
        _backWindow.backgroundColor   = [UIColor clearColor];
        _backWindow.hidden = NO;
    }
    
    return _backWindow;
}
#pragma mark ==========点击取消按钮==========
-(void)clickCancelBtn:(UIButton *)button{
    
}
- (void)dismiss:(UITapGestureRecognizer *)tap {
    
    [self dismiss];
}

-(void)dismiss{
    //添加动画
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        //设置动画
        [self.drakView setAlpha:0];
        [self.drakView setUserInteractionEnabled:NO];
        //设置动画
        CGRect frame = self.bottomView.frame;
        frame.origin.y = SCREEN_HEIGHT;
        [self.bottomView setFrame:frame];
        
        CGRect cancelFrame = self.cancelButton.frame;
        cancelFrame.origin.y = -40;
        [self.cancelButton setFrame:cancelFrame];
        
    } completion:^(BOOL finished) {
        //完成
        self.backWindow.hidden = YES;
        [self removeFromSuperview];
    }];
}
- (void)show{
    self.backWindow.hidden = NO;
    [UIView animateWithDuration:0.75f delay:0.2 usingSpringWithDamping:0.65f initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        //背景颜色
        [self.drakView setAlpha:0.4f];
        [self.drakView setUserInteractionEnabled:YES];
        
        //改变frame
        CGRect frame = self.bottomView.frame;
        frame.origin.y = SCREEN_HEIGHT-bottomOffHeight;
        [self.bottomView setFrame:frame];
        
        CGRect cancelFrame = self.cancelButton.frame;
        cancelFrame.origin.y = frame.origin.y/2;
        [self.cancelButton setFrame:cancelFrame];
    } completion:^(BOOL finished) {
        
    }];
}

@end
