//
//  ProductImagesCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ProductImagesCell.h"
@interface ProductImagesCell()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
@implementation ProductImagesCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
-(void)setContentWebView:(UIView *)contentWebView{
    _contentWebView = contentWebView;
    _contentWebView.hidden = NO;
    [self.contentView addSubview:_contentWebView];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
