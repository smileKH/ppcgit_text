//
//  ProductNaturePropsCell.h
//  HBVideoShopping
//
//  Created by hua on 2018/4/13.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
@interface ProductNaturePropsCell : UITableViewCell
@property (nonatomic, strong) NatureProps *props;
@end
