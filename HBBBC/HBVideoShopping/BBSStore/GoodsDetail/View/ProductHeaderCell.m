//
//  ProductHeaderCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ProductHeaderCell.h"
#import "XQVideoView.h"
@interface ProductHeaderCell ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) XQVideoView *videoView;
@property (nonatomic ,assign)BOOL isShowVideo;
@end
@implementation ProductHeaderCell
- (void)awakeFromNib {
    [super awakeFromNib];
    //添加监听
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closePlayVidoView) name:@"ProductHeaderCell" object:nil];
}
-(void)closePlayVidoView{
    [self.videoView stop];
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.model.catchCellHeight = self.bounds.size.height+1;
}
-(void)setProdModel:(ProductDetailModel *)prodModel{
    _prodModel = prodModel;
    if ([prodModel.shop.shop_type isEqualToString:@"other"]) {
        //人员
        self.price.hidden = YES;
        self.totalPrice.hidden = YES;
        self.havebuyNumber.hidden = YES;
    }else{
        //其他
        self.price.hidden = NO;
        self.totalPrice.hidden = NO;
        self.havebuyNumber.hidden = NO;
    }
}

-(void)setModel:(Item *)model
{
    _model = model;
    if (!model) {
        return;
    }
    _title.text = model.title;
    
    _price.text = [NSString stringWithFormat:@"¥ %0.2f",[model.price doubleValue]];
    
    NSString *string = [NSString stringWithFormat:@"¥ %0.2f",[model.mkt_price doubleValue]];
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:string attributes:attribtDic];
    
    // 赋值
    _totalPrice.attributedText = attribtStr;
    self.havebuyNumber.text = [NSString stringWithFormat:@"已有%@人购买",model.sold_quantity];

//    [self.banner.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    NSArray *imagesArray = model.images;
//
//    for (NSInteger i = 0; i < imagesArray.count; i ++) {
//        UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.banner.bounds.size.width, 0, self.banner.bounds.size.width,  self.banner.bounds.size.height)];
//        [imgv sd_setImageWithURL:[NSURL URLWithString:imagesArray[i]]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
//        [self.banner addSubview:imgv];
//    }
//    self.banner.contentSize = CGSizeMake(imagesArray.count*SCREEN_WIDTH, SCREEN_WIDTH);
    
    NSMutableArray *imagesArray = [NSMutableArray array];
    self.isShowVideo = NO;
    //@[@"http://221.228.226.5/15/t/s/h/v/tshvhsxwkbjlipfohhamjkraxuknsc/sh.yinyuetai.com/88DC015DB03C829C2126EEBBB5A887CB.mp4", @"1.jpg", @"2.jpg", @"3.jpg"];
    if (![HBHuTool isJudgeString:model.video_path]) {
        [imagesArray addObject:model.video_path];
        self.isShowVideo = YES;
    }
    [imagesArray addObjectsFromArray:model.images];
    
    if (@available(iOS 11.0, *)) {
        self.banner.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    self.banner.contentSize = CGSizeMake(self.banner.width * imagesArray.count, self.banner.height);
    self.banner.pagingEnabled = YES;
    self.banner.showsHorizontalScrollIndicator = NO;
    self.banner.delegate = self;
    
    for (NSInteger index = 0; index < imagesArray.count; index ++) {
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(self.banner.width * index, 0, self.banner.width, self.banner.height)];
        [img sd_setImageWithURL:[NSURL URLWithString:imagesArray[index]] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
        img.contentMode = UIViewContentModeScaleAspectFit;
        /** 测试 **/
        if (self.isShowVideo) {
            if (index == 0) {
                self.videoView = [XQVideoView videoViewFrame:img.frame videoUrl:imagesArray[index]];
                self.videoView.videoUrl = imagesArray[index];
                [self.banner addSubview:self.videoView];
            }else {
                [self.banner addSubview:img];
            }
        }else{
            [self.banner addSubview:img];
        }
        
    }
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.frame = CGRectMake(0, self.banner.height - 30, self.banner.width, 30);
    self.pageControl.numberOfPages = imagesArray.count;
    self.pageControl.currentPage = 0;
    self.pageControl.enabled = NO;
    self.pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    [self addSubview:self.pageControl];
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSInteger currentPage = round(scrollView.contentOffset.x / self.banner.width);
    self.pageControl.currentPage = currentPage;
    if (self.isShowVideo) {
        if (self.videoView.isPlay) {
            if (currentPage == 0) {
                [self.videoView start];
            }else {
                [self.videoView stop];
            }
        }
    }

}
- (IBAction)clickShareButton:(UIButton *)sender {
    if (self.clickProductHeaderShare) {
        self.clickProductHeaderShare(nil);
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
