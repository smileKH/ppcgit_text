//
//  HBPromotionTagCell.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/4.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBPromotionTagCell.h"

@implementation HBPromotionTagCell
-(void)layoutSubviews{
    [super layoutSubviews];
    self.model.catchCellHeight = self.bounds.size.height+1;
}
- (void)setModel:(HBPromotionListModel *)model{
    _model = model;
    if (!model) {
        return;
    }
    self.manJianLab.text = [NSString stringWithFormat:@"%@",model.promotion_tag];
    self.detailLab.text = [NSString stringWithFormat:@"%@",model.promotion_name];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.manJianLab.layer.borderColor = UIColor.redColor.CGColor;
    self.manJianLab.layer.borderWidth = 1;
    self.manJianLab.layer.cornerRadius = 3;
    self.manJianLab.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
   
}

@end
