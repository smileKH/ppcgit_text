//
//  ProductSecondCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
#import "HBCollectStateModel.h"
@interface ProductSecondCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *scBtn;
@property (weak, nonatomic) IBOutlet UIButton *interBtn;
@property (nonatomic, strong) IBOutlet UILabel *shopName;
@property (nonatomic, strong) Shop *model;
@property (nonatomic, strong) HBCollectStateModel * collModel;
- (IBAction)collectAction:(UIButton *)sender;
- (IBAction)toShopAction:(UIButton *)sender;
@property (nonatomic, strong) void(^toShop)(Shop *shop);
@property (nonatomic, strong) void(^collectShop)(Shop *shop,BOOL canCollectShop);
@end
