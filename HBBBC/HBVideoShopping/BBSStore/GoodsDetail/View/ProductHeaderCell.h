//
//  ProductHeaderCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
@interface ProductHeaderCell : UITableViewCell

@property (nonatomic, strong) Item *model;
@property (nonatomic ,strong)ProductDetailModel *prodModel;
@property (strong, nonatomic) IBOutlet UIButton *shareBtn;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *totalPrice;
@property (strong, nonatomic) IBOutlet UILabel *havebuyNumber;
@property (strong, nonatomic) IBOutlet UIScrollView *banner;
@property (nonatomic ,copy)void(^clickProductHeaderShare)(id obj);
- (IBAction)clickShareButton:(UIButton *)sender;

@end
