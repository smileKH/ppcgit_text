//
//  ProductSecondCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ProductSecondCell.h"

@implementation ProductSecondCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.scBtn.layer.masksToBounds = YES;
    self.scBtn.layer.cornerRadius = 4;
    self.scBtn.layer.borderColor = MAINTextCOLOR.CGColor;
    self.scBtn.layer.borderWidth =1;
    self.interBtn.layer.masksToBounds = YES;
    self.interBtn.layer.cornerRadius = 4;
    self.interBtn.layer.borderColor = MAINTextCOLOR.CGColor;
    self.interBtn.layer.borderWidth =1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.model.catchCellHeight = self.bounds.size.height+1;
}
- (void)setCollModel:(HBCollectStateModel *)collModel{
    _collModel = collModel;
    if ([collModel.is_shop isEqualToString:@"1"]) {
        self.scBtn.selected = YES;
    }else{
        self.scBtn.selected = NO;
    }
}
-(void)setModel:(Shop *)model
{
    _model = model;
    if (!model) {
        return;
    }
    self.shopName.text = model.shop_name;
}
- (IBAction)collectAction:(UIButton *)sender {
    
    //判断是否登录
    if ([userManager judgeLoginState]) {
        self.scBtn.selected = !self.scBtn.selected;
        self.collectShop(self.model,self.scBtn.selected);
    }
//    NSString * shop_id = self.model.shop_id;
//    if (!shop_id.length) {
//        return;
//    }
//    NSString * accessToken = LocalValue(TOKEN_XY_APP);
//    BOOL canCollect = NO;
//    if (accessToken.length) {
//        sender.selected = !sender.selected;
//        canCollect = YES;
//    }
//    
//    self.collectShop(self.model,canCollect);
}

- (IBAction)toShopAction:(UIButton *)sender {
    self.toShop(self.model);
}
@end
