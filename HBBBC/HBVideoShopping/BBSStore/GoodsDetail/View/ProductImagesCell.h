//
//  ProductImagesCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/15.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailModel.h"
#import "ProductDescModel.h"
@interface ProductImagesCell : UITableViewCell
@property (nonatomic, strong) UIView *contentWebView;

@end
