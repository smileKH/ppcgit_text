//
//  ProductDetailModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/2/28.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Share;
@class Item;
@class Spec;
@class Specs;
@class NatureProps;
@class Shop;
@class Cur_symbol;
@class SpecSku;
@class Spec_values;
@class HBPromotionTag;
@class HBPromotionListModel;
@class HBProSku;

@interface ProductDetailModel : NSObject
@property (nonatomic, strong) Share *share;
@property (nonatomic, strong) NSString *freeConf;
@property (nonatomic, strong) HBPromotionTag *promotionTag;
@property (nonatomic, strong) NSString *packages;
@property (nonatomic, strong) Item *item;
@property (nonatomic, strong) Shop *shop;
@property (nonatomic, strong) Cur_symbol *cur_symbol;
@end


@interface Share : NSObject
@property (nonatomic, strong) NSString *h5href;//分享的地址
@property (nonatomic, strong) NSString *image;//分享的图片
@end

@interface HBPromotionTag : NSObject
@property (nonatomic, strong) NSArray * fulldiscount;
@end

@interface HBPromotionListModel : NSObject
@property (nonatomic, assign) float catchCellHeight;
@property (nonatomic, strong) NSString * promotion_id;
@property (nonatomic, strong) NSString * rel_promotion_id;
@property (nonatomic, strong) NSString * promotion_name;
@property (nonatomic, strong) NSString * promotion_tag;
@property (nonatomic, strong) NSString * sku_id;
@property (nonatomic, strong) NSString * tag_sku_ids;
@end


@interface Item : NSObject
@property (nonatomic, assign) float catchCellHeight;
@property (nonatomic, strong) NSString *item_id;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *brand_id;
@property (nonatomic, strong) NSString *shop_cat_id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *sub_title;
@property (nonatomic, strong) NSString *bn;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *cost_price;
@property (nonatomic, strong) NSString *mkt_price;
@property (nonatomic, strong) NSString *show_mkt_price;
@property (nonatomic, strong) NSString *weight;
@property (nonatomic, strong) NSString *unit;
@property (nonatomic, strong) NSString *image_default_id;
@property (nonatomic, strong) NSString *order_sort;
@property (nonatomic, strong) NSString *created_time;
@property (nonatomic, strong) NSString *modified_time;
@property (nonatomic, strong) NSString *has_discount;
@property (nonatomic, strong) NSString *is_virtual;
@property (nonatomic, strong) NSString *is_timing;
@property (nonatomic, strong) NSString *violation;
@property (nonatomic, strong) NSString *is_selfshop;
@property (nonatomic, strong) NSString *nospec;
@property (nonatomic, strong) NSString *props_name;
@property (nonatomic, strong) NSString *params;
@property (nonatomic, strong) NSString *sub_stock;
@property (nonatomic, strong) NSString *outer_id;
@property (nonatomic, strong) NSString *is_offline;
@property (nonatomic, strong) NSString *barcode;
@property (nonatomic, strong) NSString *disabled;
@property (nonatomic, strong) NSString *use_platform;
@property (nonatomic, strong) NSString *dlytmpl_id;
@property (nonatomic, strong) NSString *approve_status;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *list_time;
@property (nonatomic, strong) NSString *delist_time;
@property (nonatomic, strong) NSString *sold_quantity;
@property (nonatomic, strong) NSString *rate_count;
@property (nonatomic, strong) NSString *rate_good_count;
@property (nonatomic, strong) NSString *rate_neutral_count;
@property (nonatomic, strong) NSString *rate_bad_count;
@property (nonatomic, strong) NSString *view_count;
@property (nonatomic, strong) NSString *buy_count;
@property (nonatomic, strong) NSString *aftersales_month_count;
@property (nonatomic, strong) NSString *default_weight;
@property (nonatomic, strong) NSArray *images;//图片数组
@property (nonatomic, strong) NSString *brand_name;
@property (nonatomic, strong) NSString *brand_alias;
@property (nonatomic, strong) NSString *brand_logo;
@property (nonatomic, strong) NSString *realStore;//库存 判断是否为零
@property (nonatomic, strong) NSString *freez;
@property (nonatomic, strong) NSString *store;
@property (nonatomic, strong) NSString *valid;//是否有效
@property (nonatomic, strong) NSArray *natureProps;//基本参数
@property (nonatomic, strong) Spec *spec;//货品规格相关
@property (nonatomic, strong) HBProSku *sku;//货品规格相关
@property (nonatomic, strong) NSString * spec_count;//0无规格 1单规格，2 两种规格
@property (nonatomic, strong) NSString * video_path;//是否有视频
@end


@interface HBProSku : NSObject
@property (nonatomic, strong) NSString *sku_id;
@property (nonatomic, strong) NSString *item_id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *bn;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *cost_price;
@property (nonatomic, strong) NSString *mkt_price;
@property (nonatomic, strong) NSString *barcode;
@property (nonatomic, strong) NSString *weight;
@property (nonatomic, strong) NSString *created_time;
@property (nonatomic, strong) NSString *modified_time;
@property (nonatomic, strong) NSString *spec_key;

@property (nonatomic, strong) NSString *spec_info;
@property (nonatomic, strong) NSString *spec_desc;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *outer_id;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *image_default_id;
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *brand_id;
@property (nonatomic, strong) NSString *shop_cat_id;
@property (nonatomic, strong) NSString *store;
@property (nonatomic, strong) NSString *freez;
@property (nonatomic, strong) NSString *realStore;
@end

@interface Spec : NSObject
@property (nonatomic, strong) NSArray *specSku;
@property (nonatomic, strong) NSArray *specs;
@end

@interface SpecSku :NSObject
@property (nonatomic, strong) NSString *sku_id;
@property (nonatomic, strong) NSString *item_id;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *mkt_price;
@property (nonatomic, strong) NSString *activity_price;
@property (nonatomic, strong) NSString *store;
@property (nonatomic, strong) NSString *valid;
@end

@interface Specs : NSObject
@property (nonatomic, strong) NSString *spec_id;
@property (nonatomic, strong) NSString *spec_name;
@property (nonatomic, strong) NSArray *spec_values;
@end

@interface Spec_values : NSObject
@property (nonatomic, strong) NSString *spec_value_id;
@property (nonatomic, strong) NSString *spec_value;
@property (nonatomic, strong) NSString *spec_image;
@end


@interface NatureProps :NSObject
@property (nonatomic, assign) float catchCellHeight;
@property (nonatomic, strong) NSString *prop_id;
@property (nonatomic, strong) NSString *prop_name;
@property (nonatomic, strong) NSString *prop_value_id;
@property (nonatomic, strong) NSString *prop_value;

@end


@interface Shop : NSObject
@property (nonatomic, assign) float catchCellHeight;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *shop_name;
@property (nonatomic, strong) NSString *shop_descript;
@property (nonatomic, strong) NSString *shop_logo;
@property (nonatomic, strong) NSString *shop_type;
@property (nonatomic, strong) NSString *shopname;
@property (nonatomic, strong) NSString *shoptype;
@end


@interface Cur_symbol : NSObject
@property (nonatomic, strong) NSString *sign;
@property (nonatomic, strong) NSString *decimals;
@end

