//
//  Cart_add.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/7.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cart_add : NSObject
@property (nonatomic, strong)  NSString *quantity;//    int        3    商品数量
@property (nonatomic, strong)  NSString *sku_id;//    int    required_if:obj_type,item    3    货品id
@property (nonatomic, strong)  NSString *package_sku_ids;//    string    required_if:obj_type,package    11,21,45    组合促销sku_id
@property (nonatomic, strong)  NSString *package_id;//    integer    sometimes    required    integer | 3 | 组合促销id
@property (nonatomic, strong)  NSString *obj_type;//    string    in:item,package    item    对象类型 item普通商品，package组合促销
@property (nonatomic, strong)  NSString *mode;//cart,fastbuy

//能不能加上几个，店铺名字 商品名称 数量 是否选择 价格 规格
@property (nonatomic, strong) NSString * shopName;
@property (nonatomic, strong) NSString * goodsName;
@property (nonatomic, strong) NSString * shopId;
@property (nonatomic, strong) NSString * goodsId;
@property (nonatomic, strong) NSString * money;
@property (nonatomic, strong) NSString * image_default_id;
@end
