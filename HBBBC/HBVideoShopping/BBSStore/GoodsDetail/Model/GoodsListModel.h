//
//  GoodsListModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/2/27.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Gift;
@interface GoodsListModel : NSObject
@property(nonatomic, strong) NSString * item_id;
@property(nonatomic, strong) NSString * title;
@property(nonatomic, strong) NSString * image_default_id;
@property(nonatomic, strong) NSString * price;
@property(nonatomic, strong) NSString * sold_quantity;
@property(nonatomic, strong) NSArray * promotion;
@property(nonatomic, strong) Gift * gift;
@property (nonatomic ,strong)NSString *shop_type;
@end
@interface Gift : NSObject

@property(nonatomic, strong) NSString * gift_id;
@property(nonatomic, strong) NSString * promotion_tag;
@end

@interface Promotion : NSObject
@property(nonatomic, strong) NSString * promotion_id;
@property(nonatomic, strong) NSString * tag;
@end
