//
//  ProductDescModel.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/6.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ProductDescModel.h"

@implementation ProductDescModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.catchCellHeight = -1;
    }
    return self;
}
- (void)setNatureProps:(NSArray *)natureProps
{
    _natureProps = [NatureProps mj_objectArrayWithKeyValuesArray:natureProps];
}
@end
@implementation Remark

@end
