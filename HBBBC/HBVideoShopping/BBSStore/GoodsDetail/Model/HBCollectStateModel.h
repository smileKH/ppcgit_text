//
//  HBCollectStateModel.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/4/17.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBCollectStateModel : NSObject
@property (nonatomic ,strong)NSString *is_item;
@property (nonatomic ,strong)NSString *is_shop;
@end
