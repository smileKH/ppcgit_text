//
//  GoodsAppraiseModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/2.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsAppraiseModel : NSObject
@property (nonatomic, assign) float catchCellHeight;
@property (nonatomic, strong) NSString *rate_id;
@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *rate_pic;
@property (nonatomic, strong) NSString *is_reply;
@property (nonatomic, strong) NSString *reply_content;
@property (nonatomic, strong) NSString *reply_time;
@property (nonatomic, strong) NSString *anony;
@property (nonatomic, strong) NSString *is_append;
@property (nonatomic, strong) NSString *created_time;
@property (nonatomic, strong) NSString *append;
@property (nonatomic, strong) NSString *user_name;
@end
