//
//  GoodsListModel.m
//  BBSStore
//
//  Created by 马云龙 on 2018/2/27.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "GoodsListModel.h"

@implementation GoodsListModel
-(void)setGift:(Gift *)gift
{
    _gift = gift;
}

- (void)setPromotion:(NSArray *)promotion
{
    _promotion = [Promotion mj_objectArrayWithKeyValuesArray:promotion];
}
@end


@implementation Gift
@end

@implementation Promotion
@end
