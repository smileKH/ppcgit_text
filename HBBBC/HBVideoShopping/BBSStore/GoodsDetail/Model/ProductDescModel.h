//
//  ProductDescModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/6.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductDetailModel.h"
@class Remark;
@interface ProductDescModel : NSObject
@property (nonatomic, assign) float catchCellHeight;
@property (nonatomic, strong) NSString * item_id;
@property (nonatomic, strong) id params;

@property (nonatomic, strong) NSString * wap_desc;
@property (nonatomic, strong) NSArray * natureProps;
@property (nonatomic, strong) Remark *remark;
@end

@interface Remark : NSObject
@property (nonatomic, strong) NSString *品牌;
@property (nonatomic, strong) NSString *编号;
@property (nonatomic, strong) NSString *备注;
@end
