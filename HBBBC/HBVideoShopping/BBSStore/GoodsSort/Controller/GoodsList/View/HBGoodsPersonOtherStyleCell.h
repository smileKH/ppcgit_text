//
//  HBGoodsPersonOtherStyleCell.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/31.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsListModel.h"
@interface HBGoodsPersonOtherStyleCell : UICollectionViewCell
@property (nonatomic, strong) GoodsListModel *model;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@end
