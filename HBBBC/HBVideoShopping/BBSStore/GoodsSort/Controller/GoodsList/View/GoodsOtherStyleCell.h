//
//  GoodsOtherStyleCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/14.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsListModel.h"
@interface GoodsOtherStyleCell : UICollectionViewCell
@property (nonatomic, strong) GoodsListModel *model;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *numbersd;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@end
