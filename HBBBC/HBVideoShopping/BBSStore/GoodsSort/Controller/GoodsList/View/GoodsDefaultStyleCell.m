//
//  GoodsDefaultStyleCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/14.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "GoodsDefaultStyleCell.h"

@implementation GoodsDefaultStyleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 2;
}
-(void)setModel:(GoodsListModel *)model
{
    _model = model;
    [_image sd_setImageWithURL:[NSURL URLWithString:model.image_default_id]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    _title.text = model.title;
    _price.text = [NSString stringWithFormat:@"¥ %0.2f",[model.price doubleValue]];
    _numbersd.text = [NSString stringWithFormat:@"已有%@人购买",model.sold_quantity];
}
@end
