//
//  HBGoodsPersonOtherStyleCell.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/31.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBGoodsPersonOtherStyleCell.h"

@implementation HBGoodsPersonOtherStyleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(GoodsListModel *)model
{
    _model = model;
    [_image sd_setImageWithURL:[NSURL URLWithString:model.image_default_id]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    _title.text = model.title;
}
@end
