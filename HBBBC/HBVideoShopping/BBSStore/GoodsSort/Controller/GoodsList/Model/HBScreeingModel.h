//
//  HBScreeingModel.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBScreeingModel : NSObject
@property (nonatomic, strong) NSArray * brand;
@property (nonatomic, strong) NSArray * cat;
@property (nonatomic, strong) NSArray * props;
@property (nonatomic, assign) NSInteger  brand_count;
@property (nonatomic, assign) NSInteger  cat_count;
@property (nonatomic, strong) NSArray *  activeFilter;
@end


@interface HBScreeingBrandModel:NSObject
@property (nonatomic, strong) NSString * brand_id;
@property (nonatomic, strong) NSString * brand_name;
@property (nonatomic, assign) BOOL isSelect;
@end

@interface HBScreeingCatModel:NSObject
@property (nonatomic, strong) NSString * cat_id;
@property (nonatomic, strong) NSString * cat_name;
@property (nonatomic, assign) BOOL isSelect;
/**
 默认选中 当前额栏目，不予许不选中
 */
@property (nonatomic, assign) BOOL isNotUnSelect;
@end

@interface HBActiveFilterModel:NSObject

@end

@interface HBScreeingPropsModel:NSObject
@property (nonatomic, strong) NSString * prop_id;
@property (nonatomic, strong) NSString * prop_name;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * secarch;
//@property (nonatomic, strong) NSString * show;
@property (nonatomic, strong) NSString * is_def;
@property (nonatomic, strong) NSString * show_type;
@property (nonatomic, strong) NSString * prop_type;
@property (nonatomic, strong) NSString * prop_memo;
@property (nonatomic, strong) NSString * order_sort;

@property (nonatomic, strong) NSString * modified_time;
@property (nonatomic, strong) NSString * disabled;
@property (nonatomic, strong) NSArray * prop_value;
@property (nonatomic, assign) NSInteger  prop_count;
@property (nonatomic, assign) BOOL isShow;//为了展示
@end


@interface HBProp_valueModel:NSObject
@property (nonatomic, strong) NSString * prop_value_id;
@property (nonatomic, strong) NSString * prop_id;
@property (nonatomic, strong) NSString * prop_value;
@property (nonatomic, strong) NSString * prop_image;
@property (nonatomic, strong) NSString * order_sort;
@property (nonatomic, strong) NSString * prop_index;
@property (nonatomic, assign) BOOL isSelect;
@end

