//
//  HBScreeingModel.m
//  HBVideoShopping
//
//  Created by aplle on 2018/3/25.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBScreeingModel.h"

@implementation HBScreeingModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"brand":@"HBScreeingBrandModel",@"cat":@"HBScreeingCatModel",@"props":@"HBScreeingPropsModel",@"activeFilter":@"HBActiveFilterModel"};
}
@end

@implementation HBScreeingBrandModel

@end

@implementation HBScreeingCatModel

@end

@implementation HBScreeingPropsModel
+(NSDictionary *)mj_objectClassInArray{
    return @{@"prop_value":@"HBProp_valueModel"};
}
@end

@implementation HBProp_valueModel

@end

@implementation HBActiveFilterModel

@end
