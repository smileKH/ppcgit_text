//
//  DBB_CacheTool.h
//  DBBAssistant
//
//  Created by 钟小麦 on 2017/12/4.
//  Copyright © 2017年 fenger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBB_CacheTool : NSObject

+ (instancetype)shareCacheTool;

/*********NSCoding方式缓存***********/
/**
 *  硬盘缓存
 */
- (void)cacheObjectInDisk:(id<NSCoding>)obj forFileName:(NSString *)fileName;
- (id)objectInDiskForFileName:(NSString *)fileName;

@end
