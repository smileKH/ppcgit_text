//
//  AF_BrandCell.m
//  差五个让步
//
//  Created by Elephant on 16/5/4.
//  Copyright © 2016年 Elephant. All rights reserved.
//

#import "AF_BrandCell.h"

#define offset [UIScreen mainScreen].bounds.size.width - 120
#define buttonBackgroundColor [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]

@interface AF_BrandCell ()
{
    NSMutableArray *buttonArr;
}

@end

@implementation AF_BrandCell

+ (instancetype)cellWithTableView:(UITableView *)tableView dataArr:(NSArray *)arr indexPath:(NSIndexPath *)indexPath
{
    NSString * baseCell = [NSString stringWithFormat:@"AF_BrandCell%ld",indexPath.section];

    AF_BrandCell *cell = [tableView dequeueReusableCellWithIdentifier:baseCell];
    if (!cell) {
        cell = [[AF_BrandCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:baseCell dataArr:arr];
        /** 取消cell点击状态 */
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.indexPath = indexPath;
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier dataArr:(NSArray *)arr
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        buttonArr = [NSMutableArray array];
        for (int i=0; i< arr.count; i++) {
            UIButton *button = [[UIButton alloc]init];
            [button setBackgroundColor:buttonBackgroundColor];
            button.titleLabel.font = [UIFont systemFontOfSize:13.0];
            button.tag = i;
            button.clipsToBounds = YES;
            button.layer.cornerRadius = 5.0;
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:button];
            //是否展开选项
            if (_isShrinkage) {
                button.hidden = NO;
            }else {
                if (i > 2) {
                    button.hidden = YES;
                }
            }
            [buttonArr addObject:button];
        }
    }
    return self;
}

- (void)buttonClick:(UIButton *)button
{
    int index = (int)button.tag;
    NSObject *obj = self.attributeArr[index];
    BOOL isSelect = NO;
    if ([obj isKindOfClass:HBProp_valueModel.class]) {
        HBProp_valueModel *model = (HBProp_valueModel*) obj;
        model.isSelect = !model.isSelect;
        isSelect = model.isSelect;
    }else if ([obj isKindOfClass:HBScreeingBrandModel.class]) {
        HBScreeingBrandModel *model = (HBScreeingBrandModel*) obj;
        model.isSelect = !model.isSelect;
        isSelect = model.isSelect;
    }else if ([obj isKindOfClass:HBScreeingCatModel.class]) {
        HBScreeingCatModel *model = (HBScreeingCatModel*) obj;
        
        if (!model.isNotUnSelect) {
            model.isSelect = !model.isSelect;
            isSelect = model.isSelect;
        }else{
            return;
        }
    }
    button.selected = isSelect;
    if (isSelect) {
        [button setBackgroundColor:[UIColor whiteColor]];
        button.layer.borderWidth = 0.5;
        button.layer.borderColor = [UIColor redColor].CGColor;
    }else {
        [button setBackgroundColor:buttonBackgroundColor];
        button.layer.borderWidth = 0.0;
    }
}
- (void)setAttributeArr:(NSArray *)attributeArr
{
    _attributeArr = attributeArr;
    /** 九宫格布局算法 */
    CGFloat spacing = 5.0;//行、列 间距
    int totalloc = 3;//列数
    CGFloat appvieww = (offset - spacing*4)/totalloc;
    CGFloat appviewh = 30;
    int row = 0 ;
    for (int i=0; i< attributeArr.count; i++) {
        row = i/totalloc;//行号
        int loc = i%totalloc;//列号
        
        CGFloat appviewx = spacing + (spacing + appvieww) * loc;
        CGFloat appviewy = spacing + (spacing + appviewh) * row;
        
        UIButton *button = buttonArr[i];
        
        button.frame = CGRectMake(appviewx, appviewy, appvieww, appviewh);
        NSObject *obj = attributeArr[i];
        NSString *title = @"";
        BOOL isSelect = NO;
        if ([obj isKindOfClass:HBProp_valueModel.class]) {
            HBProp_valueModel *model = (HBProp_valueModel*) obj;
            title = model.prop_value;
            isSelect = model.isSelect;
        }else if ([obj isKindOfClass:HBScreeingBrandModel.class]) {
            HBScreeingBrandModel *model = (HBScreeingBrandModel*) obj;
            title = model.brand_name;
            isSelect = model.isSelect;
        }else if ([obj isKindOfClass:HBScreeingCatModel.class]) {
            HBScreeingCatModel *model = (HBScreeingCatModel*) obj;
            title = model.cat_name;
            isSelect = model.isSelect;
        }
        [button setTitle:title forState:UIControlStateNormal];
        if (isSelect) {
            button.selected = YES;
            [button setBackgroundColor:[UIColor whiteColor]];
            button.layer.borderWidth = 0.5;
            button.layer.borderColor = [UIColor redColor].CGColor;
            
        }else {
            button.selected = NO;
            [button setBackgroundColor:buttonBackgroundColor];
            button.layer.borderWidth = 0.0;
        }
        
        if (self.isShrinkage) {
            button.hidden = NO;
        }else {
            if (i > 2) {
                button.hidden = YES;
            }
        }
    }
    if (self.isShrinkage) {
        _height = (spacing + appviewh) * (row + 1) + spacing;
    }else {
        _height = 40;
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
