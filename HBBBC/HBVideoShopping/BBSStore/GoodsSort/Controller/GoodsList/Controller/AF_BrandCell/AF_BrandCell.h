//
//  AF_BrandCell.h
//  差五个让步
//
//  Created by Elephant on 16/5/4.
//  Copyright © 2016年 Elephant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBScreeingModel.h"
@protocol AF_BrandCellDelegate <NSObject>

- (void)selectedValueChangeBlock:(NSInteger)section andIndex:(NSInteger)index andIsShow:(BOOL)isShow;

@end

@interface AF_BrandCell : UITableViewCell

@property (strong, nonatomic) NSIndexPath *indexPath;

@property (assign, nonatomic) id<AF_BrandCellDelegate> delegate;

@property (strong, nonatomic) NSArray *attributeArr;

@property (strong, nonatomic) NSMutableArray *selectedArr;

@property (assign, nonatomic) CGFloat height;

@property (assign, nonatomic) BOOL isShrinkage;

/** cell 的类方法   返回 cell 本身  */
+ (instancetype) cellWithTableView: (UITableView *)tableView dataArr:(NSArray*)arr indexPath:(NSIndexPath *)indexPath;
@end
