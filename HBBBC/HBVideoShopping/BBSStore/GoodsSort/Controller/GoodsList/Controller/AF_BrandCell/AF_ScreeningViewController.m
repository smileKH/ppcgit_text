//
//  AF_ScreeningViewController.m
//  差五个让步
//
//  Created by Elephant on 16/5/4.
//  Copyright © 2016年 Elephant. All rights reserved.
//

#import "AF_ScreeningViewController.h"

#import "AXPriceRangeCell.h"
#import "AF_BrandCell.h"



@interface AF_ScreeningViewController () <UITableViewDataSource, UITableViewDelegate, AF_BrandCellDelegate>
{
    AXPriceRangeCell *priceRangeCell;
}
/** 重置按钮 */
@property (strong, nonatomic) UIButton * resetBut;
/** 确定按钮 */
@property (strong, nonatomic) UIButton * determineBut;
/** 展示tableView */
@property (strong, nonatomic) UITableView *tableV;
///** 选项标题数组 */
//@property (strong, nonatomic) NSMutableArray *headerTitArr;
///** 选项数据数组 */
//@property (strong, nonatomic) NSMutableArray *dataArr;
///** 是否展开状态数组 */
//@property (strong, nonatomic) NSMutableArray *shrinkArr;
/** 是否选中状态字典 */
@property (strong, nonatomic) NSMutableDictionary *selectedDict;
@property (nonatomic, strong) NSMutableArray * selecteArray;
@property (nonatomic, strong) NSMutableDictionary * selectIDsDic;

//@property (nonatomic, strong) HBScreeingModel * model;
@property (nonatomic,assign)int brand;
@property (nonatomic,assign)int cat;
@property (nonatomic,assign)BOOL brandShow;
@property (nonatomic,assign)BOOL catShow;
@end

@implementation AF_ScreeningViewController
-(NSMutableArray *)selecteArray{
    if (!_selecteArray) {
        _selecteArray = [NSMutableArray array];
    }
    return _selecteArray;
}
-(NSMutableDictionary *)selectIDsDic{
    if (!_selectIDsDic) {
        _selectIDsDic = [NSMutableDictionary dictionary];
    }
    return _selectIDsDic;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.selecteArray = [NSMutableArray array];
    [self.resetBut addTarget:self action:@selector(resetButClick) forControlEvents:UIControlEventTouchUpInside];
    [self.determineBut addTarget:self action:@selector(determineButClick) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setModel:(HBScreeingModel *)model{
    _model = model;
    
    if (self.model.brand.count) {
        _brand = 1;
    }
    if (self.model.cat.count) {
        for (HBScreeingCatModel *catModel in self.model.cat) {
            if ([_cat_id isEqualToString:catModel.cat_id]) {
                catModel.isSelect = YES;
                catModel.isNotUnSelect = YES;
            }
        }
       _cat = 1;
    }
    [self.tableV reloadData];
}
#pragma mark - 重置按钮点击事件
//重置
- (void)resetButClick{
    //重置数组的值
    [self resetSelSelect_IDS];
    //清空数据
    USERDEFAULT_SET_value(@"", kLoginModelName);
}
//确定
- (void)determineButClick{
    [self collectSelect_IDS];
//    NSMutableArray *arr = [NSMutableArray array];
//
//    for (HBProp_valueModel *model in self.selecteArray) {
//        [arr addObject:model.prop_index];
//    }
//    NSString *str = [arr componentsJoinedByString:@","];
//    NSString *endStr = str;
//    NSString *value = USERDEFAULT_value(kLoginModelName);
//    if (![HBHuTool isJudgeString:value]) {
//        endStr = [NSString stringWithFormat:@"%@,%@",str,value];
//    }
//    if (![HBHuTool isJudgeString:endStr]) {
//        //有数据
//        USERDEFAULT_SET_value(endStr, kLoginModelName);
//    }
//    //返回 去搜索吧
    self.clickConfBtn(self.selecteArray,self.selectIDsDic);
}

#pragma mark - tableView UITableViewDataSource, UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2 + self.model.props.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!section) {
        if (!_brand) {
            return 0.01;
        }
    }else if (section == 1){
        if (!_cat) {
            return 0.01;
        }
    }
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * dataArr = @[];
    BOOL show = NO;
    if (!indexPath.section) {
        dataArr = self.model.brand;
        show = _brandShow;
    }else if (indexPath.section==1) {
        dataArr = self.model.cat;
        show = _catShow;
    }else{
        
        HBScreeingPropsModel *model = self.model.props[indexPath.section-2];
        show = model.isShow;
        dataArr = model.prop_value;
    }
    AF_BrandCell *cell = [AF_BrandCell cellWithTableView:tableView dataArr:dataArr indexPath:indexPath];

    cell.delegate = self;

    cell.isShrinkage = show;

    cell.attributeArr = dataArr;
    
    return cell;
    
}

#pragma mark - AF_BrandCellDelegate
/** 取得选中选项的值，改变选项状态，刷新列表 */
- (void)selectedValueChangeBlock:(NSInteger)section andIndex:(NSInteger)index andIsShow:(BOOL)isShow
{
   HBScreeingPropsModel *model =  self.model.props[section];
    //这里把传过来的标记一下 返回刷新列表
    HBProp_valueModel *proModel = model.prop_value[index];
    proModel.isSelect = isShow;
    if (isShow) {
        [self.selecteArray addObject:proModel];
    }
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:section];
    [self.tableV reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)collectSelect_IDS{
    [self.selecteArray removeAllObjects];
    [self.selectIDsDic removeAllObjects];
    NSString *brandIDS=@"";
    for (HBScreeingBrandModel *brandModel in self.model.brand) {
        if (brandModel.isSelect) {
            [self.selecteArray addObject:brandModel];
            if (!brandIDS.length) {
                brandIDS = brandModel.brand_id;
            }else{
                brandIDS = [NSString stringWithFormat:@"%@,%@",brandIDS,brandModel.brand_id];
            }
        }
    }
    [self.selectIDsDic setValue:brandIDS forKey:@"brand_id"];
    
    NSString *catIDS=@"";
    
    for (HBScreeingCatModel *catModel in self.model.cat) {
        if (catModel.isSelect) {
            [self.selecteArray addObject:catModel];
            if (!catIDS.length) {
                catIDS = catModel.cat_id;
            }else{
                catIDS = [NSString stringWithFormat:@"%@,%@",catIDS,catModel.cat_id];
            }
        }
    }
    [self.selectIDsDic setValue:catIDS forKey:@"cat_id"];
    NSString *propIDS=@"";
    for (HBScreeingPropsModel *propsModel in self.model.props) {
        
        for (HBProp_valueModel *proValueModel in propsModel.prop_value) {
            
            if (proValueModel.isSelect) {
                [self.selecteArray addObject:proValueModel];
                if (!propIDS.length) {
                    propIDS = propsModel.prop_id;
                }else{
                    propIDS = [NSString stringWithFormat:@"%@,%@",propIDS,propsModel.prop_id];
                }
                propIDS = [NSString stringWithFormat:@"%@_%@",propIDS,proValueModel.prop_value_id];
            }
        }
        
    }
    [self.selectIDsDic setValue:propIDS forKey:@"prop_id"];
    
}
-(void)resetSelSelect_IDS{

    for (HBScreeingBrandModel *brandModel in self.model.brand) {
        if (brandModel.isSelect) {
            brandModel.isSelect = NO;
        }
    }
    for (HBScreeingCatModel *catModel in self.model.cat) {
        if (catModel.isSelect && !catModel.isNotUnSelect ) {
            catModel.isSelect = NO;
        }
    }
    for (HBScreeingPropsModel *propsModel in self.model.props) {
        for (HBProp_valueModel *proValueModel in propsModel.prop_value) {
            if (proValueModel.isSelect) {
                proValueModel.isSelect = NO;
            }
        }
     }
    [self.tableV reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * dataArr = @[];
    BOOL show = NO;
    if (!indexPath.section) {
        dataArr = self.model.brand;
        show = _brandShow;
    }else if (indexPath.section==1) {
        dataArr = self.model.cat;
        show = _catShow;
    }else{
        
        HBScreeingPropsModel *model = self.model.props[indexPath.section-2];
        show = model.isShow;
        dataArr = model.prop_value;
    }

    AF_BrandCell *cell = [[AF_BrandCell alloc]init];
    cell.isShrinkage = show;
    cell.attributeArr = dataArr;
    return cell.height;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString * title;
    BOOL show;
    float height = 0.01;
    if (!section) {
        title = @"品牌";
        show = _brandShow;
        if (_brand) {
            height = 40;
        }
    }else  if (section == 1) {
        title = @"类目";
        show = _catShow;
        if (_cat) {
            height = 40;
        }
    }else{
        HBScreeingPropsModel *model = self.model.props[section-2];
        title = model.prop_name;
        show = model.isShow;
        height = 40;
    }
    return [self headerView:title tag:(int)section height:height show:show];
}
-(UIView*)headerView:(NSString*)title tag:(int)tag height:(float)height show:(BOOL)show{
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, GOODS_SCEEREN_WIDTH, height)];
    myView.clipsToBounds = YES;
    myView.backgroundColor = [UIColor whiteColor];
    myView.userInteractionEnabled = YES;
    UILabel *titLab = [[UILabel alloc]init];
    titLab.text = title;
    titLab.font = [UIFont systemFontOfSize:13.0];
    CGSize titSize = [titLab.text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0]}];
    titLab.textColor = [UIColor grayColor];
    titLab.frame = CGRectMake(5, 0, titSize.width, 40);
    [myView addSubview:titLab];
    UIImageView *shrinkImage = [[UIImageView alloc] initWithFrame:CGRectMake(GOODS_SCEEREN_WIDTH - 50, 0, 40, 40)];
    shrinkImage.contentMode = UIViewContentModeCenter;
    shrinkImage.image = [UIImage imageNamed:show?@"bbs_upDown":@"bbs_down"];
    [myView addSubview:shrinkImage];
    
    UIButton *shrinkBut = [[UIButton alloc]initWithFrame:myView.bounds];
    
    shrinkBut.tag = tag;
    
   [shrinkBut addTarget:self action:@selector(shrinkButClick:) forControlEvents:UIControlEventTouchUpInside];
    [myView addSubview:shrinkBut];
    return myView;
}
/** 改变选项收缩状态 */
- (void)shrinkButClick:(UIButton *)button
{
    if (button.tag == 0) {
        _brandShow = !_brandShow;
    }else if (button.tag == 1) {
        _catShow = !_catShow;
    }else{
        HBScreeingPropsModel * model = self.model.props[button.tag-2];
        model.isShow = !model.isShow;
    }
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:button.tag];
    
    [self.tableV reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark - 懒加载
-(UITableView *)tableV
{
    if (!_tableV) {
        float top = iPhoneX?44:20;
        float bottom = 49 + (iPhoneX?34:0);
        _tableV = [[UITableView alloc]initWithFrame:CGRectMake(0, top, self.width, [UIScreen mainScreen].bounds.size.height - top - bottom) style:UITableViewStyleGrouped];
        _tableV.backgroundColor = [UIColor whiteColor];
        /** 隐藏cell分割线 */
        [_tableV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tableV.delegate = self;
        _tableV.dataSource = self;
        
        
        [self.view addSubview:_tableV];
        UIView *spaceWidth = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width, top)];
        spaceWidth.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:spaceWidth];
    }
    return _tableV;
}

- (UIButton *)resetBut//重置
{
    if (!_resetBut) {
        float bottomHeight = iPhoneX?34:0;
        _resetBut = [[UIButton alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 49 - bottomHeight, self.width/2, 49)];
        [_resetBut setBackgroundColor:[UIColor whiteColor]];
        [_resetBut setTitle:@"重置" forState:UIControlStateNormal];
        [_resetBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_resetBut setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self.view addSubview:_resetBut];
        UIView *spaceWidth = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_resetBut.frame), self.width, bottomHeight)];
        spaceWidth.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:spaceWidth];
    }
    return _resetBut;
}
- (UIButton *)determineBut//确定
{
    if (!_determineBut) {
        float bottomHeight = iPhoneX?34:0;
        _determineBut = [[UIButton alloc]initWithFrame:CGRectMake(self.width/2, [UIScreen mainScreen].bounds.size.height - 49 - bottomHeight, self.width/2, 49)];
        [_determineBut setBackgroundColor:[UIColor redColor]];
        [_determineBut setTitle:@"确定" forState:UIControlStateNormal];
        [_determineBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_determineBut setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self.view addSubview:_determineBut];
    }
    return _determineBut;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
