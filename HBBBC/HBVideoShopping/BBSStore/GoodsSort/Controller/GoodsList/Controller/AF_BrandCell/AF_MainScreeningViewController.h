//
//  AF_MainScreeningViewController.h
//  差五个让步
//
//  Created by Elephant on 16/5/5.
//  Copyright © 2016年 Elephant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBScreeingModel.h"
@interface AF_MainScreeningViewController : UIViewController
@property (nonatomic, strong) NSString * cat_id;//类目id
@property (nonatomic ,strong)NSString *search_keywords;//搜索关键字
@property (nonatomic ,strong)HBScreeingModel *model;
@property (nonatomic, copy) void(^blockSelectCatID)(NSArray *selectModels,NSMutableDictionary *selectIDsDic);
@end
