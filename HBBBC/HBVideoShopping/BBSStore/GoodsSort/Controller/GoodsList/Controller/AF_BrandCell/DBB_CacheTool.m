//
//  DBB_CacheTool.m
//  DBBAssistant
//
//  Created by 钟小麦 on 2017/12/4.
//  Copyright © 2017年 fenger. All rights reserved.
//

#import "DBB_CacheTool.h"

@implementation DBB_CacheTool

+ (instancetype)shareCacheTool
{
    static id instance_;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance_ = [[self alloc] init];
        
    });
    return instance_;
}

/*********NSCoding方式缓存***********/
/**
 *  硬盘缓存
 */
- (void)cacheObjectInDisk:(id<NSCoding>)obj forFileName:(NSString *)fileName
{
    NSString *document = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject;
    
    [NSKeyedArchiver archiveRootObject:obj toFile:[document stringByAppendingPathComponent:fileName] ];
}

- (id)objectInDiskForFileName:(NSString *)fileName
{
    NSString *document = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject;
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[document stringByAppendingPathComponent:fileName]];
}

@end

