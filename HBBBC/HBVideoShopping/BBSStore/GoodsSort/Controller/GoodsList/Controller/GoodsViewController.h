//
//  GoodsViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/14.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsViewController : HBRootViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
- (IBAction)changeStyle:(UIButton *)sender;
- (IBAction)selectType:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *topview;


//筛选字段
@property (nonatomic, strong) NSString *item_id;//商品id，多个id用，隔开
@property (nonatomic, strong) NSString *shop_id ;//店铺id
@property (nonatomic, strong) NSString *search_shop_cat_id;//店铺搜索自有一级类目id
@property (nonatomic, strong) NSString *cat_id;//平台的商品类目id
@property (nonatomic, strong) NSString *brand_id;//平台的品牌id
@property (nonatomic, strong) NSString *search_keywords;// 商品相关关键字
@property (nonatomic, strong) NSString *is_selfshop;// 是否是自营 (1 自营)
@property (nonatomic, strong) NSString *orderBy;// modified_time desc,list_time desc    排序方式.商品的主要关键字排序
@property (nonatomic, strong) NSString *fields;//
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionTopConstraint;



@end
