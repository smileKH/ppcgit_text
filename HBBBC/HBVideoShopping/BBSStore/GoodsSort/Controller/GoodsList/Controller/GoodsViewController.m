//
//  GoodsViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/14.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "GoodsViewController.h"
#import "GoodsOtherStyleCell.h"
#import "GoodsDefaultStyleCell.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "UITextField+LeftView.h"
#import "AF_MainScreeningViewController.h"



#import "ProductFirstViewController.h"

#import "HBScreeingModel.h"
#import "GoodsListModel.h"

#import "HBGoodsPersonOtherStyleCell.h"
#import "HBGoodsPersonDefaultStyleCell.h"
#import "HBPersonDetailVC.h"

@interface GoodsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout,UITextFieldDelegate>
{
    BOOL selectType;//选择的样式   styleDefault=0   styleCollection=1
    CHTCollectionViewWaterfallLayout *layout;
    
    UITextField *searchTf;
    UIButton *navigationBtn;
    
    
    NSInteger page_no;
}
@property(nonatomic, strong) NSMutableArray *collectionarray;
@property (nonatomic, strong) NSString * prop_index;
@property (nonatomic, assign) BOOL  isPrice;
@property (nonatomic ,strong)HBScreeingModel *screeingModel;
@end

@implementation GoodsViewController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //清空数据
    USERDEFAULT_SET_value(@"", kLoginModelName);
}

- (void)requestGoodsList:(BOOL)isRefresh
{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (![HBHuTool isJudgeString:self.item_id]) {
        parameters[@"item_id"] = self.item_id;
    }
    if (![HBHuTool isJudgeString:self.shop_id]) {
        parameters[@"shop_id"] = self.shop_id;
    }
    if (![HBHuTool isJudgeString:self.search_shop_cat_id]) {
        parameters[@"search_shop_cat_id"] = self.search_shop_cat_id;
    }
    
    //判断cat_id和search_keywords
    if (![HBHuTool isJudgeString:self.cat_id]) {
        parameters[@"cat_id"] = self.cat_id;
    }
    if (![HBHuTool isJudgeString:self.search_keywords]) {
        parameters[@"search_keywords"] = self.search_keywords;
    }
    
    if (![HBHuTool isJudgeString:self.brand_id]) {
        parameters[@"brand_id"] = self.brand_id;
    }
    if (![HBHuTool isJudgeString:self.is_selfshop]) {
        parameters[@"is_selfshop"] = self.is_selfshop;
    }
    if (![HBHuTool isJudgeString:self.orderBy]) {
       parameters[@"orderBy"] = self.orderBy;
    }
    if (![HBHuTool isJudgeString:self.prop_index]) {
        parameters[@"prop_index"] = self.prop_index;
    }
    if (![HBHuTool isJudgeString:self.fields]) {
        parameters[@"fields"] = self.fields;
    }

    parameters[@"page_size"] = @(self.pageSize);
    parameters[@"page_no"] = @(self.pageNo);
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:GOODS_IETM_SEARCH params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            if (weakSelf.pageNo==1) {
                [self.collectionarray removeAllObjects];
            }
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"page_count"]intValue];
                NSArray *arr = [dic objectForKey:@"list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [GoodsListModel mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.collectionarray addObjectsFromArray:array];
                        //设置视图
                        [weakSelf setGoodsViewController];
                    }
                }
                //加载 没有更多数据
                if ((weakSelf.pageNo > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                    
                }
            }
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.collectionarray]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptNormal];
        }
        [weakSelf.collectionview reloadData];
        [weakSelf.collectionview.mj_footer endRefreshing];
        [weakSelf.collectionview.mj_header endRefreshing];
    }];
    
}
#pragma mark ==========设置是否隐藏数据==========
-(void)setGoodsViewController{
    if ([HBHuTool judgeArrayIsEmpty:self.collectionarray]) {
        GoodsListModel *model = self.collectionarray[0];
        if ([model.shop_type isEqualToString:@"other"]) {
            //隐藏
            self.topview.hidden = YES;
            navigationBtn.hidden = YES;
            self.collectionTopConstraint.constant = 0;
        }else{
            self.topview.hidden = NO;
            navigationBtn.hidden = NO;
            self.collectionTopConstraint.constant = 44;
        }
    }
    
}
#pragma mark ==========筛选数据==========
-(void)requestScreeningData{
    
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (![HBHuTool isJudgeString:self.cat_id]) {
        parameters[@"cat_id"] = self.cat_id;
    }
    if (![HBHuTool isJudgeString:self.search_keywords]){
        parameters[@"search_keywords"] = self.search_keywords;
    }
    [LBService post:GOODS_FILTER_ITEMS params:parameters completion:^(LBResponse *response) {
        if (response.succeed) {
            //
            weakSelf.screeingModel = [HBScreeingModel mj_objectWithKeyValues:response.result[@"data"]];
            //更新
//            [weakSelf updateDataTableList];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //商品列表
//    UISQURE
    [self setupCollectionview];
    //初始化数据
    [self initListData];
    
    //请求数据
    [self requestGoodsList:YES];
    
    //请求筛选数据
    [self requestScreeningData];
    WEAKSELF;
    //默认block方法：设置下拉刷新
    self.collectionview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestGoodsList:YES];
    }];

    //默认block方法：设置上拉加载更多
    self.collectionview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestGoodsList:NO];
    }];
    //配置导航栏视图
    [self setNavBar];
}
-(void)initListData{
    self.btn1.selected = YES;
    page_no = 1;
//    self.item_id = @"";
//    self.shop_id = @"";
//    self.search_shop_cat_id = @"";
//    self.cat_id = @"";
    self.brand_id = @"";
//    self.search_keywords = @"";
    self.is_selfshop = @"";
    self.orderBy = @"default_weight";
    self.fields = @"price";
//    [self list];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)setNavBar
{
//    UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    titleBtn.frame = CGRectMake(0, 0, SCREEN_WIDTH-60, 30);
//    titleBtn.layer.masksToBounds = YES;
//    titleBtn.layer.cornerRadius = 2;
//    titleBtn.layer.borderWidth = 1;
//    [titleBtn setTitle:@"寻找感兴趣的商品" forState:UIControlStateNormal];
//    titleBtn.layer.borderColor = UIColorFromHex(0xAFAFAF).CGColor;
//    titleBtn.titleLabel.font = [UIFont systemFontOfSize:14];
//    [titleBtn setTitleColor:UIColorFromHex(0xAFAFAF) forState:UIControlStateNormal];
//    self.navigationItem.titleView = titleBtn;
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-40, 30)];
    textfield.placeholder = @"寻找感兴趣的商品";
    textfield.delegate = self;
    textfield.returnKeyType = UIReturnKeySearch;
    textfield.backgroundColor = white_Color;
    textfield.font = [UIFont systemFontOfSize:14];
    [textfield setLeftSearchViewWithImageName:@"bbs_home_search"];
    [textfield setBorderColor:UIColorFromHex(0xAFAFAF)];
    textfield.borderStyle = UITextBorderStyleRoundedRect;
    self.navigationItem.titleView = textfield;
    
    navigationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [navigationBtn setTitle:@"筛选" forState:UIControlStateNormal];
    [navigationBtn setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
    [navigationBtn addTarget:self action:@selector(chooseAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:navigationBtn];

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}
//textfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textField-------%@",textField.text);
    self.search_keywords = textField.text;
    //重新请求数据
    [self requestGoodsList:YES];
}
- (void)chooseAction{
    AF_MainScreeningViewController * testVC = [AF_MainScreeningViewController new];
    //这两句必须有
    self.definesPresentationContext = YES; //self is presenting view controller
    testVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    if (![HBHuTool isJudgeString:self.cat_id]) {
        testVC.cat_id = self.cat_id;
    }else{
        testVC.search_keywords = self.search_keywords;
    }
    testVC.model = self.screeingModel;
    
    WEAKSELF;
    testVC.blockSelectCatID = ^(NSArray *selectModels,NSMutableDictionary *selectIDsDic) {
        // brand_id cat_id prop_id
        weakSelf.prop_index = selectIDsDic[@"prop_id"];
        weakSelf.cat_id = selectIDsDic[@"cat_id"];
        weakSelf.brand_id = selectIDsDic[@"brand_id"];
        
//        if ([HBHuTool isJudgeString:catIdStr]) {
//            //为空
//            weakSelf.prop_index = @"";
//        }else{
//            weakSelf.prop_index = catIdStr;
//
//        }
        [weakSelf requestGoodsList:YES];
    };
    /** 设置半透明度 */
    testVC.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.5];
    [self.navigationController presentViewController:testVC animated:NO completion:nil];
    
}
-(void)setupCollectionview
{
    self.collectionarray = [NSMutableArray new];
    
    layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.columnCount = 2;
    layout.headerHeight =0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 0, 5);
    
    self.collectionview.collectionViewLayout = layout;
    
    
    [self.collectionview registerNib:[UINib nibWithNibName:@"GoodsDefaultStyleCell" bundle:nil] forCellWithReuseIdentifier:@"default"];
    [self.collectionview registerNib:[UINib nibWithNibName:@"GoodsOtherStyleCell" bundle:nil] forCellWithReuseIdentifier:@"other"];
    [self.collectionview registerNib:[UINib nibWithNibName:@"HBGoodsPersonOtherStyleCell" bundle:nil] forCellWithReuseIdentifier:@"personOther"];
    [self.collectionview registerNib:[UINib nibWithNibName:@"HBGoodsPersonDefaultStyleCell" bundle:nil] forCellWithReuseIdentifier:@"personDefault"];
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  [self.collectionarray count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectType) {
        GoodsListModel *model = self.collectionarray[indexPath.row];
        if ([model.shop_type isEqualToString:@"other"]) {
            //个人名片
            HBGoodsPersonOtherStyleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"personOther" forIndexPath:indexPath];
            cell.model = self.collectionarray[indexPath.row];
            return cell;
        }else{
            GoodsOtherStyleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"other" forIndexPath:indexPath];
            cell.model = self.collectionarray[indexPath.row];
            return cell;
        }
        
    }else{
        GoodsListModel *model = self.collectionarray[indexPath.row];
        if ([model.shop_type isEqualToString:@"other"]) {
            //个人名片
            HBGoodsPersonDefaultStyleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"personDefault" forIndexPath:indexPath];
            cell.model = self.collectionarray[indexPath.row];
            return cell;
        }else{
            GoodsDefaultStyleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"default" forIndexPath:indexPath];
            cell.model = self.collectionarray[indexPath.row];
            return cell;
        }
        
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!selectType) {
        return CGSizeMake(SCREEN_WIDTH/2, 240);
    }
    return CGSizeMake(SCREEN_WIDTH, 100);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    GoodsListModel *model = [self.collectionarray objectAtIndex:indexPath.row];
    if ([model.shop_type isEqualToString:@"other"]) {
        //人员类详情
        HBPersonDetailVC *vc= [[HBPersonDetailVC alloc] initWithNibName:@"HBPersonDetailVC" bundle:nil];
        vc.item_id = model.item_id;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //商品详情
        ProductFirstViewController *vc= [[ProductFirstViewController alloc] initWithNibName:@"ProductFirstViewController" bundle:nil];
        vc.item_id = model.item_id;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
}
- (IBAction)changeStyle:(UIButton *)sender {
    selectType = !sender.selected;
    sender.selected = !sender.selected;
    layout.columnCount = selectType?1:2;
    layout.minimumColumnSpacing = selectType?0.1:5;
    layout.minimumInteritemSpacing = selectType?0.1:5;
    
    layout.sectionInset = selectType?UIEdgeInsetsMake(0, 0, 0, 0):UIEdgeInsetsMake(5, 5, 0, 5);
    [self.collectionview reloadData];
}

- (IBAction)selectType:(UIButton *)sender {
    
    for (NSInteger i = 1; i < 5; i ++) {
        UIButton *btns = [self.topview viewWithTag:i];
        btns.selected = NO;
    }
    sender.selected = YES;
    if  (sender.tag == 1){
        self.orderBy = @"default_weight";
    }else if (sender.tag == 2){
        self.orderBy = @"sold_quantity";
    }else if (sender.tag == 3){
        self.isPrice = !self.isPrice;
        if (self.isPrice) {
            self.orderBy = @"price desc";
        }else{
            self.orderBy = @"price asc";
        }
        
    }else{
       self.orderBy = @"modified_time";
    }
//    [self.collectionarray removeAllObjects];
//    [self.collectionview reloadData];
    self.prop_index = @"";
    [self requestGoodsList:YES];
}
@end
