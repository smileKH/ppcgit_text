//
//  CategryViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/4.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHTCollectionViewWaterfallLayout.h"
@interface CategryViewController : HBRootViewController<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic)  IBOutlet UICollectionView *collectionview;

@property (nonatomic, strong)  NSArray *scrollarray;

@property (nonatomic, strong)  NSArray *collectionarray;

@end
