//
//  ShoppingCarViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/4.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "CategryViewController.h"
#import "CategryCell.h"
#import "GoodsViewController.h"
#import "UITextField+LeftView.h"


#import "CategaryHeaderView.h"

#import "CategryModel.h"
@interface CategryViewController ()<UITextFieldDelegate>
@property (nonatomic ,strong)NSString *textFieldStr;
@property (nonatomic ,strong)NSString *keyboreType;
@property (nonatomic ,strong)NSString *selectIndex;
@end

@implementation CategryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
    self.selectIndex = @"0";
    [self setupcollectionview];
    [self list];
    
    //设置
    self.keyboreType = @"1";
    //接受通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeHomeNotification:) name:@"HBBCManageHomeChange" object:nil];
}
#pragma mark ==========接受通知==========
-(void)changeHomeNotification:(NSNotification *)nofi{
    NSDictionary * infoDic = [nofi object];
    self.selectIndex = infoDic[@"selectIndex"];
    
    if (self.scrollarray.count>0) {
        if (self.scrollview) {
            [self.scrollview removeAllSubviews];
        }
        
        NSInteger selecBBIndex = [self.selectIndex integerValue];
        if (selecBBIndex>0) {
            selecBBIndex -=2;
        }
        CGFloat w = SCREEN_WIDTH*0.3;
        
        for (NSInteger i = 0; i < self.scrollarray.count ; i ++) {
            CategryModel *model = self.scrollarray[i];
            UIButton *btns = [UIButton buttonWithType:UIButtonTypeCustom];
            btns.frame=CGRectMake(0, 0+ i *(w/2+1), w, w/2);
            btns.tag = i + 10;
            
            [btns setTitle:model.cat_name forState:UIControlStateNormal];
            [btns setTitle:model.cat_name forState:UIControlStateSelected];
            btns.titleLabel.font = [UIFont systemFontOfSize:14];
            [btns setTitleColor:MAINTextCOLOR forState:UIControlStateSelected];
            [btns setTitleColor:black_Color forState:UIControlStateNormal];
            [btns addTarget:self action:@selector(leftSelect:) forControlEvents:UIControlEventTouchUpInside];
            btns.backgroundColor = clear_Color;
            if (i == selecBBIndex) {
                btns.selected = YES;
                btns.backgroundColor = white_Color;
            }
            [self.scrollview addSubview:btns];
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, i *(w/2+1), w, 1)];
            line.backgroundColor = UIColorFromHex(0xf0f0f0);
            [self.scrollview addSubview:line];
        }
        
        self.scrollview.contentSize = CGSizeMake(w, w/2*self.scrollarray.count);
        CategryModel *model = self.scrollarray[selecBBIndex];
        self.collectionarray = model.lv2;
        [self.collectionview reloadData];
    }else{
       [self list];
    }
    
    
}
- (void)list{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:CATEGORY_ITEM_CATEGORY params:@{} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            //NSDictionary *jsonDict = [JsonTool dataToObject:data];
            NSDictionary *modelDict = response.result[@"data"];
            CategorysModel *category = [CategorysModel mj_objectWithKeyValues:modelDict];
            
            weakSelf.scrollarray = category.categorys;
            [weakSelf setupscrollview];
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupNavBar];
}
-(void)setupNavBar
{

//    UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    titleBtn.frame = CGRectMake(0, 0, SCREEN_WIDTH-60, 30);
//    titleBtn.layer.masksToBounds = YES;
//    titleBtn.layer.cornerRadius = 2;
//    titleBtn.layer.borderWidth =1;
//    [titleBtn setTitle:@"寻找感兴趣的商品" forState:UIControlStateNormal];
//    titleBtn.layer.borderColor = UIColorFromHex(0xAFAFAF).CGColor;
//    titleBtn.titleLabel.font = [UIFont systemFontOfSize:14];
//    [titleBtn setTitleColor:UIColorFromHex(0xAFAFAF) forState:UIControlStateNormal];
//
//    self.navigationItem.titleView = titleBtn;
    
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-40, 30)];
    textfield.placeholder = @"寻找感兴趣的商品";
    textfield.delegate = self;
    textfield.backgroundColor = white_Color;
    textfield.returnKeyType = UIReturnKeySearch;
    textfield.clearButtonMode = UITextFieldViewModeWhileEditing;
    textfield.font = [UIFont systemFontOfSize:14];
    [textfield setLeftSearchViewWithImageName:@"bbs_home_search"];
    [textfield setBorderColor:UIColorFromHex(0xAFAFAF)];
    textfield.borderStyle = UITextBorderStyleRoundedRect;
    self.navigationItem.titleView = textfield;

}
//textfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textField-------%@",textField.text);
    //设置
    self.keyboreType = @"1";
    self.textFieldStr = textField.text;
    if (![HBHuTool isJudgeString:textField.text]) {
        GoodsViewController *vc = [[GoodsViewController alloc]initWithNibName:@"GoodsViewController" bundle:nil];
        vc.search_keywords = textField.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textField4 - 开始编辑");
    //设置
    self.keyboreType = @"2";
}// became first responder
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (![HBHuTool isJudgeString:textField.text]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (void)setupcollectionview
{

    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.columnCount = 3;
    layout.headerHeight = 40;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 1;
    layout.minimumInteritemSpacing = 1;

    self.collectionview.collectionViewLayout = layout;

    [self.collectionview registerNib:[UINib nibWithNibName:@"CategryCell" bundle:nil] forCellWithReuseIdentifier:@"item"];
    
    [self.collectionview registerNib:[UINib nibWithNibName:@"CategaryHeaderView" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:@"header"];

}

- (void)setupscrollview
{
    if (self.scrollview) {
        [self.scrollview removeAllSubviews];
    }

    NSInteger selecBBIndex = [self.selectIndex integerValue];
    if (selecBBIndex>0) {
        selecBBIndex -=2;
    }
    
    CGFloat w = SCREEN_WIDTH*0.3;
    
    for (NSInteger i = 0; i < self.scrollarray.count ; i ++) {
        CategryModel *model = self.scrollarray[i];
        UIButton *btns = [UIButton buttonWithType:UIButtonTypeCustom];
        btns.frame=CGRectMake(0, 0+ i *(w/2+1), w, w/2);
        btns.tag = i + 10;
        
        [btns setTitle:model.cat_name forState:UIControlStateNormal];
        [btns setTitle:model.cat_name forState:UIControlStateSelected];
        btns.titleLabel.font = [UIFont systemFontOfSize:14];
        [btns setTitleColor:MAINTextCOLOR forState:UIControlStateSelected];
        [btns setTitleColor:black_Color forState:UIControlStateNormal];
        [btns addTarget:self action:@selector(leftSelect:) forControlEvents:UIControlEventTouchUpInside];
        btns.backgroundColor = clear_Color;
        if (i == selecBBIndex) {
            btns.selected = YES;
            btns.backgroundColor = white_Color;
        }
        [self.scrollview addSubview:btns];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, i *(w/2+1), w, 1)];
        line.backgroundColor = UIColorFromHex(0xf0f0f0);
        [self.scrollview addSubview:line];
    }
    
    self.scrollview.contentSize = CGSizeMake(w, w/2*self.scrollarray.count);
    CategryModel *model = self.scrollarray[selecBBIndex];
    self.collectionarray = model.lv2;
    [self.collectionview reloadData];
    
}

- (void)leftSelect:(UIButton *)sender
{
    
    for (NSInteger i = 0 ; i <self.scrollarray.count; i++) {
        UIButton *btns = [self. scrollview viewWithTag:i+10];
        btns.backgroundColor = clear_Color;
        btns.selected = NO;
    }
    sender.selected = YES;
    sender.backgroundColor = white_Color;
    CategryModel *model = self.scrollarray[sender.tag-10];
    self.collectionarray = model.lv2;
    [self.collectionview reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark ---- colllectionveiw delegate datesource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    CategrySecondModel *model = self.collectionarray[section];
    
    return model.lv3.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.collectionarray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategryCell *item = [collectionView dequeueReusableCellWithReuseIdentifier:@"item" forIndexPath:indexPath];
    CategrySecondModel *model = self.collectionarray[indexPath.section];
    item.model = model.lv3[indexPath.row];
    return item;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.keyboreType isEqualToString:@"1"]) {
        //搜索有数据 搜索跳
        CategrySecondModel *model = self.collectionarray[indexPath.section];
        CategryThirdModel *model3 = model.lv3[indexPath.row];
        GoodsViewController *vc = [[GoodsViewController alloc] initWithNibName:@"GoodsViewController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        vc.cat_id = model3.cat_id;
        vc.brand_id = model3.parent_id;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
    }
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionview.bounds.size.width/3, self.collectionview.bounds.size.width/3 *2);
}


-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString: CHTCollectionElementKindSectionHeader]) {

        CategaryHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"header" forIndexPath:indexPath];
        if (!header) {
            header = [[NSBundle mainBundle] loadNibNamed:@"CategaryHeaderView" owner:nil options:nil].firstObject;
        }
        CategrySecondModel *model = self.collectionarray[indexPath.section];
        header.titleLabel.text = model.cat_name;
        return header;
        
    }
    return nil;
}



@end

