//
//  CategryModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/2/2.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CategrySecondModel;
@class CategryThirdModel;
@interface CategryModel : NSObject
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *parent_id;
@property (nonatomic, strong) NSString *cat_name;
@property (nonatomic, strong) NSString *cat_logo;
@property (nonatomic, strong) NSString *cat_path;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *is_leaf;
@property (nonatomic, strong) NSString *child_count;
@property (nonatomic, strong) NSString *order_sort;
@property (nonatomic, strong) NSArray  *lv2;
@end

@interface CategrySecondModel:NSObject
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *parent_id;
@property (nonatomic, strong) NSString *cat_name;
@property (nonatomic, strong) NSString *cat_logo;
@property (nonatomic, strong) NSString *cat_path;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *is_leaf;
@property (nonatomic, strong) NSString *child_count;
@property (nonatomic, strong) NSString *order_sort;
@property (nonatomic, strong) NSArray  *lv3;
@end



@interface CategryThirdModel:NSObject
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *parent_id;
@property (nonatomic, strong) NSString *cat_name;
@property (nonatomic, strong) NSString *cat_logo;
@property (nonatomic, strong) NSString *cat_path;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *is_leaf;
@property (nonatomic, strong) NSString *child_count;
@property (nonatomic, strong) NSString *order_sort;
@end

@interface  CategorysModel:NSObject

@property(nonatomic, strong) NSArray *categorys;
@end



