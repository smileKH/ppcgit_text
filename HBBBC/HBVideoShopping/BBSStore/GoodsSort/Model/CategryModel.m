//
//  CategryModel.m
//  BBSStore
//
//  Created by 马云龙 on 2018/2/2.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "CategryModel.h"

@implementation CategryModel
-(void)setLv2:(NSArray *)lv2
{
    _lv2 = [CategrySecondModel mj_objectArrayWithKeyValuesArray:lv2];
}
@end

@implementation CategrySecondModel
-(void)setLv3:(NSArray *)lv3
{
    _lv3 = [CategryThirdModel mj_objectArrayWithKeyValuesArray:lv3];
}
@end

@implementation CategryThirdModel

@end

@implementation CategorysModel
-(void)setCategorys:(NSArray *)categorys
{
    _categorys = [CategryModel mj_objectArrayWithKeyValuesArray:categorys];
}

@end
