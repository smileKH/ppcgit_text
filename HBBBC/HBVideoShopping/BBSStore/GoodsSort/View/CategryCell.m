//
//  CategryCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/5.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "CategryCell.h"

@implementation CategryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(CategryThirdModel *)model
{
    NSString *urlString=[NSString stringWithFormat:@"%@%@",Config_imageUrl,model.cat_logo];
    [self.image sd_setImageWithURL:[NSURL URLWithString:urlString]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    self.title.text = model.cat_name;
}
@end
