//
//  CategryCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/5.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategryModel.h"
@interface CategryCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (nonatomic, strong) CategryThirdModel *model;
@end
