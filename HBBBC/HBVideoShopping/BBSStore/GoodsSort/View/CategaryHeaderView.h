//
//  CategaryHeaderView.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/24.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategaryHeaderView : UICollectionReusableView
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@end
