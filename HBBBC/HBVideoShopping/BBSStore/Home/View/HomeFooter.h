//
//  HomeFooter.h
//  BBSStore
//
//  Created by 马云龙 on 2018/2/25.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeFooter : UICollectionReusableView
@property (nonatomic, strong) IBOutlet UIImageView *imageview;
- (IBAction)clickFooterBtn:(UIButton *)sender;
@property (nonatomic, copy) void(^clickFooterView)(id obj);
@end
