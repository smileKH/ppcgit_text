//
//  UIView+Border.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "UIView+Border.h"

@implementation UIView (Border)
-(void)setBorderColor:(UIColor *)borderColor
{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 2;
//    self.layer.borderColor = borderColor.CGColor;
//    self.layer.borderWidth =1.0f;
}
@end
