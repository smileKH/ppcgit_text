//
//  UITextField+LeftView.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (LeftView)
//设置输入框 左侧视图 的搜索
-(void)setLeftSearchViewWithImageName:(NSString *)imageName;
@end
