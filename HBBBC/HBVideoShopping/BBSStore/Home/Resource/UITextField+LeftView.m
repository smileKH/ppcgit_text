//
//  UITextField+LeftView.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "UITextField+LeftView.h"

@implementation UITextField (LeftView)
-(void)setLeftSearchViewWithImageName:(NSString *)imageName
{
    self.leftViewMode = UITextFieldViewModeAlways;
    UIImageView *imagev = [[UIImageView alloc] init];
    imagev.frame = CGRectMake(5, 5, 20, 20);
    imagev.image = [UIImage imageNamed:imageName];
    UIView *leftView = [[UIView alloc] init];//
    leftView.bounds =  CGRectMake(0, 0, 30, 30);
    [leftView addSubview:imagev];
    self.leftView = leftView;
}
@end
