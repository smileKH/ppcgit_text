//
//  HBbcManageHomeVC.h
//  HBVideoShopping
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HBbcManageHomeVC : HBRootViewController<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@end

NS_ASSUME_NONNULL_END
