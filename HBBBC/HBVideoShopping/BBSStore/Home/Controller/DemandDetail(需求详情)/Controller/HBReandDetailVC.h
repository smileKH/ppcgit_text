//
//  HBReandDetailVC.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2020/2/8.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import "JXPagerView.h"
#import "HBDemandListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HBReandDetailVC : HBRootViewController
@property (nonatomic, strong) JXPagerView *pagerView;
@property (nonatomic ,strong)UIScrollView *nestContentScrollView;
@property (nonatomic ,strong)HBDemandListModel *listModel;
@end

NS_ASSUME_NONNULL_END
