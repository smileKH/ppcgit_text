//
//  HBReandDetailVC.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2020/2/8.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBReandDetailVC.h"

#import "HBReandDetailHeaderView.h"
#import "HBReandDetailTabView.h"
#import "JXPagerListRefreshView.h"
#import "HBReandDetailModel.h"
#import "UIButton+ImageTitleSpacing.h"

#import "UIView+SDAutoLayout.h"
#import "LEETheme.h"
#import "GlobalDefines.h"
#import "UITableView+SDAutoTableViewCellHeight.h"
#import "PFAPP.h"

static CGFloat textFieldH = 50;
@interface HBReandDetailVC ()<JXCategoryViewDelegate,JXPagerViewDelegate, JXPagerMainTableViewGestureDelegate,UITextFieldDelegate>
{
    CGFloat categoryViewHeight;
    CGFloat JXTableHeaderViewHeight;
}

@property (nonatomic, assign) BOOL isNeedFooter;
@property (nonatomic, assign) BOOL isNeedHeader;
@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) NSArray <NSString *> *titles;
@property (nonatomic ,strong)HBReandDetailHeaderView *headerView;
@property (nonatomic ,strong)NSMutableArray *typeArr;//类型标题
@property (nonatomic ,strong)NSMutableArray *menuTitleArr;//标题转换
@property (nonatomic ,strong)NSMutableArray *topBannerArr;//banner图
@property (nonatomic ,strong)HBReandDetailModel *detailData;
@property (nonatomic ,strong)UIView *bottomView;
@property (nonatomic ,strong)UIView *bottomLineView;
@property (nonatomic, strong) NSMutableArray *btnArr;
@property (nonatomic, strong) UITextField *textField;//这个
@property (nonatomic ,strong)UIView *sendMsgView;//发送信息
@property (nonatomic ,strong)UIButton *locationButton;//开始定位按钮
@property (nonatomic ,assign)BOOL isCommit;//yes 评论 NO投诉
@end

@implementation HBReandDetailVC
{
    CGFloat _lastScrollViewOffsetY;
    CGFloat _totalKeybordHeight;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_textField resignFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"需求详情";
    self.btnArr = [NSMutableArray array];
    self.view.backgroundColor = white_Color;
    
    //设置初始数据
    [self setOriginValue];
    
    [self initDate];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-kBottomSafeHeight);
        make.height.mas_equalTo(40);
    }];
    [self.bottomView addSubview:self.bottomLineView];
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.bottomView);
        make.top.equalTo(self.bottomView);
        make.height.mas_equalTo(2);
    }];
    //添加子视图
    [self addBottomSubView];
    //请求数据
    [self requestSchoolBannerData];
    [self setupTextField];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
}
#pragma mark ==========请求数据==========
-(void)requestSchoolBannerData{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        parameters[@"page_size"] = @(10);
        parameters[@"page_no"] = @(self.pageNo);
        parameters[@"demand_id"] = self.listModel.demand_id;
        WEAKSELF;
        [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        [LBService post:DEMAND_DEMAND_QUERY params:parameters completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                NSDictionary *dic = response.result[@"data"];
                if (ValidDict(dic)) {
                    NSDictionary *pagDic = dic[@"pagers"];
                    int number = [pagDic[@"current_page"]intValue];
                    NSDictionary *demandDic = [dic objectForKey:@"demand"];
                    if (ValidDict(demandDic)) {
                        //数据转模型
                        weakSelf.detailData = [HBReandDetailModel mj_objectWithKeyValues:demandDic];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //正常有数据 隐藏无数据view
                            weakSelf.headerView.dataModel = weakSelf.detailData;
                            //设置数据
                            [weakSelf setReandDetailData];
                        });
                        
                    }
                    //加载 没有更多数据
                    if ((weakSelf.pageNo > number)&&(number>0)) {
                        [app showToastView:Text_NoMoreData];
                    }
                }
            }else{
                [app showToastView:response.message];
            }
        }];
}
#pragma mark ==========设置数据==========
-(void)setReandDetailData{
//    NSString *oneStr = [NSString stringWithFormat:@"%@",model.complaint]; //今日
//    NSString *twoStr = [NSString stringWithFormat:@"%@",model.favorite];//预计收入
//    NSString *threeStr = [NSString stringWithFormat:@"%@",model.comment];//本月
//    NSString *fourStr = [NSString stringWithFormat:@"%@",model.like];//累计
//    NSArray *titleArr = @[oneStr,twoStr,threeStr,fourStr];
    
    BOOL iscollect = [self.detailData.is_favorite boolValue];
    BOOL isLike = [self.detailData.is_like boolValue];
    NSArray *selectArr = @[@(NO),@(iscollect),@(NO),@(isLike)];
    
    for (UIButton *btn in _btnArr) {
        NSInteger index = [_btnArr indexOfObject:btn];
        BOOL isSelect = [[selectArr objectAtIndex:index] boolValue];
//        NSString *title = [titleArr objectAtIndex:index];
//        [btn setTitle:title forState:UIControlStateNormal];
        btn.selected = isSelect;
//        [btn layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleTop imageTitleSpace:5];
    }
}
#pragma mark ==========设置初始数据==========
-(void)setOriginValue{
    self.navigationController.navigationBar.translucent = false;
    self.typeArr = [NSMutableArray array];
    self.menuTitleArr = [NSMutableArray array];
    self.topBannerArr = [NSMutableArray array];
    JXTableHeaderViewHeight = 210+SCREEN_WIDTH;
    _headerView = [[HBReandDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, JXTableHeaderViewHeight)];
    _headerView.backgroundColor = red_Color;
    
}

#pragma mark ==========headerDelegate==========
-(void)goldMainHeaderView:(HBReandDetailHeaderView *)headerView updateHeaderHeight:(CGFloat)height{
    //    JXTableHeaderViewHeight = height;
    //    CGRect frame = self.headerView.frame;
    //    frame.size.height = JXTableHeaderViewHeight;
    //    [self.headerView setFrame:frame];
}
#pragma mark =======初始化滚动视图==========
- (void)initDate{
    categoryViewHeight = 42.0;
    
    self.titles = @[@"评论",@"投诉"];
    //设置滚动视图
    [self setupCategoryView];
}

- (void)setupCategoryView{
    
    //    CGFloat cellWidth = 60;
    //    CGFloat spacing = 20;
    //    CGFloat leftSpacing = 0;
    //    NSInteger count = self.menuTitleArr.count;
    //    if (count<4) {
    //        leftSpacing = (SCREEN_WIDTH-(count - 1)*spacing-count*cellWidth)/2;
    //    }
    self.categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, categoryViewHeight)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = Color_BG;
    self.categoryView.delegate = self;
    self.categoryView.titleSelectedColor = black_Color;
    self.categoryView.titleColor = black_Color;
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = YES;
    self.categoryView.titleFont = Font(13);
    
    
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = black_Color;
    lineView.indicatorWidth = 30;
    self.categoryView.indicators = @[lineView];
    
    _pagerView = [self preferredPagingView];
    self.pagerView.mainTableView.gestureDelegate = self;
    [self.view addSubview:self.pagerView];
    
    self.categoryView.contentScrollView = self.pagerView.listContainerView.collectionView;
    //导航栏隐藏的情况，处理扣边返回，下面的代码要加上
    //    [self.pagerView.listContainerView.collectionView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    //    [self.pagerView.mainTableView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}



- (JXPagerView *)preferredPagingView {
    return [[JXPagerListRefreshView alloc] initWithDelegate:self];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.pagerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-bNavAllHeight-kBottomSafeHeight-40);
}

#pragma mark - JXPagerViewDelegate

- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.headerView;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return JXTableHeaderViewHeight;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return categoryViewHeight;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSInteger)numberOfListsInPagerView:(JXPagerView *)pagerView {
    //和categoryView的item数量一致
    return self.titles.count;
}

- (id<JXPagerViewListViewDelegate>)pagerView:(JXPagerView *)pagerView initListAtIndex:(NSInteger)index {
    HBReandDetailTabView *listView = [[HBReandDetailTabView alloc] init];
    listView.emptyDataSetSource = self;
    listView.emptyDataSetDelegate = self;
    listView.listModel = self.listModel;
    listView.indexCurrend = index+1;
    listView.tableFooterView = [[UIView alloc]init];
    listView.backgroundColor = Color_BG;
    listView.upController = self;
    listView.isNeedHeader = YES;
    listView.isNeedFooter = YES;
    listView.isHeaderRefreshed = NO;
    [listView beginFirstRefresh];
    return listView;
}

-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _bottomView ;
}
-(UIView *)bottomLineView{
    if (!_bottomLineView) {
        _bottomLineView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = RGBA(204, 204, 204, 1);
            
            view ;
        }) ;
    }
    return _bottomLineView ;
}
#pragma mark ==========添加底部子视图==========
-(void)addBottomSubView{
//    NSArray *bottomTitleArr = @[@"0",@"0",@"0",@"0"];
    NSArray *bottomImageArr = @[@"bs_toushu_img",@"bs_shouchang_coll",@"bs_pinglun_img",@"bs_dianzan_no"];
    NSArray *bottomSelectImageArr = @[@"bs_toushu_img",@"bs_shouchang_collSelect",@"bs_pinglun_img",@"bs_dianzan_select"];
    NSMutableArray *btnArr = [NSMutableArray array];
    for (int i = 0; i<bottomImageArr.count; i++) {
//        NSString *title = [bottomTitleArr objectAtIndex:i];
        NSString *imageName = [bottomImageArr objectAtIndex:i];
        NSString *selectImageName = [bottomSelectImageArr objectAtIndex:i];
        UIButton *btn = [[UIButton alloc] init];
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:selectImageName] forState:UIControlStateSelected];
//        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:black_Color forState:UIControlStateNormal];
        btn.titleLabel.font = Font(10);
        [self.bottomView addSubview:btn];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//        [btn layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleTop imageTitleSpace:5];
        btn.tag = i;
        [btnArr addObject:btn];
        
    }
    _btnArr = btnArr;
    WEAKSELF;
    [btnArr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:10 tailSpacing:10];
    [btnArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.bottomView).offset(0);
        make.height.mas_equalTo(40);
    }];
}
- (void)btnClick:(UIButton *)button{
    if ([userManager judgeLoginState]) {
        button.selected = !button.selected;
        if (button.tag == 0) {
            //投诉 弹窗评论
            self.isCommit = NO;
            [self clickCommentButtonPushView];
        }else if (button.tag == 1){
            //收藏
            [self likeAndCollectBool:button.selected andLike:NO];
        }else if (button.tag == 2){
            //评论 弹出评论
            self.isCommit = YES;
            [self clickCommentButtonPushView];
        }else if (button.tag == 3){
            //点赞
            [self likeAndCollectBool:button.selected andLike:YES];
        }
    }
}
#pragma mark ==========评论内容==========
-(void)clickCommentButtonPushView{
    if ([userManager judgeLoginState]) {
       [_textField becomeFirstResponder];
    }
//    [self adjustTableViewToFitKeyboard];
}
#pragma mark ==========点击点赞和收藏==========
-(void)likeAndCollectBool:(BOOL)isSelect andLike:(BOOL)isLike{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"demand_id"] = self.listModel.demand_id;
    if (isLike) {
        //喜欢
        if (isSelect) {
            //喜欢
            parameters[@"like"] = @"1";
        }else{
            //不喜欢
            parameters[@"like"] = @"0";
        }
        
    }else{
        //收藏
        if (isSelect) {
            //收藏
            parameters[@"favorite"] = @"1";
        }else{
            //取消收藏
            parameters[@"favorite"] = @"0";
        }
    }
    
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:DEMAND_DEMAND_UPDATE params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //刷新列表
            [weakSelf requestSchoolBannerData];
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didClickedItemContentScrollViewTransitionToIndex:(NSInteger)index {
    //请务必实现该方法
    //因为底层触发列表加载是在代理方法：`- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath`回调里面
    //所以，如果当前有5个item，当前在第1个，用于点击了第5个。categoryView默认是通过设置contentOffset.x滚动到指定的位置，这个时候有个问题，就会触发中间2、3、4的cellForItemAtIndexPath方法。
    //如此一来就丧失了延迟加载的功能
    //所以，如果你想规避这样的情况发生，那么务必按照这里的方法处理滚动。
    [self.pagerView.listContainerView.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    //如果你想相邻的两个item切换时，通过有动画滚动实现。未相邻的两个item直接切换，可以用下面这段代码
    //
    //     NSInteger diffIndex = labs(categoryView.selectedIndex - index);
    //     if (diffIndex > 1) {
    //     [self.pagerView.listContainerView.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    //     }else {
    //     [self.pagerView.listContainerView.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    //     }
    
}

#pragma mark - JXPagerMainTableViewGestureDelegate

- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    //禁止categoryView左右滑动的时候，上下和左右都可以滚动
    if (otherGestureRecognizer.view == self.nestContentScrollView) {
        return NO;
    }
    //禁止categoryView左右滑动的时候，上下和左右都可以滚动
    if (otherGestureRecognizer == self.categoryView.collectionView.panGestureRecognizer) {
        return NO;
    }
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}
-(CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    return -100;
}
#pragma mark - 无数据代理方法 -
//点击按钮代理方法，可在对应的子类中重写这个方法
- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button{
    // Do something
    NSLog(@"刷新数据 didTapButton");
    [self.pagerView reloadData];
}


- (void)dealloc
{
    
    [_textField removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark ==========添加评论==========
- (void)setupTextField
{
    
    _sendMsgView = [UIView new];
    _sendMsgView.backgroundColor = RGBA(249, 249, 249, 1);
    _sendMsgView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, SCREEN_WIDTH, textFieldH);
    [[UIApplication sharedApplication].keyWindow addSubview:_sendMsgView];
    
    _textField = [UITextField new];
    _textField.returnKeyType = UIReturnKeySend;
    _textField.delegate = self;
    _textField.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.8].CGColor;
    _textField.layer.borderWidth = 1;
    _textField.layer.borderColor = RGBA(242, 242, 242, 1).CGColor;
    _textField.layer.cornerRadius = 18;
    _textField.backgroundColor = RGBA(242, 242, 242, 1);
    _textField.clipsToBounds = YES;
    _textField.placeholder = @"说些什么。。。";
    _textField.textColor = black_Color;
    _textField.font = Font(14);
    
    //为textfield添加背景颜色 字体颜色的设置 还有block设置 , 在block中改变它的键盘样式 (当然背景颜色和字体颜色也可以直接在block中写)
    
//    _textField.lee_theme
//    .LeeAddBackgroundColor(DAY , [UIColor whiteColor])
//    .LeeAddBackgroundColor(NIGHT , [UIColor blackColor])
//    .LeeAddTextColor(DAY , [UIColor blackColor])
//    .LeeAddTextColor(NIGHT , [UIColor grayColor])
//    .LeeAddCustomConfig(DAY , ^(UITextField *item){
//        
//        item.keyboardAppearance = UIKeyboardAppearanceDefault;
//        if ([item isFirstResponder]) {
//            [item resignFirstResponder];
//            [item becomeFirstResponder];
//        }
//    }).LeeAddCustomConfig(NIGHT , ^(UITextField *item){
//        
//        item.keyboardAppearance = UIKeyboardAppearanceDark;
//        if ([item isFirstResponder]) {
//            [item resignFirstResponder];
//            [item becomeFirstResponder];
//        }
//    });
    
    //    _textField.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, SCREEN_WIDTH, textFieldH);
    //    [[UIApplication sharedApplication].keyWindow addSubview:_textField];
    _textField.frame = CGRectMake(10, 7, SCREEN_WIDTH-120, textFieldH-14);
    [_sendMsgView addSubview:_textField];
    
    [_textField becomeFirstResponder];
    [_textField resignFirstResponder];
    
    //添加定位按钮
    _locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _locationButton.frame = CGRectMake(SCREEN_WIDTH-100, 7, 90, textFieldH-14);
    [_locationButton setTitle:@"去开始定位" forState:UIControlStateNormal];
    [_locationButton setTitle:@"已开启定位" forState:UIControlStateSelected];
    [_locationButton setTitleColor:RGBA(252, 119, 70, 1) forState:UIControlStateNormal];
    [_locationButton setTitleColor:RGBA(204, 204, 204, 1) forState:UIControlStateSelected];
    [_locationButton setBackgroundImage:[UIImage imageWithColor:RGBA(255, 241, 236, 1)] forState:UIControlStateNormal];
    [_locationButton setBackgroundImage:[UIImage imageWithColor:RGBA(241, 241, 241, 1)] forState:UIControlStateSelected];
    _locationButton.layer.cornerRadius = 18;
    _locationButton.clipsToBounds = YES;
    _locationButton.titleLabel.font = Font(14);
    [_locationButton addTarget:self action:@selector(clickLocationButton:) forControlEvents:UIControlEventTouchUpInside];
    [_sendMsgView addSubview:_locationButton];
}
#pragma mark ==========点击定位按钮==========
-(void)clickLocationButton:(UIButton *)button{
    //拿到定位位置
    button.selected = !button.selected;
}
//- (void)adjustTableViewToFitKeyboard
//{
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    UITableViewCell *cell = [self cellForRowAtIndexPath:_currentEditingIndexthPath];
//    CGRect rect = [cell.superview convertRect:cell.frame toView:window];
//    [self adjustTableViewToFitKeyboardWithRect:rect];
//}
//
//- (void)adjustTableViewToFitKeyboardWithRect:(CGRect)rect
//{
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    CGFloat delta = CGRectGetMaxY(rect) - (window.bounds.size.height - _totalKeybordHeight);
//
//    CGPoint offset = self.contentOffset;
//    offset.y += delta;
//    if (offset.y < 0) {
//        offset.y = 0;
//    }
//
//    [self setContentOffset:offset animated:YES];
//}

#pragma mark - UITextFieldDelegate
//接口完成的事情
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length) {
        
        [_textField resignFirstResponder];
        
        //请求接口
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        parameters[@"demand_id"] = self.detailData.demand_id;
        
        parameters[@"latitude"] = @([PFAPP sharedInstance].lat);
        parameters[@"longitude"] = @([PFAPP sharedInstance].lng);
        
        NSString *urlStr = nil;
        if (self.isCommit) {
            //评论
            urlStr = DEMAND_DEMAND_COMMENT_CREATE;
            parameters[@"comment_content"] = textField.text;
            parameters[@"comment_address"] = [PFAPP sharedInstance].address;
        }else{
            //投诉
            urlStr = DEMAND_DEMAND_COMPLAINT_CREATE;
            parameters[@"complaint_content"] = textField.text;
            parameters[@"complaint_address"] = [PFAPP sharedInstance].address;
        }
        WEAKSELF;
        [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        [LBService post:urlStr params:parameters completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //            [MBProgressHUD showInfoMessage:@"成功"];
                //刷新列表
                [weakSelf requestSchoolBannerData];
                //更新tableview
                [[NSNotificationCenter defaultCenter]postNotificationName:@"reandDetailPushStrSuccess" object:nil];
                //                NSMutableArray *temp = [NSMutableArray new];
                //                [temp addObjectsFromArray:model.child_comments];
                //                SDTimeLineCellCommentItemModel *commentItemModel = [SDTimeLineCellCommentItemModel new];
                //
                //                        if (self.isReplayingComment) {
                //                            commentItemModel.firstUserName = @"GSD_iOS";
                //                            commentItemModel.firstUserId = @"GSD_iOS";
                //                            commentItemModel.secondUserName = self.commentToUser;
                //                            commentItemModel.secondUserId = self.commentToUser;
                //                            commentItemModel.commentString = textField.text;
                //
                //                            self.isReplayingComment = NO;
                //                        } else {
                //                            commentItemModel.firstUserName = @"GSD_iOS";
                //                            commentItemModel.commentString = textField.text;
                //                            commentItemModel.firstUserId = @"GSD_iOS";
                //                        }
                //                [temp addObject:commentItemModel];
                //                model.child_comments = [temp copy];
                //                [weakSelf reloadRowsAtIndexPaths:@[_currentEditingIndexthPath] withRowAnimation:UITableViewRowAnimationNone];
            }else{
                [app showToastView:response.message];
            }
        }];
        
        
        
        
        _textField.text = @"";
        _textField.placeholder = nil;
        
        return YES;
    }
    return NO;
}



- (void)keyboardNotification:(NSNotification *)notification
{
    NSDictionary *dict = notification.userInfo;
    CGRect rect = [dict[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    
    
    
    CGRect textFieldRect = CGRectMake(0, rect.origin.y - textFieldH, rect.size.width, textFieldH);
    if (rect.origin.y == [UIScreen mainScreen].bounds.size.height) {
        textFieldRect = rect;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        _sendMsgView.frame = textFieldRect;
    }];
    
    CGFloat h = rect.size.height + textFieldH;
    if (_totalKeybordHeight != h) {
        _totalKeybordHeight = h;
//        [self adjustTableViewToFitKeyboard];
    }
}
@end
