//
//  HBReandDetailModel.h
//  HBVideoShopping
//
//  Created by Mac on 2020/2/12.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBReandDetailUserModel;
@class HBReandDetailImgListModel;

NS_ASSUME_NONNULL_BEGIN

@interface HBReandDetailModel : NSObject
@property (nonatomic ,strong)NSString *demand_id;
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *demand_status;
@property (nonatomic ,strong)NSString *demand_desc;
@property (nonatomic ,strong)NSString *latitude;
@property (nonatomic ,strong)NSString *longitude;
@property (nonatomic ,strong)NSString *demand_address;
@property (nonatomic ,strong)NSString *contact;
@property (nonatomic ,strong)NSString *like;

@property (nonatomic ,strong)NSString *favorite;
@property (nonatomic ,strong)NSString *complaint;
@property (nonatomic ,strong)NSString *comment;
@property (nonatomic ,strong)HBReandDetailImgListModel *image_default_id;
@property (nonatomic ,strong)NSArray *list_image;
@property (nonatomic ,strong)NSString *video_path;

@property (nonatomic ,strong)NSNumber *created_time;
@property (nonatomic ,strong)NSString *modified_time;
@property (nonatomic ,strong)NSString *disabled;
@property (nonatomic ,strong)HBReandDetailUserModel *user;

@property (nonatomic ,strong)NSString *is_like;
@property (nonatomic ,strong)NSString *is_favorite;
@end


@interface HBReandDetailUserModel : NSObject
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *name;
@property (nonatomic ,strong)NSString *portrait_url;
@property (nonatomic ,strong)NSString *phone;
@end

@interface HBReandDetailImgListModel : NSObject
@property (nonatomic ,strong)NSString *url;
@property (nonatomic ,strong)NSString *width;
@property (nonatomic ,strong)NSString *height;
@end
NS_ASSUME_NONNULL_END
