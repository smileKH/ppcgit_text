//
//  SDTimeLineCell.m
//  GSD_WeiXin(wechat)
//
//  Created by gsd on 16/2/25.
//  Copyright © 2016年 GSD. All rights reserved.
//

/*
 
 *********************************************************************************
 *
 * GSD_WeiXin
 *
 * QQ交流群: 459274049
 * Email : gsdios@126.com
 * GitHub: https://github.com/gsdios/GSD_WeiXin
 * 新浪微博:GSD_iOS
 *
 * 此“高仿微信”用到了很高效方便的自动布局库SDAutoLayout（一行代码搞定自动布局）
 * SDAutoLayout地址：https://github.com/gsdios/SDAutoLayout
 * SDAutoLayout视频教程：http://www.letv.com/ptv/vplay/24038772.html
 * SDAutoLayout用法示例：https://github.com/gsdios/SDAutoLayout/blob/master/README.md
 *
 *********************************************************************************
 
 */

#import "SDTimeLineCell.h"

#import "SDTimeLineCellModel.h"
#import "UIView+SDAutoLayout.h"

#import "SDTimeLineCellCommentView.h"

#import "SDWeiXinPhotoContainerView.h"

#import "SDTimeLineCellOperationMenu.h"

#import "LEETheme.h"
#import "UIButton+ImageTitleSpacing.h"
const CGFloat contentLabelFontSize = 15;
CGFloat maxContentLabelHeight = 0; // 根据具体font而定

NSString *const kSDTimeLineCellOperationButtonClickedNotification = @"SDTimeLineCellOperationButtonClickedNotification";
@interface SDTimeLineCell()
//@property (nonatomic ,strong)UIView *locationView;
//@property (nonatomic ,strong)UILabel *locationLabel;
//@property (nonatomic ,strong)UIImageView *locImgView;
@property (nonatomic ,strong)UIButton *locationButton;
@end
@implementation SDTimeLineCell

{
    UIImageView *_iconView;
    UILabel *_nameLable;
    UILabel *_contentLabel;
//    SDWeiXinPhotoContainerView *_picContainerView;
//    UIButton *_locationButton;//时间改为地址
    UIButton *_moreButton;
    UIButton *_operationButton;
    SDTimeLineCellCommentView *_commentView;
    SDTimeLineCellOperationMenu *_operationMenu;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setup];
        
        //设置主题
        [self configTheme];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setup
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveOperationButtonClickedNotification:) name:kSDTimeLineCellOperationButtonClickedNotification object:nil];
    
    _iconView = [UIImageView new];
    _iconView.layer.cornerRadius = 20;
    _iconView.clipsToBounds = YES;
    
    _nameLable = [UILabel new];
    _nameLable.font = [UIFont systemFontOfSize:14];
    _nameLable.textColor = [UIColor colorWithRed:(54 / 255.0) green:(71 / 255.0) blue:(121 / 255.0) alpha:0.9];
    
    _contentLabel = [UILabel new];
    _contentLabel.font = [UIFont systemFontOfSize:contentLabelFontSize];
    _contentLabel.numberOfLines = 0;
    if (maxContentLabelHeight == 0) {
        maxContentLabelHeight = _contentLabel.font.lineHeight * 3;
    }
    
    _moreButton = [UIButton new];
    [_moreButton setTitle:@"全文" forState:UIControlStateNormal];
    [_moreButton setTitleColor:TimeLineCellHighlightedColor forState:UIControlStateNormal];
    [_moreButton addTarget:self action:@selector(moreButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    _moreButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    _operationButton = [UIButton new];
    [_operationButton setImage:[UIImage imageNamed:@"AlbumOperateMore"] forState:UIControlStateNormal];
    [_operationButton addTarget:self action:@selector(operationButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
//    _picContainerView = [SDWeiXinPhotoContainerView new];
    
    _commentView = [SDTimeLineCellCommentView new];
    _commentView.backgroundColor = red_Color;
    
    
    //添加定位按钮
    _locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _locationButton.frame = CGRectMake(SCREEN_WIDTH-100, 7, 90, 30);
    [_locationButton setTitle:@"" forState:UIControlStateNormal];
    [_locationButton setTitle:@"" forState:UIControlStateSelected];
    [_locationButton setTitleColor:RGBA(252, 119, 70, 1) forState:UIControlStateNormal];
    [_locationButton setTitleColor:RGBA(204, 204, 204, 1) forState:UIControlStateSelected];
    [_locationButton setBackgroundImage:[UIImage imageWithColor:RGBA(255, 241, 236, 1)] forState:UIControlStateNormal];
    [_locationButton setBackgroundImage:[UIImage imageWithColor:RGBA(241, 241, 241, 1)] forState:UIControlStateSelected];
    _locationButton.layer.cornerRadius = 15;
    _locationButton.clipsToBounds = YES;
    _locationButton.titleLabel.font = Font(12);
    [_locationButton setImage:ImageNamed(@"location_img_bbc") forState:UIControlStateNormal];
    [_locationButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    _locationButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    //[_locationButton addTarget:self action:@selector(clickLocationButton:) forControlEvents:UIControlEventTouchUpInside];
//    _timeLabel = [UILabel new];
//    _timeLabel.font = [UIFont systemFontOfSize:13];
    
//    _locationView = [[UIView alloc]init];
//    _locationView.backgroundColor = RGBA(255, 241, 236, 1);
//    _locationView.layer.cornerRadius = 15;
//    _locationView.clipsToBounds = YES;
    
    
    _operationMenu = [SDTimeLineCellOperationMenu new];
    __weak typeof(self) weakSelf = self;
    [_operationMenu setLikeButtonClickedOperation:^{
        if ([weakSelf.delegate respondsToSelector:@selector(didClickLikeButtonInCell:)]) {
            [weakSelf.delegate didClickLikeButtonInCell:weakSelf];
        }
    }];
    [_operationMenu setCommentButtonClickedOperation:^{
        if ([weakSelf.delegate respondsToSelector:@selector(didClickcCommentButtonInCell:)]) {
            [weakSelf.delegate didClickcCommentButtonInCell:weakSelf];
        }
    }];
    
    
    NSArray *views = @[_iconView, _nameLable, _contentLabel, _moreButton, _locationButton, _operationButton, _operationMenu, _commentView];
    
    [self.contentView sd_addSubviews:views];
    
    UIView *contentView = self.contentView;
    CGFloat margin = 10;
    
    _iconView.sd_layout
    .leftSpaceToView(contentView, margin)
    .topSpaceToView(contentView, margin + 5)
    .widthIs(40)
    .heightIs(40);
    
    _nameLable.sd_layout
    .leftSpaceToView(_iconView, margin)
    .topEqualToView(_iconView)
    .heightIs(18);
    [_nameLable setSingleLineAutoResizeWithMaxWidth:200];
    
    _contentLabel.sd_layout
    .leftEqualToView(_nameLable)
    .topSpaceToView(_nameLable, margin)
    .rightSpaceToView(contentView, margin)
    .autoHeightRatio(0);
    
    // morebutton的高度在setmodel里面设置
    _moreButton.sd_layout
    .leftEqualToView(_contentLabel)
    .topSpaceToView(_contentLabel, 0)
    .widthIs(30);
    
    
//    _picContainerView.sd_layout
//    .leftEqualToView(_contentLabel); // 已经在内部实现宽度和高度自适应所以不需要再设置宽度高度，top值是具体有无图片在setModel方法中设置
    
    _locationButton.sd_layout
    .leftEqualToView(_contentLabel)
    .topSpaceToView(_contentLabel, margin+10)
    .heightIs(25)
    .widthIs(200);
//    .widthIs(200);
    
    _operationButton.sd_layout
    .rightSpaceToView(contentView, margin)
    .centerYEqualToView(_nameLable)
    .heightIs(25)
    .widthIs(25);
    
    _commentView.sd_layout
    .leftEqualToView(_contentLabel)
    .rightSpaceToView(self.contentView, margin)
    .topSpaceToView(_locationButton, margin); // 已经在内部实现高度自适应所以不需要再设置高度
    
    _operationMenu.sd_layout
    .rightSpaceToView(_operationButton, 0)
    .heightIs(36)
    .centerYEqualToView(_operationButton)
    .widthIs(0);
    
//    //添加子视图
//    _locImgView = [UIImageView new];
//    _locImgView.backgroundColor = red_Color;
//
//    _locationLabel = [[UILabel alloc]init];
//    _locationLabel.textColor = black_Color;
//    _locationLabel.font = Font(14);
//
//    [_locationView sd_addSubviews:@[_locImgView,_locationLabel]];
//    _locImgView.sd_layout
//    .centerYEqualToView(self.locationView)
//    .leftSpaceToView(self.locationView, 5)
//    .widthIs(20)
//    .heightIs(20);
//
//    _locationLabel.sd_layout
//    .centerYEqualToView(self.locationView)
//    .leftSpaceToView(_locImgView, 5)
//    .rightSpaceToView(_locationView, 5)
//    .heightIs(20);
}

- (void)configTheme{
//    self.lee_theme
//    .LeeAddBackgroundColor(DAY , [UIColor whiteColor])
//    .LeeAddBackgroundColor(NIGHT , [UIColor blackColor]);
//
//    _contentLabel.lee_theme
//    .LeeAddTextColor(DAY , [UIColor blackColor])
//    .LeeAddTextColor(NIGHT , [UIColor grayColor]);

}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setModel:(SDTimeLineCellModel *)model
{
    _model = model;
    
    [_commentView setupWithLikeItemsArray:nil commentItemsArray:model.child_comments];
    
//    _iconView.image = [UIImage imageNamed:model.user.portrait_url];
    [_iconView sd_setImageWithURL:[NSURL URLWithString:model.user.portrait_url] placeholderImage:GOODS_DEFINE_IMG];
    _nameLable.text = model.user.name;
    _contentLabel.text = model.comment_content;
//    _picContainerView.picPathStringsArray = model.picNamesArray;
    
    if ([HBHuTool isJudgeString:model.comment_address]) {
        //没有
        //self.locationButton.hidden = YES;
        [self.locationButton setTitle:@"未开启定位" forState:UIControlStateNormal];
//        _locationLabel.text = @"未开启定位";
        _locationButton.selected = YES;
        [_locationButton layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleLeft imageTitleSpace:3];
        _locationButton.sd_layout
        .widthIs(100);
    }else{
//        _locationLabel.text = model.comment_address;
        //self.locationButton.hidden = NO;
        _locationButton.selected = NO;
        [self.locationButton setTitle:model.comment_address forState:UIControlStateNormal];
        NSString *content = self.locationButton.titleLabel.text;
        UIFont *font = self.locationButton.titleLabel.font;
        CGSize size = CGSizeMake(MAXFLOAT, 30.0f);
        CGSize buttonSize = [content boundingRectWithSize:size
                                                  options:NSStringDrawingTruncatesLastVisibleLine  | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                               attributes:@{ NSFontAttributeName:font}
                                                  context:nil].size;
        [_locationButton layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleLeft imageTitleSpace:3];
        CGFloat neButtonWidth = buttonSize.width;
        if (neButtonWidth>SCREEN_WIDTH-75) {
            neButtonWidth = SCREEN_WIDTH-75;
        }
        _locationButton.sd_layout
        .widthIs(neButtonWidth);
    }
    
    if (model.shouldShowMoreButton) { // 如果文字高度超过60
        _moreButton.sd_layout.heightIs(20);
        _moreButton.hidden = NO;
        if (model.isOpening) { // 如果需要展开
            _contentLabel.sd_layout.maxHeightIs(MAXFLOAT);
            [_moreButton setTitle:@"收起" forState:UIControlStateNormal];
        } else {
            _contentLabel.sd_layout.maxHeightIs(maxContentLabelHeight);
            [_moreButton setTitle:@"全文" forState:UIControlStateNormal];
        }
    } else {
        _moreButton.sd_layout.heightIs(0);
        _moreButton.hidden = YES;
    }
    
//    CGFloat picContainerTopMargin = 0;
//    if (model.picNamesArray.count) {
//        picContainerTopMargin = 10;
//    }
//    _picContainerView.sd_layout.topSpaceToView(_moreButton, picContainerTopMargin);
    
    UIView *bottomView;
    
    if (!model.child_comments.count) {
        bottomView = _locationButton;
    } else {
        bottomView = _commentView;
    }
    
    [self setupAutoHeightWithBottomView:bottomView bottomMargin:15];
    
//    _timeLabel.text = @"1分钟前";
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if (_operationMenu.isShowing) {
        _operationMenu.show = NO;
    }
}


#pragma mark - private actions

- (void)moreButtonClicked
{
    if (self.moreButtonClickedBlock) {
        self.moreButtonClickedBlock(self.indexPath);
    }
}
#pragma mark ==========点击评论按钮==========
- (void)operationButtonClicked
{
//    [self postOperationButtonClickedNotification];
//    _operationMenu.show = !_operationMenu.isShowing;
    if ([self.delegate respondsToSelector:@selector(didClickcCommentButtonInCell:)]) {
        [self.delegate didClickcCommentButtonInCell:self];
    }
}

- (void)receiveOperationButtonClickedNotification:(NSNotification *)notification
{
    UIButton *btn = [notification object];
    
    if (btn != _operationButton && _operationMenu.isShowing) {
        _operationMenu.show = NO;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self postOperationButtonClickedNotification];
    if (_operationMenu.isShowing) {
        _operationMenu.show = NO;
    }
}

- (void)postOperationButtonClickedNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kSDTimeLineCellOperationButtonClickedNotification object:_operationButton];
}

@end

