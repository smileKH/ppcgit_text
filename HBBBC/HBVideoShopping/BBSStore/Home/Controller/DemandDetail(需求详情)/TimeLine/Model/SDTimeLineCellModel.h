//
//  SDTimeLineCellModel.h
//  GSD_WeiXin(wechat)
//
//  Created by gsd on 16/2/25.
//  Copyright © 2016年 GSD. All rights reserved.
//

/*
 
 *********************************************************************************
 *
 * GSD_WeiXin
 *
 * QQ交流群: 459274049
 * Email : gsdios@126.com
 * GitHub: https://github.com/gsdios/GSD_WeiXin
 * 新浪微博:GSD_iOS
 *
 * 此“高仿微信”用到了很高效方便的自动布局库SDAutoLayout（一行代码搞定自动布局）
 * SDAutoLayout地址：https://github.com/gsdios/SDAutoLayout
 * SDAutoLayout视频教程：http://www.letv.com/ptv/vplay/24038772.html
 * SDAutoLayout用法示例：https://github.com/gsdios/SDAutoLayout/blob/master/README.md
 *
 *********************************************************************************
 
 */

#import <Foundation/Foundation.h>
#import "HBDemandListModel.h"
@class SDTimeLineCellLikeItemModel, SDTimeLineCellCommentItemModel,SDTimeLineCellUserInfoModel;

@interface SDTimeLineCellModel : NSObject

//@property (nonatomic, copy) NSString *iconName;
//@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *msgContent;
//@property (nonatomic, strong) NSArray *picNamesArray;
@property (nonatomic ,strong)NSString *comment_id;
@property (nonatomic ,strong)NSString *demand_id;
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *reply_user_id;
@property (nonatomic ,strong)NSString *parent_id;
@property (nonatomic ,strong)NSString *comment_content;
@property (nonatomic ,strong)NSString *latitude;
@property (nonatomic ,strong)NSString *longitude;
@property (nonatomic ,strong)NSString *comment_address;
@property (nonatomic ,strong)NSString *created_time;
@property (nonatomic ,strong)HBDemandListUserModel *user;
@property (nonatomic, strong) NSArray *child_comments;


@property (nonatomic, assign, getter = isLiked) BOOL liked;

@property (nonatomic, strong) NSArray<SDTimeLineCellLikeItemModel *> *likeItemsArray;


@property (nonatomic, assign) BOOL isOpening;

@property (nonatomic, assign, readonly) BOOL shouldShowMoreButton;


@end


@interface SDTimeLineCellLikeItemModel : NSObject

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSAttributedString *attributedContent;

@end


@interface SDTimeLineCellCommentItemModel : NSObject

//@property (nonatomic, copy) NSString *commentString;
//
//@property (nonatomic, copy) NSString *firstUserName;
//@property (nonatomic, copy) NSString *firstUserId;
//
//@property (nonatomic, copy) NSString *secondUserName;
//@property (nonatomic, copy) NSString *secondUserId;
//
@property (nonatomic, copy) NSAttributedString *attributedContent;
@property (nonatomic ,strong)NSString *comment_id;
@property (nonatomic ,strong)NSString *demand_id;
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *reply_user_id;
@property (nonatomic ,strong)NSString *parent_id;
@property (nonatomic ,strong)NSString *comment_content;
@property (nonatomic ,strong)NSString *latitude;
@property (nonatomic ,strong)NSString *longitude;
@property (nonatomic ,strong)NSString *comment_address;
@property (nonatomic ,strong)NSString *created_time;
@property (nonatomic ,strong)NSString *modified_time;
@property (nonatomic ,strong)HBDemandListUserModel *reply_user;
@property (nonatomic ,strong)SDTimeLineCellUserInfoModel *user;

@end


@interface SDTimeLineCellUserInfoModel : NSObject
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *name;
@property (nonatomic ,strong)NSString *portrait_url;

@end
