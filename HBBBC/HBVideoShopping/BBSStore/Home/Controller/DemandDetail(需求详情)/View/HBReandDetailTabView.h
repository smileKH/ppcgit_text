//
//  HBReandDetailTabView.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2020/2/8.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBBaseListTableView.h"
#import "JXPagerView.h"
#import "HBDemandListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HBReandDetailTabView : HBBaseListTableView<JXPagerViewListViewDelegate>
@property (nonatomic ,weak)UIViewController *upController;
@property (nonatomic, assign) BOOL isNeedFooter;
@property (nonatomic, assign) BOOL isNeedHeader;
@property (nonatomic, assign) BOOL isHeaderRefreshed;   //默认为YES
@property (nonatomic ,assign)NSInteger indexCurrend;//1、评论 2、投诉

- (void)beginFirstRefresh;


@property (nonatomic ,strong)HBDemandListModel *listModel;

@end

NS_ASSUME_NONNULL_END
