//
//  HBXQCarousel.m
//  视频和图片的混合轮播
//
//  Created by xzmwkj on 2018/7/10.
//  Copyright © 2018年 WangShuai. All rights reserved.
//

#import "HBXQCarousel.h"
#import "HBXQVideoView.h"
#import "HZPhotoBrowser.h"

@interface HBXQCarousel ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) HBXQVideoView *videoView;

@end

@implementation HBXQCarousel

+ (instancetype)scrollViewFrame:(CGRect)frame imageStringGroup:(NSArray *)imgArray {
    HBXQCarousel *carousel = [[self alloc] initWithFrame:frame];
    carousel.contentArray = imgArray;
    return carousel;
}

- (void)setContentArray:(NSArray *)contentArray {
    _contentArray = contentArray;
    [self loadUI];
}
-(void)stopPlayVideo{
    [self.videoView stop];
}
- (void)loadUI {
    
    //self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    self.extendedLayoutIncludesOpaqueBars = YES;
    if (@available(iOS 11.0, *)) {
        self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        //self.scrollView.automaticallyAdjustsScrollViewInsets = NO;
        //self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.scrollView.contentSize = CGSizeMake(self.frame.size.width * self.contentArray.count, self.frame.size.height);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    [self addSubview:self.scrollView];
    
    for (NSInteger index = 0; index < self.contentArray.count; index ++) {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width * index, 0, self.frame.size.width, self.frame.size.height)];
        imgView.userInteractionEnabled = YES;
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.clipsToBounds = YES;
        imgView.tag = index;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView:)];
        [imgView addGestureRecognizer:tap];
        NSString *urlStr = self.contentArray[index];
        if ([urlStr hasSuffix:@".mp4"] | [urlStr hasSuffix:@".MP4"] && [urlStr hasPrefix:@"http"]) {
            // 网络视频
            self.videoView = [HBXQVideoView videoViewFrame:imgView.frame videoUrl:self.contentArray[index]];
            self.videoView.videoUrl = self.contentArray[index];
            [self.scrollView addSubview:self.videoView];
            
        } else if ([urlStr hasSuffix:@".mp4"]) {
            
            // 本地视频
            
            
        } else if ([urlStr hasPrefix:@"http"]) {
            [imgView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:GOODS_DEFINE_IMG];
            
            // 网络图片
            [self.scrollView addSubview:imgView];
            
        } else {
            imgView.image = [UIImage imageNamed:urlStr];
            // 本地图片
            [self.scrollView addSubview:imgView];
            
        }
    }
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.frame = CGRectMake(0, self.frame.size.height - 30, self.frame.size.width, 30);
    self.pageControl.numberOfPages = self.contentArray.count;
    self.pageControl.currentPage = 0;
    self.pageControl.enabled = NO;
    self.pageControl.currentPageIndicatorTintColor = red_Color;
    self.pageControl.pageIndicatorTintColor = gray_Color;
    [self addSubview:self.pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSInteger currentPage = round(scrollView.contentOffset.x / self.frame.size.width);
    self.pageControl.currentPage = currentPage;
    
    if (self.videoView.isPlay) {
        if (currentPage == 0) {
            [self.videoView start];
        }else {
            [self.videoView stop];
        }
    }
    
}

- (void)tapImageView:(UITapGestureRecognizer *)tap{
    UIView *imageView = tap.view;
    
    NSLog(@"打印一下图片的tag是多少%ld",imageView.tag);
    NSMutableArray *imgArray = [NSMutableArray array];
    [imgArray addObjectsFromArray:self.contentArray];
    BOOL isVideo = NO;
    
    NSString *urlStr = imgArray[imageView.tag];
    if ([urlStr hasSuffix:@".mp4"] | [urlStr hasSuffix:@".MP4"] && [urlStr hasPrefix:@"http"]) {
        // 网络视频
        [imgArray removeObjectAtIndex:imageView.tag];
        isVideo = YES;
        return;
        
    } else if ([urlStr hasSuffix:@".mp4"]) {
        
        // 本地视频
    } else if ([urlStr hasPrefix:@"http"]) {
        // 网络图片
        
    } else {
        // 本地图片
        // 网络图片
        
    }
    
    HZPhotoBrowser *browser = [[HZPhotoBrowser alloc] init];
    browser.isFullWidthForLandScape = NO;
    browser.isNeedLandscape = NO;
    if (isVideo) {
        browser.currentImageIndex = imageView.tag-1;
    }else{
        browser.currentImageIndex = imageView.tag;
    }
    browser.imageArray = imgArray;
    
    [browser show];
}

@end
