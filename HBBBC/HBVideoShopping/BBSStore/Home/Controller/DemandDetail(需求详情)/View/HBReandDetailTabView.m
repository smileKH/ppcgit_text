//
//  HBReandDetailTabView.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2020/2/8.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBReandDetailTabView.h"

//#import "HBActivityListModel.h"
#import "HBReandDetailTableCell.h"


#import "SDRefresh.h"

#import "SDTimeLineTableHeaderView.h"
#import "SDTimeLineRefreshHeader.h"
#import "SDTimeLineRefreshFooter.h"
#import "SDTimeLineCell.h"
#import "SDTimeLineCellModel.h"

#import "UITableView+SDAutoTableViewCellHeight.h"

#import "UIView+SDAutoLayout.h"
#import "LEETheme.h"
#import "GlobalDefines.h"

#import "PFAPP.h"

#define kTimeLineTableViewCellId @"SDTimeLineCell"

static CGFloat textFieldH = 50;
@interface HBReandDetailTabView ()<UITableViewDataSource, UITableViewDelegate,SDTimeLineCellDelegate, UITextFieldDelegate>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, strong) UITextField *textField;//这个
@property (nonatomic ,strong)UIView *sendMsgView;//发送信息
@property (nonatomic ,strong)UIButton *locationButton;//开始定位按钮
@property (nonatomic, assign) BOOL isReplayingComment;
@property (nonatomic, strong) NSIndexPath *currentEditingIndexthPath;
@property (nonatomic, copy) NSString *commentToUser;
@end

@implementation HBReandDetailTabView
{
    SDTimeLineRefreshFooter *_refreshFooter;
    SDTimeLineRefreshHeader *_refreshHeader;
    CGFloat _lastScrollViewOffsetY;
    CGFloat _totalKeybordHeight;
}
#pragma mark - JXPagingViewListViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.scrollCallback(scrollView);
}

- (UIScrollView *)listScrollView {
    return self;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (UIView *)listView {
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self) {
        //添加分隔线颜色设置
//        self.lee_theme
//        .LeeAddSeparatorColor(DAY , [[UIColor lightGrayColor] colorWithAlphaComponent:0.5f])
//        .LeeAddSeparatorColor(NIGHT , [[UIColor grayColor] colorWithAlphaComponent:0.5f]);
        
        [self registerClass:[SDTimeLineCell class] forCellReuseIdentifier:kTimeLineTableViewCellId];
        
        [self setupTextField];
        
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
        
        //添加通知
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushChangeTablViewData) name:@"reandDetailPushStrSuccess" object:nil];
        //请求数据
//        [self requestReandDetail];
    }
    return self;
}
#pragma mark ==========更新数据==========
-(void)pushChangeTablViewData{
    [self getRequestShopGoodsListIsNew:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [_textField resignFirstResponder];
}

- (void)dealloc
{
    [_refreshHeader removeFromSuperview];
    [_refreshFooter removeFromSuperview];
    
    [_textField removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupTextField
{
    
    _sendMsgView = [UIView new];
    _sendMsgView.backgroundColor = RGBA(249, 249, 249, 1);
    _sendMsgView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, SCREEN_WIDTH, textFieldH);
    [[UIApplication sharedApplication].keyWindow addSubview:_sendMsgView];

    _textField = [UITextField new];
    _textField.returnKeyType = UIReturnKeySend;
    _textField.delegate = self;
    _textField.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.8].CGColor;
    _textField.layer.borderWidth = 1;
    _textField.layer.borderColor = RGBA(242, 242, 242, 1).CGColor;
    _textField.layer.cornerRadius = 18;
    _textField.backgroundColor = RGBA(242, 242, 242, 1);
    _textField.clipsToBounds = YES;
    _textField.placeholder = @"说些什么。。。";
    _textField.textColor = black_Color;
    _textField.font = Font(14);

    //为textfield添加背景颜色 字体颜色的设置 还有block设置 , 在block中改变它的键盘样式 (当然背景颜色和字体颜色也可以直接在block中写)

//    _textField.lee_theme
//    .LeeAddBackgroundColor(DAY , [UIColor whiteColor])
//    .LeeAddBackgroundColor(NIGHT , [UIColor blackColor])
//    .LeeAddTextColor(DAY , [UIColor blackColor])
//    .LeeAddTextColor(NIGHT , [UIColor grayColor])
//    .LeeAddCustomConfig(DAY , ^(UITextField *item){
//
//        item.keyboardAppearance = UIKeyboardAppearanceDefault;
//        if ([item isFirstResponder]) {
//            [item resignFirstResponder];
//            [item becomeFirstResponder];
//        }
//    }).LeeAddCustomConfig(NIGHT , ^(UITextField *item){
//
//        item.keyboardAppearance = UIKeyboardAppearanceDark;
//        if ([item isFirstResponder]) {
//            [item resignFirstResponder];
//            [item becomeFirstResponder];
//        }
//    });

//    _textField.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, SCREEN_WIDTH, textFieldH);
//    [[UIApplication sharedApplication].keyWindow addSubview:_textField];
    _textField.frame = CGRectMake(10, 7, SCREEN_WIDTH-120, textFieldH-14);
    [_sendMsgView addSubview:_textField];

    [_textField becomeFirstResponder];
    [_textField resignFirstResponder];
    
    //添加定位按钮
    _locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _locationButton.frame = CGRectMake(SCREEN_WIDTH-100, 7, 90, textFieldH-14);
    [_locationButton setTitle:@"去开始定位" forState:UIControlStateNormal];
    [_locationButton setTitle:@"已开启定位" forState:UIControlStateSelected];
    [_locationButton setTitleColor:RGBA(252, 119, 70, 1) forState:UIControlStateNormal];
    [_locationButton setTitleColor:RGBA(204, 204, 204, 1) forState:UIControlStateSelected];
    [_locationButton setBackgroundImage:[UIImage imageWithColor:RGBA(255, 241, 236, 1)] forState:UIControlStateNormal];
    [_locationButton setBackgroundImage:[UIImage imageWithColor:RGBA(241, 241, 241, 1)] forState:UIControlStateSelected];
    _locationButton.layer.cornerRadius = 18;
    _locationButton.clipsToBounds = YES;
    _locationButton.titleLabel.font = Font(14);
    [_locationButton addTarget:self action:@selector(clickLocationButton:) forControlEvents:UIControlEventTouchUpInside];
    [_sendMsgView addSubview:_locationButton];
}
#pragma mark ==========点击定位按钮==========
-(void)clickLocationButton:(UIButton *)button{
    //拿到定位位置
    button.selected = !button.selected;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SDTimeLineCell *cell = [tableView dequeueReusableCellWithIdentifier:kTimeLineTableViewCellId];
    cell.indexPath = indexPath;
    __weak typeof(self) weakSelf = self;
    if (!cell.moreButtonClickedBlock) {
        [cell setMoreButtonClickedBlock:^(NSIndexPath *indexPath) {
            SDTimeLineCellModel *model = weakSelf.allData[indexPath.row];
            model.isOpening = !model.isOpening;
            [weakSelf reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }];
        
        [cell setDidClickCommentLabelBlock:^(NSString *commentId, CGRect rectInWindow, NSIndexPath *indexPath) {
            weakSelf.textField.placeholder = [NSString stringWithFormat:@"  回复：%@", commentId];
            weakSelf.currentEditingIndexthPath = indexPath;
            [weakSelf.textField becomeFirstResponder];
            weakSelf.isReplayingComment = YES;
            weakSelf.commentToUser = commentId;
            [weakSelf adjustTableViewToFitKeyboardWithRect:rectInWindow];
        }];
        
        cell.delegate = self;
    }
    
    ////// 此步设置用于实现cell的frame缓存，可以让tableview滑动更加流畅 //////
    
    [cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
    
    ///////////////////////////////////////////////////////////////////////
    
    cell.model = self.allData[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // >>>>>>>>>>>>>>>>>>>>> * cell自适应 * >>>>>>>>>>>>>>>>>>>>>>>>
    id model = self.allData[indexPath.row];
    return [self cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[SDTimeLineCell class] contentViewWidth:[self cellContentViewWith]];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_textField resignFirstResponder];
    _textField.placeholder = nil;
}



- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7横屏
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}


#pragma mark - SDTimeLineCellDelegate

- (void)didClickcCommentButtonInCell:(UITableViewCell *)cell
{
    [_textField becomeFirstResponder];
    _currentEditingIndexthPath = [self indexPathForCell:cell];
    
    [self adjustTableViewToFitKeyboard];
}
#pragma mark ==========点击喜欢列表==========
- (void)didClickLikeButtonInCell:(UITableViewCell *)cell
{
    NSIndexPath *index = [self indexPathForCell:cell];
    SDTimeLineCellModel *model = self.allData[index.row];
    NSMutableArray *temp = [NSMutableArray arrayWithArray:model.likeItemsArray];
    
    if (!model.isLiked) {
        SDTimeLineCellLikeItemModel *likeModel = [SDTimeLineCellLikeItemModel new];
        likeModel.userName = @"GSD_iOS";
        likeModel.userId = @"gsdios";
        [temp addObject:likeModel];
        model.liked = YES;
    } else {
        SDTimeLineCellLikeItemModel *tempLikeModel = nil;
        for (SDTimeLineCellLikeItemModel *likeModel in model.likeItemsArray) {
            if ([likeModel.userId isEqualToString:@"gsdios"]) {
                tempLikeModel = likeModel;
                break;
            }
        }
        [temp removeObject:tempLikeModel];
        model.liked = NO;
    }
    model.likeItemsArray = [temp copy];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    });
}


- (void)adjustTableViewToFitKeyboard
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITableViewCell *cell = [self cellForRowAtIndexPath:_currentEditingIndexthPath];
    CGRect rect = [cell.superview convertRect:cell.frame toView:window];
    [self adjustTableViewToFitKeyboardWithRect:rect];
}

- (void)adjustTableViewToFitKeyboardWithRect:(CGRect)rect
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    CGFloat delta = CGRectGetMaxY(rect) - (window.bounds.size.height - _totalKeybordHeight);
    
    CGPoint offset = self.contentOffset;
    offset.y += delta;
    if (offset.y < 0) {
        offset.y = 0;
    }
    
    [self setContentOffset:offset animated:YES];
}

#pragma mark - UITextFieldDelegate
//接口完成的事情
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length) {
        
        [_textField resignFirstResponder];
        
        //请求接口
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        SDTimeLineCellModel *model = self.allData[_currentEditingIndexthPath.row];
        parameters[@"demand_id"] = model.demand_id;
        
        parameters[@"latitude"] = @([PFAPP sharedInstance].lat);
        parameters[@"longitude"] = @([PFAPP sharedInstance].lng);
        parameters[@"comment_address"] = [PFAPP sharedInstance].address;
        parameters[@"parent_id"] = model.comment_id;
        parameters[@"reply_user_id"] = model.reply_user_id;
        NSString *urlStr = nil;
        if (self.indexCurrend==1) {
            //评论
            urlStr = DEMAND_DEMAND_COMMENT_CREATE;
            parameters[@"comment_content"] = textField.text;
        }else{
            //投诉
            urlStr = DEMAND_DEMAND_COMPLAINT_CREATE;
            parameters[@"complaint_content"] = textField.text;
        }
        WEAKSELF;
        [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        [LBService post:urlStr params:parameters completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
                //            [MBProgressHUD showInfoMessage:@"成功"];
                //刷新列表
                [weakSelf getRequestShopGoodsListIsNew:YES];
//                NSMutableArray *temp = [NSMutableArray new];
//                [temp addObjectsFromArray:model.child_comments];
//                SDTimeLineCellCommentItemModel *commentItemModel = [SDTimeLineCellCommentItemModel new];
//
//                        if (self.isReplayingComment) {
//                            commentItemModel.firstUserName = @"GSD_iOS";
//                            commentItemModel.firstUserId = @"GSD_iOS";
//                            commentItemModel.secondUserName = self.commentToUser;
//                            commentItemModel.secondUserId = self.commentToUser;
//                            commentItemModel.commentString = textField.text;
//
//                            self.isReplayingComment = NO;
//                        } else {
//                            commentItemModel.firstUserName = @"GSD_iOS";
//                            commentItemModel.commentString = textField.text;
//                            commentItemModel.firstUserId = @"GSD_iOS";
//                        }
//                [temp addObject:commentItemModel];
//                model.child_comments = [temp copy];
//                [weakSelf reloadRowsAtIndexPaths:@[_currentEditingIndexthPath] withRowAnimation:UITableViewRowAnimationNone];
            }else{
                [app showToastView:response.message];
            }
        }];
        
        
        
        
        _textField.text = @"";
        _textField.placeholder = nil;
        
        return YES;
    }
    return NO;
}



- (void)keyboardNotification:(NSNotification *)notification
{
    NSDictionary *dict = notification.userInfo;
    CGRect rect = [dict[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    
    
    
    CGRect textFieldRect = CGRectMake(0, rect.origin.y - textFieldH, rect.size.width, textFieldH);
    if (rect.origin.y == [UIScreen mainScreen].bounds.size.height) {
        textFieldRect = rect;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        _sendMsgView.frame = textFieldRect;
    }];
    
    CGFloat h = rect.size.height + textFieldH;
    if (_totalKeybordHeight != h) {
        _totalKeybordHeight = h;
        [self adjustTableViewToFitKeyboard];
    }
}

#pragma mark ==========数据处理==========
- (void)setIsNeedHeader:(BOOL)isNeedHeader {
    _isNeedHeader = isNeedHeader;

    __weak typeof(self)weakSelf = self;
    if (self.isNeedHeader) {
        self.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //                [weakSelf.mj_header endRefreshing];
            //            });
            [weakSelf.mj_header endRefreshing];
            [weakSelf getRequestShopGoodsListIsNew:YES];
        }];
    }else {
        [self.mj_header endRefreshing];
        [self.mj_header removeFromSuperview];
        self.mj_header = nil;
    }
    
//    if (!_refreshHeader.superview) {
//        if (self.isNeedHeader) {
//            _refreshHeader = [SDTimeLineRefreshHeader refreshHeaderWithCenter:CGPointMake(40, 45)];
//            _refreshHeader.scrollView = self;
//            __weak typeof(_refreshHeader) weakHeader = _refreshHeader;
//            __weak typeof(self) weakSelf = self;
//            [_refreshHeader setRefreshingBlock:^{
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    weakSelf.allData = [[weakSelf creatModelsWithCount:10] mutableCopy];
//                    [weakSelf getRequestShopGoodsListIsNew:YES];
//                    [weakHeader endRefreshing];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [weakSelf reloadData];
//                    });
//                });
//            }];
//            [self.superview addSubview:_refreshHeader];
//        }
//
//    } else {
//        [self.superview bringSubviewToFront:_refreshHeader];
//    }
}

- (void)setIsNeedFooter:(BOOL)isNeedFooter {
    _isNeedFooter = isNeedFooter;
    //上拉更多
    __weak typeof(self)weakSelf = self;
    if (self.isNeedFooter) {
        self.mj_footer = [MJRefreshBackNormalFooter  footerWithRefreshingBlock:^{
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //                [weakSelf. addObject:@"加载更多成功"];
            //                [weakSelf reloadData];
            //                [weakSelf.mj_footer endRefreshing];
            //            });
            
            [weakSelf getRequestShopGoodsListIsNew:NO];
        }];
    }else {
        [self.mj_footer endRefreshing];
        [self.mj_footer removeFromSuperview];
        self.mj_footer = nil;
    }
}

- (void)beginFirstRefresh {
    if (!self.isHeaderRefreshed) {
        [self beginRefreshImmediately];
    }
}

- (void)beginRefreshImmediately {
    if (self.isNeedHeader) {
        [self.mj_header beginRefreshing];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.isHeaderRefreshed = YES;
            
            [self.mj_header endRefreshing];
        });
        
    }else {
        self.isHeaderRefreshed = YES;
        [self reloadData];
    }
}
#pragma mark ==========请求商品列表==========
- (void)getRequestShopGoodsListIsNew:(BOOL)isNew{
    if (isNew) {
        self.pageIndex = 1;
        self.pageSize = 10;
        [self.allData removeAllObjects];
        [self.mj_footer resetNoMoreData];
    }else{
        self.pageIndex ++;
        self.pageSize = 10;
    }
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"demand_id"] = self.listModel.demand_id;
    parameters[@"page_size"] = @(self.pageSize);
    parameters[@"page"] = @(self.pageIndex);
    NSString *urlStr = nil;
    if (self.indexCurrend==1) {
        //评论
        urlStr = DEMAND_DEMAND_COMMENT_LIST;
    }else{
        //投诉
        urlStr = DEMAND_DEMAND_COMPLAINT_LIST;
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:urlStr params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
           NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                NSDictionary *pagDic = dic[@"pagers"];
                int number = [pagDic[@"current_page"]intValue];
                NSArray *arr = [dic objectForKey:@"comments"];
            if ([HBHuTool judgeArrayIsEmpty:arr]) {
                //数据转模型
                NSArray *array = [SDTimeLineCellModel mj_objectArrayWithKeyValuesArray:arr];
                //正常有数据 隐藏无数据view
            if (array != nil && array.count > 0) {
                [weakSelf.allData addObjectsFromArray:array];
                //设置视图
                //[weakSelf setGoodsViewController];
                    }
                }
                                //加载 没有更多数据
                if ((weakSelf.pageIndex > number)&&(number>0)) {
                    [app showToastView:Text_NoMoreData];
                }
            }
        }else{
            [app showToastView:response.message];
        }
            //        weakSelf.collectionView.dataArray = weakSelf.allData;
//                    if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
//                        //有数据
//                        [weakSelf hideContentNullPromptView];
//                    }else{
//                        [weakSelf showContentNullPromptNormal];
//                    }
                    [weakSelf reloadData];
                    [weakSelf.mj_footer endRefreshing];
                    [weakSelf.mj_header endRefreshing];
    }];

}


@end
