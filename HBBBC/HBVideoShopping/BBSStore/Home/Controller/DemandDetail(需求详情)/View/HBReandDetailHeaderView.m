//
//  HBReandDetailHeaderView.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2020/2/8.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBReandDetailHeaderView.h"

#import "HBXQCarousel.h"
@interface HBReandDetailHeaderView ()
@property (nonatomic ,strong)HBXQCarousel *carousel;
@property (nonatomic ,strong)UIView *headerView;
@property (nonatomic ,strong)UIView *contentView;//内容
@property (nonatomic ,strong)UILabel *titleLabel;//标题
@property (nonatomic ,strong)UILabel *timeLabel;//时间
@property (nonatomic ,strong)UILabel *likeNumLab;//收藏喜欢
@property (nonatomic ,strong)UIView *centerLineView;//中间view
@property (nonatomic ,strong)UIView *userInfoView;//用户信息
@property (nonatomic ,strong)UIImageView *imgView;
@property (nonatomic ,strong)UILabel *nameLabel;//名字
@property (nonatomic ,strong)UILabel *phoneLabel;//电话号码
@property (nonatomic ,strong)UILabel *addressLab;//地址
@property (nonatomic ,strong)UIImageView *pushImgView;//图片
@property (nonatomic ,strong)UIImageView *bgImgView;//图片
@end
@implementation HBReandDetailHeaderView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Color_BG;
        //添加子视图
        [self setupUI];
        
    }
    return self;
}

-(void)stopHeaderPlayVideo{
    [self.carousel stopPlayVideo];
}

-(void)setupUI{
    /**
     测试  数组首位为视频播放的地址，其余为本地图片，可根据实际需要进行更改
     **/
    self.carousel = [HBXQCarousel scrollViewFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH) imageStringGroup:nil];
    self.carousel.backgroundColor = white_Color;
    [self addSubview:self.carousel];
    
    self.bgImgView = [UIImageView new];
    self.bgImgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH);
    self.bgImgView.backgroundColor = white_Color;
    self.bgImgView.image = ImageNamed(ZAN_WU_TUPIAN);
    [self addSubview:self.bgImgView];
    self.bgImgView.hidden = YES;
    
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.carousel.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(95);
    }];
    
    [self addSubview:self.centerLineView];
    [self.centerLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(5);
    }];
    
    [self addSubview:self.userInfoView];
    [self.userInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.centerLineView.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(110);
    }];
    
    //添加子视图
    CGFloat left = 10;
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(left);
        make.right.equalTo(self.contentView).offset(-left);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(left);
        make.bottom.equalTo(self.contentView).offset(-15);
    }];
    
    [self.contentView addSubview:self.likeNumLab];
    [self.likeNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-left);
        make.bottom.equalTo(self.contentView).offset(-15);
    }];
    
    
    [self.userInfoView addSubview:self.imgView];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userInfoView).offset(left);
        make.top.equalTo(self.userInfoView).offset(10);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    [self.userInfoView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imgView.mas_right).offset(left);
        make.centerY.equalTo(self.imgView);
        make.height.mas_equalTo(14);
    }];
    
    [self.userInfoView addSubview:self.phoneLabel];
    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel.mas_right).offset(20);
        make.centerY.equalTo(self.imgView);
        make.height.mas_equalTo(14);
    }];
    
    [self.userInfoView addSubview:self.pushImgView];
    [self.pushImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.userInfoView).offset(-left);
        make.top.equalTo(self.imgView.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.userInfoView addSubview:self.addressLab];
    [self.addressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userInfoView).offset(left);
        make.top.equalTo(self.imgView.mas_bottom).offset(10);
        make.right.equalTo(self.pushImgView.mas_left).offset(-left);
    }];
}

-(void)setDataModel:(HBReandDetailModel *)dataModel{
    _dataModel = dataModel;
    NSArray *array = dataModel.list_image;

    NSMutableArray *muArr = [NSMutableArray array];
    if (![HBHuTool isJudgeString:dataModel.video_path]) {
        [muArr addObject:dataModel.video_path];
    }
    for (HBReandDetailImgListModel *obj in array) {
        [muArr addObject:obj.url];
    }
    if ([HBHuTool judgeArrayIsEmpty:muArr]) {
        self.bgImgView.hidden = YES;
    }else{
        self.bgImgView.hidden = NO;
    }
    self.carousel.contentArray = [muArr copy];
    
    self.titleLabel.text = dataModel.demand_desc;
    self.timeLabel.text = [XH_Date_String dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss" tenNumber:dataModel.created_time];
    self.likeNumLab.text = [NSString stringWithFormat:@"%@人收藏/%@人点赞",dataModel.favorite,dataModel.like];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:dataModel.user.portrait_url] placeholderImage:GOODS_DEFINE_IMG];
    self.nameLabel.text = dataModel.user.name;
    self.addressLab.text = dataModel.demand_address;
}

-(UIView *)headerView{
    if (!_headerView) {
        _headerView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _headerView ;
}
-(UIView *)contentView{
    if (!_contentView) {
        _contentView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _contentView ;
}
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"";//内容显示
            label.textColor = black_Color;//设置字体颜色
            label.font = Font(14);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _titleLabel ;
}
-(UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(12);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _timeLabel ;
}
-(UILabel *)likeNumLab{
    if (!_likeNumLab) {
        _likeNumLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(12);//设置字体大小
            label.textAlignment = NSTextAlignmentRight;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _likeNumLab ;
}
-(UIView *)centerLineView{
    if (!_centerLineView) {
        _centerLineView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = RGBA(204, 204, 204, 1);
            
            view ;
        }) ;
    }
    return _centerLineView ;
}
-(UIView *)userInfoView{
    if (!_userInfoView) {
        _userInfoView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _userInfoView ;
}
-(UIImageView *)imgView{
    if (!_imgView) {
        _imgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:@"bbs_header"];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _imgView ;
}
-(UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(14);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _nameLabel ;
}
-(UILabel *)phoneLabel{
    if (!_phoneLabel) {
        _phoneLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(14);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _phoneLabel ;
}
-(UILabel *)addressLab{
    if (!_addressLab) {
        _addressLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(14);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 2; //行数
            
            label ;
        }) ;
    }
    return _addressLab ;
}
-(UIImageView *)pushImgView{
    if (!_pushImgView) {
        _pushImgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:@"bs_daohang_img"];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _pushImgView ;
}

@end
