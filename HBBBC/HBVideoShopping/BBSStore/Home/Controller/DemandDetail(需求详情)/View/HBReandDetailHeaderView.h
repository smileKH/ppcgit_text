//
//  HBReandDetailHeaderView.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2020/2/8.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBReandDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HBReandDetailHeaderView : UIView
@property (nonatomic ,strong)HBReandDetailModel *dataModel;
@end

NS_ASSUME_NONNULL_END
