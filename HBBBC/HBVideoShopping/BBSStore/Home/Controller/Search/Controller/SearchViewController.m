//
//  SearchViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/8.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "SearchViewController.h"
#import "HBSearchTabCell.h"


@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
    [self setupTableview];
    
    [self.tapView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismis)]];
    [self.tableview registerNib:[UINib nibWithNibName:@"HBSearchTabCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
}
- (void)dismis
{
    [self.view removeFromSuperview];
}
- (void)setupTableview
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
    return self.tablearray.count+1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        HBSearchTabCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (indexPath.row==0) {
        cell.titleLab.text = @"全部商品";
    }else{
        Shopcat *cat = self.tablearray[indexPath.row-1];
        cell.titleLab.text = cat.cat_name;
    }
    
        return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
//    return view;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.01;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        self.clickRowData(nil);
    }else{
        Shopcat *cat = self.tablearray[indexPath.row-1];
        self.clickRowData(cat);
    }
    
    [self dismis];
    
}

- (void)setHeader
{
    self.tableview.tableHeaderView = self.header;
}

- (void)setTablearray:(NSArray *)tablearray
{
    _tablearray = tablearray;
    [self.tableview reloadData];
}
//点击重置按钮
- (IBAction)clickNewSetBtn:(UIButton *)sender {
    
}
//点击确定按钮
- (IBAction)clickConfBtn:(UIButton *)sender {
    
}
@end
