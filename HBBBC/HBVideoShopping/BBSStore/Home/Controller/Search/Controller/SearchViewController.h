//
//  SearchViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/8.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopModel.h"
@interface SearchViewController : HBRootViewController<UITableViewDataSource,UITableViewDelegate>

//表单
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSArray *tablearray;//数据源
//设置头部视图
- (void)setHeader;

//选中
@property (strong, nonatomic) IBOutlet UIView *header;
@property (nonatomic, strong) void (^searchAction)(NSDictionary *selectResult);
@property (weak, nonatomic) IBOutlet UIButton *setBtn;


@property (nonatomic, copy) void(^clickRowData)(Shopcat *cat);

- (IBAction)clickNewSetBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *confBtn;
- (IBAction)clickConfBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *tapView;



@end
