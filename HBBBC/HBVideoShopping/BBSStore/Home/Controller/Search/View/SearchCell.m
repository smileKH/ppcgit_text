//
//  SearchCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/8.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectArray = [NSMutableArray array];
    }
    return self;
}

- (void)setItemsArray:(NSArray *)itemsArray
{
    _itemsArray = itemsArray;
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    int margin = 5;//间隙
    int width = 70;//格子的宽
    int height = 25;//格子的高

    for (NSInteger i = 0; i < itemsArray.count; i ++) {
        NSInteger row = i/3;
        NSInteger col = i%3;
        Children *ch = itemsArray[i];
        UIButton *btns = [UIButton buttonWithType:UIButtonTypeCustom];
        btns.frame = CGRectMake(10+col*(width+margin), 10+row*(height+margin), width, height);
        [btns setTitle:ch.cat_name forState:UIControlStateNormal];
        [btns setTitleColor:black_Color forState:UIControlStateNormal];
        [btns setTitleColor:white_Color    forState:UIControlStateSelected];
        btns.titleLabel.font = [UIFont systemFontOfSize:13];
        [btns addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        btns.layer.masksToBounds = YES;
        btns.layer.cornerRadius = 2;
        btns.backgroundColor = MAINBGCOLOR;
        [self addSubview:btns];
    }
}
- (void)click:(UIButton *)btn

{
    btn.selected = !btn.selected;
    btn.backgroundColor = btn.selected ? MAINTextCOLOR : MAINBGCOLOR;
}
- (void)setModel:(Shopcat *)model
{
    _model = model;
    self.itemsArray = model.children;
    
}
@end
