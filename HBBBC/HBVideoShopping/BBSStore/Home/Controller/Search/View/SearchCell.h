//
//  SearchCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/8.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopModel.h"
@interface SearchCell : UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
@property (nonatomic, strong) NSMutableArray *selectArray;//选中的目标
@property (nonatomic, strong) NSArray *itemsArray;//单元格

@property (nonatomic, strong) Shopcat *model;
@end
