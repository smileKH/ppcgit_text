//
//  SearchHeader.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/8.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "SearchHeader.h"
#import "Is.h"
@implementation SearchHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = white_Color;
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 50, 20)];
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        self.titleLabel.tag = 1;
        [self addSubview:self.titleLabel];
        self.scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(30, 10, self.bounds.size.width-55, 20)];
        self.scrollview.contentSize = CGSizeMake(self.bounds.size.width-35, 20);
        self.scrollview.delegate = self;
        self.scrollview.showsVerticalScrollIndicator = NO;
        self.scrollview.showsHorizontalScrollIndicator = NO;
        [self addSubview:self.scrollview];
    }
    return self;
}

- (void)setSelectsArray:(NSArray *)selectsArray
{
    _selectsArray = selectsArray;
    [self.scrollview.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat x = 0;
    for (NSInteger i = 0; i < selectsArray.count;  i ++ ) {
        CGFloat x1 = [Is textLengthWithString:selectsArray[i] withFont:[UIFont systemFontOfSize:12]];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10 + x, 7, x1, 15)];
        label.textColor = MAINCOLOR;
        label.layer.masksToBounds = YES;
        label.layer.cornerRadius = 2;
        label.font = [UIFont systemFontOfSize:10];
        label.layer.borderColor = MAINCOLOR.CGColor;
        label.layer.borderWidth=0.5;
        label.text = selectsArray[i];
        label.textAlignment = NSTextAlignmentCenter;
        [self.scrollview addSubview:label];
        x = x +x1+8;
    }
}
@end
