//
//  SearchHeader.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/8.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchHeader : UITableViewHeaderFooterView<UIScrollViewDelegate>
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIScrollView *scrollview;
@property (nonatomic ,strong) NSArray *selectsArray;
@end
