//
//  HBSearchTabCell.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/18.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBSearchTabCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end
