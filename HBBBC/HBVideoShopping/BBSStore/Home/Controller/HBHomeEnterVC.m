//
//  HBHomeEnterVC.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/7.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBHomeEnterVC.h"

@interface HBHomeEnterVC ()<UIWebViewDelegate,UIScrollViewDelegate>
@property (nonatomic , strong)UIWebView * webView;
@property (nonatomic, strong) NSString * im_account_qq;
@end

@implementation HBHomeEnterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"入驻";
    //添加子视图
    [self addRegisteredProtocolSubView];
    
    //请求平台
    [self requestQQData];
}
#pragma mark ==========请求平台工具==========
-(void)requestQQData{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:COMMON_PLATFORM_CST params:nil completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
                weakSelf.im_account_qq = dic[@"im_account_qq"];
            }
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark - 添加子视图
-(void)addRegisteredProtocolSubView{
    if (!self.webView) {
        self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-bNavAllHeight)];
        self.webView.backgroundColor = [UIColor clearColor];
        self.webView.delegate = self;
        self.webView.scrollView.delegate = self;
        self.webView.tag = 1314;
        [self.view addSubview:self.webView];
        self.webView.scalesPageToFit=YES;
        NSString *path1 = [[NSBundle mainBundle] pathForResource:@"bbs_homeZhu" ofType:@"png"];
        if ([HBHuTool isJudgeString:path1]) {
            [app showToastView:@"没找到文件"];
            return;
        }
        NSURL *url = [NSURL fileURLWithPath:path1];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [self.webView loadRequest:request];
        
        //重新监听
        [self addObserverForWebViewContentSize];
    }
}

- (void)addObserverForWebViewContentSize{
    [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:0 context:nil];
}
- (void)removeObserverForWebViewContentSize{
    [self.webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    //在这里边添加你的代码
    [self layoutCell];
}
//设置footerView的合理位置
- (void)layoutCell{
    //取消监听，因为这里会调整contentSize，避免无限递归
    [self removeObserverForWebViewContentSize];
    UIView *viewss = [self.view viewWithTag:99999];
    CGSize contentSize = self.webView.scrollView.contentSize;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"立即咨询" forState:UIControlStateNormal];
    [button setTitleColor:white_Color forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:[UIImage createImageWithColor:UIColorFromHex(0xE6E90E)] forState:UIControlStateNormal];
    button.tag = 99999;
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 4;
    button.layer.borderColor = UIColorFromHex(0xE6E90E).CGColor;
    button.layer.borderWidth = 1.0f;
    button.userInteractionEnabled = YES;
    button.frame = CGRectMake(20, contentSize.height-90, SCREEN_WIDTH-40, 40);
    [self.webView.scrollView addSubview:button];
    //    UIView *vi = [[UIView alloc]init];
    //    vi.backgroundColor = red_Color;
    //    vi.userInteractionEnabled = YES;
    //    vi.tag = 99999;
    //    vi.frame = CGRectMake(0, contentSize.height-90, SCREEN_WIDTH, 50);
    //    [self.webView.scrollView addSubview:vi];
    self.webView.scrollView.contentSize = CGSizeMake(contentSize.width, contentSize.height);
    //重新监听
    [self addObserverForWebViewContentSize];
}

-(void)clickButton:(UIButton *)button{
    NSString *str = [NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=web",self.im_account_qq];
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

