//
//  HBDemandListModel.h
//  HBVideoShopping
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>


@class HBDemandListUserModel;
@class HBDemandListPicUrlModel;
NS_ASSUME_NONNULL_BEGIN

@interface HBDemandListModel : NSObject
@property (nonatomic ,strong)NSString *like;
@property (nonatomic ,strong)NSString *demand_address;
@property (nonatomic ,strong)NSString *favorite;
@property (nonatomic ,strong)NSString *demand_status;
@property (nonatomic ,strong)NSString *demand_desc;
@property (nonatomic ,strong)NSNumber *created_time;
@property (nonatomic ,strong)NSString *is_favorite;
@property (nonatomic ,strong)NSString *longitude;
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *demand_id;
@property (nonatomic ,strong)NSString *latitude;
@property (nonatomic ,strong)NSString *comment;
@property (nonatomic ,strong)HBDemandListPicUrlModel *image_default_id;
@property (nonatomic ,strong)NSString *modified_time;
@property (nonatomic ,strong)HBDemandListPicUrlModel *video_pic_url;
@property (nonatomic ,strong)NSString *complaint;
@property (nonatomic ,strong)HBDemandListUserModel *user;
@property (nonatomic ,strong)NSString *video_path;
@property (nonatomic ,strong)NSString *is_like;
@property (nonatomic ,strong)NSArray *list_image;
@end


@interface HBDemandListUserModel : NSObject
@property (nonatomic ,strong)NSString *user_id;
@property (nonatomic ,strong)NSString *name;
@property (nonatomic ,strong)NSString *portrait_url;
@end

@interface HBDemandListPicUrlModel : NSObject
@property (nonatomic ,strong)NSString *url;
@property (nonatomic ,strong)NSString *height;//高度
@property (nonatomic ,strong)NSString *width;//宽
@end
NS_ASSUME_NONNULL_END
