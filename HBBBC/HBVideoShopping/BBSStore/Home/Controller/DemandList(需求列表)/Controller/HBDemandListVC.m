//
//  HBDemandListVC.m
//  HBVideoShopping
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBDemandListVC.h"
//#import "HBNewSearchResultCollView.h"
#import "HBDemandListModel.h"
#import "HBDemandPushVC.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "HBNewDoubleListCollectionCell.h"
#import "HBReandDetailVC.h"
#import "PFAPP.h"
//#import "SDTimeLineTableViewController.h"
@interface HBDemandListVC ()<UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic , strong)UIView * selectView;
@property (nonatomic ,strong)UIButton *addPushButton;//发布按钮
@property (nonatomic ,strong)NSString *rangeStr;//市内，附近，不限
@end

@implementation HBDemandListVC
#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

- (void)listDidAppear {
    [self.view addSubview:self.addPushButton];
    [self.view bringSubviewToFront:self.addPushButton];
}

- (void)listDidDisappear {}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHidenNaviBar = YES;
    self.view.backgroundColor = Color_BG;
    self.StatusBarStyle = UIStatusBarStyleDefault;
    self.rangeStr = @"0";
    //添加子视图
    [self setupUI];
    //请求数据
    [self requestGoodsList:YES];
    //点击头部视图
    [self addSelectViewSubView];
    //通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateSuccessChange) name:@"HBDemandPushSuccessChange" object:nil];
}
#pragma mark ==========通知改变==========
-(void)updateSuccessChange{
    //请求数据
    [self requestGoodsList:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateLayoutForOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateLayoutForOrientation:toInterfaceOrientation];
}

- (void)updateLayoutForOrientation:(UIInterfaceOrientation)orientation {
    CHTCollectionViewWaterfallLayout *layout =
    (CHTCollectionViewWaterfallLayout *)self.collectionView.collectionViewLayout;
    layout.columnCount = UIInterfaceOrientationIsPortrait(orientation) ? 2 : 3;
}
#pragma mark ==========添加子视图==========
-(void)setupUI{
//    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
//    layout.itemSize = CGSizeMake(SCREEN_WIDTH/2, 200);
//    layout.minimumLineSpacing = 10;
//    layout.minimumInteritemSpacing = 5;
    
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.columnCount  = 2;
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
//    layout.headerHeight = 15;
//    layout.footerHeight = 10;
    layout.minimumColumnSpacing = 10;
    layout.minimumInteritemSpacing = 5;
    
    CGFloat collectionHeight = SCREEN_HEIGHT-45-bTabBarHeight-bNavAllHeight-40-kBottomSafeHeight;
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,40, SCREEN_WIDTH, collectionHeight) collectionViewLayout:layout];
    self.collectionView.backgroundColor = Color_BG;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerClass:[HBNewDoubleListCollectionCell class] forCellWithReuseIdentifier:NSStringFromClass([HBNewDoubleListCollectionCell class])];
    [self.view addSubview:self.selectView];
    [self.view addSubview:self.collectionView];
//    self.collectionView.emptyDataSetSource = self;
//    self.collectionView.emptyDataSetDelegate = self;
    WEAKSELF;
//    self.collectionView.clickCollectionItem = ^(NSIndexPath *indexPath){
//        //点击数据的时候
//        [weakSelf clickCollectionDataIndexPath:indexPath];
//    };
//    self.collectionView.scrollViewDidBlock = ^(UIScrollView *scrollView) {
//
//    };
    //上拉加载 下拉刷新
    //默认block方法：设置下拉刷新
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestGoodsList:YES];
    }];
    
    
    
    //默认block方法：设置上拉加载更多
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestGoodsList:NO];
    }];
}
#pragma mark ==========点击上面三个点击按钮==========
-(void)addSelectViewSubView{
    //计算宽高
    CGFloat spacing = 12;
    CGFloat btnWidth = (SCREEN_WIDTH-2*10-2*spacing)/3;
    CGFloat btnHeight = 28;
    NSArray *arr = @[@"市内",@"附近",@"不限"];
    for (int i=0; i<arr.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(10+(btnWidth+10)*i, 6, btnWidth, btnHeight);
        [btn setTitle:arr[i] forState:UIControlStateNormal];
        [btn setTitleColor:MAINCOLOR forState:UIControlStateNormal];
        [btn setTitleColor:MAINTextCOLOR forState:UIControlStateSelected];
        btn.titleLabel.font = Font(14);
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 14;
        if (i==2) {
            btn.selected = YES;
            btn.layer.borderColor = MAINTextCOLOR.CGColor;
        }else{
            btn.layer.borderColor = MAINCOLOR.CGColor;
        }
        btn.clipsToBounds = YES;
        btn.tag = i;
        [btn addTarget:self action:@selector(clickSelectBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.selectView addSubview:btn];
    }
}
#pragma mark ==========点击按钮事件==========
-(void)clickSelectBtn:(UIButton *)button{
    for (UIView *subView in self.selectView.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *aSubBtn = (UIButton *)subView;
            aSubBtn.selected = NO;
            aSubBtn.layer.borderColor = MAINCOLOR.CGColor;
        }
    }
    button.selected = YES;
    button.layer.borderColor = MAINTextCOLOR.CGColor;
    
    if (button.tag==0) {
        self.rangeStr = @"50";
    }else if (button.tag==1){
        self.rangeStr = @"8";
    }else{
        self.rangeStr = @"0";
    }
    //请求数据
    [self requestGoodsList:YES];
}


#pragma mark - 点击商品的时候
-(void)clickCollectionDataIndexPath:(NSIndexPath *)indexPath{

}

- (void)requestGoodsList:(BOOL)isRefresh
{
    if (isRefresh) {
        self.pageNo = 1;
    }
    else{
        self.pageNo++;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"page_size"] = @(10);
    parameters[@"page"] = @(self.pageNo);
    if (![self.rangeStr isEqualToString:@"0"]) {
        parameters[@"range"] = self.rangeStr;
    }
    parameters[@"latitude"] = @([PFAPP sharedInstance].lat);
    parameters[@"longitude"] = @([PFAPP sharedInstance].lng);
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:DEMAND_INDEX_TOPAPI params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            if (weakSelf.pageNo==1) {
                [self.allData removeAllObjects];
            }
            
            [weakSelf.collectionView.mj_footer endRefreshing];
            [weakSelf.collectionView.mj_header endRefreshing];
            NSDictionary *dic = response.result[@"data"];
            if (ValidDict(dic)) {
//                NSDictionary *pagDic = dic[@"pagers"];
//                int number = [pagDic[@"current_page"]intValue];
                NSArray *arr = [dic objectForKey:@"demand_list"];
                if ([HBHuTool judgeArrayIsEmpty:arr]) {
                    //数据转模型
                    NSArray *array = [HBDemandListModel mj_objectArrayWithKeyValuesArray:arr];
                    //正常有数据 隐藏无数据view
                    if (array != nil && array.count > 0) {
                        [weakSelf.allData addObjectsFromArray:array];
                        //设置视图
                        //[weakSelf setGoodsViewController];
                    }
                }else{
                    if (weakSelf.pageNo>0) {
                        [app showToastView:Text_NoMoreData];
                        [weakSelf.collectionView.mj_footer endRefreshingWithNoMoreData];
                    }
                }
            }
        }else{
            [app showToastView:response.message];
        }
        if ([HBHuTool judgeArrayIsEmpty:weakSelf.allData]) {
            //有数据
            [weakSelf hideContentNullPromptView];
        }else{
            [weakSelf showContentNullPromptNormal];
        }
        [weakSelf.collectionView reloadData];
    }];
    
}





#pragma mark ==========getter==========
-(UIView *)selectView{
    if (!_selectView) {
        _selectView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
        _selectView.backgroundColor = clear_Color;
    }
    return _selectView;
}

#pragma mark - 无数据代理方法 -
//点击按钮代理方法，可在对应的子类中重写这个方法
- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button{
    [self requestGoodsList:YES];
}
-(UIButton *)addPushButton{
    if (!_addPushButton) {
        _addPushButton = ({
            //创建按钮
            CGFloat collectionHeight = SCREEN_HEIGHT-45-bNavBarHeight-bNavAllHeight-38-kBottomSafeHeight;
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(SCREEN_WIDTH-60, collectionHeight-10, 60, 37.5);
            [button setImage:ImageNamed(@"bs_push_addImg") forState:UIControlStateNormal];
            //添加点击事件
            [button addTarget:self action:@selector(clickAddPushButton:) forControlEvents:UIControlEventTouchUpInside];
            
            button;
        });
    }
    return _addPushButton;
}
#pragma mark ==========点击发布需求==========
-(void)clickAddPushButton:(UIButton *)button{
    if ([userManager judgeLoginState]) {
        HBDemandPushVC *vc = [[HBDemandPushVC alloc]init];
        [self.upController.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark ==========collectionViewDelegate==========
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.allData.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HBNewDoubleListCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HBNewDoubleListCollectionCell" forIndexPath:indexPath];
    cell.model = self.allData[indexPath.row];
    //[cell reloadSetModel:self.dataArray[indexPath.row] andType:1];
    cell.backgroundColor = Color_BG;
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    HBDemandListModel *model =   self.allData[indexPath.row];
    CGFloat leftSpacing = HOME_SPACING_LEFT;
    CGFloat twoRightWidth = (SCREEN_WIDTH-2*leftSpacing-10)/2;
    if ([HBHuTool isJudgeString:model.video_path]) {
        //为空，是图片
        CGFloat retrHeight = 0;
        CGFloat retrWidth = 0;
        if (model.image_default_id!=nil) {
            retrWidth = [model.image_default_id.width floatValue];
            retrHeight = [model.image_default_id.height floatValue];
        }
        //计算 根据屏幕计算
        CGFloat totalSale = retrHeight*twoRightWidth;
        if (totalSale<1) {
            totalSale = 100;
            retrWidth = 100;
        }
        CGFloat topBigHeight = totalSale/retrWidth+140;
        return CGSizeMake(twoRightWidth, topBigHeight);
//        CGFloat bottomBigHeight = twoRightWidth*model.image_default_id.height/model.image_default_id.width + 140;
//        return CGSizeMake(twoRightWidth, bottomBigHeight);
    }else{
//        CGFloat bottomBigHeight = twoRightWidth*model.video_pic_url.height/model.video_pic_url.width+ 140;
//        return CGSizeMake(twoRightWidth, bottomBigHeight);
        
        CGFloat retrHeight = 0;
        CGFloat retrWidth = 0;
        if (model.video_pic_url!=nil) {
            retrWidth = [model.video_pic_url.width floatValue];
            retrHeight = [model.video_pic_url.height floatValue];
        }
        //计算 根据屏幕计算
        CGFloat totalSale = retrHeight*twoRightWidth;
        if (totalSale<1) {
            totalSale = 100;
            retrWidth = 100;
        }
        CGFloat topBigHeight = totalSale/retrWidth+140;
        return CGSizeMake(twoRightWidth, topBigHeight);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //点击每一项
//    if (self.clickCollectionItem) {
//        self.clickCollectionItem(indexPath);
//    }
    
    HBDemandListModel *model =   self.allData[indexPath.row];
    HBReandDetailVC *vc = [[HBReandDetailVC alloc]init];
    vc.listModel = model;
    [self.upController.navigationController pushViewController:vc animated:YES];
    
//    HBDemandListModel *model =   self.allData[indexPath.row];
//    SDTimeLineTableViewController *vc = [[SDTimeLineTableViewController alloc]init];
//    vc.listModel = model;
//    [self.upController.navigationController pushViewController:vc animated:YES];
}


@end
