//
//  HBNewSearchResultCollView.h
//  HBGoldRoom
//
//  Created by Mac on 2019/12/27.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HBNewSearchResultCollView : UICollectionView<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic,strong) NSArray *dataArray;
@property (nonatomic, strong) NSString * detailString;
@property (nonatomic , copy)void(^clickCollectionItem)(NSIndexPath *indexPath);
@property (nonatomic , copy)void(^scrollViewDidBlock)(UIScrollView *scrollView);

@end

NS_ASSUME_NONNULL_END
