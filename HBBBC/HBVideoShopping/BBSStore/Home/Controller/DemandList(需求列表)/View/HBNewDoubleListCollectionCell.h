//
//  HBNewDoubleListCollectionCell.h
//  HBGoldRoom
//
//  Created by Mac on 2019/12/26.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBNewBaseCollectionListViewCell.h"
#import "HBDemandListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HBNewDoubleListCollectionCell : HBNewBaseCollectionListViewCell
@property (nonatomic ,strong)HBDemandListModel *model;
////类型 1兑换礼包 2邀新专区 3 已推好货
//-(void)reloadSetModel:(HBNewGoodsListModel *)model andType:(NSInteger)index;
//@property (nonatomic ,copy)void(^clickGoodsListCollectionBlock)(HBNewGoodsListModel *model , NSInteger index);
@property (nonatomic ,weak)UIViewController *upController;
@end

NS_ASSUME_NONNULL_END
