//
//  HBNewSearchResultCollView.m
//  HBGoldRoom
//
//  Created by Mac on 2019/12/27.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBNewSearchResultCollView.h"
#import "HBNewDoubleListCollectionCell.h"
@interface HBNewSearchResultCollView()
@property (nonatomic, strong) UITableView *contentTableView;
@property (nonatomic, strong) UICollectionView *contentCollectionView;

@property (nonatomic, assign) BOOL isDoubleList;

@end
@implementation HBNewSearchResultCollView

-(instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    if (self = [super initWithFrame:frame collectionViewLayout:layout]) {
        
        self.backgroundColor = white_Color;
        self.dataSource = self;
        self.delegate = self;

        
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        [self registerClass:[HBNewDoubleListCollectionCell class] forCellWithReuseIdentifier:NSStringFromClass([HBNewDoubleListCollectionCell class])];
    }
    return self;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HBNewDoubleListCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HBNewDoubleListCollectionCell" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    //[cell reloadSetModel:self.dataArray[indexPath.row] andType:1];
    cell.backgroundColor = Color_BG;
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = (SCREEN_WIDTH-2*HOME_SPACING_LEFT-10)/2;
    CGFloat height = (SCREEN_WIDTH-2*HOME_SPACING_LEFT-10)/2 + 140;
    return CGSizeMake(width, height);
    
//  HBDemandListModel *model =   self.dataArray[indexPath.row];
//    CGFloat leftSpacing = HOME_SPACING_LEFT;
//    CGFloat twoRightWidth = (SCREEN_WIDTH-2*leftSpacing-10)/2;
//    if ([HBHuTool isJudgeString:model.video_path]) {
//        //为空，是图片
//        CGFloat bottomBigHeight = twoRightWidth*model.image_default_id.height/model.image_default_id.width+ 140;
//        return CGSizeMake(twoRightWidth, bottomBigHeight);
//    }else{
//        CGFloat bottomBigHeight = twoRightWidth*model.video_pic_url.height/model.video_pic_url.width+ 140;
//        return CGSizeMake(twoRightWidth, bottomBigHeight);
//    }
}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0, HOME_SPACING_LEFT, 0, HOME_SPACING_LEFT);
//}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //点击每一项
    if (self.clickCollectionItem) {
        self.clickCollectionItem(indexPath);
    }
}

#pragma mark - set方法
-(void)setDataArray:(NSArray *)dataArray
{
    _dataArray = dataArray;
    dispatch_async(dispatch_get_main_queue(), ^{
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        //刷新数据
        [self reloadData];
        [CATransaction commit];
    });
    
}
//#pragma mark - ServiceLayoutDelegate
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section;{
//    CGSize size={SCREEN_WIDTH,40};
//    return size;
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.scrollViewDidBlock) {
        self.scrollViewDidBlock(scrollView);
    }
}
@end
