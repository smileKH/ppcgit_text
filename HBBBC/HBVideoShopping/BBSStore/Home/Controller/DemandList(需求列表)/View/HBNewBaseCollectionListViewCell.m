//
//  HBNewBaseCollectionListViewCell.m
//  HBGoldRoom
//
//  Created by Mac on 2019/12/25.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBNewBaseCollectionListViewCell.h"

@implementation HBNewBaseCollectionListViewCell
#pragma mark =======getter==========
-(UIImageView *)goodsImgeView{
    if (!_goodsImgeView) {
        _goodsImgeView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:ZAN_WU_TUPIAN];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _goodsImgeView ;
}
-(UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"2019-11-01";//内容显示
            label.textColor = white_Color;//设置字体颜色
            label.font = Font(9);//设置字体大小
            label.textAlignment = NSTextAlignmentCenter;//设置对齐方式
            label.numberOfLines = 1; //行数
            label.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.4];
            label.layer.cornerRadius = 16/2;
            label.clipsToBounds = YES;
           
            
            label ;
        }) ;
    }
    return _timeLabel ;
}
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"哪位大佬知道，视频中的这种衣 服是什么款式呀！";//内容显示
            label.textColor = black_Color;//设置字体颜色
            label.font = Font(11);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 2; //行数
            
            label ;
        }) ;
    }
    return _titleLabel ;
}
-(UIImageView *)videoImgView{
    if (!_videoImgView) {
        _videoImgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:@"bs_play_img"];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _videoImgView ;
}
-(UIImageView *)headerImgView{
    if (!_headerImgView) {
        _headerImgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:@"bbs_header"];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView.layer.cornerRadius = 25/2;
            imgView.clipsToBounds = YES;
            imgView ;
        }) ;
    }
    return _headerImgView ;
}
-(UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = black_Color;//设置字体颜色
            label.font = Font(10);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _nameLabel ;
}
-(UILabel *)phoneLabel{
    if (!_phoneLabel) {
        _phoneLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(10);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _phoneLabel ;
}
-(UILabel *)addressLabel{
    if (!_addressLabel) {
        _addressLabel = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(10);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _addressLabel ;
}
-(UIImageView *)pushImgView{
    if (!_pushImgView) {
        _pushImgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:@"bs_daohang_img"];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _pushImgView ;
}

@end
