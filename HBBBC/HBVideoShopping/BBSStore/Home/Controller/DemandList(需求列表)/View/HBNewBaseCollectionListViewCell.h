//
//  HBNewBaseCollectionListViewCell.h
//  HBGoldRoom
//
//  Created by Mac on 2019/12/25.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HBNewBaseCollectionListViewCell : UICollectionViewCell

@property (nonatomic ,strong)UIImageView *goodsImgeView;
@property (nonatomic, strong) UILabel *timeLabel;//时间
@property (nonatomic, strong) UILabel *titleLabel;//标题
@property (nonatomic, strong) UIImageView *videoImgView;//播放图片
@property (nonatomic, strong) UIImageView *headerImgView;//头像
@property (nonatomic, strong) UILabel *nameLabel;//名称
@property (nonatomic, strong) UILabel *phoneLabel;//电话号码
@property (nonatomic, strong) UILabel *addressLabel;//地址
@property (nonatomic ,strong)UIImageView *pushImgView;//导航

@end

NS_ASSUME_NONNULL_END
