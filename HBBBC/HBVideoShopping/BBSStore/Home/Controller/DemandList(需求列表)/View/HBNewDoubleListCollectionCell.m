//
//  HBNewDoubleListCollectionCell.m
//  HBGoldRoom
//
//  Created by Mac on 2019/12/26.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBNewDoubleListCollectionCell.h"
#import "UIButton+ImageTitleSpacing.h"

@interface HBNewDoubleListCollectionCell ()
@property (nonatomic ,strong)UIView *bgView;//背景
@property (nonatomic ,strong)UIView *moreBotView;
@property (nonatomic ,strong)UIView *lineBotView;
@property (nonatomic, strong) NSMutableArray *btnArr;
@property (nonatomic ,strong)MASConstraint *bgImgViewHeight;
@end
@implementation HBNewDoubleListCollectionCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //添加子视图
        [self setupUI];
    }
    return self;
}
-(void)setModel:(HBDemandListModel *)model{
    _model = model;
    CGFloat leftSpacing = HOME_SPACING_LEFT;
    CGFloat twoRightWidth = (SCREEN_WIDTH-2*leftSpacing-10)/2;
    if ([HBHuTool isJudgeString:model.video_path]) {
        //为空，是图片
        CGFloat retrHeight = 0;
        CGFloat retrWidth = 0;
        if (model.image_default_id!=nil) {
            retrWidth = [model.image_default_id.width floatValue];
            retrHeight = [model.image_default_id.height floatValue];
        }
        //计算 根据屏幕计算
        CGFloat totalSale = retrHeight*twoRightWidth;
        if (totalSale<1) {
            totalSale = 100;
            retrWidth = 100;
        }
        CGFloat topBigHeight = totalSale/retrWidth;
        [self.bgImgViewHeight uninstall];
        [self.goodsImgeView mas_updateConstraints:^(MASConstraintMaker *make) {
           self.bgImgViewHeight = make.height.mas_equalTo(topBigHeight);
        }];
        [self.goodsImgeView sd_setImageWithURL:[NSURL URLWithString:model.image_default_id.url] placeholderImage:ImageNamed(ZAN_WU_TUPIAN)];
        //显示播放按钮
        self.videoImgView.hidden = YES;

    }else{
        CGFloat retrHeight = 0;
        CGFloat retrWidth = 0;
        if (model.video_pic_url!=nil) {
            retrWidth = [model.video_pic_url.width floatValue];
            retrHeight = [model.video_pic_url.height floatValue];
        }
        //计算 根据屏幕计算
        CGFloat totalSale = retrHeight*twoRightWidth;
        if (totalSale<1) {
            totalSale = 100;
            retrWidth = 100;
        }
        CGFloat topBigHeight = totalSale/retrWidth;
        [self.bgImgViewHeight uninstall];
        [self.goodsImgeView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.bgImgViewHeight = make.height.mas_equalTo(topBigHeight);
        }];


      [self.goodsImgeView sd_setImageWithURL:[NSURL URLWithString:model.video_pic_url.url] placeholderImage:ImageNamed(ZAN_WU_TUPIAN)];
        //隐藏播放按钮
        self.videoImgView.hidden = NO;
    }
    
    self.titleLabel.text = model.demand_desc;
    [self.headerImgView sd_setImageWithURL:[NSURL URLWithString:model.user.portrait_url] placeholderImage:ImageNamed(@"bbs_header")];
    self.nameLabel.text = model.user.name;
    self.phoneLabel.text = @"";
    self.addressLabel.text = model.demand_address;
    self.timeLabel.text =[XH_Date_String dateStringWithFormat: @"yyyy-MM-dd" tenNumber:model.created_time];
    
    NSString *oneStr = [NSString stringWithFormat:@"%@",model.complaint]; //今日
    NSString *twoStr = [NSString stringWithFormat:@"%@",model.favorite];//预计收入
    NSString *threeStr = [NSString stringWithFormat:@"%@",model.comment];//本月
    NSString *fourStr = [NSString stringWithFormat:@"%@",model.like];//累计
    NSArray *titleArr = @[oneStr,twoStr,threeStr,fourStr];
    
    BOOL iscollect = [model.is_favorite boolValue];
    BOOL isLike = [model.is_like boolValue];
    NSArray *selectArr = @[@(NO),@(iscollect),@(NO),@(isLike)];
    
    for (UIButton *btn in _btnArr) {
        NSInteger index = [_btnArr indexOfObject:btn];
        BOOL isSelect = [[selectArr objectAtIndex:index] boolValue];
        NSString *title = [titleArr objectAtIndex:index];
        [btn setTitle:title forState:UIControlStateNormal];
        btn.selected = isSelect;
        [btn layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleTop imageTitleSpace:5];
    }
}
//@property (nonatomic ,strong)UIImageView *goodsImgeView;
//@property (nonatomic, strong) UILabel *timeLabel;//时间
//@property (nonatomic, strong) UILabel *titleLabel;//标题
//@property (nonatomic, strong) UIImageView *videoImgView;//播放图片
//@property (nonatomic, strong) UIImageView *headerImgView;//头像
//@property (nonatomic, strong) UILabel *nameLabel;//名称
//@property (nonatomic, strong) UILabel *phoneLabel;//电话号码
//@property (nonatomic, strong) UILabel *addressLabel;//地址
//@property (nonatomic ,strong)UIImageView *pushImgView;//导航

#pragma mark ==========子视图==========
-(void)setupUI{
    
     CGFloat width = (SCREEN_WIDTH-2*15-10)/2;
        
        [self addSubview:self.bgView];
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.right.equalTo(self);
        }];
    
    
        
        [self.bgView addSubview:self.goodsImgeView];
        [self.goodsImgeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.bgView);
           self.bgImgViewHeight = make.height.mas_equalTo(width);
        }];
        
        [self.goodsImgeView addSubview:self.timeLabel];
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.goodsImgeView).offset(5);
            make.bottom.equalTo(self.goodsImgeView).offset(-5);
            make.size.mas_equalTo(CGSizeMake(69, 16));
        }];
        
        [self.goodsImgeView addSubview:self.videoImgView];
        [self.videoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.goodsImgeView).offset(-8);
            make.right.equalTo(self.goodsImgeView).offset(-5);
            make.size.mas_equalTo(CGSizeMake(16, 16));
        }];
        
        
        [self.bgView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.goodsImgeView.mas_bottom).offset(7);
            make.left.equalTo(self.bgView).offset(10);
            make.right.equalTo(self.bgView).offset(-10);
        }];
        
        
       [self.bgView addSubview:self.moreBotView];
        [self.moreBotView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.bgView);
            make.right.left.equalTo(self.bgView);
            make.height.mas_equalTo(44);
        }];
    
    [self.bgView addSubview:self.lineBotView];
    [self.lineBotView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.moreBotView.mas_top);
        make.left.equalTo(self.bgView).offset(10);
        make.right.equalTo(self.bgView).offset(-10);
        make.height.mas_equalTo(1);
    }];
    
    [self.bgView addSubview:self.pushImgView];
    [self.pushImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.lineBotView.mas_top).offset(-10);
        make.right.equalTo(self.bgView).offset(-10);
        make.size.mas_equalTo(CGSizeMake(13, 13));
    }];
    
    [self.bgView addSubview:self.addressLabel];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.pushImgView);
        make.right.equalTo(self.pushImgView.mas_left).offset(-10);
        make.left.equalTo(self.bgView).offset(10);
    }];
    
    [self.bgView addSubview:self.headerImgView];
    [self.headerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.addressLabel.mas_top).offset(-10);
        make.left.equalTo(self.bgView).offset(10);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    [self.bgView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.headerImgView.mas_centerY).offset(-2);
        make.left.equalTo(self.headerImgView.mas_right).offset(7);
    }];
    
    [self.bgView addSubview:self.phoneLabel];
    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerImgView.mas_centerY).offset(2);
        make.left.equalTo(self.headerImgView.mas_right).offset(7);
    }];
    [self addMoreViewButton];
}
#pragma mark ==========添加子视图==========
-(void)addMoreViewButton{
    NSArray *bottomTitleArr = @[@"0",@"0",@"0",@"0"];
    NSArray *bottomImageArr = @[@"bs_toushu_img",@"bs_shouchang_coll",@"bs_pinglun_img",@"bs_dianzan_no"];
    NSArray *bottomSelectImageArr = @[@"bs_toushu_img",@"bs_shouchang_collSelect",@"bs_pinglun_img",@"bs_dianzan_select"];
    NSMutableArray *btnArr = [NSMutableArray array];
    for (int i = 0; i<bottomTitleArr.count; i++) {
        NSString *title = [bottomTitleArr objectAtIndex:i];
        NSString *imageName = [bottomImageArr objectAtIndex:i];
        NSString *selectImageName = [bottomSelectImageArr objectAtIndex:i];
        UIButton *btn = [[UIButton alloc] init];
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:selectImageName] forState:UIControlStateSelected];
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:black_Color forState:UIControlStateNormal];
        btn.titleLabel.font = Font(10);
        [self.moreBotView addSubview:btn];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleTop imageTitleSpace:5];
        btn.tag = i;
        [btnArr addObject:btn];
        
    }
    _btnArr = btnArr;
    WEAKSELF;
    [btnArr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:10 tailSpacing:10];
    [btnArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.moreBotView).offset(0);
        make.height.mas_equalTo(44);
    }];
}
- (void)btnClick:(UIButton *)sender{
    //    sender.selected = !sender.selected;
//    if (self.toolClickBlock) {
//        self.toolClickBlock(sender.tag, sender.selected);
//    }
//
//    if (sender.tag == 1) {
//        //收藏
//        [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_PUB_COLLECT object:sender];
//    }else if (sender.tag == 2){
//        //点赞
//        [[NSNotificationCenter defaultCenter] postNotificationName:NOTI_COMMENT_LIKE object:sender];
//    }
}
#pragma mark ==========getter==========
-(UIView *)bgView{
    if (!_bgView) {
        _bgView = ({
            UIView *view = [[UIView alloc]init];//初始化控件
            view.backgroundColor = white_Color;
            view.layer.cornerRadius = 6;
            view.clipsToBounds = YES;
            view ;
        }) ;
    }
    return _bgView ;
}
-(UIView *)moreBotView{
    if (!_moreBotView) {
        _moreBotView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _moreBotView ;
}
-(UIView *)lineBotView{
    if (!_lineBotView) {
        _lineBotView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
            
            view ;
        }) ;
    }
    return _lineBotView ;
}
@end
