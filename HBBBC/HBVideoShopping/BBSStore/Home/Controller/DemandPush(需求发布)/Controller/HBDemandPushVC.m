//
//  HBDemandPushVC.m
//  HBVideoShopping
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBDemandPushVC.h"
#import "HBEvaluationOrderTabCell.h"
//#import "HBUploadImageManage.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "HBMaterialTZImagePickerHelper.h"
#import "MoaHttpClient.h"
#import "MapChooseAddressVC.h"
#import "HBPushVideoModel.h"
#import "PFAPP.h"
@interface HBDemandPushVC ()<UITableViewDelegate,UITableViewDataSource,HBEvaluationOrderTabCellDelegate,MapChooseAddressVCDelegate>
{
    CGFloat _cellHeight[2];
}

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic ,strong)NSString *defineImgStr;//默认图
@property (nonatomic ,strong)UIButton *rightBarButton;
@property (nonatomic ,strong)NSString *titleContentStr;//需求内容

@property (nonatomic ,strong)NSMutableArray *CusSelectedPhotos;//图片数组
@property (nonatomic ,strong)NSMutableArray *CusSelectedAssets;//原图数组
@property (nonatomic ,strong)NSData *videoData;//视频data
@property (nonatomic ,strong)NSString *outPutPath;//视频data本地路径
@property (nonatomic ,strong)NSMutableArray *videoPhotosArray;//图片数组
@property (nonatomic ,strong)NSMutableArray *videoAssetsArray;//原图数组
@property (nonatomic ,strong)NSString *videoUrlStr;//视频连接

@property (nonatomic ,strong)NSMutableArray *updateImgArray;
@property (nonatomic ,strong)UIButton *pushButton;//发布按钮
@property (nonatomic ,strong)NSString *selectAddressStr;//地址选择
@property (nonatomic ,strong)NSString *selectPhoneStr;
@end
@implementation HBDemandPushVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = white_Color;
    self.updateImgArray = [NSMutableArray array];
    self.title = @"需求添加";
    if ([PFAPP sharedInstance].address.length>0) {
        self.selectAddressStr = [PFAPP sharedInstance].address;
    }
    //添加tableview
    [self addTableViewUISubView];
    //添加子视图
    [self setInitViewUI];
    
}
#pragma mark =======添加子视图==========
-(void)setInitViewUI{
    
//    //添加按钮
//    UIButton * navBtn = [HBHuTool addCusNavButton:@"发布" andImg:@"" andColor:Color_label_red andTarget:self andAction:@selector(addRightBtnClick:)];
//    self.rightBarButton = navBtn;
//    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:navBtn];
//    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

#pragma mark =======点击库存明细==========
-(void)clickPushButton:(id)sender{
    
    //    [self.navigationController popViewControllerAnimated:YES];
    //    [[NSNotificationCenter defaultCenter]postNotificationName:@"ReleaseEvaluationSuccess" object:nil];
    //    return;
    if ([HBHuTool isJudgeString:self.selectPhoneStr]) {
        [MBProgressHUD showInfoMessage:@"请输入手机号"];
        return;
    }
    WEAKSELF;
    [MBProgressHUD showActivityMessageInWindow:@"上传中..."];
    self.rightBarButton.enabled = NO;
    dispatch_group_t group = dispatch_group_create();
    //上传视频
    dispatch_group_enter(group);
    [self updateVideoCommitDataScu:^(BOOL isScu) {
        dispatch_group_leave(group);
    }];
    //上传图片
    dispatch_group_enter(group);
    [self updateImageMoreDataScu:^(BOOL isScu) {
        dispatch_group_leave(group);
    }];

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUD];
        weakSelf.rightBarButton.enabled = YES;
        //上传
        [weakSelf updateMaterialVideoAndImgData];
    });
}


#pragma mark ==========上传视频默认图==========
- (void)updateVideoCommitDataScu:(void(^)(BOOL isScu))requestisScu{
    if (self.videoData.length<1) {
        //没有视频 直接回调
        //上传成功回调
        if(requestisScu){
            requestisScu(YES);
        }
        return;
    }
    WEAKSELF;
    if (self.videoData!=nil) {
        
        //视频,根据路径获取图片
        UIImage *img = [self getVideoPreViewImage:[NSURL fileURLWithPath:self.outPutPath]];
        NSLog(@"打印一下图片%@",img);
        if (img==nil&&img==NULL) {
            //图片为空 没有封面图
            [MBProgressHUD showInfoMessage:@"上传失败！请重新上传"];
        }else{
            //上传图片
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"upload_type"] = @"binary";
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *titleStr = [formatter stringFromDate:[NSDate date]];
            parameters[@"image_input_title"] = NSStringFormat(@"%@.%@",titleStr,@"jpg");
            parameters[@"image_type"] = @"demand";
            [parameters setValue:DEMAND_IMAGE_COMMIT forKey:@"method"];
            [parameters setValue:BBStroe_Version forKey:@"v"];
            [parameters setValue:LocalValue(TOKEN_XY_APP) forKey:@"accessToken"];
            
            [PPNetworkHelper uploadImagesWithURL:Config_baseUrl parameters:parameters name:@"image" images:@[img] fileNames:nil imageScale:0.5f imageType:@"jpg" progress:^(NSProgress *progress) {
                //进度
            } success:^(id responseObject) {
                NSLog(@"上传成功了");
                NSLog(@"上传成功~~~~~~~~%@",responseObject);
                NSDictionary *dic = (NSDictionary *)responseObject;
                if (ValidDict(dic)) {
                    //上传成功上传默认图
                    NSDictionary *dataDic = dic[@"data"];
                    if (ValidDict(dataDic)) {
                        HBPushVideoModel *model = [HBPushVideoModel mj_objectWithKeyValues:dataDic];
                        //拿到图片链接
                        weakSelf.defineImgStr = model.image.image_id;
                        //上传视频
                        [weakSelf updateDefineVideoImageArray:@[model.image.image_id] updateDataScu:requestisScu];
                    }else{
                        NSLog(@"上传默认图失败");
                        [MBProgressHUD showInfoMessage:@"上传失败！请重新上传"] ;
                    }
                }
                
            } failure:^(NSError *error) {
                NSLog(@"上传图片失败了");
            }];
        }
        
    }
}
#pragma mark ==========上传视频==========
-(void)updateDefineVideoImageArray:(NSArray *)array updateDataScu:(void(^)(BOOL isScu))requestisScu{
    //上传视频
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *titleStr = [formatter stringFromDate:[NSDate date]];
    parameters[@"video_input_title"] = NSStringFormat(@"%@.%@",titleStr,@"mp4");
    parameters[@"video_type"] = @"demand";
    parameters[@"pic_url"] = self.defineImgStr;
    [parameters setValue:DEMAND_VIDEO_COMMIT forKey:@"method"];
    [parameters setValue:BBStroe_Version forKey:@"v"];
    [parameters setValue:LocalValue(TOKEN_XY_APP) forKey:@"accessToken"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 20;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"application/json", @"text/html", @"text/json", nil];
    //上传服务器接口
    [manager.requestSerializer setValue:USERDEFAULT_value(TOKEN_XY_APP) forHTTPHeaderField:@"x-auth-token"];
    
    //        NSData* data=[NSData dataWithContentsOfURL:self.VideoURL];
    
    //        [self showRoundProgressWithTitle:@"正在上传"];
    
    [manager POST:Config_baseUrl parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:self.videoData name:@"video" fileName:@"video.mp4"mimeType:@"video/mp4"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        //            NSLog(@"upload:%@",uploadProgress.fileTotalCount);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功~~~~~~~~%@",responseObject);
        
        NSDictionary *dic = (NSDictionary *)responseObject;
        if (ValidDict(dic)) {
            //上传成功上传默认图
            NSDictionary *dataDic = dic[@"data"];
            if (ValidDict(dataDic)) {
                HBPushVideoModel *model = [HBPushVideoModel mj_objectWithKeyValues:dataDic];
                //拿到图片链接
                weakSelf.videoUrlStr= model.video.video_id;
                //[weakSelf updateDefineVideoImageArray:@[model.video.url] updateDataScu:requestisScu];
            }else{
                NSLog(@"上传默认图失败");
                [MBProgressHUD showInfoMessage:@"上传失败！请重新上传"] ;
            }
            
        }
        //上传成功回调
        if(requestisScu){
            requestisScu(YES);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUD];
        //            [self hideBubble];
        //            [self  showErrorWithTitle:@"网络错误" autoCloseTime:1];
        NSLog(@"打印错误信息：%@",error.userInfo);
    }];
}
#pragma mark ==========上传图片==========
- (void)updateImageMoreDataScu:(void(^)(BOOL isScu))requestisScu{
    if (self.CusSelectedPhotos.count<1) {
        //没有图片 直接回调
        //上传成功回调
        if(requestisScu){
            requestisScu(YES);
        }
        return;
    }
    [self.updateImgArray removeAllObjects];
//    DEMAND_IMAGE_COMMIT
    //上传图片
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"upload_type"] = @"binary";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *titleStr = [formatter stringFromDate:[NSDate date]];
    parameters[@"image_input_title"] = NSStringFormat(@"%@.%@",titleStr,@"jpg");
    parameters[@"image_type"] = @"demand";
    [parameters setValue:DEMAND_IMAGE_COMMIT forKey:@"method"];
    [parameters setValue:BBStroe_Version forKey:@"v"];
    [parameters setValue:LocalValue(TOKEN_XY_APP) forKey:@"accessToken"];
    
    [PPNetworkHelper uploadImagesWithURL:Config_baseUrl parameters:parameters name:@"image" images:self.CusSelectedPhotos fileNames:nil imageScale:0.5f imageType:@"jpg" progress:^(NSProgress *progress) {
        //进度
    } success:^(id responseObject) {
        NSLog(@"上传成功了");
        NSLog(@"上传成功~~~~~~~~%@",responseObject);

        NSDictionary *dic = (NSDictionary *)responseObject;
        if (ValidDict(dic)) {
            //上传成功上传默认图
            NSDictionary *dataDic = dic[@"data"];
            if (ValidDict(dataDic)) {
                HBPushVideoModel *model = [HBPushVideoModel mj_objectWithKeyValues:dataDic];
                //拿到图片链接
                [weakSelf.updateImgArray addObject:model.image.image_id];
            }else{
                NSLog(@"上传默认图失败");
                [MBProgressHUD showInfoMessage:@"上传失败！请重新上传"] ;
            }
        }
        //上传成功回调
        if(requestisScu){
            requestisScu(YES);
        }
    } failure:^(NSError *error) {
        NSLog(@"上传图片失败了");
    }];
    

}


#pragma mark ==========上传东西==========
-(void)updateMaterialVideoAndImgData{

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSString *imagesStr;

    imagesStr = [self.updateImgArray componentsJoinedByString:@","];//附近

    parameters[@"desc"] = self.titleContentStr;
    parameters[@"latitude"] = @([PFAPP sharedInstance].lat);
    parameters[@"longitude"] = @([PFAPP sharedInstance].lng);
    parameters[@"demand_address"] = self.selectAddressStr;
    parameters[@"contact"] = self.selectPhoneStr;
    parameters[@"image_list"] = imagesStr;
    parameters[@"video_path"] = self.videoUrlStr;
    parameters[@"pic_url"] = self.defineImgStr;

    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
        [LBService post:DEMAND_DEMAND_CREATE params:parameters completion:^(LBResponse *response) {
            [MBProgressHUD hideHUD];
            if (response.succeed) {
//                [MBProgressHUD showInfoMessage:@"提交成功"];
                //发出通知
                [[NSNotificationCenter defaultCenter]postNotificationName:@"HBDemandPushSuccessChange" object:nil];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"发布需求成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                alert.tag = 1001;
                alert.delegate = self;
                [alert show];
            }else{
                [app showToastView:response.message];
            }
        }];

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self backBtnClicked];
}

#pragma mark - TableView相关
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
#pragma mark =======tableview代理方法==========
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"HBEvaluationOrderTabCell";
    HBEvaluationOrderTabCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    //系统Cell及代码创建Cell
    if (cell == nil) {
        cell = [[HBEvaluationOrderTabCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: identifier];
    }
    if ([HBHuTool isJudgeString:self.selectAddressStr]) {
        cell.addressStr = @"";
    }else{
        cell.addressStr = self.selectAddressStr;
    }
    cell.upController = self;
    cell.indexPath = indexPath;
    cell.expandableTableView = tableView;
    cell.delegate = self;
//    cell.model = self.model;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark =======添加tableview==========
-(void)addTableViewUISubView{
    
    [self.view addSubview:self.pushButton];
    [self.pushButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-20);
        make.centerX.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-2*20, 40));
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.pushButton.mas_top).offset(-20);
    }];
}
#pragma mark =======设置headerView==========
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.01)];
    headerView.backgroundColor = Color_BG;
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
    footerView.backgroundColor = white_Color;
    return footerView;
}
#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 430;
    CGFloat margin = 4;
    CGFloat itemWH = (SCREEN_WIDTH -30 - 2 * margin - 4) / 3 - margin;
    height += itemWH +20;
    return MAX(height, _cellHeight[indexPath.section]);
}

- (void)tableView:(UITableView *)tableView updatedHeight:(CGFloat)height atIndexPath:(NSIndexPath *)indexPath
{
    _cellHeight[indexPath.section] = height;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

- (void)tableView:(UITableView *)tableView updatedText:(NSString *)text atIndexPath:(NSIndexPath *)indexPath
{
    //HBMyOrderListGoodsItemModel *model = self.orderListModel.items[indexPath.section];
    self.titleContentStr = text;
    
}
//描述服务
- (void)tableView:(UITableView *)tableView updatedDescribeStars:(NSInteger )index atIndexPath:(NSIndexPath *)indexPath{
    //HBMyOrderListGoodsItemModel *model = self.orderListModel.items[indexPath.section];
    //self.model.describeStarsStr = [NSString stringWithFormat:@"%zd",index];
    
}
//物流服务
- (void)tableView:(UITableView *)tableView updatedLogisticsStars:(NSInteger )index atIndexPath:(NSIndexPath *)indexPath{
    //HBMyOrderListGoodsItemModel *model = self.orderListModel.items[indexPath.section];
    //self.model.logisticsStarsStr = [NSString stringWithFormat:@"%zd",index];
    
}
//服务评价
- (void)tableView:(UITableView *)tableView updatedFuWuStars:(NSInteger )index atIndexPath:(NSIndexPath *)indexPath{
    //self.model.fuWuStarsStr = [NSString stringWithFormat:@"%zd",index];
}
//图片 // 与视频
//图片与视频
- (void)tableView:(UITableView *)tableView updatedImgPhotos:(NSArray *)imgPhotosArr andImgAssets:(NSArray *)imgAssetsArr andVideoPhotosArray:(NSArray *)videoPhotosArr andVideoAssetsArray:(NSArray *)videoAssetsArr andVideoData:(NSData *)videoData andOutPutPath:(NSString *)outPutPath atIndexPath:(NSIndexPath *)indexPath{
    self.CusSelectedPhotos = [imgPhotosArr copy];
    self.CusSelectedAssets = [imgAssetsArr copy];
    self.videoPhotosArray = [videoPhotosArr copy];
    self.videoAssetsArray = [videoAssetsArr copy];
    self.videoData = videoData;
    self.outPutPath = outPutPath;
}
//是否匿名
- (void)tableView:(UITableView *)tableView updatedAnonymous:(BOOL )isSelect atIndexPath:(NSIndexPath *)indexPath{
    //    HBMyOrderListGoodsItemModel *model = self.orderListModel.items[indexPath.section];
    //    model.isSelectAnony = isSelect;
    //self.orderListModel.isSelectAnony = isSelect;
}
#pragma mark ==========点击选择地址==========
- (void)tableView:(UITableView *)tableView selectChangeAddress:(UIButton* )button atIndexPath:(NSIndexPath *)indexPath{
    //拖动地图选择地址
    MapChooseAddressVC *addressVC = [[MapChooseAddressVC alloc] init];
    addressVC.delegate = self;
    [self.navigationController pushViewController:addressVC animated:YES];
}
#pragma mark - MapChooseAddressVCDelegate
-(void)addressDidClickGeoPoint:(AMapGeoPoint *)location regeocode:(NSString *)address Province:(NSString *)province City:(NSString *)city
{
    //[_btnChooseAddress setTitle:address forState:UIControlStateNormal];
    NSLog(@"打印一下选择的地址是什么？%@",address);
    self.selectAddressStr = address;
    [PFAPP sharedInstance].lat = location.latitude;
    [PFAPP sharedInstance].lng = location.longitude;
    [self.tableView reloadData];
}
//改变了电话号码
- (void)tableView:(UITableView *)tableView changePhoneStr:(NSString* )phoneStr atIndexPath:(NSIndexPath *)indexPath{
    self.selectPhoneStr = phoneStr;
}
#pragma mark -Getter
-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = ({
            UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
            tableView.dataSource = self;
            tableView.delegate = self;
            
            tableView.backgroundColor = clear_Color;
            //设置分割线样式
            //cell的分割线距视图距离
            //隐藏底部多余分割线
            //隐藏头部多余间距
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            //cell的分割线距视图距离
            tableView.separatorInset=UIEdgeInsetsMake(0, 0, 0, 0);
            //隐藏底部多余分割线
            tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            //隐藏头部多余间距
            tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
            //iOS11默认开启Self-Sizing，需关闭才能设置Header,Footer高度
            tableView.estimatedRowHeight = 66;
            tableView.estimatedSectionHeaderHeight = 0;
            tableView.estimatedSectionFooterHeight = 0;
            tableView.rowHeight = 100 ;
            tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectZero];
            tableView ;
        }) ;
    }
    
    return _tableView;
}
-(UIButton *)pushButton{
    if (!_pushButton) {
        _pushButton = ({
            //创建按钮
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            //设置标题
            [button setTitle:@"发布需求" forState:UIControlStateNormal];
            //设置字体大小
            button.titleLabel.font = Font(15);
            //设置title颜色
            [button setTitleColor:white_Color forState:UIControlStateNormal];
            button.backgroundColor = RGBA(43, 164, 255, 1);
            button.layer.cornerRadius = 4;
            button.clipsToBounds = YES;
            //添加点击事件
            [button addTarget:self action:@selector(clickPushButton:) forControlEvents:UIControlEventTouchUpInside];
            
            button;
        });
    }
    return _pushButton;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (UIImage*)getVideoPreViewImage:(NSURL *)url
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];

    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return img;
}

@end
