//
//  HBPushVideoModel.h
//  HBVideoShopping
//
//  Created by Mac on 2020/2/13.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBPushVideoImageModel;
@class HBPushVideoVideoUrlModel;
NS_ASSUME_NONNULL_BEGIN

@interface HBPushVideoModel : NSObject
@property (nonatomic ,strong)HBPushVideoImageModel *image;
@property (nonatomic ,strong)HBPushVideoVideoUrlModel *video;
@end

@interface HBPushVideoImageModel : NSObject
@property (nonatomic ,strong)NSString *image_id;
@property (nonatomic ,strong)NSString *url;
@property (nonatomic ,strong)NSString *t_url;
@end

@interface HBPushVideoVideoUrlModel : NSObject
@property (nonatomic ,strong)NSString *url;
@property (nonatomic ,strong)NSString *video_id;
@end

NS_ASSUME_NONNULL_END
