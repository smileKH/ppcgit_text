//
//  HBEvaluationOrderTabCell.h
//  HBTreasureMaterial
//
//  Created by 胡明波 on 2019/7/4.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBBaseListTableCell.h"
//#import "HBEvaluationListModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol HBEvaluationOrderTabCellDelegate <UITableViewDelegate>

@optional
//文字
- (void)tableView:(UITableView *)tableView updatedText:(NSString *)text atIndexPath:(NSIndexPath *)indexPath;
//星星
- (void)tableView:(UITableView *)tableView updatedDescribeStars:(NSInteger )index atIndexPath:(NSIndexPath *)indexPath;
//星星
- (void)tableView:(UITableView *)tableView updatedLogisticsStars:(NSInteger )index atIndexPath:(NSIndexPath *)indexPath;
//星星
- (void)tableView:(UITableView *)tableView updatedFuWuStars:(NSInteger )index atIndexPath:(NSIndexPath *)indexPath;
//图片与视频
- (void)tableView:(UITableView *)tableView updatedImgPhotos:(NSArray *)imgPhotosArr andImgAssets:(NSArray *)imgAssetsArr andVideoPhotosArray:(NSArray *)videoPhotosArr andVideoAssetsArray:(NSArray *)videoAssetsArr andVideoData:(NSData *)videoData andOutPutPath:(NSString *)outPutPath atIndexPath:(NSIndexPath *)indexPath;

- (void)tableView:(UITableView *)tableView updatedAnonymous:(BOOL )isSelect atIndexPath:(NSIndexPath *)indexPath;
//选择修改地址
- (void)tableView:(UITableView *)tableView selectChangeAddress:(UIButton* )button atIndexPath:(NSIndexPath *)indexPath;

//改变了电话号码
- (void)tableView:(UITableView *)tableView changePhoneStr:(NSString* )phoneStr atIndexPath:(NSIndexPath *)indexPath;
@optional
- (void)tableView:(UITableView *)tableView updatedHeight:(CGFloat)height atIndexPath:(NSIndexPath *)indexPath;

@end
@interface HBEvaluationOrderTabCell : HBBaseListTableCell
@property (nonatomic ,strong)NSString *addressStr;//追评 1、追评  2、原来评价

@property (nonatomic ,weak)UIViewController *upController;
@property (nonatomic ,strong)NSIndexPath *indexPath;//
@property (nonatomic, weak) UITableView *expandableTableView;
@property (nonatomic ,weak)id<HBEvaluationOrderTabCellDelegate>delegate;
//@property (nonatomic ,strong)HBEvaluationListItemModel *model;
@end

NS_ASSUME_NONNULL_END
