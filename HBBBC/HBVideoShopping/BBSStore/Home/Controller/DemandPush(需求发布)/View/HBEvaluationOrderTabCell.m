//
//  HBEvaluationOrderTabCell.m
//  HBTreasureMaterial
//
//  Created by 胡明波 on 2019/7/4.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import "HBEvaluationOrderTabCell.h"
#import "HBMaterialTZImagePickerHelper.h"
#import "LxGridViewFlowLayout.h"
#import "TZImageManager.h"
#import "TZImagePickerController.h"
#import "UIView+Layout.h"
#import "HBEvaluationTZContentCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UITextView+PlaceHolder.h"

#import "XHStarRateView.h"
#import "HBCollectionFooterServiceView.h"
#import "UIButton+ImageTitleSpacing.h"
#define MAX_COUNT    5
#define spingMarge   10  //多加下边距离

@interface HBEvaluationOrderTabCell ()<TZImagePickerControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate,HBMaterialTZImagePickerHelperDelegate,UITextViewDelegate>
{
    CGFloat _itemWH;
    CGFloat _margin;
}
@property (nonatomic ,strong)NSMutableArray *CusSelectedPhotos;//图片数组
@property (nonatomic ,strong)NSMutableArray *CusSelectedAssets;//原图数组
@property (nonatomic ,strong)NSData *videoData;//视频data
@property (nonatomic ,strong)NSString *outPutPath;//视频的本地路径
@property (nonatomic ,strong)NSMutableArray *videoPhotosArray;//图片数组
@property (nonatomic ,strong)NSMutableArray *videoAssetsArray;//原图数组
@property (nonatomic ,assign)BOOL isSelectOriginalPhoto;//是否选择原图
@property (nonatomic ,strong)UITextView *titleInputTextView;//标题
@property (nonatomic ,strong)UILabel *titleInputLab;//标题提示
@property (nonatomic ,strong)NSString *titleContentStr;//标题

/**
 封装的获取图片工具类
 1. 初始化一个helper (需设置block回调已选择图片的路径数组);
 2. 调用showImagePickerControllerWithMaxCount:(NSInteger )maxCount WithViewController: (UIViewController *)superController;
 3. 调用结束后，刷新界面;
 */
@property (nonatomic, strong) HBMaterialTZImagePickerHelper *helper;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) LxGridViewFlowLayout *layout;
@property (nonatomic ,assign)CGFloat CollectionViewHeight;//高度
@property (nonatomic ,assign)selectTakeType selectType;//类型
@property (nonatomic ,strong)MASConstraint *collectionTotalHeightCon;//集合视图的高度
@property (nonatomic ,assign)CGFloat cellTotalHeight;//总高度
@property (nonatomic ,assign)BOOL isSelectVideo;//是否选择图片或者视频
//@property (nonatomic ,strong)UIButton *anonymousBtn;//匿名

@property (nonatomic ,strong)UIView *locationView;//地址选择
@property (nonatomic ,strong)UILabel *locTitleLab;//地址位置
@property (nonatomic ,strong)UIButton *selectLocBtn;//选择位置
@property (nonatomic ,strong)UILabel *locationContentLab;//位置内容

@property (nonatomic ,strong)UIView *editPhoneView;//输入手机号
@property (nonatomic ,strong)UILabel *phTitleLab;//联系电话
@property (nonatomic ,strong)UITextField *phoneTextField;//手机号
@property (nonatomic ,strong)UIView *phTopLineView;
@property (nonatomic ,strong)UIView *phBottomLineView;

@property (nonatomic ,strong)NSString *selectPhoneStr;
@end
@implementation HBEvaluationOrderTabCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //设置初始数据
        [self setOriginalValue];
        //添加子视图
        [self addEvaluationSubUI];
    }
    return self;
}
-(void)setAddressStr:(NSString *)addressStr{
    _addressStr = addressStr;
    self.locationContentLab.text = addressStr;
}
#pragma mark ==========点击选择地址==========
-(void)clickSelectLocationBtn:(UIButton *)button{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(tableView:selectChangeAddress:atIndexPath:)]) {
        [self.delegate tableView:self.expandableTableView selectChangeAddress:button atIndexPath:self.indexPath];
    }
}
-(void)clickAnonyMousBtn:(UIButton *)button{
    button.selected = !button.selected;
    if (self.delegate&&[self.delegate respondsToSelector:@selector(tableView:updatedAnonymous:atIndexPath:)]) {
        [self.delegate tableView:self.expandableTableView updatedAnonymous:button.selected atIndexPath:self.indexPath];
    }
}

#pragma mark ==========设置初始数据==========
-(void)setOriginalValue{
    _margin = 4;
    _itemWH = (SCREEN_WIDTH -30 - 2 * _margin - 4) / 3 - _margin;
    self.CollectionViewHeight = _itemWH +20;
    //添加多一行
    self.CollectionViewHeight +=_itemWH +spingMarge +10;
    //设置类型
    self.selectType = selectTakeImagePickerType;
}
#pragma mark ==========添加子视图==========
-(void)addEvaluationSubUI{
    
    self.cellTotalHeight = 0;
    
    [self addSubview:self.titleInputTextView];
    [self.titleInputTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.height.mas_offset(100);
    }];
    
    self.cellTotalHeight +=10;
    self.cellTotalHeight +=100;
    
    [self.titleInputTextView addSubview:self.titleInputLab];
    [self.titleInputLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleInputTextView).offset(5);
        make.left.equalTo(self.titleInputTextView).offset(3);
        make.right.equalTo(self.titleInputTextView).offset(-15);
        make.height.mas_offset(14);
    }];
    
    //添加
    [self addSubview:self.collectionView];
    if (_CusSelectedPhotos.count <3) {
        _CollectionViewHeight = _itemWH+spingMarge;
    }else if (_CusSelectedPhotos.count <6)
    {
        _CollectionViewHeight = 2*_itemWH+spingMarge;
    }else
    {
        _CollectionViewHeight = 3*_itemWH+spingMarge;
    }
    //添加多一行
    _CollectionViewHeight +=_itemWH +spingMarge +10;
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleInputTextView.mas_bottom).offset(10);
        make.left.equalTo(self.titleInputTextView.mas_left);
        make.right.equalTo(self.titleInputTextView.mas_right);
       self.collectionTotalHeightCon = make.height.mas_equalTo(self.CollectionViewHeight);
    }];
    
    self.cellTotalHeight +=10;
    self.cellTotalHeight +=_CollectionViewHeight;
    
    
    [self addSubview:self.locationView];
    [self.locationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collectionView.mas_bottom).offset(10);
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.height.mas_equalTo(65);
    }];
    
    self.cellTotalHeight +=10;
    self.cellTotalHeight +=65;
    
    [self addSubview:self.editPhoneView];
    [self.editPhoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.locationView.mas_bottom);
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.height.mas_equalTo(50);
    }];
    
//    self.cellTotalHeight +=10;
    self.cellTotalHeight +=50;
    
    self.cellTotalHeight +=10;
//    self.cellTotalHeight +=30;
    //添加子视图
    [self addLocationSubView];
    //设置最大数量
    _titleInputTextView.maxLength = @(500);
}
#pragma mark =======计算高度==========
-(void)calculateHeight{
    //这里可以自己计算
    self.cellTotalHeight = self.cellTotalHeight- _CollectionViewHeight;
    if (_CusSelectedPhotos.count <3) {
        _CollectionViewHeight = _itemWH+spingMarge;
    }else if (_CusSelectedPhotos.count <6)
    {
        _CollectionViewHeight = 2*_itemWH+spingMarge;
    }else
    {
        _CollectionViewHeight = 3*_itemWH+spingMarge;
    }
    //添加多一行
    _CollectionViewHeight +=_itemWH +spingMarge +10;
    
    [self.collectionTotalHeightCon uninstall];
    [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
       self.collectionTotalHeightCon = make.height.mas_equalTo(self.CollectionViewHeight);
    }];
    self.cellTotalHeight += _CollectionViewHeight;

    [self.collectionView reloadData];
    if (_delegate&&[_delegate respondsToSelector:@selector(tableView:updatedHeight:atIndexPath:)]) {
        [_delegate tableView:self.expandableTableView
               updatedHeight:self.cellTotalHeight
                 atIndexPath:self.indexPath];
    }
    
    if (_delegate&&[_delegate respondsToSelector:@selector(tableView:updatedImgPhotos:andImgAssets:andVideoPhotosArray:andVideoAssetsArray:andVideoData:andOutPutPath:atIndexPath:)]) {
        [_delegate tableView:self.expandableTableView updatedImgPhotos:self.CusSelectedPhotos andImgAssets:self.CusSelectedAssets andVideoPhotosArray:self.videoPhotosArray andVideoAssetsArray:self.videoAssetsArray andVideoData:self.videoData andOutPutPath:self.outPutPath atIndexPath:self.indexPath];
    }
}
#pragma mark ==========添加子视图==========
-(void)addLocationSubView{
    [self.locationView addSubview:self.locTitleLab];
    [self.locTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.locationView).offset(10);
        make.left.equalTo(self.locationView).offset(0);
        make.height.mas_equalTo(12);
    }];
    
    [self.locationView addSubview:self.locationContentLab];
    [self.locationContentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.locTitleLab.mas_bottom).offset(10);
        make.left.equalTo(self.locationView).offset(0);
        make.right.equalTo(self.locationView).offset(-10);
        make.height.mas_equalTo(30);
    }];
    
    [self.locationView addSubview:self.selectLocBtn];
    [self.selectLocBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.locTitleLab);
        make.right.equalTo(self.locationView).offset(0);
        make.size.mas_equalTo(CGSizeMake(120, 20));
    }];
    
    
    [self.editPhoneView addSubview:self.phTitleLab];
    [self.phTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.editPhoneView);
        make.left.equalTo(self.editPhoneView).offset(0);
        make.height.mas_equalTo(12);
    }];
    
    [self.editPhoneView addSubview:self.phTopLineView];
    [self.phTopLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.editPhoneView);
        make.left.right.equalTo(self.editPhoneView);
        make.height.mas_equalTo(1);
    }];
    
    [self.editPhoneView addSubview:self.phBottomLineView];
    [self.phBottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.editPhoneView);
        make.left.right.equalTo(self.editPhoneView);
        make.height.mas_equalTo(1);
    }];
    
    [self.editPhoneView addSubview:self.phoneTextField];
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.editPhoneView);
        make.left.equalTo(self.phTitleLab.mas_right).offset(10);
//        make.right.equalTo(self.editPhoneView);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(200);
    }];
    
    
}
//#pragma mark ==========赋值==========
//-(void)setModel:(HBEvaluationListItemModel *)model{
//    _model = model;
//    [self.goodsImgView sd_setImageWithURL:[NSURL URLWithString:model.productImg] placeholderImage:GOODS_DEFINE_IMG];
//    self.goodsTitleLab.text = model.productName;
//    self.goodsNumberLab.text = [NSString stringWithFormat:@"X%ld",model.buyNum];
//}

#pragma mark -- UICollectionViewDelegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section==0) {
        if (self.CusSelectedPhotos.count >= MAX_COUNT) {
            return self.CusSelectedPhotos.count;
        }
        return self.CusSelectedPhotos.count+1;
    }else{
        //视频
        return 1;
    }
}

-(void)cusSelectTZImagePickerSelectedPhotos:(NSMutableArray *)selectedPhotos withSelectedAssets:(NSMutableArray *)selectAssets andIsOriginalPoto:(BOOL)isSelectOriginalPhoto andBlockData:(NSData *)data outPutPath:(NSString *)outPutPath withSelectTakeType:(selectTakeType )selectType{
    
    //判断是否是视频
    if (self.isSelectVideo) {
        //视频
        self.videoPhotosArray = selectedPhotos;
        self.videoAssetsArray = selectAssets;
        self.videoData = data;
        self.outPutPath = outPutPath;
    }else{
        self.CusSelectedPhotos = selectedPhotos;
        self.CusSelectedAssets = selectAssets;
    }
    
    self.isSelectOriginalPhoto = isSelectOriginalPhoto;
    self.selectType = selectType;
    
    [self calculateHeight];//计算高度
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF;
    if (indexPath.section==0) {
        //图片
        HBEvaluationTZContentCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HBEvaluationTZContentCell" forIndexPath:indexPath];
        cell.videoImageView.hidden = YES;
        if (indexPath.item == self.CusSelectedPhotos.count) {
            cell.imageView.image = [UIImage imageNamed:@"pj_tianjitpian"];
            cell.deleteBtn.hidden = YES;
            //        cell.gifLable.hidden = YES;
        }else {
            cell.imageView.image = self.CusSelectedPhotos[indexPath.item];
            cell.asset = self.CusSelectedAssets[indexPath.item];
            cell.deleteBtn.hidden = NO;
        }
        cell.deleteBtn.tag = indexPath.item;
        //[cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
        cell.clickEvaluationDeleteBtnBlock = ^(NSInteger tag) {
            //点击删除
            [weakSelf deleteBtnClik:tag andIndexPath:indexPath];
        };
         return cell;
    }else{
        //视频
        HBEvaluationTZContentCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HBEvaluationTZContentCell" forIndexPath:indexPath];
        cell.videoImageView.hidden = YES;
        if (indexPath.item == self.videoPhotosArray.count) {
            cell.imageView.image = [UIImage imageNamed:@"pj_tianjispin"];
            cell.deleteBtn.hidden = YES;
        }else {
            cell.imageView.image = self.videoPhotosArray[indexPath.item];
            cell.asset = self.videoAssetsArray[indexPath.item];
            cell.deleteBtn.hidden = NO;
        }
        cell.deleteBtn.tag = indexPath.item;
        //[cell.deleteBtn addTarget:self action:@selector(deleteBtnClik) forControlEvents:UIControlEventTouchUpInside];
        cell.clickEvaluationDeleteBtnBlock = ^(NSInteger tag) {
            //点击删除
            [weakSelf deleteBtnClik:tag andIndexPath:indexPath];
        };
         return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.item == self.CusSelectedPhotos.count) {
            self.isSelectVideo = NO;
            //继续选择图片
            [self.helper showImagePickerControllerWithMaxCount:MAX_COUNT WithViewController:self.upController withSelectTakeType:selectTakeImagePickerType withAssetsArr:self.CusSelectedAssets andPhotosArr:self.CusSelectedPhotos];
        } else { // preview photos or video / 预览照片或者视频
            PHAsset *asset = self.CusSelectedAssets[indexPath.item];
            BOOL isVideo = NO;
            isVideo = asset.mediaType == PHAssetMediaTypeVideo;
            if (isVideo) { // perview video / 预览视频
                TZVideoPlayerController *vc = [[TZVideoPlayerController alloc] init];
                TZAssetModel *model = [TZAssetModel modelWithAsset:asset type:TZAssetModelMediaTypeVideo timeLength:@""];
                vc.model = model;
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                [self.upController presentViewController:vc animated:YES completion:nil];
            } else { // preview photos / 预览照片
                TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:self.CusSelectedAssets selectedPhotos:self.CusSelectedPhotos index:indexPath.item];
                imagePickerVc.maxImagesCount = MAX_COUNT;
                imagePickerVc.allowPickingGif = NO;
                imagePickerVc.allowPickingOriginalPhoto = self.isSelectOriginalPhoto;
                imagePickerVc.allowPickingMultipleVideo = NO;
                imagePickerVc.showSelectedIndex = YES;
                imagePickerVc.isSelectOriginalPhoto = _isSelectOriginalPhoto;
                [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
                    self->_CusSelectedPhotos = [NSMutableArray arrayWithArray:photos];
                    self->_CusSelectedAssets = [NSMutableArray arrayWithArray:assets];
                    self->_isSelectOriginalPhoto = isSelectOriginalPhoto;
                    [self->_collectionView reloadData];
                    self->_collectionView.contentSize = CGSizeMake(0, ((self->_CusSelectedPhotos.count + 2) / 3 ) * (self->_margin + self->_itemWH));
                }];
                imagePickerVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                [self.upController presentViewController:imagePickerVc animated:YES completion:nil];
            }
        }
    }else{
        //视频
        if (indexPath.item == self.videoPhotosArray.count) {
            self.isSelectVideo = YES;
            //继续选择视频
            [self.helper showImagePickerControllerWithMaxCount:MAX_COUNT WithViewController:self.upController withSelectTakeType:selectTakeVideoType withAssetsArr:self.CusSelectedAssets andPhotosArr:self.CusSelectedPhotos];
        } else { // preview photos or video / 预览照片或者视频
            PHAsset *asset = self.videoAssetsArray[indexPath.item];
            BOOL isVideo = NO;
            isVideo = asset.mediaType == PHAssetMediaTypeVideo;
            if (isVideo) { // perview video / 预览视频
                TZVideoPlayerController *vc = [[TZVideoPlayerController alloc] init];
                TZAssetModel *model = [TZAssetModel modelWithAsset:asset type:TZAssetModelMediaTypeVideo timeLength:@""];
                vc.model = model;
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                [self.upController presentViewController:vc animated:YES completion:nil];
            } else { // preview photos / 预览照片
                
            }
        }
    }

}

#pragma mark -- 内部方法
/**
 删除
 
 @param sender sender
 */
#pragma mark - Click Event
- (void)deleteBtnClik:(NSInteger )tag andIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        //图片
        if ([self collectionView:self.collectionView numberOfItemsInSection:0] <= _CusSelectedPhotos.count) {
            [_CusSelectedPhotos removeObjectAtIndex:tag];
            [_CusSelectedAssets removeObjectAtIndex:tag];
            [self.collectionView reloadData];
            return;
        }
        
        [_CusSelectedPhotos removeObjectAtIndex:tag];
        [_CusSelectedAssets removeObjectAtIndex:tag];
        [_collectionView performBatchUpdates:^{
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:0];
            [self->_collectionView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:^(BOOL finished) {
            [self->_collectionView reloadData];
        }];
    }else{
        //视频
        if ([self collectionView:self.collectionView numberOfItemsInSection:1] <= self.videoPhotosArray.count) {
                   [self.videoPhotosArray removeObjectAtIndex:tag];
                   [self.videoAssetsArray removeObjectAtIndex:tag];
                   [self.collectionView reloadData];
                   self.videoData = nil;
                   return;
               }
               
               [self.videoPhotosArray removeObjectAtIndex:tag];
               [self.videoAssetsArray removeObjectAtIndex:tag];
               self.videoData = nil;
               [_collectionView performBatchUpdates:^{
                   NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:1];
                   [self->_collectionView deleteItemsAtIndexPaths:@[indexPath]];
               } completion:^(BOOL finished) {
                   [self->_collectionView reloadData];
               }];
    }
    
    //计算高度
    [self calculateHeight];
}
#pragma mark---加载UICollectionView表头
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    { // header
        
        return nil;
    }else if([kind isEqualToString:UICollectionElementKindSectionFooter]){
        //footerview
        reuseIdentifier = @"HBCollectionFooterServiceView";
                   
        HBCollectionFooterServiceView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
            if ([kind isEqualToString:UICollectionElementKindSectionFooter])
            {
            view.backgroundColor = white_Color;
            }
        return view;
        
    }
    
    return nil;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(SCREEN_WIDTH, 10);
    
}
#pragma mark - LxGridViewDataSource
/// 以下三个方法为长按排序相关代码
- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        return indexPath.item < _CusSelectedPhotos.count;
    }
    return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)sourceIndexPath canMoveToIndexPath:(NSIndexPath *)destinationIndexPath {
    return (sourceIndexPath.item < _CusSelectedPhotos.count && destinationIndexPath.item < _CusSelectedPhotos.count);
}

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)sourceIndexPath didMoveToIndexPath:(NSIndexPath *)destinationIndexPath {
    UIImage *image = _CusSelectedPhotos[sourceIndexPath.item];
    [_CusSelectedPhotos removeObjectAtIndex:sourceIndexPath.item];
    [_CusSelectedPhotos insertObject:image atIndex:destinationIndexPath.item];

    id asset = _CusSelectedAssets[sourceIndexPath.item];
    [_CusSelectedAssets removeObjectAtIndex:sourceIndexPath.item];
    [_CusSelectedAssets insertObject:asset atIndex:destinationIndexPath.item];

    //计算高度
    [self calculateHeight];
}
#pragma mark ==========输入==========
- (void)textViewDidChange:(UITextView *)textView{
    if (textView.tag==1001) {//title
        if ([textView.text length] == 0) {
            [self.titleInputLab setHidden:NO];
        }else{
            [self.titleInputLab setHidden:YES];
        }
        if (_delegate&&[_delegate respondsToSelector:@selector(tableView:updatedText:atIndexPath:)]) {
            [_delegate tableView:self.expandableTableView
                   updatedText:textView.text atIndexPath:self.indexPath];
        }
    }else{
        
    }

}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
//                [self.contentTextView resignFirstResponder];
        [self.titleInputTextView resignFirstResponder];
        return FALSE;
    }
    return TRUE;
}

#pragma mark -- 懒加载

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 100, self.tz_width, self.tz_height - 300) collectionViewLayout:self.layout];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.backgroundColor = white_Color;
        _collectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.bounces  = NO;
        _collectionView.scrollEnabled = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[HBEvaluationTZContentCell class] forCellWithReuseIdentifier:@"HBEvaluationTZContentCell"];
        [_collectionView registerClass:[HBCollectionFooterServiceView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"HBCollectionFooterServiceView"];
    }
    return _collectionView;
}

- (LxGridViewFlowLayout *)layout
{
    if (!_layout) {
        _layout = [[LxGridViewFlowLayout alloc] init];
        _layout.itemSize = CGSizeMake(_itemWH, _itemWH);
        _layout.minimumInteritemSpacing = _margin;
        _layout.minimumLineSpacing = _margin;
    }
    return _layout;
}

- (HBMaterialTZImagePickerHelper *)helper
{
    if (!_helper) {
        _helper = [[HBMaterialTZImagePickerHelper alloc] init];
        _helper.delegate = self;
    }
    return _helper;
}
-(UILabel *)titleInputLab{
    if (!_titleInputLab) {
        _titleInputLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"请写下您的内容需求";//内容显示
            label.textColor = black_Color;//设置字体颜色
            label.font = Font(14);//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _titleInputLab ;
}
-(UITextView *)titleInputTextView{
    if (!_titleInputTextView) {
        _titleInputTextView = [[UITextView alloc]init];
        _titleInputTextView.delegate = self;
        _titleInputTextView.textColor = black_Color;
        _titleInputTextView.font = Font(14);
        _titleInputTextView.tag = 1001;
    }
    return _titleInputTextView;
}
-(UIView *)locationView{
    if (!_locationView) {
        _locationView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _locationView ;
}
-(UIView *)editPhoneView{
    if (!_editPhoneView) {
        _editPhoneView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = white_Color;
            
            view ;
        }) ;
    }
    return _editPhoneView ;
}
-(UILabel *)locTitleLab{
    if (!_locTitleLab) {
        _locTitleLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"位置信息";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(14);//设置字体大小
            
            label ;
        }) ;
    }
    return _locTitleLab ;
}
-(UIButton *)selectLocBtn{
    if (!_selectLocBtn) {
        _selectLocBtn = ({
            //创建按钮
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            //设置标题
            [button setTitle:@"修改位置" forState:UIControlStateNormal];
            [button setImage:ImageNamed(@"bbs_right_tip1") forState:UIControlStateNormal];
            [button setTitleColor:MAINTextCOLOR forState:UIControlStateNormal];
            button.titleLabel.font = Font(14);
            [button layoutButtonWithEdgeInsetsStyle:GLButtonEdgeInsetsStyleRight imageTitleSpace:3];
            //添加点击事件
            [button addTarget:self action:@selector(clickSelectLocationBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            button;
        });
    }
    return _selectLocBtn;
}
-(UILabel *)locationContentLab{
    if (!_locationContentLab) {
        _locationContentLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(14);//设置字体大小
            label.numberOfLines = 2;
            label ;
        }) ;
    }
    return _locationContentLab ;
}

-(UILabel *)phTitleLab{
    if (!_phTitleLab) {
        _phTitleLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"联系电话";//内容显示
            label.textColor = MAINCOLOR;//设置字体颜色
            label.font = Font(14);//设置字体大小
            
            label ;
        }) ;
    }
    return _phTitleLab ;
}
-(UITextField *)phoneTextField{
    if (!_phoneTextField) {
        _phoneTextField = ({
            UITextField *textField = [[UITextField alloc]initWithFrame:CGRectZero];
            //设置边框样式
            textField.borderStyle = UITextBorderStyleNone;
            //设置键盘的样式
            textField.keyboardType = UIKeyboardTypeNumberPad;
            [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//            textField.delegate = self;
            textField.placeholder = @"请输入联系电话";
            //return键变成什么键
            textField.returnKeyType =UIReturnKeyDone;
            //textField.backgroundColor = RGBA(245, 245, 245, 1);
            textField.backgroundColor = white_Color;
            textField.textAlignment = NSTextAlignmentCenter;
            
            textField ;
        }) ;
    }
    return _phoneTextField ;
}
-(UIView *)phTopLineView{
    if (!_phTopLineView) {
        _phTopLineView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = RGBA(204, 204, 204, 1);
            
            view ;
        }) ;
    }
    return _phTopLineView ;
}
-(UIView *)phBottomLineView{
    if (!_phBottomLineView) {
        _phBottomLineView = ({
            UIView *view = [UIView new];//初始化控件
            view.backgroundColor = RGBA(204, 204, 204, 1);;
            
            view ;
        }) ;
    }
    return _phBottomLineView ;
}
#pragma mark ==========监听电话号码改变==========
-(void)textFieldDidChange :(UITextField *)theTextField{
    
    if (_delegate&&[_delegate respondsToSelector:@selector(tableView:changePhoneStr:atIndexPath:)]) {
        [_delegate tableView:self.expandableTableView changePhoneStr:theTextField.text atIndexPath:self.indexPath];
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
