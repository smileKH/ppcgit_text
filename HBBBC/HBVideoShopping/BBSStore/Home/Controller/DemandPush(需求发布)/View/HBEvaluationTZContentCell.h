//
//  HBEvaluationTZContentCell.h
//  HBGoldRoom
//
//  Created by Mac on 2019/12/11.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HBEvaluationTZContentCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *videoImageView;
@property (nonatomic, strong) UIButton *deleteBtn;
@property (nonatomic, assign) NSInteger row;
@property (nonatomic, strong) id asset;

- (UIView *)snapshotView;

@property (nonatomic ,copy)void(^clickEvaluationDeleteBtnBlock)(NSInteger tag);
@end

