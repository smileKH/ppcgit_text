//
//  HBbcManageHomeVC.m
//  HBVideoShopping
//
//  Created by Mac on 2020/1/4.
//  Copyright © 2020 FirstVision. All rights reserved.
//

#import "HBbcManageHomeVC.h"
#import "HomeVC.h"
#import "GoodsViewController.h"
#import "HBDemandListVC.h"
#import "CategryModel.h"
@interface HBbcManageHomeVC ()<UITextFieldDelegate>
{
    CGFloat categoryViewHeight;
}
@property (nonatomic, copy) NSArray *titles;
@property (nonatomic, strong) JXCategoryTitleView *myCategoryView;
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;
@property (nonatomic ,strong)UIButton *searchBtn;//搜索按钮
@property (nonatomic ,strong)NSString *textFieldStr;
@property (nonatomic ,strong)NSString *keyboreType;
@property (nonatomic ,strong)CategorysModel *categoryModel;
@end
@implementation HBbcManageHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页管理";
    //隐藏导航栏
    self.isHidenNaviBar = NO;
    //设置导航栏颜色
    //    self.StatusBarStyle = UIStatusBarStyleLightContent;
    self.isShowLiftBack = NO;//每个根视图需要设置该属性为NO，否则会出现导航栏异常
//    //设置界面
//    [self initDate];
//    //设置滚动视图
//    [self setupCategoryView];
    //请求数据
    [self requestGoodsListData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupNavBar];
    self.StatusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setBackgroundImage:[self imageWithColor:MAINTextCOLOR] forBarMetrics:UIBarMetricsDefault];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.StatusBarStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"white_Nav.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.titleTextAttributes =@{NSForegroundColorAttributeName:[UIColor blackColor]};
}
-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f); //宽高 1.0只要有值就够了
    UIGraphicsBeginImageContext(rect.size); //在这个范围内开启一段上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);//在这段上下文中获取到颜色UIColor
    CGContextFillRect(context, rect);//用这个颜色填充这个上下文
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();//从这段上下文中获取Image属性,,,结束
    UIGraphicsEndImageContext();
    
    return image;
}
-(void)setupNavBar
{
    [self.navigationController.navigationBar setBarTintColor:MAINTextCOLOR];
    
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-40, 30)];
    textfield.placeholder = @"寻找感兴趣的商品";
    textfield.delegate = self;
    textfield.returnKeyType = UIReturnKeySearch;
    textfield.backgroundColor = white_Color;
    textfield.font = [UIFont systemFontOfSize:14];
    textfield.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textfield setLeftSearchViewWithImageName:@"bbs_home_search"];
    [textfield setBorderColor:MAINTextCOLOR];
    self.navigationItem.titleView = textfield;
    
    UIButton *messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    messageBtn.bounds  = CGRectMake(0, 0, 25, 25);
    //    [messageBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, -6)];
    [messageBtn setBackgroundImage:ImageNamed(@"bbs_search") forState:UIControlStateNormal];
    //[messageBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
    messageBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
    [messageBtn addTarget:self action:@selector(clickMessage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:messageBtn];
    self.navigationItem.rightBarButtonItem = item;
    
}
//点击消息
-(void)clickMessage:(UIButton *)buton{
    if ([userManager judgeLoginState]) {
        HBMessageViewController *vc = [[HBMessageViewController alloc]initWithNibName:@"HBMessageViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
//textfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textField-------%@",textField.text);
    //设置
    self.keyboreType = @"1";
    self.textFieldStr = textField.text;
    if (![HBHuTool isJudgeString:textField.text]) {
        GoodsViewController *vc = [[GoodsViewController alloc]initWithNibName:@"GoodsViewController" bundle:nil];
        vc.search_keywords = textField.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textField4 - 开始编辑");
    //设置
    self.keyboreType = @"2";
}// became first responder
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (![HBHuTool isJudgeString:textField.text]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
-(void)requestGoodsListData{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:CATEGORY_ITEM_CATEGORY params:nil completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            //NSDictionary *jsonDict = [JsonTool dataToObject:data];
            NSDictionary *modelDict = response.result[@"data"];
            weakSelf.categoryModel = [CategorysModel mj_objectWithKeyValues:modelDict];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf setupscrollview];
                //设置界面
                [weakSelf initDate];
            });
            
        }else{
            [app showToastView:response.message];
        }
    }];
}
#pragma mark ==========设置数据==========
-(void)setupscrollview{
    NSMutableArray *arr = [NSMutableArray array];
    [arr addObject:@"需求中心"];
    [arr addObject:@"推荐店铺"];
    for (CategryModel *model in self.categoryModel.categorys) {
        [arr addObject:model.cat_name];
    }
    self.titles = [arr copy];
}

#pragma mark =======初始化滚动视图==========
- (void)initDate{
    categoryViewHeight = 42.0;
//    self.titles = @[@"需求中心",@"推荐店铺",@"其他数据"];
    [self setupCategoryView];
}


- (void)setupCategoryView{
    
    if (self.myCategoryView) {
        [self.myCategoryView removeFromSuperview];
    }
    
    JXCategoryTitleView *categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, categoryViewHeight)];
    categoryView.delegate = self;
    [self.view addSubview:categoryView];
    //添加按钮
    categoryView.titles = self.titles;
    categoryView.titleFont =  Font(14);
    categoryView.titleSelectedFont = Font(16);
    categoryView.titleSelectedColor = MAINTextCOLOR;
    categoryView.titleColor = black_Color;
    categoryView.averageCellSpacingEnabled = NO;
    categoryView.backgroundColor = RGBA(247, 247, 247, 1);
    categoryView.titleColorGradientEnabled = YES;
    categoryView.titleLabelZoomEnabled = YES;
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.componentPosition = JXCategoryViewAutomaticDimension;
    lineView.indicatorHeight = 2.0;
    lineView.indicatorColor = MAINTextCOLOR;
    lineView.lineStyle = JXCategoryIndicatorLineStyle_Normal;
    categoryView.indicators = @[lineView];
    
    self.myCategoryView = categoryView;
    
    //self.listContainerView = [[JXCategoryListContainerView alloc] initWithDelegate:self];
    self.listContainerView = [[JXCategoryListContainerView alloc]initWithType:JXCategoryListContainerType_ScrollView delegate:self];
   // self.listContainerView.didAppearPercent = 0.99; //滚动一点就触发加载
    self.listContainerView.defaultSelectedIndex = 0;
    self.listContainerView.frame = CGRectMake(0, categoryViewHeight+1, SCREEN_WIDTH, SCREEN_HEIGHT - categoryViewHeight - bTabBarHeight-kBottomSafeHeight);
    [self.view addSubview:self.listContainerView];
    
    self.myCategoryView.contentScrollView = self.listContainerView.scrollView;

}

#pragma mark - JXCategoryViewDatasource
- (id<JXCategoryListContentViewDelegate>)preferredListAtIndex:(NSInteger)index {
    if (index==0) {
        HBDemandListVC *vc = [[HBDemandListVC alloc]init];
        vc.upController = self;
        return vc;
    }else if(index==1){
        HomeVC *home = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        return home;
    }else{
        return nil;
    }
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    
    if (index<2) {
        
    }else{
        [self.myCategoryView selectItemAtIndex:0];
        [self.listContainerView didClickSelectedItemAtIndex:0];
//        [self.myCategoryView reloadCellAtIndex:1];
        self.tabBarController.selectedIndex = 1;
        //通知
        [[NSNotificationCenter defaultCenter]postNotificationName:@"HBBCManageHomeChange" object:@{@"selectIndex":@(index)}];
    }
    
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //点击
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didClickSelectedItemAtIndex:(NSInteger)index {
   [self.listContainerView didClickSelectedItemAtIndex:index];
}

- (void)categoryView:(JXCategoryBaseView *)categoryView scrollingFromLeftIndex:(NSInteger)leftIndex toRightIndex:(NSInteger)rightIndex ratio:(CGFloat)ratio {
    [self.listContainerView scrollingFromLeftIndex:leftIndex toRightIndex:rightIndex ratio:ratio selectedIndex:categoryView.selectedIndex];
}

#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    id<JXCategoryListContentViewDelegate> list = [self preferredListAtIndex:index];
    return list;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

@end
