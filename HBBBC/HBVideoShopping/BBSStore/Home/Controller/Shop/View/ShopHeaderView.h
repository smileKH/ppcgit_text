//
//  ShopHeaderView.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHTCollectionViewWaterfallLayout.h"
#import "ZFPlayer.h"

@interface ShopHeaderView : UICollectionReusableView<ZFPlayerDelegate>//<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
//@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;
@property (nonatomic, strong) NSArray *collectionArray;

@property (strong, nonatomic) IBOutlet UIView *videoView;

@property (nonatomic, strong) NSString *vodeourl;//视频地址
@property (nonatomic, strong) ZFPlayerView *playerView;//播放器
@property (nonatomic, strong) ZFPlayerModel *playerModel;//播放资源model

@end
