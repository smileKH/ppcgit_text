//
//  ShopHeaderView.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShopHeaderView.h"
#import "ShopHeaderCell.h"

@implementation ShopHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    [self setupCollectionview];
}


/*
- (void)setupCollectionview
{
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.columnCount = 1;
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    self.collectionview.collectionViewLayout = layout;
    self.collectionview.delegate = self;
    self.collectionview.dataSource = self;
    [self.collectionview registerNib:[UINib nibWithNibName:@"ShopHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
}
#pragma mark <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 5;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ShopHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
//  cell.vodeourl = [self.collectionArray[indexPath.row] valueForKey:@"videosrc"];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenW, 220);
}
*/
- (void)setCollectionArray:(NSArray *)collectionArray
{
    _collectionArray = collectionArray;
    self.backgroundColor = red_Color;
//    [self.collectionview reloadData];
    [self setupScrollView];
}

- (void)setupScrollView
{
    [self addSubview:self.playerView];
    [self.playerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.left.right.equalTo(self);
        // 这里宽高比16：9，可以自定义视频宽高比
        make.height.equalTo(self.playerView.mas_width).multipliedBy(9.0f/16.0f);
    }];
    [self.playerView autoPlayTheVideo];
}

- (ZFPlayerView *)playerView
{
    if (!_playerView) {
        _playerView = [[ZFPlayerView alloc] init];
        /*****************************************************************************************
         *   // 指定控制层(可自定义)
         *   // ZFPlayerControlView *controlView = [[ZFPlayerControlView alloc] init];
         *   // 设置控制层和播放模型
         *   // 控制层传nil，默认使用ZFPlayerControlView(如自定义可传自定义的控制层)
         *   // 等效于 [_playerView playerModel:self.playerModel];
         ******************************************************************************************/
        ZFPlayerControlView *controllerView = [ZFPlayerControlView new];
        controllerView.backBtn.hidden = YES;
        controllerView.fullScreenBtn.hidden = YES;
        [_playerView playerControlView:controllerView playerModel:self.playerModel];
        
        // 设置代理
        _playerView.delegate = self;
        
        //（可选设置）可以设置视频的填充模式，内部设置默认（ZFPlayerLayerGravityResizeAspect：等比例填充，直到一个维度到达区域边界）
        // _playerView.playerLayerGravity = ZFPlayerLayerGravityResize;
        
        // 打开下载功能（默认没有这个功能）
        _playerView.hasDownload    = NO;
        
        // 打开预览图
        _playerView.hasPreviewView = YES;
        
        //        _playerView.forcePortrait = YES;
        /// 默认全屏播放
        //        _playerView.fullScreenPlay = YES;
        
    }
    return _playerView;
    
}

- (ZFPlayerModel *)playerModel
{
    
    if (!_playerModel) {
        _playerModel                  = [[ZFPlayerModel alloc] init];
        _playerModel.title            = @"";
        _playerModel.videoURL         = [NSURL URLWithString:self.vodeourl];
        _playerModel.placeholderImage = [UIImage imageNamed:@"loading_bgView1"];
        _playerModel.fatherView       = self.videoView;
        //       _playerModel.resolutionDic = @{@"高清" : self.videoURL.absoluteString,
        //       @"标清" : self.videoURL.absoluteString};
    }
    return _playerModel;
}

@end
