//
//  HBShopGoodsCollCell.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2019/1/2.
//  Copyright © 2019年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopModel.h"
@interface HBShopGoodsCollCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, strong) ItemList *model;

@end
