//
//  HBShopGoodsCollCell.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2019/1/2.
//  Copyright © 2019年 FirstVision. All rights reserved.
//

#import "HBShopGoodsCollCell.h"

@implementation HBShopGoodsCollCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.title.text = @"-";
}
-(void)setPlaceholderImage:(UIImage *)placeholderImage{
    _placeholderImage = placeholderImage;
    self.image.image = placeholderImage;
}

- (void)setModel:(ItemList *)model
{
    _model = model;
    [self.image sd_setImageWithURL:[NSURL URLWithString:model.image_default_id] placeholderImage:self.placeholderImage];
    
    self.title.text = model.title;
    
}

@end
