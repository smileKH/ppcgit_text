//
//  ShopGoodsCell.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/6.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShopModel.h"
@interface ShopGoodsCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *numbersd;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, strong) ItemList *model;
@end
