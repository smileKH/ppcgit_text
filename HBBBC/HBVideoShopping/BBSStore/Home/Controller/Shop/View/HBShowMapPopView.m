//
//  HBShowMapPopView.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBShowMapPopView.h"
static CGFloat bottomHeight = 230;
@interface HBShowMapPopView()
//主要的Windows
@property (nonatomic, strong) UIWindow *backWindow;
//黑色的view
@property (nonatomic , strong)UIView * drakView;
//白色的view
@property (nonatomic, strong) UIView *bottomView;
//取消button
@property (nonatomic , strong)UIButton * cancelButton;

//自定义view
@property (nonatomic ,strong)UIImageView *shopImgView;
@property (nonatomic ,strong)UILabel *titleLab;
@property (nonatomic ,strong)UIImageView *phImgView;
@property (nonatomic ,strong)UIImageView *adrImgView;
@property (nonatomic ,strong)UILabel *phoneLab;
@property (nonatomic ,strong)UILabel *adressLab;
@property (nonatomic ,strong)UILabel *dailLab;//距离
@property (nonatomic ,strong)UIButton *starteBtn;
@property (nonatomic ,strong)UIButton *phoneBtn;
@end
@implementation HBShowMapPopView

#pragma mark ==========点击电话按钮==========
-(void)clickPhoneBtn:(UIButton *)button{
    if (self.clickPhoneBtn) {
        self.clickPhoneBtn(nil);
    }
}
#pragma mark ==========点击开始导航==========
-(void)clickStarteBtn:(UIButton *)button{
    [self dismiss];
    if (self.clickSstarteBtn) {
        self.clickSstarteBtn(nil);
    }
}
-(void)setModel:(HBShopInfoModel *)model{
    _model = model;
    self.titleLab.text = [NSString stringWithFormat:@"%@",model.shopInfo.shop_name];
    self.dailLab.text = [NSString stringWithFormat:@"%@km",model.shopDsrData.delivery_speed_dsr];
    self.phoneLab.text = [NSString stringWithFormat:@"%@",model.shopInfo.mobile];
    self.adressLab.text = [NSString stringWithFormat:@"%@%@",model.shopInfo.shop_area,model.shopInfo.shop_addr];
    [self.shopImgView sd_setImageWithURL:[NSURL URLWithString:model.shopInfo.shop_logo] placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    self.adrImgView.image = [UIImage imageNamed:@"bbs_recen_zuo_biao"];
    self.phImgView.image = [UIImage imageNamed:@"bbs_recen_dian_hua"];
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        // 暗黑色的view
        UIView *darkView = [[UIView alloc] init];
        [darkView setAlpha:0];
        [darkView setUserInteractionEnabled:NO];
        [darkView setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [darkView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:darkView];
        self.drakView = darkView;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
        [darkView addGestureRecognizer:tap];
        
        // 所有按钮的底部view
        UIView *bottomView = [[UIView alloc] init];
        [bottomView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:bottomView];
        _bottomView = bottomView;
        
        [bottomView setFrame:CGRectMake(20, SCREEN_HEIGHT, SCREEN_WIDTH-40, bottomHeight)];
        //添加子视图
        [self addBootomView];
        
        [self setFrame:(CGRect){0, 0, SCREEN_WIDTH,SCREEN_HEIGHT}];
        [self.backWindow addSubview:self];
        
    }
    return self;
}

#pragma mark ==========添加子视图==========
-(void)addBootomView{
    //添加一个imageview
    [self.bottomView addSubview:self.shopImgView];
    [self.shopImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomView).offset(14);
        make.left.equalTo(self.bottomView).offset(11);
        make.size.mas_equalTo(CGSizeMake(85, 64));
    }];
    
    [self.bottomView addSubview:self.dailLab];
    [self.dailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shopImgView.mas_top);
        make.right.equalTo(self.bottomView).offset(-10);
        make.height.mas_equalTo(15);
    }];
    
    [self.bottomView addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shopImgView.mas_top);
        make.left.equalTo(self.shopImgView.mas_right).offset(10);
        make.right.equalTo(self.bottomView).offset(-60);
    }];
    
    [self.bottomView addSubview:self.phImgView];
    [self.phImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLab.mas_bottom).offset(10);
        make.left.equalTo(self.shopImgView.mas_right).offset(10);
        make.size.mas_equalTo(CGSizeMake(12, 12));
    }];
    
    [self.bottomView addSubview:self.phoneLab];
    [self.phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLab.mas_bottom).offset(10);
        make.left.equalTo(self.titleLab.mas_left).offset(20);
        make.height.mas_equalTo(15);
        make.right.equalTo(self.bottomView).offset(-10);
    }];
    
    [self.bottomView addSubview:self.adrImgView];
    [self.adrImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneLab.mas_bottom).offset(6);
        make.left.equalTo(self.shopImgView.mas_right).offset(10);
        make.size.mas_equalTo(CGSizeMake(12, 12));
    }];
    
    [self.bottomView addSubview:self.adressLab];
    [self.adressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneLab.mas_bottom).offset(6);
        make.left.equalTo(self.titleLab.mas_left).offset(20);
        make.right.equalTo(self.bottomView).offset(-10);
    }];
    

    
    [self.bottomView addSubview:self.starteBtn];
    [self.starteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bottomView).offset(-15);
        make.left.equalTo(self.bottomView).offset(15);
        make.height.mas_equalTo(45);
        make.width.mas_equalTo(((SCREEN_WIDTH-40)-45)/2);
    }];
    
    [self.bottomView addSubview:self.phoneBtn];
    [self.phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bottomView).offset(-15);
        make.right.equalTo(self.bottomView).offset(-15);
        make.height.mas_equalTo(45);
        make.width.mas_equalTo(((SCREEN_WIDTH-40)-45)/2);
    }];
    
}

#pragma mark ==========getter==========
-(UIImageView *)shopImgView{
    if (!_shopImgView) {
        _shopImgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:ZAN_WU_TUPIAN];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
            
        }) ;
    }
    return _shopImgView ;
}
-(UIImageView *)adrImgView{
    if (!_adrImgView) {
        _adrImgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:@"bbs_recen_zuo_biao"];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _adrImgView ;
}
-(UIImageView *)phImgView{
    if (!_phImgView) {
        _phImgView = ({
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
            //添加图片
            imgView.image = [UIImage imageNamed:@"bbs_recen_dian_hua"];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView ;
        }) ;
    }
    return _phImgView ;
}

-(UILabel *)phoneLab{
    if (!_phoneLab) {
        _phoneLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = [UIColor colorWithHexString:@"#4D5968"];//设置字体颜色
            label.font = [UIFont fontWithName:@"FZLTZHUNHK--GBK1-0" size: 11];//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 1; //行数
            
            label ;
        }) ;
    }
    return _phoneLab ;
}

-(UILabel *)adressLab{
    if (!_adressLab) {
        _adressLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = [UIColor colorWithHexString:@"#4D5968"];//设置字体颜色
            label.font = [UIFont fontWithName:@"FZLTZHUNHK--GBK1-0" size: 11];//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 3; //行数
            
            label ;
        }) ;
    }
    return _adressLab ;
}
-(UILabel *)dailLab{
    if (!_dailLab) {
        _dailLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = [UIColor colorWithHexString:@"#4D5968"];//设置字体颜色
            label.font = [UIFont fontWithName:@"FZLTZHUNHK--GBK1-0" size: 11];//设置字体大小
            label.textAlignment = NSTextAlignmentCenter;//设置对齐方式
            label.numberOfLines = 1; //行数
            label.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0];
            
            label.layer.cornerRadius = 1;
            label.clipsToBounds = YES;
            
            label ;
        }) ;
    }
    return _dailLab ;
}
-(UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = ({
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];//初始化控件
            //常用属性
            label.text = @"内容显示";//内容显示
            label.textColor = [UIColor colorWithHexString:@"#434343"];//设置字体颜色
            label.font = [UIFont fontWithName:@"FZLTZHUNHK--GBK1-0" size: 14];//设置字体大小
            label.textAlignment = NSTextAlignmentLeft;//设置对齐方式
            label.numberOfLines = 0; //行数
            
            label ;
        }) ;
    }
    return _titleLab ;
}
-(UIButton *)starteBtn{
    if (!_starteBtn) {
        _starteBtn = ({
            //创建按钮
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            //设置标题
            [button setTitle:@"开始导航" forState:UIControlStateNormal];
            //设置字体大小
            button.titleLabel.font = [UIFont fontWithName:@"FZLTZHUNHK--GBK1-0" size: 15];
            //设置title颜色
            [button setTitleColor:[UIColor colorWithRed:71/255.0 green:178/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];
            button.layer.cornerRadius = 45/2;
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor colorWithRed:71/255.0 green:178/255.0 blue:255/255.0 alpha:1.0].CGColor;
            //添加点击事件
            [button addTarget:self action:@selector(clickStarteBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            button;
        });
    }
    return _starteBtn;
}
-(UIButton *)phoneBtn{
    if (!_phoneBtn) {
        _phoneBtn = ({
            //创建按钮
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            //设置标题
            [button setTitle:@"拨打电话" forState:UIControlStateNormal];
            //设置字体大小
            button.titleLabel.font = [UIFont fontWithName:@"FZLTZHUNHK--GBK1-0" size: 15];
            //设置title颜色
            [button setTitleColor:[UIColor colorWithRed:71/255.0 green:178/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];
            button.layer.cornerRadius = 45/2;
            button.layer.borderWidth = 1;
            button.layer.borderColor = [UIColor colorWithRed:71/255.0 green:178/255.0 blue:255/255.0 alpha:1.0].CGColor;
            //添加点击事件
            [button addTarget:self action:@selector(clickPhoneBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            button;
        });
    }
    return _phoneBtn;
}
-(void)clickButton:(UIButton *)button{
    [self dismiss];
}
- (UIWindow *)backWindow {
    
    if (_backWindow == nil) {
        
        _backWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backWindow.windowLevel       = UIWindowLevelStatusBar;
        _backWindow.backgroundColor   = [UIColor clearColor];
        _backWindow.hidden = NO;
    }
    
    return _backWindow;
}
- (void)dismiss:(UITapGestureRecognizer *)tap {
    
    [self dismiss];
}

-(void)dismiss{
    if (self.popViewHideEvent) {
        self.popViewHideEvent(nil);
    }
    [UIView animateWithDuration:0.3f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         [self.drakView setAlpha:0];
                         [self.drakView setUserInteractionEnabled:NO];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = SCREEN_HEIGHT;
                         [_bottomView setFrame:frame];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         _backWindow.hidden = YES;
                         
                         [self removeFromSuperview];
                     }];
    
    
}


- (void)show {
    
    if (self.popViewShowEvent) {
        self.popViewShowEvent(nil);
    }
    
    _backWindow.hidden = NO;
    
    [UIView animateWithDuration:.75f
                          delay:0.2
         usingSpringWithDamping:0.65f
          initialSpringVelocity:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.drakView setAlpha:0.4f];
                         [self.drakView setUserInteractionEnabled:YES];
                         
                         CGRect frame = _bottomView.frame;
                         frame.origin.y = (SCREEN_HEIGHT-frame.size.height);
                         [_bottomView setFrame:frame];

                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
}


@end
