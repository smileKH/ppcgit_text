//
//  HBCollectionHeaderServiceView.h
//  HBWealProject
//
//  Created by aplle on 2018/1/6.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHTCollectionViewWaterfallLayout.h"
#import "ZFPlayer.h"
@interface HBCollectionHeaderServiceView : UICollectionReusableView
@property (nonatomic, strong) NSArray * cycleImgArray;//轮播图片

/** block方式监听点击 */
@property (nonatomic, copy) void (^clickActionBlock)(NSInteger currentIndex ,NSString *urlStr,UIImage*placeholderImage);
@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, strong) NSString *vodeourl;//视频地址
@property (nonatomic, strong) ZFPlayerView *playerView;//播放器
@property (nonatomic, strong) ZFPlayerModel *playerModel;//播放资源model
@property (nonatomic, strong) NSArray *collectionArray;
@end
