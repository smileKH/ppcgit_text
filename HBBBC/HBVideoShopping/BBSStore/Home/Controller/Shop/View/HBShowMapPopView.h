//
//  HBShowMapPopView.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBShopInfoModel.h"
@interface HBShowMapPopView : UIView
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * thumbnail;
@property (nonatomic, copy)void(^clickPhoneBtn)(id obj);
@property (nonatomic, copy)void(^clickSstarteBtn)(id obj);

@property (nonatomic, copy)void(^popViewHideEvent)(id obj);
@property (nonatomic, copy)void(^popViewShowEvent)(id obj);
- (void)show;
@property (nonatomic ,strong)HBShopInfoModel *model;

@end
