//
//  HBCollectionHeaderServiceView.m
//  HBWealProject
//
//  Created by aplle on 2018/1/6.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBCollectionHeaderServiceView.h"
#import "SDCycleScrollView.h"
#import "NSString+videoURl.h"
@interface HBCollectionHeaderServiceView ()<SDCycleScrollViewDelegate,ZFPlayerDelegate>
@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic, strong) UIView * videoView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) UIButton *beginVideoBtn;
@property (nonatomic, strong) NSMutableArray *imgArrs;
@end
@implementation HBCollectionHeaderServiceView
//
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //添加子视图
        _currentIndex = -1;
        [self addStarImageView];
        [self setupUI];
    }
    return self;
}
-(void)setPlaceholderImage:(UIImage *)placeholderImage{
    _placeholderImage = placeholderImage;
    _cycleScrollView.placeholderImage = placeholderImage;
}
-(void)setupUI{
    [self addSubview:self.cycleScrollView];
    [self bringSubviewToFront:self.beginVideoBtn];
}
-(void)addStarImageView{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 80, 80);
    btn.backgroundColor = [UIColor clearColor];
    btn.center = self.center;
    [btn setImage:[UIImage imageNamed:@"play_btn"] forState:UIControlStateNormal];
    btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
    btn.clipsToBounds = YES;
    
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    self.beginVideoBtn = btn;
}
-(void)btnAction:(UIButton*)btn{
    [self returnCliskAction];
}
#pragma mark - 界面的编写
- (SDCycleScrollView *)cycleScrollView
{
    if (_cycleScrollView == nil) {
        _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, IMAGE_HEIGHT) delegate:self placeholderImage:self.placeholderImage];
        _cycleScrollView.pageDotColor = [UIColor grayColor];
        _cycleScrollView.autoScrollTimeInterval = 2;
        _cycleScrollView.showPageControl = YES;
        _cycleScrollView.currentPageDotColor = [UIColor whiteColor];
        _cycleScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        WEAKSELF;
        _cycleScrollView.itemDidScrollOperationBlock = ^(NSInteger currentIndex) {
            weakSelf.currentIndex = currentIndex;
        };
        
        _cycleScrollView.clickItemOperationBlock = ^(NSInteger currentIndex) {
            weakSelf.currentIndex = currentIndex;
            [weakSelf returnCliskAction];
        };
    }
    return _cycleScrollView;
}
-(void)returnCliskAction{
    if (self.clickActionBlock) {
        if (self.currentIndex !=-1 && self.currentIndex < self.cycleImgArray.count) {
           
            UIImage *selecImage = _imgArrs[self.currentIndex];
            NSDictionary *dic = self.cycleImgArray [self.currentIndex];
            self.clickActionBlock(self.currentIndex, dic[@"videosrc"],selecImage);
        }
    }
}
- (void)setCycleImgArray:(NSArray *)cycleImgArray{
    if (!cycleImgArray.count) {
        return;
    }
    _cycleImgArray = cycleImgArray;
    
    _imgArrs = [NSMutableArray array];
    
    for (NSDictionary *dic in cycleImgArray) {
        NSString *videoUrlStr = dic[@"videosrc"];
        UIImage *videoFirstImage = [videoUrlStr thumbnailImageForVideo:videoUrlStr atTime:1];
        self.currentIndex = 0;
        [_imgArrs addObject:videoFirstImage];
    }
    _cycleScrollView = nil;
    [self setupUI];
    //设置滚动视图图片
    self.cycleScrollView.imageURLStringsGroup = _imgArrs;
//    [self.cycleScrollView reloadData];
}

- (void)setCollectionArray:(NSArray *)collectionArray
{
//    _collectionArray = collectionArray;
    self.backgroundColor = red_Color;
//        [self.collectionview reloadData];
//    [self setupScrollView];
}

- (void)setupScrollView
{
//    [self.videoView addSubview:self.playerView];
//    [self.playerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self).offset(0);
//        make.left.right.equalTo(self);
//        // 这里宽高比16：9，可以自定义视频宽高比
//        make.height.equalTo(self.playerView.mas_width).multipliedBy(9.0f/16.0f);
//    }];
//    [self.playerView pause];
}

//- (ZFPlayerView *)playerView
//{
//    if (!_playerView) {
//        _playerView = [[ZFPlayerView alloc] init];
//        /*****************************************************************************************
//         *   // 指定控制层(可自定义)
//         *   // ZFPlayerControlView *controlView = [[ZFPlayerControlView alloc] init];
//         *   // 设置控制层和播放模型
//         *   // 控制层传nil，默认使用ZFPlayerControlView(如自定义可传自定义的控制层)
//         *   // 等效于 [_playerView playerModel:self.playerModel];
//         ******************************************************************************************/
//        ZFPlayerControlView *controllerView = [ZFPlayerControlView new];
//
//        controllerView.backBtn.hidden = YES;
//        controllerView.fullScreenBtn.hidden = YES;
//        [_playerView playerControlView:controllerView playerModel:self.playerModel];
//
//        // 设置代理
//        _playerView.delegate = self;
//        _playerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200);
//
//        //（可选设置）可以设置视频的填充模式，内部设置默认（ZFPlayerLayerGravityResizeAspect：等比例填充，直到一个维度到达区域边界）
//        // _playerView.playerLayerGravity = ZFPlayerLayerGravityResize;
//
//        // 打开下载功能（默认没有这个功能）
//        _playerView.hasDownload    = NO;
//
//        // 打开预览图
//        _playerView.hasPreviewView = YES;
//
//        //        _playerView.forcePortrait = YES;
//        /// 默认全屏播放
//        //        _playerView.fullScreenPlay = YES;
//
//    }
//    return _playerView;
//
//}
//
//- (ZFPlayerModel *)playerModel
//{
//
//    if (!_playerModel) {
//        _playerModel                  = [[ZFPlayerModel alloc] init];
//        _playerModel.title            = @"";
//        _playerModel.videoURL         = [NSURL URLWithString:self.vodeourl];
//        _playerModel.placeholderImage = [UIImage imageNamed:@"loading_bgView1"];
//        _playerModel.fatherView       = self.videoView;
//        //       _playerModel.resolutionDic = @{@"高清" : self.videoURL.absoluteString,
//        //       @"标清" : self.videoURL.absoluteString};
//    }
//    return _playerModel;
//}
@end
