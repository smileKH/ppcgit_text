//
//  ShopGoodsCell.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/6.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShopGoodsCell.h"

@implementation ShopGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.title.text = @"-";
    self.price.text =@"-";
    self.numbersd.text = @"-";
}
-(void)setPlaceholderImage:(UIImage *)placeholderImage{
    _placeholderImage = placeholderImage;
    self.image.image = placeholderImage;
}

- (void)setModel:(ItemList *)model
{
    _model = model;
    [self.image sd_setImageWithURL:[NSURL URLWithString:model.image_default_id] placeholderImage:self.placeholderImage];
   
    self.title.text = model.title;
    
    self.price.text = [NSString stringWithFormat:@"¥ %0.2f",[model.price doubleValue]];
    self.numbersd.text = [NSString stringWithFormat:@"已有%@人购买",model.sold_quantity];
}
@end
