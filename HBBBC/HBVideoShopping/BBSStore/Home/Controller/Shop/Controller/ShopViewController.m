//
//  ShopViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/16.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShopViewController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "ShopGoodsCell.h"
#import "ShopHeaderView.h"
#import "ShopModel.h"
#import "ProductFirstViewController.h"
#import "PopoverView.h"

#import "HBCollectionHeaderServiceView.h"
#import "HBCollectionFooterServiceView.h"
#import "SearchViewController.h"
#import "GoodsViewController.h"
#import "HBShopInfoModel.h"
#import "HBShowShopMapVC.h"
#import "HBShopGoodsCollCell.h"
#import "HBPersonDetailVC.h"
@interface ShopViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout,UIAlertViewDelegate>
{
    CHTCollectionViewWaterfallLayout *layout;
    NSArray *_banner;
    NSArray *_shopGoodsList;
    NSString *_videoUrl;
}
@property (nonatomic, strong) ShopModel *shopModel;
@property (nonatomic , strong) HBCollectionHeaderServiceView * headerView;
@property (nonatomic, strong) VideoViewController *videoVC;
// 网络请求签第一次展示商品区域
@property (nonatomic, assign) BOOL showFirstDis;
@property (nonatomic ,strong)HBShopInfoModel *shopInfoModel;

@end

@implementation ShopViewController

#pragma mark ---- 获取店铺信息
- (void)shopDetial
{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"shop_id"] = self.shop_id;
    [LBService post:GOODS_SHOP_INDEX params:parameters completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        if (response.succeed) {
            //
            weakSelf.shopModel = [ShopModel mj_objectWithKeyValues:response.result[@"data"]];
            
            NSArray *arrys =  self.shopModel.widgets;
            
            for (NSInteger i = 0; i < arrys.count; i ++) {
                Widgets *wid = arrys[i];
                if ([wid.widgets_type isEqualToString:@"slider_v"]) {
                    //banner 滚动视频
                    _banner = wid.params;
                }else if ([wid.widgets_type isEqualToString:@"goods"]){
                    //商品
                    _shopGoodsList = wid.itemlist;
                }else if([wid.widgets_type isEqualToString:@"nav"]){
                    //导航栏 没有单视频了  有待完善
                    //播放的视频 onevideo
//                    _videoUrl = [wid.params valueForKey:@"videourl"];
//                    if ([HBHuTool judgeArrayIsEmpty:wid.params]) {
//                        _videoUrl = wid.params[0][@"videourl"];
//                    }
                    
                }
            }
            weakSelf.showFirstDis = NO;
            [self.collectionview reloadData];
            if ([_videoUrl isKindOfClass:[NSArray class]]) {
                
            }else{
                //视频连接
                if (_videoUrl.length > 0) {
                    [weakSelf loadVideo:_videoUrl placeholderImage:weakSelf.placeholderImage];
                }
            }
            
            
        }else{
            [app showToastView:response.message];
        }
    }];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _showFirstDis = YES;
    //店铺信息
    [self setupCollectionview];
    //获取店铺详情
    [self shopDetial];
    //获取店铺基本信息
    [self requestShopInfoData];
    //添加悬浮按钮
    [self addFolatingball];
    
}
#pragma mark - 添加addFolatingball
/** 添加悬浮按钮 */
- (void)addFolatingball {

    UIButton *topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    topBtn.frame =CGRectMake(SCREEN_WIDTH-60-20, SCREEN_HEIGHT-80-120, 60, 60);
    [topBtn setImage:[UIImage imageNamed:@"bbs_recen_zuo_top"] forState:UIControlStateNormal];
    [topBtn addTarget:self action:@selector(clickTopBtn:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:topBtn];
    
    UIButton *bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bottomBtn.frame =CGRectMake(SCREEN_WIDTH-60-20, SCREEN_HEIGHT-20-120, 60, 60);
    [bottomBtn setImage:[UIImage imageNamed:@"bbs_recen_zuo_bottom"] forState:UIControlStateNormal];
    [bottomBtn addTarget:self action:@selector(clickBottomBtnBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bottomBtn];
}
#pragma mark ==========点击电话按钮按钮==========
-(void)clickTopBtn:(UIButton *)button{
    
    //拨打电话
    NSString *phoneStr = nil;
    if (self.shopInfoModel.shopInfo.tel.length>0) {
        phoneStr = self.shopInfoModel.shopInfo.tel;
    }else{
        phoneStr = self.shopInfoModel.shopInfo.mobile;
    }
    NSString *message = [NSString stringWithFormat:@"确认拨打店铺电话%@",phoneStr];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    alert.tag = 1001;
    alert.delegate = self;
    [alert show];
}
#pragma mark ==========点击底部导航按钮==========
-(void)clickBottomBtnBtn:(UIButton *)button{
    HBShowShopMapVC *vc= [[HBShowShopMapVC alloc] initWithNibName:@"HBShowShopMapVC" bundle:nil];
    vc.model = self.shopInfoModel;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark ==========alertViewDelegate==========
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1001) {
        if (buttonIndex==1) {
            NSString *phoneStr = nil;
            if (self.shopInfoModel.shopInfo.tel.length>0) {
                phoneStr = self.shopInfoModel.shopInfo.tel;
            }else{
                phoneStr = self.shopInfoModel.shopInfo.mobile;
            }
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",phoneStr];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }
}
#pragma mark ==========获取基本信息==========
-(void)requestShopInfoData{
    WEAKSELF;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"shop_id"] = self.shop_id;
    [LBService post:GOODS_SHOP_BASIC params:parameters completion:^(LBResponse *response) {
        if (response.succeed) {
            //成功
            weakSelf.shopInfoModel = [HBShopInfoModel mj_objectWithKeyValues:response.result[@"data"]];
        }else{
            [app showToastView:response.message];
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.topItem.title = @"";
    self.title  = @"店铺";
    
    [self setNavBar];
}

- (void)setNavBar
{
    UIButton *messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    messageBtn.bounds  = CGRectMake(0, 0, 30, 30);
    [messageBtn setImage:ImageNamed(@"bbs_list") forState:UIControlStateNormal];
    [messageBtn addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:messageBtn];

    UIButton *messageBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn1 setImage:ImageNamed(@"nav_more") forState:UIControlStateNormal];
    messageBtn1.bounds  = CGRectMake(0, 0, 30, 30);
    [messageBtn1 addTarget:self action:@selector(rightAction1:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithCustomView:messageBtn1];
    self.navigationItem.rightBarButtonItems = @[item1,item];
}
-(void)clickBtn:(UIButton *)button{
    if (button.tag==1) {
        //
        [self rightAction:nil];
    }else{
        [self rightAction1:nil];
    }
}
#pragma mark ==========一进来就播放视频==========
//模拟器展示有问题
- (void)loadVideo:(NSString*)videoUrlStr placeholderImage:(UIImage *)placeholderImage{
    if (!videoUrlStr.length) {
        [app showToastView:@"视频不存在"];
        return;
    }
    _videoVC = nil;
    if (!_videoVC) {
        _videoVC =[[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
        _videoVC.view.frame = CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT);
    }
        _videoVC.placeholderImage = placeholderImage;
        _videoVC.vodeourl = [videoUrlStr URLEncodedString];
    
        [self.view.window addSubview:_videoVC.view];
        [self addChildViewController:_videoVC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupCollectionview
{
    self.collectionarray = [NSMutableArray arrayWithArray:@[@"",@"",@"",@"",@"",@"",@""]];
    
    layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.columnCount = 2;
    layout.headerHeight =0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 0, 5);
    layout.headerHeight = IMAGE_HEIGHT;
    layout.footerHeight = 10;
    self.collectionview.collectionViewLayout = layout;
    
    [self.collectionview registerNib:[UINib nibWithNibName:@"ShopGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"default"];
    [self.collectionview registerNib:[UINib nibWithNibName:@"HBShopGoodsCollCell" bundle:nil] forCellWithReuseIdentifier:@"HBShopGoodsCollCell"];
    [self.collectionview registerClass:[HBCollectionHeaderServiceView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:@"HBCollectionHeaderServiceView"];
    [self.collectionview registerClass:[HBCollectionFooterServiceView class] forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:@"HBCollectionFooterServiceView"];
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (_showFirstDis) {
        return 6;
    }
    return  [_shopGoodsList count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.shopModel.shopdata.shop_type isEqualToString:@"other"]) {
        //其他
        HBShopGoodsCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HBShopGoodsCollCell" forIndexPath:indexPath];
        cell.placeholderImage = self.placeholderImage;
        if (!_showFirstDis) {
            cell.model = _shopGoodsList[indexPath.row];
        }
        return cell;
    }else{
        ShopGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"default" forIndexPath:indexPath];
        cell.placeholderImage = self.placeholderImage;
        if (!_showFirstDis) {
            cell.model = _shopGoodsList[indexPath.row];
        }
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(SCREEN_WIDTH/2, 245);
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_showFirstDis) {
        return;
    }
    if ([self.shopModel.shopdata.shop_type isEqualToString:@"other"]) {
        //个人名片
        ItemList *items = _shopGoodsList[indexPath.row];
        //人员类详情
        HBPersonDetailVC *vc= [[HBPersonDetailVC alloc] initWithNibName:@"HBPersonDetailVC" bundle:nil];
        vc.item_id = items.item_id;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        ItemList *items = _shopGoodsList[indexPath.row];
        ProductFirstViewController *vc= [[ProductFirstViewController alloc] initWithNibName:@"ProductFirstViewController" bundle:nil];
        vc.item_id = items.item_id;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{

    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader])
    { // header
        if (!self.headerView) {
            self.headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"HBCollectionHeaderServiceView" forIndexPath:indexPath];
            self.headerView.backgroundColor = white_Color;
            WEAKSELF;
            self.headerView.clickActionBlock = ^(NSInteger currentIndex, NSString *urlStr ,UIImage*placeholderImage) {
                [weakSelf loadVideo:urlStr placeholderImage: placeholderImage];
            };
        }
        if (!self.headerView.cycleImgArray.count) {
            self.headerView.cycleImgArray = _banner;
        }
        if (self.placeholderImage != self.headerView.placeholderImage) {
            self.headerView.placeholderImage = self.placeholderImage;
        }
        return self.headerView;
    }else if([kind isEqualToString:CHTCollectionElementKindSectionFooter]){
        
        HBCollectionFooterServiceView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"HBCollectionFooterServiceView"
                                                                                        forIndexPath:indexPath];
        view.backgroundColor = Color_BG;
        return view;
    }
    
    return nil;
  
}

#pragma mark-表头尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{

    return CGSizeMake(SCREEN_WIDTH, IMAGE_HEIGHT);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(SCREEN_WIDTH, 10);
}
#pragma mark ==========点击更多菜单按钮==========
- (void)rightAction1:(UIButton *)sender
{
    //弹出视图
    [self popoverActionSender:sender andIsShow:NO andShareUrl:nil];
}

- (void)rightAction:(UIButton *)sender{
    SearchViewController *vc= [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    vc.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [vc setHeader];
    vc.tablearray = self.shopModel.shopcat;
    WEAKSELF;
    vc.clickRowData = ^(Shopcat *cat) {
        //返回
        if (cat) {
            //
            NSLog(@"打印一下cat_id %@  和shop_id  %@",cat.cat_id,cat.shop_id);
            GoodsViewController *vc = [[GoodsViewController alloc]initWithNibName:@"GoodsViewController" bundle:nil];
            vc.search_shop_cat_id = cat.cat_id;
            vc.shop_id = cat.shop_id;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else{
            GoodsViewController *vc = [[GoodsViewController alloc]initWithNibName:@"GoodsViewController" bundle:nil];
            vc.shop_id = self.shop_id;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
    };
    [self.view.window addSubview:vc.view];
    [self addChildViewController:vc];
    
}
@end

