//
//  ShopViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/16.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoViewController.h"
@interface ShopViewController : HBRootViewController

@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSArray *collectionarray;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionview;
@end
