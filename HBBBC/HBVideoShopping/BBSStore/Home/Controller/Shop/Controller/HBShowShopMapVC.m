//
//  HBShowShopMapVC.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//显示地图

#import "HBShowShopMapVC.h"
#import "MapManager.h"
#import "HBShowMapPopView.h"
#import "JhtFloatingBall.h"
//gps纠偏类
//#import <JZLocationConverter.h>
@interface HBShowShopMapVC ()<UIAlertViewDelegate>
//@property (nonatomic, strong)MAMapView *mapView;
////导航起点
//@property (nonatomic,strong)AMapNaviPoint *startPoint;
////导航终点
//@property (nonatomic,strong)AMapNaviPoint *endPoint;
@property (nonatomic ,strong)HBShowMapPopView *popView;
@property (nonatomic ,strong)JhtFloatingBall *floatingBtn;
@property (nonatomic ,strong)MapManager *manager;
@end

@implementation HBShowShopMapVC
- (void)viewDidLoad {
    self.title = @"店铺地址";
    //不管做什么地图操作都先定位自己的位置，不然后面会有一些bug（当然只是这个demo而言）
    [self locationOnlySelf];
    
    //添加自定义view
    [self addCusShopInfoView];
    //添加悬浮按钮
//    [self addFolatingball];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"商家信息" style:UIBarButtonItemStylePlain target:self action:@selector(addAnonation)];
    self.navigationItem.rightBarButtonItem = rightItem;
}
//给一个坐标，在地图上显示大头针
-(void)addAnonation{
    WEAKSELF;
    self.popView = [[HBShowMapPopView alloc]init];
    self.popView.model = self.model;
    self.popView.clickPhoneBtn = ^(id obj) {
        //点击电话
        [weakSelf popPhoneTel];
    };
    self.popView.clickSstarteBtn = ^(id obj) {
        //点击开始导航
        [weakSelf popNavigation];
    };
    self.popView.popViewShowEvent = ^(id obj) {
        //显示
        //        weakSelf.floatingBtn.hidden = YES;
    };
    self.popView.popViewHideEvent = ^(id obj) {
        //隐藏
        //        weakSelf.floatingBtn.hidden = NO;
    };
    [self.popView show];
}
//显示自己的定位信息
-(void)locationOnlySelf{
    //显示定位
    self.manager = [MapManager sharedManager];
    self.manager.controller = self;
    WEAKSELF;
    [self.manager initMapViewWithBlock:^{
        //标注一个点
        CLLocationCoordinate2D coor;
        coor.latitude = [weakSelf.model.shopInfo.latitude doubleValue];
        coor.longitude = [weakSelf.model.shopInfo.longitude doubleValue];
        [[MapManager sharedManager] addAnomationWithCoor:coor];
    }];
    
}

#pragma mark ==========添加子定义==========
-(void)addCusShopInfoView{
    WEAKSELF;
    self.popView = [[HBShowMapPopView alloc]init];
    self.popView.model = self.model;
    self.popView.clickPhoneBtn = ^(id obj) {
        //点击电话
    };
    self.popView.clickSstarteBtn = ^(id obj) {
        //点击开始导航
        [weakSelf popNavigation];
    };
    self.popView.popViewShowEvent = ^(id obj) {
        //显示
//        weakSelf.floatingBtn.hidden = YES;
    };
    self.popView.popViewHideEvent = ^(id obj) {
        //隐藏
//        weakSelf.floatingBtn.hidden = NO;
    };
    [self.popView show];
}
#pragma mark ==========打电话==========
-(void)popPhoneTel{
    NSString *phoneStr = nil;
    if (self.model.shopInfo.tel.length>0) {
        phoneStr = self.model.shopInfo.tel;
    }else{
        phoneStr = self.model.shopInfo.mobile;
    }
    NSString *message = [NSString stringWithFormat:@"确认拨打店铺电话%@",phoneStr];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    alert.tag = 1001;
    alert.delegate = self;
    [alert show];
}
#pragma mark ==========alertViewDelegate==========
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1001) {
        if (buttonIndex==1) {
            NSString *phoneStr = nil;
            if (self.model.shopInfo.tel.length>0) {
                phoneStr = self.model.shopInfo.tel;
            }else{
                phoneStr = self.model.shopInfo.mobile;
            }
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",phoneStr];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }
}
#pragma mark ==========开始导航==========
-(void)popNavigation{
    [[MapManager sharedManager] openNavigationManage];
}
#pragma mark - 添加addFolatingball
/** 添加悬浮按钮 */
- (void)addFolatingball {
//    UIImage *suspendedBallImage = [UIImage imageNamed:@"bbs_recen_zuo_bottom"];
//    self.floatingBtn = [[JhtFloatingBall alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-suspendedBallImage.size.width, SCREEN_HEIGHT-suspendedBallImage.size.height-120, 60, 60)];
//    self.floatingBtn.image = suspendedBallImage;
//    self.floatingBtn.stayAlpha = 0.5;
//    self.floatingBtn.delegate = self;
//    self.floatingBtn.stayMode = stayMode_OnlyLeftAndRight;
//    [self.view addSubview:self.floatingBtn];
//    [self.floatingBtn  mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.view) ;
//        make.bottom.equalTo(self.view).offset(-60) ;
//        make.size.mas_equalTo(CGSizeMake(60, 60)) ;
//    }] ;
}
#pragma mark - JhtFloatingBallDelegate
/** 悬浮按钮点击 事件 */
- (void)tapFloatingBall {
    [self.popView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
