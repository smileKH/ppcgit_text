//
//  HBShowShopMapVC.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBRootViewController.h"
#import "HBShopInfoModel.h"
@interface HBShowShopMapVC : HBRootViewController
@property (nonatomic ,strong)HBShopInfoModel *model;
@end
