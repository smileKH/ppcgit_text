//
//  VideoViewController.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "VideoViewController.h"
#import "NSString+videoURl.h"
@interface VideoViewController ()<ZFPlayerDelegate>

@end

@implementation VideoViewController
-(void)setVodeourl:(NSString *)vodeourl{
    _vodeourl = vodeourl;
    if (!self.placeholderImage &&vodeourl.length) {
        self.placeholderImage = [vodeourl thumbnailImageForVideo:vodeourl atTime:1];
    }
     [self.playerView autoPlayTheVideo];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UISQURE
//    [self.view addSubview:self.playerView];
   
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.playerView pause];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self dissMissVideo];
    if (_videoDissMissBlock) {
        _videoDissMissBlock(YES);
    }
}

-(void)dissMissVideo{
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    _playerModel = nil;
    _playerView = nil;
}

- (ZFPlayerView *)playerView
{
    if (!_playerView) {
        
        
        _playerView = [[ZFPlayerView alloc] init];
        
        /*****************************************************************************************
         *   // 指定控制层(可自定义)
         *   // ZFPlayerControlView *controlView = [[ZFPlayerControlView alloc] init];
         *   // 设置控制层和播放模型
         *   // 控制层传nil，默认使用ZFPlayerControlView(如自定义可传自定义的控制层)
         *   // 等效于 [_playerView playerModel:self.playerModel];
         ******************************************************************************************/
        ZFPlayerControlView *controllerView = [ZFPlayerControlView new];
        controllerView.backBtn.hidden = YES;
        controllerView.fullScreenBtn.hidden = YES;
        [_playerView playerControlView:controllerView playerModel:self.playerModel];
        
        
        
        // 设置代理
        _playerView.delegate = self;
//        _playerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        
        //（可选设置）可以设置视频的填充模式，内部设置默认（ZFPlayerLayerGravityResizeAspect：等比例填充，直到一个维度到达区域边界）
        // _playerView.playerLayerGravity = ZFPlayerLayerGravityResize;
        
        // 打开下载功能（默认没有这个功能）
        _playerView.hasDownload    = NO;
        
        // 打开预览图
        _playerView.hasPreviewView = YES;
        
        //        _playerView.forcePortrait = YES;
        /// 默认全屏播放
        //        _playerView.fullScreenPlay = YES;
        
    }
    return _playerView;
    
}

- (ZFPlayerModel *)playerModel
{
    if (!_playerModel) {
        _playerModel                  = [[ZFPlayerModel alloc] init];
        _playerModel.title            = @"";
        _playerModel.videoURL         = [NSURL URLWithString:self.vodeourl];
       _playerModel.placeholderImage = _placeholderImage;
        _playerModel.fatherView       = self.videoView;
        //        _playerModel.resolutionDic = @{@"高清" : self.videoURL.absoluteString,
        //                                       @"标清" : self.videoURL.absoluteString};
    }
    return _playerModel;
}
@end
