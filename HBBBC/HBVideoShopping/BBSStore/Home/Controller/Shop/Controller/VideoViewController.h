//
//  VideoViewController.h
//  BBSStore
//
//  Created by 马云龙 on 2018/1/17.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFPlayer.h"



@interface VideoViewController : HBRootViewController
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, strong) NSString *vodeourl;//视频地址
@property (nonatomic, strong) ZFPlayerView *playerView;//播放器
@property (nonatomic, strong) ZFPlayerModel *playerModel;//播放资源model
/** block方式监听点击 */
@property (nonatomic, copy) void (^videoDissMissBlock)(BOOL isDissmiss);
/**
 销毁video
 */
-(void)dissMissVideo;
@end
