//
//  ShopModel.m
//  BBSStore
//
//  Created by 马云龙 on 2018/3/6.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "ShopModel.h"

@implementation ShopModel
- (void)setShopcat:(NSArray *)shopcat
{
    _shopcat = [Shopcat mj_objectArrayWithKeyValuesArray:shopcat];
}
- (void)setWidgets:(NSArray *)widgets
{
    _widgets = [Widgets mj_objectArrayWithKeyValuesArray:widgets];
}
@end

@implementation Widgets
-(void)setShowitems:(NSDictionary *)showitems

{
    _showitems = showitems;
    _itemlist = [ItemList mj_objectArrayWithKeyValuesArray:showitems[@"list"]];
}
@end

@implementation ItemList

@end
@implementation PromotionShop

@end

@implementation GiftShop

@end
@implementation Shopdata

@end


@implementation Shopcat
- (void)setChildren:(NSArray *)children
{
    _children = [Children mj_objectArrayWithKeyValuesArray:children];
}
@end
@implementation Children

@end
