//
//  ShopModel.h
//  BBSStore
//
//  Created by 马云龙 on 2018/3/6.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Shopcat;
@class Shopdata;
@class children;
@class ItemList;
@class Gift;
@class Widgets;
@class Promotion;
@interface ShopModel : NSObject
@property (nonatomic, strong) NSString *tally_dsr;
@property (nonatomic, strong) NSString *attitude_dsr;
@property (nonatomic, strong) NSString *delivery_speed_dsr;
@property (nonatomic, strong) NSArray *widgets;
@property (nonatomic, strong) Shopdata *shopdata;
@property (nonatomic, strong) NSArray *shopcat;
@end

@interface Widgets : NSObject
@property (nonatomic, strong) NSString *widgets_id;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *widgets_type;
@property (nonatomic, strong) NSString *page_name;
@property (nonatomic, strong) NSString *theme;
@property (nonatomic, strong) NSString *order_sort;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) id params;
@property (nonatomic, strong) NSString *modified_time;
@property (nonatomic, strong) NSDictionary *showitems;
@property (nonatomic, strong) NSArray *itemlist;
@property (nonatomic, strong) NSString *itemIds;
@end

@interface ItemList : NSObject
@property (nonatomic, strong) NSString *item_id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image_default_id;;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *sold_quantity;
@property (nonatomic, strong) NSArray *promotion;
@property (nonatomic, strong) Gift *gift;
@end


@interface PromotionShop : NSObject
@property (nonatomic, strong) NSString *promotion_id;
@property (nonatomic, strong) NSString *tag;
@end
//
@interface GiftShop : NSObject
@property (nonatomic, strong) NSString *gift_id;
@property (nonatomic, strong) NSString *promotion_tag;
@end


@interface Shopdata : NSObject
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *shop_name;
@property (nonatomic, strong) NSString *shop_descript;
@property (nonatomic, strong) NSString *shop_type;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *open_time;
@property (nonatomic, strong) NSString *qq;
@property (nonatomic, strong) NSString *wangwang;
@property (nonatomic, strong) NSString *shop_logo;
@property (nonatomic, strong) NSString *shop_area;
@property (nonatomic, strong) NSString *shop_addr;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *shopname;
@property (nonatomic, strong) NSString *shoptype;
@property (nonatomic, strong) NSString *h5href;
@end

@interface Shopcat : NSObject
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *parent_id;
@property (nonatomic, strong) NSString *cat_path;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *is_leaf;
@property (nonatomic, strong) NSString *cat_name;
@property (nonatomic, strong) NSString *order_sort;
@property (nonatomic, strong) NSString *modified_time;
@property (nonatomic, strong) NSString *disabled;
@property (nonatomic, strong) NSArray *children;
@end

@interface Children : NSObject
@property (nonatomic, strong) NSString *cat_id;
@property (nonatomic, strong) NSString *shop_id;
@property (nonatomic, strong) NSString *parent_id;
@property (nonatomic, strong) NSString *cat_path;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *is_leaf;
@property (nonatomic, strong) NSString *cat_name;
@property (nonatomic, strong) NSString *order_sort;
@property (nonatomic, strong) NSString *modified_time;
@property (nonatomic, strong) NSString *disabled;

@end
