//
//  HBShopInfoModel.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2018/12/30.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HBShopInfoItemModel;
@class HBShopDsrDataModel;
@interface HBShopInfoModel : NSObject
@property (nonatomic ,strong)HBShopInfoItemModel *shopInfo;
@property (nonatomic ,strong)HBShopDsrDataModel *shopDsrData;
@end

@interface HBShopInfoItemModel : NSObject
@property (nonatomic ,strong)NSString *shop_id;
@property (nonatomic ,strong)NSString *shop_name;
@property (nonatomic ,strong)NSString *shop_descript;
@property (nonatomic ,strong)NSString *shop_type;
@property (nonatomic ,strong)NSString *status;
@property (nonatomic ,strong)NSString *open_time;
@property (nonatomic ,strong)NSString *qq;
@property (nonatomic ,strong)NSString *wangwang;
@property (nonatomic ,strong)NSString *shop_logo;
@property (nonatomic ,strong)NSString *shop_area;
@property (nonatomic ,strong)NSString *shop_addr;
@property (nonatomic ,strong)NSString *longitude;
@property (nonatomic ,strong)NSString *latitude;
@property (nonatomic ,strong)NSString *mobile;
@property (nonatomic ,strong)NSString *tel;
@property (nonatomic ,strong)NSString *shopname;
@property (nonatomic ,strong)NSString *shoptype;
@property (nonatomic ,strong)NSString *distance;
@end

@interface HBShopDsrDataModel : NSObject
@property (nonatomic ,strong)NSString *tally_dsr;
@property (nonatomic ,strong)NSString *attitude_dsr;
@property (nonatomic ,strong)NSString *delivery_speed_dsr;
@end
