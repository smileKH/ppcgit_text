//
//  MapChooseAddressVC.h
//  MapChooseAddress
//
//  Created by mac on 2018/9/10.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "HBRootViewController.h"
//高德地图
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
@protocol MapChooseAddressVCDelegate <NSObject>

@optional
//AMapGeoPoint *location
//AMapReGeocode *regeocode;
-(void)addressDidClickGeoPoint:(AMapGeoPoint *)location regeocode:(NSString *)address Province:(NSString *)province City:(NSString *)city;

@end

@interface MapChooseAddressVC : HBRootViewController
@property(nonatomic,weak)id<MapChooseAddressVCDelegate>delegate;
@end
