//
//  HomeVC.m
//  BBSStore
//
//  Created by 马云龙 on 2018/1/4.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "HomeVC.h"
#import "HomeItem.h"
#import "UITextField+LeftView.h"
#import "ShopViewController.h"
#import "HomeFooter.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "GoodsViewController.h"
#import "HBHomeEnterVC.h"
@interface HomeVC ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout,UITextFieldDelegate>
@property (nonatomic, strong) NSMutableDictionary *footModel;//广告位
@property (nonatomic ,strong)NSString *textFieldStr;
@property (nonatomic ,strong)NSString *keyboreType;
@end

@implementation HomeVC
#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

- (void)listDidAppear {}

- (void)listDidDisappear {}
- (void)list{
    WEAKSELF;
    [MBProgressHUD showActivityMessageInView:Text_JIA_ZAI_ZHONG];
    [LBService post:HOME_THEME_MODULES params:@{@"tmpl":@"index"} completion:^(LBResponse *response) {
        [MBProgressHUD hideHUD];
        [weakSelf.collectionView.mj_footer endRefreshing];
        [weakSelf.collectionView.mj_header endRefreshing];
        if (response.succeed) {
            //
            NSDictionary *returndata = response.result[@"data"];
            NSLog(@"打印一下获取到的参数%@",returndata);
            NSArray *items = returndata[@"modules"];
            //            NSString *banners =  returndata[@"brand_logo"];
            
            for (NSDictionary *tmpl in items) {
                
                NSString *widget = tmpl[@"widget"];
                if ([widget isEqualToString:@"double_pics"]) {
                    NSArray *picItems = tmpl[@"params"][@"pic"];
                    for (NSDictionary *dic in picItems) {
                        
                        [self.collectionarray addObject:dic];
                    }
                }else{
                    NSDictionary *dic = tmpl[@"params"];
                    [weakSelf.collectionarray addObject:dic];
                    
                }
                
            }
            for (int i=0; i<weakSelf.collectionarray.count; i++) {
                NSDictionary *dic = weakSelf.collectionarray[i];
                if ([dic[@"linktype"] isEqualToString:@"h5"]) {
                    weakSelf.footModel = [dic copy];
                    [weakSelf.collectionarray removeObject:dic];
                }
            }
//            weakSelf.footModel = weakSelf.collectionarray.lastObject;
//            [weakSelf.collectionarray removeLastObject];
            
            [weakSelf.collectionView reloadData];
        }else{
            //出现错误 提示
            [app showToastView:response.message];
        }
        //验证有没有数据
        if (weakSelf.collectionarray.count>0) {
            //隐藏
            weakSelf.collectionView.hidden = NO;
            [weakSelf hideContentNullPromptView];

        }
        else{
            //显示
            self.collectionView.hidden = YES;
            [weakSelf showContentNullPromptImg:nil withLabelName:nil withButtonName:nil];
        }
    }];
    
}
#pragma mark - 点击刷新按钮
- (void)clickSpaceButton{
    //请求数据
    [self list];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //隐藏导航栏
    self.isHidenNaviBar = YES;
    //设置导航栏颜色
//    self.StatusBarStyle = UIStatusBarStyleLightContent;
    self.isShowLiftBack = NO;//每个根视图需要设置该属性为NO，否则会出现导航栏异常
    [self setupcollectionveiw];
    self.footModel = [NSMutableDictionary new];
    [self list];
    //设置
    self.keyboreType = @"1";
    
    WEAKSELF;
    //默认block方法：设置下拉刷新
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf list];
    }];
}
//有网络的时候
-(void)haveNetwork:(NSNotification *)noti{
    //有网络时候，隐藏界面提示，
    [super haveNetwork:noti];
    if (![HBHuTool judgeArrayIsEmpty:self.collectionarray]) {
        //没有数据的时候,刷新界面
        [self list];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [self setupNavBar];
//    self.StatusBarStyle = UIStatusBarStyleLightContent;
//    [self.navigationController.navigationBar setBackgroundImage:[self imageWithColor:MAINTextCOLOR] forBarMetrics:UIBarMetricsDefault];
//}
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//     self.StatusBarStyle = UIStatusBarStyleDefault;
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"white_Nav.png"] forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.titleTextAttributes =@{NSForegroundColorAttributeName:[UIColor blackColor]};
//}
-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f); //宽高 1.0只要有值就够了
    UIGraphicsBeginImageContext(rect.size); //在这个范围内开启一段上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);//在这段上下文中获取到颜色UIColor
    CGContextFillRect(context, rect);//用这个颜色填充这个上下文
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();//从这段上下文中获取Image属性,,,结束
    UIGraphicsEndImageContext();
    
    return image;
}
-(void)setupNavBar
{
    [self.navigationController.navigationBar setBarTintColor:MAINTextCOLOR];

    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-40, 30)];
    textfield.placeholder = @"寻找感兴趣的商品";
    textfield.delegate = self;
    textfield.returnKeyType = UIReturnKeySearch;
    textfield.backgroundColor = white_Color;
    textfield.font = [UIFont systemFontOfSize:14];
    textfield.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textfield setLeftSearchViewWithImageName:@"bbs_home_search"];
    [textfield setBorderColor:MAINTextCOLOR];
    self.navigationItem.titleView = textfield;
    
    UIButton *messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    messageBtn.bounds  = CGRectMake(0, 0, 25, 25);
//    [messageBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 6, 0, -6)];
    [messageBtn setBackgroundImage:ImageNamed(@"bbs_search") forState:UIControlStateNormal];
    //[messageBtn setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
    messageBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
    [messageBtn addTarget:self action:@selector(clickMessage:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:messageBtn];
    self.navigationItem.rightBarButtonItem = item;
    
//    [self.view addSubview:self.customNavBar];
//
//    //设置自定义导航栏颜色 红色
//    self.customNavBar.barBackgroundColor = Color_MainC;
//    //点击搜索按钮和右边按钮
//    WEAKSELF;
//    self.customNavBar.onClickSearchButton = ^(){
//
//    };
//    self.customNavBar.onClickRightButton =^(){
//        //点击消息按钮
//        if ([userManager judgeLoginState]) {
////            //登录成功
//        }
//        else{
//            //还没有登录
//        }
//    };
    
}
//点击消息
-(void)clickMessage:(UIButton *)buton{
    HBMessageViewController *vc = [[HBMessageViewController alloc]initWithNibName:@"HBMessageViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
//textfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textField-------%@",textField.text);
    //设置
    self.keyboreType = @"1";
    self.textFieldStr = textField.text;
    if (![HBHuTool isJudgeString:textField.text]) {
        GoodsViewController *vc = [[GoodsViewController alloc]initWithNibName:@"GoodsViewController" bundle:nil];
        vc.search_keywords = textField.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textField4 - 开始编辑");
    //设置
    self.keyboreType = @"2";
}// became first responder
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (![HBHuTool isJudgeString:textField.text]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (void)setupcollectionveiw
{
    self.collectionarray = [NSMutableArray array];
    
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.columnCount = 3;
    layout.headerHeight = 0;
    layout.footerHeight = 185;
    layout.minimumColumnSpacing = 8;
    layout.minimumInteritemSpacing = 7;
    layout.sectionInset = UIEdgeInsetsMake(0, 7, 0, 7);
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.contentInset = UIEdgeInsetsMake(16, 0, 0, 0);
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeItem" bundle:nil] forCellWithReuseIdentifier:@"item"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeFooter" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH/3, SCREEN_WIDTH/3 *1.5);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionarray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeItem *item = [collectionView dequeueReusableCellWithReuseIdentifier:@"item" forIndexPath:indexPath];
    [item.imageview sd_setImageWithURL:[NSURL URLWithString:self.collectionarray[indexPath.row][@"imagesrc"]]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
    return item;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.keyboreType isEqualToString:@"1"]) {
        //搜索有数据 搜索跳
//        HomeItem *item = (HomeItem*)[collectionView cellForItemAtIndexPath:indexPath];
        ShopViewController *vc= [[ShopViewController alloc] initWithNibName:@"ShopViewController" bundle:nil];
        NSDictionary *dic = self.collectionarray[indexPath.row][@"webparam"];
        if (ValidDict(dic)) {
            vc.shop_id = dic[@"shopid"];
        }
        vc.placeholderImage = [UIImage imageNamed:ZAN_WU_TUPIAN];
        [self.navigationController pushViewController:vc animated:YES];
        //    HBShopContentVC *vc = [[HBShopContentVC alloc]init];
        //    vc.shop_id = self.collectionarray[indexPath.row][@"linktarget"];
        //    [self.navigationController pushViewController:vc animated:YES];
    }else{
        
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == CHTCollectionElementKindSectionFooter) {
        HomeFooter *footer = [collectionView dequeueReusableSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:indexPath];
        [footer.imageview sd_setImageWithURL:[NSURL URLWithString:self.footModel[@"imagesrc"]]placeholderImage:[UIImage imageNamed:ZAN_WU_TUPIAN]];
        WEAKSELF;
        footer.clickFooterView = ^(id obj) {
            //点击入驻
            HBHomeEnterVC *vc = [[HBHomeEnterVC alloc]initWithNibName:@"HBHomeEnterVC" bundle:nil];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        };
       return footer;
    }else{
        return nil;
    }
}


@end
