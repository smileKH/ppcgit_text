//
//  HBHeaderFileName.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//
//全局需要的头文件
#ifndef HBHeaderFileName_h
#define HBHeaderFileName_h
//第三方
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <YYKit.h>
#import <MBProgressHUD.h>
#import <Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "PPNetworkHelper.h"
#import "MBProgressHUD+XY.h"
#import <SDAutoLayout.h>
#import <UIImage+GIF.h>
#import <WXApi.h>
#import "UIView+Frame.h"
#import <MJExtension.h>
#import <MJRefresh.h>
#import <AFNetworking.h>
#import "NSString+QJM.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "YiGouPaySDK.h"
#import "NSDictionary+LSCore.h"

//基础类
#import "AppDelegate+AppService.h"
#import "AppDelegate+PushService.h"
#import "AppDelegate+PaymentService.h"
#import "AppDelegate+ShareService.h"
#import "HBRootViewController.h"
#import "HBRootTableViewController.h"
#import "HBRootCollectionViewController.h"
#import "HBRootNavigationController.h"
#import "HBRootWebViewController.h"
#import "HBMainTabBarController.h"
#import "HBBaseWebViewController.h"
#import "HBAppManager.h"
#import "HBShareManager.h"
#import "HBUserManager.h"
#import "LBService.h"
#import "HBButton.h"
#import "HBHuTool.h"
#import "XH_Date_String.h"
#import "HBMessageButton.h"
#import "HBRootNavigationController.h"
#import "JXCategoryView.h"


//--------------------------HBBSStore---------------------//
#import "UIView+Border.h"
#import "StringAttribute.h"
#import "Is.h"
#import "LoginViewController.h"
#import "HBMyOrderDetailModel.h"
#import "HBOrderPayModel.h"
#import "PopoverAction.h"
#import "PopoverView.h"
#import "HBMyOrderModel.h"
#import "HBRootViewController.h"
#import "HBMessageViewController.h"
#import "HBMineViewController.h"
#import "ShopViewController.h"//店铺
#import "ProductFirstViewController.h"//商品详情
#import "HBMyOrderDetailVC.h"
#import "HBCashierDeskVC.h"
#import "ShoppingCarViewController.h"
#import "MyOrderViewController.h"

#import "UITextField+LeftView.h"
#import "UIImage+Extension.h"
#import "FMDB.h"
#import "Cart_add.h"
#import "HBCartOffterLineModel.h"

#endif /* HBHeaderFileName_h */
