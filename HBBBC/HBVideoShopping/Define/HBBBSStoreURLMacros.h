//
//  HBBBSStoreURLMacros.h
//  HBVideoShopping
//
//  Created by aplle on 2018/3/10.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#ifndef HBBBSStoreURLMacros_h
#define HBBBSStoreURLMacros_h

//以下是内网测试服务器配置
#define Config_baseUrl               @"http://www.shipinggouwu.com:8001/index.php/topapi"
#define Config_imageUrl              @"http://www.shipinggouwu.com:8001"
#define Config_APPLOGIN              @"http://www.shipinggouwu.com:8001/api/"
#define Config_UPDATETOKEN           @"http://www.shipinggouwu.com:8001/api/"
#define Config_INSTRUCTOR            @"http://www.shipinggouwu.com/api/"
#define Config_NOTITIFTIONS          @"http://www.shipinggouwu.com/api/"
#define Config_FORGETPASSWORD        @"http://www.shipinggouwu.com/api/"
#define BBStroe_Version  @"v1"
#define HttpToken                    @"68cb1afb3fb8f12d7a90e1b3c8b66d04ffe0c1aed79f4c479c0c5427a5f4fb58"

////以下是正式地址
//#define Config_baseUrl               @"http://www.shipinggouwu.com/index.php/topapi"
//#define Config_imageUrl              @"http://www.shipinggouwu.com"
//#define Config_APPLOGIN              @"http://www.shipinggouwu.com/api/"
//#define Config_UPDATETOKEN           @"http://www.shipinggouwu.com/api/"
//#define Config_INSTRUCTOR            @"http://www.shipinggouwu.com/api/"
//#define Config_NOTITIFTIONS          @"http://www.shipinggouwu.com/api/"
//#define Config_FORGETPASSWORD        @"http://www.shipinggouwu.com/api/"
//#define BBStroe_Version  @"v1"
//#define HttpToken                    @"68cb1afb3fb8f12d7a90e1b3c8b66d04ffe0c1aed79f4c479c0c5427a5f4fb58"


//---------------------------首页-------------------------//
//首页
#define HOME_THEME_MODULES                        @"theme.modules"
//请求地区数据
#define ADDRESS_REGION_JSON                      @"region.json"

//---------------------------物流信息-------------------------//
//获取快递公司列表
#define LOGISTICS_LIST                       @"logistics.list.get"
//售后退换货，用户回寄商品给商家
#define LOGISTICS_SEND                        @"logistics.send"
//显示物流信息
#define LOGISTICS_GET                        @"logistics.get"

//---------------------------分类-------------------------//
//分类
#define CATEGORY_ITEM_CATEGORY                   @"category.itemCategory"

//---------------------------用户登录-------------------------//
//发送短信验证码
#define USER_SEND_SMS                            @"user.sendSms"
//获取图片验证码
#define USER_VC_CODE                             @"user.vcode"
//验证图片验证码
#define USER_VERIFY_ACCOUNT                      @"user.verifyAccount"
//发送验证码
#define USER_VERIFY_SMS                          @"user.verifySms"
//注册
#define USER_SIGN_UP                             @"user.signup"
//登录
#define USER_LOGIN_BBS                           @"user.login"
//忘记密码
#define USER_FORGOT_PASSWORD                     @"user.forgot.resetpassword"
//退出登录
#define USER_LOGOUT_BBS                          @"user.logout"
//注册协议
#define USER_LICENSE                             @"user.license"
//使用协议
#define USER_PROTOCOL                             @"user.protocol"

//---------------------------第三方登录-------------------------//
#define USER_TRUST_BIND                            @"user.trust.bindUser"

//---------------------------商品列表-------------------------//
//商品列表
#define GOODS_IETM_SEARCH                        @"item.search"
//商品详情
#define GOODS_ITEM_DETAIL                        @"item.detail"
//商品描述
#define GOODS_DESC_ITEM                          @"item.desc"
//获取商品评论列表
#define GOODS_RATE_LIST                          @"item.rate.list"
//获取商品购买详细信息
#define GOODS_SKU_DETAIL                         @"item.sku.detail"
//获取筛选信息
#define GOODS_FILTER_ITEMS                        @"item.filterItems"

//---------------------------店铺-------------------------//
//获取店铺信息
#define GOODS_SHOP_INDEX                         @"shop.index"
//获取店铺基本信息
#define GOODS_SHOP_BASIC                         @"shop.basic"


//---------------------------购物车-------------------------//
//获取购物车信息
#define CART_GET_LIST                        @"cart.get"
//获取购物车基础信息
#define CART_BASIC_INFO                       @"cart.get.basic"
//添加购物车或者立即购买
#define GOODS_ADD_CART                        @"cart.add"
//删除购物车数据
#define CART_GOODS_DEL                        @"cart.del"
//更新购物车
#define CART_GOODS_UPDATE                     @"cart.update"
//购物车结算页
#define CART_CHECK_OUT                        @"cart.checkout"
//统计购物车商品数量
#define CART_NUMBER_COUNT                     @"cart.count"
//购物车结算页计算金额
#define CART_TOTAL_MONEY                      @"cart.total"
//购物车结算页出来会员积分
#define CART_USER_POINT                       @"cart.user.point"
//---------------------------优惠券 促销-------------------------//
//获取平台活动列表
#define PRO_ACTIVITY_LIST                  @"promotion.activity.list"
//获取平台活动详情及其商品列表
#define PRO_ACTIVITY_DETAIL                @"promotion.activity.detail"
//获取平台活动商品详情
#define PRO_ACTIVITY_ITEMDETAIL            @"promotion.activity.itemdetail"
//活动订阅信息
#define PRO_ACTIVITY_REMINDINFO            @"promotion.activity.remindinfo"
//订阅活动提醒
#define PRO_ACTIVITY_REMINDSUBMIT          @"promotion.activity.remindsubmit"
//获取商家促销详情
#define PRO_SHOP_CART_DETAIL               @"promotion.shop.cartpromotion.detail"
//获取商家优惠券商品列表
#define PRO_SHOP_COUPON_DETAIL             @"promotion.shop.coupon.detail"
//用户使用优惠券
#define PRO_SHOP_COUPON_USE                @"promotion.coupon.use"
//订单结算取消使用优惠券
#define PRO_SHOP_CANCEL                    @"promotion.coupon.cancel"
// 用户领取优惠券
#define PRO_SHOP_CODE_GET                  @"promotion.coupon.code.get"
//用户可领取商家优惠券列表
#define PRO_SHOP_LIST_GET                  @"promotion.coupon.list.get"
//获取会员的红包列表
#define PRO_SHOP_HONGBAO_LIST              @"promotion.hongbao.list"
//获取促销页面信息
#define PRO_SHOP_PAGE_INFO                 @"promotion.page.info"

//---------------------------个人中心-------------------------//

//获取个人信息
#define MEMBER_BASICS_GET                     @"member.basics.get"
//更新个人信息
#define MEMBER_BASICS_UPDATE                  @"member.basics.update"
//获取个人中心信息  小红点
#define MEMBER_INDEX_NUMBER                    @"member.index"
//设置用户名
#define MEMBER_SET_ACCOUNT                    @"member.setAccount"

//更新会员头像
#define USER_UPDATE_PORTRAIT                  @"member.basics.updatePortrait"


//---------------------------优惠券 积分 存款充值 明细-------------------------//
//会员预存款明细
#define MEMBER_DEPOSIT_DETAIL                    @"member.deposit.detail"
//预存款充值
#define MEMBER_DEPOSIT_RECHARGE                   @"member.deposit.recharge"
//我的优惠券列表
#define CENTER_COUPON_LIST                        @"member.coupon.list"
//会员积分明细
#define MENBER_POINT_DETAIL                        @"member.point.detail"

//-------------------------------------收货地址------------------------//
//新增收货地址
#define ADDRESS_CREATE_MEMBER                  @"member.address.create"
//新增收货地址
#define ADDRESS_LIST_MEMBER                    @"member.address.list"
//删除地址
#define ADDRESS_MENBER_DELETE                 @"member.address.delete"
//设置默认地址
#define ADDRESS_SET_DEFAULTT                   @"member.address.setDefault"
//获取单个收货地址详情
#define ADDRESS_GET_MEMBER                     @"member.address.get"
//更新收货地址
#define ADDRESS_UPDATE_MEMBER                   @"member.address.update"

//-------------------------------------申请退货------------------------//
//会员退换货记录列表
#define AFTERSALES_MEMBER_LIST                  @"member.aftersales.list"
//会员退换货记录详情
#define AFTERSALES_MEMBER_GET                    @"member.aftersales.get"

//售后申请数据获取
#define AFTERSALES_MEMBER_APPLY_INFO             @"member.aftersales.applyInfo.get"
//会员申请退换货
#define AFTERSALES_MEMBER_APPLY                   @"member.aftersales.apply"

//---------------------------评价与售后-------------------------//
//会员中心我的评价列表
#define MEMBER_RATE_LIST                           @"member.rate.list"
//对已完成的订单新增商品评论和店铺评分
#define MEMBER_RATE_ADD                            @"member.rate.add"
//会员评价订单信息
#define MEMBER_RATE_GET                             @"member.ratetrade.get"
//对商品评论进行追评
#define MEMBER_RATE_APEND                           @"member.rate.append"
//投诉商家
#define COMPLAINTS_CARATE                           @"member.complaints.create"
//撤销投诉
#define COMPLAINTS_CLOSE                            @"member.complaints.close"
//会员中心我的投诉列表
#define COMPLAINTS_LIST                             @"member.complaints.list"
//会员中心我的投诉详情
#define COMPLAINTS_GET                              @"member.complaints.get"
//会员中心我的投诉类型
#define COMMON_TYPE_LIST                             @"common.complaintsType"

//---------------------------安全中心-------------------------//
//验证登录密码
#define SECURITY_CHECK_LOGINPS                     @"member.security.checkLoginPassword"
//修改登录密码
#define SECURITY_UPDATE_LOGINPS                    @"member.security.updateLoginPassword"
//设置支付密码
#define SECURITY_SETPAY_PASSWORD                   @"member.security.setPayPassword"
//忘记预存款密码的短信验证
#define SECURITY_SETPAY_MOBLIE                     @"member.security.setPayPasswordByMoblie"
//验证支付密码
#define SECURITY_CHECK_PAY_PS                      @"member.security.checkPayPassword"
//修改支付密码
#define SECURITY_UPDATE_PAYPS                      @"member.security.updatePayPassword"
//会员手机号绑定/修改,发送短信验证码
#define SECURITY_UPDATE_MOBILE                     @"member.security.updateMobile"
//会员手机号保存
#define SECURITY_SAVE_MOBILE                       @"member.security.saveMobile"
//安全中心相关所有配置
#define SECURITY_USER_CONF                         @"member.security.userConf"




//-------------------------------------我的订单  交易------------------------//
//订单列表
#define MY_TRADE_LIST                             @"trade.list"
//订单详情
#define MY_ORDER_TRADE                            @"trade.get"
//子订单详情
#define MY_ORDER_ZI_TRADE                         @"trade.order.get"
//取消订单
#define MY_ORDER_CANCEL_CREATE                    @"trade.cancel.create"
//取消订单列表
#define MY_ORDER_CANCEL_LIST                      @"trade.cancel.list"
//确认收货
#define MY_ORDER_CONFIRM                          @"trade.confirm"
//获取会员取消订单详情
#define MY_ORDER_CANCEL_GET                        @"trade.cancel.get"
//获取平台配置的取消订单原因
#define MY_ORDER_REASON_GET                        @"trade.cancel.reason.get"
//创建订单
#define MY_ORDER_CREATE                            @"trade.create"

//-------------------------------------我的收藏------------------------//
//获取用户的所有收藏
#define GOODS_FAVORITE_ALL                        @"member.favorite.all"
//我的商品收藏列表
#define GOODS_FAVORITE_LIST_ITEM                   @"member.favorite.item.list"
//移除收藏商品
#define GOODS_FAVORITE_ITEM_REMOVE                  @"member.favorite.item.remove"
//收藏商品
#define GOODS_FAVORITE_ADD                         @"member.favorite.item.add"
//我的店铺收藏列表
#define GOODS_FAVORITE_LIST_SHOP                   @"member.favorite.shop.list"
//移除收藏店铺
#define GOODS_FAVORITE_SHOP_REMOVE                    @"member.favorite.shop.remove"
//收藏店铺
#define GOODS_FAVORITE_SHOP_ADD                    @"member.favorite.shop.add"
//获取会员收藏详情
#define MEMBER_SHOP_ISCOLLECT                    @"member.favorite.shop.isCollect"


//---------------------------支付-------------------------//
//获取支付方式列表
#define PAYMENT_PAY_PAYCENTER                       @"payment.pay.paycenter"
//获取支付单
#define PAYMENT_PAY_GETPAYMENT                      @"payment.pay.getPayment"
//创建支付单
#define PAYMENT_PAY_CREATE                          @"payment.pay.create"
//去支付
#define PAYMENT_PAY_DODO                             @"payment.pay.do"
//去支付
#define PAYMENT_PAY_RECHARGE                         @"payment.recharge.do"
//模拟支付
#define PAYMENT_PAYMENT_ORDER_PAY                    @"payment.orderPay"

//---------------------------内容管理-------------------------//
//获取文章详情
#define CONTENT_INFO                                 @"content.info"
//获取文章列表
#define CONTENT_LIST                                 @"content.list"
//获取目录列表
#define CONTENT_NODE_LIST                            @"content.node.list"
//获取商家文章详情
#define CONTENT_SHOP_INFO                            @"content.shop.info"

//---------------------------附近商家-------------------------//
//1、    在店铺首页显示店铺地址和电话，并且可以点击电话拨打电话，点击地址进入地图，并且可以进行导航到该地址
#define RECENTLY_SHOP_BASIC                    @"shop.basic"
//2、    APP首页加一个附近商家导航栏，点击附近商家弹出地图，可在地图展示附近的商家，并且显示店铺列表，可进行导航
#define RECENTLY_SHOP_POSITION                    @"shop.position"
//3、    根据店铺类型，判断店铺首页展示方式，商品类和人员类展示方式区别
#define RECENTLY_ITEM_SEARCH                   @"item.search"
//4、    详情页人员信息展示，去掉金额、加入购物车、立即购买等功能
#define RECENTLY_ITEM_DETAIL                   @"item.detail"
//5、    商品主图可支持上传视频
#define RECENTLY_ITEM_DETALI_ALL                 @"item.detail"

//---------------------------上传图片-------------------------//
//上传图片
#define IMAGE_UPLOAD_IMAGE                            @"image.upload"
//平台客服工具
#define COMMON_PLATFORM_CST                            @"common.platformCst"

//-----------------------需求中心----------------------------------
//需求列表
#define DEMAND_INDEX_TOPAPI                           @"member.demand.list"
//需求详情
#define DEMAND_QUERY_DETAIL                           @"member.demand.query"
//上传视频
#define DEMAND_VIDEO_COMMIT                           @"member.demand.video"
//上传图片
#define DEMAND_IMAGE_COMMIT                           @"member.demand.image"
//获取评论列表
#define DEMAND_DEMAND_COMMENT_LIST                    @"member.demand.comment.list"
//发布评论
#define DEMAND_DEMAND_COMMENT_CREATE                  @"member.demand.comment.create"
//删除评论
#define DEMAND_DEMAND_COMMENT_DELETE                  @"member.demand.comment.delete"
//获取需求详情
#define DEMAND_DEMAND_QUERY                          @"member.demand.query"
//发布需求
#define DEMAND_DEMAND_CREATE                         @"member.demand.create"
//删除需求
#define DEMAND_DEMAND_DELETE                         @"member.demand.delete"
//更新需求
#define DEMAND_DEMAND_UPDATE                         @"member.demand.update"

//获取投诉列表
#define DEMAND_DEMAND_COMPLAINT_LIST                    @"member.demand.complaint.list"
//获取投诉列表
#define DEMAND_DEMAND_COMPLAINT_CREATE                    @"member.demand.complaint.create"
//删除投诉
#define DEMAND_DEMAND_COMPLAINT_DELETE                    @"member.demand.complaint.delete"

//会员获取需求列表
#define DEMAND_INDEX_LIST_FAUTH                           @"member.demand.listfauth"
//会员获取收藏列表
#define DEMAND_INDEX_FAVORITE                           @"member.demand.favorite"

#endif /* HBBBSStoreURLMacros_h */

