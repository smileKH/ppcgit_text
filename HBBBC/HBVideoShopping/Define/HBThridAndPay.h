//
//  HBThridAndPay.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//
//第三方资料和支付资料
#ifndef HBThridAndPay_h
#define HBThridAndPay_h
//微信----------------------------------------------------------
#import <WXApi.h>
#define WXAppID  @"wx20b87b30f22cd133"
#define AppSecret @"89a9320f31bd410105679b6bd4c32088"
//微信授权------------------------------------------------------
#define WX_OPENID  @"WX_OPENID"
#define WX_UNIONID @"WX_UNIONID"
#define WX_ACCESS_TOKEN  @"WX_ACCESS_TOKEN"
#define WX_AUTH_TOKEN_ALLDATA  @"WX_AUTH_TOKEN_ALLDATA"

//支付宝----------------------------------------------------------
#import <AlipaySDK/AlipaySDK.h>     // 导入AlipaySDK
// 导入支付类
#import "Order.h"                   // 导入订单类
/**
 *  appSckeme:应用注册scheme,在Info.plist定义URLtypes，处理支付宝回调
 */
#define kAppScheme @"HBVideoShopping"

//友盟分享
#import <UMSocialCore/UMSocialCore.h>
#import <UShareUI/UShareUI.h>

#define UMAppKey @"59daf0180734be4411f00002d"
//微信
#define UMWechatAppKey @"wxcae30ff7b5b2fba95"
#define UMWechat_AppSecret @"52d200886f53ce135db83c7816d66866a"
//QQ
#define UMQQAppID @"11064940460"
#define UMQQAppKey @"2i6wdsdtjD0n9C6gq"
//微博
#define UMSinaID @"39217000954"
#define UMSinaAppKey @"04b48b0940faeb16683c32669824ebdad"
//支付宝
#define UMAlipayID @"20170902608941825"
#define UMAlipayAppKey @""

#endif /* HBThridAndPay_h */
