//
//  HBNotificationMacros.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//
//通知资料和提示文字
#ifndef HBNotificationMacros_h
#define HBNotificationMacros_h
#pragma mark - ——————— 用户相关 ————————
//登录状态改变通知
#define KNotificationLoginStateChange @"loginStateChange"

//自动登录成功
#define KNotificationAutoLoginSuccess @"KNotificationAutoLoginSuccess"

//被踢下线
#define KNotificationOnKick @"KNotificationOnKick"

//用户信息缓存 名称
#define KUserCacheName @"KUserCacheName"

//用户model缓存
#define KUserModelCache @"KUserModelCache"
//用户model缓存
#define kLoginModelName @"kLoginModel"

//微信支付通知
#define WEAL_WEIXIN_PAYNotice  @"WEAL_WEIXIN_PAY"
//支付宝支付通知
#define WEAL_PAY_PAYNOTICE     @"WEAL_PAY_PAYNOTICE"
//农行支付通知
#define ABC_APP_CALLER         @"ABC_APP_CALLER"

#pragma mark - ——————— 网络状态相关 ————————

//网络状态变化
#define KNotificationNetWorkStateChange @"KNotificationNetWorkStateChange"

//登录成功或者登录失败
#define     NotificationLogoutSuccess               @"NotificationLogoutSuccess"
#define     NotificationLoginSuccess                @"NotificationLoginSuccess"

//接收登录返回的通知
#define     NotificationHBLoginVC                @"NotificationHBLoginVC"
//刷新网页HBWebManage
#define     NotificationUpdateHBWebManage               @"NotificationUpdateHBWebManage"
//有网
#define NotificationHaveNet       @"NotificationHaveNet"
//断网
#define NotificationBrokenNetwork @"NotificationBrokenNetwork"
//网络标记
#define NetworkThereAnyTag        @"NetworkThereAnyTag"

#define     Text_NoContentPrompt            @"没有找到对应的数据！"
#define     Text_NoNetworkPrompt            @"当前网络不可用，请检查你的网络设置!"
#define     RequestServerFaild              @"网络加载出错，请稍候再试!"
#define     RequestFalsePrompt              @"抱歉，服务异常，请稍候再试"
#define     NotData_Ptrompt                 @"暂无数据"
//没有更多数据
#define     Text_NoMoreData                 @"亲,没有更多数据了哦!!!"
//加载中
#define     Text_JIA_ZAI_ZHONG                 @"加载中..."


#pragma mark - 账号系统
#define     PASSWORD                        @"PASSWORD"
#define     ACCOUNT                         @"ACCOUNT"
#define     AUTOLOGIN                       @"AUTOLOGIN"

#pragma mark ==========设置头像改变==========
#define     CHANGE_HEADER_IMAGE                @"HERADER_IMAGE_URL"
#define     HEADER_IMAGE_NOTIFICATIO            @"changeHeaderImage"

#define  TEST_ACCOUNT_WOZONG                @"18620135588"
#define  SAVE_STORE_DATA                    @"SAVE_STORE_DATA_KET"

//token
#define     TOKEN_XY_APP                    @"accessToken_APP"
#define     OPEN_ID_XY_APP                  @"openid"
#define     CREATE_DATE_XY_APP              @"createDate"
//押金
#define     DEPOSIT_XY_APP                  @"deposit"
//绑定
#define     AUTH_RESULT_BINDED              @"authResult_binded"

//订单状态
//去支付
#define     STATUTS_PAY                    @"去付款"//
//取消订单
#define     STATUTS_CANCEL                 @"取消订单"//
//确认收货
#define     STATUTS_CONFIRM                @"确认收货"//
//查看物流
#define     STATUTS_LOGISTICS              @"查看物流"//
//去评价
#define     STATUTS_EXALUATION             @"去评价"//
//已评价
#define     STATUTS_THE_EXALUATION          @"已评价"
//查看详情
#define     STATUTS_CHA_KAN_XIANGQ          @"查看详情"


//未确认
#define     STATUTS_UNCONFIRMED            @"未确认"
//已确认
#define     STATUTS_BEEN_CONFIRMED         @"已确认"
//已完成
#define     STATUTS_BEEN_COMPLETED         @"已完成"
//已取消
#define     STATUTS_BEEN_CANCELLED         @"已取消"
//已删除
#define     STATUTS_DELETED                @"已删除"

//审核中
#define     USER_STATUTS_ZHON               @"审核中"
//审核通过
#define     USER_STATUTS_TONGGUO             @"审核通过"
//审核拒绝
#define     USER_STATUTS_JUEJUE              @"审核拒绝"
//未交押金
#define     USER_STATUTS_YEJIN               @"未交押金"
//店铺资料
#define     USER_STATUTS_ZILIAO               @"店铺资料"
//
//

//---------------------------------status---------------------------//
//待付款
#define     WAIT_BUYER_PAY               @"WAIT_BUYER_PAY"
//已付款等待发货
#define     WAIT_SELLER_SEND_GOODS       @"WAIT_SELLER_SEND_GOODS"
//已发货等待确认收货
#define     WAIT_BUYER_CONFIRM_GOODS     @"WAIT_BUYER_CONFIRM_GOODS"
//已完成
#define     TRADE_FINISHED               @"TRADE_FINISHED"
//已关闭(退款关闭订单)
#define     TRADE_CLOSED                 @"TRADE_CLOSED"
//已关闭(卖家或买家主动关闭);)
#define     TRADE_CLOSED_BY_SYSTEM       @"TRADE_CLOSED_BY_SYSTEM"

//待评价请求数据
#define     WAIT_RATE                     @"WAIT_RATE"

//---------------------------------cancel_status---------------------------//
//未申请
#define     CANCEL_NO_APPLY_CANCEL     @"NO_APPLY_CANCEL"
//等待审核
#define     CANCEL_WAIT_PROCESS       @"WAIT_PROCESS"
//退款处理
#define     CANCEL_REFUND_PROCESS     @"REFUND_PROCESS"
//取消成功
#define     CANCEL_SUCCESS              @"SUCCESS"
//取消失败
#define     CANCEL_FAILS               @"FAILS"

//---------------------------------aftersales_status---------------------------//
//买家已经申请退款，等待卖家同意;
#define     WAIT_SELLER_AGREE           @"WAIT_SELLER_AGREE"
//卖家已经同意退款，等待买家退货;
#define     WAIT_BUYER_RETURN_GOODS       @"WAIT_BUYER_RETURN_GOODS"
//买家已经退货，等待卖家确认收货;
#define     WAIT_SELLER_CONFIRM_GOODS     @"WAIT_SELLER_CONFIRM_GOODS"
//退款成功;
#define     AFTERSALES_SUCCESS              @"SUCCESS"
//退款关闭;
#define     AFTERSALES_CLOSED               @"CLOSED"
//退款中;
#define     AFTERSALES_REFUNDING               @"REFUNDING"
//卖家拒绝退款;
#define     SELLER_REFUSE_BUYER               @"SELLER_REFUSE_BUYER"
//卖家已发货;)
#define     SELLER_SEND_GOODS               @"SELLER_SEND_GOODS"
//[payment](实付金额,订单最终总额)
//申请退换货
#define     AF_TUI_HUAN_HUO               @"申请退换货"
//售后处理中
#define     AF_SHOU_CHU_LI               @"售后处理中"
//已同意,请退货
#define     AF_QING_TUI_HUO               @"已同意,请退货"
//待卖家确认收货
#define     AF_QUE_REN_SHOU_HUO               @"待卖家确认收货"
//退款中
#define     AF_TUI_KUAN_ZHON               @"退款中"
//退款完成
#define     AF_TUI_KUAN_WAN_CHENG               @"退款完成"
//售后驳回
#define     AF_SHOU_HOU_BO_HUI               @"售后驳回"
//投诉商家
#define     AF_TOU_SU_SHANG_JIA               @"投诉商家"
//投诉处理中
#define     AF_TOU_SU_CHU_LI_ZHONG              @"投诉处理中"

//再次申请退换货
#define     AF_ZAI_CI_SHEN_QING              @"再次申请退换货"
//投诉已撤销
#define     AF_TOU_SU_CEH_XIAO             @"投诉已撤销"
//投诉已关闭
#define     AF_TOU_SU_GUAN_BI              @"投诉已关闭"

//---------------------------------complaints_status---------------------------//
//买家未进行投诉;
#define     COMPLAINTS_NOT_COMPLAINTS      @"NOT_COMPLAINTS"
//买家投诉，等待平台处理;
#define     COMPLAINTS_WAIT_SYS_AGREE      @"WAIT_SYS_AGREE"
//处理完成;
#define     COMPLAINTS_FINISHED            @"FINISHED"
//买家撤销投诉;
#define     COMPLAINTS_BUYER_CLOSED        @"BUYER_CLOSED"
//平台关闭投诉，不需要处理直接关闭;)
#define     COMPLAINTS_CLOSED              @"CLOSED"


//每个选择按钮的高度
#define     BUTTTON_HEIGHT                 40
//首页八个item的高度
#define     HOME_ITEM_HEIGHT               200
//首页四个item的高度
#define     HOME_ITEM_FORE_HEIGHT          110
//每个选择按钮的高度
#define     BUTTTON_HEIGHT                 40
//首页两边间距
#define     HOME_SPACING_LEFT                13

//1、横向滚动
#define     HOME_HORIZONTAL_VIEW          SCREEN_WIDTH/3+40
//3、左边大图、右边大图
#define     HOME_LARGER_VIEW              SCREEN_WIDTH*0.75
//2、上面大图
#define     HOME_TWO_ITEM_HEIGHT          SCREEN_WIDTH/3+30
//4、双列表
#define     HOME_DOUBLE_SERVICE           SCREEN_WIDTH/2+50
//5、右边大图
#define     HOME_RIGHT_LARGER             SCREEN_WIDTH*0.75
//6、大图列表
#define     HOME_LARGER_TABLE             SCREEN_WIDTH*0.45
//7、小图列表
#define     HOME_THUMBNAIL_TABLE          120
//首页集合视图的headerview的高度
#define HOME_COLLECTION_HEADER_HEIGHT     60

//----------------首页自定义导航栏高度--------------
#define     NAVIG_MARGIN                10
#define     NAVIG_BUTTON_HEIGHT         30
#define     NAVIG_BUTTON_WIDTH          30
#define     NAVIG_TITLE_HEIGHT          44
#define     NAVIG_TITLE_WIDTH         SCREEN_WIDTH-2*NAVIG_BUTTON_WIDTH-4*NAVIG_MARGIN


//滚动视图的高度 商品分类的
#define IMAGE_HEIGHT 252.5


//------------------------------图片相关---------------------------
#define ZAN_WU_TUPIAN        @"BB_Public_image_placehoder"
#define GOODS_DEFINE_IMG     [UIImage imageNamed:@"bbs_header"]


//------------------------------商品列表筛选宽度---------------------------

#define GOODS_SCEEREN_WIDTH        SCREEN_WIDTH-120
#endif /* HBNotificationMacros_h */
