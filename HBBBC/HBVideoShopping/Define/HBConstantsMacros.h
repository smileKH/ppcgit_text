//
//  HBConstantsMacros.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//
//工具宏 屏幕大小 和自定义宏
#ifndef HBConstantsMacros_h
#define HBConstantsMacros_h
//重写NSLog,Debug模式下打印日志,方法名字,方法,当前行数
#if DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"---[方法名:%s]\n---[行号:%d]\n---打印内容:\n%s\n",__FUNCTION__, __LINE__,[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif

//等于
#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
//大于
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
//大于等于
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
//小于
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
//小于等于
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//strong weak  self
#define WEAKSELF typeof(self) __weak weakSelf = self

#define STRONGSELF typeof(weakSelf) __strong strongSelf = weakSelf
//获取系统对象
#define bApplication        [UIApplication sharedApplication]
#define bAppWindow          [UIApplication sharedApplication].delegate.window
#define bAppDelegate        [AppDelegate shareAppDelegate]
#define bRootViewController [UIApplication sharedApplication].delegate.window.rootViewController
#define bUserDefaults       [NSUserDefaults standardUserDefaults]
#define bNotificationCenter [NSNotificationCenter defaultCenter]

//系统高度，宽度
#define SCREEN_WIDTH            ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT           ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_BOUNDS [UIScreen mainScreen].bounds
//系统状态栏高度
#define bStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
//系统导航栏高度
#define bNavBarHeight self.navigationController.navigationBar.frame.size.height
//系统底部TabBar高度
#define bTabBarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height>20?83:49)
//系统导航栏总高度
#define bNavAllHeight (kDevice_Is_iPhoneXScreen ? 88 : 64)
//对于view里面来说
#define bOtherNavHeight (bStatusBarHeight + 44)
//是否刘海屏
#define kDevice_Is_iPhoneXScreen  (SCREEN_HEIGHT == 812.0f || SCREEN_HEIGHT == 896.0f)

//判断是否为iPhone X
#define IPHONE_X \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})

// 底部安全区域远离高度 现在使用
#define kBottomSafeHeight   (CGFloat)(IPHONE_X?(34):(0))

//拼接字符串
#define NSStringFormat(format,...) [NSString stringWithFormat:format,##__VA_ARGS__]
//数据验证
#define StrValid(f) (f!=nil && [f isKindOfClass:[NSString class]] && ![f isEqualToString:@""])
#define SafeStr(f) (StrValid(f) ? f:@"")
#define HasString(str,key) ([str rangeOfString:key].location!=NSNotFound)

#define ValidStr(f) StrValid(f)
#define ValidDict(f) (f!=nil && [f isKindOfClass:[NSDictionary class]])
#define ValidArray(f) (f!=nil && [f isKindOfClass:[NSArray class]] && [f count]>0)
#define ValidNum(f) (f!=nil && [f isKindOfClass:[NSNumber class]])
#define ValidClass(f,cls) (f!=nil && [f isKindOfClass:[cls class]])
#define ValidData(f) (f!=nil && [f isKindOfClass:[NSData class]])

//发送通知
#define bPostNotification(name,obj) [[NSNotificationCenter defaultCenter] postNotificationName:name object:obj];


//单例化一个类
#define SINGLETON_FOR_HEADER(className) \
\
+ (className *)shared##className;

#define SINGLETON_FOR_CLASS(className) \
\
+ (className *)shared##className { \
static className *shared##className = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##className = [[self alloc] init]; \
}); \
return shared##className; \
}

//----------------------GCD线程----------------------------
//-------------------NSUserDefaults-----------------------
//NSUserDefaults 实例化 取值
#define USERDEFAULT [NSUserDefaults standardUserDefaults]
#define USERDEFAULT_value(key) [[NSUserDefaults standardUserDefaults] valueForKey:key]

#define USERDEFAULT_object(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]
#define USERDEFAULT_BOOL(key) [[NSUserDefaults standardUserDefaults] boolForKey:key]
#define USERDEFAULT_integer(key) [[NSUserDefaults standardUserDefaults] integerForKey:key]

#define USERDEFAULT_int(key) [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue]

#define USERDEFAULT_float(key) [[NSUserDefaults standardUserDefaults] floatForKey:key]
//NSUserDefaults 实例化 存值
// object
#define USERDEFAULT_SET_value(_value_,_key_) [[NSUserDefaults standardUserDefaults] setValue:_value_ forKey:_key_];\
[[NSUserDefaults standardUserDefaults] synchronize]

#define USERDEFAULT_SET_object(_object_,_key_) [[NSUserDefaults standardUserDefaults] setObject:_object_ forKey:_key_];\
[[NSUserDefaults standardUserDefaults] synchronize]
// int
#define USERDEFAULT_SET_int(_int_,_key_) NSString *uIntString=[NSString stringWithFormat:@"%d",_int_];\
[[NSUserDefaults standardUserDefaults] setObject:uIntString forKey:_key_];\
[[NSUserDefaults standardUserDefaults] synchronize]
//float
#define USERDEFAULT_SET_float(_float_,_key_) NSString *uFloatString=[NSString stringWithFormat:@"%f",_float_];\
[[NSUserDefaults standardUserDefaults] setObject:uFloatString forKey:_key_];\
[[NSUserDefaults standardUserDefaults] synchronize]

#define USERDEFAULT_SET_bool(_bool_,_key_)   [[NSUserDefaults standardUserDefaults]setBool:_bool_ forKey:_key_];\
[[NSUserDefaults standardUserDefaults] synchronize];


//-------------------NSUserDefaults-----------------------

/** 边框*/
#define ViewBorder(View,Width,Color)\
\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

#define KHistorySearchPath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"PYSearchhistories.plist"]

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
#define EM_MULTILINE_TEXTSIZE(text, font, maxSize, mode) [text length] > 0 ? [text \
boundingRectWithSize:maxSize options:(NSStringDrawingUsesLineFragmentOrigin) \
attributes:@{NSFontAttributeName:font} context:nil].size : CGSizeZero;
#else
#define EM_MULTILINE_TEXTSIZE(text, font, maxSize, mode) [text length] > 0 ? [text \
sizeWithFont:font constrainedToSize:maxSize lineBreakMode:mode] : CGSizeZero;
#endif


//--------------------------BBBStore----------------------//
#define UISQURE  if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {[self setEdgesForExtendedLayout:UIRectEdgeNone];} ;self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

//定义UIImage对象
#define ImageNamed(_pointer) [UIImage imageNamed:_pointer]

//float-->string   integer-->string
#define floatString(floatValue) [NSString stringWithFormat:@"%.2f",floatValue]
#define integerString(integerValue) [NSString stringWithFormat:@"%.ld",(long)integerValue]
#define boolString(boolVaule) [NSString stringWithFormat:@"%.ld",(long)boolVaule]
#define LocalValue(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]

//APP版本号
#define kAppVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#endif /* HBConstantsMacros_h */
