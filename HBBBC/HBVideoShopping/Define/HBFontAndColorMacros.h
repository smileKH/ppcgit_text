//
//  HBFontAndColorMacros.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//
//字体大小和颜色
#ifndef HBFontAndColorMacros_h
#define HBFontAndColorMacros_h
//默认间距
#define KNormalSpace 12.0f

#pragma mark -  颜色区
//建议使用前两种宏定义,性能高于后者
//----------------------图片----------------------------
//----------------------颜色类---------------------------
// rgb颜色转换（16进制->10进制）
#define HRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)
#define COLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
//主题色 导航栏颜色
//#define CNavBgColor  [UIColor colorWithHexString:@"F2F2F2"]
#define CNavBgColor  [UIColor colorWithHexString:@"ffffff"]
#define CNavBgFontColor  [UIColor colorWithHexString:@"2E2E2E"]

//每一个界面默认的背景颜色
#define     Color_BG                      RGBA(240, 240, 240, 1)
//本智生活红色
#define     Color_MainC                   [UIColor colorWithHexString:@"#c24631"]

//默认页面背景色
#define CViewBgColor [UIColor colorWithHexString:@"f2f2f2"]

//分割线颜色
#define CLineColor [UIColor colorWithHexString:@"ededed"]

//次级字色
#define CFontColor1 [UIColor colorWithHexString:@"1f1f1f"]

//再次级字色
#define CFontColor2 [UIColor colorWithHexString:@"5c5c5c"]

//图片颜色
#define image_Color(_imageName_)  [UIColor colorWithPatternImage:  [UIImage imageNamed:(_imageName_)]]
//清除背景色
#define clear_Color [UIColor clearColor]
//红色背景色
#define red_Color   [UIColor redColor]
//绿色背景色
#define green_Color [UIColor greenColor]
//黑色背景色
#define black_Color [UIColor blackColor]
//白色背景色
#define white_Color [UIColor whiteColor]
//灰色背景色
#define gray_Color  [UIColor grayColor]

//---------------------------BBStroe----------------------//
/*
 以下可以定义自定义的颜色
 */
#define UIColorFromHex(s) [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
#define MAINBGCOLOR             UIColorFromHex(0xf7f7f7)//统一按钮颜色 
#define MAINCOLOR               UIColorFromHex(0x4D5968)//字体颜色 灰色
#define MAINTextCOLOR           UIColorFromHex(0x2BA4FF)//字体颜色 蓝色
#define MAINLIGHTTEXTCOLOR      UIColorFromHex(0x999999)
#define MAINREDCOLOR            UIColorFromHex(0xFF3363)//字体颜色 红色
#define MAINBLACKCOLOR          UIColorFromHex(0x434343)//按钮背景颜色 黑色
#define OTHERBLACKCOLOR         UIColorFromHex(0x4D5968)



#pragma mark -  字体区
//账户安全里面的itemview高度设置
#define     ACCOUNT_SET_VIEW_HEIGHT        110
//店铺详情里面的高度
#define     SHOP_DETAIL_VIEW_HEIGHT        64+47
//用于长段描述文字、文章正文、副标题、类目名称、灰色按钮文字等
#define     Color_textColor               HRGB(0x313131)
//标准字36px18sp/18pt导航标题、文章标题、重要突出词句
#define     textFont_Title18                   Font18
//标准字32px16sp/16pt用户姓名、列表文章标题、分类栏目、模块名称、按钮文字等
#define     textFont_TitleVice16               Font16
//标准字28px14sp/14pt长段描述文字、非标题文字、文章正文、提示性文字等
#define     textFont_Content14                 Font14
//标准字24px12sp/12pt次要描述文字、小副标题、 次要提示、标签文字等
#define     textFont_ContentVice12             Font12
//标准字20px10sp/10pt标签栏名称、次要长段描述或提示文字
#define     textFont_TextPrompt10              Font10





#define FFont1 [UIFont systemFontOfSize:12.0f]

/*font
 Font18: 标准字36px18sp/18pt导航标题、文章标题、重要突出词句
 Font16: 标准字32px16sp/16pt用户姓名、列表文章标题、分类栏目、模块名称、按钮文字等
 Font14: 标准字28px14sp/14pt长段描述文字、非标题文字、文章正文、提示性文字等
 Font12: 标准字24px12sp/12pt次要描述文字、小副标题、 次要提示、标签文字等
 Font10: 标准字20px10sp/10pt标签栏名称、次要长段描述或提示文字
 */
#define     Font(F)             [UIFont systemFontOfSize:(F)]
#define     FontBold(F)         [UIFont boldSystemFontOfSize:(F)]
#define     Font20              [UIFont fontWithName:@"Helvetica" size:20]
#define     Font20Bold          [UIFont fontWithName:@"Helvetica-Bold" size:20]
#define     Font19              [UIFont fontWithName:@"Helvetica" size:19]
#define     Font19Bold          [UIFont fontWithName:@"Helvetica-Bold" size:19]
#define     Font18              [UIFont fontWithName:@"Helvetica" size:18]
#define     Font18Bold          [UIFont fontWithName:@"Helvetica-Bold" size:18]
#define     Font17              [UIFont fontWithName:@"Helvetica" size:17]
#define     Font17Bold          [UIFont fontWithName:@"Helvetica-Bold" size:17]
#define     Font16              [UIFont fontWithName:@"Helvetica" size:16]
#define     Font16Bold          [UIFont fontWithName:@"Helvetica-Bold" size:16]
#define     Font15              [UIFont fontWithName:@"Helvetica" size:15]
#define     Font15Bold          [UIFont fontWithName:@"Helvetica-Bold" size:15]
#define     Font14              [UIFont fontWithName:@"Helvetica" size:14]
#define     Font14Bold          [UIFont fontWithName:@"Helvetica-Bold" size:14]
#define     Font13              [UIFont fontWithName:@"Helvetica" size:13]
#define     Font13Bold          [UIFont fontWithName:@"Helvetica-Bold" size:13]
#define     Font12              [UIFont fontWithName:@"Helvetica" size:12]
#define     Font12Bold          [UIFont fontWithName:@"Helvetica-Bold" size:12]
#define     Font10              [UIFont fontWithName:@"Helvetica" size:10]
#define     Font10Bold          [UIFont fontWithName:@"Helvetica-Bold" size:10]
#define     FontTitleNormal     [UIFont fontWithName:@"Helvetica-Bold" size:15]
#define     FontPromptNormal    [UIFont fontWithName:@"Helvetica" size:14]
//设置字体
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
#define EM_MULTILINE_TEXTSIZE(text, font, maxSize, mode) [text length] > 0 ? [text \
boundingRectWithSize:maxSize options:(NSStringDrawingUsesLineFragmentOrigin) \
attributes:@{NSFontAttributeName:font} context:nil].size : CGSizeZero;
#else
#define EM_MULTILINE_TEXTSIZE(text, font, maxSize, mode) [text length] > 0 ? [text \
sizeWithFont:font constrainedToSize:maxSize lineBreakMode:mode] : CGSizeZero;
#endif
#endif /* HBFontAndColorMacros_h */
