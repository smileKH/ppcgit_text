//
//  HBFMDBManage.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/6.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBFMDBManage : NSObject
//只要一个数据库
@property (nonatomic ,strong) FMDatabase *queue;
//单例
SINGLETON_FOR_HEADER(HBFMDBManage)
//+ (HBFMDBManage *)shareHBFMDBManage;
//建立表
-(void)createTableWithType;
//写入数据库
-(void)writeDBWithData:(Cart_add *)addModel;
//获取数据
-(NSArray *)selectTableName:(NSString *)tableName size:(int)size startPage:(int)page;
//删除所有商品表
-(void)deleteAllTable;
@end
