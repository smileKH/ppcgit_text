//
//  HBB_DataManage.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/6.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBB_DataManage.h"
#define KFMDBName @"VideoShopping.sqlite"//数据库(也可以是.db)
#define KTBUserInfo @"CartListItem"//用户信息表

#define documents [NSHomeDirectory() stringByAppendingString:@"/Documents"]
@interface HBB_DataManage (){
    FMDatabase *_fmdb;
}

@end
@implementation HBB_DataManage
//单例
SINGLETON_FOR_CLASS(HBB_DataManage);
//创建数据库
-(id)init{
    
    self = [super init];
    if (self) {
        [self createDB];
    }
    return self;
    
}
-(void)createDB{
    NSString *database_path = [documents stringByAppendingPathComponent:KFMDBName];
    //数据库打开、创建
    _fmdb = [FMDatabase databaseWithPath:database_path];
}
//建立表
-(void)createTableWithName:(NSString *)tableName{
    //商品数量 sku_id package_sku_ids package_id obj_type mode
    NSString *sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID INTEGER PRIMARY KEY AUTOINCREMENT, shopName VARCHAR,shopId VARCHAR,goodsName VARCHAR, goodsId VARCHAR, money VARCHAR, quantity VARCHAR, sku_id VARCHAR, package_sku_ids VARCHAR, package_id VARCHAR, obj_type VARCHAR, mode VARCHAR, image_default_id VARCHAR);",KTBUserInfo];
    if ([self fmdbExecSql:sql]) {
        NSLog(@"建表成功");
    }else{
        NSLog(@"建表失败");
    }
}
#pragma mark - fmdbUpdate
- (BOOL)fmdbExecSql:(NSString *)sql{
    if ([_fmdb open]) {
        
        /*
         * 只要sql不是SELECT命令的都视为更新操作(使用executeUpdate方法)。就包括 CREAT,UPDATE,INSERT,ALTER,BEGIN,COMMIT,DETACH,DELETE,DROP,END,EXPLAIN,VACUUM,REPLACE等等。SELECT命令的话，使用executeQuery方法。
         * 执行更新返回一个BOOL值。YES表示 执行成功，否则表示有错误。你可以调用 -lastErrorMessage 和 -lastErrorCode方法来得到更多信息。
         */
        if ([_fmdb executeUpdate:sql]) {
            NSLog(@"%@%@%@",@"fmdb操作表",KTBUserInfo,@"成功！");
            return YES;
        }else{
            NSLog(@"%@%@%@ lastErrorMessage：%@，lastErrorCode：%d",@"fmdb创建",KTBUserInfo,@"失败！",_fmdb.lastErrorMessage,_fmdb.lastErrorCode);
            return NO;
        }
    }else{
        NSLog(@"%@",@"fmdb数据库打开失败！");
        return NO;
    }
}
//写入数据库
-(BOOL)writeDBWithData:(Cart_add *)addModel{
    NSString *sql = [NSString stringWithFormat:
                     @"INSERT INTO '%@' ('shopName', 'shopId', 'goodsName', 'goodsId', 'money', 'quantity', 'sku_id', 'package_sku_ids', 'package_id', 'obj_type', 'mode', 'image_default_id') VALUES ('%@', '%@', '%@','%@','%@', '%@', '%@','%@','%@', '%@', '%@', '%@');",KTBUserInfo, addModel.shopName, addModel.shopId, addModel.goodsName,addModel.goodsId,addModel.money,addModel.quantity,addModel.sku_id,addModel.package_sku_ids,addModel.package_id,addModel.obj_type,addModel.money,addModel.image_default_id];
    if ([self fmdbExecSql:sql]) {
        NSLog(@"写入成功");
        return YES;
    }else{
        NSLog(@"写入失败");
        return NO;
    }
    
}

#pragma mark - fmdbSelectData
- (NSArray *)fmdbSelectData{
    NSString *sqlQuery = [NSString stringWithFormat:@"SELECT * FROM %@;",KTBUserInfo];
    
    //根据条件查询
    FMResultSet *resultSet = [_fmdb executeQuery:sqlQuery];
    
    NSMutableArray *arr = [NSMutableArray array];
    //遍历结果集合
    while ([resultSet  next]){
        Cart_add *model = [[Cart_add alloc]init];
        model.shopName = [resultSet
                         objectForColumnName:@"shopName"];
        model.shopId = [resultSet
                          objectForColumnName:@"shopId"];
        model.goodsName = [resultSet
                          objectForColumnName:@"goodsName"];
        model.goodsId = [resultSet
                          objectForColumnName:@"goodsId"];
        model.money = [resultSet
                          objectForColumnName:@"money"];
        model.quantity = [resultSet
                          objectForColumnName:@"quantity"];
        model.sku_id = [resultSet
                          objectForColumnName:@"sku_id"];
        model.package_sku_ids = [resultSet
                          objectForColumnName:@"package_sku_ids"];
        model.package_id = [resultSet
                                 objectForColumnName:@"package_id"];
        model.obj_type = [resultSet
                                 objectForColumnName:@"obj_type"];
        model.mode = [resultSet
                                 objectForColumnName:@"mode"];
        model.image_default_id = [resultSet
                      objectForColumnName:@"image_default_id"];
        
        [arr addObject:model];
        
    }
    return arr;
    /*
     * fmdb封装过后的读取数据是要比原生的sqlite3方便了很多哈
     */
}
//更新数据库
- (BOOL)fmdbUpdateData:(HBCartOfGoodsItemModel *)model{
    NSString *str = [NSString stringWithFormat:@"%ld",model.quantity];
    NSString *sql = [NSString stringWithFormat:@"UPDATE %@ set quantity='%@' WHERE goodsId= %@;",KTBUserInfo,str,model.goodsId];
    if ([self fmdbExecSql:sql]) {
        NSLog(@"更新成功");
        return YES;
    }else{
        NSLog(@"更新失败");
        return NO;
    }
}

//删除数据库数据
- (BOOL)fmdbDeleteData:(HBCartOfGoodsItemModel *)model{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE goodsId= %@;;",KTBUserInfo,model.goodsId];
    if ([self fmdbExecSql:sql]) {
        NSLog(@"删除成功");
        return YES;
    }else{
        NSLog(@"删除失败");
        return NO;
    }
}

#pragma mark - fmdb多线程
- (void)fmdbQueue:(NSArray *)arr{
    
    //创建队列
    FMDatabaseQueue *queue = [FMDatabaseQueue
                              databaseQueueWithPath:[documents stringByAppendingPathComponent:KFMDBName]];
    __block BOOL tag = true;
    
    //把任务放到到队列里
    [queue inTransaction:^(FMDatabase *dbe, BOOL *rollback)
     {
         tag &= [dbe executeUpdate:@"INSERT INTO userInfo ('age') VALUES (?)",[NSNumber numberWithInt:11]];
         tag &= [dbe executeUpdate:@"INSERT INTO userInfo ('age') VALUES (?)",[NSNumber numberWithInt:22]];
         tag &= [dbe executeUpdate:@"INSERT INTO userInfo ('age') VALUES (?)",[NSNumber numberWithInt:33]];
         //如果有错误 返回
         if (!tag)
         {
             *rollback = YES;
             return;
         }
     }];
}
//删除所有表
-(void)deleteAllTable{
     NSString * sql = [NSString stringWithFormat:@"DROP TABLE %@",KTBUserInfo];
    if ([self fmdbExecSql:sql]) {
        NSLog(@"删除成功");
    }else{
        NSLog(@"删除失败");
    }
}

@end
