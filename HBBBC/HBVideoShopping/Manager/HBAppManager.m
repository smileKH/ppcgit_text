//
//  HBAppManager.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "HBAppManager.h"
#import "AdPageView.h"

#import "YYFPSLabel.h"

#import "XHLaunchAd.h"
@interface HBAppManager ()<XHLaunchAdDelegate>

@end
@implementation HBAppManager
+(void)appStart{
    //加载广告
    AdPageView *adView = [[AdPageView alloc] initWithFrame:SCREEN_BOUNDS withTapBlock:^{
        HBRootNavigationController *loginNavi =[[HBRootNavigationController alloc] initWithRootViewController:[[HBRootWebViewController alloc] initWithUrl:@"http:www.hao123.com"]];
        [bRootViewController presentViewController:loginNavi animated:YES completion:nil];
    }];
    adView = adView;
}
#pragma mark ————— FPS 监测 —————
+(void)showFPS{
    YYFPSLabel *_fpsLabel = [YYFPSLabel new];
    [_fpsLabel sizeToFit];
    _fpsLabel.bottom = SCREEN_HEIGHT - 55;
    _fpsLabel.right = SCREEN_WIDTH - 10;
    //    _fpsLabel.alpha = 0;
    [bAppWindow addSubview:_fpsLabel];
}

#pragma mark - 图片开屏广告-本地数据-示例
//图片开屏广告 - 本地数据
+(void)example{
    
    //设置你工程的启动页使用的是:LaunchImage 还是 LaunchScreen.storyboard(不设置默认:LaunchImage)
    [XHLaunchAd setLaunchSourceType:SourceTypeLaunchImage];
    
    //配置广告数据
    XHLaunchImageAdConfiguration *imageAdconfiguration = [XHLaunchImageAdConfiguration new];
    //广告停留时间
    imageAdconfiguration.duration = 5;
    //广告frame
    imageAdconfiguration.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //广告图片URLString/或本地图片名(.jpg/.gif请带上后缀)
    imageAdconfiguration.imageNameOrURLString = [self setAppLanchImage];
    //设置GIF动图是否只循环播放一次(仅对动图设置有效)
    imageAdconfiguration.GIFImageCycleOnce = NO;
    //图片填充模式
    imageAdconfiguration.contentMode = UIViewContentModeScaleAspectFill;
    //广告点击打开页面参数(openModel可为NSString,模型,字典等任意类型)
    imageAdconfiguration.openModel = @"http://www.dysjdp.com";
    //广告显示完成动画
    imageAdconfiguration.showFinishAnimate =ShowFinishAnimateLite;
    //广告显示完成动画时间
    imageAdconfiguration.showFinishAnimateTime = 0.8;
    //跳过按钮类型
    imageAdconfiguration.skipButtonType = SkipTypeTimeText;
    //后台返回时,是否显示广告
    imageAdconfiguration.showEnterForeground = YES;
    //设置要添加的子视图(可选)
    //imageAdconfiguration.subViews = [self launchAdSubViews];
    //显示开屏广告
    [XHLaunchAd imageAdWithImageAdConfiguration:imageAdconfiguration delegate:self];
    
}
//跳过按钮点击事件
-(void)skipAction{
    
    //移除广告
    [XHLaunchAd removeAndAnimated:YES];
}
#pragma mark - XHLaunchAd delegate - 其他
/**
 广告点击事件回调
 */
-(void)xhLaunchAd:(XHLaunchAd *)launchAd clickAndOpenModel:(id)openModel clickPoint:(CGPoint)clickPoint{
    
    NSLog(@"广告点击事件");
    /**
     openModel即配置广告数据设置的点击广告时打开页面参数
     */
    
//    WebViewController *VC = [[WebViewController alloc] init];
//    NSString *urlString = (NSString *)openModel;
//    VC.URLString = urlString;
//    //此处不要直接取keyWindow
//    UIViewController* rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
//    [rootVC.myNavigationController pushViewController:VC animated:YES];
    
}
/**
 *  广告显示完成
 */
-(void)xhLaunchAdShowFinish:(XHLaunchAd *)launchAd{
    
    NSLog(@"广告显示完成");
}
#pragma mark --- 设置延迟启动LanchImage
+(NSString *)setAppLanchImage{
    NSString *imageName = @"LIM640X960";
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    if (screenH == 568) {
        imageName = @"LIM640X1136.png";
    } else if (screenH == 667) {
        imageName = @"LIM750X1334.png";
    } else if (screenH == 736) {
        imageName = @"LIM1242X2208.png";
    }else if (screenH == 812) {
        imageName = @"LIM1125X2436.png";
    }
    return imageName;
}
@end
