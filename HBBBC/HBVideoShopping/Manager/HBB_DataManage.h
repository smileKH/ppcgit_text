//
//  HBB_DataManage.h
//  HBVideoShopping
//
//  Created by aplle on 2018/4/6.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBB_DataManage : NSObject
//单例
SINGLETON_FOR_HEADER(HBB_DataManage)

//建立表
-(void)createTableWithName:(NSString *)tableName;
//写入数据库
-(BOOL)writeDBWithData:(Cart_add *)addModel;
//读取数据
- (NSArray *)fmdbSelectData;
//更新数据库
- (BOOL)fmdbUpdateData:(HBCartOfGoodsItemModel *)model;
//删除数据库数据
- (BOOL)fmdbDeleteData:(HBCartOfGoodsItemModel *)model;
//多线程
- (void)fmdbQueue:(NSArray *)arr;
//删除所有表
-(void)deleteAllTable;
@end
