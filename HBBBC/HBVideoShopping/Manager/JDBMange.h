//
//  JDBMange.h
//  fengerPosSystem
//
//  Created by apple on 17/2/15.
//  Copyright © 2017年 fenger. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JDBMange : NSObject
//只要一个数据库
@property (nonatomic ,strong) FMDatabaseQueue *queue;

@property (nonatomic ,strong) FMDatabaseQueue *OfflineQueue;
//单例
+ (JDBMange *)shareMange;

//建立表
-(void)createTableWithType;


//写入数据库
-(void)writeDBWithData:(NSDictionary *)dic;

//分页获取数据
-(NSArray *)selectTableName:(NSString *)tableName size:(int)size startPage:(int)page ;

//按条件分页获取数据
-(NSArray *)selectTableName:(NSString *)tableName size:(int)size startPage:(int)page where:(NSString *)Id idType:(NSString *)idType;

//根据条码搜索商品
-(NSArray *)selectTableName:(NSString *)tableName whereBarCode:(NSString *)code;


//删除所有商品表
-(void)deleteAllTable;

//建立离线操作表
-(void)createOfflineOperationTable;

//插入离线操作数据
-(void)insertOfflineOperationTableWithParametres:(NSString *)parameter Url:(NSString *)url OperationState:(NSString *)state;

//更新离线操作数据
-(void)updateOfflineOperationTableWithArr:(NSMutableArray *)arr;


//检查是否有未提交的离线操作数据
-(BOOL)checkOfflineOperationTable;

//获取未提交的离线操作数据
-(NSArray *)selectOfflineOperationTable;

//根据id查找商品规格数据
-(NSArray *)selectDataFromProductSkuWithSkuId:(NSString *)Id;



//清空所有商品表内容
-(void)deleteAllTableContent;


//写入商品品牌数据库
-(void)writeDBWithBrandListData:(NSDictionary *)dic;

@end
