//
//  HBAppManager.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 包含应用层的相关服务
 */
@interface HBAppManager : NSObject
#pragma mark ————— APP启动接口 —————
+(void)appStart;

#pragma mark ————— FPS 监测 —————
+(void)showFPS;

#pragma mark ==========启动页==========
+(void)example;
@end
