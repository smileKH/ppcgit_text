//
//  HBUserManager.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "HBUserManager.h"
#import <UMSocialCore/UMSocialCore.h>
@implementation HBUserManager
SINGLETON_FOR_CLASS(HBUserManager);

/*!
 @method
 @brief             判断登录状态，已登录返回yes，未登录则会跳到登录页面
 
 已登录返回yes，未登录返回no
 */
- (BOOL)judgeLoginState
{
    BOOL flag = NO;
    if (self.loginState == 1) {
        flag = YES;
    }else {
        [self showLoginView];
    }
    
    return flag;
}



/*!
 @method
 @brief             判断登录状态，已登录返回yes，未登录
 
 已登录返回yes，未登录返回no
 */
- (BOOL)oneceJudgeLoginState
{
    BOOL flag = NO;
    if (self.loginState == 1) {
        flag = YES;
    }else {
        //        [self showLoginView];
    }
    
    return flag;
}
//弹出登录页面
- (void)showLoginView
{
    UIViewController *currentVC = [app getNowViewController];
    LoginViewController*loginVC = [[LoginViewController alloc] init];
//    HBRootNavigationController *navController = [[HBRootNavigationController alloc] initWithRootViewController:loginVC];
//    [currentVC presentViewController:navController animated:YES completion:nil];
    [currentVC.navigationController pushViewController:loginVC animated:YES];
}

-(instancetype)init{
    self = [super init];
    if (self) {
        //被踢下线
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onKick)
                                                     name:KNotificationOnKick
                                                   object:nil];
    }
    return self;
}

#pragma mark ————— 三方登录 —————
-(void)login:(UserLoginType )loginType completion:(loginBlock)completion{
    [self login:loginType params:nil completion:completion];
}

#pragma mark ————— 带参数登录 —————
-(void)login:(UserLoginType )loginType params:(NSDictionary *)params completion:(loginBlock)completion{
    //友盟登录类型
    UMSocialPlatformType platFormType;
    
    if (loginType == bUserLoginTypeQQ) {
        platFormType = UMSocialPlatformType_QQ;
    }else if (loginType == bUserLoginTypeWeChat){
        platFormType = UMSocialPlatformType_WechatSession;
    }else{
        platFormType = UMSocialPlatformType_UnKnown;
    }
    //第三方登录
    if (loginType != bUserLoginTypePwd) {
        [MBProgressHUD showActivityMessageInView:@"授权中..."];
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:platFormType currentViewController:nil completion:^(id result, NSError *error) {
            if (error) {
                [MBProgressHUD hideHUD];
                if (completion) {
                    completion(NO,error.localizedDescription);
                }
            } else {
                
                UMSocialUserInfoResponse *resp = result;
                //
                //                // 授权信息
                //                NSLog(@"QQ uid: %@", resp.uid);
                //                NSLog(@"QQ openid: %@", resp.openid);
                //                NSLog(@"QQ accessToken: %@", resp.accessToken);
                //                NSLog(@"QQ expiration: %@", resp.expiration);
                //
                //                // 用户信息
                //                NSLog(@"QQ name: %@", resp.name);
                //                NSLog(@"QQ iconurl: %@", resp.iconurl);
                //                NSLog(@"QQ gender: %@", resp.unionGender);
                //
                //                // 第三方平台SDK源数据
                //                NSLog(@"QQ originalResponse: %@", resp.originalResponse);
                
                //登录参数
                NSDictionary *params = @{@"openid":resp.openid, @"nickname":resp.name, @"photo":resp.iconurl, @"sex":[resp.unionGender isEqualToString:@"男"]?@1:@2, @"cityname":resp.originalResponse[@"city"], @"fr":@(loginType)};
                
                self.loginType = loginType;
                //登录到服务器
                [self loginToServer:params completion:completion];
            }
        }];
    }else{
        //账号登录 暂未提供
        [self loginToServer:params completion:completion];
    }
}

#pragma mark ————— 手动登录到服务器 —————
-(void)loginToServer:(NSDictionary *)params completion:(loginBlock)completion{

    [LBService post:USER_LOGIN_BBS params:params completion:^(LBResponse *response) {
        if (response.succeed) {
//            [MBProgressHUD hideHUD];
//             [self LoginSuccess:response.result completion:completion];
            //获取用户信息
            if (ValidDict(response.result)) {
                //先保存token
                NSDictionary *dic = response.result[@"data"];
                NSString *accessToken = dic[@"accessToken"];
                NSNumber *createDate = dic[@"createDate"];
                //保存账号信息
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:accessToken forKey:TOKEN_XY_APP];
                [userDefaults setObject:createDate forKey:CREATE_DATE_XY_APP];
                [userDefaults synchronize];
                self.isLogined = YES;
                userManager.loginState = 1;
            }
            if (completion) {
                completion(YES,response.message);
            }
        }else{
//            [MBProgressHUD hideHUD];
            if (completion) {
                completion(NO,response.message);
            }
        }
    }];
}

#pragma mark ————— 自动登录到服务器 —————
-(void)autoLoginToServer:(loginBlock)completion{
    //请求用户信息
    [self requestUserInfo:nil completion:completion];
}

#pragma mark ————— 登录成功处理 —————
-(void)LoginSuccess:(id )responseObject completion:(loginBlock)completion{
    //获取用户信息
    if (ValidDict(responseObject)) {
        //先保存token
        NSDictionary *dic = responseObject[@"data"];
        NSString *accessToken = dic[@"accessToken"];
        NSNumber *createDate = dic[@"createDate"];
        //保存账号信息
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:accessToken forKey:TOKEN_XY_APP];
        [userDefaults setObject:createDate forKey:CREATE_DATE_XY_APP];
        [userDefaults synchronize];
        
    }
    
}
#pragma mark ————— 请求用户信息 —————
-(void)requestUserInfo:(id)responseObject completion:(userInfoBlock)completion{
    //再请求用户信息
    [LBService post:MEMBER_BASICS_GET params:nil completion:^(LBResponse *response) {
//        [MBProgressHUD hideHUD];
        if (response.succeed) {
            if (ValidDict(response.result)) {
                NSDictionary *dic = response.result[@"data"];
                //保存用户信息
                self.curUserInfo = [HBUserInfo modelWithDictionary:dic];
                [self saveUserInfo];
                            
                //登录成功，保存头像
   
                if (completion) {
                    completion(YES,response.message);
                }
            }
            
        }else{
            if (completion) {
                completion(NO,response.message);
            }
            //错误
            [app showToastView:response.message];
        }
        
    }];
}

#pragma mark ————— 更新用户信息 —————
-(void)updateUserInfo:(id)parm completion:(loginBlock)completion{
    [LBService post:MEMBER_BASICS_UPDATE params:parm completion:^(LBResponse *response) {
        //        [MBProgressHUD hideHUD];
        if (response.succeed) {
            if (ValidDict(response.result)) {
//                NSDictionary *dic = response.result[@"data"];
                //保存用户信息
//                self.curUserInfo = [HBUserInfo modelWithDictionary:dic];
//                [self saveUserInfo];
                
                //成功
                
                if (completion) {
                    completion(YES,response.message);
                }
            }
            
        }else{
            if (completion) {
                completion(NO,response.message);
            }
            //错误
            [app showToastView:response.message];
        }
        
    }];
}
#pragma mark ————— 储存用户信息 —————
-(void)saveUserInfo{
    if (self.curUserInfo) {
        YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
        NSDictionary *dic = [self.curUserInfo modelToJSONObject];
        [cache setObject:dic forKey:KUserModelCache];
    }
    
}
#pragma mark ————— 加载缓存的用户信息 —————
-(BOOL)loadUserInfo{
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    NSDictionary * userDic = (NSDictionary *)[cache objectForKey:KUserModelCache];
    if (userDic) {
        self.curUserInfo = [HBUserInfo modelWithJSON:userDic];
        return YES;
    }
    return NO;
}
#pragma mark ————— 被踢下线 —————
-(void)onKick{
    [self logout:nil];
}
#pragma mark ————— 退出登录 —————
- (void)logout:(void (^)(BOOL, NSString *))completion{
    
    //    accessToken
    NSString * accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:TOKEN_XY_APP];
    if (accessToken.length <= 0) {
        return;
    }

    WEAKSELF;
    NSString *url = USER_LOGOUT_BBS;
    [LBService post:url params:@{} completion:^(LBResponse *response) {
        if (response.succeed) {
            if (completion) {
                completion(YES,response.message);
            }
            //退出登录成功
            if ([HBHuTool exitAccount]) {
                //退出成功，进行操作
                [weakSelf backSucceedSetValue:completion];
                
            }else{
                //退出失败
                if (completion) {
                    completion(NO,response.message);
                }
            }
            
        }else{
            //退出失败
            if (completion) {
                completion(NO,response.message);
            }
        }
        
    }];
    
   
}
#pragma mark ==========退出登录成功，清理缓存==========
-(void)backSucceedSetValue:(void (^)(BOOL, NSString *))completion{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
    self.curUserInfo = nil;
    self.isLogined = NO;
    //设置状态
    userManager.loginState = 0;
    //发一个通知
    [[NSNotificationCenter defaultCenter]postNotificationName:NotificationLogoutSuccess object:nil];
    //移除缓存
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    [cache removeAllObjectsWithBlock:^{
        if (completion) {
            completion(YES,nil);
        }
    }];
    
}
@end
