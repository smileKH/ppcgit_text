//
//  HBUserManager.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HBUserInfo.h"

typedef NS_ENUM(NSInteger, UserLoginType){
    bUserLoginTypeUnKnow = 0,//未知
    bUserLoginTypeWeChat,//微信登录
    bUserLoginTypeQQ,///QQ登录
    bUserLoginTypePwd,///账号登录
};

typedef void (^loginBlock)(BOOL success, NSString * des);
//用户信息
typedef void (^userInfoBlock)(BOOL success, NSString * des);

#define isLogin [HBUserManager sharedHBUserManager].isLogined
#define curUser [HBUserManager sharedHBUserManager].curUserInfo
#define userManager [HBUserManager sharedHBUserManager]
/**
 包含用户相关服务
 */
@interface HBUserManager : NSObject
//单例
SINGLETON_FOR_HEADER(HBUserManager)

//当前用户
@property (nonatomic, strong) HBUserInfo *curUserInfo;
@property (nonatomic, assign) UserLoginType loginType;
@property (nonatomic, assign) BOOL isLogined;
@property (nonatomic, assign) NSInteger     loginState;

#pragma mark - ——————— 登录相关 ————————


/**
 三方登录
 
 @param loginType 登录方式
 @param completion 回调
 */
-(void)login:(UserLoginType )loginType completion:(loginBlock)completion;

/**
 带参登录
 
 @param loginType 登录方式
 @param params 参数，手机和账号登录需要
 @param completion 回调
 */
-(void)login:(UserLoginType )loginType params:(NSDictionary *)params completion:(loginBlock)completion;

/**
 自动登录
 
 @param completion 回调
 */
-(void)autoLoginToServer:(loginBlock)completion;

/**
 退出登录
 
 @param completion 回调
 */
- (void)logout:(loginBlock)completion;

/**
 加载缓存用户数据
 
 @return 是否成功
 */
-(BOOL)loadUserInfo;

/*!
 @method
 @brief             判断登录状态，已登录返回yes，
 @return            已登录返回yes，未登录返回no
 */
- (BOOL)oneceJudgeLoginState;

/*!
 @method
 @brief             判断登录状态，已登录返回yes，未登录则弹出登录页面
 @return            已登录返回yes，未登录返回no
 */
- (BOOL)judgeLoginState;

#pragma mark ————— 请求用户信息 —————
-(void)requestUserInfo:(id)responseObject completion:(userInfoBlock)completion;
#pragma mark ————— 更新用户信息 —————
-(void)updateUserInfo:(id)parm completion:(loginBlock)completion;
@end
