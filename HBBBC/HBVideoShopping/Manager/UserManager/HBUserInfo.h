//
//  HBUserInfo.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GameInfo;

typedef NS_ENUM(NSInteger,UserGender){
    UserGenderUnKnow = 0,
    UserGenderMale, //男
    UserGenderFemale //女
};

@interface HBUserInfo : NSObject
//用户名
@property(nonatomic, strong) NSString       *login_account;
//真实姓名
@property(nonatomic, strong) NSString       *username;
//昵称
@property (nonatomic,strong)NSString        *name;
//生日
@property(nonatomic, strong) NSString       *birthday;
//性别 0女 1男 2保密
@property(nonatomic, assign) NSInteger      sex;
//会员等级ID
@property(nonatomic, strong) NSString       *grade_id;
//会员等级名称
@property(nonatomic, strong) NSString       *grade_name;
//会员等级名称
@property(nonatomic, strong) NSString       *portrait_url;

//----------------------------BBBStore------------------//
@property (nonatomic, strong) NSString * accessToken;
@property (nonatomic,copy) NSString * user_id;//昵称
@end
