//
//  HBFMDBManage.m
//  HBVideoShopping
//
//  Created by aplle on 2018/4/6.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "HBFMDBManage.h"

@implementation HBFMDBManage
//单例
SINGLETON_FOR_CLASS(HBFMDBManage);
//+ (HBFMDBManage *)shareHBFMDBManage{
//    
//    static HBFMDBManage *manager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken,^{
//        if (manager == nil) {
//            manager = [[HBFMDBManage alloc]init];
//            [manager createDB];
//        }
//    });
//    return manager;
//    
//}

-(id)init{
    
    self = [super init];
    if (self) {
        [self createDB];
    }
    return self;
    
}
//创建数据库
-(void)createDB{
    
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES).firstObject;
    NSString *filePath = [path stringByAppendingPathComponent:@"shoppingCart.db"];
    
    NSLog(@"dbpath===========%@",path);
    
    self.queue = [FMDatabase databaseWithPath:filePath];
    
    //打开数据库
    [self.queue open];
    
    //创建学生表
    BOOL isSucc = [self.queue executeUpdate:@"create table CartList (id integer primary key autoincrement, shopName text, shopid text, goodsName text, goodsId text);"];
    NSLog(@"打印有没有创建表成功%d", isSucc);
}
//建立表
-(void)createTableWithType{
    
    //建表 保存商品名称
    
    
//    [self.queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
//        //建表 id 名字 店铺id
//        NSString *sql = @"create table CartList (id integer primary key autoincrement, shopName text, shopid text, goodsName text, goodsId text);";
//
//        BOOL result = [db executeUpdate:sql];
//        if ( !result ) {
//            //当最后*rollback的值为YES的时候，事务回退，如果最后*rollback为NO，事务提交
//            *rollback = YES;
//            return;
//        }
//
//
//    }];
    
}

//写入数据库
-(void)writeDBWithData:(Cart_add *)addModel{
    
    NSString *Id = addModel.sku_id;
    NSString *shopName = addModel.shopName;
    NSString *shopId = addModel.shopId;
    NSString *goodsName = addModel.goodsName;
    NSString *goodsId = addModel.goodsId;
    NSString *addSql = [NSString stringWithFormat:@"insert into CartList(id,shopName,shopid,goodsName,goodsId) values ('%@','%@','%@','%@','%@');", Id,shopName,shopId,goodsName,goodsId];
    
    BOOL isSuccess = [self.queue executeUpdate:addSql];
    
    if (isSuccess) {
        NSLog(@"插入成功");
    }else{
        NSLog(@"插入失败");
    }
    
//    [self.queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
//
//        NSString *Id = addModel.sku_id;
//        NSString *shopName = addModel.shopName;
//        NSString *shopId = addModel.shopId;
//        NSString *goodsName = addModel.goodsName;
//        NSString *goodsId = addModel.goodsId;
//        NSString *sql = @"insert into CartList(id,shopName,shopid,goodsName,goodsId) values(?,?,?,?,?);";
//
//        BOOL result = [db executeUpdate:sql,Id,shopName,shopId,goodsName,goodsId];
//
//        if (!result) {
//
//            *rollback = YES;
//
//            return;
//
//        }
//
//    }];
}

//获取数据
-(NSArray *)selectTableName:(NSString *)tableName size:(int)size startPage:(int)page{
    NSMutableArray *temp=[NSMutableArray array];
    NSString *sql=[NSString stringWithFormat:@"Select * From %@ Limit %d Offset %d",tableName,size,(page-1)*size];
    
//    [self.queue inDatabase:^(FMDatabase *db) {
//        FMResultSet *rs=[db executeQuery:sql];
//
//        while ([rs next]){
//            //用模型来接收
//        }
//    }];
    
    return temp;
}

//删除所有商品表
-(void)deleteAllTable{
    
    
//    NSString * sql = [NSString stringWithFormat:@"DROP TABLE %@",@"Industry"];
//
//    [self.queue inDatabase:^(FMDatabase *db) {
//        BOOL deletebool =[db executeUpdate:sql];
//
//        if (deletebool)
//        {
//            NSLog(@"删除Industry成功");
//        }
//        else
//        {
//            NSLog(@"删除Industry失败");
//        }
//
//    }];
    
}
@end
