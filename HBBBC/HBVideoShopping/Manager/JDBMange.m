//
//  JDBMange.m
//  fengerPosSystem
//
//  Created by apple on 17/2/15.
//  Copyright © 2017年 fenger. All rights reserved.
//

#import "JDBMange.h"

@implementation JDBMange


//单例
+ (JDBMange *)shareMange{

    static JDBMange *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        if (manager == nil) {
            manager = [[JDBMange alloc]init];
            [manager createDB];
        }
    });
    return manager;

}

-(id)init{
    
    self = [super init];
    if (self) {
        [self createDB];
    }
    return self;
    
}
//创建数据库
-(void)createDB{

    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES).firstObject;
    NSString *filePath = [path stringByAppendingPathComponent:@"videoShopping.db"];
    
    NSLog(@"dbpath===========%@",path);
    
    self.queue = [FMDatabaseQueue databaseQueueWithPath:filePath];
}

//建立表
-(void)createTableWithType{

    //建表 保存商品名称
    [self.queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        //建表 id 名字 店铺id
        NSString *sql = @"create table CartList (id integer primary key autoincrement, shopName text, shopid text, goodsName text, number text);";
        
        BOOL result = [db executeUpdate:sql];
        if ( !result ) {
            //当最后*rollback的值为YES的时候，事务回退，如果最后*rollback为NO，事务提交
            *rollback = YES;
            return;
        }
        
        
    }];

}



//分页获取数据
-(NSArray *)selectTableName:(NSString *)tableName size:(int)size startPage:(int)page{
    NSMutableArray *temp=[NSMutableArray array];
    NSString *sql=[NSString stringWithFormat:@"Select * From %@ Limit %d Offset %d",tableName,size,(page-1)*size];
    
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs=[db executeQuery:sql];
        
        while ([rs next]){
            //用模型来接收
        }
    }];
    
    return temp;
}

//按条件分页获取数据
-(NSArray *)selectTableName:(NSString *)tableName size:(int)size startPage:(int)page where:(NSString *)Id idType:(NSString *)idType{

    NSMutableArray *temp=[NSMutableArray array];
    NSString *sql;
    
    //获取门店业态分页
    sql=[NSString stringWithFormat:@"Select * From %@ WHERE %@=%@ ORDER BY id ASC Limit %d Offset %d ",tableName,idType,Id,size,(page-1)*size];
    
    [self.queue inDatabase:^(FMDatabase *db) {
        
        FMResultSet *rs=[db executeQuery:sql];
        
        
        while ([rs next]){
            //用模型来接收
            
        }
    }];

    return temp;

}


//根据ID搜索商品 这里就能够使用对应的店铺和对应的商品
-(NSArray *)selectTableName:(NSString *)tableName whereBarCode:(NSString *)code{
    
    NSString *trimmedString = [code stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] NSLog(trimmedString);
    
//    ProductSkuModel *psm = [[ProductSkuModel alloc]init];
//    ProductModel *pm = [[ProductModel alloc]init];

    NSMutableArray *temp=[NSMutableArray array];
    NSString *sql=[NSString stringWithFormat:@"Select * From %@ WHERE sku='%@'",tableName,trimmedString];
    
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs=[db executeQuery:sql];
        
        while ([rs next]){
            
//            psm.Id = [rs stringForColumn:@"id"];
//            psm.barCode = [rs stringForColumn:@"barCode"];
//            psm.price = [rs stringForColumn:@"price"];
//            psm.sku = [rs stringForColumn:@"sku"];
//            psm.productName = [rs stringForColumn:@"productName"];
//            psm.productId = [rs stringForColumn:@"productId"];
//            psm.skuValuesStr = [rs stringForColumn:@"skuValuesStr"];
        }
    }];
    
//    if (psm.sku==nil) {
//        return nil;
//    }
    
//    NSString *sql2=[NSString stringWithFormat:@"Select * From %@ WHERE id=%@",@"Product",psm.productId];
//
//    [self.queue inDatabase:^(FMDatabase *db) {
//        FMResultSet *rs=[db executeQuery:sql2];
//
//        while ([rs next]){
//
//            pm.Id = [rs stringForColumn:@"id"];
//            pm.salesPrice = [rs stringForColumn:@"salesPrice"];
//            pm.thumbnail = [rs stringForColumn:@"thumbnail"];
//            pm.name = [rs stringForColumn:@"name"];
//            pm.productBrand = [rs stringForColumn:@"productBrand"];
//            pm.industry = [rs stringForColumn:@"industry"];
//            pm.year = [rs stringForColumn:@"year"];
//            pm.season = [rs stringForColumn:@"season"];
//        }
//    }];
//
//    if (pm.Id==nil) {
//        return nil;
//    }
    
//    [temp addObject:psm];
//    [temp addObject:pm];
    
    return temp;

}
//删除所有商品表
-(void)deleteAllTable{
    
    
    NSString * sql = [NSString stringWithFormat:@"DROP TABLE %@",@"Industry"];
    
    [self.queue inDatabase:^(FMDatabase *db) {
        BOOL deletebool =[db executeUpdate:sql];
        
        if (deletebool)
        {
            NSLog(@"删除Industry成功");
        }
        else
        {
            NSLog(@"删除Industry失败");
        }
        
    }];
    
}

//清空所有商品表内容
-(void)deleteAllTableContent{
    
    
    NSString * sql = [NSString stringWithFormat:@"DELETE FROM %@",@"Industry"];
    
    [self.queue inDatabase:^(FMDatabase *db) {
        BOOL deletebool =[db executeUpdate:sql];
        
        if (deletebool)
        {
            NSLog(@"清空Industry成功");
        }
        else
        {
            NSLog(@"清空Industry失败");
        }
        
    }];
    
  
}


//建立离线操作表
-(void)createOfflineOperationTable{

    [self.OfflineQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        NSString *sql = @"create table OfflineOperation (createTime integer primary key autoincrement, parameters text, url text, state text);";
        
        
        BOOL result = [db executeUpdate:sql];
        if ( !result ) {
            //当最后*rollback的值为YES的时候，事务回退，如果最后*rollback为NO，事务提交
            *rollback = YES;
            return;
        }
        
        
    }];
    

}
//插入离线操作数据
-(void)insertOfflineOperationTableWithParametres:(NSString *)parameter Url:(NSString *)url OperationState:(NSString *)state{

    //时间
    NSString *time = @"20180406";
    
    
    NSString *sql = @"insert into OfflineOperation(createTime,parameters,url,state) values(?,?,?,?);";
    
    
    
    [self.OfflineQueue inDatabase:^(FMDatabase *db) {
        
        
        BOOL result = [db executeUpdate:sql,time,parameter,url,state];
        
        
        if (result)
        {
            NSLog(@"添加OfflineOperation数据成功");
        }
        else
        {
            NSLog(@"添加OfflineOperation数据失败");
        }
        
    }];

}

//更新离线操作数据
-(void)updateOfflineOperationTableWithArr:(NSMutableArray *)arr{
    
    

    [self.OfflineQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        
        
        for (int i=0; i<1; i++) {
            
            NSDictionary *rowDic = arr[i];
            
            
            
            NSString * time = [[NSString alloc] initWithFormat:@"%@",rowDic[@"createTime"]];
            
            
            NSString *sql=[NSString stringWithFormat:@"update OfflineOperation set state = %@ WHERE createTime=%@",@"1",time];
            
            BOOL result = [db executeUpdate:sql];
            
            if (!result) {
                
                *rollback = YES;
                
                return;
                
            }
        }
    }];
    
    

}

//检查是否有未提交的离线操作数据
-(BOOL)checkOfflineOperationTable{

    __block int count = 0;
    
    NSString *sql=[NSString stringWithFormat:@"Select * From %@ WHERE state=%@",@"OfflineOperation",@"0"];
    
    [self.OfflineQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs=[db executeQuery:sql];
        
        count = [rs columnCount];
        
    }];
    
    if (count==0) {
        return NO;
    }else{
    
        return YES;
    }

}

//获取未提交的离线操作数据
-(NSArray *)selectOfflineOperationTable{
    
    NSMutableArray *temp=[NSMutableArray array];
    NSString *sql=[NSString stringWithFormat:@"Select * From %@ WHERE state=%@",@"OfflineOperation",@"0"];
    
    [self.OfflineQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs=[db executeQuery:sql];
        
        while ([rs next]){
            
            NSLog(@"seletedTime====%@",[rs stringForColumn:@"createTime"]);
        }
    }];
    
    return temp;
}
//根据id查找商品规格数据
-(NSArray *)selectDataFromProductSkuWithSkuId:(NSString *)Id{
    
    NSMutableArray *temp=[NSMutableArray array];
    
    NSString *sql=[NSString stringWithFormat:@"Select * From ProductSku WHERE id=%@",Id];
    
    [self.queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs=[db executeQuery:sql];
        
        while ([rs next]){
            
//            ProductSkuModel *model = [[ProductSkuModel alloc]init];
//            model.Id = [rs stringForColumn:@"id"];
//            model.barCode = [rs stringForColumn:@"barCode"];
//            model.price = [rs stringForColumn:@"price"];
//            model.sku = [rs stringForColumn:@"sku"];
//            model.productName = [rs stringForColumn:@"productName"];
//            model.productId = [rs stringForColumn:@"productId"];
//            model.skuValuesStr = [rs stringForColumn:@"skuValuesStr"];
            
//            [temp addObject:model];
            
        }
        
    }];
    
    return temp;

}




@end
