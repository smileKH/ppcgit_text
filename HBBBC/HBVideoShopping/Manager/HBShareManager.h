//
//  HBShareManager.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/18.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 分享 相关服务
 */
@interface HBShareManager : NSObject

//单例
SINGLETON_FOR_HEADER(HBShareManager)


/**
 展示分享页面
 */
-(void)showShareView;
@end
