//
//  HBMaterialTZImagePickerHelper.h
//  HBTreasureMaterial
//
//  Created by 胡明波 on 2019/5/16.
//  Copyright © 2019 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TZImagePickerController.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,selectTakeType) {
    selectTakePhotoType,//拍照图片
    selectTakeShootingVideoType,//拍照视频
    selectTakeVideoType,//选择视频
    selectTakeImagePickerType//选择图片
};
@protocol HBMaterialTZImagePickerHelperDelegate <NSObject>

///选择图片回调
-(void)cusSelectTZImagePickerSelectedPhotos:(NSMutableArray *)selectedPhotos withSelectedAssets:(NSMutableArray *)selectAssets andIsOriginalPoto:(BOOL)isSelectOriginalPhoto andBlockData:(NSData *)data outPutPath:(NSString *)outPutPath withSelectTakeType:(selectTakeType )selectType;
@end

@interface HBMaterialTZImagePickerHelper : NSObject<UINavigationControllerDelegate,  UIImagePickerControllerDelegate, TZImagePickerControllerDelegate>

@property (nonatomic, weak)id<HBMaterialTZImagePickerHelperDelegate>delegate;

/**打开手机图片库

@param maxCount 最大张数
@param superController superController
@param selectType selectType
*/
- (void)showImagePickerControllerWithMaxCount:(NSInteger )maxCount WithViewController: (UIViewController *)superController withSelectTakeType:(selectTakeType )selectType withAssetsArr:(NSMutableArray *)CusSelectedAssets andPhotosArr:(NSMutableArray *)CusSelectedPhotos;
@end

NS_ASSUME_NONNULL_END
