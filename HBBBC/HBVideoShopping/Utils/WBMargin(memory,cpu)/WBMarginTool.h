//
//  WBMarginTool.h
//  fps_Cpu_Menory
//
//  Created by Weimob-fu on 2017/2/10.
//  Copyright © 2017年 Weimob-fu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WBMarginTool : NSObject
+ (double)memoryUsed;
+ (NSUInteger)cpuCount;
+ (float)cpuUsage ;
+ (NSString*)systemVersion ;
+ (NSString*)appVersion;

@end
