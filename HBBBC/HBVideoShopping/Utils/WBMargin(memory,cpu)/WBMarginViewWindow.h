//
//  WBMarginViewWindow.h
//  fps_Cpu_Menory
//
//  Created by Weimob-fu on 2017/2/9.
//  Copyright © 2017年 Weimob-fu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WBMarginLabel.h"
@interface WBMarginViewWindow : UIWindow

@property (nonatomic,strong) WBMarginLabel *marginLabel;

@end
