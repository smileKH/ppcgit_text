//
//  WBMarginViewWindow.m
//  fps_Cpu_Menory
//
//  Created by Weimob-fu on 2017/2/9.
//  Copyright © 2017年 Weimob-fu. All rights reserved.
//

#import "WBMarginViewWindow.h"
#import "WBMarginLabel.h"
#import "WBMarginTool.h"
#import "WBWBMarginViewController.h"
#import "UIViewController+ClassName.h"
#import "UIViewController+ClassName.h"
#define IPHONE_X_CUSTOM ([[UIScreen mainScreen] bounds].size.height == 812)
@interface WBMarginViewWindow ()

@property (nonatomic,assign)CFTimeInterval screenUpdatesBeginTime;

@property (nonatomic,assign)CFTimeInterval averageScreenUpdatesTime;

@property (nonatomic,assign) NSInteger fpsCount;

@property (nonatomic,assign) BOOL isShowBig;

@property (nonatomic,assign) float fps;
/**
 *  当前 VC name
 */
@property (nonatomic, copy  ) NSString *className;
@end

@implementation WBMarginViewWindow

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initData];
//        [self addNotification];
        [self addReceiveClassNameNotification];
        self.isShowBig = NO;
        [self initSelfUI];
        [self addLabel];
        [UIViewController displayClassName:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:tap];
        
        UILongPressGestureRecognizer *longtap = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longtapAction)];
        [self addGestureRecognizer:longtap];
        [self addRunLoop];
    }
    return self;
}
-(void)longtapAction{
    
    [[UIPasteboard generalPasteboard] setString:[self className]];
    
    UILabel *alertLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH -30, 30)];
    
    alertLabel.textAlignment = NSTextAlignmentCenter;
    
    alertLabel.layer.cornerRadius = 15;
    
    alertLabel.clipsToBounds = YES;
    
    alertLabel.backgroundColor = [UIColor grayColor];
    
    alertLabel.text = [NSString stringWithFormat:@"已复制:%@",[self className]];
    
    NSLog(@"复制的VC名字:\n\n%@\n",[self className]);
    alertLabel.textColor = [UIColor whiteColor];
    
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    
    alertLabel.center = window.center;
    alertLabel.alpha = 0;
    [window addSubview:alertLabel];
   
    [UIView animateWithDuration:1.0 animations:^{
        alertLabel.alpha = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            alertLabel.alpha = 0;
        } completion:^(BOOL finished) {
            [alertLabel removeFromSuperview];
        }];
        
    }];
    
    
}
-(void)tapAction{
    
    CGRect screenFrame= [UIScreen mainScreen].bounds;
    float width =300;
    float x = 0;
    float y = 0;
    if (self.isShowBig) {
        
        width = 40;
        
        x = screenFrame.size.width-width -10;
        
        
    }else{
        if (IPHONE_X_CUSTOM) {
            // 刘海高度
            y = 30;
        }
        x = (screenFrame.size.width-width)/2.0;
    }
    self.isShowBig =!self.isShowBig;
    
    [self disPlayMarginLabelText];
    float height = self.isShowBig?25:20;
    CGRect newFrame = CGRectMake(x, 0, width, height);
   
    CGPoint newCenter =  CGPointMake(x + width/2.0, y + height/2.0);
    self.frame=newFrame;
    self.center = newCenter;
    _marginLabel.frame = self.bounds;

    
}
-(void)initData{

    self.screenUpdatesBeginTime = 0.0;
    self.averageScreenUpdatesTime = 0.017;

}
-(void)addNotification{
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationStateActiveAction:) name:UIApplicationStateActive object:nil];
}
-(void)addReceiveClassNameNotification{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addReceiveClassNameAction:) name:@"ClassNameNotification_key" object:nil];
}
-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)addReceiveClassNameAction:(NSNotification*)noti{

    if ([noti.object isKindOfClass:[NSString class]]) {
        
        self.className = noti.object;
        
    }else{
        self.className = nil;
    }

}
-(void)applicationStateActiveAction:(NSNotification*)noti{

    _marginLabel.backgroundColor = [UIColor greenColor];
    
}
-(void)initSelfUI{
    CGRect screenFrame= [UIScreen mainScreen].bounds;
    float width =40;
    float height =20;
    self.frame=CGRectMake(screenFrame.size.width-width -10, 0, width, height);
    self.backgroundColor = [UIColor clearColor];
    self.rootViewController=[[WBWBMarginViewController alloc]init];
    
    self.windowLevel = UIWindowLevelAlert;
    self.clipsToBounds=YES;
    self.layer.cornerRadius = 5.0;
    [self makeKeyAndVisible];
    
}


-(void)addLabel{

    _marginLabel=[[WBMarginLabel alloc]initWithFrame:self.bounds];
    _marginLabel.font=[UIFont systemFontOfSize:9];
    _marginLabel.textColor=[UIColor blueColor];
    _marginLabel.textAlignment = NSTextAlignmentCenter;
    _marginLabel.backgroundColor = [UIColor greenColor];
    _marginLabel.numberOfLines = 2;
    _marginLabel.hidden = YES;
    [self addSubview:_marginLabel];
    
}
-(void)addRunLoop{
    
    CADisplayLink* gameTimer;
    
    gameTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateDisplay:)];
    
    [gameTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}
- (void)updateDisplay:(CADisplayLink*)displayLink
{
   
    if (self.screenUpdatesBeginTime == 0.0) {
        
        self.screenUpdatesBeginTime = displayLink.timestamp;
        
    } else {
        _fpsCount ++;
        double  screenUpdatesTime = displayLink.timestamp - self.screenUpdatesBeginTime;
        
        if (screenUpdatesTime >= 1.0) {
            
            self.screenUpdatesBeginTime = displayLink.timestamp;
          
            float fps = _fpsCount / screenUpdatesTime;
            _fpsCount = 0;
            [self takeReadings:(int)round(fps)];
        }
    }
}

-(void)takeReadings:(NSInteger)fps{
    if (_marginLabel.hidden) {
        _marginLabel.hidden = NO;
    }
    
    self.fps = fps;
    
    if (!self.isShowBig) {
        
        _marginLabel.text=[NSString stringWithFormat:@"%.2f",[WBMarginTool memoryUsed]];
    
    }else{
    
        _marginLabel.text=[NSString stringWithFormat:@"memory: %.2fM cpu:%.2f%%  FPS:%ld system:%@ version:%@\n %@",[WBMarginTool memoryUsed],[WBMarginTool cpuUsage],(long)fps,[WBMarginTool systemVersion],[WBMarginTool appVersion],[self className]];
    }
    
}
-(void)disPlayMarginLabelText{

    if (!self.isShowBig) {
        
        _marginLabel.text=[NSString stringWithFormat:@"%.2f",[WBMarginTool memoryUsed]];
        
    }else{
        
        _marginLabel.text=[NSString stringWithFormat:@"memory: %.2fM cpu:%.2f%%  FPS:%f system:%@ version:%@\n %@",[WBMarginTool memoryUsed],[WBMarginTool cpuUsage],self.fps,[WBMarginTool systemVersion],[WBMarginTool appVersion],self.className];
    }
    
}

@end
