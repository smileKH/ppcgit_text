//
//  UIImage+Compress.h
//  eoa
//
//  Created by lb on 2018/6/15.
//  Copyright © 2018年 South China Market of Human Resources. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Compress)

-(NSData *)compressBySizeWithMaxLength:(NSUInteger)maxLength;

@end
