//
//  NSDictionary+ChangeNullToStr.m
//  demo
//
//  Created by 周郎 on 16/4/7.
//  Copyright © 2016年 周郎. All rights reserved.
//

#import "NSDictionary+ChangeNullToStr.h"

@implementation NSDictionary (ChangeNullToStr)


+(id)changeType:(id)myobj{
    if ([myobj isKindOfClass:[NSArray class]]) {
        return [self nullInArray:myobj];
    }else if ([myobj isKindOfClass:[NSDictionary class]]){
        return [self nullInDictionary:myobj];
    }else if ([myobj isKindOfClass:[NSString class]]){
        return [self stringToString:myobj];
    }else if ([myobj isKindOfClass:[NSNull class]]){
        return [self nullToString];
    }else{
        return myobj;
    }
}

//NULL转换成@""
+(NSString *)nullToString{
    return @"";
}
//字符串不变
+(NSString *)stringToString:(NSString *)str{
    return str;
}
//数组中的NULL转换成@""
+(NSArray *)nullInArray:(NSArray *)myArr{
    NSMutableArray *resetArr = [NSMutableArray array];
    
    for (id obj in myArr) {
        [resetArr addObject:[self changeType:obj]];
    }
    return resetArr;
}
//字典中的NULL转换成@""
+(NSDictionary *)nullInDictionary:(NSDictionary *)myDic{
    NSArray *keyArr = [myDic allKeys];
    NSMutableDictionary *resetDic = [NSMutableDictionary dictionary];
    for (int i = 0; i<keyArr.count; i++) {
        id obj = [myDic objectForKey:keyArr[i]];
        [resetDic setObject:[self changeType:obj] forKey:keyArr[i]];
    }
    return resetDic;
}

@end
