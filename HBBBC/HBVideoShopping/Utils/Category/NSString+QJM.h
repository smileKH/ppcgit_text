//
//  NSString+QJM.h
//  IStore
//
//  Created by carisok on 15/3/24.
//  Copyright (c) 2015年 carisok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (QJM)

- (NSDate *)to3339Date;
- (NSDictionary *)toDictionary;
- (NSString *)replaceString;

// 中文转UTF－8
- (NSString *)URLEncodedString;
// UTF－8转中文
- (NSString *)URLDecodedString;

// 计算字符串字节长度
- (NSInteger)getTextLength;
/**
 获取缓存路径
 
 @return 将当前字符串拼接到cache目录后面
 */
- (NSString *)cacheDic;
//随机数
-(NSString *)ret32bitString;
@end
