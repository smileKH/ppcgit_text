//
//  NSDictionary+NSDictionaryAddition.h
//  Skin
//
//  Created by YinJun on 16/6/24.
//  Copyright © 2016年 com.cn.*. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionaryAddition)

- (NSString *)convertIntoJson;

@end
