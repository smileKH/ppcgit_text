//
//  Is.m
//  APPS
//
//  Created by 小码 on 16/7/13.
//  Copyright © 2016年 HBiOS. All rights reserved.
//

#import "Is.h"

@implementation Is

void async(dispatch_block_t b) {
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        b();
    });
}

void async_safe(dispatch_block_t b) {
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        @try {
            b();
        }
        @catch (NSException *ex) {
            NSLog(@"未处理的异常:%@", ex);
        }
    });
}

void async_background_main(dispatch_block_t b1, dispatch_block_t b2) {
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        b1();
        dispatch_async(dispatch_get_main_queue(), b2);
    });
    
}

void Notify_S(NSString *msg,void(^style)(UIAlertActionStyle style))
{
//    async_main(^{
        
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:msg preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok  = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        style(action.style);
        
    }];
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        style(action.style);
    }];
    [alertController addAction:ok];
    [alertController addAction:cancle];

    UIViewController *controller =  [UIApplication sharedApplication].keyWindow.rootViewController;
    [controller presentViewController:alertController animated:YES completion:nil];
    
//    });
}


void Notify_I(NSString *msg ,void(^style)(UIAlertActionStyle style)) {
//    async_main(^{
    
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:msg preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok  = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            style(action.style);
            
        }];
        [alertController addAction:ok];
        UIViewController *controller =  [UIApplication sharedApplication].keyWindow.rootViewController;
        [controller presentViewController:alertController animated:YES completion:nil];
        
//    });

}

void Notify_NULL(NSString *msg) {
//    async_main(^{

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
        [controller presentViewController:alertController animated:YES completion:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alertController dismissViewControllerAnimated:YES completion:nil];
        });
//    });
    
}


void Notify_datePicker(UIDatePickerMode model, NSString *formatterString,void(^select)(NSString *dateString,NSDate *date))
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height/3)];
    picker.datePickerMode = model;
    [alertController.view addSubview:picker];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        select([Is formatter:formatterString date:picker.date],picker.date);
    }];
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:ok];
    [alertController addAction:cancle];
    
    UIWindow * window = [UIApplication sharedApplication].keyWindow;

    [window.rootViewController presentViewController:alertController animated:YES completion:nil];//
    
    

}
/*
 UUID
 */
NSString *newGuid() {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef str = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    NSString *str1 = [((__bridge_transfer NSString *) str) lowercaseString];
    return [str1 stringByReplacingOccurrencesOfString:@"-" withString:@""];
}


+(BOOL)isValidateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)validateIDCardNumber:(NSString *)value
{
    value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSUInteger length =0;
    if (!value) {
        return NO;
    }else {
        length = value.length;
        
        if (length !=15 && length !=18) {
            return NO;
        }
    }
    // 省份代码
    NSArray *areasArray =@[@"11",@"12", @"13",@"14", @"15",@"21", @"22",@"23", @"31",@"32", @"33",@"34", @"35",@"36", @"37",@"41", @"42",@"43", @"44",@"45", @"46",@"50", @"51",@"52", @"53",@"54", @"61",@"62", @"63",@"64", @"65",@"71", @"81",@"82", @"91"];
    
    NSString *valueStart2 = [value substringToIndex:2];
    BOOL areaFlag =NO;
    for (NSString *areaCode in areasArray) {
        if ([areaCode isEqualToString:valueStart2]) {
            areaFlag =YES;
            break;
        }
    }
    
    if (!areaFlag) {
        return NO;
    }
    
    
    NSRegularExpression *regularExpression;
    NSUInteger numberofMatch;
    
    int year =0;
    switch (length) {
        case 15:
            year = [value substringWithRange:NSMakeRange(6,2)].intValue +1900;
            
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            // [regularExpression release];
            
            if(numberofMatch >0) {
                return YES;
            }else {
                return NO;
            }
        case 18:
            
            year = [value substringWithRange:NSMakeRange(6,4)].intValue;
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            // [regularExpressionrelease];
            
            if(numberofMatch >0) {
                int S = ([value substringWithRange:NSMakeRange(0,1)].intValue + [value substringWithRange:NSMakeRange(10,1)].intValue) *7 + ([value substringWithRange:NSMakeRange(1,1)].intValue + [value substringWithRange:NSMakeRange(11,1)].intValue) *9 + ([value substringWithRange:NSMakeRange(2,1)].intValue + [value substringWithRange:NSMakeRange(12,1)].intValue) *10 + ([value substringWithRange:NSMakeRange(3,1)].intValue + [value substringWithRange:NSMakeRange(13,1)].intValue) *5 + ([value substringWithRange:NSMakeRange(4,1)].intValue + [value substringWithRange:NSMakeRange(14,1)].intValue) *8 + ([value substringWithRange:NSMakeRange(5,1)].intValue + [value substringWithRange:NSMakeRange(15,1)].intValue) *4 + ([value substringWithRange:NSMakeRange(6,1)].intValue + [value substringWithRange:NSMakeRange(16,1)].intValue) *2 + [value substringWithRange:NSMakeRange(7,1)].intValue *1 + [value substringWithRange:NSMakeRange(8,1)].intValue *6 + [value substringWithRange:NSMakeRange(9,1)].intValue *3;
                int Y = S %11;
                NSString *M =@"F";
                NSString *JYM =@"10X98765432";
                M = [JYM substringWithRange:NSMakeRange(Y,1)];// 判断校验位
                if ([M isEqualToString:[value substringWithRange:NSMakeRange(17,1)]]) {
                    return YES;// 检测ID的校验位
                }else {
                    return NO;
                }
                
            }else {
                return NO;
            }
        default:
            return NO;
    }
}


+(CGFloat)calulateHeightForOneLyricStr_SDK:(NSString *)lrcStr
                                  FontType:(UIFont *)fontType
                                  RowWidth:(CGFloat)rowWidth
{
    if ([Is EmptyOrNullString:lrcStr])
    {
        return 0;
    }
    CGSize size = CGSizeMake(rowWidth, 0);
    NSDictionary *attribute = @{NSFontAttributeName: fontType};
    CGSize sizeName  = [lrcStr boundingRectWithSize:size
                                            options:
                       NSStringDrawingTruncatesLastVisibleLine |
                         NSStringDrawingUsesLineFragmentOrigin |
                                NSStringDrawingUsesFontLeading
                                attributes:attribute context:nil].size;

    return sizeName.width+4;
}

+ (BOOL)EmptyOrNullString:(NSString *)str
{
    
    if (str == nil) return YES;
    
    if ([str isEqual:[NSNull null]]) return YES;
    
    if (![str isKindOfClass:[NSString class]]) return NO;
    
    return str.length == 0 || [str isEqualToString:@"<null>"];
}
+(BOOL)isEmptyOrNullDictionary:(NSDictionary *)dict
{
    if (dict == nil) return YES;
    if (dict.count == 0) return YES;
    if ([dict isEqual:[NSNull class]]) return YES ;
    
    return NO;
    
}
+ (BOOL)isPureInt:(NSString*)string
{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

+ (BOOL)checknumber:(NSString *)str
{
    
    if ([str length] == 0){
        
        return NO;
    }
    NSCharacterSet *disallowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm "] invertedSet];
    NSRange foundRange = [str rangeOfCharacterFromSet:disallowedCharacters];
    
    if (foundRange.location != NSNotFound) {

        return NO;
    }
    
    return YES;
}

+ (BOOL)isPureFloat:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    float val;
    return[scan scanFloat:&val] && [scan isAtEnd];
}

+(NSString *)isdatatime:(NSString *)string
             dataformat:(NSString *)datastring
             timeformat:(NSString *)timestring
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:datastring];//设置源时间字符串的格式
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"GMT+8"];//设置时区
    [formatter setTimeZone:timeZone];
    NSDate* date = [formatter dateFromString:string];
    
//    NSDateFormatter *formatters = [[NSDateFormatter alloc] init];
//    [formatters setDateStyle:NSDateFormatterMediumStyle];
//    [formatters setTimeStyle:NSDateFormatterShortStyle];
//    [formatters setDateFormat:timestring];//设置源时间字符串的格式
//    [formatter setTimeZone:timeZone];
    NSString * locationString=[formatter stringFromDate:date];
    
    return locationString;
}


+ (BOOL)checkTel:(NSString *)str
{
    if ([str length] == 0){
        return NO;
    }
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0-9])|(17[0,5-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:str];
    
    if (!isMatch) {
        return NO;
    }
    
    return YES;
}

+(UIImage *)decodeBase64ToImage:(NSString *)strEncodeData
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

+(BOOL)isrequestFinisheds:(id)reques
{
    if ([[reques class] isSubclassOfClass:NSString.class])  return YES;
    
    if ([[reques class] isSubclassOfClass:NSArray.class]){
        
        NSArray * array = (NSArray *)reques;
        if (array.count == 0) {
            return YES;
        }else
        {
            return NO;
        }
    }
    return YES;
}

/*
 *     判断字符串是否为纯数字
 */
- (BOOL)isNumber:(NSString *)string
{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    
    if ([scan scanInt:&val] && [scan isAtEnd]) {
        return YES;
    }else{
        return NO;
    }
}


+ (BOOL)dateCompare:(NSString *)early later:(NSString *)later
{
    //无论是何种形式的日期类型  转化为int后 特殊字符都会消失
    if ([early containsString:@"-"]) {
        early = [early stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    if ([early containsString:@":"]) {
        early = [early stringByReplacingOccurrencesOfString:@":" withString:@""];
    }
    if ([early containsString:@"/"]) {
        early = [early stringByReplacingOccurrencesOfString:@"/" withString:@""];
    }
    
    if ([later containsString:@"-"]) {
        later = [later stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    if ([later containsString:@":"]) {
        later = [later stringByReplacingOccurrencesOfString:@":" withString:@""];
    }
    if ([later containsString:@"/"]) {
        later = [later stringByReplacingOccurrencesOfString:@"/" withString:@""];
    }
    NSUInteger earlys = [early integerValue];
    NSUInteger laters = [later integerValue];
    
    if (earlys < laters) {//开始日期早于结束日期
        return YES;
    }else if(earlys > laters){
        return NO;
    }else{
        return YES;
    }
}

+ (BOOL)compareEarly:(NSDate *)early later:(NSDate *)later
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *oneDayStr = [dateFormatter stringFromDate:early];
    NSString *anotherDayStr = [dateFormatter stringFromDate:later];
    early = [dateFormatter dateFromString:oneDayStr];
    later = [dateFormatter dateFromString:anotherDayStr];
    NSComparisonResult reault =[early compare:later];
    if (reault == NSOrderedAscending ) {//上升
        return YES;
    }else if (reault == NSOrderedDescending){//下降
        return NO;//early是未来的  也就是说
    }else {
        //        if (reault == NSOrderedSame)
        return YES;
    }
    
}

+ (NSInteger)totalnum:(NSInteger)totalNum  loadNum:(NSInteger)loadNum
{
    return  (totalNum*loadNum)/100;
}
+ (NSString *)nowDate:(NSString *)loadDateString
{

    NSArray *array = [loadDateString componentsSeparatedByString:@" "];//对字符以空格分割
    
    NSString *first = array.firstObject;//年月日
    NSString *last = array.lastObject;//时分
    //如果输入的时间字符是当前的时间   、则为今天   zuotian  否则就是输入时间
    NSDate *date = [NSDate date];//获取当前时间
    
    NSDate *yestoday = [NSDate dateWithTimeInterval:-24*60*60 sinceDate:date];//昨天
    NSString *datestr;
    NSString *yestodayStr;
    if ([loadDateString containsString:@"-"]) {
        datestr = [self formatter:date];//字符串
        yestodayStr = [self formatter:yestoday];
    }else{
        datestr = [self formatter1:date];//字符串
        yestodayStr = [self formatter1:yestoday];
    }
    
    
    if ([first isEqualToString:datestr]) {
        return [NSString stringWithFormat:@"今天 %@",last];
    }
    if ([first isEqualToString:yestodayStr]) {
        return [NSString stringWithFormat:@"昨天 %@",last];
    }
    return loadDateString;
}
//+ (NSString *)nowDateWithMinute:(NSString *)loadDateString
//{
//    NSDate *date = [NSDate date];//获取当前时间
//    
//    NSDate *yestoday = [NSDate dateWithTimeInterval:-24*60*60 sinceDate:date];//昨天
//    
//    NSString *datestr = [self formatter:date];//字符串
//    NSString *yestodayStr = [self formatter:yestoday];
//}
//时间转换其
+ (NSString *)formatter:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    return [formatter stringFromDate:date];
}

+ (NSString *)formatter2:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [formatter stringFromDate:date];
}

+ (NSString *)formatter1:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy/MM/dd";
    return [formatter stringFromDate:date];
}

+ (NSString *)formatter:(NSString *)string date:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = string;
    return [formatter stringFromDate:date];
}

+ (NSString *)getMaxValueInArray:(NSArray *)array
{
    //取得两个值比较大的一个
    NSComparator cmptr = ^(id obj1, id obj2){
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    

    NSArray *arrays = [array sortedArrayUsingComparator:cmptr];
    NSString *max = [arrays lastObject];
    
    return max;
}

+ (NSString *)currentTimeString
{
   return  [Is formatter1:[NSDate date]];
}




+(BOOL)rightOrWrong:(NSDictionary *)dict
{
    //yes  right date
    //no  wrong date
    
    NSArray *keys = [dict allKeys];
    
    for (NSString *key in keys) {
        if ([key isEqualToString:@"errors"]) {
            return NO;
        }
    }
    
    return YES;

}

+ (BOOL)isOnline
{
//    if ([GETNETSTATUS isEqualToString:@"2"]) {
//        return NO;
//    }
    return YES;
}


//标准时间（0000）转化为设备当前所在时区的时间
+ (NSDate *)getNowDateFromatAnDate:(NSString *)anyDateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *anyDate = [formatter dateFromString:anyDateString];//把标准时间字符串  转化为date
    
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];//或GMT
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    
    return destinationDateNow;
}


+ (NSDictionary *)hanelDict:(NSDictionary *)dic
{
    NSMutableDictionary *dict  = [NSMutableDictionary dictionary];
    
    NSArray *keys = [dic allKeys];
    for (NSString *key in keys) {
        NSString *values = dic[key];
        if ([Is EmptyOrNullString:values]) {
            [dict setObject:@"" forKey:key];
        }else{
            [dict setObject:values forKey:key];
        }
    }
    return [NSDictionary dictionaryWithDictionary:dict];
    
}


#pragma mark   ------- 检测代码执行时间
/*
 //    NSDate* tmpStartData =[NSDate date] ;
 //    //You code here...
 //
 //    double deltaTime = [[NSDate date] timeIntervalSinceDate:tmpStartData];
 //    NSLog(@">>>>>>>>>>cost time = %f", deltaTime);
 
 */



+ (NSString *)toEnglish:(NSString *)mouthString
{
    int mouth = [mouthString intValue];
    
    if (mouth == 1) {
        return @"Jan";
    }else if (mouth == 2){
        return @"Feb";
    }else if (mouth == 3){
        return @"Mar";
    }else if (mouth == 4){
        return @"Apr";
    }else if (mouth == 5){
        return @"May";
    }else if (mouth == 6){
        return @"Jun";
    }else if (mouth == 7){
        return @"Jul";
    }else if (mouth == 8){
        return @"Aug";
    }else if (mouth == 9){
        return @"Sep";
    }else if (mouth == 10){
        return @"Oct";
    }else if (mouth == 11){
        return @"Nov";
    }else{
        return @"Dec";
    }
    
}


+ (NSString *)toWeek:(NSString *)dateString
{
    //首先把字符串转化为日期

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSDate *date = [formatter dateFromString:dateString];
    NSArray * arrWeek=[NSArray arrayWithObjects:@"NULL",@"SUN",@"MON",@"THE",@"WEN",@"THU",@"FRI",@"SAT", nil];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear |
    NSCalendarUnitMonth |
    NSCalendarUnitDay |
    NSCalendarUnitWeekday |
    NSCalendarUnitHour |
    NSCalendarUnitMinute |
    NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:date];
    NSInteger week = [comps weekday];
    //    NSInteger year=[comps year];
    //    NSInteger month = [comps month];
    //    NSInteger day = [comps day];
    //
    
    return  arrWeek[week];
    
}

+(CGFloat)textLengthWithString:(NSString *)string withFont:(UIFont *)font
{
    CGSize titleSize = [string sizeWithAttributes:@{NSFontAttributeName:font}];
    return titleSize.width + 30;
}

+ (NSString *)stringToTimeString:(NSString *)time
{
    NSDateFormatter *stampFormatter = [[NSDateFormatter alloc] init];
    [stampFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    //以 1970/01/01 GMT为基准，然后过了secs秒的时间
    NSDate *stampDate2 = [NSDate dateWithTimeIntervalSince1970:[time integerValue]];
    return  [stampFormatter stringFromDate:stampDate2];
}
@end
