//
//  StringAttribute.m
//  SH
//
//  Created by 小马 on 16/9/21.
//  Copyright © 2016年 Cloud. All rights reserved.
//

#import "StringAttribute.h"
#import "Is.h"
@implementation StringAttribute
+(NSAttributedString *)stringToAttributeString:(NSString *)string withRange:(NSRange)range withFont:(float)font
{
    if ([Is EmptyOrNullString:string]) {
        return nil;
    }else{
        
        NSMutableAttributedString *attributestring = [[ NSMutableAttributedString alloc] initWithString:string];
        [attributestring addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:font] range:range];
        return attributestring;
    }
    
}


+(NSAttributedString *)stringToAttributeString:(NSString *)string withRange:(NSRange)range withFont:(float)font withColor:(UIColor *)color
{
    if ([Is EmptyOrNullString:string]) {
        return nil;
    }else{
        
        NSMutableAttributedString *attributestring = [[ NSMutableAttributedString alloc] initWithString:string];
        [attributestring addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:font] range:range];
        [attributestring addAttribute:NSForegroundColorAttributeName value:color range:range];
        return attributestring;
    }
 
}
//三个字符
+(NSAttributedString *)stringToAttributeString:(NSString *)headStr andBodystring:(NSString *)bodyStr withFont:(float)font withColor:(UIColor *)color
{
    //    if (headStr.length == 0 && bodyStr.length == 0) {
    //        return nil;
    //    }
    if ([Is EmptyOrNullString:headStr]) {
        headStr = @"";
    }
    if ([Is EmptyOrNullString:bodyStr]) {
        bodyStr = @"";
    }
    NSString *string = [NSString stringWithFormat:@"%@%@",headStr,bodyStr];
    NSMutableAttributedString *attributestring = [[ NSMutableAttributedString alloc] initWithString:string];
    [attributestring addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:font] range:NSMakeRange(headStr.length, bodyStr.length)];
    [attributestring addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(headStr.length, bodyStr.length)];
    return attributestring;
}

+(NSAttributedString *)titstring:(NSString *)str1  and:(NSString *)string2
{
    
    NSString *string= [NSString stringWithFormat:@"%@ %@",str1,string2];
    NSMutableAttributedString *attributestring = [[ NSMutableAttributedString alloc] initWithString:string];
    [attributestring addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:14] range:NSMakeRange(0, 3)];
    [attributestring addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(4, string.length-4)];
    return attributestring;
    
    
}

//三个字符串
+(NSAttributedString *)stringToAttributeString:(NSString *)headStr andBodystring:(NSString *)bodyStr andFootstrings:(NSString *)footStr withFont:(float)font withColor:(UIColor *)color
{
//    if (headStr.length == 0 && bodyStr.length == 0) {
//        return nil;
//    }
    if ([Is EmptyOrNullString:headStr]) {
        headStr = @"";
    }
    if ([Is EmptyOrNullString:bodyStr]) {
        bodyStr = @"";
    }
    if ([Is EmptyOrNullString:footStr]) {
        footStr = @"";
    }
    NSString *string = [NSString stringWithFormat:@"%@%@%@",headStr,bodyStr,footStr];
    NSMutableAttributedString *attributestring = [[ NSMutableAttributedString alloc] initWithString:string];
    [attributestring addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:font] range:NSMakeRange(headStr.length, bodyStr.length)];
    
    [attributestring addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(headStr.length, bodyStr.length)];
    return attributestring;
}


+(NSAttributedString *)stringToAttributeString:(NSString *)firstString andSecond:(NSString *)secondString andThird:(NSString *)thirdString withSecondFont:(float)font withSecondColor:(UIColor *)color
{
    if ([Is EmptyOrNullString:firstString]) {
        firstString = @"";
    }
    if ([Is EmptyOrNullString:secondString]) {
        secondString = @"";
    }
    if ([Is EmptyOrNullString:thirdString]) {
        thirdString = @"";
    }
    
    NSString *string = [NSString stringWithFormat:@"%@%@%@",firstString,secondString,thirdString];
    NSMutableAttributedString *attributestring = [[ NSMutableAttributedString alloc] initWithString:string];
    
    [attributestring addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:font] range:NSMakeRange(firstString.length, secondString.length)];
    [attributestring addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(firstString.length, secondString.length)];
    
    return attributestring;
}

+(NSString *)toMoney:(NSString *)string
{

    if ([Is EmptyOrNullString:string]) {
        return @"0.00";
    }
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    [formatter setPositiveFormat:@"#,###.00"];
    formatter.numberStyle = NSNumberFormatterRoundDown;
    return  [formatter stringFromNumber:[NSNumber numberWithDouble:[string doubleValue]]];
}
+(NSString *)toStrFromMoney:(NSString *)str{
    str  = [str stringByReplacingOccurrencesOfString:@"US$" withString:@""];
    str  = [str stringByReplacingOccurrencesOfString:@"￥" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
    return str;
}
+(NSString *)toSquareMeter:(NSString *)string{
    if ([Is EmptyOrNullString:string]) {
        return @"0.00m²";
    }
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    return  [NSString stringWithFormat:@"%@m²",[formatter stringFromNumber:[NSNumber numberWithDouble:[string doubleValue]]]];
    
}

//过滤掉特殊字符
+(NSString *)deleteSpacCharacters:(NSString *)string{
    NSMutableString *responseString = [NSMutableString stringWithString:string];
    NSString *character = nil;
    for (int i = 0; i < responseString.length; i ++) {
        character = [responseString substringWithRange:NSMakeRange(i, 1)];
        if ([character isEqualToString:@"|"])
           [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"0"])
            [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"1"])
          [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"2"])
            [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"3"])
           [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"4"])
           [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"5"])
            [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"6"])
            [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"7"])
           [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"8"])
            [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
        if ([character isEqualToString:@"9"])
           [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];

    }
    for (int i = 0; i < responseString.length; i ++) {
        character = [responseString substringWithRange:NSMakeRange(i, 1)];
        if ([character isEqualToString:@"0"])
            [responseString replaceCharactersInRange:NSMakeRange(i, 1) withString:@""];
       
    }
    [responseString deleteCharactersInRange:NSMakeRange(0, 1)];
    
    return responseString;

}

+ (NSAttributedString *)stringColor:(NSArray *)colorArray  stringFont:(NSArray *)fontArray string:(NSArray *)stringArray;
{

    NSString *string = [stringArray componentsJoinedByString:@""];
     NSMutableAttributedString *attributestring = [[ NSMutableAttributedString alloc] initWithString:string];
    if (colorArray.count != stringArray.count || fontArray.count != stringArray.count) {
        return attributestring;
    }
    NSInteger loc = 0;
    for (NSInteger i = 0; i < stringArray.count; i ++) {
        NSString *str = stringArray[i];
    
        [attributestring addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:[fontArray[i] integerValue]] range:NSMakeRange(loc, str.length)];
        [attributestring addAttribute:NSForegroundColorAttributeName value:colorArray[i] range:NSMakeRange(loc, str.length)];
        loc = loc+str.length;
    }
    
    return attributestring;
    
}
@end
