//
//  NSDictionary+ChangeNullToStr.h
//  demo
//
//  Created by 周郎 on 16/4/7.
//  Copyright © 2016年 周郎. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ChangeNullToStr)

//把json中包含的NULL数据转换成@""
+(id)changeType:(id)myobj;

@end
