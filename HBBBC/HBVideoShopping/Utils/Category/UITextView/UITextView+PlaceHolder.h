//
//  UITextView+PlaceHolder.h
//  FengHeJia
//
//  Created by han on 2015/07/16.
//  Copyright (c) 2015年 韩赵凯. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface UITextView (PlaceHolder) <UITextViewDelegate>

@property (strong, nonatomic) UITextView *placeHolderTextView;
/**
 *输入的最大长度，0为不限制长度
 */
@property (strong, nonatomic) NSNumber* maxLength;
- (void)addPlaceHolder:(NSString *)placeHolder;

@end
