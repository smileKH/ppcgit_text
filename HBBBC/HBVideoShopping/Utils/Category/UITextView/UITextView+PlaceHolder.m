//
//  UITextView+PlaceHolder.m
//  FengHeJia
//
//  Created by han on 2015/07/16.
//  Copyright (c) 2015年 韩赵凯. All rights reserved.
//

#import "UITextView+PlaceHolder.h"
#import "UIColor+YYAdd.h"
static const char *phTextView = "placeHolderTextView";
static const char *textMaxLength = "textMaxLength";
@implementation UITextView (PlaceHolder)

-(NSNumber*)maxLength
{
   return  objc_getAssociatedObject(self, textMaxLength);;
}

-(void)setMaxLength:(NSNumber*)maxLength
{
    objc_setAssociatedObject(self, textMaxLength, maxLength, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UITextView *)placeHolderTextView
{
    return objc_getAssociatedObject(self, phTextView);
}

- (void)setPlaceHolderTextView:(UITextView *)placeHolderTextView
{
    objc_setAssociatedObject(self, phTextView, placeHolderTextView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)addPlaceHolder:(NSString *)placeHolder
{
    if (![self placeHolderTextView])
    {
        self.delegate = self;
        UITextView *textView = [[UITextView alloc] initWithFrame:self.bounds];
        textView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        textView.font = self.font;
        textView.backgroundColor = [UIColor clearColor];
        textView.textColor = [UIColor grayColor];
        textView.userInteractionEnabled = NO;
        textView.text = placeHolder;
        [self addSubview:textView];
        [self setPlaceHolderTextView:textView];
    }
}
# pragma mark -
# pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.placeHolderTextView.hidden = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""])
    {
        self.placeHolderTextView.hidden = NO;
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (!self.maxLength)
    {
        return YES;
    }
    //修复文本框摇一摇撤销越界闪退
    if (textView.text.length < range.location + range.length) {
        textView.text=@"";
    
        [app showToastView:@"超出最大字数"];
        return NO;
    }
    if (textView.text.length + text.length > [self.maxLength integerValue]) {
        NSString*allString = [NSString stringWithFormat:@"%@%@",textView.text,text];
        textView.text=[allString substringToIndex:[self.maxLength integerValue]];
        [app showToastView:@"超出最大字数"];
        return NO;
    }
    
    return YES;
}

@end
