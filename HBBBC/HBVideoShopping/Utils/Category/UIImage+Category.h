//
//  UIImage+Category.h
//  ShengXianSearch
//
//  Created by 曾浩 on 2017/3/6.
//  Copyright © 2017年 曾浩. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Category)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
/**
 
 生成二维码
 
 QRStering：字符串
 
 imageFloat：二维码图片大小
 
 */

+ (UIImage *)createQRCodeWithString:(NSString *)QRStering withImgSize:(CGFloat)imageFloat;
/**
 
 生成二维码(中间有小图片)
 
 QRStering：字符串
 
 centerImage：二维码中间的image对象
 
 */

+ (UIImage *)createImgQRCodeWithString:(NSString *)QRString centerImage:(UIImage *)centerImage;
@end
