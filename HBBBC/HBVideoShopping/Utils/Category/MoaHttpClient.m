//
//  MoaHttpClient.m
//  MOAProject
//
//  Created by 周郎 on 2018/8/9.
//

#import "MoaHttpClient.h"
//#import "NSString+MD5Addition.h"
#import "UIImage+Compress.h"
#import "NSDictionary+NSDictionaryAddition.h"
#import "NSDictionary+ChangeNullToStr.h"

#define LOCAL_SAVE_PATH @"nfrcDownloadFileDic"
/**
 *  存放 网络请求的线程
 */
static NSMutableArray *sg_requestTasks;


@implementation MoaHttpClient

static MoaHttpClient * webUtil = nil;

+ (instancetype )shardManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        webUtil = [[[self class] alloc] init];
        webUtil.networkStatus = AFNetworkReachabilityStatusReachableViaWWAN;
        [webUtil setupNetworkReachability];
    });
    
    return webUtil;
}

+ (void)setSharedURLCache{
    NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024 diskCapacity:30  * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:cache];
}

+ (NSString *)autoMainUrlString{
    return Config_baseUrl;
}

- (void)setupNetworkReachability{
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        // 当网络状态改变了, 就会调用这个block
        self.networkStatus = status;
        if (status == AFNetworkReachabilityStatusReachableViaWiFi) {
            
            NSLog(@"已连接Wi-Fi网络");
            
        }else if (status == AFNetworkReachabilityStatusReachableViaWWAN){
            
            NSLog(@"已连接蜂窝移动网络");
            
        }else if (status == AFNetworkReachabilityStatusUnknown){
            
            NSLog(@"已连接未知网络");
            
        }else if (status == AFNetworkReachabilityStatusNotReachable){
            
            NSLog(@"没有网络(断网)");
            
        }
    }];
    [mgr startMonitoring];
}

#pragma mark - AFnetworking manager getter

- (AFHTTPSessionManager *)createAFHTTPSessionManager{
    
    NSString *mainURLString = [MoaHttpClient autoMainUrlString];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainURLString]];
    //设置请求参数的类型:HTTP (AFJSONRequestSerializer,AFHTTPRequestSerializer)
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [self setDefaultSettingWithSessionManager:manager];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    return manager;
}

- (void)setDefaultSettingWithSessionManager:(AFHTTPSessionManager *)manager{
   // NSString *sysVersion = [[UIDevice currentDevice] systemVersion]; //获取系统版本 例如：9.2
   // NSString *deviceUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; //获取设备唯一标识符 例如：FBF2306E-A0D8-4F4B-BDED-9333B627D3E6
//    NSString *deviceModel = [[UIDevice currentDevice] deviceString]; //获取设备的型号 例如：iPhone
    //NSString *appVersion = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    
    [manager.requestSerializer setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    //设置请求的超时时间
    manager.requestSerializer.timeoutInterval = 30.f;
    //设置服务器返回结果的类型:JSON (AFJSONResponseSerializer,AFHTTPResponseSerializer)
    
//    [manager.requestSerializer setValue:deviceModel forHTTPHeaderField:@"PHONEMODEL"];
//    [manager.requestSerializer setValue:deviceUUID forHTTPHeaderField:@"UUID"];
//    [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"OS"];
//    [manager.requestSerializer setValue:sysVersion forHTTPHeaderField:@"OS_VERSION"];
//    [manager.requestSerializer setValue:appVersion forHTTPHeaderField:@"APP_VERSION"];
//    [manager.requestSerializer setValue:[NSString stringWithFormat:@"iOS_MOA_%@",appVersion] forHTTPHeaderField:@"T_VERSION"];
    NSString *token = USERDEFAULT_value(TOKEN_XY_APP);
//    NSLog(@"打印一下token=====%@",token);
    if (token && token.length>0) {
        [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    }else{
//        [manager.requestSerializer setValue:@"abc" forHTTPHeaderField:@"Authorization"];
    }
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"application/x-www-form-urlencoded", nil];
}

- (NSMutableArray *)allTasks{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sg_requestTasks = [[NSMutableArray alloc] init];
    });
    
    return sg_requestTasks;
}

- (void)cancelRequestWithURL:(NSString *)url {
    
    if (url == nil) {
        return;
    }
    
    @synchronized(self) {
        [[self allTasks] enumerateObjectsUsingBlock:^(MOAURLSessionTask * _Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([task isKindOfClass:[MOAURLSessionTask class]]
                && [task.currentRequest.URL.absoluteString hasSuffix:url]) {
                [task cancel];
                [[self allTasks] removeObject:task];
                return;
            }
        }];
    };
}

- (void)cancelAllRequest {
    @synchronized(self) {
        [[self allTasks] enumerateObjectsUsingBlock:^(MOAURLSessionTask * _Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([task isKindOfClass:[MOAURLSessionTask class]]) {
                [task cancel];
            }
        }];
        
        [[self allTasks] removeAllObjects];
    };
}
#pragma mark GET请求
- (MOAURLSessionTask *)getRequestURLString:(NSString *)url_path
                                parameters:(id)parameters
                                   success:(RequestSuccessBlock)success
                                     error:(RequestErrorBlock)errorBlock
                                      fail:(RequestFailureBlock)fail{
    
    
    NSString *paraStr = [parameters convertIntoJson];
    NSLog(@"%@\n请求参数：%@",url_path,paraStr);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager];
    if (self.networkStatus == AFNetworkReachabilityStatusNotReachable) {
        //离线时
        [manager.requestSerializer setCachePolicy:NSURLRequestReturnCacheDataDontLoad];
    }else{
        [manager.requestSerializer setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    }
    
    MOAURLSessionTask *session=nil;
    session = [manager GET:url_path parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self showCustomMessageWithResponseObject:responseObject success:success error:errorBlock fail:fail];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUD];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (fail) {
            fail(error);
        }else{
            [MBProgressHUD showErrorMessage:MSG_Failure];
        }
        
    }];
    
    if (session) {
        [[self allTasks] addObject:session];
    }
    
    return session;
}
#pragma mark POST请求

- (MOAURLSessionTask *)newPostRequestURLString:(NSString *)url_path
                                    parameters:(id)parameters
                                       success:(RequestSuccessBlock)success
                                         error:(RequestErrorBlock)errorBlock
                                          fail:(RequestFailureBlock)fail{
    
    
    NSString *paraStr = [parameters convertIntoJson];
    NSLog(@"%@\n请求参数：%@",url_path,paraStr);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager];
    
    MOAURLSessionTask *session=nil;
    
    session = [manager POST:url_path parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self showCustomMessageWithResponseObject:responseObject success:success error:errorBlock fail:fail];
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUD];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (fail) {
            fail(error);
        }else{
            [MBProgressHUD showErrorMessage:MSG_Failure];
        }
        
    }];
    
    if (session) {
        [[self allTasks] addObject:session];
    }
    
    return session;
}


#pragma mark 图片、文件上传
- (void)uploadImageWithPath:(NSString *)path
                     params:(NSDictionary *)params
                     photos:(NSArray *)photos
                    success:(RequestSuccessBlock)success
                      error:(RequestErrorBlock)errorBlock
                       fail:(RequestFailureBlock)fail
{
    
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"multipart/form-data;boundary=%@",@"POST_BOUNDS123"] forHTTPHeaderField:@"Content-Type"];
    [manager POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < photos.count; i ++) {
            NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
            formatter.dateFormat=@"yyyyMMddHHmmss";
            //            NSString *str=[formatter stringFromDate:[NSDate date]];
            //            NSString *fileName=[NSString stringWithFormat:@"%@.png",str];
            UIImage *image = photos[i];
            NSData *imageData = [image compressBySizeWithMaxLength:2*1024*1024];
            
            [formData appendPartWithFileData:imageData
                                        name:[NSString stringWithFormat:@"header%d",i+1]
                                    fileName:[NSString stringWithFormat:@"header%d.jpg",i+1]
                                    mimeType:@"image/jpg"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"总大小：%lld,当前大小:%lld",uploadProgress.totalUnitCount,uploadProgress.completedUnitCount);
        CGFloat progressFloat = uploadProgress.completedUnitCount*1.0/uploadProgress.totalUnitCount;
        [MBProgressHUD showActivityMessageInWindow:@"上传中"];
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self showCustomMessageWithResponseObject:responseObject success:success error:errorBlock fail:fail];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        fail(error);
    }];
}


- (MOAURLSessionTask *)uploadVedioWithUrl:(NSString *)url
                              WithParams:(NSDictionary*)params
                                   image:(NSData *)vedioData
                                filename:(NSString *)name
                                mimeType:(NSString *)mimetype
                                  success:(RequestSuccessBlock)success
                                    error:(RequestErrorBlock)errorBlock
                                     fail:(RequestFailureBlock)fail
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager];
    
    MOAURLSessionTask *operation = [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.mp4",str];
        [formData appendPartWithFileData:vedioData name:name fileName:fileName mimeType:mimetype];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self showCustomMessageWithResponseObject:responseObject success:success error:errorBlock fail:fail];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUD];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        errorBlock(error);
    }];
    
    return operation;
}

- (MOAURLSessionTask *)uploadErrorTxtWithUrl:(NSString *)url
                                 WithParams:(NSDictionary*)params
                                       text:(NSData *)imageData
                                   filename:(NSString *)name
                                    mimeType:(NSString *)mimetype
                                     success:(RequestSuccessBlock)success
                                       error:(RequestErrorBlock)errorBlock
                                        fail:(RequestFailureBlock)fail
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager];
    //    [SVProgressHUD show];
    MOAURLSessionTask *operation = [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.txt",str];
        
        // 上传图片，以文件流的格式
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:mimetype];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self showCustomMessageWithResponseObject:responseObject success:success error:errorBlock fail:fail];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBProgressHUD hideHUD];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        errorBlock(error);
        
    }];
    return operation;
    
}

#pragma mark -Private Method-
- (void)showCustomMessageWithResponseObject:(NSDictionary *)responseObject
                                    success:(RequestSuccessBlock)success
                                      error:(void (^)(id errorMsg))errorBlock
                                       fail:(void (^)(NSError *error))fail{
    
    [MBProgressHUD hideHUD];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *responseDic = responseObject;
        
        responseDic = [NSDictionary changeType:responseObject];
        
        NSDictionary *statusDic = responseDic[@"status"];
        if (![statusDic isKindOfClass:[NSDictionary class]]) {
            [MBProgressHUD showErrorMessage:MSG_Error_Undefine];
            return;
        }
        NSString *code = statusDic[@"code"];
        NSString *message = statusDic[@"message"];
        
        id entry = responseDic[@"data"];
        
        NSString *responseStr;
        NSData *data = [NSJSONSerialization dataWithJSONObject:responseDic options:NSJSONWritingPrettyPrinted error:nil];
        responseStr =  [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        //    NSLog(@"类型转换-请求结果:%@",responseStr);
        printf("请求结果：%s\n", [[NSString stringWithFormat:@"%@", responseStr] UTF8String]);
        switch ([code integerValue]) {
            case 1:
                //成功  返回数据（如有）
                if (success) {
                    success(entry,message);
                }
                break;
            case 2:
                //微信登陆（算他成功）
                if (success) {
                    success(entry,code);
                }
                break;
            case -1:
                //服务器异常
                if (errorBlock) {
                    errorBlock(message);
                }else{
                    [MBProgressHUD showErrorMessage:MSG_Error_Undefine];
                }
                break;
            case 0:
                //请求失败
                if (errorBlock) {
                    errorBlock(message);
                }else{
                    [MBProgressHUD showErrorMessage:message];
                }
                
                break;
            case 4:
                //token已过期 刷新token
//                [userManager requestUserInfoData];
                
                break;
            case 5:
                //被挤下线
                [MBProgressHUD showErrorMessage:Session_Error];
//                [AppShare logoutOperation];
                break;
            default:
                //未知错误
            {
                NSString *errorStr = MSG_Error_Undefine;
                if (errorBlock) {
                    errorBlock(errorStr);
                }else{
                    [MBProgressHUD showErrorMessage:errorStr];
                }
            }
                break;
        }
        
        
    }else{
        [MBProgressHUD showErrorMessage:MSG_Error_Undefine];
    }
    
}


//根据文件类型、名字、创建时间获得本地文件的路径
+ (NSString *)getLocalFilePathWithFileName:(NSString *)fileName{
    
    NSString *dirPath = [self getLocalFileSaveDirectoryPath];
    NSString *path = [dirPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", fileName]];
    
    return path;
    
}

//获取存储的文件夹路径
+ (NSString *)getLocalFileSaveDirectoryPath{
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dirPath = [cachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", LOCAL_SAVE_PATH]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:dirPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    
    return dirPath;
}

//根据url判断是否已经保存到本地了
+ (BOOL)isSavedFileToLocalFileName:(NSString *)fileName{
    // 判断是否已经离线下载了
    NSString *path = [self getLocalFilePathWithFileName:fileName];
    NSFileManager *filemanager = [NSFileManager defaultManager];
    if ([filemanager fileExistsAtPath:path]) {
        return YES;
    }
    return NO;
}

@end
