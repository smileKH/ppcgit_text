//
//  UIImage+Category.m
//  ShengXianSearch
//
//  Created by 曾浩 on 2017/3/6.
//  Copyright © 2017年 曾浩. All rights reserved.
//

#import "UIImage+Category.h"

@implementation UIImage (Category)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}
/**
 
 生成二维码
 
 QRStering：字符串
 
 imageFloat：二维码图片大小
 
 */

+ (UIImage *)createQRCodeWithString:(NSString *)QRString withImgSize:(CGFloat)imageFloat{
    
    CIFilter *filter = [CIFilter filterWithName:@"XiaoGuiGe"];
    
    [filter setDefaults];
    
    NSString *getString = QRString;
    
    NSData *dataString = [getString dataUsingEncoding:NSUTF8StringEncoding];
    
    [filter setValue:dataString forKey:@"inputMessage"];
    
    ///获取滤镜输出的图像
    
    CIImage *outImage = [filter outputImage];
    
    UIImage *imageV = [self imageWithImageSize:imageFloat withCIIImage:outImage];
    
    //返回二维码图像
    
    return imageV;
    
}
/**
 
 生成二维码(中间有小图片)
 
 QRStering：所需字符串
 
 centerImage：二维码中间的image对象
 
 */

+ (UIImage *)createImgQRCodeWithString:(NSString *)QRString centerImage:(UIImage *)centerImage{
    
    // 创建滤镜对象
    
    CIFilter *filter = [CIFilter filterWithName:@"XiaoGuiGe"];
    
    // 恢复滤镜的默认属性
    
    [filter setDefaults];
    
    // 将字符串转换成 NSdata
    
    NSData *dataString = [QRString dataUsingEncoding:NSUTF8StringEncoding];
    
    // 设置过滤器的输入值, KVC赋值
    
    [filter setValue:dataString forKey:@"inputMessage"];
    
    // 获得滤镜输出的图像
    
    CIImage *outImage = [filter outputImage];
    
    // 图片小于(27,27),我们需要放大
    
    outImage = [outImage imageByApplyingTransform:CGAffineTransformMakeScale(20, 20)];
    
    // 将CIImage类型转成UIImage类型
    
    UIImage *startImage = [UIImage imageWithCIImage:outImage];
    
    // 开启绘图, 获取图形上下文
    
    UIGraphicsBeginImageContext(startImage.size);
    
    
    
    // 把二维码图片画上去 (这里是以图形上下文, 左上角为(0,0)点
    
    [startImage drawInRect:CGRectMake(0, 0, startImage.size.width, startImage.size.height)];
    
    // 再把小图片画上去
    
    CGFloat icon_imageW = 200;
    
    CGFloat icon_imageH = icon_imageW;
    
    CGFloat icon_imageX = (startImage.size.width - icon_imageW) * 0.5;
    
    CGFloat icon_imageY = (startImage.size.height - icon_imageH) * 0.5;
    
    [centerImage drawInRect:CGRectMake(icon_imageX, icon_imageY, icon_imageW, icon_imageH)];
    
    // 获取当前画得的这张图片
    
    UIImage *qrImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 关闭图形上下文
    
    UIGraphicsEndImageContext();
    
    //返回二维码图像
    
    return qrImage;
    
}
/** 将CIImage转换成UIImage 并放大(内部转换使用)*/

+ (UIImage *)imageWithImageSize:(CGFloat)size withCIIImage:(CIImage *)ciiImage{
    
    CGRect extent = CGRectIntegral(ciiImage.extent);
    
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    
    size_t width = CGRectGetWidth(extent) * scale;
    
    size_t height = CGRectGetHeight(extent) * scale;
    
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    
    CIContext *context = [CIContext contextWithOptions:nil];
    
    CGImageRef bitmapImage = [context createCGImage:ciiImage fromRect:extent];
    
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    
    CGContextScaleCTM(bitmapRef, scale, scale);
    
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    
    CGContextRelease(bitmapRef);
    
    CGImageRelease(bitmapImage);
    
    return [UIImage imageWithCGImage:scaledImage];
    
}
@end
