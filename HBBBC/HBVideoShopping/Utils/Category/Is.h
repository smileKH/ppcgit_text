//
//  Is.h
//  APPS
//
//  Created by 小码 on 16/7/13.
//  Copyright © 2016年 HBiOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Is : NSObject
//async
void async(dispatch_block_t b);

void async_safe(dispatch_block_t b);

void async_background_main(dispatch_block_t b1, dispatch_block_t b2);

void async_main(dispatch_block_t b);

void Notify_S(NSString *msg ,void(^style)(UIAlertActionStyle style));

void Notify_I(NSString *msg ,void(^style)(UIAlertActionStyle style));

void Notify_NULL(NSString *msg);

void Notify_datePicker(UIDatePickerMode model, NSString *formatterString,void(^select)(NSString *dateString,NSDate *date));

/*
 UUID
 */
NSString *newGuid();

/*
 身份证号码校验
 */
+ (BOOL)validateIDCardNumber:(NSString *)value;

/*
    邮箱格式判断
 */
+(BOOL)isValidateEmail:(NSString *)email;

/*
    计算高度
 */
+(CGFloat)calulateHeightForOneLyricStr_SDK:(NSString *)lrcStr  FontType:(UIFont *)fontType RowWidth:(CGFloat)rowWidth;

/*
 字符串是否为空
 */
+(BOOL)EmptyOrNullString:(NSString *)str;

+(BOOL)isEmptyOrNullDictionary:(NSDictionary *)dict;
/*
 判断是否为int
 */
+ (BOOL)isPureInt:(NSString*)string;

/*
    是否是数字和字母组合
 */
+ (BOOL)checknumber:(NSString *)str;

/*
 判断是否为float
 */
+ (BOOL)isPureFloat:(NSString*)string;

/*
 时间格式转换
 */
+(NSString *)isdatatime:(NSString *)string dataformat:(NSString *)datastring timeformat:(NSString *)timestring;
+ (NSString *)formatter:(NSString *)string date:(NSDate *)date;

/**
    手机号码判断
 */
+ (BOOL)checkTel:(NSString *)str;

/*
    字符串转图片
 */
+(UIImage *)decodeBase64ToImage:(NSString *)strEncodeData;


//判断返回的数据类型
+(BOOL)isrequestFinisheds:(id)reques;


//判断字符串是否为纯数字
- (BOOL)isNumber:(NSString *)string;

+ (NSString *)currentTimeString;

//判断日期的先后
+ (BOOL)dateCompare:(NSString *)early later:(NSString *)later;
+ (BOOL)compareEarly:(NSDate *)early later:(NSDate *)later;
+ (NSString *)formatter:(NSDate*)date;
+ (NSString *)formatter2:(NSDate*)date;
//以100为单位    先计算出比例  在计算出实际位置  (进度条)
+ (NSInteger)totalnum:(NSInteger)totalNum  loadNum:(NSInteger)loadNum;

//根据时间字符串得出 （今天，昨天   和以前）
+ (NSString *)nowDate:(NSString *)loadDateString;//控制到日期

//获取数组 最大值
+ (NSString *)getMaxValueInArray:(NSArray *)array;

//判断是正确返回还是错误返回
+(BOOL)rightOrWrong:(NSDictionary *)dict;

+ (BOOL)isOnline;

+ (NSDate *)getNowDateFromatAnDate:(NSString *)anyDateString;

//如果字典的值为NSNull  返回为一个@""字符
+ (NSDictionary *)hanelDict:(NSDictionary *)dic;

+ (NSString *)toEnglish:(NSString *)mouthString;
+ (NSString *)toWeek:(NSString *)dateString;

/*
 
 计算单行文本文字长度
*/

+(CGFloat)textLengthWithString:(NSString *)string withFont:(UIFont *)font;

+(NSString *)stringToTimeString:(NSString *)time;


@end
