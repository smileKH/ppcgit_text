//
//  XH_Date_String.h
//  XH_WORK
//
//  Created by 献华 付 on 15/9/6.
//  Copyright (c) 2015年 xianhua Fu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XH_Date_String : NSObject
/**
 *  当前时间字符串
 *
 *  @param format 时间格式 @"yyyy-MM-dd HH:mm:ss"
 *
 *  @return 时间字符串
 */
+ (NSString *)dateStringWithFormat:(NSString *)format;
//
/**
 *  用规格:时间获取正确时区的时间字符串
 *
 *  @param format 时间格式 @"yyyy-MM-dd HH:mm:ss"
 *  @param date   时间
 *
 *  @return 时间字符串
 */
+ (NSString *)dateStringWithFormat:(NSString *)format date:(NSDate *)date;
/**
 *  时间字符串 获取规格的时间字符串
 *
 *  @param format 时间格式
 *  @param date   时间
 *
 *  @return 时间字符串
 */
+ (NSString *)dateStringWithFormat:(NSString *)format dateString:(NSString *)date;
//
/**
 *  时间 距离现在的天数
 *
 *  @param date 时间
 *
 *  @return 返回天数
 */
+ (int)daysFromDate:(NSDate *)date;
//获取聊天时间
/**
 *  获取距离当前的时间
 *
 *  @param date 日期
 *
 *  @return 日期-昨天-前天-2015-01-01
 */
+ (NSString *)dateStringForChat:(NSDate *)date;
//
/**
 *  时间戳转换成时间字符串
 *
 *  @param format    日期格式
 *  @param timestamp 时间戳
 *
 *  @return 时间字符串
 */
+ (NSString *)dateStringWithFormat:(NSString *)format timestamp:(NSTimeInterval)timestamp;


//时间转时间戳
+(NSString *)dateStringForDate:(NSDate *)date;

//传入NSNumber 然后变成时间字符串
+ (NSString *)dateStringWithFormat:(NSString *)format number:(NSNumber *)number;

//十位转换
+ (NSString *)dateStringWithFormat:(NSString *)format tenNumber:(NSNumber *)number;


+ (NSString *)dateStringWithStringFormat:(NSString *)format dateString:(NSString *)string;

//用NSString转NSDte
+(NSDate *)dateFormat:(NSString *)format dateString:(NSString *)string;
//判断一个月有多少天
+ (NSInteger)howManyDaysInThisYear:(NSInteger)year withMonth:(NSInteger)month;
/**
 *  ** 在当前日期时间加上 某个时间段(传负数即返回当前时间之前x月x日的时间)
 *
 *  @param year   当前时间若干年后 （传负数为当前时间若干年前）
 *  @param month  当前时间若干月后  （传0即与当前时间一样）
 *  @param day    当前时间若干天后
 *  @param hour   当前时间若干小时后
 *  @param minute 当前时间若干分钟后
 *  @param second 当前时间若干秒后
 *
 *  @return 处理后的时间字符串
 */
+ (NSString *)dateStringAfterlocalDateForYear:(NSInteger)year Month:(NSInteger)month Day:(NSInteger)day Hour:(NSInteger)hour Minute:(NSInteger)minute Second:(NSInteger)second;
#pragma mark - 获取当前时间的 时间戳
+(NSInteger)getNowTimestamp;
#pragma mark - 将某个时间转化成 时间戳

+(NSString *)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format;

//将NSDate类型的时间转换为时间戳 转换成毫秒,从1970/1/1开始

+(long long)getDateTimeTOMilliSeconds:(NSDate *)datetime;
//格式转换
+(NSString *)endStringFormat:(NSString *)endStr andStateStringFormat:(NSString *)stateStr andValueStr:(NSString *)valueStr;
@end
