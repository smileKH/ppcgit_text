//
//  MoaHttpClient.h
//  MOAProject
//
//  Created by 周郎 on 2018/8/9.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#define Loading_Msg         @"正在加载"
#define Session_Error       @"登录信息已过期，请重新登录"
#define Session_Failue      @"登录信息失效，请重新登录"
#define MSG_Error_Undefine  @"请求失败"
#define MSG_Failure         @"请求失败"
#define MSG_NoPermission    @"没有访问权限"
//
//typedef void (^transferValue) (id obj,NSError* error);

/** 请求成功的Block */
typedef void(^RequestSuccessBlock)(id dic, NSString *message);

/** 请求失败的Block */
typedef void(^RequestErrorBlock)(id errorMsg);

typedef void(^RequestFailureBlock)(NSError *error);

/** 请求任务 */
typedef NSURLSessionTask MOAURLSessionTask;

@interface MoaHttpClient : NSObject

@property (nonatomic)AFNetworkReachabilityStatus networkStatus;

+ (instancetype )shardManager;


/**
 设置缓存大小
 */
+ (void)setSharedURLCache;


/**
 获取接口地址

 @return 接口地址
 */
+ (NSString *)autoMainUrlString;



/**
 *
 *    取消所有请求
 */
- (void)cancelAllRequest;

/**
 *
 *    取消某个请求。如果是要取消某个请求，最好是引用接口所返回来的HYBURLSessionTask对象，
 *  然后调用对象的cancel方法。如果不想引用对象，这里额外提供了一种方法来实现取消某个请求
 *
 *    @param url                URL，可以是绝对URL，也可以是path（也就是不包括baseurl）
 */
- (void)cancelRequestWithURL:(NSString *)url;


/**
 提交类型为Json的GET请求方法

 @param url_path 请求地址
 @param parameters 请求参数
 @param success 请求成功，返回状态成功block
 @param errorBlock 请求成功，返回状态失败block
 @param fail 请求失败block
 @return 请求体
 */
- (MOAURLSessionTask *)getRequestURLString:(NSString *)url_path
                                parameters:(id)parameters
                                   success:(RequestSuccessBlock)success
                                     error:(RequestErrorBlock)errorBlock
                                      fail:(RequestFailureBlock)fail;
/**
 提交类型为Json的请求方法
 
 @param url_path 请求地址
 @param parameters 请求参数
 @param success 请求成功，返回状态成功block
 @param errorBlock 请求成功，返回状态失败block
 @param fail 请求失败block
 @return 请求体
 */
- (MOAURLSessionTask *)newPostRequestURLString:(NSString *)url_path
                                    parameters:(id)parameters
                                       success:(RequestSuccessBlock)success
                                         error:(RequestErrorBlock)errorBlock
                                          fail:(RequestFailureBlock)fail;

/**
 多图上传
 
 @param path 请求url
 @param params 参数
 @param photos 图片数组
 @param success 文件上传成功的回调
 @param errorBlock 文件上传错误的回调
 @param fail 文件上传失败的回调
 */
- (void)uploadImageWithPath:(NSString *)path
                     params:(NSDictionary *)params
                     photos:(NSArray *)photos
                    success:(RequestSuccessBlock)success
                      error:(RequestErrorBlock)errorBlock
                       fail:(RequestFailureBlock)fail;



/**
 上传音视频文件
 
 @param url 请求url
 @param params 参数
 @param vedioData 要上传的文件流
 @param name 文件名
 @param mimetype mimetype
 @param success 成功回调
 @param errorBlock 错误回调
 @param fail 失败回调
 @return 返回task
 */
- (MOAURLSessionTask *)uploadVedioWithUrl:(NSString *)url
                               WithParams:(NSDictionary*)params
                                    image:(NSData *)vedioData
                                 filename:(NSString *)name
                                 mimeType:(NSString *)mimetype
                                  success:(RequestSuccessBlock)success
                                    error:(RequestErrorBlock)errorBlock
                                     fail:(RequestFailureBlock)fail;


- (MOAURLSessionTask *)uploadErrorTxtWithUrl:(NSString *)url
                                  WithParams:(NSDictionary*)params
                                        text:(NSData *)imageData
                                    filename:(NSString *)name
                                    mimeType:(NSString *)mimetype
                                     success:(RequestSuccessBlock)success
                                       error:(RequestErrorBlock)errorBlock
                                        fail:(RequestFailureBlock)fail;



//根据文件类型、名字、创建时间获得本地文件的路径
+ (NSString *)getLocalFilePathWithFileName:(NSString *)fileName;

//获取存储的文件夹路径
+ (NSString *)getLocalFileSaveDirectoryPath;

//根据url判断是否已经保存到本地了
+ (BOOL)isSavedFileToLocalFileName:(NSString *)fileName;

@end
