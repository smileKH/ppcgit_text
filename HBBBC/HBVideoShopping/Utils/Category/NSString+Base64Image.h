//
//  NSString+Base64Image.h
//  BBSStore
//
//  Created by 马云龙 on 2018/2/1.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base64Image)
-(UIImage *)toBase64Image;

@end
