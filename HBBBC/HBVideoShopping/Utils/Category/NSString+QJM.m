//
//  NSString+QJM.m
//  IStore
//
//  Created by carisok on 15/3/24.
//  Copyright (c) 2015年 carisok. All rights reserved.
//

#import "NSString+QJM.h"

@implementation NSString (QJM)

- (NSDate *)to3339Date {
    if(!self) return nil;
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [rfc3339DateFormatter dateFromString: self];
    return date;
}

- (NSDictionary *)toDictionary {
    NSData *data = [self dataUsingEncoding: NSUTF8StringEncoding];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return dict;
}
/**
 获取缓存路径
 
 @return 将当前字符串拼接到cache目录后面
 */
- (NSString *)cacheDic
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    return [path stringByAppendingPathComponent:self.lastPathComponent];
}

- (NSString *)replaceString {
    NSString *reString = [self stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    reString = [reString stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    reString = [reString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    reString = [reString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    reString = [reString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    reString = [reString stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    reString = [reString stringByReplacingOccurrencesOfString:@"&cent;" withString:@"￠"];
    reString = [reString stringByReplacingOccurrencesOfString:@"&pound;" withString:@"£"];
    reString = [reString stringByReplacingOccurrencesOfString:@"&yen;" withString:@"¥"];
    reString = [reString stringByReplacingOccurrencesOfString:@"&euro;" withString:@"€"];
    
    return reString;
}

// 中文转UTF－8（编码）
- (NSString *)URLEncodedString
{
    //    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)self,NULL,CFSTR("!*'();:@&=+$,/?%#[]"),kCFStringEncodingUTF8));
    //    return result;
    
    NSString *result = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

// UTF－8转中文（解码）
- (NSString *)URLDecodedString
{
    //    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,(CFStringRef)self, CFSTR(""),kCFStringEncodingUTF8));
    //    return result;
    
    NSString *result = [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}


// 计算字符串字节长度
- (NSInteger)getTextLength {
    
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSData *da = [self dataUsingEncoding:enc];
    
    return da.length;
}

-(NSString *)ret32bitString
{
    char data[32];
    for (int x=0;x<32;data[x++] = (char)('A' + (arc4random_uniform(26))));
    return [[NSString alloc] initWithBytes:data length:32 encoding:NSUTF8StringEncoding];
}

@end
