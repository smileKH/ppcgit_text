//
//  NSDictionary+NSDictionaryAddition.m
//  Skin
//
//  Created by YinJun on 16/6/24.
//  Copyright © 2016年 com.cn.*. All rights reserved.
//

#import "NSDictionary+NSDictionaryAddition.h"

@implementation NSDictionary (NSDictionaryAddition)

- (NSString *)convertIntoJson
{
    return [self buildJsonEncode:NSUTF8StringEncoding];
}

- (NSString *)buildJsonEncode:(NSStringEncoding)encode
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:self options:0 error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:encode];
    return jsonStr;
}

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString{
    if (!jsonString) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *err;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if (err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}


@end
