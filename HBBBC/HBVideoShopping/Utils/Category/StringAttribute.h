//
//  StringAttribute.h
//  SH
//
//  Created by 小马 on 16/9/21.
//  Copyright © 2016年 Cloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface StringAttribute : NSObject
+(NSAttributedString *)stringToAttributeString:(NSString *)string withRange:(NSRange)range withFont:(float)font;
+(NSAttributedString *)stringToAttributeString:(NSString *)string withRange:(NSRange)range withFont:(float)font withColor:(UIColor *)color;


+(NSAttributedString *)stringToAttributeString:(NSString *)headStr andBodystring:(NSString *)bodyStr withFont:(float)font withColor:(UIColor *)color;
+(NSAttributedString *)stringToAttributeString:(NSString *)headStr andBodystring:(NSString *)bodyStr andFootstrings:(NSString *)footStr withFont:(float)font withColor:(UIColor *)color;

+(NSString *)toMoney:(NSString *)string;
+(NSString *)toStrFromMoney:(NSString *)str;
//转换平方米的格式
+(NSString *)toSquareMeter:(NSString *)string;
//去除特殊0-9，|符号
+(NSString *)deleteSpacCharacters:(NSString *)string;

+(NSAttributedString *)stringToAttributeString:(NSString *)firstString andSecond:(NSString *)secondString andThird:(NSString *)thirdString withSecondFont:(float)font withSecondColor:(UIColor *)color;


+(NSAttributedString *)titstring:(NSString *)str1  and:(NSString *)string2;


+ (NSAttributedString *)stringColor:(NSArray *)colorArray  stringFont:(NSArray *)fontArray string:(NSArray *)stringArray;

@end
