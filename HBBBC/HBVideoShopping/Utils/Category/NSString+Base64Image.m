//
//  NSString+Base64Image.m
//  BBSStore
//
//  Created by 马云龙 on 2018/2/1.
//  Copyright © 2018年 马云龙. All rights reserved.
//

#import "NSString+Base64Image.h"

@implementation NSString (Base64Image)
-(UIImage *)toBase64Image
{
    if (self.length< 30) {
        return nil;
    }
    NSData *imageData =[[NSData alloc] initWithBase64EncodedString: [self substringWithRange:NSMakeRange(22, self.length-22)] options:0];
    return [UIImage imageWithData:imageData] ;
}
@end
