//
//  NSString+videoURl.h
//  HBVideoShopping
//
//  Created by hua on 2018/4/15.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (videoURl)
- (UIImage*) thumbnailImageForVideo:(NSString *)videoURLStr atTime:(NSTimeInterval)time;
@end
