//
//  XH_Date_String.m
//  XH_WORK
//
//  Created by 献华 付 on 15/9/6.
//  Copyright (c) 2015年 xianhua Fu. All rights reserved.
//

#import "XH_Date_String.h"

@implementation XH_Date_String
//用规格:当前时间字符串
+ (NSString *)dateStringWithFormat:(NSString *)format{
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    //[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *dateString = [dateFormatter stringFromDate:now];
    return  dateString;
}
//用规格:时间获取正确时区的时间字符串
+ (NSString *)dateStringWithFormat:(NSString *)format date:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    //[dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    //[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return  dateString;
}
//时间字符串 获取规格的时间字符串
+ (NSString *)dateStringWithFormat:(NSString *)format dateString:(NSString *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:format];
    NSDate *fDate = [formatter dateFromString:date];
    return [formatter stringFromDate:fDate];
}
//时间 距离现在的天数
+ (int)daysFromDate:(NSDate *)date{
    NSCalendarUnit unit = NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //    NSDateComponents *start = [gregorian components:unit fromDate:[NSDate date]];
    //    NSDateComponents *end = [gregorian components:unit fromDate:date];
    
    return (int)[[gregorian components:unit fromDate:date toDate:[NSDate date] options:0] day];
}
//获取聊天时间
+ (NSString *)dateStringForChat:(NSDate *)date{
    int days = [self daysFromDate:date];
    NSString *dateString;
    if (days == 0) {
        dateString = [self dateStringWithFormat:@"HH:mm" date:date];
    }else if (days == 1) {
        dateString = [NSString stringWithFormat:@"昨天 %@",[self dateStringWithFormat:@"HH:mm" date:date]];
    }else if (days == 2) {
        dateString = [NSString stringWithFormat:@"前天 %@",[self dateStringWithFormat:@"HH:mm" date:date]];
    }else {
        dateString = [self dateStringWithFormat:@"yyyy-MM-dd HH:mm" date:date];
    }
    return dateString;
}
//时间戳转换成时间字符串
+ (NSString *)dateStringWithFormat:(NSString *)format timestamp:(NSTimeInterval)timestamp{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp/1000];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    return [dateFormatter stringFromDate:date];
}

//时间转时间戳
+(NSString *)dateStringForDate:(NSDate *)date{

    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
    
    return timeSp;
}

+ (NSString *)dateStringWithFormat:(NSString *)format number:(NSNumber *)number{

    NSString*str=nil;//时间戳
    if (![number isKindOfClass:[NSNumber class]]) {
        str=(NSString*)number;//时间戳
    }
    else{
       str=[number stringValue];//时间戳
    }
    
    
    NSTimeInterval time =[str doubleValue]+28800;//因为时差问题要加8小时 == 28800 sec
    
    NSDate*detaildate=[NSDate dateWithTimeIntervalSince1970:time/1000];
    
    //实例化一个NSDateFormatter对象
    
    NSDateFormatter*dateFormatter = [[NSDateFormatter alloc]init];
    
    //设定时间格式,这里可以设置成自己需要的格式
    
    [dateFormatter setDateFormat:format];
    
    NSString*currentDateStr = [dateFormatter stringFromDate:detaildate];
    
    return currentDateStr;
}
//十位转换
+ (NSString *)dateStringWithFormat:(NSString *)format tenNumber:(NSNumber *)number{
    
    NSString*str=nil;//时间戳
    if (![number isKindOfClass:[NSNumber class]]) {
        str=(NSString*)number;//时间戳
    }
    else{
        str=[number stringValue];//时间戳
    }
    NSTimeInterval time =[str doubleValue]+28800;//因为时差问题要加8小时 == 28800 sec
    
    NSDate*detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    
    //实例化一个NSDateFormatter对象
    
    NSDateFormatter*dateFormatter = [[NSDateFormatter alloc]init];
    
    //设定时间格式,这里可以设置成自己需要的格式
    
    [dateFormatter setDateFormat:format];
    
    NSString*currentDateStr = [dateFormatter stringFromDate:detaildate];
    
    return currentDateStr;
}
+ (NSString *)dateStringWithStringFormat:(NSString *)format dateString:(NSString *)string{

    id result;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    result=[formatter numberFromString:string];
    if(result)
    {
        double timesTamp = [string doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timesTamp/1000];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:format];
        //[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        
        //[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
        NSString*confromTimespStr = [dateFormatter stringFromDate:date];
        
        return confromTimespStr;
        
    }
    return nil;
}
//用NSString转NSDte
+(NSDate *)dateFormat:(NSString *)format dateString:(NSString *)string{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat: format];
    
    NSDate *destDate= [dateFormatter dateFromString:string];
    
    return destDate;

    
}
#pragma mark - 获取某年某月的天数
+ (NSInteger)howManyDaysInThisYear:(NSInteger)year withMonth:(NSInteger)month{
    if((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12))
        return 31 ;
    
    if((month == 4) || (month == 6) || (month == 9) || (month == 11))
        return 30;
    
    if((year % 4 == 1) || (year % 4 == 2) || (year % 4 == 3))
    {
        return 28;
    }
    
    if(year % 400 == 0)
        return 29;
    
    if(year % 100 == 0)
        return 28;
    
    return 29;
}
/**
 *  ** 在当前日期时间加上 某个时间段(传负数即返回当前时间之前x月x日的时间)
 *
 *  @param year   当前时间若干年后 （传负数为当前时间若干年前）
 *  @param month  当前时间若干月后  （传0即与当前时间一样）
 *  @param day    当前时间若干天后
 *  @param hour   当前时间若干小时后
 *  @param minute 当前时间若干分钟后
 *  @param second 当前时间若干秒后
 *
 *  @return 处理后的时间字符串
 */
+ (NSString *)dateStringAfterlocalDateForYear:(NSInteger)year Month:(NSInteger)month Day:(NSInteger)day Hour:(NSInteger)hour Minute:(NSInteger)minute Second:(NSInteger)second
{
    // 当前日期
    NSDate *localDate = [NSDate date]; // 为伦敦时间
    // 在当前日期时间加上 时间：格里高利历
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponent = [[NSDateComponents alloc]init];
    
    [offsetComponent setYear:year ];  // 设置开始时间为当前时间的前x年
    [offsetComponent setMonth:month];
    [offsetComponent setDay:day];
    [offsetComponent setHour:(hour+8)]; // 中国时区为正八区，未处理为本地，所以+8
    [offsetComponent setMinute:minute];
    [offsetComponent setSecond:second];
    // 当前时间后若干时间
    NSDate *minDate = [gregorian dateByAddingComponents:offsetComponent toDate:localDate options:0];
    
    NSString *dateString = [NSString stringWithFormat:@"%@",minDate];
    
    return dateString;
}
#pragma mark - 获取当前时间的 时间戳

+(NSInteger)getNowTimestamp{

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间
    
    
    
    NSLog(@"设备当前的时间:%@",[formatter stringFromDate:datenow]);
    
    //时间转时间戳的方法:
    
    
    
    NSInteger timeSp = [[NSNumber numberWithDouble:[datenow timeIntervalSince1970]] integerValue];
    
    
    
    NSLog(@"设备当前的时间戳:%ld",(long)timeSp); //时间戳的值
    
    
    
    return timeSp;
    
}
#pragma mark - 将某个时间转化成 时间戳

+(NSString *)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format{
    
    
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    
//    [formatter setDateStyle:NSDateFormatterMediumStyle];
//    
//    [formatter setTimeStyle:NSDateFormatterShortStyle];
//    
//    [formatter setDateFormat:format]; //(@"YYYY-MM-dd hh:mm:ss") ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
//    
//    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
//    
//    [formatter setTimeZone:timeZone];
//    
//    
//    
//    NSDate* date = [formatter dateFromString:formatTime]; //------------将字符串按formatter转成nsdate
//    
//    //时间转时间戳的方法:
//    
//    NSInteger timeSp = [[NSNumber numberWithDouble:[date timeIntervalSince1970]] integerValue];
//    
//    
//    
//    NSLog(@"将某个时间转化成 时间戳&&&&&&&timeSp:%ld",(long)timeSp); //时间戳的值
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH-mm-sss"];
    
    NSDate *resDate = [formatter dateFromString:formatTime];
    
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[resDate timeIntervalSince1970]];
    
    return timeSp;
    
}
//将NSDate类型的时间转换为时间戳 转换成毫秒,从1970/1/1开始

+(long long)getDateTimeTOMilliSeconds:(NSDate *)datetime

{
    
    NSTimeInterval interval = [datetime timeIntervalSince1970];
    
//    NSLog(@"转换的时间戳=%f",interval);
    
    long long totalMilliseconds = interval*1000 ;
    
//    NSLog(@"totalMilliseconds=%llu",totalMilliseconds);
    
    return totalMilliseconds;
    
}

+(NSString *)endStringFormat:(NSString *)endStr andStateStringFormat:(NSString *)stateStr andValueStr:(NSString *)valueStr{
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:stateStr];
    
    //    NSString *dateString=[NSString stringWithFormat:valueStr];
    //
    //    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    
    NSDate *date =[dateFormat dateFromString:valueStr];
    
    NSDateFormatter* dateFormat2 = [[NSDateFormatter alloc] init];
    
    [dateFormat2 setDateFormat:endStr];
    
    NSString *publishtimeStr = [dateFormat2 stringFromDate:date ];
    
    return publishtimeStr;
}

@end
