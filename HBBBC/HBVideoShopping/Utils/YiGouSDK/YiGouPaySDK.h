//
//  YiGouPaySDK.h
//  YiGouPaySDK
//
//  Created by 程伟 on 2017/5/8.
//  Copyright © 2017年 程伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "YiGouPaySDKObject.h"

@protocol ZLPayApiDelegate <NSObject>
@optional


/*! @brief 发送一个sendReq后，收到易购支付的回应
 *
 * 收到一个来自易购支付的处理结果。调用一次sendReq后会收到onResp。
 * @param resp 具体的回应内容
 */
- (void)yiGouPayOnResp:(YiGouPayBaseResp *)resp;


@end

@interface YiGouPaySDK : NSObject


/*! @brief 判断易购商城app是否安装
 *
 * @return 支持返回YES，不支持返回NO。
 */
+ (BOOL)isZLAppInstall;
/*! @brief 获取众联商城的itunes安装地址
 *
 * @return 众联商城的安装地址字符串。
 */
+ (NSString *)getZLAppInstallUrl;

/*! @brief 处理易购支付通过URL启动App时传递的数据
 *
 * 需要在 application:openURL:sourceApplication:annotation:或者application:handleOpenURL中调用。iOS 9.0以后在application:openURL:options:
 中调用
 * @param url 易购支付启动第三方应用时传递过来的URL
 * @param delegate  ZLPayApiDelegate对象，用来接收易购支付触发的消息。
 * @return 成功返回YES，失败返回NO。
 */
+ (BOOL)handleOpenYiGouPayURL:(NSURL *)url delegate:(id<ZLPayApiDelegate>)delegate;

/*! @brief 发送请求到易购支付，等待易购支付返回onResp
 *
 * 函数调用后，会切换到易购支付的界面。第三方应用程序等待易购支付返回onResp。易购支付在异步处理完成后一定会调用onResp。支持以下类型
 * @param req 具体的发送请求，在调用函数后，请自己释放。
 * @return 成功返回YES，失败返回NO。
 */
+ (BOOL)sendYiGouPayReq:(YiGouPayReq *)req;


@end
