//
//  YiGouPaySDKObject.h
//  YiGouPaySDK
//
//  Created by 程伟 on 2017/5/8.
//  Copyright © 2017年 程伟. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! @brief 错误码
 *
 */
enum  YGErrCode {
    YGSuccess           = 1,    /**< 成功    */
    YGErrCodeUserCancel = -1,   /**< 用户点击取消并返回    */
    YGErrCodeCommon     = -2,   /**< 支付失败    */
    
};


@interface YiGouPayBaseResp : NSObject

/** 错误码  */
@property (nonatomic,assign) int errCode;
/** 错误提示字符串  */
@property (nonatomic,copy) NSString *errDescription;


@end


@interface YiGouPayReq : NSObject

/** 商家向易购支付申请的商家id */
@property (nonatomic, copy) NSString *miKey;
/** 商户产品应用标识 */
@property (nonatomic, copy) NSString *mpLabel;
/** 随机串，防重发 */
@property (nonatomic, copy) NSString *nonceStr;
/** 预支付id  */
@property (nonatomic,copy) NSString *prepayId;
/** 预支付结果  */
@property (nonatomic,copy) NSString *resultCode;
/** 预支付结果描述  */
@property (nonatomic,copy) NSString *resultDescription;
/** 商家根据易购开放平台文档对数据做的签名 */
@property (nonatomic, copy) NSString *sign;

@end
