//
//  AppDelegate+JPush.h
//  JPushTestFxh
//
//  Created by hua on 2018/4/25.
//  Copyright © 2018年 weimob. All rights reserved.
//

#import "AppDelegate.h"
static NSString *appKey = @"bf37076bda8d2f6a06fb3617";
static NSString *channel = @"App Store";
static BOOL isProduction = FALSE;
// 引入JPush功能所需头文件
#import "JPUSHService.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
@interface AppDelegate (JPush)<JPUSHRegisterDelegate>
-(void)initJPush:(NSDictionary *)launchOptions;
- (void)JPushApplication:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void)JPushApplication:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void)JPushApplicationDidEnterBackground:(UIApplication *)application;
- (void)JPushApplicationWillEnterForeground:(UIApplication *)application;
// ios 7 以上收到通知
- (void)JPushApplication:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
// ios 6 以上收到通知
- (void)JPushApplication:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (void)JPushApplication:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification;
@end
