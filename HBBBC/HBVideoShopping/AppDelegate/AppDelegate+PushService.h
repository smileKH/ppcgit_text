//
//  AppDelegate+PushService.h
//  MiAiApp
//
//  Created by 徐阳 on 2017/5/25.
//  Copyright © 2017年 徐阳. All rights reserved.
//

#import "AppDelegate.h"
#define ReplaceRootViewController(vc) [[AppDelegate shareAppDelegate] replaceRootViewController:vc]
/**
 推送相关在这里处理
 */
@interface AppDelegate (PushService)
//初始化友盟推送
-(void)initUMengMessageWithOptions:(NSDictionary *)launchOptions;
@end
