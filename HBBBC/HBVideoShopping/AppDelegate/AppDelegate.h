//
//  AppDelegate.h
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/17.
//  Copyright © 2017年 FirstVision. All rights reserved.
//
//账号密码：admin
//admin123

#import <UIKit/UIKit.h>
#import "HBMainTabBarController.h"
// fxh
#import "WBMarginViewWindow.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    UILabel                 *_noticeInfoLabel;
}

@property (strong, nonatomic) UIWindow *window;
/**
 *  添加memory cup使用情况 fxh
 */
@property (strong, nonatomic) UIWindow *marginViewWindow;

@property (strong, nonatomic)HBMainTabBarController *mainTabBar;

//没有网络的时候或者出错的时候显示
- (void)showToastView:(NSString *)str;
//判断是不是iPhoneX
- (BOOL)isNewIphoneX;
- (UIViewController *)getNowViewController;
//获取当前界面（有navigation的时候）
- (UIViewController *)getNowViewController;
@end

