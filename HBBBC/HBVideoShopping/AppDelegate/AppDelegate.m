//
//  AppDelegate.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/17.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+JPush.h"
#import "AppDelegate+PushService.h"
#import "AppDelegate+Bugly.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import "AppDelegate+ShareService.h"
//#import "AppDelegate+ShareService.h"
//#import "UMessage.h"
//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 100000
//#import <UserNotifications/UserNotifications.h>
//#endif
////以下几个库仅作为调试引用引用的
//#import <AdSupport/AdSupport.h>
//#import <CommonCrypto/CommonDigest.h>
//@interface AppDelegate ()<UNUserNotificationCenterDelegate>
//#import "AppDelegate+JPush.h"
//高德地图
#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "PFAPP.h"
@interface AppDelegate ()<MAMapViewDelegate,AMapSearchDelegate>
@property (strong, nonatomic) MAMapView *mapView;
@property (strong,nonatomic)AMapSearchAPI *search;
@end
AppDelegate *app = nil;
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // fxh
//    [self addMarginViewWindow];
    
    //初始化window
    [self initWindow];
    //设置全局APP
    app = self;
    
    //初始化友盟推送
    [self initUMengMessageWithOptions:launchOptions];
    
    //UMeng初始化
    [self initUMeng];
    //极光分享初始化
    [self initJSHAREService];
    // JPush初始化
    [self initJPush:launchOptions];
    // Bugly
    [self initBugly];
    //请求配置
    [self initRequestService];
    
    //初始化app服务
    [self initService];
    
    //注册微信支付
    [self registerWXAndPayApp];
    
    //初始化IM
    //    [[IMManager sharedIMManager] initIM];
    
    //初始化用户系统
    [self initUserManager];
    
    //网络监听
    [self monitorNetworkStatus];
    

    //高德地图
    [AMapServices sharedServices].apiKey = @"3ce6bc4f16b93eaf23ff319bca441ab8";
    [[AMapServices sharedServices] setEnableHTTPS:YES];
    self.mapView = [[MAMapView alloc] init];
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
    //搜索对象，逆地理编码
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    
    //启动页
    //    [HBAppManager example];
    //改变状态栏颜色
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    return YES;
}
//高德地图
-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if (updatingLocation) {
        //取出当前位置坐标
        //  NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
        
        double longitude = userLocation.coordinate.longitude;
        double latitude = userLocation.coordinate.latitude;
        
        //WGS坐标转GCJ02
        //        NSMutableArray *arr=[LocationUtls WgsConvertToGcj:latitude andWgs_lng:longitude];
        //        CLLocation *mycurLocaton=[[CLLocation alloc]initWithLatitude:[arr[0] doubleValue] longitude:[arr[1] doubleValue]];
        
        //        [PFAPP sharedInstance].lat = mycurLocaton.coordinate.latitude;
        //        [PFAPP sharedInstance].lng = mycurLocaton.coordinate.longitude;
        [PFAPP sharedInstance].lat = latitude;
        [PFAPP sharedInstance].lng = longitude;
        
        self.mapView.showsUserLocation = NO;
        
        
        //请求逆地理编码
        AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
        
        regeo.location                    = [AMapGeoPoint locationWithLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
        regeo.requireExtension            = YES;
        
        [self.search AMapReGoecodeSearch:regeo];
        
        
    }
    
}
/* 逆地理编码回调. */
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if (response.regeocode != nil)
    {
        //解析response获取地址描述，具体解析见 Demo
        NSString *strAddress = [NSString stringWithFormat:@"%@ %@",response.regeocode.addressComponent.province,response.regeocode.addressComponent.city];
        NSLog(@"%@",strAddress);
        
        [PFAPP sharedInstance].Province = response.regeocode.addressComponent.province;
        if ([response.regeocode.addressComponent.city isEqualToString:@""]||response.regeocode.addressComponent.city==nil) {
            //区
            [PFAPP sharedInstance].City = response.regeocode.addressComponent.district;
        }
        else
        {
            //市
            [PFAPP sharedInstance].City = response.regeocode.addressComponent.city;
        }
        
        
        [PFAPP sharedInstance].address = strAddress;
        
    }
}

//定位将要启动时调用该接口
-(void)mapViewWillStartLocatingUser:(MAMapView *)mapView
{
    if (![CLLocationManager locationServicesEnabled]) {
        //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"定位失败" message:@"请再手机设置中开启定位功能" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        //        [alertView show];
        
        UIAlertController *alret=[UIAlertController alertControllerWithTitle:@"定位失败" message:@"请再手机设置中开启定位功能" preferredStyle:UIAlertControllerStyleAlert];
        [alret addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }]];
        self.mapView = nil;
        self.mapView.delegate = nil;
        return;
    }
    else
    {
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
            
            UIAlertController *alret=[UIAlertController alertControllerWithTitle:@"定位失败" message:@"请再手机设置中开启定位功能" preferredStyle:UIAlertControllerStyleAlert];
            
            [alret addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
            }]];
            self.mapView = nil;
            self.mapView.delegate = nil;
            return;
        }
    }
}
/**
 *  添加memory cup使用情况
 */
- (void)addMarginViewWindow
{
#ifdef DEBUG
    WBMarginViewWindow *marginViewWindow=[[WBMarginViewWindow alloc]init];
    self.marginViewWindow = marginViewWindow;
#else
    
#endif
    
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    //[self JPushApplication:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
#pragma mark --🍎--🙏-- 实现注册APNs失败接口（可选
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //[self JPushApplication:application didFailToRegisterForRemoteNotificationsWithError:error];
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    //[self JPushApplicationWillEnterForeground:application];
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    //[self JPushApplicationDidEnterBackground:application];
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler {
    
    //[self JPushApplication:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    //[self JPushApplication:application didReceiveRemoteNotification:userInfo];
}
// 本地接收到通知
- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
    //[self JPushApplication:application didReceiveLocalNotification:notification];
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //刚开始活跃的时候
    //[HBAppManager appStart];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)isNewIphoneX
{
    if (CGRectEqualToRect([UIScreen mainScreen].bounds,CGRectMake(0, 0, 375, 812))) {
        return YES;
    } else {
        return NO;
    }
}
//提示内容
- (void)showToastView:(NSString *)str
{
    if (str==nil || str==NULL) {
        return;
    }
    if ([str isKindOfClass:[NSNull class]]) {
        return ;
    }
    if ([str length] == 0)
    {
        return;
    }
    //    [_noticeInfoLabel removeAllSubviews];
    _noticeInfoLabel = nil;
    _noticeInfoLabel = [[UILabel alloc] init];
    _noticeInfoLabel.frame = CGRectMake((SCREEN_WIDTH-100)/2, SCREEN_HEIGHT/2, 100, 150);
    _noticeInfoLabel.textAlignment = NSTextAlignmentCenter;
    _noticeInfoLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    _noticeInfoLabel.numberOfLines = 5;
    _noticeInfoLabel.textColor = [UIColor whiteColor];
    _noticeInfoLabel.layer.cornerRadius = 4;
    _noticeInfoLabel.layer.masksToBounds = YES;
    _noticeInfoLabel.backgroundColor = [UIColor colorWithRed:66/255.0 green:66/255.0 blue:66/255.0 alpha:1.0];
    
    _noticeInfoLabel.text = str;
    CGSize size = EM_MULTILINE_TEXTSIZE(_noticeInfoLabel.text, _noticeInfoLabel.font, CGSizeMake(200, 150), NSLineBreakByCharWrapping);
    _noticeInfoLabel.frame = CGRectMake((SCREEN_WIDTH-(size.width+20))/2, SCREEN_HEIGHT/2,size.width+20, size.height+20);
    _noticeInfoLabel.alpha = 0.7;
    [app.window addSubview:_noticeInfoLabel];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:2.0];
    _noticeInfoLabel.alpha = 0;
    [UIView commitAnimations];
}
//返回当前的VC
- (UIViewController *)getNowViewController
{
    UIViewController *reController = nil;
    if (_mainTabBar!=nil)
    {
        UINavigationController  *nav = (UINavigationController *)[[_mainTabBar viewControllers] objectAtIndex:_mainTabBar.selectedIndex];
        
        UIViewController *topController = [nav topViewController];
        id tempViewController = topController;
        while ([(UIViewController *)tempViewController  presentedViewController]!=nil)
        {
            tempViewController = [(UIViewController *)tempViewController  presentedViewController];
        }
        if (tempViewController!=nil)
        {
            if ([tempViewController isKindOfClass:[UINavigationController class]])
            {
                reController = [(UINavigationController *)tempViewController topViewController];
            }
            else
            {
                reController = tempViewController;
            }
        }
        else
        {
            reController = topController;
        }
    }
    //NSLog(@"getNowViewController is %@....",[reController class]);
    return reController;
}
@end
