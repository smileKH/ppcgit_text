//
//  HBPopView.h
//  HBPopView
//
//  Created by 胡明波 on 2016/12/12.
//  Copyright © 2016年 mingboJob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBPopView : UIView
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * thumbnail;
@property (nonatomic, copy)void(^clickPopViewImgView)(NSString *urlStr);
- (void)show;

@end
