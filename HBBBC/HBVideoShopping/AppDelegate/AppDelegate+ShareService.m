//
//  AppDelegate+ShareService.m
//  HBApparelHome
//
//  Created by 胡明波 on 2017/12/29.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "AppDelegate+ShareService.h"

@implementation AppDelegate (ShareService)
//初始化 UMeng分享
-(void)initUMeng{
    
}
//初始化 极光分享
-(void)initJSHAREService{
    JSHARELaunchConfig *config = [[JSHARELaunchConfig alloc] init];
    config.appKey = @"bf37076bda8d2f6a06fb3617";
    config.SinaWeiboAppKey = @"374535501";
    config.SinaWeiboAppSecret = @"baccd12c166f1df96736b51ffbf600a2";
    config.SinaRedirectUri = @"https://www.jiguang.cn";
    config.QQAppId = @"1105864531";//需要
    config.QQAppKey = @"glFYjkHQGSOCJHMC";//需要
    config.WeChatAppId = WXAppID;
    config.WeChatAppSecret = AppSecret;
    config.FacebookAppID = @"1847959632183996";
    config.FacebookDisplayName = @"JShareDemo";
    config.TwitterConsumerKey = @"4hCeIip1cpTk9oPYeCbYKhVWi";
    config.TwitterConsumerSecret = @"DuIontT8KPSmO2Y1oAvby7tpbWHJimuakpbiAUHEKncbffekmC";
    config.isSupportWebSina = YES;
    [JSHAREService setupWithConfig:config];
    [JSHAREService setDebug:YES];
}

//分享文字
- (void)shareTextWithPlatform:(JSHAREPlatform)platform andShareText:(NSString *)shareText {
    JSHAREMessage *message = [JSHAREMessage message];
    message.text = shareText;
    message.platform = platform;
    message.mediaType = JSHAREText;
    [JSHAREService share:message handler:^(JSHAREState state, NSError *error) {
        [self showAlertWithState:state error:error];
    }];
}
//分享单张图片加文字
- (void)shareImageWithPlatform:(JSHAREPlatform)platform andShareText:(NSString *)shareText andSingleImage:(NSString*)imgUrl{
    JSHAREMessage *message = [JSHAREMessage message];
    NSString *imageURL = imgUrl;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
    
    message.mediaType = JSHAREImage;
    message.text = shareText;
    message.platform = platform;
    message.image = imageData;
    
    
    
    /*QQ 空间 / Facebook/Messenger /Twitter 支持多张图片
     1.QQ 空间图片数量限制为20张。若只分享单张图片使用 image 字段即可。
     2.Facebook/Messenger 图片数量限制为6张。如果分享单张图片，图片大小建议不要超过12M；如果分享多张图片，图片大小建议不要超过700K，否则可能出现重启手机或者不能分享。
     3、Twitter最多支持4张*/
    
    //message.images = @[imageData,imageData];
    [JSHAREService share:message handler:^(JSHAREState state, NSError *error) {
        [self showAlertWithState:state error:error];
    }];
}
//分享链接
- (void)shareLinkWithPlatform:(JSHAREPlatform)platform title:(NSString *)title andShareText:(NSString *)shareText andSingleImage:(NSString*)imgUrl andLineUrl:(NSString *)lineUrl{
    JSHAREMessage *message = [JSHAREMessage message];
    message.mediaType = JSHARELink;
    message.url = lineUrl;
    message.text = shareText;
    message.title = title;
    message.platform = platform;
    NSString *imageURL = imgUrl;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
    
    message.image = imageData;
    [JSHAREService share:message handler:^(JSHAREState state, NSError *error) {
        [self showAlertWithState:state error:error];
    }];
}

- (void)showAlertWithState:(JSHAREState)state error:(NSError *)error{
    
    NSString *string = nil;
    if (error) {
        string = [NSString stringWithFormat:@"分享失败,error:%@", error.description];
    }else{
        switch (state) {
            case JSHAREStateSuccess:
                string = @"分享成功";
                break;
            case JSHAREStateFail:
                string = @"分享失败";
                break;
            case JSHAREStateCancel:
                string = @"分享取消";
                break;
            case JSHAREStateUnknown:
                string = @"分享失败";
                break;
            default:
                break;
        }
    }
    
    UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:nil message:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [Alert show];
    });
}


@end
