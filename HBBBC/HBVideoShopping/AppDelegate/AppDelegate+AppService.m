//
//  AppDelegate+AppService.m
//  MiAiApp
//
//  Created by 徐阳 on 2017/5/19.
//  Copyright © 2017年 徐阳. All rights reserved.
//

#import "AppDelegate+AppService.h"
#import <UMSocialCore/UMSocialCore.h>
#import "OpenUDID.h"
@interface AppDelegate()

@end

@implementation AppDelegate (AppService)


#pragma mark ————— 初始化服务 —————
-(void)initService{
    //注册登录状态监听
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginStateChange:)
                                                 name:KNotificationLoginStateChange
                                               object:nil];    
    
    //网络状态监听
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(netWorkStateChange:)
                                                 name:KNotificationNetWorkStateChange
                                               object:nil];
}

#pragma mark ————— 初始化window —————
-(void)initWindow{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = white_Color;
    [self.window makeKeyAndVisible];
    [[UIButton appearance] setExclusiveTouch:YES];
//    [[UIButton appearance] setShowsTouchWhenHighlighted:YES];
    [UIActivityIndicatorView appearanceWhenContainedIn:[MBProgressHUD class], nil].color = white_Color;
    if (@available(iOS 11.0, *)){
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
}


#pragma mark ————— 初始化用户系统 —————
-(void)initUserManager{
    
    NSLog(@"设备IMEI ：%@",[OpenUDID value]);
    //获取账号信息
//    NSDictionary *accountDic = [[NSUserDefaults standardUserDefaults] objectForKey:AUTOLOGIN];
//    NSString *account = [accountDic objectForKey:ACCOUNT];
//    NSString *password = [accountDic objectForKey:PASSWORD];
//
//    if ([HBHuTool isJudgeString:account]) {
//         bPostNotification(KNotificationLoginStateChange, @NO)
//        return;
//    }
//    if ([HBHuTool isJudgeString:password]) {
//         bPostNotification(KNotificationLoginStateChange, @NO)
//        return;
//    }w
    //如果token还在，那么就算是登录了
    NSString *accessToken =  LocalValue(TOKEN_XY_APP);
    NSLog(@"当前token为----------%@",accessToken);
    if (![HBHuTool isJudgeString:accessToken]) {
        //请求个人资料
        [userManager requestUserInfo:nil completion:^(BOOL success, NSString *des) {
            if (success) {
                //
                userManager.loginState = 1;//默认登录
                [[NSNotificationCenter defaultCenter]postNotificationName:NotificationLoginSuccess object:nil];
                //发一个通知
            }else{
                
            }
        }];
        NSLog(@"我已经登录过了哦");
    }
    //如果有本地数据，展示launch界面，并且自动登录
    self.mainTabBar = [HBMainTabBarController new];
    self.window.rootViewController = self.mainTabBar;
    
//    //如果有本地数据，先展示TabBar 随后异步自动登录
//    if([userManager loadUserInfo]){
//        //这里要控制五秒才显示登录成功
//        [userManager autoLoginToServer:^(BOOL success, NSString *des) {
//            if (success) {
//                NSLog(@"自动登录成功");
//                bPostNotification(KNotificationAutoLoginSuccess, nil);
//            }else{
////                 bPostNotification(KNotificationLoginStateChange, @NO)
//                //[MBProgressHUD showErrorMessage:NSStringFormat(@"自动登录失败：%@",des)];
//            }
//        }];
//
//    }else{
//        //没有登录过，展示登录页面
////        bPostNotification(KNotificationLoginStateChange, @NO)
//    }
}
#pragma mark ————— 请求配置 —————
-(void)initRequestService{
    // 关闭日志
    [LBService closeLog];
    //关闭加密
    [LBService closeAES];
    //设置请求类型和返回类型
    [PPNetworkHelper setRequestSerializer:PPRequestSerializerHTTP];
    [PPNetworkHelper setResponseSerializer:PPResponseSerializerJSON];
    [PPNetworkHelper setAFHTTPSessionManagerProperty:^(AFHTTPSessionManager *sessionManager) {
        sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/*",@"text/encode", nil];
    }];
   
    NSLog(@"网络缓存大小cache = %fKB",[PPNetworkCache getAllHttpCacheSize]/1024.f);
    
}

#pragma mark ————— 登录状态处理 —————
- (void)loginStateChange:(NSNotification *)notification
{
    BOOL loginSuccess = [notification.object boolValue];
    
    if (loginSuccess) {//登陆成功加载主窗口控制器
        
        //为避免自动登录成功刷新tabbar
        if (!self.mainTabBar || ![self.window.rootViewController isKindOfClass:[HBMainTabBarController class]]) {
            self.mainTabBar = [HBMainTabBarController new];

            CATransition *anima = [CATransition animation];
//            anima.type = @"fade";//设置动画的类型
            anima.type = kCATransitionFade;
            anima.subtype = kCATransitionFromRight; //设置动画的方向
            anima.duration = 1.0f;
            
            self.window.rootViewController = self.mainTabBar;
            
            [bAppWindow.layer addAnimation:anima forKey:@"revealAnimation"];
            
        }
        
    }else {//登陆失败加载登陆页面控制器
        
        self.mainTabBar = nil;
//        HBRootNavigationController *loginNavi =[[HBRootNavigationController alloc] initWithRootViewController:[HBLoginVC new]];
//
//        CATransition *anima = [CATransition animation];
////        anima.type = @"fade";//设置动画的类型
//        anima.type = kCATransitionFade;
//        anima.subtype = kCATransitionFromRight; //设置动画的方向
//        anima.duration = 1.0f;
//
//        self.window.rootViewController = loginNavi;
//
//        [bAppWindow.layer addAnimation:anima forKey:@"revealAnimation"];
        
    }
    
    //展示FPS
    [HBAppManager showFPS];
}


#pragma mark ————— 网络状态变化 —————
- (void)netWorkStateChange:(NSNotification *)notification
{
    
    BOOL isNetWork = [notification.object boolValue];
    
    if (isNetWork) {//有网络
        if ([userManager loadUserInfo] && !isLogin) {//有用户数据 并且 未登录成功 重新来一次自动登录
            [userManager autoLoginToServer:^(BOOL success, NSString *des) {
                if (success) {
                    NSLog(@"网络改变后，自动登录成功");
//                    [MBProgressHUD showSuccessMessage:@"网络改变后，自动登录成功"];
                    bPostNotification(KNotificationAutoLoginSuccess, nil);
                }else{
                    [MBProgressHUD showErrorMessage:NSStringFormat(@"自动登录失败：%@",des)];
                }
            }];
        }
        
    }else {//登陆失败加载登陆页面控制器
        [MBProgressHUD showTopTipMessage:@"网络状态不佳" isWindow:YES];
    }
}




#pragma mark ————— 网络状态监听 —————
- (void)monitorNetworkStatus
{
    // 网络状态改变一次, networkStatusWithBlock就会响应一次
    [PPNetworkHelper networkStatusWithBlock:^(PPNetworkStatusType networkStatus) {
        
        switch (networkStatus) {
                // 未知网络
            case PPNetworkStatusUnknown:
                NSLog(@"网络环境：未知网络");
                // 无网络
            case PPNetworkStatusNotReachable:
                NSLog(@"网络环境：无网络");
//                KPostNotification(KNotificationNetWorkStateChange, @NO);
                break;
                // 手机网络
            case PPNetworkStatusReachableViaWWAN:
                NSLog(@"网络环境：手机自带网络");
                // 无线网络
            case PPNetworkStatusReachableViaWiFi:
                NSLog(@"网络环境：WiFi");
//                KPostNotification(KNotificationNetWorkStateChange, @YES);
                break;
        }
        if(networkStatus ==PPNetworkStatusReachableViaWWAN || networkStatus == PPNetworkStatusReachableViaWiFi)
        {
            NSLog(@"有网");
            //如果有网，消除记号 并且发一个通知给需要的界面
            NSString *networkTag = [[NSUserDefaults standardUserDefaults] objectForKey:NetworkThereAnyTag];
            if ([networkTag isEqualToString:@"1"]) {
                //刚刚有网，发一个通知
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:@"0" forKey:NetworkThereAnyTag];
                [userDefaults synchronize];
                [[NSNotificationCenter defaultCenter]postNotificationName:NotificationHaveNet object:nil];
            }
            
        }else
        {
            NSLog(@"没有网");
            //如果没有网，增加一个记号
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:@"1" forKey:NetworkThereAnyTag];
            [userDefaults synchronize];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:NotificationBrokenNetwork object:nil];
        }
        
    }];
    
}
//设置AppDelegate单例
+ (AppDelegate *)shareAppDelegate{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

//返回当前控制器
-(UIViewController *)getCurrentVC{
    
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}
//返回当前UI控制器
-(UIViewController *)getCurrentUIVC
{
    UIViewController  *superVC = [self getCurrentVC];
    
    if ([superVC isKindOfClass:[UITabBarController class]]) {
        
        UIViewController  *tabSelectVC = ((UITabBarController*)superVC).selectedViewController;
        
        if ([tabSelectVC isKindOfClass:[UINavigationController class]]) {
            
            return ((UINavigationController*)tabSelectVC).viewControllers.lastObject;
        }
        return tabSelectVC;
    }else
        if ([superVC isKindOfClass:[UINavigationController class]]) {
            
            return ((UINavigationController*)superVC).viewControllers.lastObject;
        }
    return superVC;
}


@end
