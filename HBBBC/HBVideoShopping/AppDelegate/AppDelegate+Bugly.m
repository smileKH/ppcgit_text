//
//  AppDelegate+Bugly.m
//  HBVideoShopping
//
//  Created by hua on 2018/4/27.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "AppDelegate+Bugly.h"

#define BUGLY_APP_ID @"1afd5300f8"
@implementation AppDelegate (Bugly)
-(void)initBugly{
    BuglyConfig * config = [[BuglyConfig alloc] init];
    #if DEBUG
    config.debugMode = YES;
    config.consolelogEnable = YES;
    config.viewControllerTrackingEnable = YES;
    #endif
    config.blockMonitorEnable = YES;
    config.blockMonitorTimeout = 1.5;
    config.channel = @"Bugly";
    config.delegate = self;
    [Bugly startWithAppId:BUGLY_APP_ID
#if DEBUG
        developmentDevice:YES
#endif
                   config:config];
    
}
#pragma mark - BuglyDelegate
- (NSString *)attachmentForException:(NSException *)exception {
    NSLog(@"异常信息F:(%@:%d) %s %@",[[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__,exception);
    
    return @"This is an attachment";
}
@end
