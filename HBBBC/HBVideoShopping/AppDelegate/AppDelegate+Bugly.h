//
//  AppDelegate+Bugly.h
//  HBVideoShopping
//
//  Created by hua on 2018/4/27.
//  Copyright © 2018年 FirstVision. All rights reserved.
//

#import "AppDelegate.h"
#import <Bugly/Bugly.h>
@interface AppDelegate (Bugly)<BuglyDelegate>
-(void)initBugly;
@end
