//
//  AppDelegate+ShareService.h
//  HBApparelHome
//
//  Created by 胡明波 on 2017/12/29.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "AppDelegate.h"
// 引入JSHAREService功能所需头文件
#import "JSHAREService.h"
@interface AppDelegate (ShareService)
//初始化 UMeng分享
-(void)initUMeng;

//初始化 极光分享
-(void)initJSHAREService;
//分享文字
- (void)shareTextWithPlatform:(JSHAREPlatform)platform andShareText:(NSString *)shareText;
//分享单张图片加文字
- (void)shareImageWithPlatform:(JSHAREPlatform)platform andShareText:(NSString *)shareText andSingleImage:(NSString*)imgUrl;
//分享链接
- (void)shareLinkWithPlatform:(JSHAREPlatform)platform title:(NSString *)title andShareText:(NSString *)shareText andSingleImage:(NSString*)imgUrl andLineUrl:(NSString *)lineUrl;
@end
