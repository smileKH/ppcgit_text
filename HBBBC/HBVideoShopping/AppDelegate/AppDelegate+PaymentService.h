//
//  AppDelegate+PaymentService.h
//  HBApparelHome
//
//  Created by 胡明波 on 2017/12/29.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (PaymentService)
//注册微信支付
-(void)registerWXAndPayApp;
@end
