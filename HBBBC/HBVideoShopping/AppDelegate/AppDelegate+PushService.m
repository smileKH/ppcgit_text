//
//  AppDelegate+PushService.m
//  MiAiApp
//
//  Created by 徐阳 on 2017/5/25.
//  Copyright © 2017年 徐阳. All rights reserved.
//

#import "AppDelegate+PushService.h"
#import "UMessage.h"
#import "HBPopView.h"
//#import "HBMessageVC.h"
//#import "HBMeOrderVC.h"
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 100000
#import <UserNotifications/UserNotifications.h>
#endif
//以下几个库仅作为调试引用引用的
#import <AdSupport/AdSupport.h>
#import <CommonCrypto/CommonDigest.h>
@interface AppDelegate()<UNUserNotificationCenterDelegate>

@end
@implementation AppDelegate (PushService)
-(void)initUMengMessageWithOptions:(NSDictionary *)launchOptions{
    //设置 AppKey 及 LaunchOptions
    [UMessage startWithAppkey:UMAppKey launchOptions:launchOptions httpsEnable:YES ];
    [UMessage openDebugMode:YES];
    //UIStoryboard *board=[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    //    UIViewController *vc = [[HBLoginViewController alloc]init];
    [UMessage addLaunchMessageWithWindow:self.window finishViewController:app.mainTabBar];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")){
        //iOS10以上必须加下面这段代码。
        if (@available(iOS 10.0, *)) {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate=self;
            UNAuthorizationOptions types10=UNAuthorizationOptionBadge|UNAuthorizationOptionAlert|UNAuthorizationOptionSound;
            [center requestAuthorizationWithOptions:types10 completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if (granted) {
                    //点击允许
                    
                } else {
                    //点击不允许
                    
                }
            }];
            
            UNNotificationAction *action1_ios10 = [UNNotificationAction actionWithIdentifier:@"action1_ios10_identifier" title:@"打开应用" options:UNNotificationActionOptionForeground];
            UNNotificationAction *action2_ios10 = [UNNotificationAction actionWithIdentifier:@"action2_ios10_identifier" title:@"忽略" options:UNNotificationActionOptionForeground];
            
            //UNNotificationCategoryOptionNone
            //UNNotificationCategoryOptionCustomDismissAction  清除通知被触发会走通知的代理方法
            //UNNotificationCategoryOptionAllowInCarPlay       适用于行车模式
            UNNotificationCategory *category1_ios10 = [UNNotificationCategory categoryWithIdentifier:@"category101" actions:@[action1_ios10,action2_ios10]   intentIdentifiers:@[] options:UNNotificationCategoryOptionCustomDismissAction];
            NSSet *categories_ios10 = [NSSet setWithObjects:category1_ios10, nil];
            [center setNotificationCategories:categories_ios10];
            
        }
        //注册通知
        [UMessage registerForRemoteNotifications];
        
    }else if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
        action1.identifier = @"action1_identifier";
        action1.title=@"打开应用";
        action1.activationMode = UIUserNotificationActivationModeForeground;//当点击的时候启动程序
        
        UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];  //第二按钮
        action2.identifier = @"action2_identifier";
        action2.title=@"忽略";
        action2.activationMode = UIUserNotificationActivationModeBackground;//当点击的时候不启动程序，在后台处理
        action2.authenticationRequired = YES;//需要解锁才能处理，如果action.activationMode = UIUserNotificationActivationModeForeground;则这个属性被忽略；
        action2.destructive = YES;
        UIMutableUserNotificationCategory *actionCategory1 = [[UIMutableUserNotificationCategory alloc] init];
        actionCategory1.identifier = @"category1";//这组动作的唯一标示
        [actionCategory1 setActions:@[action1,action2] forContext:(UIUserNotificationActionContextDefault)];
        NSSet *categories = [NSSet setWithObjects:actionCategory1, nil];
        //注册通知
        [UMessage registerForRemoteNotifications:categories];
        
        
        
    }else{//8.0以下的
        [UMessage registerForRemoteNotifications];
        
    }
    
    //如果对角标，文字和声音的取舍，请用下面的方法
    //        UIRemoteNotificationType types7 = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
    //        UIUserNotificationType types8 = UIUserNotificationTypeAlert|UIUserNotificationTypeSound|UIUserNotificationTypeBadge;
    //        [UMessage registerForRemoteNotifications:categories withTypesForIos7:types7 withTypesForIos8:types8];
    
    //for log
    [UMessage setAutoAlert:NO];
    [UMessage setLogEnabled:YES];
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    //1.2.7版本开始不需要用户再手动注册devicetoken，SDK会自动注册
    // [UMessage registerDeviceToken:deviceToken];
    //用户可以在这个方法里面获取devicetoken
    NSLog(@"打印设备%@",[[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]
                      stringByReplacingOccurrencesOfString: @">" withString: @""]
                     stringByReplacingOccurrencesOfString: @" " withString: @""]);
    NSLog(@"打印设备token%@",deviceToken);
    //下面这句代码只是在demo中，供页面传值使用。
    
}
#pragma mark ==========iOS10以下需要实现的方法==========
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //关闭友盟自带的弹出框
    [UMessage setAutoAlert:NO];
    [UMessage didReceiveRemoteNotification:userInfo];
    
    //        self.userInfo = userInfo;
    //定制自定的的弹出框
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"标题"
                                                            message:@"Test On ApplicationStateActive"
                                                           delegate:self
                                                  cancelButtonTitle:@"确定"
                                                  otherButtonTitles:nil];
        
        [alertView show];
        
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSString stringWithFormat:@"%@",userInfo] forKey:@"UMPuserInfoNotification"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userInfoNotification" object:self userInfo:@{@"userinfo":[NSString stringWithFormat:@"%@",userInfo]}];
    
}
//iOS10新增：处理前台收到通知的代理方法
#pragma mark ==========APP在前台的时候收到通知方法==========
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler NS_AVAILABLE_IOS(10_0){
    NSDictionary * userInfo = notification.request.content.userInfo;
    //    UNNotificationRequest *request = notification.request;//收到推送的请求
    //    UNNotificationContent *content = request.content;//收到推送的消息内容
    //    NSNumber *badge = content.badge;//推送消息的角标
    //    NSString *body = content.body;//推送消息体
    //    UNNotificationSound *sound = content.sound;//推送消息的声音
    //    NSString *subtitle = content.subtitle;// 推送消息的副标题
    //    NSString *title = content.title; //推送消息的标题
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [UMessage setAutoAlert:NO];
        //应用处于前台时的远程推送接受
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:[NSString stringWithFormat:@"%@",userInfo] forKey:@"UMPuserInfoNotification"];
        NSLog(@"打印一下在前台收到的消息通知:%@",userInfo);
        //这里是在前台接受到的消息通知，比如说弹窗，跳到网页，跳到物流，跳到我的信息界面
    }else{
        //应用处于前台时的本地推送接受
    }
    completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
}

//iOS10新增：处理后台点击通知的代理方法
#pragma mark ==========APP在后台点击后打印的方法，点击后的方法==========
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    if (@available(iOS 10.0, *)) {
        NSDictionary * userInfo = response.notification.request.content.userInfo;
        if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
            //应用处于后台时的远程推送接受
            //必须加这句代码
            [UMessage didReceiveRemoteNotification:userInfo];
            
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:[NSString stringWithFormat:@"%@",userInfo] forKey:@"UMPuserInfoNotification"];
            NSLog(@"点击消息通知的时候出来:%@",userInfo);
            //处理iOS 10以上的推送通知
            [self customPushServiceUserInfo:userInfo];
            
            
        }else{
            //应用处于后台时的本地推送接受
            NSLog(@"在后台接受通知的时候打印通知返回的信息:%@",userInfo);
        }
    }
}

#pragma mark ==========自定义推送处理==========
-(void)customPushServiceUserInfo:(id)userInfo{
    
    //推送类型type  1、弹出广告--》跳到网页  2、跳到消息中心  3、跳到我的订单  4、跳到配送工单
    //url:网页链接
    //thumbnail:广告图片链接
    if (ValidDict(userInfo)) {
        NSString *type = [NSString stringWithFormat:@"%@",userInfo[@"type"]];
        switch ([type intValue]) {
            case 0:
                //没有
                break;
            case 1:
                //弹出广告--》跳到网页
            {
                //看来只能发一个通知，到了首页才能使用
                if ([userManager oneceJudgeLoginState]) {
                    HBPopView *popView = [[HBPopView alloc]init];
                    popView.url = [NSString stringWithFormat:@"%@",userInfo[@"url"]];
                    popView.thumbnail = [NSString stringWithFormat:@"%@",userInfo[@"thumbnail"]];
                    popView.clickPopViewImgView = ^(NSString *urlStr){
                        //点击了图片 跳到网页
                        //                        app.mainTabBar.selectedIndex = 0;
                        UIViewController *currentVC = [app getNowViewController];
                        HBRootWebViewController *vc = [[HBRootWebViewController alloc]init];
                        vc.url = urlStr;
                        [currentVC.navigationController pushViewController:vc animated:YES];
                    };
                    [popView show];
                }
            }
                break;
            case 2:
                //跳到消息中心
            {
                if ([userManager oneceJudgeLoginState]) {
                    UIViewController *currentVC = [app getNowViewController];
//                    HBMessageVC *vc = [[HBMessageVC alloc]init];
//                    [currentVC.navigationController pushViewController:vc animated:YES];
                }
            }
                break;
            case 3:
                //跳到我的订单
            {
                if ([userManager oneceJudgeLoginState]) {
                    UIViewController *currentVC = [app getNowViewController];
//                    HBMeOrderVC*vc = [[HBMeOrderVC alloc]init];
//                    [currentVC.navigationController pushViewController:vc animated:YES];
                }
            }
                break;
            case 4:
                //跳到配送工单
            {
                if ([userManager oneceJudgeLoginState]) {
//                    UIViewController *currentVC = [app getNowViewController];
//                    HBDistributionStatusVC *vc = [[HBDistributionStatusVC alloc]init];
//                    [currentVC.navigationController pushViewController:vc animated:YES];
                }
                
            }
                break;
                
            default:
                break;
        }
        
    }
}

@end

