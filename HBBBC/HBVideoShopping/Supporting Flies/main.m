//
//  main.m
//  HBVideoShopping
//
//  Created by 胡明波 on 2017/12/17.
//  Copyright © 2017年 FirstVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
