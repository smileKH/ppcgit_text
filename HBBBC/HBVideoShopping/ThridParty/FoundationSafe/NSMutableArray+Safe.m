//
//  NSMutableArray+Safe.m
//  TestMethod
//
//  Created by js on 15/11/9.
//  Copyright © 2015年 js. All rights reserved.
//

#import "NSMutableArray+Safe.h"
#import <objc/runtime.h>
@implementation NSMutableArray (Safe)

+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //NSArray
        Method method = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), @selector(objectAtIndex:));
        Method method2 = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), @selector(safeObjectAtIndex:));
        method_exchangeImplementations(method, method2);
        #define IOSVERSION ([[[UIDevice currentDevice] systemVersion] floatValue])
        if (IOSVERSION >= 11.0) {
            
            // array[0] 简便方法的安全保护
            SEL sel = NSSelectorFromString(@"objectAtIndexedSubscript:");
            
            Method method3 = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), sel);
            
            Method method4 = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), @selector(safeObjectAtIndexedSubscript:));
            
            method_exchangeImplementations(method3, method4);
            
        }
        
        //         replaceObjectAtIndex: withObject:
        Method methodReplaceObjectAtIndex = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), @selector(replaceObjectAtIndex:withObject:));
        Method method2ReplaceObjectAtIndex = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), @selector(safeReplaceObjectAtIndex:withObject:));
        method_exchangeImplementations(methodReplaceObjectAtIndex, method2ReplaceObjectAtIndex);
        
        //         replaceObjectAtIndex: withObject:
        Method methodInsertObjectAtIndex = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), @selector(insertObject:atIndex:));
        Method methodInsertObjectAtIndex2 = class_getInstanceMethod(NSClassFromString(@"__NSArrayM"), @selector(safeInsertObject:atIndex:));
        method_exchangeImplementations(methodInsertObjectAtIndex, methodInsertObjectAtIndex2);
        
    });
}
- (id )safeObjectAtIndexedSubscript:(NSInteger)index{
    if (self.count > index) {
        return [self safeObjectAtIndexedSubscript:index];
    }
    
    NSLog(@"数组越界");
    return nil;
}
- (id )safeObjectAtIndex:(NSInteger)index{
    if (self.count > index) {
        return [self safeObjectAtIndex:index];
    }
    
    NSLog(@"数组越界");
    return nil;
}

- (id )objectForKey:(NSString *)index{
    return nil;
}
- (void)safeReplaceObjectAtIndex:(NSUInteger)index withObject:(id)withObject{
    
    @autoreleasepool {
        if (index < self.count && withObject) {
            [self safeReplaceObjectAtIndex:index withObject:withObject];
        }
        else{
            NSLog(@"替换数组越界或对象为空");
        }
    }
}
- (void)safeInsertObject:(id)anObject atIndex:(NSUInteger)index;{
    
    @autoreleasepool {
        if (anObject) {
            [self safeInsertObject:anObject atIndex:index];
        }else{
            NSLog(@"插入数组越界|数组为空");
        }
    }
}


@end



