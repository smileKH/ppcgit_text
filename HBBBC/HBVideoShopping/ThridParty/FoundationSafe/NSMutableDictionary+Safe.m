//
//  NSMutableDictionary+Safe.m
//  TestMethod
//
//  Created by js on 15/11/9.
//  Copyright © 2015年 js. All rights reserved.
//

#import "NSMutableDictionary+Safe.h"
#import <objc/runtime.h>
@implementation NSMutableDictionary (Safe)
+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //NSArray
        Method method = class_getInstanceMethod(NSClassFromString(@"__NSDictionaryM"), @selector(setObject:forKey:));
        Method method2 = class_getInstanceMethod(NSClassFromString(@"__NSDictionaryM"), @selector(safeSetObject:forKey:));
        method_exchangeImplementations(method, method2);
    });
}

- (void)safeSetObject:(id)object forKey:(id)key{
    if (object && key){
        [self safeSetObject:object forKey:key];
    }
    else{
        NSLog(@"字典赋值存在空 key: %@, val: %@",key,object);
    }
}
@end
