//
//  NSArray+Safe.m
//  TestMethod
//
//  Created by js on 15/11/9.
//  Copyright © 2015年 js. All rights reserved.
//

#import "NSArray+Safe.h"
#import <objc/runtime.h>
@implementation NSArray (Safe)

+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //NSArray
        Method method = class_getInstanceMethod(NSClassFromString(@"__NSArrayI"), @selector(objectAtIndex:));
        Method method2 = class_getInstanceMethod(NSClassFromString(@"__NSArrayI"), @selector(safeObjectAtIndex:));
        method_exchangeImplementations(method, method2);
        
        
        if (FoundationSafe_IOSVERSION >= 11.0) {
            // array[0] 简便方法的安全保护
            SEL sel = NSSelectorFromString(@"objectAtIndexedSubscript:");
            
            Method method3 = class_getInstanceMethod(NSClassFromString(@"__NSArrayI"), sel);
            
            Method method4 = class_getInstanceMethod(NSClassFromString(@"__NSArrayI"), @selector(safeObjectAtIndexedSubscript:));
            
            method_exchangeImplementations(method3, method4);
        }
        
        
    });
}
- (id )safeObjectAtIndexedSubscript:(NSInteger)index{
    if (self.count > index) {
        return [self safeObjectAtIndexedSubscript:index];
    }
    
    NSLog(@"数组越界");
    return nil;
}
- (id )safeObjectAtIndex:(NSInteger)index{
    if (self.count > index) {
        return [self safeObjectAtIndex:index];
    }
    
    NSLog(@"数组越界");
    return nil;
}

- (id )objectForKey:(NSString *)index{
    return nil;
}



@end
