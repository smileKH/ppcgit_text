//
//  NSMutableSet+Safe.m
//  WBApps
//
//  Created by 韩赵凯 on 16/8/3.
//  Copyright © 2016年 Weimob. All rights reserved.
//

#import "NSMutableSet+Safe.h"
#import <objc/runtime.h>


@implementation NSMutableSet (Safe)


+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        Method methodAdd = class_getInstanceMethod(NSClassFromString(@"__NSSetM"), @selector(addObject:));
        Method method2Add = class_getInstanceMethod(NSClassFromString(@"__NSSetM"), @selector(safeAddObject:));
        method_exchangeImplementations(methodAdd, method2Add);
    });
}

- (void)safeAddObject:(id)obj
{
    @autoreleasepool {
        
        if (obj)
        {
            return [self safeAddObject:obj];
        }
        NSLog(@"obj 为空，无法添加");
    }
}

@end
