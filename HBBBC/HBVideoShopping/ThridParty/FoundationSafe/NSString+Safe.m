//
//  NSString+Safe.m
//  TestMethod
//
//  Created by js on 15/11/11.
//  Copyright © 2015年 js. All rights reserved.
//

#import "NSString+Safe.h"
#import <objc/runtime.h>

@implementation NSString (Safe)

long funLongValue(id self, SEL _cmd){
    return 0;
}

+ (BOOL)resolveInstanceMethod:(SEL)sel{
    NSString *selName = NSStringFromSelector(sel);
    if ([selName isEqualToString:@"longValue"]) {
        class_addMethod([self class], sel, (IMP)funLongValue, "i@:");
        return YES;
    }
    
    return [super resolveInstanceMethod:sel];
}
@end
