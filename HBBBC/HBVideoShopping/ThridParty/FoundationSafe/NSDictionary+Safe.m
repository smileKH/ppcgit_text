//
//  NSDictionary+Safe.m
//  WMBusinessHelper
//
//  Created by 韩赵凯 on 2016/12/1.
//  Copyright © 2016年 Weimob. All rights reserved.
//

#import "NSDictionary+Safe.h"
#import <objc/runtime.h>


@implementation NSDictionary (Safe)


+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        Method method = class_getInstanceMethod(object_getClass((id)self), @selector(initWithObjects:forKeys:count:));
        Method method2 = class_getInstanceMethod(object_getClass((id)self), @selector(gl_initWithObjects:forKeys:count:));
        method_exchangeImplementations(method, method2);
        
        
        
        Method method3 = class_getInstanceMethod(object_getClass((id)self), @selector(dictionaryWithObjects:forKeys:count:));
        Method method4 = class_getInstanceMethod(object_getClass((id)self), @selector(gl_dictionaryWithObjects:forKeys:count:));
        method_exchangeImplementations(method3, method4);
    });
}

+ (instancetype)gl_dictionaryWithObjects:(const id [])objects forKeys:(const id<NSCopying> [])keys count:(NSUInteger)cnt {
    id safeObjects[cnt];
    id safeKeys[cnt];
    NSUInteger j = 0;
    for (NSUInteger i = 0; i < cnt; i++) {
        id key = keys[i];
        id obj = objects[i];
        if (!key) {
            continue;
        }
        if (!obj) {
            
            NSLog(@"字典赋值存在空 key: %@,",key);

            continue;

        }
        safeKeys[j] = key;
        safeObjects[j] = obj;
        j++;
    }
    return [self gl_dictionaryWithObjects:safeObjects forKeys:safeKeys count:j];
}

- (instancetype)gl_initWithObjects:(const id [])objects forKeys:(const id<NSCopying> [])keys count:(NSUInteger)cnt {
    id safeObjects[cnt];
    id safeKeys[cnt];
    NSUInteger j = 0;
    for (NSUInteger i = 0; i < cnt; i++) {
        id key = keys[i];
        id obj = objects[i];
        if (!key) {
            continue;
        }
        if (!obj) {
            obj = [NSNull null];
        }
        safeKeys[j] = key;
        safeObjects[j] = obj;
        j++;
    }
    return [self gl_initWithObjects:safeObjects forKeys:safeKeys count:j];
}


@end
